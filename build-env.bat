@echo off
::
:: Sets up the environment variables to build the java tree
::

set TOP=%CD%

:: versions
set ANT_VERSION=1.7.1
set ANT_CONTRIB_VERSION=1.0b3
set IVY_VERSION=2.3.0
set JDK_VERSION=1.8.0_31

set EXPORT_TOOLS=%TOP%\export\tools\build
set UNZIP_BIN=%TOP%\tools\build\unzip.exe
set CURL_BIN=%TOP%\tools\build\curl.exe

set DOWNLOAD_URL=http://source.rfcode.com:8081/artifactory

IF DEFINED RFCODE_BUILD_HOME set EXPORT_TOOLS=%RFCODE_BUILD_HOME%

IF NOT EXIST "%EXPORT_TOOLS%\win" MKDIR "%EXPORT_TOOLS%\win"

:: Unpack the JDK

set JAVA_HOME="%EXPORT_TOOLS%\win\jdk%JDK_VERSION%"
SET PATH=%JAVA_HOME%\bin;%PATH%
IF EXIST "%JAVA_HOME%" GOTO ant
cd /D "%EXPORT_TOOLS%\win"
echo Downloading JDK from subversion...
"%CURL_BIN%" %DOWNLOAD_URL%/tools/jdk/1.8/jdk%JDK_VERSION%.zip -o jdk%JDK_VERSION%.zip
"%UNZIP_BIN%" jdk%JDK_VERSION%.zip

:: Unpack Ant

:ant
set ANT_HOME=%EXPORT_TOOLS%\apache-ant-%ANT_VERSION%
set ANT_ZIP=apache-ant-%ANT_VERSION%-bin.zip
SET PATH=%ANT_HOME%\bin;%PATH%
IF EXIST "%ANT_HOME%" GOTO ant_contrib
cd /D "%EXPORT_TOOLS%"
echo Downloading ant from subversion...
svn export %SVN_URL%/tools/ant/%ANT_ZIP%
"%UNZIP_BIN%" %ANT_ZIP%

:: Needed by mail task
svn export %SVN_URL%/ivy/ivy-releases/javamail/javamail/1.4.1/javamail-1.4.1.jar %ANT_HOME%\lib\javamail-1.4.1.jar

:: Unpack ant contrib

:ant_contrib
set ANT_CONTRIB_HOME=%EXPORT_TOOLS%\ant-contrib
set ANT_CONTRIB_ZIP=ant-contrib-%ANT_CONTRIB_VERSION%-bin.zip
SET PATH=%ANT_CONTRIB_HOME%\bin;%PATH%
IF EXIST %ANT_CONTRIB_HOME% GOTO ivy
cd /D "%EXPORT_TOOLS%"
echo Downloading ant contrib from subversion...
svn export %SVN_URL%/tools/ant/%ANT_CONTRIB_ZIP%
"%UNZIP_BIN%" %ANT_CONTRIB_ZIP%

:: Unpack ivy

:ivy
set IVY_HOME=%EXPORT_TOOLS%\apache-ivy-%IVY_VERSION%
set IVY_ZIP=apache-ivy-%IVY_VERSION%-bin.zip
SET PATH=%IVY_HOME%\bin;%PATH%
IF EXIST %IVY_HOME% GOTO end
cd /D "%EXPORT_TOOLS%"
echo Downloading ivy from subversion...
svn export %SVN_URL%/tools/ant/%IVY_ZIP%
"%UNZIP_BIN%" %IVY_ZIP%

:end

::
:: PATH

cd /D %TOP%

