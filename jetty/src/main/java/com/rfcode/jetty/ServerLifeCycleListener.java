//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2007-2012. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//Class Name:          ServerLifeCycleListener
//
//Written By:          Richie Jefts
//------------------------------ tabstop = 4 ----------------------------------
package com.rfcode.jetty;

import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import java.util.Properties;

import org.eclipse.jetty.util.component.LifeCycle;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.server.ServerConnector;

/**
 * Listens for the jetty server starting lifecycle event and adds connectors to
 * the server based on properties in the system.properties file.
 */
public class ServerLifeCycleListener implements LifeCycle.Listener {

    private static final String HTTP_HOST = "http.host";
    private static final String HTTP_PORT = "http.port";

    private static final String HTTPS_HOST = "https.host";
    private static final String HTTPS_PORT = "https.port";

    private static final String HTTP_LOCALHOST_PORT = "http.localhost.port";

    private static final String HTTPS_CERT_PASSWORD = "https.certPassword";
    private static final String HTTPS_KEYSTORE_PASSWORD = "https.keyStorePassword";

    private static final String DEFAULT_CONF_DIRECTORY = System.getProperty("user.home") + File.separator + ".rfcode";

    @Override()
    public void lifeCycleStarting(LifeCycle event) {
        Server     server = (Server) event;
        Properties props  = loadProperties();

        // HTTPS connector
        int https = getInt(props, HTTPS_PORT, -1);
        if (https > 0) {
            File keystore = new File(getConfigDirectory(), "keystore");
            SslContextFactory sslcf = new SslContextFactory();
            sslcf.setKeyStorePath(keystore.getAbsolutePath());
            sslcf.setKeyStorePassword(props.getProperty(HTTPS_CERT_PASSWORD));
            sslcf.setKeyManagerPassword(props.getProperty(HTTPS_KEYSTORE_PASSWORD));

            ServerConnector con = new ServerConnector(server, sslcf);
            con.setHost(props.getProperty(HTTPS_HOST));
            con.setPort(https);
            con.setIdleTimeout(60000);

            server.addConnector(con);
        }

        // HTTP connector
        int http = getInt(props, HTTP_PORT, (https > 0 ? -1 : 6580));
        if (http > 0) {
            ServerConnector con = new ServerConnector(server);
            con.setHost(props.getProperty(HTTP_HOST));
            con.setPort(http);
            con.setIdleTimeout(60000);
            server.addConnector(con);
        }

        // localhost for upconnect
        int localhostport = getInt(props, HTTP_LOCALHOST_PORT, 6580);
        System.out.println(HTTP_LOCALHOST_PORT + ": " + localhostport);
        if (http != localhostport) {
            ServerConnector con = new ServerConnector(server);
            con.setHost("localhost");
            con.setPort(localhostport);
            con.setIdleTimeout(60000);
            server.addConnector(con);
        }
    }

    private int getInt(Properties props, String name, int defaultValue) {
        String value  = props.getProperty(name);
        int    result = defaultValue;
        if (value != null) {
            try {
                result = Integer.parseInt(value);
            }
            catch (NumberFormatException ignored) {
            }
        }
        return result;
    }

    private Properties loadProperties() {
        File        file  = new File(getConfigDirectory(), "system.properties");
        Properties  props = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(file);
            props.load(input);
        }
        catch (IOException e) {
            System.out.println("Unable to load properties file: " + file);
            props = new Properties();
        }
        finally {
            try {
                if (input != null) input.close();
            }
            catch (IOException ignored) {
            }
        }
        return props;
    }

    private File getConfigDirectory() {
        String conf = System.getProperty("rfcode.conf");
        if (conf == null) {
            conf = DEFAULT_CONF_DIRECTORY;
        }
        return new File(conf);
    }

    @Override()
    public void lifeCycleStarted(LifeCycle event) {
    }

    @Override()
    public void lifeCycleFailure(LifeCycle event,Throwable cause) {
    }

    @Override()
    public void lifeCycleStopping(LifeCycle event) {
    }

    @Override()
    public void lifeCycleStopped(LifeCycle event) {
    }
}
