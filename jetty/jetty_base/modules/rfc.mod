#
# Deploy Feature
#

[depend]
webapp

[lib]
../lib/com.rfcode.jetty-@VERSION@.jar
../lib/asm-5.0.1.jar
../lib/asm-commons-5.0.1.jar
../lib/javax.annotation-api-1.2.jar
../lib/jsp/*.jar

