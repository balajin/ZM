package com.rfcode.ranger.commands;

import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.mantis2.MantisIITagType04V;
import com.rfcode.ranger.CommandContext;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by dy on 9/28/16.
 */
public class DetectedTagsHandlerTest {

    @Test
    public void testInvokeCommand() throws DuplicateEntityIDException {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        StringBuilder sb = new StringBuilder();

        CommandContext context = mock(CommandContext.class);
        Tag rat = mock(Tag.class);
        when(rat.getTagGUID()).thenReturn("RATRAT0000001");
        when(context.appendAttrib(anyObject(), anyObject(), anyObject(), anyBoolean()))
                .thenAnswer(ivc -> sb.append(ivc.getArguments()[2]));


        TagGroup tagGroup = new TagGroup(new MantisIITagType04V()) {

            {
                addTag(rat);
                setID("MantisIITagType04V");
            }

            @Override
            public boolean isMatchingTagID(String tag_guid) {
                return false;
            }
        };
        ReaderEntityDirectory.addEntity(tagGroup);

        handler.invokeCommand(DetectedTagsHandler.DETECTED_TAGS_LIST, context, Collections.emptyMap());

        verify(context, times(1)).sendString(anyString());
        assertTrue(sb.toString().contains("RATRAT0000001"));
    }

    @Test
    public void testInvokeWrongCommand() throws DuplicateEntityIDException {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        CommandContext context = mock(CommandContext.class);

        handler.invokeCommand("", context, Collections.emptyMap());

        verify(context, times(1)).sendString(same(DetectedTagsHandler.NO_HELP));
    }

    @Test
    public void testTranslateCommandLine() {
        assertTrue(new DetectedTagsHandler().translateCommandLine("", Collections.emptyList()).isEmpty());
    }

    @Test
    public void testGetCommandIDs() {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        assertTrue(handler.getCommandIDs().length == 1);
        assertEquals(DetectedTagsHandler.DETECTED_TAGS_LIST, handler.getCommandIDs()[0]);
    }

    @Test
    public void testGetCommandHelp() {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandHelp(cmd).length > 0);
        }

        assertTrue(Arrays.equals(new String[]{DetectedTagsHandler.NO_HELP}, handler.getCommandHelp("")));
    }

    @Test
    public void testGetCommandSyntaxHelp() {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandSyntaxHelp(cmd).length > 0);
        }

        assertTrue(Arrays.equals(new String[]{DetectedTagsHandler.NO_HELP}, handler.getCommandSyntaxHelp("")));
    }

    @Test
    public void testGetCommandWebSyntaxHelp() {
        DetectedTagsHandler handler = new DetectedTagsHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandWebSyntaxHelp(cmd).length > 0);
        }

        assertTrue(Arrays.equals(new String[]{DetectedTagsHandler.NO_HELP}, handler.getCommandWebSyntaxHelp("")));
    }

    @Test
    public void testIsAdminRequired() {
        assertFalse(new DetectedTagsHandler().isAdminRequired());
    }

    @Test
    public void testIsConfigReadRequired() {
        assertFalse(new DetectedTagsHandler().isConfigReadRequired());
    }
}
