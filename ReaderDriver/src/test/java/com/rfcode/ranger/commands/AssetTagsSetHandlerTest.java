package com.rfcode.ranger.commands;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;

import com.rfcode.ranger.container.Containers;
import org.junit.Test;

import java.util.*;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static junit.framework.Assert.*;

/**
 * Created by dy on 9/21/16.
 */
public class AssetTagsSetHandlerTest {

    @Test
    public void testInvokeAddCommand() throws CommandException {
        // define
        String tagId1 = "THSCK00000001";
        String tagId2 = "THSCK00000002";
        Map<String, String> tags = new HashMap<>();
        tags.put(tagId1, "");
        tags.put(tagId2, "");
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        CommandContext ctx = mock(CommandContext.class);

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, ctx, tags);

        // check
        assertTrue(Containers.assetTags().contains(tagId1));
        assertTrue(Containers.assetTags().contains(tagId2));
    }

    @Test
    public void testInvokeRemoveCommand() throws CommandException {
        // define
        String tagId1 = "THSCK00000001";
        String tagId2 = "THSCK00000002";
        Map<String, String> tags = new HashMap<>();
        tags.put(tagId1, "");
        tags.put(tagId2, "");
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        CommandContext ctx = mock(CommandContext.class);

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, ctx, tags);

        // check
        assertTrue(Containers.assetTags().contains(tagId1));
        assertTrue(Containers.assetTags().contains(tagId2));

        // define
        tags.remove(tagId1);

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_REMOVE, ctx, tags);

        //check
        assertTrue(Containers.assetTags().contains(tagId1));
        assertFalse(Containers.assetTags().contains(tagId2));
    }

    @Test
    public void testInvokeRemoveAllCommand() throws CommandException {
        // define
        String tagId1 = "THSCK00000001";
        String tagId2 = "THSCK00000002";
        Map<String, String> tags = new HashMap<>();
        tags.put(tagId1, "");
        tags.put(tagId2, "");
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        CommandContext ctx = mock(CommandContext.class);

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, ctx, tags);

        // check
        assertTrue(Containers.assetTags().contains(tagId1));
        assertTrue(Containers.assetTags().contains(tagId2));

        // define
        tags.clear();
        tags.put("*", "");

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_REMOVE, ctx, tags);

        //check
        assertFalse(Containers.assetTags().contains(tagId1));
        assertFalse(Containers.assetTags().contains(tagId2));
    }

    @Test
    public void testInvokeListCommand() throws CommandException {
        // define
        String tagId1 = "THSCK00000001";
        String tagId2 = "THSCK00000002";
        Map<String, String> tags = new HashMap<>();
        tags.put(tagId1, "");
        tags.put(tagId2, "");

        StringBuilder sb = new StringBuilder();
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        CommandContext ctx = mock(CommandContext.class);
        when(ctx.appendStartObject(anyObject(), anyString(), anyString(), anyBoolean())).thenReturn(sb);
        when(ctx.appendAttrib(anyObject(), anyString(), anyObject(), anyBoolean())).thenAnswer(ivc -> sb.append(ivc.getArguments()[2]));
        when(ctx.appendEndObject(anyObject(), anyString(), anyString())).thenReturn(sb);
        doNothing().when(ctx).sendString(anyString());

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, ctx, tags);
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_LIST, ctx, Collections.emptyMap());

        // check
        verify(ctx, times(1)).appendStartObject(anyObject(), anyString(), anyString(), anyBoolean());
        verify(ctx, times(1)).appendEndObject(anyObject(), anyString(), anyString());
        verify(ctx, times(1)).appendAttrib(anyObject(), anyString(), anyObject(), anyBoolean());
        verify(ctx, times(1)).sendString(anyString());

        assertEquals(Arrays.toString(new String[]{tagId1, tagId2}), sb.toString());
    }

    @Test(expected = CommandException.class)
    public void testInvokeListCommandWithCtxException() throws CommandException {
        // define
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        CommandContext ctx = mock(CommandContext.class);
        doThrow(CommandException.class).when(ctx).sendString(anyString());

        // call
        handler.invokeCommand(AssetTagsSetHandler.ASSET_TAGS_SET_LIST, ctx, Collections.emptyMap());
    }


    @Test
    public void testInvokeNullCommand() {
        // define
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        Map<String, String> params = mock(Map.class);
        CommandContext ctx = mock(CommandContext.class);

        // call
        handler.invokeCommand(null, ctx, params);

        // verify
        verifyZeroInteractions(params, ctx);
    }

    @Test
    public void testTranslateAddRemoveCommandLine() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        Map<String, String> params = handler.translateCommandLine(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, Arrays.asList("T1,T2, T3"));
        assertTrue(params.keySet().contains("T1"));
        assertTrue(params.keySet().contains("T2"));
        assertTrue(params.keySet().contains("T3"));

        params = handler.translateCommandLine(AssetTagsSetHandler.ASSET_TAGS_SET_REMOVE, Arrays.asList("T1,T2, T3"));
        assertTrue(params.keySet().contains("T1"));
        assertTrue(params.keySet().contains("T2"));
        assertTrue(params.keySet().contains("T3"));
    }

    @Test
    public void testTranslateAddRemoveCommandLineWithEmptyParamList() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        Map<String, String> params = handler.translateCommandLine(AssetTagsSetHandler.ASSET_TAGS_SET_ADD, Collections.emptyList());
        assertTrue(params.isEmpty());

        params = handler.translateCommandLine(AssetTagsSetHandler.ASSET_TAGS_SET_REMOVE, Collections.emptyList());
        assertTrue(params.isEmpty());
    }

    @Test
    public void testTranslateListCommandLine() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        Map<String, String> params = handler.translateCommandLine(AssetTagsSetHandler.ASSET_TAGS_SET_LIST, Collections.emptyList());

        assertTrue(params.isEmpty());
    }

    @Test
    public void testTranslateNullCommandLine() {
        // define
        AssetTagsSetHandler handler = new AssetTagsSetHandler();

        // mock
        List<String> params = mock(List.class);

        // call
        handler.translateCommandLine(null, params);

        // verify
        verifyZeroInteractions(params);
    }

    @Test
    public void testGetCommandIDs() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        String act = Arrays.toString(handler.getCommandIDs());
        String exp = Arrays.toString(new String[]{AssetTagsSetHandler.ASSET_TAGS_SET_ADD,
                AssetTagsSetHandler.ASSET_TAGS_SET_REMOVE,
                AssetTagsSetHandler.ASSET_TAGS_SET_LIST});

        assertEquals(exp, act);
    }

    @Test
    public void testGetCommandHelp() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandHelp(cmd).length > 0);
        }

        assertTrue(handler.getCommandHelp(null).length == 0);
        assertTrue(handler.getCommandHelp("").length == 0);
    }

    @Test
    public void testGetCommandSyntaxHelp() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandSyntaxHelp(cmd).length > 0);
        }

        assertTrue(handler.getCommandSyntaxHelp(null).length == 0);
        assertTrue(handler.getCommandSyntaxHelp("").length == 0);
    }

    @Test
    public void testGetCommandWebSyntaxHelp() {
        AssetTagsSetHandler handler = new AssetTagsSetHandler();
        for (String cmd : handler.getCommandIDs()) {
            assertTrue(handler.getCommandWebSyntaxHelp(cmd).length > 0);
        }

        assertTrue(handler.getCommandWebSyntaxHelp(null).length == 0);
        assertTrue(handler.getCommandWebSyntaxHelp("").length == 0);
    }

    @Test
    public void testIsAdminRequired() {
        assertFalse(new AssetTagsSetHandler().isAdminRequired());
    }

    @Test
    public void testIsConfigReadRequired() {
        assertFalse(new AssetTagsSetHandler().isConfigReadRequired());
    }
}
