package com.rfcode.ranger.container;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by dy on 9/20/16.
 */
public class ConcurrentMapContainerTest {
    @Before
    public void setUp() {
        Containers.assetTags().removeAll();
    }

    @Test
    public void testPut() {
        Containers.assetTags().put("HI");
        Assert.assertTrue(Containers.assetTags().contains("HI"));
    }

    @Test
    public void testRemove() {
        Containers.assetTags().put("HI");
        Assert.assertTrue(Containers.assetTags().contains("HI"));
        Containers.assetTags().remove("HI");
        Assert.assertFalse(Containers.assetTags().contains("HI"));
    }

    @Test
    public void testRemoveAll() {
        Containers.assetTags().put("HI");
        Containers.assetTags().put("HI2");
        Assert.assertTrue(Containers.assetTags().contains("HI"));
        Assert.assertTrue(Containers.assetTags().contains("HI2"));
        Containers.assetTags().removeAll();
        Assert.assertEquals(0, Containers.assetTags().size());
    }

    @Test
    public void testContains() {
        Containers.assetTags().put("HI");
        Assert.assertTrue(Containers.assetTags().contains("HI"));
    }

    @Test
    public void testValues() {
        Containers.assetTags().put("HI");
        Assert.assertEquals("HI", Containers.assetTags().elements().iterator().next());
    }

    @Test
    public void testSize() {
        Containers.assetTags().put("HI");
        Assert.assertEquals("HI", Containers.assetTags().elements().iterator().next());
        Assert.assertEquals(1, Containers.assetTags().size());
    }
}
