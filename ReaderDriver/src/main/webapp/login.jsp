<%@ taglib prefix="jwr" uri="http://jawr.net/tags" %>
<%@ taglib prefix='c' uri='/WEB-INF/tld/c.tld' %>
<%@ page import="com.rfcode.ranger.RangerServer" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="brand/images/favicon.ico" />
    <jwr:style src="/bundles/login.css"/>
</head>

<body>
<div class="loginFormNested1">
    <div class="loginFormNested2">
        <div class="loginFormNested3">
            <div class="loginApplicationTitle">
<%= RangerServer.getProductName() %>
            </div>
            <form action="zonemgr/login" method="POST">
                <table align='center'>
                    <c:if test="${not empty param.login_error}">
                    <tr>
                        <td colspan="2">
                            <div class="loginError">Your login attempt was not successful. Please try again.</div>
                        </td>
                    </tr>
                    </c:if>

                    <tr>
                        <td>User:</td>
                        <td>
                            <input type="text" id="username" name="username" style="width: 150px;">
                        </td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td>
                            <input type="password" name="password" style="width: 150px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;">
                            <input type="submit" value="Login" />
                        </td>
                    </tr>
                </table>
            </form>
            <script language="JavaScript" type="text/javascript">
            document.getElementById('username').focus();
            </script>
        </div>
    </div>
</div>
<div class="loginBottomInfo">
    <div>
     <%= RangerServer.getCompanyName() %> <%= RangerServer.getProductName() %> -
     Version <%= RangerServer.getVersion() %>



    </div>
    <div>
        Copyright &copy; RF Code, Inc., 2008-2016
    </div>
    <div>
        <a target="_BLANK" href="<%= RangerServer.getCompanyURL() %>"><%= RangerServer.getCompanyURL() %></a>
    </div>
</div>
</body>
</html>
