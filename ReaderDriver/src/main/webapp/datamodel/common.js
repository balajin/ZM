[
    /* Common base tag attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagMotion"
        ,"$zName"      : "motion"
        ,"use"         : "status"
        ,"name"        : "Motion"
        ,"description" : "Indicates if the tag has detected that it is in motion."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTamper"
        ,"$zName"      : "tamper"
        ,"use"         : "status"
        ,"name"        : "Tamper"
        ,"description" : "Indicates if the tag has detected a tamper attempt."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTamperArmed"
        ,"$zName"      : "tamperArmed"
        ,"use"         : "status"
        ,"name"        : "Tamper Armed"
        ,"description" : "Indicates if the tag has a tamper tab installed."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLowBattery"
        ,"$zName"      : "lowbattery"
        ,"use"         : "status"
        ,"name"        : "Low Battery"
        ,"description" : "Indicates if the tag has detected a low battery condition."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPanic"
        ,"$zName"      : "panic"
        ,"use"         : "status"
        ,"name"        : "Panic"
        ,"description" : "Indicates if the panic switch on the tag has been activated."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagIRLocator"
        ,"$zName"      : "irlocator"
        ,"use"         : "status"
        ,"name"        : "IR Locator Code"
        ,"description" : "Indicates the ID code from any IR Locator currently visible to the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDoorOpen"
        ,"$zName"      : "dooropen"
        ,"use"         : "status"
        ,"name"        : "Door Open"
        ,"description" : "Indicates if the door switch on the tag is reporting a door opened condition."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
        ,"values"      : [ "Closed", "Open" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDryContactOpen"
        ,"$zName"      : "dryopen"
        ,"use"         : "status"
        ,"name"        : "Dry Contact Open"
        ,"description" : "Indicates if the dry contact input on the tag is reporting a dry contact opened condition."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
        ,"values"      : [ "Closed", "Open" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDryContact2Open"
        ,"$zName"      : "dryopen2"
        ,"use"         : "status"
        ,"name"        : "Dry Contact #2 Open"
        ,"description" : "Indicates if the second dry contact input on the tag is reporting a dry contact opened condition."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
        ,"values"      : [ "Closed", "Open" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDryContact3Open"
        ,"$zName"      : "dryopen3"
        ,"use"         : "status"
        ,"name"        : "Dry Contact #3 Open"
        ,"description" : "Indicates if the third dry contact input on the tag is reporting a dry contact opened condition."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
        ,"values"      : [ "Closed", "Open" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagUserPayload"
        ,"$zName"      : "userpayload"
        ,"use"         : "status"
        ,"name"        : "User Payload"
        ,"description" : "Reports the numeric payload for a user-payload tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagMotionCount"
        ,"$zName"      : "motioncount"
        ,"use"         : "status"
        ,"name"        : "Motion Count"
        ,"description" : "Reports the motion count payload reported by the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLockCount"
        ,"$zName"      : "lockcount"
        ,"use"         : "status"
        ,"name"        : "Lock Count"
        ,"description" : "Reports the lock count payload reported by the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLockOpen"
        ,"$zName"      : "lockopen"
        ,"use"         : "status"
        ,"name"        : "Lock Opened"
        ,"description" : "Reports the lock opened status reported by the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLockClosed"
        ,"$zName"      : "lockclosed"
        ,"use"         : "status"
        ,"name"        : "Lock Closed"
        ,"description" : "Reports the lock closed status reported by the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTemperature"
        ,"$zName"      : "temp"
        ,"use"         : "status"
        ,"name"        : "Temperature"
        ,"description" : "Reports the temperature at the tag, in degrees C."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "celsius"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDewPoint"
        ,"$zName"      : "dewpoint"
        ,"use"         : "status"
        ,"name"        : "Dew Point"
        ,"description" : "Reports the dew point at the tag, in degrees C."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "celsius"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPressure"
        ,"$zName"      : "pressure"
        ,"use"         : "status"
        ,"name"        : "Pressure"
        ,"description" : "Reports the pressure measured by the tag, in PSI."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "psi"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDiffPressure"
        ,"$zName"      : "diffpress"
        ,"use"         : "status"
        ,"name"        : "Differential Pressure"
        ,"description" : "Reports the difference between the two air pressures measured by the tag, in Pascals."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "pa"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagAvgDiffPressure"
        ,"$zName"      : "avgdiffpress"
        ,"use"         : "status"
        ,"name"        : "Averaged Differential Pressure"
        ,"description" : "Reports the time-averaged difference between the two air pressures measured by the tag, in Pascals."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "pa"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPressRounding"
        ,"$zName"      : "pressround"
        ,"use"         : "config"
        ,"name"        : "Pressure Increment"
        ,"description" : "Specifies the increment to be used for rounding pressure readings, in Pascals."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.0
            ,"max" : 10.0
        }
        ,"units"       : "pa"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagHumidity"
        ,"$zName"      : "humidity"
        ,"use"         : "status"
        ,"name"        : "Humidity"
        ,"description" : "Reports the humidity at the tag, in % relative humidity."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "rh"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagCurrentLoopReading"
        ,"$zName"      : "currentLoop"
        ,"use"         : "status"
        ,"name"        : "Current Loop Reading"
        ,"description" : "Reports the raw current loop reading, in millamps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "mamps"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagSensorVoltageReading"
        ,"$zName"      : "sensorVoltage"
        ,"use"         : "status"
        ,"name"        : "Sensor Voltage Reading"
        ,"description" : "Reports the raw sensor voltage reading, in volts."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "volts"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagSensorLowBattery"
        ,"$zName"      : "sensorlowbatt"
        ,"use"         : "status"
        ,"name"        : "Low Sensor Battery"
        ,"description" : "Indicates if the tag has detected a low battery condition for the attached sensor."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFluid"
        ,"$zName"      : "fluid"
        ,"use"         : "status"
        ,"name"        : "Fluid Detected"
        ,"description" : "Indicates if the tag has detected fluid using the attached sensor."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
        ,"values"      : [ "Dry", "Wet" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagSensorDisconnect"
        ,"$zName"      : "disconnect"
        ,"use"         : "status"
        ,"name"        : "Sensor Disconnected"
        ,"description" : "Indicates if the tag has detected a connection problem with the attached sensor."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagParentTag"
        ,"$zName"      : "parenttag"
        ,"use"         : "status"
        ,"name"        : "Parent Tag"
        ,"description" : "Identity of the parent tag of the subtag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$zTag"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagSubIndex"
        ,"$zName"      : "subindex"
        ,"use"         : "status"
        ,"name"        : "Subtag Index"
        ,"description" : "Identity of the subtag, under parent tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
         "class"       : "attribute_type"
        ,"guid"        : "$zTagHasChildren"
        ,"use"         : "hidden"
        ,"name"        : "Has Children"
        ,"description" : "Indicates if the tag has child tags."
        ,"history"     : false
        ,"deletable"   : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagBrkAmps"
        ,"$zName"      : "brkAmps"
        ,"use"         : "status"
        ,"name"        : "Breaker Amperage"
        ,"description" : "Average amperage measured on breaker, in amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "amps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagBrkTripped"
        ,"$zName"      : "brkTripped"
        ,"use"         : "status"
        ,"name"        : "Breaker Tripped"
        ,"description" : "Indicates if the breaker is tripped (open)."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagBrkOverload"
        ,"$zName"      : "brkOverload"
        ,"use"         : "status"
        ,"name"        : "Bank Overload"
        ,"description" : "Indicates if the bank is overloaded."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagBrkCfg"
        ,"$zName"      : "brkConfig"
        ,"use"         : "status"
        ,"name"        : "Breaker Configuration"
        ,"description" : "Breaker configuration, indicating phase associated with breaker."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "N/A", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagBrkBankID"
        ,"$zName"      : "brkBank"
        ,"use"         : "status"
        ,"name"        : "Bank ID"
        ,"description" : "Identity of the bank for the breaker."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletVolts"
        ,"$zName"      : "outVolts"
        ,"use"         : "status"
        ,"name"        : "Outlet Voltage"
        ,"description" : "Average voltage measured on outlet, in volts AC."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "vac"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletActivePwr"
        ,"$zName"      : "outTruePower"
        ,"use"         : "status"
        ,"name"        : "Outlet Active Power"
        ,"description" : "Average active (true) power measured on outlet, in watts."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watts"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletPwrFactor"
        ,"$zName"      : "outPwrFactor"
        ,"use"         : "status"
        ,"name"        : "Outlet Power Factor"
        ,"description" : "Average power factor measured on outlet, in percent."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "percent"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletAppPwr"
        ,"$zName"      : "outAppPower"
        ,"use"         : "status"
        ,"name"        : "Outlet Apparent Power"
        ,"description" : "Average apparent power measured on outlet, in volt-amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletAmps"
        ,"$zName"      : "outAmps"
        ,"use"         : "status"
        ,"name"        : "Outlet Amperage"
        ,"description" : "Average amperage measured on outlet, in amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "amps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletBankID"
        ,"$zName"      : "outBank"
        ,"use"         : "status"
        ,"name"        : "Outlet Bank ID"
        ,"description" : "Identity of the bank associated with the outlet"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletCfg"
        ,"$zName"      : "outConfig"
        ,"use"         : "status"
        ,"name"        : "Outlet Configuration"
        ,"description" : "Outlet configuration, indicating source lines used for outlet."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "N/A", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletTotWH"
        ,"$zName"      : "outTotalWH"
        ,"use"         : "status"
        ,"name"        : "Outlet Total Active Power Used"
        ,"description" : "Cumulative total active power consumed since start time, in watt-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watthours"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletTotVAH"
        ,"$zName"      : "outTotalVAH"
        ,"use"         : "status"
        ,"name"        : "Outlet Total Apparent Power Used"
        ,"description" : "Cumulative total apparent power consumed since start time, in volt-amp-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamphours"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletTotPwrTS"
        ,"$zName"      : "outTotalPwrTS"
        ,"use"         : "status"
        ,"name"        : "Outlet Total Power Start Time"
        ,"description" : "Start time-stamp for cumulative power usage, as UTC seconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletLabel"
        ,"$zName"      : "outLabel"
        ,"use"         : "status"
        ,"name"        : "Outlet Label"
        ,"description" : "Default label for outlet."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagOutletSwitch"
        ,"$zName"      : "outSwitch"
        ,"use"         : "status"
        ,"name"        : "Switch State"
        ,"description" : "If outlet is switched, indicates if outlet is on (true) or off (false)."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseVoltsN"
        ,"$zName"      : "phVoltsN"
        ,"use"         : "status"
        ,"name"        : "Phase Voltage (L-N)"
        ,"description" : "Average voltage measured on phase versus neutral, in volts AC."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "vac"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseVoltsP"
        ,"$zName"      : "phVoltsP"
        ,"use"         : "status"
        ,"name"        : "Phase Voltage (L-L)"
        ,"description" : "Average voltage measured on phase versus next phase, in volts AC."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "vac"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseActivePwr"
        ,"$zName"      : "phTruePower"
        ,"use"         : "status"
        ,"name"        : "Phase Active Power"
        ,"description" : "Average active (true) power measured on phase, in watts."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watts"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhasePwrFactor"
        ,"$zName"      : "phPwrFactor"
        ,"use"         : "status"
        ,"name"        : "Phase Power Factor"
        ,"description" : "Average power factor measured on phase, in percent."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "percent"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseAppPwr"
        ,"$zName"      : "phAppPower"
        ,"use"         : "status"
        ,"name"        : "Phase Apparent Power"
        ,"description" : "Average apparent power measured on phase, in volt-amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseAmps"
        ,"$zName"      : "phAmps"
        ,"use"         : "status"
        ,"name"        : "Phase Amperage"
        ,"description" : "Average amperage measured on phase, in amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "amps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseTotPwrTS"
        ,"$zName"      : "phTotalPwrTS"
        ,"use"         : "status"
        ,"name"        : "Phase Total Power Start Time"
        ,"description" : "Start time-stamp for cumulative power usage, in UTC time."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseTotWH"
        ,"$zName"      : "phTotalWH"
        ,"use"         : "status"
        ,"name"        : "Phase Total Active Power Used"
        ,"description" : "Cumulative total active power consumed since start time, in watt-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watthours"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseTotVAH"
        ,"$zName"      : "phTotalVAH"
        ,"use"         : "status"
        ,"name"        : "Phase Total Apparent Power Used"
        ,"description" : "Cumulative total apparent power consumed since start time, in volt-amp-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamphours"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPhaseCfg"
        ,"$zName"      : "phConfig"
        ,"use"         : "status"
        ,"name"        : "Phase Configuration"
        ,"description" : "Phase configuration, indicating source lines used for outlet."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "N/A", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFeedAmps"
        ,"$zName"      : "feedAmps"
        ,"use"         : "status"
        ,"name"        : "Line Amperage"
        ,"description" : "Average amperage measured on feed line, in amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "amps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFeedCfg"
        ,"$zName"      : "feedConfig"
        ,"use"         : "status"
        ,"name"        : "Feed Line ID"
        ,"description" : "Feed line identity."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "N/A", "L1", "L2", "L3", "N" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFeedOverload"
        ,"$zName"      : "feedOverload"
        ,"use"         : "status"
        ,"name"        : "Line Overload"
        ,"description" : "Indicates if the feed line is overloaded."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFeedLoadWarning"
        ,"$zName"      : "feedLoadWarning"
        ,"use"         : "status"
        ,"name"        : "Load Warning"
        ,"description" : "Indicates if the feed line is close to overload (above 80% rated capacity)."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTowerID"
        ,"$zName"      : "towerID"
        ,"use"         : "status"
        ,"name"        : "Tower ID"
        ,"description" : "For multi-tower PDUs, this identifies the tower associated with the object."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagFeedSetID"
        ,"$zName"      : "feedSetID"
        ,"use"         : "status"
        ,"name"        : "Feed Line Set ID"
        ,"description" : "For PDUs with multiple feed line sets, this identifies the feed line set associated with the object."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagChannelAmps"
        ,"$zName"      : "channelAmps"
        ,"use"         : "status"
        ,"name"        : "Channel Amperage"
        ,"description" : "Average amperage measured on channel, in amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "amps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUActivePwr"
        ,"$zName"      : "pduTruePower"
        ,"use"         : "status"
        ,"name"        : "PDU Active Power"
        ,"description" : "Average active (true) power measured for PDU, in watts."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watts"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUAppPwr"
        ,"$zName"      : "pduAppPower"
        ,"use"         : "status"
        ,"name"        : "PDU Apparent Power"
        ,"description" : "Average apparent power measured for PDU, in volt-amps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamps"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUPwrFactor"
        ,"$zName"      : "pduPwrFactor"
        ,"use"         : "status"
        ,"name"        : "PDU Power Factor"
        ,"description" : "Average power factor for PDU, in percent."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "percent"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUTotPwrTS"
        ,"$zName"      : "pduTotalPwrTS"
        ,"use"         : "status"
        ,"name"        : "PDU Total Power Start Time"
        ,"description" : "Start time-stamp for cumulative power usage, in UTC time."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUTotWH"
        ,"$zName"      : "pduTotalWH"
        ,"use"         : "status"
        ,"name"        : "PDU Total Active Power Used"
        ,"description" : "Cumulative total active power consumed since start time, in watt-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "watthours"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUTotVAH"
        ,"$zName"      : "pduTotalVAH"
        ,"use"         : "status"
        ,"name"        : "PDU Total Apparent Power Used"
        ,"description" : "Cumulative total apparent power consumed since start time, in volt-amp-hours."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "voltamphours"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUDisconnect"
        ,"$zName"      : "pduDisconnect"
        ,"use"         : "status"
        ,"name"        : "PDU Disconnected"
        ,"description" : "Indicates if the tag has detected a connection problem with the attached PDU."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUUpgradeNeeded"
        ,"$zName"      : "pduFWUpgradeNeeded"
        ,"use"         : "status"
        ,"name"        : "Firmware Upgrade Needed"
        ,"description" : "Indicates that the PDU firmware is not compatable with one or more installed modules, and requires upgrade."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUTowerDisconnect"
        ,"$zName"      : "pduTowerDisconnect"
        ,"use"         : "status"
        ,"name"        : "PDU Tower Disconnected"
        ,"description" : "Indicates if the tag has detected one or more disconnected PDU towers."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUDisconnectedTowers"
        ,"$zName"      : "pduDisconnectedTowers"
        ,"use"         : "status"
        ,"name"        : "PDU Disconnected Towers"
        ,"description" : "List of IDs of PDU towers that have been reported as disconnected."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUTowerCount"
        ,"$zName"      : "pduTowerCount"
        ,"use"         : "status"
        ,"name"        : "PDU Tower Count"
        ,"description" : "Number of towers connected to PDU controller."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUModel"
        ,"$zName"      : "pduModel"
        ,"use"         : "status"
        ,"name"        : "PDU Model"
        ,"description" : "Model identifier for PDU."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUModelList"
        ,"$zName"      : "pduModelList"
        ,"use"         : "status"
        ,"name"        : "PDU Tower Models"
        ,"description" : "Model identifier for PDU towers."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUSerialNum"
        ,"$zName"      : "pduSerial"
        ,"use"         : "status"
        ,"name"        : "PDU Serial Number"
        ,"description" : "Serial number for PDU."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUSerialNumList"
        ,"$zName"      : "pduSerialList"
        ,"use"         : "status"
        ,"name"        : "PDU Tower Serial Numbers"
        ,"description" : "Serial number for PDU towers."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagPDUModelMatch"
        ,"$zName"      : "pduModelMatch"
        ,"use"         : "status"
        ,"name"        : "PDU Model Recognized"
        ,"description" : "Indicates if the model reported by the PDU is recognized by the system.  An unrecognized PDU may not have all data properly reported."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagDefLabel"
        ,"$zName"      : "defLabel"
        ,"use"         : "status"
        ,"name"        : "Default Label"
        ,"description" : "Default label for tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagMsgLossRate"
        ,"$zName"      : "msgLoss"
        ,"use"         : "status"
        ,"name"        : "Message Loss Rate"
        ,"description" : "Estimated message loss rate, in percent."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"       : "percent"
        ,"printf"      : "%.0f"
    }
    ,
    {
        /* this is a string attribute instead of a type ref since we only care
         * about the location on the asset. When we transfer this attribute to an
         * asset it gets converted to a typeref */
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLocation"
        ,"$zName"      : "locationzone"
        ,"use"         : "info"
        ,"name"        : "Tag Location"
        ,"description" : "Indicates the identity of the location computed for the tag, if any."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGPSID"
        ,"$zName"      : "gpsid"
        ,"use"         : "info"
        ,"name"        : "Tag GPS ID"
        ,"description" : "Indicates the identity of the GPS closest to the tag's location, if any."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$tGPS"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastGPSID"
        ,"$zName"      : "lastgpsid"
        ,"use"         : "info"
        ,"name"        : "Last Tag GPS ID"
        ,"description" : "Indicates the identity of the last GPS closest to the tag's location, if any."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$tGPS"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastGPSTS"
        ,"$zName"      : "lastgpsts"
        ,"use"         : "info"
        ,"name"        : "Last GPS Timestamp"
        ,"description" : "Time-stamp for last known GPS position."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTagGroup"
        ,"$zName"      : "taggroup"
        ,"use"         : "info"
        ,"name"        : "Tag Group"
        ,"description" : "Tag group of the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$tTagGroup"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor1"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 1"
        ,"use"         : "status"
        ,"$zName"      : "usersensor1"
        ,"description" : "Custom Sensor Attribute 1"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor2"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 2"
        ,"use"         : "status"
        ,"$zName"      : "usersensor2"
        ,"description" : "Custom Sensor Attribute 1"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor3"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 3"
        ,"use"         : "status"
        ,"$zName"      : "usersensor3"
        ,"description" : "Custom Sensor Attribute 3"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor4"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 4"
        ,"use"         : "status"
        ,"$zName"      : "usersensor4"
        ,"description" : "Custom Sensor Attribute 4"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor5"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 5"
        ,"use"         : "status"
        ,"$zName"      : "usersensor5"
        ,"description" : "Custom Sensor Attribute 5"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor6"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 6"
        ,"use"         : "status"
        ,"$zName"      : "usersensor6"
        ,"description" : "Custom Sensor Attribute 6"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor7"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 7"
        ,"use"         : "status"
        ,"$zName"      : "usersensor7"
        ,"description" : "Custom Sensor Attribute 7"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor8"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 8"
        ,"use"         : "status"
        ,"$zName"      : "usersensor8"
        ,"description" : "Custom Sensor Attribute 8"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor9"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 9"
        ,"use"         : "status"
        ,"$zName"      : "usersensor9"
        ,"description" : "Custom Sensor Attribute 9"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zUserSensor10"
        ,"type"        : "double"
        ,"deletable"   : false
        ,"history"     : false
        ,"name"        : "ZM User Sensor 10"
        ,"use"         : "status"
        ,"$zName"      : "usersensor10"
        ,"description" : "Custom Sensor Attribute 10"
    }
    ,
    /* Common base tag link attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLinkSSI"
        ,"$zName"      : "ssi"
        ,"use"         : "status"
        ,"name"        : "Signal Strength (SSI)"
        ,"description" : "Signal strength measured for tag messages by receiving radio."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "double"
        ,"units"       : "dbm"
    }
    ,
    /* Common base reader attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderType"
        ,"$zName"      : "type"
        ,"use"         : "info"
        ,"name"        : "Reader Type"
        ,"description" : "Reader type of the reader"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderState"
        ,"$zName"      : "state"
        ,"use"         : "status"
        ,"name"        : "Reader State"
        ,"description" : "Current state of reader."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "DISABLED", "CONNECTING", "INITIALIZING", "INITIALIZED", "ACTIVE", "DISCONNECTING", "DISCONNECTED", "READERFAILURE", "CONFIGFAILURE", "CONNECTFAILURE", "NOISEDETECTED", "UNKNOWN", "ACCESSDENIED", "HIGHTRAFFIC", "FIRMWAREUPGRADING" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderRemoteAddr"
        ,"$zName"      : "remoteaddr"
        ,"use"         : "status"
        ,"name"        : "Connected Address"
        ,"description" : "Remote IP address of current connection to/from reader."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGroups"
        ,"$zName"      : "groups"
        ,"use"         : "hidden"
        ,"name"        : "Tag Groups"
        ,"description" : "Set of tag groups to be monitored by reader."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"constraints" : {
            "typeref"  : "$tTagGroup"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSupportedTagTypes"
        ,"use"         : "info"
        ,"name"        : "Supported Tag Types"
        ,"description" : "Tag types supported by reader"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref-list"
        ,"constraints" : {
            "typeref"  : "$tTagGroup"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSupportedTagGroups"
        ,"use"         : "info"
        ,"name"        : "Supported Tag Groups"
        ,"description" : "Tag groups supported by reader"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderTagLinkType"
        ,"use"         : "info"
        ,"name"        : "Tag Link Type"
        ,"description" : "Type of tag link reported by the reader."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tTagLink"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderAgeInCount"
        ,"$zName"      : "ageincount"
        ,"use"         : "config"
        ,"name"        : "Tag Age-In Count"
        ,"description" : "Number of messages that must be received from a tag before the tag is reported."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"constraints" : {
            "min"  : 0
            ,"max" : 255
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderAgeOutTimeA"
        ,"$zName"      : "ageouttimeA"
        ,"use"         : "config"
        ,"name"        : "Tag Age-Out Time (Channel A)"
        ,"description" : "Amount of time a tag is unobserved by channel A, but seen by other channels, before its considered lost to channel A."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 1
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderAgeOutTimeB"
        ,"$zName"      : "ageouttimeB"
        ,"use"         : "config"
        ,"name"        : "Tag Age-Out Time (Channel B)"
        ,"description" : "Amount of time a tag is unobserved by channel B, but seen by other channels, before its considered lost to channel B."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 1
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSSICutoffA"
        ,"$zName"      : "ssicutoffA"
        ,"use"         : "config"
        ,"name"        : "SSI Cutoff (Channel A)"
        ,"description" : "Minimum signal strength for tag messages received from channel A."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSSICutoffB"
        ,"$zName"      : "ssicutoffB"
        ,"use"         : "config"
        ,"name"        : "SSI Cutoff (Channel B)"
        ,"description" : "Minimum signal strength for tag messages received from channel B."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderChannelBiasA"
        ,"$zName"      : "channelbiasA"
        ,"use"         : "config"
        ,"name"        : "Channel Bias (Channel A)"
        ,"description" : "Offset to add to reported SSI values for channel A - used to compensate for antenna and radio variability."
        ,"deleteable"  : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
        ,"constraints" : {
            "min"  : -100
            ,"max" : 100
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderChannelBiasB"
        ,"$zName"      : "channelbiasB"
        ,"use"         : "config"
        ,"name"        : "Channel Bias (Channel B)"
        ,"description" : "Offset to add to reported SSI values for channel B - used to compensate for antenna and radio variability."
        ,"deleteable"  : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
        ,"constraints" : {
            "min"  : -100
            ,"max" : 100
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderUserID"
        ,"$zName"      : "userid"
        ,"use"         : "config"
        ,"name"        : "User ID"
        ,"description" : "User ID used to authenticate communications to reader."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPassword"
        ,"$zName"      : "password"
        ,"use"         : "config"
        ,"name"        : "Password"
        ,"description" : "Password used to authenticate communications to reader."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "password"
        ,"encoding"    : "Blowfish"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderOfflineLinkAgeOut"
        ,"$zName"      : "offlinelinkageout"
        ,"use"         : "config"
        ,"name"        : "Tag Age-Out Time (Reader Offline)"
        ,"description" : "Amount of time reader can be offline before its observed tags are assumed lost."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderTagTimeout"
        ,"$zName"      : "tagtimeout"
        ,"use"         : "config"
        ,"name"        : "Tag Age-Out Time"
        ,"description" : "The amount of time a tag is unobserved by all channels of the reader before it is considered lost to all channels."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min"  : 10
            ,"max" : 32767
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSSIDeltaThresh"
        ,"$zName"      : "ssideltathresh"
        ,"use"         : "config"
        ,"name"        : "SSI Change Threshold"
        ,"description" : "Size of change in measured SSI since the last reported value that is considered significant enough to report as a new value."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
        ,"constraints" : {
            "min"  : 0
            ,"max" : 100
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderChannels"
        ,"use"         : "info"
        ,"name"        : "Reader Channels"
        ,"description" : "The channels representing the reader's distinct radios."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"constraints" : {
            "typeref"  : "$tReaderChannel"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderNoiseThreshold"
        ,"$zName"      : "noisethresh"
        ,"use"         : "config"
        ,"name"        : "Noise Threshold"
        ,"description" : "Maximum allowed level for noise floor, in dBm."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderNoiseA"
        ,"$zName"      : "noiseA"
        ,"use"         : "status"
        ,"name"        : "Noise Floor (Channel A)"
        ,"description" : "Current measured noise floor for channel A, in dBm."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderNoiseB"
        ,"$zName"      : "noiseB"
        ,"use"         : "status"
        ,"name"        : "Noise Floor (Channel B)"
        ,"description" : "Current measured noise floor for channel B, in dBm."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderStartupTS"
        ,"$zName"      : "startuptime"
        ,"use"         : "status"
        ,"name"        : "Startup Timestamp"
        ,"description" : "Time-stamp of last reader boot (as reported by reader's clock), in UTC milliseconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderEncryptedConnection"
        ,"$zName"      : "encrypted"
        ,"use"         : "status"
        ,"name"        : "Connection Encrypted"
        ,"description" : "If true, communications with the reader are encrypted."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
         "class"        : "attribute_type"
         ,"guid"        : "$zReaderReportTriggers"
         ,"$zName"      : "reporttriggers"
         ,"use"         : "config"
         ,"name"        : "Report Tag Controller Events"
         ,"description" : "If enabled, reader will report tag controller events, such as those caused by the A600 Tag Controller."
         ,"deletable"   : false
         ,"history"     : false
         ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderJoinChannels"
        ,"$zName"      : "joinchannels"
        ,"use"         : "config"
        ,"name"        : "Join Reader Channels"
        ,"description" : "If enabled, reader will combine both channels into a single channel, which can improve noise rejection and range if both channels are observing the same tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderMergeChannels"
        ,"$zName"      : "mergechannels"
        ,"use"         : "config"
        ,"name"        : "Merge Reader Channels"
        ,"description" : "If enabled, reader will use the strongest tag messages from either channel and report them on both channels."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderIgnoreLowConfInitialBeacons"
        ,"$zName"      : "ignorelowconfinit"
        ,"use"         : "config"
        ,"name"        : "Ignore Low Confidence Initial Beacons"
        ,"description" : "If enabled, reader will ignore potentially invalid low confidence initial beacons."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderFirmwareVersion"
        ,"$zName"      : "fwversion"
        ,"use"         : "status"
        ,"name"        : "Reader Firmware Version"
        ,"description" : "Current firmware version of reader."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderFirmwareFamily"
        ,"$zName"      : "fwfamily"
        ,"use"         : "status"
        ,"name"        : "Reader Firmware Family"
        ,"description" : "Current firmware family of reader."
        ,"deletable"   : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderUpConnectEnabled"
        ,"$zName"      : "upconnenabled"
        ,"use"         : "config"
        ,"name"        : "Up Connection Enabled"
        ,"description" : "If enabled, reader is permitted to up connect to server."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderUpConnectReaderID"
        ,"$zName"      : "upconnrdrid"
        ,"use"         : "config"
        ,"name"        : "Up Connection Reader ID"
        ,"description" : "Reader ID used by the reader when up connecting to server."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"constraints" : {
            "value_unique" : true
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderUpConnectPassword"
        ,"$zName"      : "upconnpwd"
        ,"use"         : "config"
        ,"name"        : "Up Connection Password"
        ,"description" : "Password used by the reader to authenticate when up connecting to server."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "password"
        ,"encoding"    : "Blowfish"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPositionSource"
        ,"$zName"      : "positionsrc"
        ,"use"         : "config"
        ,"name"        : "Position Data Source"
        ,"description" : "Allows reader to provide position data, either statically or via an attached GPS"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"values"      : [ "NONE", "GPS", "STATIC" ]

    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSMinSSI"
        ,"$zName"      : "gpsminssi"
        ,"use"         : "config"
        ,"name"        : "Minimum Tag SSI for Position Match"
        ,"description" : "Minimum SSI required for a tag to be correlated with the reader as a position source, in dBm. 0=no minimum"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
        ,"constraints" : {
            "max" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSPositionCoord"
        ,"$zName"      : "staticlatlon"
        ,"use"         : "config"
        ,"name"        : "Reader Position"
        ,"description" : "Static coordinates (latitude, longitude) of the reader"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "lat-lon-coord"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSData"
        ,"$zName"      : "gpsdata"
        ,"use"         : "config"
        ,"name"        : "GPS Data Reporting"
        ,"description" : "Controls type of GPS data reported: lat-lon=latitude/longitude, alt=altitude, spd=speed, epe=estimated positional error, use-global=use global defaults"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"values"      : [ "use-global",
             "lat-lon", "lat-lon-epe", "lat-lon-spd", "lat-lon-spd-epe",
             "lat-lon-alt", "lat-lon-alt-epe", "lat-lon-alt-spd", "lat-lon-alt-spd-epe" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSMinPeriod"
        ,"$zName"      : "gpsminperiod"
        ,"use"         : "config"
        ,"name"        : "Minimum GPS Update Period"
        ,"description" : "Minimum time between GPS position updates, in seconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSMinHoriz"
        ,"$zName"      : "gpsminhoriz"
        ,"use"         : "config"
        ,"name"        : "Minimum Horizontal Change"
        ,"description" : "Minimum horizontal position change of interest."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"        : "meters"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderGPSMinVert"
        ,"$zName"      : "gpsminvert"
        ,"use"         : "config"
        ,"name"        : "Minimum Vertical Change"
        ,"description" : "Minimum altitude change of interest."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"units"        : "meters"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSStatus"
        ,"$zName"      : "gpsfix"
        ,"use"         : "status"
        ,"name"        : "GPS Status"
        ,"description" : "Current status of GPS."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "NO_GPS", "NO_FIX", "FIX_2D", "FIX_3D" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPartIndex"
        ,"$zName"      : "partindex"
        ,"use"         : "config"
        ,"name"        : "Reader Partition Index"
        ,"description" : "Partition index of the reader, when partition is active.."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"constraints" : {
            "min"  : 0
            ,"max" : 31
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPartCount"
        ,"$zName"      : "partcount"
        ,"use"         : "config"
        ,"name"        : "Reader Partition Count"
        ,"description" : "Partition count.  If above 1, reader will only see tags whose ID divided by this count has a remainder matching the partition index."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"constraints" : {
            "min"  : 0
            ,"max" : 32
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPartRotateTime"
        ,"$zName"      : "partrotperiod"
        ,"use"         : "config"
        ,"name"        : "Reader Partition Rotation Time"
        ,"description" : "If greater that zero, this setting determines the number of seconds between automatic stepping of the partition index."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderEvtPerSecA"
        ,"$zName"      : "evtpersecA"
        ,"use"         : "status"
        ,"name"        : "Event Rate (Channel A)"
        ,"description" : "Estimated raw tag event rate observed by channel A, in events per second."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderEvtPerSecB"
        ,"$zName"      : "evtpersecB"
        ,"use"         : "status"
        ,"name"        : "Event Rate (Channel B)"
        ,"description" : "Estimated raw tag event rate observed by channel B, in events per second."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderEvtPerSecThreshold"
        ,"$zName"      : "evtpersecthresh"
        ,"use"         : "config"
        ,"name"        : "Tag Event Rate Threshold"
        ,"description" : "Maximum allowed raw tag events per second - disabled if set to zero."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"constraints" : {
           "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderTagCapacityUsed"
        ,"$zName"      : "tagcapused"
        ,"use"         : "status"
        ,"name"        : "Tag Capacity Used (%)"
        ,"description" : "Portion of reader's tag capacity used (0-100%)."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"constraints" : {
            "format" : [
                {
                    "fgcolor"   : "#B90000"
                    ,"bgcolor"  : ""
                    ,"operator" : "ge"
                    ,"value"    : "90"
                }
            ]
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSerialBaud"
        ,"$zName"      : "serialbaud"
        ,"use"         : "config"
        ,"name"        : "Serial Baud Rate"
        ,"description" : "Baud rate for the serial port driver in bits per second."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSerialDriver"
        ,"$zName"      : "serialdriver"
        ,"use"         : "config"
        ,"name"        : "Serial Driver"
        ,"description" : "Serial driver for the reader's RS-232 interface."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"values"      : [ "bridge", "text-8n1", "text-7e1", "text-7o1" ]
    }
    ,
    /* Common reader channel attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderChannelID"
        ,"$zName"      : "rel-id"
        ,"use"         : "info"
        ,"name"        : "Channel ID"
        ,"description" : "ID of channel, relative to reader."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderChannelLabel"
        ,"$zName"      : "label"
        ,"use"         : "info"
        ,"name"        : "Channel label"
        ,"description" : "Label for channel."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPFIMask"
        ,"$zName"      : "pfimask"
        ,"use"         : "config"
        ,"name"        : "Fault In Mask"
        ,"description" : "See Reader Engineering Specification"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPFIValue"
        ,"$zName"      : "pfivalue"
        ,"use"         : "config"
        ,"name"        : "Fault In Value"
        ,"description" : "See Reader Engineering Specification"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderPCHGIGNMask"
        ,"$zName"      : "pchgignmask"
        ,"use"         : "config"
        ,"name"        : "Change Ignore Mask"
        ,"description" : "See Reader Engineering Specification"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSSLMode"
        ,"$zName"      : "sslmode"
        ,"use"         : "config"
        ,"name"        : "SSL Mode"
        ,"description" : "Controls use of SSL on reader communications"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"values"      : [ "OFF", "IFAVAIL", "REQUIRED", "STRICT" ]
    }
    ,
    /* SPM reader specific attribs */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSPMCDUPeriod"
        ,"$zName"      : "cduperiod"
        ,"use"         : "config"
        ,"name"        : "CDU Refresh Period"
        ,"description" : "Minimum time between refreshes of CDU data, in seconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
           "min" : 5
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zReaderSPMCatalogPeriod"
        ,"$zName"      : "catalogperiod"
        ,"use"         : "config"
        ,"name"        : "CDU List Refresh Period"
        ,"description" : "Minimum time between refreshes of CDU population, in seconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
           "min" : 5
        }
    }
    ,
    /* Common base location rule attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleType"
        ,"$zName"      : "type"
        ,"use"         : "info"
        ,"name"        : "Rule Type"
        ,"description" : "Type of the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleChannelList"
        ,"$zName"      : "channellist"
        ,"use"         : "config"
        ,"name"        : "Reader Channel List"
        ,"description" : "List of reader channels that should be considered as contributing to the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"subtype"     : "channel-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleInsideChannelList"
        ,"$zName"      : "insidechannellist"
        ,"use"         : "config"
        ,"name"        : "Reader Channel List (Inside)"
        ,"description" : "List of reader channels that should be considered as representing the 'inside' (matching) of a location for the location rule."
        ,"deleteable"  : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"subtype"     : "channel-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleOutsideChannelList"
        ,"$zName"      : "outsidechannellist"
        ,"use"         : "config"
        ,"name"        : "Reader Channel List (Outside)"
        ,"description" : "List of reader channels that should be considered as representing the 'outside' (not matching) of a location for the location rule."
        ,"deleteable"  : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"subtype"     : "channel-list"
    }
    ,
    {
        "class"               : "attribute_type"
        ,"guid"               : "$zRuleTargetLocation"
        ,"$zName"             : "locid"
        ,"use"                : "config"
        ,"name"               : "Location"
        ,"description"        : "Location implied when the rule matches on a given tag."
        ,"deletable"          : false
        ,"history"            : true
        ,"inherit_attributes" : false
        ,"type"               : "typeref"
        ,"constraints"        : {
            "typeref"         : "$tLocation"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleSSIHighConf"
        ,"$zName"      : "ssihighconf"
        ,"use"         : "config"
        ,"name"        : "SSI Threshold (High Confidence)"
        ,"description" : "Signal strength level that indicates a high level of matching confidence (0.9) for the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleOffSSIMinimum"
        ,"$zName"      : "offssiminimum"
        ,"use"         : "config"
        ,"name"        : "Threshold Offset From Ref SSI (Minimum)"
        ,"description" : "Offset, added to the reference tag SSI, that indicates a signal strength corresponding to a minimum matching confidence (0.0) for the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "db"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleOffSSIHighConf"
        ,"$zName"      : "offssihighconf"
        ,"use"         : "config"
        ,"name"        : "Threshold Offset From Ref SSI (High Confidence)"
        ,"description" : "Offset, added to the reference tag SSI, that indicates a signal strength correspoding to a high level of matching confidence (0.9) for the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "db"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleRefSSIMinimum"
        ,"$zName"      : "refminimum"
        ,"use"         : "config"
        ,"name"        : "Reference SSI Threshold (Minimum)"
        ,"description" : "Minimum signal strength level for reference tags, below which no tags will match."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleSSIMinimum"
        ,"$zName"      : "ssiminimum"
        ,"use"         : "config"
        ,"name"        : "SSI Threshold (Minimum)"
        ,"description" : "Signal strength level that indicates a minimum level of matching confidence (0.0) for the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleSSIChanMinimum"
        ,"$zName"      : "ssichanminimum"
        ,"use"         : "config"
        ,"name"        : "SSI Threshold (Per-Channel Minimum)"
        ,"description" : "The minimum SSI strength that any one channel can report."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleIRLocatorCode"
        ,"$zName"      : "irlocatorcode"
        ,"use"         : "config"
        ,"name"        : "Matching IR Locator"
        ,"description" : "ID of the IR Locator that should be reported in order to match the rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleIRNotFoundTimeout"
        ,"$zName"      : "irnotfoundtimeout"
        ,"use"         : "config"
        ,"name"        : "IR Locator Timeout"
        ,"description" : "Amount of time after last report of a valid IR locator code before the tag is assumed to have moved away from the locator."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleInsideUnreportedConf"
        ,"$zName"      : "insideunreportedconf"
        ,"use"         : "config"
        ,"name"        : "Assumed Matching Confidence"
        ,"description" : "Confidence reported by rule when tag is assumed to be matching due to having last been reported as matching."
        ,"deleteable"  : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.0
            ,"max" : 0.5
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleRefTagList"
        ,"$zName"      : "reftags"
        ,"use"         : "config"
        ,"name"        : "Reference Tag List"
        ,"description" : "List of tag IDs for tags mounted at the target location for the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
        ,"subtype"     : "tag-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleRefTagSSIMinimum"
        ,"$zName"      : "reftagssiminimum"
        ,"use"         : "config"
        ,"name"        : "Reference Tag SSI (Minimum)"
        ,"description" : "Minimum signal strength level needed in order for a reader channel to match a reference tag for the location rule, indicating that it is close enough to contribute other matching tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "dbm"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleRefTagMatchingTime"
        ,"$zName"      : "reftagmatchtime"
        ,"use"         : "config"
        ,"name"        : "Reference Tag Matching Time"
        ,"description" : "Controls the amount of time, in seconds, that a reference tag needs to be observed, above the minimum signal strength, before additional tags are considered for matching the location rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleMatchingConf"
        ,"$zName"      : "matchingconf"
        ,"use"         : "config"
        ,"name"        : "Confidence When Match"
        ,"description" : "Indicates the confidence used when rule's conditions are satisfied."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min" : 0.0
            ,"max" : 1.0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleCoordList"
        ,"$zName"      : "coordlist"
        ,"use"         : "config"
        ,"name"        : "Coordinate List"
        ,"description" : "List of GPS coordinates (latitude, longitide in degreees) used for matching rule.  If one, matches if within 100 meters; if two, matches if within corresponding rectangle; 3 or more, matches if within polygon bounded by the provided points."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "lat-lon-coord-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleExceptDescription"
        ,"use"         : "status"
        ,"name"        : "Conflict description"
        ,"description" : "Description of the conflict"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleExceptType"
        ,"use"         : "status"
        ,"name"        : "Conflict type"
        ,"description" : "Type of rule conflict"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleExceptAssociatedRules"
        ,"use"         : "status"
        ,"name"        : "Conflict rules"
        ,"description" : "List of rules in conflict."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleIgnoreLocUpdDelay"
        ,"use"         : "config"
        ,"$zName"      : "ignorelocupddelay"
        ,"name"        : "Ignore Location Update Delay"
        ,"description" : "Tags matching rule immediately update to rule's location."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    /* Common base tag group attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupGroupCode"
        ,"$zName"      : "groupcode"
        ,"use"         : "config"
        ,"name"        : "Group Code"
        ,"description" : "Group code defining a group of RF Code Mantis class tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"constraints" : {
            "regex" : "^([A-Z][A-Z][A-Z][A-Z][A-Z][A-Z])$"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupLocUpdateDelay"
        ,"$zName"      : "locupddelay"
        ,"use"         : "config"
        ,"name"        : "Location Update Delay"
        ,"description" : "Controls the amount of time, in seconds, that location updates are delayed, in order to reduce 'bouncing' from spurrious SSI readings."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupAvgPeriod"
        ,"$zName"      : "avgperiod"
        ,"use"         : "config"
        ,"name"        : "Pressure Averaging Period"
        ,"description" : "Controls the time constant, in minutes, used for calculating averaged pressure readings."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "minutes"
        ,"constraints" : {
            "min" : 1
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zRuleLocatorRefTag"
        ,"$zName"      : "locatorreftagid"
        ,"use"         : "config"
        ,"name"        : "Locator's Reference Tag"
        ,"description" : "Optional ID of tag associated with IR locator: only readers that receive beacons from the tag will match the rule."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$zTag"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupEnhPayloadVerify"
        ,"$zName"      : "enhpayloadverify"
        ,"use"         : "config"
        ,"name"        : "Enhanced Payload Verify"
        ,"description" : "Option to enable enhanced payload verification, which can result in some delay in tag payload reporting."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"values"      : [ "NO", "YES", "IFSUPPORTED", "MULTI3", "MULTI4", "MULTI5" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupLocMatchBoost"
        ,"$zName"      : "locmatchboost"
        ,"use"         : "config"
        ,"name"        : "Location Match Confidence Boost"
        ,"description" : "Controls the maximum amount of location matching confidence to give the currently matching tag location."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min" : 0.0,
            "max" : 0.2
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupLocMatchBoostTime"
        ,"$zName"      : "locmatchboosttime"
        ,"use"         : "config"
        ,"name"        : "Location Match Boost Time"
        ,"description" : "Controls the time constant, in seconds, for applying the confidence boost - 63% of remaining boost is applied each period of time."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min" : 0
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupBlockAttribUpdate"
        ,"$zName"      : "blockattrupd"
        ,"use"         : "config"
        ,"name"        : "Ignored Attributes"
        ,"description" : "Comma-separated list of tag attributes to ignore/block value updates.  This can reduce traffic due to changes in sensors that are not of interest."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupNoDataOnLowBattery"
        ,"$zName"      : "nodatalowbatt"
        ,"use"         : "config"
        ,"name"        : "No Readings When Battery Low"
        ,"description" : "If set, sensor readings will not be reported when the sensor tag has a low battery condition.  Prevents reporting of potentially unreliable readings."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupTagType"
        ,"use"         : "hidden"
        ,"name"        : "Tag Type"
        ,"description" : "Tag type supported by tag group."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tTag"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupTagValidation"
        ,"use"         : "hidden"
        ,"name"        : "Tag ID Validation"
        ,"description" : "Validation string used for validating type ids."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupTagTypeValue"
        ,"$zName"      : "type"
        ,"use"         : "hidden"
        ,"name"        : "Tag Type Value"
        ,"description" : "Value of the tag type to use when creating a tag group."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUBreakerAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Breaker Asset Type"
        ,"description" : "The type of asset to create for PDU breaker tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUOutletAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Outlet Asset Type"
        ,"description" : "The type of asset to create for PDU outlet tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUPhaseAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Phase Asset Type"
        ,"description" : "The type of asset to create for PDU phase tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUFeedLineAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Feed Line Asset Type"
        ,"description" : "The type of asset to create for PDU feed line tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUChannelAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Channel Asset Type"
        ,"description" : "The type of asset to create for PDU channel tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupPDUSensorAssetType"
        ,"use"         : "config"
        ,"name"        : "PDU Sensor Asset Type"
        ,"description" : "The type of asset to create for PDU sensor tags."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tAsset"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupTagTimeout"
        ,"$zName"      : "grouptimeout"
        ,"use"         : "config"
        ,"name"        : "Tag Age-Out Time"
        ,"description" : "Tag group specific tag timeout - if -1, reader's tag timeout is used."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
        ,"units"       : "seconds"
        ,"constraints" : {
            "min"  : -1
            ,"max" : 32767
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupTempRounding"
        ,"$zName"      : "tmpround"
        ,"use"         : "config"
        ,"name"        : "Temperature Increment"
        ,"description" : "Specifies the increment to be used for rounding temperature readings, in degrees C."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.1
            ,"max" : 2.0
        }
        ,"units"       : "celsiusdiff"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionValueAt4mA"
        ,"$zName"      : "value4mA"
        ,"use"         : "config"
        ,"name"        : "Reading at 4mA"
        ,"description" : "Sensor reading corresponding to 4mA current loop value"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionValueAt20mA"
        ,"$zName"      : "value20mA"
        ,"use"         : "config"
        ,"name"        : "Reading at 20mA"
        ,"description" : "Sensor reading corresponding to 20mA current loop value"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionValueAt0V"
        ,"$zName"      : "value0V"
        ,"use"         : "config"
        ,"name"        : "Reading at 0V"
        ,"description" : "Sensor reading corresponding to 0V sensor voltage value"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionValueAt5V"
        ,"$zName"      : "value5V"
        ,"use"         : "config"
        ,"name"        : "Reading at 5V"
        ,"description" : "Sensor reading corresponding to 5V sensor voltage value"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionDestination"
        ,"$zName"      : "dest"
        ,"use"         : "config"
        ,"name"        : "Target Reading"
        ,"description" : "Destination attribute for sensor definition."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "string"
        ,"values"      : [ "temp", "humidity", "usersensor1", "usersensor2", "usersensor3", "usersensor4", "usersensor5", "usersensor6", "usersensor7", "usersensor8", "usersensor9", "usersensor10" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionType"
        ,"$zName"      : "type"
        ,"use"         : "config"
        ,"name"        : "Definition Type"
        ,"description" : "Type of sensor definition."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "typeref"
        ,"constraints" : {
            "typeref"  : "$tSensorDefinition"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionTypeValue"
        ,"$zName"      : "type"
        ,"use"         : "hidden"
        ,"name"        : "Definition Type"
        ,"description" : "Type of sensor definition."
        ,"deletable"   : false
        ,"history"     : true
        ,"type"        : "enum"
        ,"values"      : [ "currentloop", "voltage0to5" ]
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupSensorDefinition"
        ,"$zName"      : "defaultSensorDef"
        ,"use"         : "config"
        ,"name"        : "Default Sensor Definition"
        ,"description" : "Default sensor definition to be used by tags"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref"
        ,"constraints" : {
            "typeref"  : "$tSensorDefinition"
        }
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupCurrentRounding"
        ,"$zName"      : "currentRound"
        ,"use"         : "config"
        ,"name"        : "Current Reading Increment"
        ,"description" : "Specifies the increment to be used for rounding current loop readings, in milliamps."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.001
            ,"max" : 1.0
        }
        ,"units"       : "mamps"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupVoltageRounding"
        ,"$zName"      : "voltageRound"
        ,"use"         : "config"
        ,"name"        : "Voltage Reading Increment"
        ,"description" : "Specifies the increment to be used for rounding sensor voltage readings, in volts."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.001
            ,"max" : 0.1
        }
        ,"units"       : "volts"
        ,"printf"      : "%.3f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagGroupHumiRounding"
        ,"$zName"      : "humround"
        ,"use"         : "config"
        ,"name"        : "Humidity Increment"
        ,"description" : "Specifies the increment to be used for rounding humidity readings, in percent RH."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "double"
        ,"constraints" : {
            "min"  : 0.1
            ,"max" : 2.0
        }
        ,"units"       : "rh"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagAccumulatedAttributes"
        ,"use"         : "hidden"
        ,"name"        : "Tag Accumulated Attributes"
        ,"description" : "List of accumulated tag attributes"
        ,"history"     : false
        ,"deletable"   : false
        ,"type"        : "string-list"
        ,"subtype"     : "attribute"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagAccumulatedAttributeTS"
        ,"use"         : "hidden"
        ,"name"        : "Tag Accumulated Attribute Timestamp"
        ,"description" : "Timestamp attribute for accumulated attributes."
        ,"history"     : false
        ,"deletable"   : false
        ,"type"        : "string"
        ,"subtype"     : "attribute"
    }
    ,
    /* GPS attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSPositionCoord"
        ,"$zName"      : "latlon"
        ,"use"         : "status"
        ,"name"        : "Latitude/Longitude"
        ,"description" : "Current coordinates of entity (latitude/longitude)."
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "lat-lon-coord"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zLastGPSPositionCoord"
        ,"$zName"      : "last_latlon"
        ,"use"         : "status"
        ,"name"        : "Last GPS Position"
        ,"description" : "Last known GPS coordinates."
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "lat-lon-coord"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSSpeed"
        ,"$zName"      : "speed"
        ,"use"         : "status"
        ,"name"        : "Speed"
        ,"description" : "Current speed of entity."
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "double"
        ,"units"       : "meterspersec"
        ,"printf"      : "%.1f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSCourse"
        ,"$zName"      : "course"
        ,"use"         : "status"
        ,"name"        : "Course"
        ,"description" : "Current direction of movement of entity, in degrees from north"
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "double"
        ,"units"       : "degrees"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSAltitude"
        ,"$zName"      : "altitude"
        ,"use"         : "status"
        ,"name"        : "Altitude"
        ,"description" : "Current altitude of entity above WGS-84 sea level"
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "double"
        ,"units"       : "meters"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSPositionError"
        ,"$zName"      : "epe_horiz"
        ,"use"         : "status"
        ,"name"        : "Estimated Position Error"
        ,"description" : "Current estimated horizontal position error of entity"
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "double"
        ,"units"       : "meters"
        ,"printf"      : "%.0f"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zGPSAltitudeError"
        ,"$zName"      : "epe_vert"
        ,"use"         : "status"
        ,"name"        : "Estimated Altitude Error"
        ,"description" : "Current estimated vertical position error of entity"
        ,"history"     : true
        ,"deletable"   : false
        ,"type"        : "double"
        ,"units"       : "meters"
        ,"printf"      : "%.0f"
    }
    /* Aurora attributes */
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastProximityTS"
        ,"$zName"      : "lastProxTS"
        ,"use"         : "status"
        ,"name"        : "Last Proximity Event Time"
        ,"description" : "Time-stamp of last proximity event detected by tag, as UTC seconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastProximityID"
        ,"$zName"      : "lastProxID"
        ,"use"         : "status"
        ,"name"        : "Last Proximity ID"
        ,"description" : "Indicates the ID of the last proximity event detected by the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastProximityMatched"
        ,"$zName"      : "lastProxMatched"
        ,"use"         : "status"
        ,"name"        : "Last Proximity Matched"
        ,"description" : "Indicates if the tag was the best matching tag during the last reported proximity event."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagButton0"
        ,"$zName"      : "button0"
        ,"use"         : "status"
        ,"name"        : "Button #1"
        ,"description" : "State of first button on tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagButton1"
        ,"$zName"      : "button1"
        ,"use"         : "status"
        ,"name"        : "Button #2"
        ,"description" : "State of second button on tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagButton2"
        ,"$zName"      : "button2"
        ,"use"         : "status"
        ,"name"        : "Button #3"
        ,"description" : "State of third button on tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagProximityID"
        ,"$zName"      : "proxID"
        ,"use"         : "status"
        ,"name"        : "Proximity ID"
        ,"description" : "Indicates the proximity even ID of the tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagActCount1"
        ,"$zName"      : "actCount1"
        ,"use"         : "status"
        ,"name"        : "Activation Count #1"
        ,"description" : "Total number of activations on input #1."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagActCount2"
        ,"$zName"      : "actCount2"
        ,"use"         : "status"
        ,"name"        : "Activation Count #2"
        ,"description" : "Total number of activations on input #2."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "long"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagActInput1"
        ,"$zName"      : "actInput1"
        ,"use"         : "status"
        ,"name"        : "Activation Input #1"
        ,"description" : "State of input #1."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagActInput2"
        ,"$zName"      : "actInput2"
        ,"use"         : "status"
        ,"name"        : "Activation Input #2"
        ,"description" : "State of input #2."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastAct1TS"
        ,"$zName"      : "lastActTS1"
        ,"use"         : "status"
        ,"name"        : "Last Activation Time #1"
        ,"description" : "Time-stamp of last activation of input #1, in UTC milliseconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastAct2TS"
        ,"$zName"      : "lastActTS2"
        ,"use"         : "status"
        ,"name"        : "Last Activation Time #2"
        ,"description" : "Time-stamp of last activation of input #2, in UTC milliseconds."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastActMatched1"
        ,"$zName"      : "lastProxMatched1"
        ,"use"         : "status"
        ,"name"        : "Last Activation Matched #1"
        ,"description" : "Indicates if the last activation on input #1 was matched to an associated tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagLastActMatched2"
        ,"$zName"      : "lastProxMatched2"
        ,"use"         : "status"
        ,"name"        : "Last Activation Matched #2"
        ,"description" : "Indicates if the last activation on input #2 was matched to an associated tag."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "bool"
    }
    ,
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagActCountTS"
        ,"$zName"      : "actCountTS"
        ,"use"         : "status"
        ,"name"        : "Activation Count Start Time"
        ,"description" : "Start time for activation counts (number of activations is relative to this time)."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "timestamp"
    }
    ,
    /* Common base tag type attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagTypeValue"
        ,"use"         : "hidden"
        ,"name"        : "Tag Type Value"
        ,"description" : "Value of the tag type."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }
    ,
    /* Common base virtual tag type attributes */
    {
        "class"        : "attribute_type"
        ,"guid"        : "$zTagAssetTypeAC"
        ,"use"         : "hidden"
        ,"name"        : "Tag Asset Type Attribute Class"
        ,"description" : "Value of the attribute class for this tag's tag group."
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
        ,"subtype"     : "attribute"
    }
    ,
    /* Base entity defs */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag"
        ,"name"        : "RFID Tag"
        ,"parent"      : "$tTag"
        ,"deletable"   : false
        ,"description" : "Generic RFID Tag."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTagGroup"
                ,"sortPriority"   : 0
            }
            ,
            {
                "guid"            : "$zTagLocation"
                ,"sortPriority"   : 1
            }
            ,
            {
                "guid"            : "$zTagGPSID"
                ,"sortPriority"   : 2
            }
            ,
            {
                "guid"            : "$zTagLastGPSID"
                ,"sortPriority"   : 3
            }
            ,
            {
                "guid"            : "$zTagLastGPSTS"
                ,"sortPriority"   : 4
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag"
        ,"name"        : "Virtual Tag"
        ,"parent"      : "$tTag"
        ,"deletable"   : false
        ,"description" : "Generic Virtual Tag."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTagGroup"
                ,"sortPriority"   : 0
            }
            ,
            {
                "guid"            : "$zTagLocation"
                ,"sortPriority"   : 1
            }
            ,
            {
                "guid"            : "$zTagGPSID"
                ,"sortPriority"   : 2
            }
            ,
            {
                "guid"            : "$zTagLastGPSID"
                ,"sortPriority"   : 3
            }
            ,
            {
                "guid"            : "$zTagLastGPSTS"
                ,"sortPriority"   : 4
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagLinkWithSSI"
        ,"name"        : "Tag link with SSI"
        ,"parent"      : "$tTagLink"
        ,"deletable"   : false
        ,"description" : "Tag link with SSI measurement"
        ,"attributes"  : [
            {
                "guid"            : "$zTagLinkSSI"
                ,"isStatic"       : false
                ,"sortPriority"   : 0
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zReaderChannel"
        ,"name"        : "Reader Channel"
        ,"parent"      : "$tReaderChannel"
        ,"deletable"   : false
        ,"description" : "Tag reading channel of a tag reader."
        ,"attributes"  : [
            {
                "guid"            : "$aReader"
                ,"sortPriority"   : 0
            }
            ,
            {
                "guid"            : "$zReaderChannelID"
                ,"sortPriority"   : 1
            }
            ,
            {
                "guid"            : "$zReaderChannelLabel"
                ,"sortPriority"   : 2
            }
        ]
    }
    ,
    /* Base GPS entity defs */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zGPS"
        ,"name"        : "GPS"
        ,"parent"      : "$tGPS"
        ,"deletable"   : false
        ,"description" : "Generic GPS"
        ,"attributes"  : [
            {
                "guid"            : "$zGPSStatus"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 100
            }
            ,
            {
                "guid"            : "$zGPSPositionCoord"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 101
            }
            ,
            {
                "guid"            : "$zGPSAltitude"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 102
            }
            ,
            {
                "guid"            : "$zGPSSpeed"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 103
            }
            ,
            {
                "guid"            : "$zGPSCourse"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 104
            }
            ,
            {
                "guid"            : "$zGPSPositionError"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 105
            }
            ,
            {
                "guid"            : "$zGPSAltitudeError"
                ,"category"       : "Basic Information"
                ,"sortPriority"   : 106
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zReaderGPS"
        ,"name"        : "Reader GPS"
        ,"parent"      : "$zGPS"
        ,"deletable"   : false
        ,"description" : "Reader-attached GPS"
        ,"attributes"  : [
            {
                "guid"            : "$aReader"
                ,"sortPriority"   : 10
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zGPSSource"
        ,"name"        : "GPS"
        ,"deletable"   : false
        ,"description" : "GPS data source"
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zNoGPSSource"
        ,"name"        : "None"
        ,"parent"      : "$zGPSSource"
        ,"deletable"   : false
        ,"description" : "No GPS data source"
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zReaderGPSSource"
        ,"name"        : "Reader GPS"
        ,"parent"      : "$zGPSSource"
        ,"deletable"   : false
        ,"description" : "Reader GPS"
        ,"attributes"  : [
            {
              "guid"             : "$zReaderGPSMinSSI",
              "required"         : true,
              "isStatic"         : false,
              "value"            : 0,
              "category"         : "Position Settings",
              "sortPriority"     : 172
            },

            {
              "guid"             : "$zReaderGPSData",
              "required"         : false,
              "isStatic"         : false,
              "value"            : "use-global",
              "category"         : "Position Settings",
              "sortPriority"     : 173
            },

            {
              "guid"             : "$zReaderGPSMinPeriod",
              "required"         : false,
              "isStatic"         : false,
              "value"            : -1,
              "category"         : "Position Settings",
              "sortPriority"     : 174
            },

            {
              "guid"             : "$zReaderGPSMinHoriz",
              "required"         : false,
              "isStatic"         : false,
              "value"            : -1,
              "category"         : "Position Settings",
              "sortPriority"     : 175
            },

            {
              "guid"             : "$zReaderGPSMinVert",
              "required"         : false,
              "isStatic"         : false,
              "value"            : -1,
              "category"         : "Position Settings",
              "sortPriority"     : 176
            }
        ]
    }
    ,
    {
        "class"        : "entity_type"
        ,"guid"        : "$zStaticGPSSource"
        ,"name"        : "Static Position"
        ,"parent"      : "$zGPSSource"
        ,"deletable"   : false
        ,"description" : "Static Position"
        ,"attributes"  : [
            {
              "guid"             : "$zReaderGPSMinSSI",
              "required"         : true,
              "isStatic"         : false,
              "value"            : 0,
              "category"         : "Position Settings",
              "sortPriority"     : 172
            },

            {
              "guid"             : "$zReaderGPSPositionCoord",
              "required"         : true,
              "isStatic"         : false,
              "category"         : "Position Settings",
              "sortPriority"     : 174
            }
        ]
    }
    ,
    {
        "class"               : "attribute_type"
        ,"guid"               : "$zGPSSourceSelector"
        ,"use"                : "config"
        ,"name"               : "Position Source"
        ,"description"        : "Source of position data if any."
        ,"deletable"          : false
        ,"history"            : false
        ,"inherit_attributes" : true
        ,"type"               : "typeref"
        ,"constraints"        : {
            "typeref"         : "$zGPSSource"
        }
    }
]
