[
  /* Entity type for rule of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zRuleIRLocatorRule",
     "name"        : "Match by IR Locator",
     "parent"      : "$tRule",
     "deletable"   : false,
     "description" : "Location rule for matching based on IR Locator codes, optionally limited to tag visibility by specific reader channels.",
     "attributes"  : [
                       {
                         "guid"            : "$zRuleTargetLocation",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : "",
                         "sortPriority"    : 12,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleIRLocatorCode",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : "001",
                         "sortPriority"    : 13,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleIRNotFoundTimeout",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 30,
                         "sortPriority"    : 20,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleChannelList",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : [ ],
                         "sortPriority"    : 30,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleLocatorRefTag",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "sortPriority"    : 40,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleIgnoreLocUpdDelay",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "sortPriority"    : 42,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$aRanger",
                         "required"        : false,
                         "isStatic"        : false,
                         "sortPriority"    : 45,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "IRLocatorRule",
                         "sortPriority"    : 100,
                         "category"        : "Rule Configuration"
                       }

                     ]
  }
]
