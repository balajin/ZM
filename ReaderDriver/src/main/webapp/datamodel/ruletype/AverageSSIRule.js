[
  /* Entity type for rule of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zRuleAverageSSIRule",
     "name"        : "Match by Average SSI",
     "parent"      : "$tRule",
     "deletable"   : false,
     "description" : "Location rule for matching based on all of a set of channels observing a tag, and the average SSI exceeding a given threshold.",
     "attributes"  : [
                       {
                         "guid"            : "$zRuleTargetLocation",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : "",
                         "sortPriority"    : 12,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleSSIMinimum",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -100,
                         "sortPriority"    : 13,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleSSIChanMinimum",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -120,
                         "sortPriority"    : 14,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleSSIHighConf",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -60,
                         "sortPriority"    : 20,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleChannelList",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : [ ],
                         "sortPriority"    : 30,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "AverageSSIRule",
                         "sortPriority"    : 100,
                         "category"        : "Rule Configuration"
                       }

                     ]
  }
]
