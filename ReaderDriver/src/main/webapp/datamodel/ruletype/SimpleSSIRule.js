[
  /* Entity type for rule of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zRuleSimpleSSIRule",
     "name"        : "Match by Simple SSI",
     "parent"      : "$tRule",
     "deletable"   : false,
     "description" : "Location rule for matching based on any of a set of channels observing a tag with stronger than a given SSI.",
     "attributes"  : [
                       {
                         "guid"            : "$zRuleTargetLocation",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : "",
                         "sortPriority"    : 12,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleSSIMinimum",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -90,
                         "sortPriority"    : 13,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleSSIHighConf",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -60,
                         "sortPriority"    : 20,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleChannelList",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : [ ],
                         "sortPriority"    : 30,
                         "category"        : "Rule Configuration"
                       },

                       {
                         "guid"            : "$zRuleType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "SimpleSSIRule",
                         "sortPriority"    : 100,
                         "category"        : "Rule Configuration"
                       }

                     ]
  }
]
