[
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduOutlet"
        ,"name"        : "RF Code PDU Outlet Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "RF Code virtual PDU outlet tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduOutlet"
            }
            ,
            {
                "guid"            : "$zTagAssetTypeAC"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "$zTagGroupPDUOutletAssetType"
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : [ "$zTagOutletTotWH", "$zTagOutletTotVAH" ]
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : "$zTagOutletTotPwrTS"
            }
            ,
            {
                "guid"            : "$zTagOutletVolts"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletActivePwr"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletPwrFactor"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletAppPwr"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletAmps"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletCfg"
                ,"required"       : false
                ,"isStatic"       : false
            }
			,
			{
				"guid"			  : "$zTagOutletBankID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagFeedSetID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagTowerID"
				,"required"		  : false
				,"isStatic"		  : false
			}
            ,
            {
                "guid"            : "$zTagOutletTotWH"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletTotVAH"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletTotPwrTS"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletLabel"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagOutletSwitch"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
