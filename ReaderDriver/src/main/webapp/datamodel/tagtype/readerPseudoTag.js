[
  /* Entity type for tag group of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTagGroup_readerPseudoTag",
     "name"        : "Reader Pseudo Tag Group",
     "parent"      : "$tTagGroup",
     "deletable"   : false,
     "description" : "Tag group for Reader Pseudo-Tags.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagGroupGroupCode",
                         "required"        : true,
                         "isStatic"        : true,
                         "category"        : "Basic Information",
                         "value"           : "READER",
                         "sortPriority"    : 20
                       },
                       {
                         "guid"            : "$zTagGroupLocUpdateDelay",
                         "required"        : true,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : 0,
                         "sortPriority"    : 21
                       },
                       {
                         "guid"            : "$zTagGroupTagType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "$zTag_readerPseudoTag",
                         "sortPriority"    : 22
                       },
                       {
                         "guid"            : "$zTagGroupTagTypeValue",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "readerPseudoTag"
                       },
                       {
                         "guid"            : "$zTagGroupTagValidation",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "^([A-Z]{6})(_\\S*$)"
                       }
                     ]
  },
  /* Entity type for tags of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTag_readerPseudoTag",
     "name"        : "RF Code Reader Pseudo-Tag",
     "parent"      : "$zTag",
     "deletable"   : false,
     "description" : "RF Code Reader Pseudo-Tag.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagTypeValue",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "readerPseudoTag"
                       }
                     ]
  }
]
