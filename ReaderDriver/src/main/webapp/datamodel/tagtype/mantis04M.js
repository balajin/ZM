[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04M"
        ,"name"        : "Treatment 04M Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04M."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "STIRCK"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04M"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04M"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
            ,
            /* types for creating assets for virtual tags */
            {
                "guid"             : "$zTagGroupPDUBreakerAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 100
                ,"value"           : "PDU_BREAKER"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUOutletAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 101
                ,"value"           : "PDU_OUTLET"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUPhaseAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 102
                ,"value"           : "PDU_PHASE"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUFeedLineAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 103
                ,"value"           : "PDU_FEEDLINE"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04M"
        ,"name"        : "RF Code Treatment 04M Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04M."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04M"
            }
            ,
            {
                "guid"             : "$zTagHasChildren"
                ,"required"        : true
                ,"isStatic"        : false
                ,"value"           : true
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagPDUTowerDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnectedTowers"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zTagPDUModel"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNum"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zTagPDUModelList"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNumList"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
			,
            {
                "guid"             : "$zTagPDUTowerCount"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
			,
            {
                "guid"             : "$zTagPDUActivePwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 10
            }
            ,
            {
                "guid"             : "$zTagPDUAppPwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 11
            }
            ,
            {
                "guid"             : "$zTagPDUPwrFactor"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 12
            }
            ,
            {
                "guid"             : "$zTagPDUTotPwrTS"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 13
            }
            ,
            {
                "guid"             : "$zTagPDUTotWH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 14
            }
            ,
            {
                "guid"             : "$zTagPDUTotVAH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 15
            }
            ,
            {
                "guid"             : "$zTagMsgLossRate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 16
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 17
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : [ "$zTagPDUTotWH", "$zTagPDUTotVAH" ]
                ,"sortPriority"   : 18
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : "$zTagPDUTotPwrTS"
                ,"sortPriority"   : 19
            }
        ]
    }
]
