[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04N"
        ,"name"        : "Treatment 04N Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04N."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "GSTRCK"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04N"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04N"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
            ,
            /* types for creating assets for virtual tags */
            {
                "guid"             : "$zTagGroupPDUBreakerAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 100
                ,"value"           : "PDU_BREAKER"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUOutletAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 101
                ,"value"           : "PDU_OUTLET"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUPhaseAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 102
                ,"value"           : "PDU_PHASE"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUFeedLineAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 103
                ,"value"           : "PDU_FEEDLINE"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUChannelAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 104
                ,"value"           : "PDU_CHANNEL"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04N"
        ,"name"        : "RF Code Treatment 04N Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04N."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04N"
            }
            ,
            {
                "guid"             : "$zTagHasChildren"
                ,"required"        : true
                ,"isStatic"        : false
                ,"value"           : true
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagPDUModel"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNum"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zTagMsgLossRate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
			,
            {
                "guid"             : "$zTagPDUModelMatch"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
        ]
    }
]
