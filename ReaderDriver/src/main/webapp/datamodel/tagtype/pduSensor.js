[
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduSensor"
        ,"name"        : "PDU Sensor Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "PDU sensor tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduSensor"
            }
            ,
            {
                "guid"            : "$zTagAssetTypeAC"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "$zTagGroupPDUSensorAssetType"
            }
            ,
            {
                "guid"             : "$zTagTemperature"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 1
            }
            ,
            {
                "guid"             : "$zTagHumidity"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagDewPoint"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
			,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
