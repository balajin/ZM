[
  /* Entity type for tag group of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTagGroup_barcodeTag",
     "name"        : "Barcode Tag Group",
     "parent"      : "$tTagGroup",
     "deletable"   : false,
     "description" : "Tag group for barcode tags with RF Code style tag IDs.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagGroupGroupCode",
                         "required"        : true,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : "BARCOD",
                         "sortPriority"    : 20
                       },
                       {
                         "guid"            : "$zTagGroupLocUpdateDelay",
                         "required"        : true,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : 0,
                         "sortPriority"    : 21
                       },
                       {
                         "guid"            : "$zTagGroupTagType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "$zTag_barcodeTag",
                         "sortPriority"    : 22
                       },
                       {
                         "guid"            : "$zTagGroupTagTypeValue",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "barcodeTag"
                       },
                       {
                         "guid"            : "$zTagGroupTagValidation",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "^([A-Z]{6})(_\\S*$)"
                       }
                     ]
  },
  /* Entity type for tags of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTag_barcodeTag",
     "name"        : "Barcode Tag",
     "parent"      : "$zTag",
     "deletable"   : false,
     "description" : "Barcode tag with RF Code tag ID.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagTypeValue",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "barcodeTag"
                       }
                     ]
  }
]
