[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04W"
        ,"name"        : "Treatment 04W Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04W."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "RCKIRT"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04W"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04W"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
        ]
    },
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04W"
        ,"name"        : "RF Code Treatment 04W Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04W."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04W"
            }
            ,
            {
                "guid"             : "$zTagMotion"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagPanic"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagTamper"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zTagTamperArmed"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
        ]
    }
]
