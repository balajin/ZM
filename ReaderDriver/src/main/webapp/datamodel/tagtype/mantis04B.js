[
  /* Entity type for tag group of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTagGroup_mantis04B",
     "name"        : "Treatment 04B Tag Group",
     "parent"      : "$tTagGroup",
     "deletable"   : false,
     "description" : "Tag group for Reader tags supporting treatment code 04B.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagGroupGroupCode",
                         "required"        : true,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : "RFCDAT",
                         "sortPriority"    : 20
                       },
                       {
                         "guid"            : "$zTagGroupLocUpdateDelay",
                         "required"        : true,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : 0,
                         "sortPriority"    : 21
                       },
                       {
                         "guid"            : "$zTagGroupBlockAttribUpdate",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : "",
                         "sortPriority"    : 23
                       },
                       {
                         "guid"            : "$zTagGroupTagTimeout",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "value"           : -1,
                         "sortPriority"    : 24
                       },
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0                ,"sortPriority"    : 25
            },
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            },
                       {
                         "guid"            : "$zTagGroupTagType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "$zTag_mantis04B"
                       },
                       {
                         "guid"            : "$zTagGroupTagTypeValue",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "mantis04B"
                       },
                       {
                         "guid"            : "$zTagGroupTagValidation",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "^([A-Z]{6})(\\d{8})$"
                       }
                     ]
  },
  /* Entity type for tags of this type */
  {
     "class"       : "entity_type",
     "guid"        : "$zTag_mantis04B",
     "name"        : "RF Code Treatment 04B Tag",
     "parent"      : "$zTag",
     "deletable"   : false,
     "description" : "RF Code Mantis tags supporting treatment code 04B.",
     "attributes"  : [
                       {
                         "guid"            : "$zTagTypeValue",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "mantis04B"
                       },
                       {
                         "guid"            : "$zTagUserPayload",
                         "required"        : true,
                         "isStatic"        : false,
                         "sortPriority"    : 1
                       }
                     ]
  }
]
