[
  /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduPhase"
        ,"name"        : "RF Code PDU Phase Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "RF Code virtual phase tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduPhase"
            }
            ,
            {
              "guid"              : "$zTagAssetTypeAC"
              ,"required"         : false
              ,"isStatic"         : false
              ,"value"            : "$zTagGroupPDUPhaseAssetType"
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : [ "$zTagPhaseTotWH", "$zTagPhaseTotVAH" ]
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : "$zTagPhaseTotPwrTS"
            }
            ,
            {
                "guid"            : "$zTagPhaseVoltsN"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseVoltsP"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseActivePwr"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhasePwrFactor"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseAppPwr"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseAmps"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseTotPwrTS"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseTotWH"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagPhaseTotVAH"
                ,"required"       : false
                ,"isStatic"       : false
            }
			,
			{
				"guid"			  : "$zTagPhaseCfg"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagFeedSetID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagTowerID"
				,"required"		  : false
				,"isStatic"		  : false
			}
            ,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
