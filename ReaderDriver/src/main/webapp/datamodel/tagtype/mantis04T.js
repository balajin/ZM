[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04T"
        ,"name"        : "Treatment 04T Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04T."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IRFPRX"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04T"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04T"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
        ]
    },
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04T"
        ,"name"        : "RF Code Treatment 04T Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04T."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04T"
            }
			,
            {
                "guid"             : "$zTagProximityID"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 1
            }
			,
            {
                "guid"             : "$zTagActCount1"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 10
            }
			,
            {
                "guid"             : "$zTagActInput1"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 11
            }
			,
            {
                "guid"             : "$zTagLastAct1TS"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 12
            }
			,
            {
                "guid"             : "$zTagLastActMatched1"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 13
            }
			,
            {
                "guid"             : "$zTagActCount2"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 20
            }
			,
            {
                "guid"             : "$zTagActInput2"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 21
            }
			,
            {
                "guid"             : "$zTagLastAct2TS"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 22
            }
			,
            {
                "guid"             : "$zTagLastActMatched2"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 23
            }
			,
            {
                "guid"             : "$zTagActCountTS"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 30
            }
			,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 40
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : [ "$zTagActCount1", "$zTagActCount2" ]
                ,"sortPriority"   : 50
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : "$zTagActCountTS"
                ,"sortPriority"   : 51
            }
        ]
    }
]
