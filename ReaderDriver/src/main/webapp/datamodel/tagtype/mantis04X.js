[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04X"
        ,"name"        : "Treatment 04X Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04X."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "CLSRCK"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupCurrentRounding"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.01
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 27
            }
            ,
            {
                "guid"             : "$zTagGroupSensorDefinition"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"sortPriority"    : 28
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04X"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04X"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04X"
        ,"name"        : "RF Code Treatment 04X Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04X."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04X"
            }
            ,
            {
                "guid"             : "$zTagCurrentLoopReading"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 1
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagSensorDisconnect"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagMsgLossRate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zUserSensor1"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zUserSensor2"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zUserSensor3"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zUserSensor4"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
            ,
            {
                "guid"             : "$zUserSensor5"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
            ,
            {
                "guid"             : "$zUserSensor6"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 10
            }
            ,
            {
                "guid"             : "$zUserSensor7"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 11
            }
            ,
            {
                "guid"             : "$zUserSensor8"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 12
            }
            ,
            {
                "guid"             : "$zUserSensor9"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 13
            }
            ,
            {
                "guid"             : "$zUserSensor10"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 14
            }
        ]
    }
]
