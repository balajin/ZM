[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04J"
        ,"name"        : "Treatment 04J Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04J."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "RFCRCK"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04J"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04J"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04J"
        ,"name"        : "RF Code Treatment 04J Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04J."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04J"
            }
            ,
            {
                "guid"             : "$zTagFluid"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagMotion"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagDoorOpen"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zTagDryContactOpen"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagSensorDisconnect"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zTagPanic"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zTagTamper"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
        ]
    }
]
