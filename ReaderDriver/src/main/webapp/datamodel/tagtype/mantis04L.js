[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_mantis04L"
        ,"name"        : "Treatment 04L Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for Reader tags supporting treatment code 04L."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "RTNRCK"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocUpdateDelay"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0
                ,"sortPriority"    : 21
            }
            ,
            {
                "guid"             : "$zTagGroupEnhPayloadVerify"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "IFSUPPORTED"
                ,"sortPriority"    : 22
            }
            ,
            {
                "guid"             : "$zTagGroupBlockAttribUpdate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : ""
                ,"sortPriority"    : 23
            }
            ,
            {
                "guid"             : "$zTagGroupTagTimeout"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : -1
                ,"sortPriority"    : 24
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_mantis04L"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "mantis04L"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
            ,
            /* types for creating assets for virtual tags */
            {
                "guid"             : "$zTagGroupPDUBreakerAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 100
                ,"value"           : "PDU_BREAKER"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUOutletAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 101
                ,"value"           : "PDU_OUTLET"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUPhaseAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 102
                ,"value"           : "PDU_PHASE"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_mantis04L"
        ,"name"        : "RF Code Treatment 04L Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "RF Code Mantis tags supporting treatment code 04L."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "mantis04L"
            }
            ,
            {
                "guid"             : "$zTagHasChildren"
                ,"required"        : true
                ,"isStatic"        : false
                ,"value"           : true
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagPDUModel"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNum"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zTagPDUActivePwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagPDUAppPwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zTagPDUPwrFactor"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zTagPDUTotPwrTS"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
            ,
            {
                "guid"             : "$zTagPDUTotWH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
            ,
            {
                "guid"             : "$zTagPDUTotVAH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 10
            }
            ,
            {
                "guid"             : "$zTagMsgLossRate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 11
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 12
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : [ "$zTagPDUTotWH", "$zTagPDUTotVAH" ]
                ,"sortPriority"   : 13
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : false
                ,"value"          : "$zTagPDUTotPwrTS"
                ,"sortPriority"   : 14
            }
        ]
    }
]
