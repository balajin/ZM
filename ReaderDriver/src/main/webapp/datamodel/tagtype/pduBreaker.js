[
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduBreaker"
        ,"name"        : "RF Code PDU Breaker Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "RF Code virtual PDU breaker tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduBreaker"
            }
            ,
            {
                "guid"            : "$zTagAssetTypeAC"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "$zTagGroupPDUBreakerAssetType"
            }
			,
			{
				"guid"			  : "$zTagBrkAmps"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagBrkTripped"
				,"required"		  : false
				,"isStatic"		  : false
			}
            ,
			{
				"guid"			  : "$zTagBrkOverload"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagBrkCfg"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagBrkBankID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagFeedSetID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
			{
				"guid"			  : "$zTagTowerID"
				,"required"		  : false
				,"isStatic"		  : false
			}
            ,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
