[
    /* Entity type for tag group of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTagGroup_SPMCDU"
        ,"name"        : "SPM CDU Tag Group"
        ,"parent"      : "$tTagGroup"
        ,"deletable"   : false
        ,"description" : "Tag group for CDUs reported by SPM servers."
        ,"attributes"  : [
            {
                "guid"             : "$zTagGroupGroupCode"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "SPMCDU"
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoost"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 25
            }
            ,
            {
                "guid"             : "$zTagGroupLocMatchBoostTime"
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 300
                ,"sortPriority"    : 26
            }
            ,
            {
                "guid"             : "$zTagGroupTagType"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "$zTag_SPMCDU"
            }
            ,
            {
                "guid"             : "$zTagGroupTagTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "spmCDU"
            }
            ,
            {
                "guid"             : "$zTagGroupTagValidation"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : "^([A-Z]{6})(\\d{8})$"
            }
            ,
            /* types for creating assets for virtual tags */
            {
                "guid"             : "$zTagGroupPDUBreakerAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 100
                ,"value"           : "PDU_BREAKER"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUOutletAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 101
                ,"value"           : "PDU_OUTLET"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUPhaseAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 102
                ,"value"           : "PDU_PHASE"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUFeedLineAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 103
                ,"value"           : "PDU_FEEDLINE"
            }
            ,
            {
                "guid"             : "$zTagGroupPDUSensorAssetType"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "PDU Asset Types"
                ,"sortPriority"    : 104
                ,"value"           : "PDU_SENSOR"
            }
        ]
    }
    ,
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zTag_SPMCDU"
        ,"name"        : "SPM CDU Tag"
        ,"parent"      : "$zTag"
        ,"deletable"   : false
        ,"description" : "CDU tag reported by SPM Server."
        ,"attributes"  : [
            {
                "guid"             : "$zTagTypeValue"
                ,"required"        : false
                ,"isStatic"        : false
                ,"value"           : "spmCDU"
            }
            ,
            {
                "guid"             : "$zTagHasChildren"
                ,"required"        : true
                ,"isStatic"        : false
                ,"value"           : true
            }
            ,
            {
                "guid"             : "$zTagDefLabel"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 1
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 2
            }
            ,
            {
                "guid"             : "$zTagPDUTowerDisconnect"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 3
            }
            ,
            {
                "guid"             : "$zTagPDUDisconnectedTowers"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 4
            }
            ,
            {
                "guid"             : "$zTagPDUModel"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 5
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNum"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 6
            }
            ,
            {
                "guid"             : "$zTagPDUModelList"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 7
            }
            ,
            {
                "guid"             : "$zTagPDUSerialNumList"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 8
            }
			,
            {
                "guid"             : "$zTagPDUTowerCount"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 9
            }
			,
            {
                "guid"             : "$zTagPDUActivePwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 10
            }
            ,
            {
                "guid"             : "$zTagPDUAppPwr"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 11
            }
            ,
            {
                "guid"             : "$zTagPDUPwrFactor"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 12
            }
            ,
            {
                "guid"             : "$zTagPDUTotPwrTS"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 13
            }
            ,
            {
                "guid"             : "$zTagPDUTotWH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 14
            }
            ,
            {
                "guid"             : "$zTagPDUTotVAH"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 15
            }
            ,
            {
                "guid"             : "$zTagMsgLossRate"
                ,"required"        : false
                ,"isStatic"        : false
                ,"sortPriority"    : 16
            }
            ,
            {
                "guid"             : "$zTagLowBattery"
                ,"required"        : true
                ,"isStatic"        : false
                ,"sortPriority"    : 17
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributes"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : [ "$zTagPDUTotWH", "$zTagPDUTotVAH" ]
                ,"sortPriority"   : 18
            }
            ,
            {
                "guid"            : "$zTagAccumulatedAttributeTS"
                ,"required"       : true
                ,"isStatic"       : true
                ,"value"          : "$zTagPDUTotPwrTS"
                ,"sortPriority"   : 19
            }
        ]
    }
]
