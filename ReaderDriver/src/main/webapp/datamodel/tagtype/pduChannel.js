[
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduChannel"
        ,"name"        : "RF Code PDU Channel Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "RF Code virtual PDU channel tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduChannel"
            }
            ,
            {
                "guid"            : "$zTagAssetTypeAC"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "$zTagGroupPDUChannelAssetType"
            }
            ,
            {
                "guid"            : "$zTagChannelAmps"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
			{
				"guid"			  : "$zTagTowerID"
				,"required"		  : false
				,"isStatic"		  : false
			}
			,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
