[
    /* Entity type for tags of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zVirtualTag_pduFeedLine"
        ,"name"        : "RF Code PDU Feed Line Tag"
        ,"parent"      : "$zVirtualTag"
        ,"deletable"   : false
        ,"description" : "RF Code virtual PDU feed line tags."
        ,"attributes"  : [
            {
                "guid"            : "$zTagTypeValue"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "pduFeedLine"
            }
            ,
            {
                "guid"            : "$zTagAssetTypeAC"
                ,"required"       : false
                ,"isStatic"       : false
                ,"value"          : "$zTagGroupPDUFeedLineAssetType"
            }
            ,
            {
                "guid"            : "$zTagFeedAmps"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagFeedOverload"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagFeedLoadWarning"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagFeedCfg"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagFeedSetID"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagTowerID"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagParentTag"
                ,"required"       : false
                ,"isStatic"       : false
            }
            ,
            {
                "guid"            : "$zTagSubIndex"
                ,"required"       : false
                ,"isStatic"       : false
            }
        ]
    }
]
