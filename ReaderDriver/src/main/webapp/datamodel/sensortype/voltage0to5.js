[
    /* Entity type for sensor definiton of this type */
    {
        "class"        : "entity_type"
        ,"guid"        : "$zSensorDefinition_voltage0to5"
        ,"name"        : "Voltage (0-5 V) Sensor"
        ,"parent"      : "$tSensorDefinition"
        ,"deletable"   : false
        ,"description" : "Sensor definition for voltage (0-5 V) sensors."
        ,"attributes"  : [
            {
                "guid"             : "$zSensorDefinitionValueAt0V"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 0.0
                ,"sortPriority"    : 20
            }
            ,
            {
                "guid"             : "$zSensorDefinitionValueAt5V"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : 100.0
                ,"sortPriority"    : 30
            }
            ,
            {
                "guid"             : "$zSensorDefinitionDestination"
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "Basic Information"
                ,"value"           : "temp"
                ,"sortPriority"    : 40
            }
            ,
            {
                "guid"             : "$zSensorDefinitionTypeValue"
                ,"required"        : true
                ,"isStatic"        : true
                ,"value"           : 1
            }
        ]
    }
]
