[
    /*
     * Datamodel used only by the Ranger UI, not to be used by AM
     */
    {
       "class"       : "attribute_type",
       "guid"        : "$aName",
       "use"         : "config-view",
       "name"        : "Name",
       "description" : "Name",
       "history"     : true,
       "deletable"   : false,
       "type"        : "string"
    }

    ,{
       "class"       : "attribute_type",
       "guid"        : "$aEnabled",
       "use"         : "config-view",
       "$zName"      : "enabled",
       "name"        : "Enabled",
       "description" : "Enabled/disabled setting",
       "history"     : true,
       "deletable"   : false,
       "type"        : "bool"
    }

    ,{
       "class"       : "attribute_type",
       "guid"        : "$aHostname",
       "$zName"      : "hostname",
       "use"         : "config-view",
       "name"        : "Hostname",
       "description" : "TCP/IP hostname",
       "history"     : false,
       "deletable"   : false,
       "type"        : "string"
    }

    ,{
       "class"       : "attribute_type",
       "guid"        : "$aPortNum",
       "$zName"      : "port",
       "use"         : "config",
       "name"        : "Port",
       "description" : "TCP/IP port",
       "history"     : false,
       "deletable"   : false,
       "type"        : "long",
       "constraints" : {
          "min"   : 1,
          "max"   : 65535
       }
    }

    ,{
         "class"              : "attribute_type"
        ,"guid"               : "$zPassword"
        ,"$zName"             : "password"
        ,"use"                : "config"
        ,"name"               : "Password"
        ,"description"        : "Password"
        ,"history"            : true
        ,"deletable"          : false
        ,"type"               : "password"
    }

    ,{
         "class"              : "attribute_type"
        ,"guid"               : "$zRoles"
        ,"$zName"             : "role"
        ,"use"                : "config"
        ,"name"               : "Roles"
        ,"description"        : "User Roles"
        ,"history"            : true
        ,"deletable"          : false
        ,"type"               : "string"
        ,"values"             : [ "admin", "configview", "eventview" ]
    }

    /* override changing use to config */
    ,{
       "class"       : "attribute_type",
       "guid"        : "$zReaderGroups",
       "$zName"      : "groups",
       "use"         : "config",
       "name"        : "Tag Groups",
       "description" : "Set of tag groups to be monitored by reader.",
       "deletable"   : false,
       "history"     : false,
       "type"        : "entityref-list",
       "constraints" : { "typeref" : "$tTagGroup" }
    }

    ,{
       "class"       : "attribute_type",
       "guid"        : "$zTagGroupReaderList",
       "$zName"      : "readers",
       "use"         : "config",
       "name"        : "Readers",
       "description" : "List of readers associated with the tag group.",
       "deletable"   : false,
       "history"     : false,
       "type"        : "entityref-list",
       "constraints" : { "typeref" : "$tReader" }
    }

    /* Label attribute - AM uses the $aName attribute for the label */
    ,{
        "class"        : "attribute_type"
        ,"guid"        : "$zLabel"
        ,"$zName"      : "label"
        ,"use"         : "config"
        ,"name"        : "Label"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "string"
    }

    /* Specify which tags belong to the sensor definition */
    ,{
        "class"        : "attribute_type"
        ,"guid"        : "$zSensorDefinitionTags"
        ,"$zName"      : "tags"
        ,"use"         : "config"
        ,"name"        : "Tags"
        ,"deletable"   : false
        ,"history"     : false
        ,"type"        : "entityref-list"
    }

    ,{
         "guid"        : "$zUser"
        ,"class"       : "entity_type"
        ,"name"        : "User"
        ,"deletable"   : false
        ,"description" : "User"
        ,"attributes"  : [
            {
                 "guid"            : "$aName"
                ,"deletable"       : false
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "User Information"
                ,"sortPriority"    : 15
            }

            ,{
                 "guid"            : "$zPassword"
                ,"deletable"       : false
                ,"required"        : false
                ,"isStatic"        : false
                ,"category"        : "User Information"
                ,"sortPriority"    : 25
            }

            ,{
                 "guid"            : "$zRoles"
                ,"deletable"       : false
                ,"required"        : true
                ,"isStatic"        : false
                ,"category"        : "User Information"
                ,"sortPriority"    : 50
                ,"value"           : "configview"
            }
        ]
    }

    ,{
       "guid"         : "$tTagGroup"
      ,"class"        : "entity_type"
      ,"name"         : "Tag Group"
      ,"parent"       : "$tConfiguration"
      ,"deletable"    : false
      ,"description"  : "Tag Group"
      ,"attributes"  : [
          {
               "guid"            : "$aName"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 0
          }
          ,{
               "guid"            : "$zLabel"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 1
          }
          ,{
               "guid"            : "$zTagGroupReaderList"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "Readers"
              ,"sortPriority"    : 100
          }
      ]
   }

    ,{
       "guid"         : "$tSensorDefinition"
      ,"class"        : "entity_type"
      ,"name"         : "Sensor Definition"
      ,"parent"       : "$tConfiguration"
      ,"deletable"    : false
      ,"description"  : "Sensor Definition"
      ,"attributes"  : [
          {
               "guid"            : "$aName"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 0
          }
          ,{
               "guid"            : "$zLabel"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 1
          }
          ,{
               "guid"            : "$zSensorDefinitionTags"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 1000
          }
      ]
   }

  ,{
       "guid"         : "$tRule"
      ,"class"        : "entity_type"
      ,"name"         : "Rule"
      ,"parent"       : "$tConfiguration"
      ,"deletable"    : false
      ,"description"  : "Rule"
      ,"attributes"   : [
          {
               "guid"            : "$aName"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 0
          }
          ,{
                "guid"          : "$aEnabled"
                ,"isStatic"     : false
                ,"required"     : false
                ,"category"     : "Rule Configuration"
                ,"sortPriority" : 11
                ,"value"        : true
          }
      ]
  }

  ,{
       "guid"         : "$tReader"
      ,"class"        : "entity_type"
      ,"name"         : "Reader"
      ,"parent"       : "$tSystem"
      ,"deletable"    : false
      ,"description"  : "Root of the reader tree"
      ,"attributes"   : [
          {
               "guid"            : "$aName"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 0
          }
          ,{
               "guid"            : "$zLabel"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "Basic Information"
              ,"sortPriority"    : 1
          }
          ,{
              "guid"          : "$aEnabled"
              ,"isStatic"     : false
              ,"required"     : false
              ,"category"     : "Basic Information"
              ,"sortPriority" : 2
              ,"value"        : true
          }
      ]
  }

  ,{
       "guid"        : "$zUser"
      ,"class"       : "entity_type"
      ,"name"        : "User"
      ,"deletable"   : false
      ,"description" : "User"
      ,"attributes"  : [
          {
               "guid"            : "$aName"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "User Information"
              ,"sortPriority"    : 15
          }

          ,{
               "guid"            : "$zPassword"
              ,"deletable"       : false
              ,"required"        : false
              ,"isStatic"        : false
              ,"category"        : "User Information"
              ,"sortPriority"    : 25
          }

          ,{
               "guid"            : "$zRoles"
              ,"deletable"       : false
              ,"required"        : true
              ,"isStatic"        : false
              ,"category"        : "User Information"
              ,"sortPriority"    : 50
              ,"value"           : "configview"
          }
      ]
  }

  ,{
     "guid"         : "$zGPSData"
    ,"class"        : "entity_type"
    ,"name"         : "GPS Data"
    ,"parent"       : "$tConfiguration"
    ,"deletable"    : false
    ,"description"  : "GPS data"
    ,"attributes"  : [
        {
             "guid"            : "$zReaderGPSData"
            ,"deletable"       : false
            ,"isStatic"        : false
            ,"category"        : "Global GPS Defaults"
            ,"sortPriority"    : 0
        }
        ,{
             "guid"            : "$zReaderGPSMinPeriod"
            ,"deletable"       : false
            ,"isStatic"        : false
            ,"category"        : "Global GPS Defaults"
            ,"sortPriority"    : 1
        }
        ,{
             "guid"            : "$zReaderGPSMinHoriz"
            ,"deletable"       : false
            ,"isStatic"        : false
            ,"category"        : "Global GPS Defaults"
            ,"sortPriority"    : 2
        }
        ,{
             "guid"            : "$zReaderGPSMinVert"
            ,"deletable"       : false
            ,"isStatic"        : false
            ,"category"        : "Global GPS Defaults"
            ,"sortPriority"    : 3
        }
    ]
 }

  ,
  {
      "class"        : "attribute_type"
      ,"guid"        : "$zReaderGPSLatLonCoord"
      ,"$zName"      : "latlon"
      ,"use"         : "status"
      ,"name"        : "Latitude/Longitude"
      ,"deletable"   : false
      ,"history"     : false
      ,"type"        : "lat-lon-coord"
  }
  ,
  {
      "class"        : "attribute_type"
      ,"guid"        : "$zReaderGPSAltitude"
      ,"$zName"      : "altitude"
      ,"use"         : "status"
      ,"name"        : "Altitude"
      ,"deletable"   : false
      ,"history"     : false
      ,"type"        : "double"
      ,"units"       : "meters"
      ,"printf"      : "%.1f"
  }
  ,
  {
      "class"        : "attribute_type"
      ,"guid"        : "$zReaderGPSSpeed"
      ,"$zName"      : "speed"
      ,"use"         : "status"
      ,"name"        : "Speed"
      ,"deletable"   : false
      ,"history"     : false
      ,"type"        : "double"
      ,"units"       : "meterspersec"
      ,"printf"      : "%.1f"
  }
  ,
  {
      "class"        : "attribute_type"
      ,"guid"        : "$zReaderGPSEPEHoriz"
      ,"$zName"      : "epe_horiz"
      ,"use"         : "status"
      ,"name"        : "Estimated Position Error"
      ,"deletable"   : false
      ,"history"     : false
      ,"type"        : "double"
      ,"units"       : "meters"
      ,"printf"      : "%.1f"
  }
  ,
  {
      "class"        : "attribute_type"
      ,"guid"        : "$zReaderGPSEPEVert"
      ,"$zName"      : "epe_vert"
      ,"use"         : "status"
      ,"name"        : "Estimated Altitude Error"
      ,"deletable"   : false
      ,"history"     : false
      ,"type"        : "double"
      ,"units"       : "meters"
      ,"printf"      : "%.1f"
  }

]
