[
  /* Entity type for reader */
  {
     "class"       : "entity_type",
     "guid"        : "$zReaderM240",
     "name"        : "M240 Reader",
     "parent"      : "$tReader",
     "deletable"   : false,
     "description" : "Reader driver for M240 Reader, using TCP/IP interface.",
     "attributes"  : [
                       {
                         "guid"            : "$zReaderType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "M240"
                       },
                       {
                         "guid"            : "$zReaderGroups",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "sortPriority"    : 2
                       },

                       {
                         "guid"            : "$aHostname",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "192.168.1.129",
                         "category"        : "Network Settings",
                         "sortPriority"    : 20
                       },

                       {
                         "guid"            : "$aPortNum",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 6500,
                         "category"        : "Network Settings",
                         "sortPriority"    : 21
                       },

                       {
                         "guid"            : "$zReaderSSLMode",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "IFAVAIL",
                         "category"        : "Network Settings",
                         "sortPriority"    : 22
                       },

                       {
                         "guid"            : "$zReaderUserID",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Authentication",
                         "sortPriority"    : 100
                       },

                       {
                         "guid"            : "$zReaderPassword",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Authentication",
                         "sortPriority"    : 110
                       },

                       {
                         "guid"            : "$zReaderUpConnectEnabled",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "category"        : "Up Connect Settings",
                         "sortPriority"    : 150
                       },

                       {
                         "guid"            : "$zReaderUpConnectReaderID",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Up Connect Settings",
                         "sortPriority"    : 151
                       },

                       {
                         "guid"            : "$zReaderUpConnectPassword",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Up Connect Settings",
                         "sortPriority"    : 152
                       },

                       {
                         "guid"            : "$zGPSSourceSelector",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "$zNoGPSSource",
                         "category"        : "Position Settings",
                         "sortPriority"    : 170
                       },

                       {
                         "guid"            : "$zReaderSSIDeltaThresh",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 3,
                         "category"        : "Advanced",
                         "sortPriority"    : 200
                       },

                       {
                         "guid"            : "$zReaderSSICutoffA",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Advanced",
                         "sortPriority"    : 210
                       },

                       {
                         "guid"            : "$zReaderSSICutoffB",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Advanced",
                         "sortPriority"    : 211
                       },

                       {
                         "guid"            : "$zReaderTagTimeout",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 60,
                         "category"        : "Advanced",
                         "sortPriority"    : 220
                       },

                       {
                         "guid"            : "$zReaderAgeOutTimeA",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 30,
                         "category"        : "Advanced",
                         "sortPriority"    : 230
                       },

                       {
                         "guid"            : "$zReaderAgeOutTimeB",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 30,
                         "category"        : "Advanced",
                         "sortPriority"    : 231
                       },

                       {
                         "guid"            : "$zReaderOfflineLinkAgeOut",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 60,
                         "category"        : "Advanced",
                         "sortPriority"    : 240
                       },

                       {
                         "guid"            : "$zReaderAgeInCount",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Advanced",
                         "sortPriority"    : 245
                       },

                       {
                         "guid"            : "$zReaderChannelBiasA",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Advanced",
                         "sortPriority"    : 250
                       },

                       {
                         "guid"            : "$zReaderChannelBiasB",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Advanced",
                         "sortPriority"    : 251
                       },

                       {
                         "guid"            : "$zReaderReportTriggers",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 253
                       },

                       {
                         "guid"            : "$zReaderJoinChannels",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 254
                       },

                       {
                         "guid"            : "$zReaderMergeChannels",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 255
                       },

                       {
                         "guid"            : "$zReaderIgnoreLowConfInitialBeacons",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 257
                       },

                       {
                         "guid"            : "$zReaderPFIMask",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 260
                       },

                       {
                         "guid"            : "$zReaderPFIValue",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 265
                       },

                       {
                         "guid"            : "$zReaderPCHGIGNMask",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Advanced",
                         "sortPriority"    : 270
                       },

                       {
                         "guid"            : "$zReaderPartCount",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 1,
                         "category"        : "Reader Partitioning",
                         "sortPriority"    : 400
                       },

                       {
                         "guid"            : "$zReaderPartIndex",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Reader Partitioning",
                         "sortPriority"    : 410
                       },

                       {
                         "guid"            : "$zReaderPartRotateTime",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Reader Partitioning",
                         "sortPriority"    : 420
                       },

                       {
                         "guid"            : "$zReaderNoiseThreshold",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : -80,
                         "category"        : "Diagnostics",
                         "sortPriority"    : 500
                       },

                       {
                         "guid"            : "$zReaderEvtPerSecThreshold",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 0,
                         "category"        : "Diagnostics",
                         "sortPriority"    : 510
                       },

                       /* Serial Port */
                       /* Baud isn't needed for M240 - configured on device {
                         "guid"            : "$zReaderSerialBaud",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Serial Port",
                         "sortPriority"    : 600,
                         "value"           : 115200
                       }, */

                       {
                         "guid"            : "$zReaderSerialDriver",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Serial Port",
                         "sortPriority"    : 610,
                         "value"           : "bridge"
                       },

                       {
                         "guid"            : "$zReaderSupportedTagTypes",
                         "required"        : false,
                         "isStatic"        : true,
                         "value"           : [ "$zTagGroup_mantis04A",
                                               "$zTagGroup_mantis04B", "$zTagGroup_mantis04C",
                                               "$zTagGroup_mantis04D", "$zTagGroup_mantis04E",
                                               "$zTagGroup_mantis04F", "$zTagGroup_mantis04H",
                                               "$zTagGroup_mantis04I", "$zTagGroup_mantis04J",
                                               "$zTagGroup_mantis04L", "$zTagGroup_mantis04M",
                                               "$zTagGroup_mantis04N", "$zTagGroup_mantis04O",
                                               "$zTagGroup_mantis04P", "$zTagGroup_mantis04Q",
                                               "$zTagGroup_mantis04R", "$zTagGroup_mantis04S", 
                                               "$zTagGroup_mantis04T", "$zTagGroup_mantis04U",
                                               "$zTagGroup_mantis04V", "$zTagGroup_mantis04W",
                                               "$zTagGroup_mantis04X", "$zTagGroup_mantis05A",
                                               "$zTagGroup_mantis05B", "$zTagGroup_mantis05C",
                                               "$zTagGroup_mantis06A"
                                            ]
                       },

                       {
                         "guid"            : "$zReaderTagLinkType",
                         "required"        : false,
                         "isStatic"        : true,
                         "value"           : "$zTagLinkWithSSI"
                       },

                       {
                         "guid"            : "$zReaderChannels",
                         "required"        : false
                       },

                       {
                         "guid"            : "$zReaderState",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 700
                       },

                       {
                         "guid"            : "$zReaderNoiseA",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 701
                       },

                       {
                         "guid"            : "$zReaderNoiseB",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 702
                       },

                       {
                         "guid"            : "$zReaderEvtPerSecA",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 703
                       },

                       {
                         "guid"            : "$zReaderEvtPerSecB",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 704
                       },

                       {
                         "guid"            : "$zReaderTagCapacityUsed",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 705
                       },

                       {
                         "guid"            : "$zReaderRemoteAddr",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 706
                       },

                       {
                         "guid"            : "$zGPSStatus",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 707
                       },

                       {
                         "guid"            : "$zReaderStartupTS",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 708
                       },

                       {
                         "guid"            : "$zReaderEncryptedConnection",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 709
                       },

                       {
                         "guid"            : "$zReaderFirmwareVersion",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 710
                       }
                     ]
  }
]
