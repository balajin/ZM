[
  /* Entity type for reader */
  {
     "class"       : "entity_type",
     "guid"        : "$zReaderSPM",
     "name"        : "SPM Server",
     "parent"      : "$tReader",
     "deletable"   : false,
     "description" : "Driver for API interface to SPM Server.",
     "attributes"  : [
                       {
                         "guid"            : "$zReaderType",
                         "required"        : true,
                         "isStatic"        : true,
                         "value"           : "SPM"
                       },

                       {
                         "guid"            : "$zReaderGroups",
                         "required"        : false,
                         "isStatic"        : false,
                         "category"        : "Basic Information",
                         "sortPriority"    : 2
                       },

                       {
                         "guid"            : "$aHostname",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "localhost",
                         "category"        : "Network Settings",
                         "sortPriority"    : 20
                       },

                       {
                         "guid"            : "$aPortNum",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : 80,
                         "category"        : "Network Settings",
                         "sortPriority"    : 21
                       },

                       {
                         "guid"            : "$zReaderUserID",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Authentication",
                         "sortPriority"    : 100
                       },

                       {
                         "guid"            : "$zReaderPassword",
                         "required"        : false,
                         "isStatic"        : false,
                         "value"           : "",
                         "category"        : "Authentication",
                         "sortPriority"    : 110
                       },

                       {
                         "guid"            : "$zReaderSPMCDUPeriod",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 60,
                         "category"        : "Data Refresh Rates",
                         "sortPriority"    : 200
                       },
                       {
                         "guid"            : "$zReaderSPMCatalogPeriod",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 600,
                         "category"        : "Data Refresh Rates",
                         "sortPriority"    : 210
                       },

                       {
                         "guid"            : "$zReaderOfflineLinkAgeOut",
                         "required"        : true,
                         "isStatic"        : false,
                         "value"           : 60,
                         "category"        : "Advanced",
                         "sortPriority"    : 240
                       },

                       {
                         "guid"            : "$zReaderSupportedTagTypes",
                         "required"        : false,
                         "isStatic"        : true,
                         "value"           : [ "$zTagGroup_SPMCDU" ]
                       },

                       {
                         "guid"            : "$zReaderTagLinkType",
                         "required"        : false,
                         "isStatic"        : true,
                         "value"           : "$zTagLink"
                       },

                       {
                         "guid"            : "$zReaderState",
                         "required"        : false,
                         "category"        : "Status",
                         "sortPriority"    : 700
                       }
                     ]
  }
]
