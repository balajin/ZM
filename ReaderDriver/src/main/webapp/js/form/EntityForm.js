/**
 * @class Z.form.EntityForm
 * A simple form which displays controls for an entity. Does not support
 * typerefs.
 * @cfg {R.form.FieldFactory} factory The factory for creating fields
 * @cfg {String/R.ET} et The entity type or entity type GUID
 * @cfg {Boolean} editable False to display a form with readonly fields
 * @cfg {Boolean} status True to also show status attributes
 */
Z.form.EntityForm = Ext.extend(R.form.FormPanel, {
    /**
     * @cfg {Boolean} editable False to display this form with readonly
     * fields (defaults to true)
     */
    editable: true

    ,constructor : function(config) {
        var editable   = config.editable === false ? false : true;
        var items      = [];
        var et         = R.ET.get(config.et);
        var etas       = et.getETAs();
        var labelWidth = config.labelWidth || this.calcLabelWidth(etas, editable);
        var factory    = config.factory || R.form.FieldFactory.DefaultFactory;
        var i, ac, eta, fieldset, controller;

        // controllers currently used by the form sorted by sortPriority
        // key = AC guid
        this.controllers = new Ext.util.MixedCollection();

        // the typerefs currently referenced in the form
        // key = AC guid
        // value = selected ET for the typeref
        this.typerefs = new Ext.util.MixedCollection();
        this.typerefs.add('type', et);

        // needed by createController
        this.editable = config.editable;
        this.factory = factory;
        this.status = config.status;

        // used when a typeref changes value
        this.values = {};

        etas.sort(R.compare('sortPriority'));

        for (i = 0; i < etas.length; i++) {
            eta = etas[i];
            ac  = R.AC.get(eta.guid);

            controller = this.createController(ac, eta);
            if (controller && controller.field) {
                // add fieldset for category
                if (!fieldset || eta.category != fieldset.title) {
                    if (fieldset && fieldset.items.length > 0) {
                        items.push(fieldset);
                    }
                    fieldset = {
                        autoHeight  : true
                        ,autoWidth  : true
                        ,labelWidth : labelWidth
                        ,title      : eta.category
                        ,xtype      : 'fieldset'
                        ,items      : []
                    };
                }

                fieldset.items.push(controller.field);
            }
        }

        if (fieldset && fieldset.items.length > 0) {
            items.push(fieldset);
        }

        Z.form.EntityForm.superclass.constructor.call(this, Ext.apply({
            items : items
        }, config));
    }

    // private
    ,getFieldLabel : function(ac, required) {
        var label;
        if (ac.guid === R.AC.NAME) {
            return R.ui.required('ID');
        }
        label = ac.getDisplayLabel(required);

        if (!Ext.isEmpty(ac.description)) {
            label = '<span qtip="' + ac.description + '">' + label + '</span>';
        }
        return label;
    }

    /**
     * Calculates the maximum width of all the entity type attribute names.
     * @private
     */
    ,calcLabelWidth : function(etAttrs, editable) {
        var testEl = Ext.get(document.body).createChild({
            id     : Ext.id()
            ,cls   : 'x-form-item'
            ,style : 'position : absolute; visibility : hidden;'
            ,tag   : 'label'
        });

        var width = 0;
        for (var i = 0; i < etAttrs.length; i++) {
            var guid = etAttrs[i].guid;
            var ac   = R.AC.get(guid);
            if (ac) {
                // take into account required field
                testEl.dom.innerHTML = ac.getDisplayLabel();
                width = Math.max(width, testEl.getComputedWidth());
            }
        }
        Ext.destroy(testEl);
        return Math.max(75, width) + 10;
    }

    /**
     * Returns the values of this form as an object.
     * @return {Object} values
     */
    ,getValues : function() {
        var json = {};
        var result = {};
        var ac, guid, zname;

        this.controllers.each(function(c) {
            c.toJson(json);
        });

        // now convert to znames
        for (guid in json) {
            ac = R.AC.get(guid);
            zname = ac['$zName'];

            if (zname) {
                // ExtJS sends multiple parameters with the same key when
                // dealing with arrays, plus this is a 2-dimensional array
                if (ac.type === 'lat-lon-coord-list') {
                    result[zname] = _.map(json[guid], function(item) {
                        return '[' + item[0] + ',' + item[1] + ']';
                    }).join(',');
                }
                else {
                    result[zname] = json[guid];
                }
            }
            else if (guid === '$zGPSSourceSelector') {
                result['positionsrc'] = json[guid];
            }
        }

        // zm does not support the name attribute, instead expose the ID
        if (json[R.AC.NAME]) {
            result['id'] = json[R.AC.NAME];
        }

        return result;
    }

    /**
     * Sets the values on this form.
     * @param {Object} values The values of each field
     */
    ,setValues : function(values) {
        var update = function(controller) {
            var ac = controller.ac;
            var zname = ac['$zName'];

            if (ac.guid === R.AC.NAME) {
                zname = 'id';
            }
            else if (ac.guid === '$zGPSSourceSelector') {
                zname = 'positionsrc';
            }

            if (zname) {
                controller.setValue(values[zname]);
            }

            if (controller.field) {
                // existing users assume passwords have values since we have
                // no idea if they really do, ZM does not send them
                if (values['id'] && ac.type == 'password' && controller.type == 'write') {
                    controller.field.setHasValue();
                }
                else {
                    controller.field.originalValue = controller.field.getValue();
                }
            }
        };

        this.values = Ext.apply({}, values);

        // update typerefs first so we create the proper controllers and can
        // then update the correct fields
        this.controllers.each(function(controller) {
            var ac = controller.ac;

            if (controller.field && ac.isTypeRef()) {
                update.call(this, controller);
                this.onTypeRefSelect(controller.field);
            }
        }, this);

        this.controllers.each(update, this);
    }

    /**
     * @private
     * @param {R.AC} ac
     * @param {Object} eta
     */
    ,createController : function(ac, eta) {
        var type = 'read';
        var controller, field, name, required;

        if (ac && (ac.isConfigurable() || this.status)) {
            if (this.editable && !eta.isStatic && ac.isConfigurable()) {
                type = 'write';
            }

            controller = this.factory.create(ac, type);

            if (controller) {
                required = (type === 'write' && eta.required);

                if (ac.guid === R.AC.NAME) {
                    name = 'id';
                }
                else if (ac.guid === '$zGPSSourceSelector') {
                    name = 'positionsrc';
                }
                else {
                    name = ac['$zName'];
                }

                field = controller.create({
                    guid        : ac.guid
                    ,name       : name
                    ,fieldLabel : this.getFieldLabel(ac, required)
                    ,required   : required
                    ,width      : 200
                });

                if (eta.value !== undefined) {
                    controller.setValue(eta.value);
                }

                if (field && ac.isTypeRef() && ac.inherit_attributes) {
                    field.on('select', this.onTypeRefSelect, this);
                }

                // needed by findControllerIndexBeforeETA
                controller.sortPriority = eta.sortPriority;

                this.controllers.add(ac.guid, controller);
            }
            else {
                // sanity check
                alert('Unknown attribute: ' + ac.guid + ' -> ' + ac.name + ' -> ' + ac.type);
            }
        }
        return controller;
    }

    // private
    ,onTypeRefSelect : function(field, record) {
        var et     = R.ET.get(field.getValue());
        var prevET = this.typerefs.get(field.guid);

        // occurs if user selects same value for the field
        if (this.isTypeRefActive(R.AC.get(field.guid), et)) {
            return;
        }

        // only save the values if this call is from a user selecting a new
        // typeref, not when this function is called during a setValues. If
        // called during setValues, the fields are not populated yet and
        // therefore getValues returns old or default values
        if (record) {
            Ext.apply(this.values, this.getValues());
        }

        if (prevET) {
            // remove the previous ET's attributes
            this.removeET(prevET);
        }

        this.typerefs.add(field.guid, et);

        et.getETAs().each(function(eta) {
            var index  = this.findControllerIndexBeforeETA(eta);
            var bcon   = this.controllers.itemAt(index);
            var bfield = bcon.field;
            var fs     = bfield.ownerCt;
            var ac     = R.AC.get(eta.guid);
            var con    = this.createController(ac, eta);
            var value  = this.values[ac['$zName']] || eta.value;

            if (Ext.isDefined(value)) {
                con.setValue(value);
            }

            if (con.field) {
                fs.insert(index+1, con.field);
            }
        }, this);

        this.doLayout();
    }

    /**
     * Removes the fields of the entity type's attributes.
     * @private
     */
    ,removeET : function(et) {
        et.getETAs().each(function(eta) {
            var con = this.controllers.removeKey(eta.guid);
            if (con && con.field) {
                con.field.ownerCt.remove(con.field, true);
            }
        }, this);
    }

    ,isTypeRefActive : function(ac, et) {
        var typeref = this.typerefs.get(ac.guid);
        return typeref && typeref.guid == et.guid;
    }

    /**
     * Returns the index of the field right before the given ETA's sort priority.
     * @private
     */
    ,findControllerIndexBeforeETA : function(eta) {
        var sortPriority = eta.sortPriority || 0;
        var controllers = this.controllers;
        var i, controller, result;

        for (i = 0; i < controllers.length; i++) {
            controller = controllers.itemAt(i);

            if (controller.sortPriority < sortPriority) {
                result = i;
            }
            else {
                break;
            }
        }
        return result;
    }
});
