/**
 * @class Z.form.ListField
 * @extends R.form.StringListField
 * A field which allows a user to enter a list of tags.
 */
Z.form.TagListField = Ext.extend(R.form.StringListField, {

    showEditButton : true

    ,constructor : function(config) {
        var showEditButton = Ext.value(config.showEditButton, this.showEditButton);

        if (!config.actions) {
            config.actions = [ this.createAddAction() ];
            if (showEditButton) {
                config.actions.push(this.createEditAction());
            }
            config.actions.push(this.createDeleteAction());
        }
        Z.form.TagListField.superclass.constructor.call(this, config);
    }

    // override
    ,setValue : function(value) {
        if (typeof value === 'string') {
            value = value.split(',');
        }
        Z.form.TagListField.superclass.setValue.call(this, value);
    }

    // override
    ,onAddAction : function() {
        this.onEditAction();
    }

    // override
    ,onEditAction : function(record) {
        var value = record && record.get('value');

        var field = Z.createTagSearchField({
            fieldLabel : 'Tag ID'
            ,value     : value
        });
        var win = new R.ui.Dialog({
            height      : 120
            ,labelAlign : 'top'
            ,layout     : 'form'
            ,bodyStyle  : 'padding: 5px'
            ,title      : this.fieldLabel
            ,width      : 350
            ,items      : field
        });
        win.on('ok', function() {
            if (value) {
                this.editString(value, field.getValue());
            }
            else {
                this.addString(field.getValue());
            }
        }, this);
        win.show();
    }
});
Ext.reg('z-taglistfield', Z.form.TagListField);
