Z.Navigation = Ext.extend(R.Navigation, {

    constructor : function() {
        Z.Navigation.superclass.constructor.call(this);

        this.configurationTreeRoot = new Ext.tree.TreeNode();
        this.summaryTreeRoot = new Ext.tree.TreeNode();
    }

    ,createUI : function(config) {
        var items = [];

        this.configurationTree = new Ext.tree.TreePanel({
            cls          : 'nav-panel'
            ,iconCls     : 'configuration-category-icon'
            ,region      : 'center'
            ,root        : this.configurationTreeRoot
            ,rootVisible : false
            ,title       : 'Configuration'
        });
        this.configurationTree.getSelectionModel().on('beforeselect', this.onBeforeSelect, this);
        this.summaryTree = new Ext.tree.TreePanel({
            cls          : 'nav-panel'
            ,height      : 175
            ,iconCls     : 'summary-views-category-icon'
            ,region      : 'north'
            ,root        : this.summaryTreeRoot
            ,rootVisible : false
            ,title       : 'Status'
        });
        this.summaryTree.getSelectionModel().on('beforeselect', this.onBeforeSelect, this);

        items.push(this.summaryTree);
        if (this.configurationTreeRoot.childNodes.length > 0) {
            items.push(this.configurationTree);
        }
        else {
            this.summaryTree.region = 'center';
        }

        this.ui = new Ext.Panel(Ext.apply({
            frame   : true
            ,header : true
            ,layout : 'border'
            ,items  : items
        }, config));
        return this.ui;
    }

    /**
     * Adds the given module to the navigation UI.
     */
    ,add : function(module) {
        var root = this.summaryTreeRoot;
        if (module.category == 'configuration') {
            root = this.configurationTreeRoot;
        }

        root.appendChild(new Ext.tree.AsyncTreeNode({
            text     : module.label()
            ,mid     : module.id
            ,iconCls : module.id + '-small-icon'
            ,leaf    : true
        }));
    }

    ,getSelectedModuleID : function() {
        var node = this.configurationTree.getSelectionModel().getSelectedNode();
        if (!node) {
            node = this.summaryTree.getSelectionModel().getSelectedNode();
        }
        if (node) {
            return node.attributes.mid;
        }
    }

    /**
     * Selects the given module. Does not fire a selectionchange event.
     */
    ,select : function(module, params, proceed) {
        var tree = (module.category == 'configuration' ? this.configurationTree : this.summaryTree);
        var sm   = tree.getSelectionModel();

        // unselect node in the other tree
        if (tree === this.configurationTree) {
            this.summaryTree.getSelectionModel().clearSelections();
        }
        else {
            this.configurationTree.getSelectionModel().clearSelections();
        }

        sm.un('beforeselect', this.onBeforeSelect, this);
        sm.select(tree.getRootNode().findChild('mid', module.id));
        sm.on('beforeselect', this.onBeforeSelect, this);

        proceed();
    }

    // private
    ,getSelectionModel : function(module) {
        if (module.category === 'configuration') {
            return this.configurationTree.getSelectionModel();
        }
        return this.summaryTree.getSelectionModel();
    }

    // private
    ,onBeforeSelect : function(sm, node) {
        this.fireEvent('selectionchange', node.attributes.mid);
        return false;
    }
});

