/**
 * @class Z.grid.TagGridPanel
 * @extends R.grid.GridPanel
 * GridPanel which displays tag data.
 */
Z.grid.TagGridPanel = Ext.extend(R.grid.GridPanel, {

    // private - these are not displayed by the grid (except ID which is added
    // separately)
    znamesToIgnore : R.toMap([
                             'confidencebyrule', 'id', 'irlocator', 'parenttag',
                             'subindex', 'taggroup', 'tagtype'
                             ])

    ,constructor : function(config) {
        var fields = [ 'tagid' ];
        var store = new Z.data.TagUpdatesStore({
            baseParams : {
                _objtype : 'tag'
            }
            ,fields : config.fields || fields
        });
        store.on('load', this.onStoreLoad, this);

        // ensure paging toolbar updates its count information
        var handler = function(store) {
            this.pagingToolbar.onLoad(store, null, {});
        };
        store.on('add', handler, this);
        store.on('remove', handler, this);

        var tbar = config.tbar || [];

        this.loadText = new Ext.Toolbar.TextItem('Tag count updated: ');

        tbar.push(new Ext.Toolbar.Fill());
        tbar.push(this.loadText);

        Z.grid.TagGridPanel.superclass.constructor.call(this, Ext.apply(config, {
            autoScroll  : true
            ,columns    : config.columns || fields.map(this.createColumn)
            ,showPaging : true
            ,showPageSize : true
            ,stripeRows : true
            ,store      : store
            ,tbar       : tbar
            ,plugins    : [
                new R.grid.RowToolTipPlugin({
                    onTipRender  : this.onTipRender.createDelegate(this)
                    ,onTipUpdate : this.onTipUpdate
                })
                ,new R.grid.GridActions({
                    items : [
                        this.createViewAction()
                       ,this.createViewByLocationAction()
                       ,this.createViewChildrenAction()
                       ,this.createViewParentAction()
                    ]
                })
            ]
        }));
    }

    // private
    ,createViewAction : function() {
        return new R.ui.SingleAction({
            cmenu    : true
            ,def     : true
            ,text    : 'View Tag'
            ,scope   : this
            ,handler : function() {
                var record = this.getSelectionModel().getSelected();
                R.App.select('tag-find-view', { tag: record.id });
            }
        });
    }

    // private
    ,createViewByLocationAction : function() {
        return new R.ui.SingleAction({
            cmenu    : true
            ,text    : 'View Tags by Location'
            ,scope   : this
            ,handler : function() {
                var record = this.getSelectionModel().getSelected();
                R.App.select('tag-by-location-view', { location: record.data.locationzone });
            }
            ,update : function() {
                var record = this.getSelected();
                var location = record && record.data.locationzone;

                if (location) {
                    this.setText('View Tags by Location: ' + location);
                    this.enable();
                }
                else {
                    this.disable();
                }
            }

        });
    }

    // private
    ,createViewChildrenAction : function() {
        return new R.ui.SingleAction({
            cmenu    : true
            ,text    : 'View Tag\'s Children'
            ,scope   : this
            ,handler : function() {
                var record = this.getSelectionModel().getSelected();
                R.App.select('tag-by-parent-view', { tag: record.id });
            }
        });
    }

    // private
    ,createViewParentAction : function() {
        return new R.ui.SingleAction({
            cmenu    : true
            ,text    : 'View Tag\'s Parent'
            ,scope   : this
            ,handler : function() {
                var record = this.getSelectionModel().getSelected();
                R.App.select('tag-find-view', { tag: record.data.parenttag });
            }
            ,update : function() {
                var count = this.sm.getCount();
                var parent, record;

                if (count === 1) {
                    record = this.sm.getSelected();
                    parent = record.data.parenttag;
                    this.setText(parent ? 'View Tag\'s Parent: ' + parent : 'View Tag\'s Parent');
                }
                this.setDisabled(!parent);
            }
        });
    }

    // private
    ,onTipRender : function(el, response) {
        var json = Ext.decode(response.responseText);
        if (json.__result === 'error') {
            el.dom.innerHTML = 'Tag not found';
        }
        else {
            var id, i, record, type;
            for (id in json) {
                break;
            }
            var tag = new Z.data.Tag(json[id]);
            var records = tag.records();
            var rules = [];
            var links = [];
            var main  = [];

            for (i = 0; i < records.length; i++) {
                record = records[i];
                type   = record.get('type');

                if (type === 'confidencebyrule') {
                    rules.push(record);
                }
                else if (type === 'taglinks') {
                    links.push(record);
                }
                else {
                    main.push(record);
                }
            }
            main.sort(R.compare(function(r) { return r.get('name') }));
            rules.sort(R.compare(function(r) { return r.get('value') }));
            links.sort(R.compare(function(r) { return r.get('value') }));
            // our scope is the Updater renderer
            if (!this.tpl) {
                this.tpl = new Ext.XTemplate(
                    '<table class="tag-tip">',
                    '<tr><th class="{values.cls}" colspan="2">{values.title}</th></tr>',
                    '<tpl for="records">',
                        '<tr><td>{[values.get("name")]}</td><td>{[parent.tag.renderer(values.get("value"), null, values)]}</td></tr>',
                        '</tpl>',
                    '</table>'
                );
                this.tpl.compile();
            }
            // tag sensors
            this.tpl.overwrite(el, {
                cls: 'tag-tip-main'
                    ,records: main
                    ,tag : tag
                    ,title: 'Tag: ' + tag.id
            });
            // rules
            if (rules.length > 0) {
                this.tpl.append(el, {
                    cls: 'tag-tip-extra'
                    ,records: rules.reverse().slice(0, 4)
                    ,tag  : tag
                    ,title: 'Rules (max 4)'
                });
            }
            // tag links
            this.tpl.append(el, {
                cls: 'tag-tip-extra'
                ,records: links.reverse().slice(0, 4)
                ,tag  : tag
                ,title: 'Tag Links (max 4)'
            });
        }
    }

    // private
    ,onTipUpdate : function(record) {
        return {
            method  : 'POST'
            ,url    : R.url('tagprint')
            ,params : {
                tagid0 : record.id
            }
        };
    }

    // private
    ,onStoreLoad : function(store) {
        var znames = {};
        var fields = [];

        // determine which columns to add to the grid
        store.each(function(r) {
            var data = r.data;
            for (var key in data) {
                if (!znames[key] && !this.znamesToIgnore[key]) {
                    znames[key] = true;
                    fields.push(key);
                }
            }
        }, this);
        this.setFields(fields);

        this.loadText.setText('Tag count updated: ' + new Date().format('Y-m-d H:i:s'));
    }

    // private
    ,setFields : function(fields) {
        var store = this.store;

        var columns = fields.map(this.createColumn);
        columns.sort(R.compare('header'));
        columns.unshift({ dataIndex: 'id', header: 'Tag', sortable: true, width: 165 });

        // reconfigure columns
        var rtype = Ext.data.Record.create(columns.map(function(c) {
            return c.dataIndex;
        }));
        store.recordType = rtype;
        store.fields = rtype.prototype.fields;
        this.reconfigure(store, new Ext.grid.ColumnModel(columns));
    }

    // private
    ,createColumn : function(id) {
        return {
            dataIndex : id
            ,header   : Z.data.Tag.label(id)
            ,id       : id
            ,renderer : Z.data.Tag.renderer(id)
            ,sortable : true
        };
    }
});
