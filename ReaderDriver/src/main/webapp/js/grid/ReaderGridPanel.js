/**
 * @class Z.grid.ReaderGridPanel
 * @extends R.grid.GridPanel
 * @cfg {Array} fields The fields to display for each reader.
 */
Z.grid.ReaderGridPanel = Ext.extend(R.grid.GridPanel, {
    constructor : function(config) {
        var fields = config.fields;
        delete config.fields;

        // available columns
        var columns = {
            enabled : {
                header    : 'Enabled'
                ,width    : 75
                ,renderer : R.AC.get(R.AC.ENABLED).renderer()
            }
            ,evtpersecA : 'Event Rate (Channel A)'
            ,evtpersecB : 'Event Rate (Channel B)'
            ,fwavail    : 'Firmware Available'
            ,fwversion  : 'Firmware Version'
            ,hostname   : 'Hostname'
            ,id         : 'Reader ID'
            ,label      : 'Label'
            ,noiseA     : 'Noise (Channel A)'
            ,noiseB     : 'Noise (Channel B)'
            ,port       : {
                header : 'Port'
                ,width : 75
            }
            ,state      : {
                header    : 'Status'
                ,width    : 160
                ,renderer : function(value, p, record) {
                    var progress = record.get('fwupgradeprogress');

                    if (value === 'ACTIVE') {
                        p.attr = 'style="color:green"';
                    }
                    else if (value === 'FIRMWAREUPGRADING') {
                        if (progress) {
                            value += ' (' + progress + '%)';
                        }
                        p.attr = 'style="color:green"';
                    }
                    else {
                        p.attr = 'style="color:red"';
                    }

                    return value;
                }
            }
            ,tagcapused : 'Tag Capacity Used (%)'
            ,remoteaddr : 'Connected Address'
            ,encrypted  : 'Connection Encrypted'
            ,gpsfix     : 'GPS Status'
        };

        Z.grid.ReaderGridPanel.superclass.constructor.call(this, Ext.apply({
            autoLoadStore : false
            ,autoScroll   : true
            ,header       : true
            ,frame        : true
            ,region       : 'center'
            ,showPageSize : true
            ,showPaging   : true
            ,stripeRows   : true
            ,store        : new Z.data.TagUpdatesStore({
                baseParams : {
                    _objtype : 'reader'
                }
                ,fields : fields
                ,remoteSort : false
            })
            ,columns : fields.map(function(f) {
                var col = columns[f];
                if (typeof col === 'string') {
                    col = { header: col };
                }
                return Ext.apply({
                    dataIndex : f
                    ,id       : f
                    ,sortable : true
                }, col);
            })
            ,listeners : {
                rowdblclick : function(grid, row) {
                    var record = grid.store.getAt(row);
                    if (record) {
                        R.App.select('reader-config', { reader: record.id });
                    }
                }
            }
        }, config));

        this.store.on('load', this.onStoreLoad, this);
    }

    // private
    ,onStoreLoad : function() {
        if (!this.task) {
            this.task = new Ext.util.DelayedTask(this.onTask, this);
            this.onTask();
        }
    }

    // private
    ,onTask : function() {
        R.Ajax.request({
            method    : 'GET'
            ,url      : R.url('readerlist')
            ,scope    : this
            ,callback : this.onReaderList
        });
    }

    // private
    ,onReaderList : function(options, success, response) {
        var store = this.store;

        if (!store || !store.data) {
            return; // store was destroyed
        }

        var readers = Ext.decode(response.responseText);
        var fw = R.App.firmware;
        var famversions = fw.fwfamversions;

        for (var key in readers) {
            var record = store.getById(key);
            var reader = readers[key];
            var attrs  = reader.attributes;

            if (attrs && record) {
                var family = attrs.fwfamily;
                var version = attrs.fwversion;
                var newversion = famversions[family];

                record.beginEdit();
                if (!Ext.isEmpty(attrs.fwupgradeprogress)) {
                    record.set('fwupgradeprogress', attrs.fwupgradeprogress);
                }
                record.set('fwavail', this.getAvailableFirmware(version, newversion));
                record.endEdit();
            }
            store.commitChanges();
        }
        this.task.delay(1000);
    }

    // private
    // cv = current version, nv = new version
    ,getAvailableFirmware : function(cv, nv) {
        if (Ext.isEmpty(cv) || Ext.isEmpty(nv)) {
            return '';
        }

        var ca = cv.split('.');
        var na = nv.split('.');
        if (ca.length === 2) {
            ca.push(0);
        }
        if (na.length === 2) {
            na.push(0);
        }

        var c = 0, n = 0, m = 1;
        for (var i = ca.length-1; i >= 0; i--) {
            c += parseInt(ca[i], 10) * m;
            n += parseInt(na[i], 10) * m;
            m *= 1000;
        }
        return c >= n ? 'Up to date' : nv;
    }

    // override
    ,onDestroy : function() {
        Z.grid.ReaderGridPanel.superclass.onDestroy.call(this);

        if (this.request) {
            Ext.Ajax.abort(this.request);
            delete this.request;
        }
        if (this.task) {
            this.task.cancel();
            delete this.task;
        }
    }
});
