
// Redirect 401's back to the login screen
Ext.Ajax.on('requestexception', function(conn, response, options) {
    if (response.status == 401) {
        location.replace('/rfcode_zonemgr/login.jsp');
    }
    return false;
});


// main entry point for Zone Manager UI
Ext.onReady(function() {

    // change password
    R.App.onPasswordClick = function() {
        var win = new R.ui.Dialog({
            bodyStyle : 'padding: 10px 5px'
            ,height   : 140
            ,labelWidth : 95
            ,layout   : 'form'
            ,title    : 'Change Password'
            ,width    : 280
            ,items    : [
                {
                    fieldLabel : 'New Password'
                    ,name      : 'password'
                    ,xtype     : 'r-password'
                }
            ]
        });
        win.on('ok', function() {
            var field = win.find('name', 'password')[0];
            var passwd = field.getValue();

            if (passwd.length > 0 && field.validate()) {
                R.Ajax.request({
                    method  : 'POST'
                    ,url    : R.url('passwd')
                    ,params : {
                        id : R.App.user.id
                        ,password : passwd
                    }
                    ,callback : function(options, success) {
                        if (success) {
                            Ext.Msg.alert('Status', 'Password changed');
                        }
                        else {
                            Ext.Msg.alert('Error', 'Error saving password');
                        }
                        win.close();
                    }
                });
            }
            return false;
        });
        win.show();
    };

    R.Ajax.queue({
        decode    : true
        ,method   : 'GET'
        ,requests : [ 'datamodel.json', R.url('version'), R.url('whoami'), R.url('fwlibversion') ]
        ,success  : function(result) {
            var datamodel = result[0];
            var version   = result[1];
            var whoami    = result[2];
            var firmware  = result[3];
            var id, user;

            // determine user/role
            var user = { id: 'N/A', role: 'admin' };
            for (id in whoami) {
                user = { id: id, role: whoami[id].attributes.role };
                break;
            }

            // initialize R.AC and R.ET
            Ext.each(datamodel, function(objs) {
                var i, obj, cls;

                for (i = 0; i < objs.length; i++) {
                    obj = objs[i];
                    cls = obj['class'];

                    if (cls === 'attribute_type') {
                        R.AC.add(obj);
                    }
                    else if (cls === 'entity_type') {
                        R.ET.add(obj);
                    }
                }
            });

            // create app factory
            var factory = new R.AppFactory({
                // override
                navigation : function() {
                    return new Z.Navigation();
                }

                // override
                ,status : function(config) {
                    var left = [];
                    var right = [];

                    if (user.id !== 'N/A') {
                        left.push({ text: 'User: ' + user.id, onclick: 'R.App.onPasswordClick();' });
                        right.push({ text: 'Logout', href: 'zonemgr/logout' });
                        right.push('-');
                    }

                    right.push({ text: 'About', onclick: 'R.ui.showAboutWindow();' });

                    this.status = new R.Status({ left: left, right: right });
                    return this.status.createUI(config);
                }
            });

            R.App.start({
                baseURL   : 'rfcode_zonemgr/zonemgr/api/'
                ,factory  : factory
                ,firmware : firmware // firmware-config module updates this
                                     // value if a user uploads a new firmware
                                     // package
                ,tagtypes : []
                ,title    : version.product
                ,user     : user
                ,version  : version
            });
        }
    });
});

// Add override for role checking in module
Ext.override(R.Module, {
    isAllowed : function() {
        if (this.roles) {
            return this.roles.some(function(r) {
                return R.App.user.role === r;
            });
        }
        return true;
    }
});
