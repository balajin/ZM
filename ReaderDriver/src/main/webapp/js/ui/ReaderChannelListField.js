Z.ui.ReaderChannelListField = Ext.extend(Ext.form.TriggerField,  {

    defaultAutoCreate : { tag: 'textarea', rows: 4 }

    ,editable : false

    ,triggerClass : 'x-form-win-trigger'

    ,getValue : function() {
        if (this.value) {
            if (typeof this.value == 'string') {
                return this.value;
            }
            else {
                var list = '';
                for (var i = 0; i < this.value.length;i++) {
                    list += this.value[i] + ',';
                }
                if (list.length > 0) {
                    list = list.substring(0, list.length - 1);//truncate trailing comma
                }
                return list;
            }
        }
        return [];
    }

    ,setValue : function(v) {
        if (this.rendered) {
            if (typeof v == 'string') {
                this.setRawValue(v.split(',').join('\n'));
            }
            else {
                var channel, labels;
                if (this.channels) {
                    labels = [];
                    for (var i = 0; i < v.length; i++) {
                        labels.push(v[i]);
                    }
                }
                else {
                    labels = v;
                }
                labels.sort(R.compare());
                this.setRawValue(labels.join('\n'));
            }
        }
        this.value = v;
    }

    ,onRender : function() {
        Z.ui.ReaderChannelListField.superclass.onRender.apply(this, arguments);

        var that = this;
        var callback = function() {
            that.setValue(that.getValue());
            if (that.triggered) {
                Ext.getBody().unmask();
                that.showWindow();
            }
        };
        this.doLoad(callback);
    }

    /**
     * Returns the label to use for the given reader channel.
     */
    ,toLabel : function(reader) {
        return channel.id;
    }

    /**
     * Loads all of our reader channels.
     * @param {Function} callback the function to call once loading is complete
     */
    ,doLoad : function(callback) {
        // load our entities
        var channels = [];

        Ext.Ajax.request({
            method   : 'GET'
            ,url     : R.url('readerlist')
            ,scope   : this
            ,success : function(response) {
                this.readers = Ext.decode(response.responseText);
                this.channels = [];
                for (var i in this.readers) {
                    for (var j = 0; j < this.readers[i].attributes.channels.length; j++) {
                        this.channels.push(this.readers[i].attributes.channels[j]);
                    }
                }
            }
        });
    }

    ,onTriggerClick : function() {
        if (this.disabled) {
            return;
        }

        // in case entities have not loaded yet
        if (!this.entities) {
//            this.triggered = true;
//            Ext.getBody().mask('Loading...', 'x-mask-loading');
//            return;
        }

        this.showWindow();
    }

    ,showWindow : function() {
        var field = new Z.ui.SelectReaderChannelPanel({
            channels  : this.channels
            ,id       : 'selector'
            ,readers  : this.readers
            ,selected : this.getValue()
        });
        var window = new R.ui.Dialog({
            title       : this.fieldLabel
            ,width      : 530
            ,height     : 324
            ,minWidth   : 300
            ,minHeight  : 200
            ,layout     : 'fit'
            ,items      : field
            ,listeners : {
                'ok' : {
                    scope : this
                    ,fn   : function() {
                        this.setValue(field.getValue());
                    }
                }
            }
        });
        window.show();
    }
});

