/**
 * @class Z.ui.SelectReaderChannelPanel
 * @extends Ext.Panel
 * Panel which allows an administrator to select one or more reader channels.
 * @cfg {Array} channels A list of all the reader channels
 * @cfg {Array} readers A list of all the readers
 * @cfg {Array} selected A list of selected reader channel GUIDs
 */
Z.ui.SelectReaderChannelPanel = Ext.extend(Ext.Panel, {
    constructor : function(config) {
        var rangerGUID = 'LOCAL_RANGER';
        var selected = config.selected;
        if (typeof selected == 'string') {
            selected = selected.split(',');
        }
        var selectedNodes = [];

        delete config.selected;

        if (Ext.isArray(selected) && selected.length > 0) {
            R.each(selected, function(channelID) {
                var channel = R.find(config.channels, function(channel) {
                    return channel === channelID;
                });

                if (channel) {
                    selectedNodes.push(new Ext.tree.TreeNode({
                        id       : channel
                        ,text    : channel
                        ,iconCls : 'tree-node-no-icon'
                    }));
                }
            });
        }

        this.readers = config.readers;
        this.channels = config.channels;

        var root = new Ext.tree.TreeNode();
        this.updateAvailableTree(root, selected);

        this.field = new R.form.DualTreeField({
            fromTitle : 'Available Reader Channels'
            ,root     : root
            ,toTitle  : 'Selected Reader Channels'
            ,value    : selected
            ,readers : this.readers
            ,onMoveRight : this.onMoveRight
            ,onMoveLeft : this.onMoveLeft
            ,setValue : function(v) {
                 R.form.DualTreeField.prototype.setValue.apply(this, arguments);

                 var children = this.ltTree.getRootNode().childNodes;
                 for (var i = children.length-1; i >= 0; i--) {
                     var node = children[i];
                     if (node.childNodes.length === 0) {
                         node.remove();
                         node.destroy();
                     }
                 }
            }
        });
        config = Ext.apply({
            layout : 'fit'
            ,items : this.field
        }, config);
        Z.ui.SelectReaderChannelPanel.superclass.constructor.call(this, config);
    }

    // private
    ,onMoveRight : function(nodes) {
        var handle = function(node) {
            if (Ext.isEmpty(rtTree.getNodeById(node.id))) {
                rtRoot.appendChild(node);
            }
            else {
                node.remove();
                node.destroy();
            }
        };

        var rtTree = this.rtTree;
        var rtRoot = rtTree.getRootNode();
        var children, node, parent;

        for (var i = nodes.length-1; i >= 0; i--) {
            node     = nodes[i];
            children = node.childNodes;

            if (children.length > 0) {
                // node is a reader
                parent = node;
                for (var j = children.length-1; j >= 0; j--) {
                    handle(children[j]);
                }
            }
            else {
                // node is a channel
                parent = node.parentNode;
                handle(node);
            }

            if (parent.childNodes.length === 0) {
                parent.remove();
                parent.destroy();
            }
        }
    }

    ,onMoveLeft : function(nodes) {
        var ltTree = this.ltTree;
        var readers = this.readers;
        var node, id, parent, reader;

        for (var i = nodes.length-1; i >= 0; i--) {
            node = nodes[i];
            id = node.attributes.id.substring(0, node.attributes.id.indexOf('_channel_'));

            // find its reader node
            parent = ltTree.getNodeById(id);
            if (!parent) {
                reader = readers[id];
                if (reader) {
                    parent = new Ext.tree.TreeNode({
                        id: id, text: id, iconCls : 'tree-node-no-icon'
                    });
                    ltTree.getRootNode().appendChild(parent);
                }
            }

            if (parent) {
                parent.appendChild(node);
            }
        }
    }

    /**
     * @private
     */
    ,updateAvailableTree : function(root, selected) {
        var readers  = [];
        for (var i in this.readers) {
            readers.push(this.readers[i]);
        }
        var channels = {}; // key = reader GUID, value = channel array

        R.each(this.channels, function(channel) {
            var reader;
            for (var r in this.readers) {
                if (channel.indexOf(r) == 0) {
                    reader = this.readers[r];
                    break;
                }
            }
            var list = channels[reader.id];

            if (!list) {
                list = [];
                channels[reader.id] = list;
            }
            list.push(channel);
        }, this);
        root.beginUpdate();
        R.each(readers, function(reader) {
            var parent;

            var list = channels[reader.id];

            R.each(list || [], function(channel) {
                if (!parent) {
                    parent = new Ext.tree.TreeNode({
                        id: reader.id, text: reader.id, iconCls : 'tree-node-no-icon'
                    });
                }
                parent.appendChild(new Ext.tree.TreeNode({
                    id       : channel
                    ,text    : channel
                    ,iconCls : 'tree-node-no-icon'
                }));
            });

            if (parent) {
                root.appendChild(parent);
            }
        });
        root.endUpdate();
    }

    /**
     * Return an array of the selected reader channel GUIDs.
     */
    ,getValue : function() {
        var list = '';
        R.each(this.field.rtTree.getRootNode().childNodes, function(node) {
            list += node.attributes.text + ',';
        });
        if (list.length > 0) {
            list = list.substring(0, list.length - 1);//truncate trailing comma
        }
        return list;
    }
});
