/**
 * Store which performs tag updates to retrieve its contents.
 */
Z.data.TagUpdatesStore = Ext.extend(Ext.data.Store, {

    autoDestroy : true

    ,remoteSort : true

    ,constructor : function(config) {
        Z.data.TagUpdatesStore.superclass.constructor.call(this, config);
        this.handlers = {
            'create'      : this.onCreateEvent
            ,'update'     : this.onUpdateEvent
            ,'delete'     : this.onDeleteEvent
            ,'init-start' : this.onInitStartEvent
            ,'init-end'   : this.onInitEndEvent
        };
        this.recordType = Ext.data.Record.create(this.fields);
        this.fields = this.recordType.prototype.fields;
    }

    ,load : function(options) {
        options = options || {};

        if (this.fireEvent('beforeload', this, options) !== false){
            this.storeOptions(options);

            var params = options.params || {};
            var start = params.start || 0;

            if (this.isTag(this.baseParams._objtype) || start === 0) {
                this.onLoad(Ext.apply(params || {}, this.baseParams));
            }
            else {
                // other object types we page ourselves
                this.loadRecords({
                    records: this._cache.getRange(start, start+params.limit)
                    ,totalRecords: this.totalLength },
                    this.lastOptions, true);
            }
            return true;
        }
        return false;
    }

    // private
    ,onLoad : function(options) {
        var sortInfo = this.sortInfo;
        var objtype  = this.baseParams._objtype;

        options = Ext.apply({
            scope    : this
            ,handler : this.onEvent
        }, options, this.baseParams);

        // only tag supports these other attributes
        if (this.isTag(objtype)) {
            if (!Ext.isEmpty(options.start)) {
                options._sortstart = options.start;
                delete options.start;
            }
            if (!Ext.isEmpty(options.limit)) {
                options._sortrowcnt = options.limit;
                delete options.limit;
            }

            // sort info
            if (sortInfo) {
                options._sortattrib = sortInfo.field;
                if (sortInfo.direction === 'DESC') {
                    options._sortorder = 'dec';
                }
            }
            else {
                options._sortattrib = 'id';
            }
        }
        this.stop();

        this.tagupdates = new Z.data.TagUpdates(options);
        this.tagupdates.start();

        // commit records which have not changed in 10 seconds - this removes
        // the change notification icon from the cell
        this.task = Ext.TaskMgr.start({
            interval : 10000
            ,scope   : this
            ,run     : function() {
                if (this.data) {
                    var now = new Date().getTime();
                    var m = this.modified;
                    var i, ts;
                    for (i = 0; i < m.length; i++) {
                        ts = m[i].get('_ts');
                        if (now - ts > 10000) {
                            m[i].commit();
                        }
                    }
                }
            }
        });
    }

    // private
    ,isTag : function(objtype) {
        return Ext.isEmpty(objtype) || objtype === 'tag';
    }

    // private
    ,afterEdit : function(record) {
        // set timestamp for committing changes
        record.set('_ts', new Date().getTime());
        Z.data.TagUpdatesStore.superclass.afterEdit.call(this, record);
    }

    /**
     * Stops the currently running tag updates, effectively pausing updates to
     * this store. Use "load" to restart the tag updates.
     */
    ,stop : function() {
        if (this.tagupdates) {
            this.tagupdates.stop();
        }
        if (this.task) {
            Ext.TaskMgr.stop(this.task);
            delete this.task;
        }
    }

    // override
    ,destroy : function() {
        if (!this.isDestroyed) {
            this.stop();
            Z.data.TagUpdatesStore.superclass.destroy.apply(this, arguments);
        }
    }

    ,onEvent : function(event) {
        // this.data invalid if store has been destroyed
        if (this.data) {
            var fn = this.handlers[event.eventtype];
            if (fn) {
                fn.call(this, event);
            }
        }
    }

    // override
    ,getById : function(id) {
        if (this._cache) {
            return this._cache.key(id);
        }
        return Z.data.TagUpdatesStore.superclass.getById.call(this, id);
    }

    ,onCreateEvent : function(event) {
        var attrs  = event.object.attributes;
        var record = new this.recordType(Ext.apply(attrs, { id: event.id }), event.id);

        if (this._records) {
            // have not received an init-end event yet so batch the record
            this._records.push(record);
        }
        // not sure why we get create on known tag - need to investigate
        else if (this.getById(record.id)) {
            this.onUpdateEvent(event);
        }
        else {
            var a = [record];

            if (this._cache) {
                this._cache.add(record);
                this.totalLength = this._cache.length;
                if (this.getCount() < this.lastOptions.params.limit) {
                    this.add(a);
                }
            }
            else {
                this.totalLength++;
                this.add(a);
            }
        }
    }

    ,onUpdateEvent : function(event) {
        var attrs  = event.object.attributes;
        var record = this._cache ? this._cache.key(event.id) : this.getById(event.id);

        if (record) {
            record.beginEdit();
            for (var key in attrs) {
                record.set(key, attrs[key]);
            }
            record.endEdit();
        }
    }

    ,onDeleteEvent : function(event) {
        var record = this.getById(event.id);
        if (record) {
            this.totalLength--;
            this.remove(record);
        }

        if (this._cache) {
            var record = this._cache.key(event.id);
            if (record) {
                this._cache.remove(record);
                this.totalLength = this._cache.length;
            }
        }
    }

    ,onInitStartEvent : function(event) {
        // totalLength defined by superclass
        this.totalLength = event.totalcnt;
        this._records = [];
        delete this._cache;
    }

    ,onInitEndEvent : function(event) {
        var params = this.lastOptions.params || {};
        var records = this._records;

        if (!this.isTag(params._objtype) && params.limit > 0) {
            // load only one page into the store's data field
            records = this._records.slice(params.start,params.start+params.limit);
            this._cache = new Ext.util.MixedCollection();
            this._cache.addAll(this._records);
            this.totalLength = this._records.length;
        }
        this.loadRecords({ records: records, totalRecords: this.totalLength },
            this.lastOptions, true);
        delete this._records;
    }

    // override
    ,sortData : function() {
        if (this._cache) {
            // readers are not paged/sorted on the server, so we sort here
            var sortInfo = this.sortInfo;
            var dir = sortInfo.direction || 'ASC';
            var field = sortInfo.field;
            var params = this.lastOptions.params;
            var st = this.fields.get(field).sortType;
            var fn = function(r1, r2){
                var v1 = st(r1.data[field]), v2 = st(r2.data[field]);
                return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
            };
            this._cache.sort(dir, fn);
            this.removeAll();
            this.add(this._cache.getRange(params.start, params.start+params.limit));
        }
        else {
            Z.data.TagUpdatesStore.superclass.sort.apply(this, arguments);
        }
    }
});
