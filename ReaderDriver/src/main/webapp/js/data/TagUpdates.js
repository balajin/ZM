/**
 * @class Z.data.TagUpdates
 * @cfg {String} _objtype (optional) The type of objects to find. If null, then all objects are grabbed.
 * @cfg {String} _sortattrib (optional) The attribute to sort the returned list of tags
 * @cfg {String} _sortorder (optional) "dec" to sort in descending order
 * @cfg {String} _sortstart (optional) The first index (0-based) for a window of tags
 * @cfg {String} _sortrowcnt (optional) The maximum number of tags to return
 * @cfg {String} _sortlocation (optional) The location to further subset the returned tags
 * @cfg {Array} _guids (optional) An array of tag IDs to load
 * @cfg {Function} handler The function which gets the event notification
 * @cfg {Object} scope (optional) The scope in which to execute the handler
 * function. The handler function's "this" context.
 */
Z.data.TagUpdates = function(config) {
    this.handler = config.handler;
    this.scope = config.scope;
    delete config.handler;
    delete config.scope;

    this.params = {};
    Ext.apply(this.params, config);

    this.task = new Ext.util.DelayedTask(this.request, this);
};

Z.data.TagUpdates.prototype = {
    /**
     * Starts the tag updates
     */
    start : function() {
        // create random session ID
        this.params._sid = 'rfc_' + Math.round(Math.random() * 1000000);
        this.running = true;
        this.request(true);
    }

    /**
     * Stops the tag updates
     */
    ,stop : function() {
        if (this.running) {
            this.task.cancel();
            Ext.Ajax.abort(this.requestID);
            this.running = false;
            delete this.requestID;
        }
    }

    /**
     * Stops the tag updates.
     */
    ,destroy : function() {
        this.stop();
        delete this.task;
    }

    // private
    ,onAjaxRequest : function(opts, success, response) {
        // check sid to ensure this request is not from a previously stopped
        // request
        if (this.running && opts._sid == this.params._sid) {
            if (success) {
                var events = Ext.decode(response.responseText);
                Ext.each(events, this.handler, this.scope);

                // delay a bit to allow more events in each update
                this.task.delay(1000);
            }
        }
    }

    // private
    ,request : function(initial) {
        var guids = this.params._guids;
        var params = this.params;

        if (initial || guids) {
            params = Ext.apply({}, params);
            if (guids) {
                params._guids = params._guids.join(',');
            }
            if (initial) {
                params._init = true;
            }
            params._incgps = true;  // Always get GPS info
        }

        this.requestID = Ext.Ajax.request({
            method    : 'GET'
            ,url      : R.url('tagupdates')
            ,params   : Ext.urlEncode(params)
            ,_sid     : this.params._sid
            ,timeout  : 35000
            ,scope    : this
            ,callback : this.onAjaxRequest
        });
    }
};


