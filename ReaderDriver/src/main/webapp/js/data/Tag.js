(function() {
    // key = zname, value = Mapper
    var _mappers = {};

    var _Record = Ext.data.Record.create([
        { name : 'name' }, { name : 'value' }
    ]);

    /**
     * @class Z.data.Tag
     * Represents a tag as returned by a tagprint URL.
     */
    Z.data.Tag = function(config) {
        if (config instanceof Ext.data.Record) {
            this.attrs = {};

            for (var key in config.data) {
                this.attrs[key] = config.data[key];
            }
            this.attrs.taglinks = config.data.taglinks;
        }
        else {
            this.attrs = {
                taglinks : config.taglinks
            };
            Ext.apply(this.attrs, config.attributes);
        }
        this.id = config.id;
    };

    Z.data.Tag.prototype = {
        /**
         * Returns the tag values as an array of Ext.data.Record objects
         * @method
         */
        records : function() {
            var recs = [];
            var attrs = this.attrs;
            var i, id, mapper, value;

            for (id in attrs) {
                mapper = getMapper(id);
                value = attrs[id];
                if (mapper && mapper.record) {
                    mapper.record(recs, id, value);
                }
                else if (id !== 'tagid') {
                    recs.push(new _Record({ name: id, value: value }, id));
                }
            }

            // add tag field to records
            for (i = 0; i < recs.length; i++) {
                recs[i].data.tag = this;
            }
            return recs;
        }

        /**
         * Returns the renderer for the given tag record.
         * @method
         */
        ,renderer : function(value, metadata, record) {
            var mapper = getMapper(record.get('type'));
            if (mapper && mapper.renderer) {
                return mapper.renderer.apply(mapper, arguments);
            }
            return Ext.isEmpty(value) ? '' : value;
        }
    };

    /**
     * Returns the label for the given tag attribute.
     * @method
     * @static
     * @param {String} attr The tag attribute
     */
    Z.data.Tag.label = function(attr) {
        var mapper = getMapper(attr);
        return mapper ? mapper.name : attr;
    };

    /**
     * Returns a new renderer for the given tag attribute.
     * @method
     * @static
     * @param {String} attr The tag attribute
     */
    Z.data.Tag.renderer = function(attr) {
        var mapper = getMapper(attr);
        if (mapper && mapper.renderer) {
            // renderer scope must be mapper
            return mapper.renderer.createDelegate(mapper);
        }
    };

    // Returns a mapper object for the given zname
    var getMapper = function(zname) {
        var m = _mappers[zname];
        if (!m && zname !== 'tagid') {
            var ac = R.AC.getByZName(zname);
            if (ac) {
                m = map(zname, {
                    ac     : ac
                    ,name  : ac.name
                    ,type  : ac.type
                    ,units : ac.units
                });
            }
        }
        return m;
    };

    var push = function(recs, type, id, name, value) {
        recs.push(new _Record({ name: name, value: value, type: type}, id));
    };

    var Mapper = function(config) {
        Ext.apply(this, config);

        if (this.ac) {
            this.renderer = this.ac.renderer();
        }
    };
    Mapper.prototype = {
        record : function(recs, id, value) {
            push(recs, id, id, this.name, value);
        }

        ,renderer : function(value) {
            return Ext.isEmpty(value) ? '' : value;
        }
    };

    var map = function(zname, v) {
        var m = new Mapper(v);
        _mappers[zname] = m;
        return m;
    };

    // renames Tag Location to Location
    map('locationzone', { name: 'Location', type: 'string' });

    // the rest are not in the datamodel
    map('irlocator', { name: 'Room Locator' });
    map('taggroupid', { name: 'Tag Group ID' });
    map('tagtype', { name: 'Tag Type' });

    map('confidencebyrule', {
        record : function(recs, id, value) {
            var rule;
            for (var rule in value) {
                push(recs, 'confidencebyrule', rule, 'Rule: ' + rule, value[rule]);
            }
        }

        ,renderer : function(value) {
            return 'Confidence: ' + value;
        }
    });
    map('taglinks', {
        record : function(recs, id, value) {
            var i, link;
            if (value) {
                for (i = 0; i < value.length; i++) {
                    link = value[i];
                    push(recs, 'taglinks', link.channelid, link.channelid, link.ssi);
                }
            }
        }

        ,renderer : function(value) {
            return 'SSI: ' + value;
        }
    });

    map('tagid', { name: 'Tag' });
})();
