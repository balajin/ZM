R.ModuleManager.register(new R.Module({
    id          : 'tag-by-location-view'
    ,category   : 'summary'
    ,label      : 'Tags by Location'

    // override
    ,onCreateUI : function() {
        this.grid = new Z.grid.TagGridPanel({
            autoLoadStore : false
            ,frame  : true
            ,header : true
            ,region : 'center'
        });
        this.tree = new R.tree.FilterTreePanel({
            frame   : true
            ,header : true
            ,region : 'west'
            ,root   : new Ext.tree.TreeNode()
            ,rootVisible : false
            ,split  : true
            ,width  : 225
        });
        this.tree.getSelectionModel().on('selectionchange', this.onLocationChange, this);

        return new Ext.Panel({
            layout : 'border'
            ,items : [ this.tree, this.grid ]
        });
    }

    // private
    ,onLocationChange : function(sm, node) {
        R.App.select(this.id, { location: node.id });
    }

    // override
    ,refresh : function(params) {
        var location;
        if (params['location']) {
            location = params['location'];
        }

        if (this.tree.getRootNode().hasChildNodes()) {
            this.loadTags(location);
        }
        else {
            R.Ajax.request({
                location  : location
                ,method   : 'GET'
                ,url      : R.url('loclist')
                ,scope    : this
                ,callback : this.callback(this.onAjaxLocList)
            });
        }
    }

    // private
    ,loadTags : function(location) {
        var store = this.grid.store;
        var tree = this.tree;
        var sm = tree.getSelectionModel();
        var node;

        // find and select node in tree
        if (location) {
            node = tree.getNodeById(location);
        }
        if (!node) {
            node = tree.getRootNode().childNodes[0];
        }
        node.ensureVisible();
        sm.un('selectionchange', this.onLocationChange, this);
        sm.select(node);
        sm.on('selectionchange', this.onLocationChange, this);

        if (node.id === '__unknown') {
            store.baseParams._sortlocation = '';
        }
        else {
            store.baseParams._sortlocation = node.id;
        }
        store.load();
    }

    // private
    ,onAjaxLocList : function(options, success, response) {
        var tree = this.tree;
        var root = tree.getRootNode();
        var locations;

        if (success) {
            locations = Ext.decode(response.responseText);
            if (locations.__result === 'error') {
                locations = null;
            }
        }
        if (locations) {
            // locations
            var id, loc;
            for (id in locations) {
                this.appendChild(locations, id);
            }
            root.appendChild(new Ext.tree.TreeNode({
                iconCls : 'location-config-small-icon'
                ,id     : '__unknown'
                ,text   : 'Unknown'
            }));
            this.loadTags(options.location);
        }
        this.fireEvent('refresh', this);
    }

    // private
    ,appendChild : function(locations, id) {
        var tree  = this.tree;
        var loc, node, parent;

        if (id) {
            loc = locations[id];
            node = tree.getNodeById(id);
            if (!node) {
                parent = this.appendChild(locations, loc.attributes.parent);
                parent.appendChild(new Ext.tree.TreeNode({
                    iconCls : 'location-config-small-icon'
                    ,id     : id
                    ,text   : loc.id
                }));
            }
            return node;
        }
        return tree.getRootNode();
    }
}));
