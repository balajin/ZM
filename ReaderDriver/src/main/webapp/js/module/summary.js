R.ModuleManager.register(new R.Module({
    id          : 'summary-view'
    ,category   : 'summary'
    ,label      : 'Summary'

    // override
    ,onCreateUI : function() {
         this.summary = new Ext.grid.GridPanel({
             columns : [
                 { dataIndex: 'name', header: 'Name', sortable: true, width: 200 }
                 ,{ dataIndex: 'value', header: 'Value', sortable: true, width: 300 }
             ]
             ,frame  : true
             ,header : true
             ,region : 'center'
             ,store  : new Ext.data.SimpleStore({
                 fields  : [ 'name', 'value' ]
             })
             ,stripeRows : true
         });

         if (!Z.isEventView()) {
             this.groups = new Ext.grid.GridPanel({
                 columns : [
                     { dataIndex: 'name', header: 'Tag Group', sortable: true, width: 200 }
                     ,{ dataIndex: 'value', header: 'Number of Tags', sortable: true, width: 300 }
                 ]
                 ,frame  : true
                 ,header : true
                 ,height : 300
                 ,region : 'south'
                 ,split  : true
                 ,store  : new Ext.data.SimpleStore({
                     fields  : [ 'name', 'value' ]
                 })
                 ,stripeRows : true
             });
         }

         var items = [ this.summary ];
         if (this.groups) {
             items.push(this.groups);
         }

         return new Ext.Panel({
             layout : 'border'
             ,items : items
         });
    }

    // override
    ,refresh : function() {
        var requests = [
            R.url('version'), R.url('loclist'), R.url('tagcount')
        ];

        if (!Z.isEventView()) {
            requests.push(
                R.url('readerlist')
                ,R.url('rulelist')
                ,R.url('grouplist')
            );
            if (Z.isAdmin()) {
                requests.push(R.url('licenselist'));
            }
        }

        R.Ajax.queue({
            decode    : true
            ,method   : 'GET'
            ,requests : requests
            ,scope    : this
            ,success  : this.callback(function(result) {
                var version  = result[0];
                var loclist = result[1];
                var tagcount = result[2];
                var readerlist = result[3];
                var rulelist = result[4];
                var grouplist = result[5];
                var licenselist = result[6];
                var store    = this.summary.store;
                var records  = [];

                // returns the number of keys in the object
                var count = function(obj) {
                    var result = 0;
                    for (var p in obj) {
                        result++;
                    }
                    return result;
                };

                records.push(new Ext.data.Record({ name: 'Version', value: version.release }));
                records.push(new Ext.data.Record({ name: 'Build', value: version.build }));
                records.push(new Ext.data.Record({ name: 'Number of Tags', value: tagcount.count }));
                records.push(new Ext.data.Record({ name: 'Number of Locations', value: count(loclist) }));

                if (!Z.isEventView()) {
                    records.push(new Ext.data.Record({ name: 'Number of Readers', value: count(readerlist) }));
                    records.push(new Ext.data.Record({ name: 'Number of Location Rules', value: count(rulelist) }));

                    // licenses
                    if (Z.isAdmin()) {
                        var avail = 'Unlimited';
                        if (licenselist.limit !== 'unlimited') {
                            avail = licenselist.limit - licenselist.used;
                        }
                        records.push(new Ext.data.Record({
                            name: 'Licenses Available'
                            ,value: avail
                        }));
                        records.push(new Ext.data.Record({
                            name: 'Licenses Used'
                            ,value: licenselist.used
                        }));
                    }

                    // tag groups
                    this.loadTagGroups(grouplist);
                }
                store.removeAll();
                store.add(records);

                this.fireEvent('refresh', this);
            })
        });
    }

    // private
    ,loadTagGroups : function(groups) {
        var store = new Ext.data.Store({
            data : groups
            ,reader : new R.data.ObjectReader({
                fields : [ 'groupcode' ]
            })
        });

        var requests = [];

        // get count per group
        store.each(function(r) {
            requests.push({
                url     : R.url('tagcountbygroup')
                ,params : {
                    groupid0 : r.id
                }
            });
        });

        if (requests.length > 0) {
            R.Ajax.queue({
                decode    : true
                ,method   : 'GET'
                ,requests : requests
                ,scope    : this
                ,success  : this.callback(function(result) {
                    var i;
                    var records = [];
                    for (i = 0; i < store.getCount(); i++) {
                        records.push(new Ext.data.Record({
                            name   : store.getAt(i).get('groupcode')
                            ,value : result[i].count
                        }));
                    }
                    this.groups.store.add(records);
                })
            });
        }
    }
}));
