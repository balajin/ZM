R.ModuleManager.register(new Z.module.EntityMasterDetailModule({
    id           : 'sensordef-config'
    ,category    : 'configuration'
    ,label       : 'Sensor Definitions'
    ,roles       : [ 'admin', 'configview' ]
    ,itemIconCls : 'taggroup-config-small-icon'
    ,deleteURL   : R.url('sensordefdelete')
    ,listURL     : R.url('sensordeflist')
    ,nameField   : 'label'
    ,etGuid      : '$tSensorDefinition'
    ,etaTypeGuid : '$zSensorDefinitionTypeValue'

    // override
    ,createDetailComponent : function() {
        var factory = new R.form.FieldFactory();
        factory.registerGuid('$zSensorDefinitionTags', 'write', {
            onCreate : function(config) {
                return new Z.form.TagListField(Ext.apply(config, {
                    height : 400
                    ,showEditButton : false
                }));
            }
        });
        this.factory = factory;

        return Z.module.EntityMasterDetailModule.prototype.createDetailComponent.call(this);
    }

    // override
    ,initStore : function() {
        var store = Z.module.EntityMasterDetailModule.prototype.initStore.call(this);
        store.on('update', function(store, record) {
            var node = this.tree.getNodeById(record.id);
            var text;
            if (node) {
                text = record.get('label') || record.id;
                if (text !== node.text) {
                    node.setText(text);
                }
            }
        }, this);
        return store;
    }

    // override
    ,onEdit : function(record) {
        Z.module.EntityMasterDetailModule.prototype.onEdit.apply(this, arguments);

        R.Ajax.request({
            method   : 'GET'
            ,url     : R.url('tagsensordeflist')
            ,params  : { id: record.get('id') }
            ,waitMsg : 'Loading tags...'
            ,waitMsgDelay : 200
            ,scope   : this
            ,success : this.callback(this.onTagSensorListLoad)
        });
    }

    ,onTagSensorListLoad : function(response) {
        var selected = this.tree.getSelectionModel().getSelectedNode();
        var field = this.formpanel.form.findField('tags');
        var json = Ext.decode(response.responseText);
        var id = _.first(_.keys(json));
        var tags = json[id].attributes.tags;

        // ensure this request was for the currently selected sensor definition
        if (!selected || id != selected.attributes.id) {
            return;
        }

        field.setValue(tags);
        field.originalValue = tags;

        this.tags = tags;
    }

    // override
    ,onSubmit : function() {
        var form = this.formpanel.form;
        var exists = form.findField('id').disabled;
        var data = this.formpanel.getValues();

        if (!exists) {
            // convert from AM to ZM namespace
            eta = this.getSelectedEntityType().getETA(this.etaTypeGuid);
            var ac = R.AC.get(this.etaTypeGuid);
            if (ac && ac.type === 'enum') {
                data['type'] = ac.values[eta.value];
            }
            else {
                data['type'] = eta.value;
            }
        }

        if (!data['label']) {
            data['label'] = data.id;
        }

        // tags are saved in onActionComplete
        delete data.tags;

        return {
            data      : data
            ,method   : 'POST'
            ,url      : exists ? R.url('sensordefset') : R.url('sensordefcreate')
        };
    }

    // override
    ,onActionComplete : function(form, action) {
        Z.module.EntityMasterDetailModule.prototype.onActionComplete.apply(this, arguments);

        var id = action.options.data.id;
        var previous = this.tags;
        var current = this.formpanel.form.findField('tags').getValue();
        var add = _.without(current, previous);
        var remove = _.without(previous, current);
        var requests = [];


        if (add.length > 0) {
            requests.push({
                url : R.url('tagsensordefset')
                ,params : {
                    id   : id
                    ,tag : add.join(',')
                }
            });
        }

        if (remove.length > 0) {
            requests.push({
                url : R.url('tagsensordefset')
                ,params : {
                    id   : 'null'
                    ,tag : remove.join(',')
                }
            });
        }
        if (requests.length > 0) {
            R.Ajax.queue({
                decode    : true
                ,method   : 'POST'
                ,requests : requests
                ,scope    : this
                ,failure  : this.onTagSensorDefSetFailure
            });
        }
    }

    // private
    ,onTagSensorDefSetFailure : function(result) {
        var i, msg;

        for (i = 0; i < result.length; i++) {
            if (result[i].__result === 'error') {
                msg = result[i].__msg;
            }
        }

        if (msg) {
            this.formpanel.form.markInvalid([{ msg: msg } ]);
        }
    }
}));
