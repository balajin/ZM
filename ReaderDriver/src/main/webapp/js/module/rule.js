R.ModuleManager.register(new R.Module({
    id          : 'rule-view'
    ,category   : 'summary'
    ,label      : 'Rules'
    ,roles      : [ 'admin', 'configview' ]

    // override
    ,onCreateUI : function() {
        return new R.grid.GridPanel({
            autoLoadStore : false
            ,autoExpandColumn : 'condition'
            ,autoScroll : true
            ,header     : true
            ,frame      : true
            ,showPaging : true
            ,showPageSize : true
            ,stripeRows : true
            ,store      : new Ext.data.Store({
                proxy : new R.data.PagingProxy(new Ext.data.HttpProxy({
                    method : 'GET'
                    ,url   : R.url('rulelist')
                }))
                ,reader : new R.data.ObjectReader({
                    fields : [ 'id', 'type', 'target_location', 'enabled' ]
                    ,transform : this.transform.createDelegate(this)
                })
                ,remoteSort : true
            })
            ,columns  : [
                { dataIndex: 'id', header: 'Reader ID', sortable: true, width: 200 }
                ,{ dataIndex: 'type', header: 'Type', sortable: true }
                ,{ dataIndex: 'target_location', header: 'Target', sortable: true }
                ,{
                    dataIndex : 'enabled'
                    ,header   : 'Enabled'
                    ,sortable : true
                    ,renderer : R.AC.get(R.AC.ENABLED).renderer()
                }
                ,{ id: 'condition', dataIndex: 'condition', header: 'Condition', sortable: true }
            ]
            ,listeners : {
                rowdblclick : function(grid, row) {
                    var record = grid.store.getAt(row);
                    if (record) {
                        R.App.select('location-config', { rule: record.id });
                    }
                }
            }
        });
    }

    // override
    ,refresh : function(params) {
        var store = this.component.store;
        store.on('load', function() {
            this.fireEvent('refresh', this);
        }, this, { single: true });
        store.load();
    }

    // override
    ,onCleanup : function() {
        this.component.store.destroy();
    }

    // private
    ,transform : function(attrs) {
        var type = attrs.type;
        var tr   = this[type];
        if (tr) {
            attrs.condition = tr.call(this, attrs);
        }
        return attrs;
    }

    // private
    ,AverageSSIRule : function(attrs) {
        return 'Minimum SSI: ' + attrs.ssiminimum + " High Confidence SSI: " + attrs.ssihighconf;
    }

    // private
    ,IRLocatorRule : function(attrs) {
        return 'Room Locator ID: ' + attrs.irlocatorcode;
    }

    // private
    ,PortalSSIRule : function(attrs) {
        return 'Minimum SSI: ' + attrs.ssiminimum + " High Confidence SSI: " + attrs.ssihighconf;
    }

    // private
    ,RefTagSSIRule : function(attrs) {
        return 'Reference Tags: ' + attrs.reftags;
    }

    // private
    ,SimpleRefSSIRule : function(attrs) {
        return 'Reference Tags: ' + attrs.reftags;
    }

    // private
    ,SimpleSSIRule : function(attrs) {
        return 'Minimum SSI: ' + attrs.ssiminimum + " High Confidence SSI: " + attrs.ssihighconf;
    }

    // private
    ,GPSRefRule : function(attrs) {
        return 'Coordinate List: ' + attrs.coordlist + " Match Confidence: " + attrs.matchingconf;
    }

}));
