R.ModuleManager.register(new Z.module.EntityMasterDetailModule({
    id           : 'reader-config'
    ,category    : 'configuration'
    ,label       : 'Readers'
    ,roles       : [ 'admin', 'configview' ]
    ,itemIconCls : 'reader-config-small-icon'

    ,deleteURL : R.url('readerdelete')
    ,listURL   : R.url('readerlist')
    ,nameField : 'label'

    ,etGuid : R.ET.READER
    ,etaTypeGuid : '$zReaderType'

    // override
    ,refresh : function(params) {
        if (params['reader']) {
            // this.selection defined in ListMasterDetailModule
            this.selection = params['reader'];
        }

        var store = this.store;
        store.on('load', this.callback(function() {
            // load our tag groups
            var groups  = new Ext.data.Store({
                reader : new R.data.ObjectReader({
                    fields : [ 'id', 'groupcode' ]
                })
                ,url : R.url('grouplist')
            });
            groups.on('load', this.callback(this.onGroupsLoad), this);
            groups.load();

            // track reader state
            this.tagupdates = new Z.data.TagUpdates({
                _objtype : 'reader'
                ,handler : this.onTagUpdate
                ,scope   : this
            });
            this.tagupdates.start();
        }), this, { single: true });
        store.on('update', function(store, record) {
            var node = this.tree.getNodeById(record.id);
            var text;
            if (node) {
                text = this.getItemText(record.id, record.data);
                node.setText(text.text);
            }
        }, this);
        store.load();
    }

    // private
    ,onTagUpdate : function(event) {
        var record = this.store.getById(event.id);
        if (record) {
            var v = this.getItemText(event.id, event.object.attributes);
            var node = this.setText(record.id, v.text, v.iconCls);

            if (node) {
                node.ui[v.cls ? 'addClass' : 'removeClass']('r-tree-node-disabled');
            }
        }
    }

    // override
    ,onCreateItem : function(config) {
        var v = this.getItemText(config.record.id, config.record.data);
        config.text = v.text;
        config.iconCls = v.iconCls;
        config.cls = v.cls;
        return config;
    }

    // private
    ,getItemText : function(id, data) {
        var state = data.state;
        var enabled = ('true' === data.enabled) || data.enabled === true;
        var online = ('ACTIVE' === state
                      || 'NOISEDETECTED' === state
                      || 'HIGHTRAFFIC' === state
                      || 'DISCONNECTING' === state); // occurs when disabled, don't go offline first
        var text = data.label || id, iconCls, cls;

        if (!enabled) {
            text += ' (' + tr('disabled') + ')';
            cls = 'r-tree-node-disabled';
        }
        else if (!online) {
            text += ' (' + tr('offline') + ')';
            iconCls = 'r-offline-tree-node-icon';
        }

        return { text: text, iconCls: iconCls, cls: cls };
    }

    // private
    ,onGroupsLoad : function(groups) {
        var factory = new R.form.FieldFactory();
        var module = this;
        var defNumField = {
            onCreate : function(config) {
                var ac = this.ac;

                if (ac.units) {
                    config.plugins = new R.form.FieldRightLabelPlugin({
                        label : ac.units.label()
                    });
                }
                config.minValue = ac.getMinConstraint();
                config.maxValue = ac.getMaxConstraint();

                return new R.form.DefaultNumberField(config);
            }
        };
        factory.registerGuid('$zReaderGroups', 'write', {
            groups : groups

            ,onCreate : function(config) {
                return new R.form.MutualChoiceField(Ext.apply({
                    choice1Label : 'All Tag Groups'
                    ,choice2Label : this.ac.name
                    ,choice2Field : new R.form.ListField({
                        displayField : 'groupcode'
                        ,store       : groups
                        ,valueField  : 'id'
                    })
                }, config));
            }

            ,toJson : function(json) {
                var value = this.field.getValue() || this.groups.collect('id');
                if (value.length > 0) {
                    json['$zReaderGroups'] = value.join(',');
                }
            }

            ,setValue : function(value) {
                if (typeof value === 'string') {
                    value = value.split(',');
                }
                if (Ext.isArray(value) && value.length === this.groups.getCount()) {
                    // null corresponds to all choice
                    value = null;
                }
                this.field.setValue(value);
            }
        });
        factory.registerGuid('$zReaderSerialBaud', 'write', {
            onCreate : function(config) {
                return new Ext.form.ComboBox(Ext.apply({
                    mode : 'local'
                    ,store : [ 128000, 115200, 57600, 38400, 19200, 14400, 9600 ]
                    ,triggerAction : 'all'
                }, config));
            }
        });
        factory.registerGuid('$zReaderGPSMinPeriod', 'write', {
            onCreate : function(config) {
                var ac = this.ac;
                if (ac.units) {
                    config.plugins = new R.form.FieldRightLabelPlugin({
                        label : ac.units.label()
                    });
                }
                config.allowDecimals = false;
                return new R.form.DefaultNumberField(config);
            }
        });
        factory.registerGuid('$zReaderGPSMinHoriz', 'write', defNumField);
        factory.registerGuid('$zReaderGPSMinVert', 'write', defNumField);

        factory.registerGuid('$zGPSSourceSelector', 'write', {
            onCreate : function(config) {
                var ac = this.ac;
                var typeref = R.ET.get(ac.constraints.typeref);
                var et = module.getSelectedEntityType();
                var data;

                // M200 supports only none and static GPS
                if (et.guid === '$zReaderM200') {
                    data = [ R.ET.get('$zNoGPSSource'), R.ET.get('$zStaticGPSSource') ];
                }
                else {
                    data = typeref.leaves();
                }

                return new Ext.form.ComboBox(Ext.applyIf({
                    clearable     : config.required
                    ,displayField : 'name'
                    ,editable     : false
                    ,mode         : 'local'
                    ,name         : 'positionsrc'
                    ,store        : new Ext.data.JsonStore({
                        data      : data
                        ,fields   : [ 'guid', 'name' ]
                        ,sortInfo : { field: 'name' }
                    })
                    ,triggerAction : 'all'
                    ,valueField    : 'guid'
                }, config));
            }

            ,toJson : function(params) {
                var value = 'NONE';
                switch (this.field.getValue()) {
                case '$zReaderGPSSource': value = 'GPS'; break;
                case '$zStaticGPSSource': value = 'STATIC'; break;
                }
                params[this.ac.guid] = value;
            }

            ,setValue : function(value) {
                switch (value) {
                case 'NONE': value = '$zNoGPSSource'; break;
                case 'GPS': value = '$zReaderGPSSource'; break;
                case 'STATIC': value = '$zStaticGPSSource'; break;
                }
                this.field.setValue(value);
            }
        });

        factory.registerGuid('$zGPSSourceSelector', 'read', {
            onCreate : function(config) {
                return new R.form.DisplayField(Ext.apply({
                    toText : function(value) {
                        var et = R.ET.get(value);
                        return et ? et.name : value;
                    }
                }, config));
            }
            ,setValue : function(value) {
                switch (value) {
                case 'NONE': value = '$zNoGPSSource'; break;
                case 'GPS': value = '$zReaderGPSSource'; break;
                case 'STATIC': value = '$zStaticGPSSource'; break;
                }
                this.field.setValue(value);
            }
        });


        this.factory = factory;
        this.fireEvent('refresh', this);
    }

    // override
    ,onSubmit : function() {
        var form   = this.formpanel.form;
        var exists = form.findField('id').disabled;
        var data   = this.formpanel.getValues();
        var eta, key, value;

        if (exists) {
            // fix blank password and don't send unmodified fields
            form.items.each(function(f) {
                var name = f.name;
                var ac;

                if (f.isDirty()) {
                    ac = R.AC.getByZName(name);

                    if (ac.type === 'password' && data[name] === null) {
                        // Ext.urlEncode encodes undefined as an empty value
                        data[name] = undefined;
                    }
                }
                else if (name !== 'id') {
                    delete data[name];
                }
            }, this);
        }
        else {
            // convert from AM to ZM namespace
            eta = this.getSelectedEntityType().getETA(this.etaTypeGuid);
            data['type'] = eta.value;
        }

        if (!data['label']) {
            data['label'] = data.id;
        }

        for (key in data) {
            if (data[key] === null) {
                delete data[key];
            }
        }
        return {
            data    : data
            ,method : 'POST'
            ,url    : exists ? R.url('readerset') : R.url('readercreate')
        };
    }
}));
