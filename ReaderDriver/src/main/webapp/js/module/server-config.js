/**
 * Module for server configuration properties (upconnect, premises, etc)
 */
R.ModuleManager.register(new R.Module({
    id          : 'server-config'
    ,category   : 'configuration'
    ,label      : 'Server Options'
    ,roles      : [ 'admin', 'configview' ]

    // override
    ,onCreateUI : function() {
        var fs = function(title, items) {
            return {
                autoHeight : true
                ,autoWidth : true
                ,defaults  : {
                    disabled : !Z.isAdmin()
                    ,width   : 180
                    ,xtype   : 'textfield'
                }
                ,items : items
                ,title : title
                ,xtype : 'fieldset'
            };
        };

        var plugins;

        if (Z.isAdmin()) {
            plugins = new R.form.SaveFormPlugin({ url: R.url('configset') });
        }

        var form = new R.form.FormPanel({
            border       : false
            ,bodyStyle   : 'padding: 0px 5px'
            ,buttonAlign : 'right'
            ,defaults    : {
                msgTarget : 'side'
                ,width    : 150
            }
            ,defaultType : 'fieldset'
            ,frame       : true
            ,header      : true
            ,labelWidth  : 125
            ,width       : 325
            ,xtype       : 'form'
            ,items       : [
                fs('Up Connect', this.createUpConnect())
                ,fs('Event Store & Forward', this.createStoreAndForward())
                ,fs('Advanced', this.createAdvanced())
                ,fs('IBM&#174; WebSphere&#174; Sensor Events', this.createPremises())
            ]
            ,plugins : plugins
        });
        return form;
    }

    // override
    ,refresh : function() {
        Ext.Ajax.request({
            method   : 'GET'
            ,url     : R.url('configlist')
            ,scope   : this
            ,success : this.callback(this.onConfigList)
        });
    }

    // private
    ,onConfigList : function(response) {
        var reader = new R.data.ObjectReader({
            fields : [ 'maxhisttime', 'tagexplocbump' ]
        });
        var records = reader.read(response).records;
        var config = {};

        Ext.each(records, function(r) {
            config[r.id] = r.get('value');
        });

        config.upconnect_confirm = config.upconnect_password;

        if (Ext.isEmpty(config.tagexplocbump)) {
            config.tagexplocbump = '0.0000001';
        }
        if (Ext.isEmpty(config.upconnect_port)) {
            config.upconnect_port = 6580;
        }
        if (Ext.isEmpty(config.upconnect_protocol)) {
            config.upconnect_protocol = 'HTTP';
        }
        if (Ext.isEmpty(config.maxhisttime)) {
            config.maxhisttime = 0;
        }
        this.component.form.setValues(config);
        this.fireEvent('refresh', this);
    }

    // private
    ,createAdvanced : function() {
        return [
            {
                fieldLabel : 'Expected Location Bias'
                ,name      : 'tagexplocbump'
            },
            {
                fieldLabel : 'Send events for bound tags only'
                ,name      : 'listen_asset_tags_only'
                ,xtype     : 'checkbox'
            }
        ];
    }

    // private
    ,createPremises : function() {
        return [
            {
                fieldLabel : 'Sensor Events Integration Enabled'
                ,name      : 'premises_enabled'
                ,width     : 'auto'
                ,xtype     : 'checkbox'
            }
            ,{
                fieldLabel : 'Sensor Events Server Hostname'
                ,name      : 'premises_hostname'
            }
            ,{
                allowNegative     : false
                ,decimalPrecision : 0
                ,fieldLabel       : 'Sensor Events Server Port'
                ,name             : 'premises_port'
                ,xtype      : 'uxspinner'
                ,strategy  : {
                    xtype     : 'number'
                    ,minValue : 0
                    ,maxValue : 65535
                }
            }
            ,{
                fieldLabel : 'Sensor Events Source ID'
                ,name      : 'premises_source'
            }
            ,{
                editable    : false
                ,fieldLabel : 'Sensor Events API Version'
                ,name       : 'premises_version'
                ,listWidth  : 200
                ,store      : [
                    ['6', 'IBM Premises v6.x' ]
                    ,['7', 'IBM Sensor Events v7']
                ]
                ,triggerAction : 'all'
                ,xtype         : 'combo'
            }
            ,{
                fieldLabel : 'Send Location As Source ID'
                ,name      : 'wse_zone_for_source'
                ,width     : 'auto'
                ,xtype     : 'checkbox'
            }
            ,{
                fieldLabel : 'Send Tag Events As Notification'
                ,name      : 'wse_tag_notification'
                ,width     : 'auto'
                ,xtype     : 'checkbox'
            }
            ,{
                allowNegative     : false
                ,decimalPrecision : 0
                ,fieldLabel       : 'Maximum Location Report Interval (seconds)'
                ,name             : 'wse_max_loc_interval'
                ,xtype      : 'uxspinner'
                ,strategy  : {
                    xtype     : 'number'
                    ,minValue : 0
                    ,maxValue : 9999999
                }
            }
        ];
    }

    // private
    ,createStoreAndForward : function() {
        return [
            {
                allowNegative     : false
                ,decimalPrecision : 0
                ,fieldLabel       : 'Maximum Event Storage'
                ,name             : 'maxhisttime'
                ,xtype      : 'uxspinner'
                ,strategy  : {
                    xtype     : 'number'
                    ,minValue : 0
                }
                ,plugins : new R.form.FieldRightLabelPlugin({
                    label: 'Hours'
                })
            }
        ];
    }

    // private
    ,createUpConnect : function() {
        function passwordValidator(f) {
            var p = Ext.getCmp('upconnect_password');
            var c = Ext.getCmp('upconnect_confirm');
            var msg;

            if (p && c && p.getValue() !== c.getValue()) {
                msg = 'Passwords do not match';
                p.markInvalid(msg);
                c.markInvalid(msg);
                return msg;
            }
            else if (p && c) {
                p.clearInvalid();
                c.clearInvalid();
            }
            return true;
        };

        return [
            {
                fieldLabel : 'Enabled'
                ,name      : 'upconnect_enabled'
                ,width     : 'auto'
                ,xtype     : 'checkbox'
            }
            ,{
                fieldLabel : 'Hostname'
                ,name      : 'upconnect_hostname'
            }
            ,{
                allowNegative     : false
                ,decimalPrecision : 0
                ,fieldLabel       : 'Port'
                ,name             : 'upconnect_port'
                ,xtype      : 'uxspinner'
                ,strategy  : {
                    xtype     : 'number'
                    ,minValue : 0
                }
            }
            ,{
                editable    : false
                ,fieldLabel : 'SSL'
                ,name       : 'upconnect_protocol'
                ,listWidth  : 200
                ,store      : [
                    ['HTTP', 'Do not use SSL' ]
                    ,['HTTPS', 'SSL - No Verification']
                    ,['HTTPS_VERIFY', 'SSL Verify Certificate & Hostname']
                ]
                ,triggerAction : 'all'
                ,xtype         : 'combo'
            }
            ,{
                fieldLabel : 'Zone Manager ID'
                ,name      : 'upconnect_zmid'
            }
            ,{
                fieldLabel : 'User ID'
                ,name      : 'upconnect_userid'
            }
            ,{
                fieldLabel : 'Password'
                ,inputType : 'password'
                ,name      : 'upconnect_password'
                ,id        : 'upconnect_password'
                ,validator : passwordValidator
            }
            ,{
                fieldLabel : 'Confirm Password'
                ,dataIndex : 'upconnect_confirm' // dataIndex prevents showing in form.getValues()
                ,inputType : 'password'
                ,id        : 'upconnect_confirm'
                ,validator : passwordValidator
            }
        ];
    }
}));
