R.ModuleManager.register(new R.Module({
    id          : 'tag-gps'
    ,category   : 'summary'
    ,label      : 'Tags by GPS'

    // override
    ,onCreateUI : function() {
        var tbar = [];

        this.groupsCombo = new Ext.form.ComboBox({
            displayField : 'groupcode'
            ,editable    : false
            ,mode        : 'local'
            ,store       : new Ext.data.Store({
                reader : new R.data.ObjectReader({
                    fields : [ 'id', 'groupcode' ]
                })
                ,sortInfo : { field: 'groupcode' }
            })
            ,triggerAction : 'all'
            ,valueField    : 'id'
            ,width         : 200
        });
        this.groupsCombo.on('select', this.onComboChange, this);

        if (!Z.isEventView()) {
            tbar.push('Tag Group: ', this.groupsCombo);
        }

        this.gpsCombo = new Ext.form.ComboBox({
            displayField : 'name'
            ,editable    : false
            ,mode        : 'local'
            ,name        : 'gps'
            ,store       : new Ext.data.Store({
                reader : new R.data.ObjectReader({
                    fields : [ 'id', 'name' ]
                })
                ,sortInfo : { field: 'name' }
            })
            ,triggerAction : 'all'
            ,valueField    : 'id'
            ,width         : 200
        });
        this.gpsCombo.on('select', this.onComboChange, this);

        tbar.push(' ', 'GPS: ', this.gpsCombo);

        var columns = [
            { dataIndex: 'id',            header: 'Tag' }
            ,{ dataIndex: 'locationzone', header: 'Location' }
            ,{ dataIndex: 'coords',       header: 'Latitude/Longitude' }
        ];

        var grid = new Z.grid.TagGridPanel({
            autoLoadStore : false
            ,columns : columns.map(function(c) {
                c.width = 165;
                c.sortable = true;
                return c;
            })
            ,fields : [ 'id', 'locationzone', 'coords' ]
            ,frame  : true
            ,header : true
            ,tbar   : tbar
            ,setFields : Ext.emptyFn // we set our own fields
        });
        grid.store.on('load', function(store) {
            if (this.gps) {
                // update values with existing gps data
                store.each(function(r) {
                    var gpsid = r.get('gpsid');
                    var gps, coords;
                    if (gpsid) {
                        gps = this.gps[gpsid];
                        if (gps && gps.coords) {
                            r.set('coords', R.AC.prototype.latLonValueToString(gps.coords[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(gps.coords[1], "E", "W"));
                        }
                    }
                }, this);
            }
            else {
                this.gpsTagUpdates = new Z.data.TagUpdates({
                    _objtype : 'gps'
                    ,scope   : this
                    ,handler : this.callback(this.onGpsUpdate)
                });
                this.gpsTagUpdates.start();
                this.gps = {};
            }
        }, this);
        return grid;
    }

    // override
    ,onCleanup : function() {
        Ext.destroy(this.gpsTagUpdates);
        delete this.gps;
        delete this.gpsTagUpdates;
    }

    // private
    ,onComboChange : function() {
        var store = this.component.store;

        this.gpsId = this.gpsCombo.getValue();
        this.groupId = this.groupsCombo.getValue();

        // group
        if (!this.groupId || this.groupId === '__all') {
            delete store.baseParams._groupid;
        }
        else {
            store.baseParams._groupid = this.groupId;
        }

        // gps
        if (this.gpsId === '__all') {
            delete store.baseParams._attribute;
            delete store.baseParams._value;
        }
        else {
            store.baseParams._attribute = 'gpsid';
            store.baseParams._value = this.gpsId;
        }

        store.load();
    }

    // override
    ,refresh : function(params) {
        if (params['group']) {
            this.groupId = params['group'];
        }
        var requests = [ R.url('gpslist') ];
        if (!Z.isEventView()) {
            requests.push(R.url('grouplist2'));
        }

        R.Ajax.queue({
            decode    : true
            ,method   : 'GET'
            ,requests : requests
            ,scope    : this
            ,success  : this.callback(this.onAjaxRefresh)
        });
    }

    // private
    ,onAjaxRefresh : function(response) {
        var store = this.component.store;
        var all = new Ext.data.Record({id: '__all', groupcode: 'All', name: 'All' }, '__all');
        var gps = response[0];
        var groups = response[1];
        var id, attrs, record;

        // groups
        if (groups) {
            if (groups.__result != 'error') {
                // subgroups do not have a groupcode
                for (id in groups) {
                    attrs = groups[id].attributes;
                    attrs.groupcode = attrs.groupcode || id;
                }
                this.groupsCombo.store.loadData(groups);
            }
            this.groupsCombo.store.insert(0, [ all ]);
            record = this.groupsCombo.store.getById(this.groupId || '__all');
            this.groupsCombo.setValue(record.id);
        }

        // gps
        if (gps.__result != 'error') {
            for (id in gps) {
                gps[id].attributes.name = id;
            }
            this.gpsCombo.store.loadData(gps);
        }
        this.gpsCombo.store.insert(0, [ all ]);
        record = this.gpsCombo.store.getById(this.gpsId || '__all');
        this.gpsCombo.setValue(record.id);

        this.onComboChange();
        this.fireEvent('refresh', this);
    }

    // private
    ,onGpsUpdate : function(event) {
        var obj = event.object;
        var attrs = obj && obj.attributes;
        var store = this.component.store;

        if (attrs) {
            if (!this.gps[obj.id]) {
                this.gps[obj.id] = {};
            }
            Ext.apply(this.gps[obj.id], attrs);

            if (attrs.latlon) {

                // update each tag in store with coords
                store.each(function(r) {
                    if (r.get('gpsid') == obj.id) {
                        r.set('coords', R.AC.prototype.latLonValueToString(attrs.latlon[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(attrs.latlon[1], "E", "W"));
                    }
                });
                this.gps[obj.id].coords = attrs.latlon;
            }
        }
    }
}));
