/**
 * @class Z.module.EntityMasterDetailModule
 * @extends R.module.ListMasterDetailModule
 * A module which allows a user to add/edit/delete a list of entities.
 * @cfg {String} etGuid The entity type GUID
 * @cfg {Srting} etaTypeGuid The GUID of the entity type attribute which
 * corresponds to the entity type (eg. $zRuleType). This allows conversion
 * to/from ZM and AM namespaces.
 * @cfg {String} listURL The URL used to load the list of records
 */
Z.module.EntityMasterDetailModule = Ext.extend(R.module.ListMasterDetailModule, {
    nameField : 'id'

    // override
    ,onCleanup : function() {
        delete this.et;
        delete this.factory;
        delete this.saveFormPlugin;
        Z.module.EntityMasterDetailModule.superclass.onCleanup.call(this);
    }

    // override
    ,createStore : function() {
        return new Ext.data.Store({
            reader : new R.data.ObjectReader({
                fields : [ 'id' ]
            })
            ,url : this.listURL
        });
    }

    // override
    ,createDetailComponent : function() {
        this.et = R.ET.get(this.etGuid);

        this.saveFormPlugin = new R.form.SaveFormPlugin({
            scope : this
            ,onSubmit : this.onSubmit
        });

        if (this.et.children.length > 0) {
            this.etCombo = new Ext.form.ComboBox({
                disabled    : !Z.isAdmin()
                ,displayField : 'name'
                ,editable   : false
                ,fieldLabel : this.et.name
                ,mode       : 'local'
                ,name       : 'type'
                ,id         : 'type'
                ,store      : new Ext.data.Store({
                    data : this.et.leaves()
                    ,reader : new Ext.data.JsonReader({
                        fields : [ 'guid', 'name' ]
                    })
                })
                ,triggerAction : 'all'
                ,valueField    : 'guid'
                ,width         : 300
            });
            this.etCombo.on('select', this.onEntityTypeChange, this);

            return new Ext.Panel({
                autoScroll : true
                ,bodyStyle : 'padding: 0px 5px'
                ,frame  : true
                ,header : true
                ,layout : 'form'
                ,items  : [
                    {
                        cls     : 'ie-fieldset-pad-top'
                        ,height : 'auto'
                        ,xtype  : 'fieldset'
                        ,items  : this.etCombo
                    }
                ]
                ,plugins : this.saveFormPlugin
            });
        }
        this.formpanel = new Z.form.EntityForm({
            bodyStyle : 'padding: 0px 5px'
            ,editable : Z.isAdmin()
            ,et       : this.et
            ,factory  : this.factory
            ,frame    : true
            ,header   : true
            ,plugins  : this.saveFormPlugin
        });
        this.formpanel.form.on('actioncomplete', this.onActionComplete, this);
        return this.formpanel;
    }

    // override
    ,createActions : function() {
        if (Z.isAdmin()) {
            return Z.module.EntityMasterDetailModule.superclass.createActions.call(this);
        }
    }

    /**
     * @return {R.ET} The selected entity type
     */
    ,getSelectedEntityType : function() {
        return R.ET.get(this.etCombo.getValue());
    }

    // override
    ,onEdit : function(record) {
        var children = this.et.leaves();

        if (children.length > 0) {
            // eg. type = mantis04A or M200
            var type = record.get('type');

            // find the correct entity type child for the record
            var child = R.find(children, function(child) {
                var eta = child.getETA(this.etaTypeGuid);
                var ac  = R.AC.get(this.etaTypeGuid);
                if (ac && ac.type === 'enum') {
                    for (var i = 0; i < ac.values.length; i++) {
                        if (ac.values[i] === type) {
                            return eta.value === i;
                        }
                    }
                    return false;
                }
                else {
                    return eta.value === type;
                }
            }, this);

            if (child) {
                this.etCombo.setValue(child.guid);
                this.etCombo.disable();
                this.onEntityTypeChange(this.etCombo);

                // fix $zRuleTargetLocation
                if (record.data['target_location']) {
                    record.data['locid'] = record.data['target_location'];
                }
            }
            else {
                throw 'Unable to find type[' + type + '] in record[' + record.id + ']';
            }
        }
        this.formpanel.form.findField('id').disable();
        this.formpanel.setValues(record.data);
    }

    // private
    ,onEntityTypeChange : function(combo) {
        var value = combo.getValue();
        var et    = R.ET.get(value);

        if (this.formpanel) {
            this.detail.remove(this.formpanel);
        }
        this.formpanel = new Z.form.EntityForm({
            editable : Z.isAdmin()
            ,et      : et
            ,factory : this.factory
        });
        this.formpanel.form.on('actioncomplete', this.onActionComplete, this);
        this.saveFormPlugin.setFormPanel(this.formpanel);
        this.detail.add(this.formpanel);
        this.detail.doLayout();
    }

    // private
    ,onActionComplete : function(form, action) {
        var data = action.options.data;
        var store = this.store;
        var existing = store.getById(data.id);
        var key;

        if (existing) {
            existing.beginEdit();
            for (key in data) {
                existing.set(key, data[key]);
            }
            existing.commit();
        }
        else {
            store.add(this.newRecord(store.recordType, action));
            this.select(data.id);
        }
    }

    ,newRecord : function(recordType, action) {
        var data = action.options.data;
        return new recordType(data, data.id);
    }

    // private
    ,onAdd : function() {
        if (this.etCombo) {
            this.etCombo.setDisabled(!Z.isAdmin());
            this.etCombo.setValue(null);
            this.detail.remove(this.formpanel);
            delete this.formpanel;
        }
        else {
            var values = {};
            var i, eta;

            for (i = 0; i < this.et.attributes.length; i++) {
                eta = this.et.attributes[i];
                values[eta.guid] = eta.value;
            }
            values.id = null;
            this.formpanel.setValues(values);
            this.formpanel.form.findField('id').enable();
        }
    }
});
