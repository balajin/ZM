R.ModuleManager.register(new Z.module.EntityMasterDetailModule({
    id           : 'taggroup-config'
    ,category    : 'configuration'
    ,label       : 'Tag Groups'
    ,roles       : [ 'admin', 'configview' ]
    ,itemIconCls : 'taggroup-config-small-icon'
    ,deleteURL   : R.url('groupdelete')
    ,listURL     : R.url('grouplist')
    ,nameField   : 'label'

    ,etGuid : R.ET.TAG_GROUP
    ,etaTypeGuid : '$zTagGroupTagTypeValue'

    // override
    ,initialize : function() {
         Z.module.EntityMasterDetailModule.prototype.initialize.call(this);
         this.on('edit', function(module, record) {
             this.formpanel.form.findField('groupcode').setDisabled(true);
         }, this);
    }

    // override
    ,refresh : function() {
        var store = this.store;
        store.on('load', this.callback(function() {
            // load our readers
            var readers  = new Ext.data.Store({
                reader : new R.data.ObjectReader({
                    fields : [ 'id', 'label' ]
                })
                ,url : R.url('readerlist')
            });
            readers.on('load', this.callback(this.onReadersLoad), this);
            readers.load();
        }), this, { single: true });
        store.on('update', function(store, record) {
            var node = this.tree.getNodeById(record.id);
            var text;
            if (node) {
                text = record.get('label') || record.id;
                if (text !== node.text) {
                    node.setText(text);
                }
            }
        }, this);
        store.load();
    }

    // override
    ,onCleanup : function() {
        delete this.factory;
        Z.module.EntityMasterDetailModule.prototype.onCleanup.call(this);
    }

    // override
    ,onEntityTypeChange : function(combo) {
        var taggroup = R.ET.get(combo.getValue());
        var tagtype = R.ET.get(taggroup.getETA('$zTagGroupTagType').value);

        // we must register the new controller before calling our super method
        // since the super creates the form panel and by then its too late
        this.factory.registerGuid('$zTagGroupBlockAttribUpdate', 'write', {
            onCreate : function(config) {
                var data = [];
                var ignore = {
                    '$zTagTypeValue': true
                    ,'$zTagTagGroup': true
                    ,'$zTagLocation': true
                    ,'$zTagTypeValue': true
                    ,'$zTagHasChildren': true
                };

                tagtype.getETAs().each(function(eta) {
                    var ac = R.AC.get(eta.guid);
                    if (ac && !ignore[ac.guid]) {
                        data.push(ac);
                    }
                });

                return new R.form.ListField(Ext.apply({
                    displayField : 'name'
                    ,store : new Ext.data.JsonStore({
                        data : data
                        ,fields : [
                            { name: 'name', sortType: Ext.data.SortTypes.asUCString }
                            ,{ name: '$zName' }
                        ]
                    })
                    ,valueField : '$zName'
                }, config));
            }

            ,toJson : function(json) {
                var value = this.field.getValue();
                json[this.ac.guid] = value.join(',');
            }
        });

        Z.module.EntityMasterDetailModule.prototype.onEntityTypeChange.call(this, combo);
    }

    // private
    ,onReadersLoad : function(readers) {
        var groups = this.store;

        // add a 'readers' value to each group
        groups.each(function(group) {
            var len = readers.getCount();
            var i, reader, rgroups;
            var greaders = [];

            for (i = 0; i < len; i++) {
                reader = readers.getAt(i);
                rgroups = reader.get('groups');

                if (rgroups && rgroups.indexOf(group.id) !== -1) {
                    greaders.push(reader.id);
                }
            }
            group.set('readers', greaders);
        }, this);

        var factory = new R.form.FieldFactory();
        factory.registerGuid('$zTagGroupReaderList', 'write', {
            readers : readers

            ,onCreate : function(config) {
                return new R.form.MutualChoiceField(Ext.apply({
                    choice1Label : 'All Readers'
                    ,choice2Label : this.ac.name
                    ,choice2Field : new R.form.ListField({
                        displayField : 'label'
                        ,store       : readers
                        ,valueField  : 'id'
                    })
                }, config));
            }

            ,toJson : function(json) {
                var value = this.field.getValue();
                if (!value) {
                    value = this.readers.collect('id');
                }
                json['$zTagGroupReaderList'] = value.join(',');
            }

            ,setValue : function(value) {
                if (Ext.isArray(value) && value.length === this.readers.getCount()) {
                    // null corresponds to all choice
                    value = null;
                }
                this.field.setValue(value);
            }
        });
        factory.registerGuid('$zTagGroupSensorDefinition', 'write', {
            onCreate : function(config) {
                return new Ext.form.ComboBox(Ext.apply({
                    clearable : true
                    ,displayField : 'label'
                    ,editable    : false
                    ,store       : new Ext.data.Store({
                        url : R.url('sensordeflist')
                        ,reader : new R.data.ObjectReader({
                            fields : [
                               'id',
                               { name: 'label', sortType: Ext.data.SortTypes.asUCString }

                            ]
                        })
                        ,sortInfo : { field: 'label' }
                    })
                    ,triggerAction : 'all'
                    ,valueField    : 'id'
                }, config));
            }
        });

        this.factory = factory;

        this.fireEvent('refresh', this);
    }

    // override
    ,onActionComplete : function(form, action) {
        Z.module.EntityMasterDetailModule.prototype.onActionComplete.apply(this, arguments);

        var groupid = action.options.data.id;
        var readers = action.options.readers;
        var original = R.toMap(readers.original);
        var updated = R.toMap(readers.updated);
        var add = [];
        var remove = [];
        var id;
        var requests = [];

        // find added readers
        for (id in updated) {
            if (!original[id]) {
                add.push(id);
            }
        }
        if (add.length > 0) {
            requests.push({
                url : R.url('readeraddgroup')
                ,params : { id: add.join(','), groupid0: groupid }
            });
        }

        // find removed readers
        for (id in original) {
            if (!updated[id]) {
                remove.push(id);
            }
        }
        if (remove.length > 0) {
            requests.push({
                url : R.url('readerremovegroup')
                ,params : { id: remove.join(','), groupid0: groupid }
            });
        }

        if (requests.length > 0) {
            R.Ajax.queue({
                decode    : true
                ,method   : 'POST'
                ,requests : requests
                ,scope    : this
                ,failure  : this.onReaderGroupFailure
            });
        }

        // update the store since readers are not considered part of the group
        // object
        this.store.getById(groupid).set('readers', readers.updated);
    }

    // override
    ,newRecord : function(recordType, action) {
        var data = action.options.data;
        data.readers = action.options.readers.updated;
        return new recordType(data, data.id);
    }

    // private
    ,onReaderGroupFailure : function(result) {
        var i, msg;

        for (i = 0; i < result.length; i++) {
            if (result[i].__result === 'error') {
                msg = result[i].__msg;
            }
        }

        if (msg) {
            this.formpanel.form.markInvalid([{ msg: msg } ]);
        }
    }

    // override
    ,onSubmit : function() {
        var form = this.formpanel.form;
        var exists = form.findField('id').disabled;
        var data = this.formpanel.getValues();
        var readers = data.readers;
        var eta, existing, type;

        if (exists) {
            existing = this.store.getById(data.id);
            delete data['groupcode'];
        }
        else {
            // convert from AM to ZM namespace
            eta = this.getSelectedEntityType().getETA(this.etaTypeGuid);
            data['type'] = eta.value;
        }

        if (!data['label']) {
            data['label'] = data.id;
        }

        // readers need to be updated by separate command
        delete data.readers;

        return {
            data     : data
            ,method  : 'POST'
            ,readers : {
                original : existing ? existing.get('readers') : []
                ,updated : Ext.isEmpty(readers) ? [] : readers.split(',')
            }
            ,url     : exists ? R.url('groupset') : R.url('groupcreate')
        };
    }
}));
