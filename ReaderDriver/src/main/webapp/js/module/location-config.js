R.ModuleManager.register(new R.module.TreeMasterDetailModule({
    id          : 'location-config'
    ,category   : 'configuration'
    ,label      : 'Locations & Rules'
    ,roles      : [ 'admin', 'configview' ]

    ,nameField  : 'id'

    // override
    ,onCleanup : function() {
        delete this.addRuleAction;
        delete this.saveFormPlugin;
        delete this.factory;
        R.module.TreeMasterDetailModule.prototype.onCleanup.call(this);
    }

    // override
    ,refresh : function(params) {
        if (params['rule']) {
            // this.selection defined in TreeMasterDetailModule
            this.selection = params['rule'];
        }

        var store = this.store;
        store.on('load', this.callback(function() {
            R.Ajax.request({
                method    : 'GET'
                ,url      : R.url('rulelist')
                ,scope    : this
                ,callback : this.callback(this.onRuleList)
            });
        }), this, { single: true });
        store.load();
    }

    // private
    ,onRuleList : function(options, success, response) {
        if (success) {
            var reader = new R.data.ObjectReader({
                fields : [ 'id' ]
            });
            var rules = reader.read(response).records;
            var store = this.store;
            var i, rule;
            for (i = 0; i < rules.length; i++) {
                rule = rules[i];
                // parent required by TreeMasterDetailModule
                rule.set('parent', rule.get('target_location'));
            }
            this.store.add(rules);
        }
        this.fireEvent('refresh', this);
    }

    // override
    ,createStore : function() {
        return new Ext.data.Store({
            reader : new R.data.ObjectReader({
                fields : [ 'id' ]
            })
            ,url : R.url('loclist')
        });
    }

    // override
    ,createActions : function() {
        var actions;

        this.addRuleAction = new R.ui.Action({
            disabled : true
            ,tbar    : true
            ,cls     : 'button-new x-btn-text-icon'
            ,text    : tr('lrNewRule')
            ,scope   : this
            ,handler : this.onRuleAdd
        });
        if (Z.isAdmin()) {
            actions = [];
            actions.push(new R.ui.Action({
                tbar     : true
                ,cls     : 'button-new x-btn-text-icon'
                ,text    : tr('lrNewLocation')
                ,scope   : this
                ,handler : this.onLocationAdd
            }));
            actions.push(this.addRuleAction);
            actions.push(new R.ui.SingleAction({
                tbar     : true
                ,cls     : 'button-delete x-btn-text-icon'
                ,text    : tr('delete')
                ,scope   : this
                ,handler : this.onDelete
                ,update  : function() {
                    var tree = this.manager.tree;
                    var node = tree.getSelectionModel().getSelectedNode();
                    this.setDisabled(!node || node.childNodes.length > 0);
                }
            }));
        }
        return actions;
    }

    // private
    ,isRuleSelected : function() {
        return (this.detail.getLayout().activeItem !== this.locFormPanel);
    }

    // private
    ,onLocationAdd : function() {
        if (this.isRuleSelected()) {
            this.detail.getLayout().setActiveItem(0);
            this.saveFormPlugin.setFormPanel(this.locFormPanel);
        }

        this.locFormPanel.form.clearInvalid();
        this.locFormPanel.form.setValues({ id: '', excludedtags: [], definesirdomain: false });
        this.locFormPanel.form.findField('id').enable();
        this.detail.setTitle(tr('lrNewLocation'));
    }

    // private
    ,onRuleAdd : function() {
        if (!this.isRuleSelected()) {
            this.detail.getLayout().setActiveItem(1);
        }

        if (this.ruleFormPanel) {
            this.detail.remove(this.ruleFormPanel);
        }
        this.ruleCombo.setValue('$zRuleSimpleSSIRule');
        this.ruleCombo.enable();
        this.detail.setTitle(tr('lrNewRule'));
        this.onRuleTypeChange(this.ruleCombo);
    }

    // private
    ,onDelete : function() {
        var node = this.tree.getSelectionModel().getSelectedNode();
        var record = node.attributes.record;

        R.ui.Msg.remove({
            title   : 'Delete'
            ,msg    : 'Are you sure you want to delete ' + record.id + '?'
            ,scope  : this
            ,remove : function() {
                Ext.Ajax.request({
                    method   : 'POST'
                    ,url     : record.data.type ? R.url('ruledelete') : R.url('locdelete')
                    ,params  : { id: record.id }
                    ,scope   : this
                    ,success : function(response) {
                        var json = Ext.decode(response.responseText);
                        if (json['__result'] == 'error') {
                            Ext.Msg.alert('Error', json['__msg']);
                        }
                        else {
                            this.store.remove(record);
                        }
                    }
                });
            }
        });
    }

    // override
    ,createDetailComponent : function() {
        // location
        this.locFormPanel = new R.form.FormPanel({
            items : [
                {
                    disabled    : !Z.isAdmin()
                    ,fieldLabel : 'Location'
                    ,name       : 'id'
                    ,width      : 250
                    ,xtype      : 'textfield'
                }
                ,{
                    disabled    : !Z.isAdmin()
                    ,fieldLabel : 'Excluded Tags'
                    ,name       : 'excludedtags'
                    ,width      : 250
                    ,xtype      : 'z-taglistfield'
                }
                ,{
                    disabled    : !Z.isAdmin()
                    ,fieldLabel : 'IR Bounded Location'
                    ,name       : 'definesirdomain'
                    ,width     : 'auto'
                    ,xtype     : 'checkbox'
                }
            ]
        });
        this.locFormPanel.form.on('actioncomplete', this.onActionComplete, this);
        this.ruleCombo = this.createRuleCombo();
        this.rulePanel = new Ext.Panel({
            layout : 'form'
            ,items : {
                height : 'auto'
                ,xtype : 'fieldset'
                ,items : this.ruleCombo
            }
        });

        this.saveFormPlugin = new R.form.SaveFormPlugin({
            scope : this
            ,onSubmit : this.onSubmit
        });
        this.saveFormPlugin.setFormPanel(this.locFormPanel);

        return new Ext.Panel({
            activeItem : 0
            ,bodyStyle : 'padding: 0px 5px'
            ,frame     : true
            ,header    : true
            ,layout    : 'card'
            ,title     : '&nbsp;'
            ,items     : [ this.locFormPanel, this.rulePanel ]
            ,plugins   : this.saveFormPlugin
        });
    }

    // private
    ,createRuleCombo : function() {
        var rules = R.ET.get(R.ET.RULE).leaves();

        // list rules in a specific order
        var data = [
            '$zRuleSimpleSSIRule'
            ,'$zRuleIRLocatorRule'
            ,'$zRuleAverageSSIRule'
        ];
        // add any rules we missed
        for (var i = 0; i < rules.length; i++) {
            if (data.indexOf(rules[i].guid) === -1) {
                data.push(rules[i].guid);
            }
        }

        var combo = new Ext.form.ComboBox({
            disabled    : !Z.isAdmin()
            ,displayField : 'name'
            ,editable   : false
            ,fieldLabel : 'Rule Type'
            ,listWidth  : 300
            ,mode       : 'local'
            ,name       : 'type'
            ,id         : 'type'
            ,store      : new Ext.data.Store({
                data    : data.map(function(guid) { return R.ET.get(guid); })
                ,reader : new Ext.data.JsonReader({
                    fields : [ 'guid', 'name' ]
                })
            })
            ,triggerAction : 'all'
            ,valueField    : 'guid'
            ,width         : 300
        });
        combo.on('select', this.onRuleTypeChange, this);
        return combo;
    }

    // override
    ,onCreateNode : function(config) {
        var data = config.record.data;
        if (data.type) {
            if (data.enabled) {
                // enabled rule
                config.iconCls = 'location-rule-small-icon';
            }
            else {
                // disabled rule
                config.iconCls = 'location-rule-offline-small-icon';
            }
            config.leaf = true;
        }
        else {
            // location
            config.iconCls = 'location-config-small-icon';
        }
        return config;
    }

    // private
    ,onRuleTypeChange : function(combo) {
        var guid = combo.getValue();
        var tree = this.tree;

        if (!this.factory) {
            this.factory = new R.form.FieldFactory();
            this.factory.registerGuid('$zRuleTargetLocation', 'write', {
                onCreate : Ext.emptyFn
                ,toJson  : function(json) {
                    var node = tree.getSelectionModel().getSelectedNode();

                    // check if rule is selected
                    if (node.attributes.record.data.type) {
                        node = node.parentNode;
                    }
                    json[this.ac.guid] = node.id;
                }
            });
            this.factory.registerGuid('$zRuleTargetLocation', 'read', {
                onCreate : Ext.emptyFn
            });
        }

        if (this.ruleFormPanel) {
            this.rulePanel.remove(this.ruleFormPanel);
        }
        this.ruleFormPanel = new Z.form.EntityForm({
            editable : Z.isAdmin()
            ,et      : guid
            ,factory : this.factory
        });
        this.ruleFormPanel.form.on('actioncomplete', this.onActionComplete, this);

        this.saveFormPlugin.setFormPanel(this.ruleFormPanel);
        this.rulePanel.add(this.ruleFormPanel);
        this.rulePanel.doLayout();
    }

    // override
    ,onSelectionChange : function(record) {
        R.module.TreeMasterDetailModule.prototype.onSelectionChange.apply(this, arguments);

        var data, formpanel, title;
        if (record) {
            data = record.data;
            if (data.type) {
                title = tr('lrEditRule');

                // find the correct rule entity type for the record
                var child = R.find(R.ET.get(R.ET.RULE).leaves(), function(child) {
                    var eta = child.getETA('$zRuleType');
                    return eta.value === data.type;
                }, this);
                this.ruleCombo.setValue(child.guid);
                this.ruleCombo.disable();
                this.onRuleTypeChange(this.ruleCombo);

                formpanel = this.ruleFormPanel;

                // fix $zRuleTargetLocation
                if (data['target_location']) {
                    data['locid'] = data['target_location'];
                }

                this.ruleFormPanel.setValues(data);
                this.saveFormPlugin.saveButton.enable();
                this.detail.getLayout().setActiveItem(1);
                this.detail.doLayout();
            }
            else {
                title = tr('lrEditLocation');
                formpanel = this.locFormPanel;

                // we reuse the form, so make sure this is cleared if newly
                // selected location has no excluded tags
                data.excludedtags = data.excludedtags || [];
                data.definesirdomain = data.definesirdomain || false;
                
                this.locFormPanel.form.setValues(data);
                this.locFormPanel.form.clearInvalid();
                this.saveFormPlugin.saveButton.disable();
                this.detail.getLayout().setActiveItem(0);
            }
            this.detail.setTitle(title);
            formpanel.form.findField('id').disable();
            this.saveFormPlugin.setFormPanel(formpanel);
            this.addRuleAction.setDisabled(this.isRuleSelected());
        }
        else {
            this.onLocationAdd();
            this.addRuleAction.disable();
        }
    }

    // private
    ,onActionComplete : function(form, action) {
        var data = action.options.data;

        if (action.options.parent) {
            // parent required by TreeMasterDetailModule
            data.parent = action.options.parent;
        }

        // we send coordlist as a string, but the field expects an array, so
        // just use the value from the field
        if (data.coordlist) {
            data.coordlist = form.getValues().coordlist;
        }

        this.store.add([new Ext.data.Record(data, data.id)]);
        this.select(data.id);
    }

    // override
    ,onSubmit : function() {
        var data, parent, url;
        var node = this.tree.getSelectionModel().getSelectedNode();

        if (this.isRuleSelected()) {
            data = this.ruleFormPanel.getValues();

            if (this.ruleFormPanel.form.findField('id').disabled) {
                url = R.url('ruleset');
                delete data.locid;
            }
            else {
                url = R.url('rulecreate');
                data.type = R.ET.get(this.ruleCombo.getValue()).getETA('$zRuleType').value;

                // location is selected
                data.locid = node.id;
                parent = node.id;
            }
        }
        else {
            // location
            data = this.locFormPanel.form.getValues();

            if (this.locFormPanel.form.findField('id').disabled) {
                url = R.url('locset');
                data.excludedtags = this.locFormPanel.form.findField('excludedtags').getValue();
                data.excludedtags = data.excludedtags.join(',');
                data.definesirdomain = this.locFormPanel.form.findField('definesirdomain').getValue();
            }
            else {
                if (node) {
                    data.parent = this.isRule(node) ? node.parentNode.id : node.id;
                }
                url = R.url('loccreate');
            }
        }

        return {
            data    : data
            ,method : 'POST'
            ,url    : url
            ,parent : data.parent || parent
        };
    }

    // private
    ,isRule : function(node) {
        return !!node.attributes.record.data.locid;
    }
}));
