R.ModuleManager.register(new R.Module({
    id        : 'import-export-config'
    ,label    : 'Import & Export'
    ,category : 'configuration'
    ,roles    : [ 'admin', 'configview' ]

    // override
    ,label : function() {
        return Z.isAdmin() ? 'Import & Export' : 'Export';
    }

    // override
    ,onCreateUI : function() {
        this.importPanel = this.createImportPanel();
        this.importResult = new Ext.form.Label({
            isFormField : false
        });

        var items = [ this.createExportPanel() ];
        if (Z.isAdmin()) {
            items.push(this.importPanel, this.importResult);
        }

        return new Ext.Panel({
            autoScroll : true
            ,bodyStyle : 'padding: 0px 5px'
            ,frame     : true
            ,header    : true
            ,layout    : 'form'
            ,items     : items
        });
    }

    // private
    ,createExportPanel : function() {
        var button = function(text, cls, url) {
            return new R.ui.ExportButton({
                cellCls   : 'export-config-cell'
                ,iconCls  : cls
                ,text     : text
                ,minWidth : 100
                ,url      : url
                ,scope    : this
                ,handler  : function(btn) {
                    this.checkAuth(this.doExport, btn.url);
                }
            });
        };

        var link = function(label, cls, url) {
            return {
                autoEl : {
                    tag   : 'div'
                    ,'class' : 'export-config-item ' + cls
                    ,children : [
                        { tag: 'a', href: 'zonemgr/api/' + url + '.csv?__ui=true', html: label }
                    ]
                }
                ,xtype : 'container'
            };
        };

        return new Ext.form.FieldSet({
            height : 'auto'
            ,layout : 'table'
            ,title : 'Export Configuration'
            ,items : [
                link('Tag Groups', 'taggroup-config-small-icon', 'groupexport')
                ,link('Readers', 'reader-config-small-icon', 'readerexport')
                ,link('Locations', 'location-config-small-icon', 'locexport')
                ,link('Rules', 'location-rule-small-icon', 'ruleexport')
            ]
        });
    }

    // private
    ,createImportPanel : function() {
        var btn = new R.ui.UploadButton({
            disabled : true
            ,text    : 'Upload'
            ,scope   : this
            ,handler : this.doImport
        });
        var fileselected = function() {
            btn.enable();
        };
        var field = function(label) {
            return {
                fieldLabel : label
                ,name      : 'csvfile'
                ,width     : 400
                ,xtype     : 'fileuploadfield'
                ,listeners : {
                    'fileselected' : fileselected
                }
            };
        };

        return new Ext.form.FormPanel({
            fileUpload : true
            ,items : [
                {
                    buttonAlign : 'left'
                    ,height     : 'auto'
                    ,labelWidth : 145
                    ,title      : 'Import Configuration'
                    ,xtype      : 'fieldset'
                    ,items      : [
                        field('Tag Groups (groups.csv)')
                        ,field('Readers (readers.csv)')
                        ,field('Locations (locations.csv)')
                        ,field('Rules (rules.csv)')
                    ]
                    ,buttons : [ btn ]
                }
            ]
        });
    }

    // private
    ,doImport : function() {
        this.importResult.setText('');

        Ext.Msg.wait('Importing configuration files', 'Upload');
        // the upload occurs in an iframe preventing us from getting the
        // http status code. Therefore run a quick authorization check
        // before proceeding with download.
        R.Ajax.request({
            method   : 'GET'
            ,url     : R.url('whoami')
            ,scope   : this
            ,success : this.callback(this.onImportAuth)
        });
    }

    // private
    ,onImportAuth : function(response, options) {
        var form = this.importPanel.form;

        // disable fields with no value so they are not sent to the server
        form.items.each(function(f) {
            var dom = f.fileInput.dom;
            if (!dom.value) {
                dom.disabled = true;
            }
        });
        R.Ajax.request({
            form      : form.el.dom
            ,isUpload : true
            ,url      : 'zonemgr/cfgimport'
            ,scope    : this
            ,callback : this.callback(this.onImport)
        });
    }

    // private
    ,onImport : function(options, success, response) {
        var rsp = response.responseText;
        var msg;
        if (rsp.indexOf('HTTP ERROR 500') === -1) {
            msg = '<pre>Import results:\n</pre>' + rsp;
        }
        else {
            msg = '<pre>Import results:\n&lt;error&gt;: Bad CSV file format</pre>';
        }
        this.importResult.setText(msg, false);

        this.importPanel.form.items.each(function(f) {
            f.fileInput.dom.disabled = false;
        });
        Ext.Msg.hide();
    }

    // private
    ,createAddAction : function() {
        var callback = function(btn, text) {
            if (btn === 'ok') {
                R.Ajax.request({
                    method  : 'POST'
                    ,url    : R.url('licenseadd')
                    ,params : {
                        key0 : text.trim()
                    }
                    ,scope   : this
                    ,success : function(response) {
                        var json = Ext.decode(response.responseText);
                        if (json['__result'] == 'error') {
                            Ext.Msg.alert('Error', json['__msg']);
                        }
                        else {
                            this.refresh();
                        }
                    }
                });
            }
        };

        return new R.ui.Action({
            tbar     : true
            ,text    : 'Add'
            ,cls     : 'button-new x-btn-text-icon'
            ,scope   : this
            ,handler : function() {
                Ext.Msg.prompt('New License Key', 'Enter License Key', callback, this);
            }
        })
    }

    // private
    ,createDeleteAction : function() {
        return new R.ui.MultiAction({
            tbar     : true
            ,text    : 'Delete'
            ,scope   : this
            ,cls     : 'button-delete x-btn-text-icon'
            ,handler : function() {
                R.ui.Msg.remove({
                    title   : 'Delete'
                    ,msg    : 'Are you sure you want to delete the selected licenses?'
                    ,scope  : this
                    ,remove : function() {
                        var records = this.grid.getSelectionModel().getSelections();
                        var params  = {};

                        Ext.each(records, function(r, i) {
                            params['key' + i] = r.id
                        });
                        R.Ajax.request({
                            method   : 'POST'
                            ,url     : R.url('licenseremove')
                            ,params  : params
                            ,scope   : this
                            ,success : this.refresh
                        });
                    }
                });
            }
        })
    }

    // private
    ,createSummaryField : function(name, text) {
        return new Ext.form.TextField({
            disabled    : true
            ,fieldLabel : text
            ,name       : name
            ,style      : 'color: #000000;font-weight: bold;border: 0px;padding-top: 3px;background: transparent none;'
            ,value      : '0'
        });
    }

    // override
    ,refresh2 : function() {
        return;
        R.Ajax.request({
            method    : 'GET'
            ,url      : R.url('licenselist')
            ,scope    : this
            ,callback : this.callback(function(options, success, response) {
                if (success) {
                    var licenseList = Ext.decode(response.responseText);
                    var store    = this.grid.store;
                    var records  = [];

                    // update store
                    var keys = licenseList['keys'];
                    var expdates = licenseList['expdates'];
                    for (var i = 0; i < keys.length; i++) {
                        records.push(new Ext.data.Record({ key : keys[i], expdate : expdates[keys[i]]}, keys[i]));
                    }
                    store.removeAll();
                    store.add(records);

                    // update summary
                    var total = licenseList['limit'];
                    var used  = licenseList['used'];
                    var avail = 'Unlimited';

                    if (total === 'unlimited') {
                        total = 'Unlimited';
                    }
                    else {
                        avail = total - used;
                    }
                    this.summary.find('name', 'total')[0].setValue(total);
                    this.summary.find('name', 'used')[0].setValue(used);
                    this.summary.find('name', 'avail')[0].setValue(avail);
                }

                this.fireEvent('refresh', this);
            })
        });
    }
}));
