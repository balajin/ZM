R.ModuleManager.register(new R.Module({
    id          : 'tag-find-view'
    ,category   : 'summary'
    ,label      : 'Tag Find'

    // override
    ,onCreateUI : function() {
         // value renderer
         var rValue = function(value, metadata, record) {
             var tag = record.get('tag');
             var group;

             if (tag) {
                 if (this.zmObjects && 'taggroupid' === record.id) {
                     group = this.zmObjects[value];
                     return group.label || record.id;
                 }
                 return tag.renderer.apply(this, arguments);
             }
         };

         // name renderer
         var rName = function(value, p, record) {
             if (record.get('loading')) {
                 p.css += 'x-mask-loading';
             }
             else if (!record.get('tag')) {
                 value = '<span style="color:red;">Tag Not Found</span>';
             }
             else if (this.zmObjects) {
                 var channel = this.zmObjects[value];
                 var reader;

                 if (channel) {
                     reader = this.zmObjects[channel.reader];

                     if (reader) {
                         value = reader.label + ' (' + channel.label + ')';
                     }
                 }
             }
             return value;
         };

         this.grid = new Ext.grid.GridPanel({
             columns : [
                 { dataIndex: 'name', header: 'Name', sortable: true, width: 200, renderer: _.bind(rName, this) }
                 ,{ dataIndex: 'value', header: 'Value', sortable: true, width: 300, renderer: _.bind(rValue, this) }
             ]
             ,store : new Ext.data.SimpleStore({
                 fields  : [ 'name', 'value' ]
                 ,sortInfo : { field: 'name', direction: 'ASC' }
             })
             ,stripeRows : true
         });

         this.search = Z.createTagSearchField();
         this.search.on('search', this.onSearch, this);

         // the task for running another tag print
         this.task = new Ext.util.DelayedTask(this.doTagPrint, this);

         return new Ext.Panel({
             frame   : true
             ,header : true
             ,layout : 'fit'
             ,tbar   : [ 'Tag ID: ', this.search ]
             ,items  : this.grid
         });
    }

    // override
    ,refresh : function(params) {
        if (params['tag']) {
            this.tagId = params['tag'];
        }
        if (this.tagId) {
            this.search.setValue(this.tagId);
            this.onSearch(this.search, this.tagId);
        }
        this.fireEvent('refresh', this);

        R.Ajax.queue({
            decode    : true
            ,method   : 'GET'
            ,requests : [ R.url('channellist'), R.url('grouplist'), R.url('readerlist') ]
            ,scope    : this
            ,success  : this.callback(this.onReaderAndGroupLoad)
        });
    }

    // private
    ,onReaderAndGroupLoad : function(result) {
        var channels = result[0];
        var readers = result[1];
        var groups = result[2];

        this.zmObjects = {};

        var reducer = function(result, obj, key) {
            result[key] = obj.attributes;
            return result;
        };
        result = _.reduce(channels, reducer, {});
        result = _.reduce(readers, reducer, result);
        result = _.reduce(groups, reducer, result);

        this.zmObjects = result;
    }

    // override
    ,onCleanup : function() {
        if (this.task) {
            this.task.cancel();
            delete this.task;
        }
        Ext.Ajax.abort(this.request);
        delete this.request;
        delete this.zmObjects;
    }

    // private
    ,onSearch : function(field, value) {
        if (value) {
            var store = this.grid.store;

            store.removeAll();
            store.add([ new Ext.data.Record({ name: 'Loading: ' + value, loading: true }) ]);

            // used if user comes back to this module (see refresh)
            this.tagId = value;

            Ext.Ajax.abort(this.request);

            if (this.task) {
                this.task.cancel();
            }

            this.doTagPrint();
        }
    }


    // private
    ,doTagPrint : function() {
        // we poll tagprint instead of tagupdates so we can retrieve the SSI
        // values
        this.request = R.Ajax.request({
            url : R.url('tagprint')
            ,params : {
                tagid0 : this.tagId
            }
            ,scope    : this
            ,callback : this.onTagPrint
        });
    }

    // private
    ,onTagPrint : function(options, success, response) {
        if (this.request && this.request.tId === response.tId) {
            var store = this.grid.store;
            var json  = Ext.decode(response.responseText);

            if (json.__result === 'error') {
                store.removeAll();
                store.add([ new Ext.data.Record({}) ]);
            }
            else {
                var id;
                for (id in json) {
                    break;
                }
                var tag = new Z.data.Tag(json[id]);

                // loadRecords sorts the records, unlike add
                store.loadRecords({ records: tag.records() }, {}, true);
            }

            // load tag again
            this.task.delay(1000);
        }
    }
}));
