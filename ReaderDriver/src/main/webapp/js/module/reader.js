R.ModuleManager.register(new R.Module({
    id          : 'reader-view'
    ,category   : 'summary'
    ,label      : 'Readers'
    ,roles      : [ 'admin', 'configview' ]

    // override
    ,onCreateUI : function() {
        return new Z.grid.ReaderGridPanel({
            fields : [ 'id', 'label', 'hostname', 'port', 'state', 'enabled',
                    'noiseA', 'noiseB', 'evtpersecA', 'evtpersecB', 'tagcapused',
                    'fwversion', 'remoteaddr', 'encrypted', 'gpsfix'
            ]
        });
    }

    // override
    ,refresh : function() {
        var store = this.component.store;
        store.on('load', function() {
            this.fireEvent('refresh', this);
        }, this, { single: true });
        store.load();
    }

    // override
    ,onCleanup : function() {
        this.component.store.destroy();
    }
}));
