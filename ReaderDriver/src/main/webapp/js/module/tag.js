R.ModuleManager.register(new R.Module({
    id          : 'tag-view'
    ,category   : 'summary'
    ,label      : 'Tags'

    // override
    ,onCreateUI : function() {
        var tbar;

        this.combo = new Ext.form.ComboBox({
            displayField : 'groupcode'
            ,editable    : false
            ,mode        : 'local'
            ,store       : new Ext.data.Store({
                reader : new R.data.ObjectReader({
                    fields : [ 'id', 'groupcode' ]
                })
                ,sortInfo : { field: 'groupcode' }
            })
            ,triggerAction : 'all'
            ,valueField    : 'id'
            ,width         : 200
        });
        this.combo.on('select', this.onGroupChange, this);

        if (!Z.isEventView()) {
            tbar = [ 'Tag Group: ', this.combo ];
        }
        return new Z.grid.TagGridPanel({
            autoLoadStore : false
            ,frame  : true
            ,header : true
            ,tbar   : tbar
        });
    }

    // private
    ,onGroupChange : function(combo, record) {
        var id = combo.getValue();
        var store = this.component.store;

        if (id === '__all') {
            delete store.baseParams._groupid;
        }
        else {
            store.baseParams._groupid = id;
        }
        this.groupId = id;

        store.load();
    }

    // override
    ,refresh : function(params) {
        if (params['group']) {
            this.groupId = params['group'];
        }
        R.Ajax.request({
            method : 'GET'
            ,url   : R.url('grouplist2')
            ,scope   : this
            ,success : this.callback(this.onGroupLoad)
        });
    }

    // private
    ,onGroupLoad : function(response) {
        var store = this.combo.store;
        var all = new Ext.data.Record({id: '__all', groupcode: 'All' }, '__all');
        var data = Ext.decode(response.responseText);

        if (data.__result != 'error') {
            // subgroups do not have a groupcode
            for (var id in data) {
                var attrs = data[id].attributes;
                attrs.groupcode = attrs.groupcode || id;
            }
            store.loadData(data);
        }
        store.insert(0, [ all ]);

        var record = store.getById(this.groupId || '__all');
        this.combo.setValue(record.id);
        this.onGroupChange(this.combo, record);
        this.fireEvent('refresh', this);
    }
}));
