R.ModuleManager.register(new R.Module({
    id           : 'gps-config'
    ,category    : 'configuration'
    ,label       : 'GPS'
    ,roles       : [ 'admin', 'configview' ]

    ,deleteURL : R.url('readerdelete')
    ,listURL   : R.url('gpslist')

    ,etGuid : R.ET.READER
    ,etaTypeGuid : '$zReaderType'

    // override
    ,onCreateUI : function() {
        var columns = [
            { header: 'Reader', dataIndex: 'reader' }
            ,{ header: 'Fix', dataIndex: 'gpsfix' }
            ,{ dataIndex: 'latlon' }
            ,{ dataIndex: 'altitude' }
            ,{ dataIndex: 'course' }
            ,{ dataIndex: 'speed' }
            ,{ dataIndex: 'epe_horiz' }
            ,{ dataIndex: 'epe_vert' }
        ];
        var grid = new R.grid.GridPanel({
            autoScroll : true
            ,colModel : new Ext.grid.ColumnModel({
                defaults : { sortable : true }
                ,columns : columns.map(function(c) {
                    var ac = R.AC.getByZName(c.dataIndex);
                    if (ac) {
                        c.header = ac.name;
                        c.renderer = ac.renderer();
                    }
                    return c;
                })
            })
            ,frame  : true
            ,header : true
            ,region : 'center'
            ,store  : new Z.data.TagUpdatesStore({
                baseParams : {
                    _objtype : 'gps'
                }
                ,fields : columns.map(function(c) { return c.dataIndex; })
                ,remoteSort : false
            })
            ,stripeRows : true
            ,listeners : {
                rowdblclick : function(grid, row) {
                    var record = grid.store.getAt(row);
                    if (record && record.get('reader')) {
                        R.App.select('reader-config', { reader: record.get('reader') });
                    }
                }
            }
        });

        var factory = new R.form.FieldFactory();
        factory.registerGuid('$zReaderGPSData', 'write', {
            onCreate : function(config) {
                // remove use-globel
                var values = this.ac.values.slice(1);
                return new Ext.form.ComboBox(Ext.apply(config, {
                    mode            : 'local'
                    ,clearable      : !config.required
                    ,triggerAction  : 'all'
                    ,editable       : false
                    ,selectOnFocus  : true
                    ,displayField   : 'value'
                    ,valueField     : 'value'
                    ,store          : new Ext.data.ArrayStore({
                        data        : values
                        ,fields     : ['value']
                        ,expandData : true
                    })
                }));
            }
        });

        this.formpanel = new Z.form.EntityForm({
            bodyStyle : 'padding: 0px 5px'
            ,editable : Z.isAdmin()
            ,et       : '$zGPSData'
            ,factory  : factory
            ,frame    : true
            ,header   : true
            ,height   : 190
            ,region   : 'north'
            ,split    : true
            ,plugins  : new R.form.SaveFormPlugin({
                onSubmit : this.onSubmit
            })
        });
        return new Ext.Container({
            layout : 'border'
            ,style : 'background-color: transparent'
            ,items : [ grid, this.formpanel ]
        });
    }

    // override
    ,refresh : function() {
        R.Ajax.request({
            method   : 'GET'
            ,url     : R.url('gpsdefget')
            ,scope   : this
            ,success : this.callback(function(response) {
                var data = Ext.decode(response.responseText);
                this.formpanel.setValues(data);
                this.fireEvent('refresh', this);
            })
        });
    }

    // private
    ,onSubmit : function(form) {
        return {
            data    : this.formpanel.getValues()
            ,method : 'POST'
            ,url    : R.url('gpsdefset')
        };
    }
}));
