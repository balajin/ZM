R.ModuleManager.register(new R.Module({
    id          : 'tag-by-parent-view'
    ,category   : 'summary'
    ,label      : 'Tags by Parent'

    // override
    ,onCreateUI : function() {
        this.parentField = Z.createTagSearchField();
        this.parentField.on('search', this.onTagSearch, this);

        return new Z.grid.TagGridPanel({
            autoLoadStore : false
            ,frame  : true
            ,header : true
            ,tbar   : [ 'Parent Tag: ', this.parentField ]
        });
    }

    // private
    ,onTagSearch : function(field, value) {
        if (value) {
            R.App.select(this.id, { tag: value });
        }
    }

    // override
    ,refresh : function(params) {
        if (params['tag']) {
            this.tagId = params['tag'];
        }
        if (this.tagId) {
            var store = this.component.store;
            var params = store.baseParams;

            this.parentField.setValue(this.tagId);

            params._attribute = 'parenttag';
            params._operator = 'eq';
            params._value = this.tagId;
            store.load();
        }
        this.fireEvent('refresh');
    }
}));
