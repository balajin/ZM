R.ModuleManager.register(new R.Module({
    id        : 'license-config'
    ,label    : 'License Keys'
    ,category : 'configuration'
    ,roles    : [ 'admin' ]

    // override
    ,onCreateUI : function() {
        var renderer = function(value) {
            if (Ext.isEmpty(value)) {
                value = 'Never expires';
            }
            return value;
        };

        this.grid = new Ext.grid.GridPanel({
            autoExpandColumn : 'expdate'
            ,columns : [
                { id: 'key', dataIndex: 'key', header: 'License Key', sortable: true, width: 175 }
               ,{ id: 'expdate', dataIndex: 'expdate', header: 'Expiration Date', sortable: true, width: 175, renderer: renderer }
            ]
            ,frame  : true
            ,header : true
            ,region : 'center'
            ,store  : new Ext.data.Store({
                fields : [ 'key', 'expdate' ]
            })
        });

        this.summary = new Ext.Panel({
            bodyStyle : 'padding: 0px 5px'
            ,frame  : true
            ,header : true
            ,height : 350
            ,region : 'south'
            ,split  : true
            ,items  : [
                {
                    height : 'auto'
                    ,title  : 'License Key Information'
                    ,xtype : 'fieldset'
                    ,items : [
                        this.createSummaryField('used', 'Licenses in Use')
                        ,this.createSummaryField('total', 'Total Licenses')
                        ,this.createSummaryField('avail', 'Available Licenses')
                    ]
                }
            ]
        });

        return new Ext.Panel({
            layout : 'border'
            ,items : [ this.grid, this.summary ]
            ,plugins : new R.grid.GridActions({
                grid : this.grid
                ,items : [ this.createAddAction(), this.createDeleteAction() ]
            })
        });
    }

    // private
    ,createAddAction : function() {
        var callback = function(btn, text) {
            if (btn === 'ok') {
                R.Ajax.request({
                    method  : 'POST'
                    ,url    : R.url('licenseadd')
                    ,params : {
                        key0 : text.trim()
                    }
                    ,scope   : this
                    ,success : function(response) {
                        var json = Ext.decode(response.responseText);
                        if (json['__result'] == 'error') {
                            Ext.Msg.alert('Error', json['__msg']);
                        }
                        else {
                            this.refresh();
                        }
                    }
                });
            }
        };

        return new R.ui.Action({
            tbar     : true
            ,text    : 'Add'
            ,cls     : 'button-new x-btn-text-icon'
            ,scope   : this
            ,handler : function() {
                Ext.Msg.prompt('New License Key', 'Enter License Key', callback, this);
            }
        })
    }

    // private
    ,createDeleteAction : function() {
        return new R.ui.MultiAction({
            tbar     : true
            ,text    : 'Delete'
            ,scope   : this
            ,cls     : 'button-delete x-btn-text-icon'
            ,handler : function() {
                R.ui.Msg.remove({
                    title   : 'Delete'
                    ,msg    : 'Are you sure you want to delete the selected licenses?'
                    ,scope  : this
                    ,remove : function() {
                        var records = this.grid.getSelectionModel().getSelections();
                        var params  = {};

                        Ext.each(records, function(r, i) {
                            params['key' + i] = r.id
                        });
                        R.Ajax.request({
                            method   : 'POST'
                            ,url     : R.url('licenseremove')
                            ,params  : params
                            ,scope   : this
                            ,success : this.refresh
                        });
                    }
                });
            }
        })
    }

    // private
    ,createSummaryField : function(name, text) {
        return new Ext.form.TextField({
            disabled    : true
            ,fieldLabel : text
            ,name       : name
            ,style      : 'color: #000000;font-weight: bold;border: 0px;padding-top: 3px;background: transparent none;'
            ,value      : '0'
        });
    }

    // override
    ,refresh : function() {
        R.Ajax.request({
            method    : 'GET'
            ,url      : R.url('licenselist')
            ,scope    : this
            ,callback : this.callback(function(options, success, response) {
                if (success) {
                    var licenseList = Ext.decode(response.responseText);
                    var store    = this.grid.store;
                    var records  = [];

                    // update store
                    var keys = licenseList['keys'];
                    var expdates = licenseList['expdates'];
                    for (var i = 0; i < keys.length; i++) {
                        records.push(new Ext.data.Record({ key : keys[i], expdate : expdates[keys[i]]}, keys[i]));
                    }
                    store.removeAll();
                    store.add(records);

                    // update summary
                    var total = licenseList['limit'];
                    var used  = licenseList['used'];
                    var avail = 'Unlimited';

                    if (total === 'unlimited') {
                        total = 'Unlimited';
                    }
                    else {
                        avail = total - used;
                    }
                    this.summary.find('name', 'total')[0].setValue(total);
                    this.summary.find('name', 'used')[0].setValue(used);
                    this.summary.find('name', 'avail')[0].setValue(avail);
                }

                this.fireEvent('refresh', this);
            })
        });
    }
}));
