R.ModuleManager.register(new R.Module({
    id        : 'firmware-config'
    ,label    : 'Reader Firmware'
    ,category : 'configuration'
    ,roles    : [ 'admin' ]

    // override
    ,onCreateUI : function() {
        this.grid = new Z.grid.ReaderGridPanel({
            autoExpandColumn : 'fwavail'
            ,fields   : [ 'id', 'label', 'hostname', 'enabled', 'state', 'fwversion', 'fwavail' ]
            ,plugins : new R.grid.GridActions({
                items : [
                    this.createUpgradeAction(),
                    this.createUpgradeAction(true),
                    this.createCancelUpgradeAction()
                ]
            })
        });

        var btn = new R.ui.UploadButton({
            disabled        : true
            ,isFormField    : true
            ,labelSeparator : ''
            ,text           : 'Upload'
            ,scope          : this
            ,handler        : this.doUpload
        });
        this.summary = new Ext.form.FormPanel({
            bodyStyle : 'padding: 0px 5px'
            ,fileUpload : true
            ,frame  : true
            ,header : true
            ,height : 200
            ,region : 'south'
            ,split  : true
            ,items  : [
                {
                    buttonAlign : 'left'
                    ,height : 'auto'
                    ,title  : 'Firmware Information'
                    ,xtype  : 'fieldset'
                    ,items : [
                        {
                            disabled    : true
                            ,fieldLabel : 'Version'
                            ,name       : 'version'
                            ,style      : 'color: #000000;font-weight: bold;border: 0px;padding-top: 3px;background: transparent none;'
                            ,value      : this.firmwareToString(R.App.firmware)
                            ,width      : '100%'
                            ,xtype      : 'textfield'
                        }
                        ,{
                            fieldLabel : 'Package'
                            ,name      : 'FIRMWARELIB'
                            ,width     : 400
                            ,xtype     : 'fileuploadfield'
                            ,listeners : {
                                scope : btn
                                ,'fileselected' : btn.enable
                            }
                        }
                        ,btn
                        ,{
                            isFormField : false
                            ,name       : 'result'
                            ,xtype      : 'label'
                        }
                    ]
                }
            ]
        });

        return new Ext.Panel({
            layout : 'border'
            ,items : [ this.grid, this.summary ]
        });
    }

    // private
    ,doUpload : function() {
        Ext.Msg.wait('Uploading firmware package', 'Upload');

        this.summary.find('name', 'result')[0].setText('');

        // the upload occurs in an iframe preventing us from getting the
        // http status code. Therefore run a quick authorization check
        // before proceeding with download.
        R.Ajax.request({
            method   : 'GET'
            ,url     : R.url('whoami')
            ,scope   : this
            ,success : this.callback(this.onUploadAuth)
        });
    }

    // private
    ,onUploadAuth : function(response) {
        R.Ajax.request({
            form      : this.summary.form.el.dom
            ,isUpload : true
            ,url      : 'zonemgr/fwlibupdate'
            ,scope    : this
            ,callback : this.callback(this.onUpload)
        });
    }

    // private
    ,onUpload : function(options, success, response) {
        var rsp = response.responseText;
        var msg = '<pre>\nUpload results: </pre>' + rsp;

        this.summary.find('name', 'result')[0].setText(msg, false);

        // load firmware version
        R.Ajax.request({
            method   : 'GET'
            ,url     : R.url('fwlibversion')
            ,scope   : this
            ,success : this.callback(this.onFwLibVersion)
        });
    }

    // private
    ,onFwLibVersion : function(response) {
        var firmware = Ext.decode(response.responseText);
        var field = this.summary.find('name', 'version')[0];

        field.setValue(this.firmwareToString(firmware));
        R.App.firmware = firmware;
        Ext.Msg.hide();
    }

    // private
    ,firmwareToString : function(firmware) {
        if (Ext.isEmpty(firmware.version)) {
            return 'No firmware uploaded';
        }
        return firmware.version + ' - ' + firmware.desc;
    }

    // private
    ,createUpgradeAction : function(isAll) {
        return new (isAll ? R.ui.Action : R.ui.MultiAction)({
            isAll    : !!isAll
            ,tbar    : true
            ,text    : isAll ? 'Upgrade All' : 'Upgrade Selected'
            ,cls     : 'button-new x-btn-text-icon'
            ,scope   : this
            ,handler : this.onUpgradeAction
        })
    }

    // private
    ,onUpgradeAction : function(action) {
        Ext.Msg.show({
            buttons : {
                yes : action.isAll ? 'Upgrade All Readers' : 'Upgrade'
                ,no : 'Don\'t Upgrade'
            }
            ,icon  : Ext.Msg.QUESTION
            ,msg   : action.isAll ?
                'Do you want to upgrade all readers?' :
                'Do you want to upgrade the selected readers?'
            ,title : 'Upgrade'
            ,isAll : action.isAll     // used by onConfirmUpgradeCommand
            ,url   : 'readerupgrade'  // used by onConfirmUpgradeCommand
            ,scope : this
            ,fn    : this.onConfirmUpgradeCommand
        });
    }

    // private
    ,createCancelUpgradeAction : function() {
        return new R.ui.MultiAction({
            tbar    : true
            ,text    : 'Cancel Upgrade'
            ,cls     : 'button-x x-btn-text-icon'
            ,scope   : this
            ,handler : this.onUpgradeCancelAction
        })
    }

    // private
    ,onUpgradeCancelAction : function() {
        Ext.Msg.show({
            buttons : {
                yes : 'Cancel Upgrade'
                ,no : 'Don\'t Cancel Upgrade'
            }
            ,icon  : Ext.Msg.QUESTION
            ,msg   : 'Do you want to cancel the upgrade for the selected readers?'

            ,title : 'Cancel Upgrade'
            ,url   : 'readerupgradecancel' // used by onConfirmUpgradeCommand
            ,scope : this
            ,fn    : this.onConfirmUpgradeCommand
        });
    }

    // private
    ,onConfirmUpgradeCommand : function(btn, text, opt) {
        if (btn === 'yes') {
            var records = [ { id: '*' } ];

            if (!opt.isAll) {
                records = this.grid.getSelectionModel().getSelections();
            }
            var params = {
                id: records.map(function(r) { return r.id; }).join(',')
            };

            R.Ajax.request({
                method  : 'POST'
                ,url    : R.url(opt.url)
                ,params : Ext.urlEncode(params)
                ,success : function(response) {
                    var json = Ext.decode(response.responseText);

                    if (json.__result === 'error') {
                        Ext.Msg.alert('Error', json.__msg);
                    }
                }
            });
        }
    }

    // override
    ,refresh : function() {
        var store = this.grid.store;
        store.on('load', function() {
            this.fireEvent('refresh', this);
        }, this, { single: true });
        store.load();
    }

    // override
    ,onCleanup : function() {
        this.grid.store.destroy();
    }
}));
