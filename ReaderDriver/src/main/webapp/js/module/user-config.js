R.ModuleManager.register(new Z.module.EntityMasterDetailModule({
    id           : 'user-config'
    ,category    : 'configuration'
    ,label       : 'Users'
    ,roles       : [ 'admin', 'configview' ]
    ,itemIconCls : 'user-config-small-icon'

    ,deleteURL : R.url('userdelete')
    ,listURL   : R.url('userlist')

    ,etGuid : '$zUser'

    // override
    ,initialize : function() {
        Z.module.EntityMasterDetailModule.prototype.initialize.call(this);

        var handler = function(module, record) {
            var role = this.formpanel.form.findField('role');
            var empty = Ext.isEmpty(record);
            if (empty) {
                role.setValue('admin');
            }
            role.setDisabled(!empty || this.store.getCount() === 0);
        };
        this.on('add', handler, this);
        this.on('edit', handler, this);
    }

    // override
    ,onSubmit : function() {
        var form = this.formpanel.form;
        var exists = form.findField('id').disabled;
        var data = this.formpanel.getValues();

        if (exists) {
            data = { id: data.id, password: data.password };
        }

        return {
            data    : data
            ,method : 'POST'
            ,url    : exists ? R.url('passwd') : R.url('usercreate')
        };
    }
}));
