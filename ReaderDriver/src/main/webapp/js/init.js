Ext.BLANK_IMAGE_URL='ext/resources/images/default/s.gif';

Ext.ns('Z', 'Z.data', 'Z.form', 'Z.grid', 'Z.module', 'Z.ui', 'Z.util');

/**
 * @return true if the logged in user has admin role
 */
Z.isAdmin = function() {
    return R.App.user.role === 'admin';
};

/**
 * @return true if the logged in user has configview role
 */
Z.isConfigView = function() {
    return R.App.user.role === 'configview';
};

/**
 * @return true if the logged in user has eventview role
 */
Z.isEventView = function() {
    return R.App.user.role === 'eventview';
};


/**
 * Creates a field which autocompletes a user entered tag ID
 * @param {Object} config The field configuration
 * @return {R.form.SearchField} field
 */
Z.createTagSearchField = function(config) {
    return new R.form.SearchField(Ext.apply({
        displayField     : 'id'
        ,mode            : 'local'
        ,store           : new Ext.data.Store({
            reader : new R.data.ObjectReader({
                fields : [ 'id' ]
            })
        })
        ,triggerAction   : 'all'
        ,validationDelay : 500
        ,width           : 320
        ,lookup          : function(value) {
            Ext.Ajax.request({
                method   : 'GET'
                ,params  : { _rowcnt: 10, _value: value }
                ,url     : R.url('taglist')
                ,scope   : this
                ,success : function(response) {
                    var tags  = Ext.decode(response.responseText);
                    var store = this.store;

                    if (store) {
                        store.removeAll();
                        store.loadData(tags);

                        if (store.getCount() > 0) {
                            this.expand();
                        }
                    }
                }
           });
        }
    }, config));
};

/**
 * Returns a proper URL for contacting the reader.
 * @method
 */
R.url = function(url) {
    return 'zonemgr/api/' + url + '.json';
};

/**
 * @class R.form.DisplayField
 * @extends Ext.form.DisplayField
 * Same as Ext.form.DisplayField except with the option to have the displayed
 * value differ from the value of the field as returned by getValue().
 * @cfg {Function} toText A function to convert a value to a suitable text representation
 */
 R.form.DisplayField = Ext.extend(Ext.form.DisplayField, {

    /**
     * Converts the given value into a text representation.
     * @return {String} text The text representation of the value
     */
    toText : function(value) {
        return value;
    }

    // override
    ,getValue : function() {
        return this.value;
    }

    // override
    ,setValue : function(value) {
        this.setRawValue(this.toText(value));
        this.value = value;
        return this;
    }
});

// custom fields for the default field factory
(function() {
    var factory = R.form.FieldFactory.DefaultFactory;

    factory.registerType('typeref', 'read', {
        onCreate : function(config) {
            return new R.form.DisplayField(Ext.apply({
                toText : function(value) {
                    var et = R.ET.get(value);
                    return et ? et.name : value;
                }
            }, config));
        }
    });

    factory.registerGuid('$zRoles', 'read', {
        setValue : function(value) {
            switch (value) {
            case 'admin': value = 'Administrator'; break;
            case 'configview': value = 'Config View'; break;
            case 'eventview': value = 'Event View'; break;
            }
            this.field.setValue(value);
        }
    });
    factory.registerGuid('$zRoles', 'write', {
        onCreate : function(config) {
            return new Ext.form.ComboBox(Ext.apply(config, {
                editable       : false
                ,store         : [
                    [ 'admin', 'Administrator' ]
                    ,[ 'configview', 'Config View' ]
                    ,[ 'eventview', 'Event View' ]
                ]
                ,mode          : 'local'
                ,triggerAction : 'all'
                ,value         : this.ac.values[0]
            }));
        }
    });

    factory.registerGuid('$zRuleChannelList', 'write', {
        onCreate : function(config) {
            return new Z.ui.ReaderChannelListField(config);
        }
    });

    factory.registerGuid('$zRuleInsideChannelList', 'write', {
        onCreate : function(config) {
            return new Z.ui.ReaderChannelListField(config);
        }
    });

    factory.registerGuid('$zRuleOutsideChannelList', 'write', {
        onCreate : function(config) {
            return new Z.ui.ReaderChannelListField(config);
        }
    });

    factory.registerGuid('$zRuleRefTagList', 'write', {
        onCreate : function(config) {
            config.width = 250;
            return new Z.form.TagListField(config);
        }

        ,toJson : function(params) {
            var value = this.field.getValue();
            if (Ext.isArray(value)) {
                params[this.ac.guid] = value.join(',');
            }
        }
    });

    factory.registerGuid('$zReaderGPSPositionCoord', 'write', {
        onCreate : function(config) {
            return new R.form.LatLonField(config);
        }

        ,toJson : function(params) {
            var value = this.field.getValue();
            if (Ext.isArray(value)) {
                params[this.ac.guid] = value.join(',');
            }
        }
    });

    // PDU asset type fields - only used by AM
    var controller = {
        onCreate : Ext.emptyFn
        ,toJson  : Ext.emptyFn
    };
    factory.registerGuid('$zTagGroupPDUBreakerAssetType',  'read', controller);
    factory.registerGuid('$zTagGroupPDUOutletAssetType',   'read', controller);
    factory.registerGuid('$zTagGroupPDUPhaseAssetType',    'read', controller);
    factory.registerGuid('$zTagGroupPDUFeedLineAssetType', 'read', controller);
    factory.registerGuid('$zTagGroupPDUChannelAssetType',  'read', controller);
    factory.registerGuid('$zTagGroupPDUBreakerAssetType',  'write', controller);
    factory.registerGuid('$zTagGroupPDUOutletAssetType',   'write', controller);
    factory.registerGuid('$zTagGroupPDUPhaseAssetType',    'write', controller);
    factory.registerGuid('$zTagGroupPDUFeedLineAssetType', 'write', controller);
    factory.registerGuid('$zTagGroupPDUChannelAssetType',  'write', controller);
    factory.registerGuid('$zTagGroupPDUSensorAssetType',  'write', controller);

    // eg. IR rule has $aRanger when used on AM, but for ZM UI this does not apply
    factory.registerGuid('$aRanger', 'write', controller);
})();

R.recurseHTMLEncode = function(obj) {
    if (obj) {
        if (typeof obj == 'string') {
            obj = Ext.util.Format.htmlEncode(obj);
        }
        else {
            for (var i in obj) {
                if (typeof obj[i] == 'string') {
                    obj[i] = Ext.util.Format.htmlEncode(obj[i]);
                }
                else if (typeof obj[i] == 'object') {
                    R.recurseHTMLEncode(obj[i]);
                }
            }
        }
    }
    return(obj);
};

/**
 * Add a trigger to the combobox which clears the current value.
 */
Ext.form.ComboBox.override({
    clearable : false

    ,typeAhead  : true

    ,initComponent : Ext.form.ComboBox.prototype.initComponent.createInterceptor(function() {
        if (this.clearable) {
            Ext.apply(this, {
                getTrigger       : Ext.form.TwinTriggerField.prototype.getTrigger
                ,initTrigger     : Ext.form.TwinTriggerField.prototype.initTrigger
                ,onTrigger2Click : Ext.form.ComboBox.prototype.onTriggerClick
                ,trigger2Class   : Ext.form.ComboBox.prototype.triggerClass
                ,onTrigger1Click : function() {
                    this.clearValue();
                    this.fireEvent('select', this, null, null);
                }
                ,trigger1Class   : 'x-form-clear-trigger'
            });
            this.triggerConfig = {
                tag:'span', cls:'x-form-twin-triggers', cn:[
                {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class},
                {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger2Class}
            ]};
        }
        return true;
    })
});

