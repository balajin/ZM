<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ taglib uri="http://jawr.net/tags" prefix="jwr" %>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Zone Manager</title>
    <link rel="shortcut icon" type="image/x-icon" href="brand/images/favicon.ico" />

      <jwr:style src="/bundles/app.css"/>
      <jwr:script src="/bundles/app.js"/>
  </head>
  <body scroll="no">
    <!-- ExtJS history management -->
    <form id="history-form" class="x-hidden">
        <input type="hidden" id="x-history-field"/>
        <iframe id="x-history-frame"></iframe>
    </form>
  </body>
</html>

