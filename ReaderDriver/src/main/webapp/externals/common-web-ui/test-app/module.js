R.ModuleManager.register(new R.Module({
    id : 'dashboard'
    ,label : 'Dashboard'

    ,onCreateUI : function() {
         return new Ext.Panel({
             frame   : true
             ,header : true
             ,html   : '<h1>Dashboard</h1>'
         });
    }
}));

R.ModuleManager.register(new R.Module({
    id : 'status'
    ,label : 'Status'

    ,onCreateUI : function() {
         return new Ext.Panel({
             frame   : true
             ,header : true
             ,html   : '<h1>Status</h1>'
         });
    }

    ,hide : function(proceed) {
        var cb = function(btn) {
            proceed(btn === 'yes');
        };
        Ext.Msg.confirm('Module Change', 'A custom hide module function. Continue?', cb);
    }
}));
