Ext.onReady(function() {
    var factory = new R.AppFactory({
        navigation : function() {
            return new OurNavigation();
        }

        ,status : function(config) {
            return new R.Status({
                left : [
                    { text: 'User: will' }
                ]
                ,right : [
                    { text: 'About', onclick: 'alert(\'Show about window\');' }
                ]
            }).createUI(config);
        }
    });

    R.App.start({
        factory : factory
        ,title  : 'Common UI'
    });
});

OurNavigation = Ext.extend(R.Navigation, {
    constructor : function() {
        OurNavigation.superclass.constructor.call(this);

        this.root = new Ext.tree.TreeNode();
    }

    ,createUI : function(config) {
        this.ui = new Ext.tree.TreePanel(Ext.apply({
            frame        : true
            ,header      : true
            ,root        : this.root
            ,rootVisible : false
        }, config));
        this.ui.getSelectionModel().on('beforeselect', this.onBeforeSelect, this);
        return this.ui;
    }

    /**
     * Adds the given module to the navigation UI.
     */
    ,add : function(module) {
        this.root.appendChild(new Ext.tree.TreeNode({
            mid   : module.id
            ,text : module.label()
        }));
    }

    ,getSelectedModuleID : function() {
        var node = this.ui.getSelectionModel().getSelectedNode();
        if (node) {
            return node.attributes.mid;
        }
    }

    /**
     * Selects the given module. Does not fire a selectionchange event.
     */
    ,select : function(module, params, proceed) {
        var sm = this.ui.getSelectionModel();
        sm.un('beforeselect', this.onBeforeSelect, this);
        sm.select(this.root.findChild('mid', module.id));
        sm.on('beforeselect', this.onBeforeSelect, this);
        proceed();
    }

    // private
    ,onBeforeSelect : function(sm, node) {
        this.fireEvent('selectionchange', node.attributes.mid);
        return false;
    }
});
