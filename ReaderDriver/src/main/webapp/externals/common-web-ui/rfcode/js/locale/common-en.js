
var common = {

    'add'                       : 'Add',
    'edit'                      : 'Edit',
    'delete'                    : 'Delete',
    'up'                        : 'Up',
    'down'                      : 'Down',
    'id'                        : 'ID',
    'name'                      : 'Name',
    'description'               : 'Description',
    'enabled'                   : 'Enabled',
    'disabled'                  : 'Disabled',
    'host'                      : 'Hostname',
    'new'                       : 'New',
    'stop'                      : 'Stop',
    'start'                     : 'Start',
    'password'                  : 'Password',
    'confirmPassword'           : 'Confirm Password',
    'passwordFieldsError'       : 'Password fields do not match',
    'port'                      : 'Port',
    'saveChanges'               : 'Save Changes',
    'saveChangesMessage'        : 'You have unsaved changes.<br>Would you like to save your changes?',
    'ssl'                       : 'SSL',
    'username'                  : 'Username',
    'loading'                   : 'Loading...',
    'saving'                    : 'Saving...',
    'retiring'                  : 'Retiring...',
    'unretiring'                : 'Unretiring...',
    'deleting'                  : 'Deleting...',
    'adding'                    : 'Adding...',
    'parent'                    : 'Parent',
    'cancel'                    : 'Cancel',
    'ok'                        : 'OK',

    'currentPassword'           : 'Current Password',
    'newPassword'               : 'New Password',
    'changeProfile'             : 'Change Profile',
    'changeProfileSuccess'      : 'Profile change succeeded.',
    'changeProfileReload'       : 'Profile change succeeded.<br>You need to reload your browser for changes to take effect.',
    'changeProfileFailed'       : 'Profile change failed.',

    'save'                     : 'Save',
    'dontsave'                  : 'Don\'t Save'

    ,'online'                   : 'Online'
    ,'offline'                  : 'Offline'

    // buttons
    ,'button-add'               : 'Add'
    ,'button-collapse-all'      : 'Collapse All'
    ,'button-copy'              : 'Copy'
    ,'button-delete'            : 'Delete'
    ,'button-edit'              : 'Edit'
    ,'button-expand-all'        : 'Expand All'
    ,'button-export'            : 'Export'
    ,'button-new'               : 'New'
    ,'button-save'              : 'Save Changes'
    ,'button-test'              : 'Test'
    ,'button-testldap'          : 'Test LDAP User'
    ,'button-view'              : 'View'
    ,'button-pause'             : 'Pause'
    ,'button-play'              : 'Play'
    ,'button-resolve'           : 'Resolve'
    ,'button-group'             : 'Groups'
};

new RFC_nls().registerBundle('common', common);
