/**
 * @class R.ui.MultiAction
 * @extends R.ui.Action
 * An action which performs an update on multiple items.
 */
R.ui.MultiAction = Ext.extend(R.ui.Action, {
    update : function() {
        this.setDisabled(this.sm.getCount() < 1);
    }

    ,getSelections : function(){
        return this.sm.getSelections();
    }
});

