/**
 * @class R.grid.GridPanel
 * @extends Ext.grid.GridPanel
 * GridPanel which adds a PagingToolbar if requested and does automatic
 * configuration of the page size based on the GridPanel's dimensions
 */
R.grid.GridPanel = Ext.extend(Ext.grid.GridPanel, {
    /**
     * @cfg {Boolean} autoLoadStore true to load automatically load the store
     * when the grid panel is first rendered
     */
    autoLoadStore : true

    /**
     * @cfg {Boolean} showPaging true to show a paging toolbar.
     */
    ,showPaging : true

    // private - contains the calculated grid row height
    ,calcRowHeight : -1

    // override
    ,initComponent : function() {
        if (this.showPaging) {
            this.initPageUI();
        }

        R.grid.GridPanel.superclass.initComponent.apply(this, arguments);
    }

    // private
    ,initPageUI : function() {
        if (this.showPageSize) {
            this.pageSizer = new Ext.ux.grid.PageSizer({
                sizes : [ 'Fit', 25, 50, 100, 200 ]
                ,comboCfg : {width: 55}
            });
        }

        this.pagingToolbar = new Ext.PagingToolbar({
            displayInfo    : true
            ,pageSize      : 'Fit'
            ,paramNames    : this.paramNames || { start: 'start', limit: 'limit' }
            ,store         : this.store
            ,displayMsg    : '{0} - {1} of {2}'
            ,fitPageSize   : -1
            ,plugins       : this.showPageSize ? this.pageSizer : []
            ,updateButtons : function() {
                var d = this.getPageData(), ap = d.activePage, ps = d.pages;

                this.afterTextItem.setText(String.format(this.afterPageText, d.pages));
                this.inputItem.setValue(ap);
                this.first.setDisabled(ap == 1);
                this.prev.setDisabled(ap == 1);
                this.next.setDisabled(ap == ps);
                this.last.setDisabled(ap == ps);

                this.updateInfo();
            }
        });
        this.bbar = this.pagingToolbar;
        
        this.on('columnresize', this.fixHorizontalScrollBar, this);

        this.on('resize', function() {
            this.calculateRowHeight();

            var scroller       = this.el.child('.x-grid3-scroller');
            var oldFitPageSize = this.pagingToolbar.fitPageSize;
            var newFitPageSize = Math.max(Math.floor(scroller.getHeight() / R.grid.GridPanel.prototype.calcRowHeight), 1);

            // no need to reload of page size did not change
            if (oldFitPageSize !== newFitPageSize) {
                this.pagingToolbar.fitPageSize = newFitPageSize;

                // lastOptions is set if the store has already loaded at least once
                // Only load if size is 'fit' or we have not loaded yet
                var lastOptions = this.store.lastOptions;
                if ((lastOptions && lastOptions.fit) || this.autoLoadStore) {
                    var opts = { params: {} };
                    opts.params[this.store.paramNames.limit] = this.pagingToolbar.fitPageSize;
                    this.store.reload(opts);
                }
            }

            this.fixHorizontalScrollBar();
        }, this, { buffer: 100 });

        this.store.on('add', this.pagingToolbar.updateButtons, this.pagingToolbar);
        this.store.on('remove', this.pagingToolbar.updateButtons, this.pagingToolbar);
        this.store.on('beforeload', this.onStoreBeforeLoad, this);
    }

    // override
    ,reconfigure : function(store, colModel) {
        if (this.store) {
            this.store.un('add', this.pagingToolbar.updateButtons, this.pagingToolbar);
            this.store.un('remove', this.pagingToolbar.updateButtons, this.pagingToolbar);
            this.store.un('beforeload', this.onStoreBeforeLoad, this);
        }
        store.on('add', this.pagingToolbar.updateButtons, this.pagingToolbar);
        store.on('remove', this.pagingToolbar.updateButtons, this.pagingToolbar);
        store.on('beforeload', this.onStoreBeforeLoad, this);

        R.grid.GridPanel.superclass.reconfigure.call(this, store, colModel);
    }

    // private
    ,onStoreBeforeLoad : function(store, options) {
        var tb = this.pagingToolbar;

        // allows store.load() to work without having to provide any
        // parameters (eg. first, count)
        options.params = options.params || {};

        var pn = store.paramNames;
        var limit = options.params[pn.limit];

        if (limit === 'Fit' || limit === 'fit' || Ext.isEmpty(limit)) {
            options.fit = true;
            options.params[pn.limit] = tb.fitPageSize;

            // setting autoLoadStore ensures the resize will load the store if
            // not already loaded
            this.autoLoadStore = true;
        }
        if (Ext.isEmpty(options.params[pn.start])) {
            options.params[pn.start] = 0;
        }

        var limit = options.params[pn.limit];
        if (limit != -1) {
            this.pagingToolbar.pageSize = limit;
            return true;
        }
        // make sure we have calculated the page size before we attempt to load
        return false;
    }

    /**
     * Returns the current page size
     * @return {Mixed} 'fit' or a number
     */
    ,getPageSize : function() {
        var size = -1;
        if (this.pageSizer) {
            size = this.pageSizer.combo.getValue();
            if (size == 'Fit') {
                size = 'fit'
            }
        }
        return size;
    }

    // private
    ,calculateRowHeight : function() {
        var div, grid;

        if (R.grid.GridPanel.prototype.calcRowHeight === -1) {
            grid = new Ext.grid.GridPanel({
                columns : [
                    { header: 'Company' }
                ]
                ,deferRowRender : false
                ,enabledColumnHide : false
                ,enableColumnMove : false
                ,enabledColumnResize : false
                ,enabledHdMenu : false
                ,store  : new Ext.data.SimpleStore({
                    data    : [ 'RFCode' ]
                    ,fields : [ 'company' ]
                })
                ,viewConfig : { foo : 'bar' }
            });

            div = document.createElement('div');
            div.style.visibility = 'hidden';
            document.body.appendChild(div);
            grid.render(div);

            var row = grid.el.child('.x-grid3-row');
            R.grid.GridPanel.prototype.calcRowHeight = row.getHeight();
            Ext.destroy(grid);
        }
    }

    /**
     * Fixes a bug in which the horizontal scrollbar does not appear if the
     * columns are wider than the available width of the grid panel.
     */
    ,fixHorizontalScrollBar : function() {
        // view or view.cm is null if the grid was destroyed before it
        // fully loaded its UI
        var view = this.getView();
        if (view && view.cm) {
            var style = view.mainBody.dom.style;
            style.width = view.getTotalWidth();
            style.height = '1px';
        }
    }
});
