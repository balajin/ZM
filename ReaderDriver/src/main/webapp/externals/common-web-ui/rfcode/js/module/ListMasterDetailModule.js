/**
 * @class R.module.ListMasterDetailModule
 * @extends R.Module
 * A module which displays a list of items (master) and a details panel to
 * add/edit items to the list.
 * @cfg {String} deleteURL The URL used to delete a record
 * @cfg {String} nameField The underlying data field name to use for displaying
 * a representation of a record in the list.
 * @cfg {String} itemCls (optional) The class to use for each list item
 * @cfg {String} itemIconCls (optional) The icon class to use for each list item
 */
R.module.ListMasterDetailModule = Ext.extend(R.Module, {

    constructor : function(config) {
        R.module.ListMasterDetailModule.superclass.constructor.call(this, config);

        this.addEvents(
            /**
             * @event add
             * Fired after a user clicks the add button.
             * @param {R.Module} this
             */
            'add'
            /**
             * @event edit
             * Fired after a record is selected for edit.
             * @param {R.Module} this
             * @param {Ext.data.Record} The selected record.
             */
            ,'edit'
        );

        /**
         * ID of the last selected item.
         * @property selection
         */
        this.selection = null;

        this.on('refresh', function() {
            if (this.selection) {
                this.select(this.selection);
            }
        }, this);
    }

    // override
    ,createUI : function() {
        /**
         * The component representing the details
         * @property detail
         */
        this.detail = this.createDetailComponent();
        this.detail.region = 'center';

        this.store = this.initStore();
        this.tree = new R.tree.FilterTreePanel({
            frame   : true
            ,header : true
            ,hideExpandCollapse : true
            ,region : 'west'
            ,root   : new Ext.tree.TreeNode()
            ,rootVisible : false
            ,split  : true
            ,width  : 200
        });
        this.tree.getSelectionModel().on('selectionchange', function(sm) {
            var node = sm.getSelectedNode();
            var record;

            if (node) {
                record = node.attributes.record;
                this.selection = node.id;
                this.onEdit(record);
                this.fireEvent('edit', this, record);
            }
            else {
                delete this.selection;
                this.add();
            }
        }, this);

        // optional actions
        var actions = this.createActions();
        var plugins;
        if (actions) {
            plugins = new R.tree.TreeActions({
                tree   : this.tree
                ,items : actions
            });
        }
        return (this.component = this.onCreateUI(plugins));
    }

    /**
     * @param {Array} plugins Optional list of plugins for the UI component.
     */
    ,onCreateUI : function(plugins) {
        return new Ext.Panel({
            layout   : 'border'
            ,items   : [ this.tree, this.detail ]
            ,plugins : plugins
        });
    }

    /**
     * Returns an array of action objects used by R.tree.TreeActions. By default
     * returns add and delete actions.
     * @return {Array} actions Array of R.ui.Action objects
     */
    ,createActions : function() {
        return [ this.createAddAction(), this.createDeleteAction() ];
    }

    // private
    ,initStore : function() {
        var store = this.createStore();
        store.on('load', function(store) {
            var tree     = this.tree;
            var sm       = tree.getSelectionModel();
            var root     = new Ext.tree.TreeNode();
            var selected = sm.getSelectedNode();

            store.each(function(r) {
                this.appendChild(root, r);
            }, this);
            tree.setRootNode(root);

            if (selected) {
                selected = tree.getNodeById(selected.id);
                if (selected) {
                    sm.select(selected);
                }
            }
            else {
                this.add();
            }
        }, this);
        store.on('clear', function() {
            this.tree.getRootNode().removeChildren();
        }, this);
        store.on('add', function(store, records) {
            var root = this.tree.getRootNode();
            Ext.each(records, function(r) {
                this.appendChild(root, r);
            }, this);
        }, this);
        store.on('remove', function(store, record) {
            var node = this.tree.getNodeById(record.id);
            if (node) {
                node.remove();
            }
        }, this);
        return store;
    }

    // private
    ,appendChild : function(parent, r) {
        parent.appendChild(new Ext.tree.TreeNode(this.onCreateItem({
            id       : r.id
            ,cls     : this.itemCls
            ,iconCls : this.itemIconCls
            ,leaf    : true
            ,text    : r.get(this.nameField) || r.id
            ,record  : r
        })));
    }

    /**
     * Returns the configuration object for creating a new item.
     * @param {Object} config The item's config object
     */
    ,onCreateItem : function(config) {
        return config;
    }

    // override
    ,refresh : function() {
        var store = this.store;
        store.on('load', this.callback(function() {
            this.fireEvent('refresh', this);
        }), this, { single: true });
        store.load();
    }

    // override
    ,onCleanup : function() {
        Ext.destroy(this.store);
        delete this.store;
    }

    /**
     * Selects the record with the given ID
     * @param {String} id The ID of the record to select.
     */
    ,select : function(id) {
        var tree = this.tree;
        var node = tree.getNodeById(id);
        if (node) {
            tree.getSelectionModel().select(node);
        }
    }

    /**
     * Sets the text on the item with the given id.
     * @param {String} id ID of the record to update
     * @param {String} text text for the item
     * @param {String} iconCls icon class for the item
     * @return {Ext.tree.TreeNode} The updated node if it exists
     */
    ,setText : function(id, text, iconCls) {
        var node = this.tree.getNodeById(id);
        if (node) {
            node.setText(text);

            iconCls = iconCls || this.itemIconCls;
            if (node.attributes.iconCls != iconCls) {
                node.setIconCls(iconCls);
            }
        }
        return node;
    }

    /**
     * Adds a new record.
     * @method
     */
    ,add : function() {
        var sm = this.tree.getSelectionModel();
        sm.suspendEvents();
        sm.clearSelections();
        sm.resumeEvents();
        this.onAdd();
        this.fireEvent('add', this);
    }

    /**
     * Returns a component used to display the details of the selected record.
     * @param {Object} config Configuration options
     * @return {Ext.Component}
     */
    ,createDetailComponent : Ext.emptyFn

    /**
     * Returns a store used by the list.
     * @return {Ext.data.Store}
     */
    ,createStore : Ext.emptyFn

    /**
     * Called when an existing record is selected.
     * @param {Ext.data.Record} record The selected record
     */
    ,onEdit : Ext.emptyFn

    /**
     * Called when the add button is clicked or a row is unselected.
     * @method
     */
    ,onAdd : Ext.emptyFn

    // private
    ,createAddAction : function() {
        return new R.ui.Action({
            tbar     : true
            ,text    : 'New'
            ,cls     : 'button-new x-btn-text-icon'
            ,scope   : this
            ,handler : this.add
        });
    }

    // private
    ,createDeleteAction : function() {
        return new R.ui.SingleAction({
            tbar     : true
            ,text    : 'Delete'
            ,cls     : 'button-delete x-btn-text-icon'
            ,scope   : this
            ,handler : function() {
                var node = this.tree.getSelectionModel().getSelectedNode();
                var record = node.attributes.record;

                R.ui.Msg.remove({
                    title   : 'Delete'
                    ,msg    : 'Are you sure you want to delete ' + record.get(this.nameField) + '?'
                    ,scope  : this
                    ,remove : function() {
                        var params =  {id : record.id };

                        Ext.Ajax.request({
                            method   : 'POST'
                            ,url     : this.deleteURL
                            ,params  : params
                            ,scope   : this.refresh ? this : this.store
                            ,success : function(response) {
                                var json = Ext.decode(response.responseText);
                                if (json['__result'] == 'error') {
                                    Ext.Msg.alert('Error', json['__msg']);
                                }
                                else {
                                    this.store.remove(record);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    /**
     * Called when an item is deleted
     * @param {string} form id of the deleted item
     */
    ,onDelete : Ext.emptyFn
});
