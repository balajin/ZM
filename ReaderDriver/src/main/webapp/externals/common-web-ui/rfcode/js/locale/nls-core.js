//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2008. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//------------------------------ tabstop = 4 ----------------------------------

function RFC_nls() {
    if (!RFC_nls.instance) {
        this.bundles = { };
        RFC_nls.instance = this;
    }
    return RFC_nls.instance;
}

RFC_nls.instance = null; //public static member


RFC_nls.prototype = {

    getString : function(key) {
        var str = null;
        if (key != null) {
            for (bundleName in this.bundles) {
                if (this.bundles[bundleName][key] != null) {
                    str = this.bundles[bundleName][key];
                    break;
                }
            }
        }
        if (str != null) {
            var args = [].slice.call( arguments, 0 );
            args[0] = str;
            str = String.format.apply(this, args);
        }
        else {
            str = key;
        }
        return str;
    },

    getStringFromBundle : function(bundle, key) {
        var str = null;
        if (arguments.length >= 2) {
            if (this.bundles[bundle] != null) {
                str = this.bundles[bundle][key];
            }
        }
        if (str != null) {
            var args = [].slice.call( arguments, 0 );
            args[0] = str;
            str = String.format.apply(this, args);
        }
        return str;
    },

    registerBundle : function(bundleName, bundle) {
        this.bundles[bundleName] = bundle;
    }
}
