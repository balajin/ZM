/**
 * @class R.form.MutualChoiceField
 * @extends Ext.Container
 * A field which allows a user to choose a whole set of values or a subset.
 * @cfg {String} choice1Label The label for the first radio button (all values)
 * @cfg {String} choice2Label The label for the second radio button (subset of values)
 * @cfg {Object} choice2Field The field for selecting a subset of values
 */
R.form.MutualChoiceField = Ext.extend(Ext.form.Field, {

    defaultAutoCreate : { tag: 'div' }

    ,focusClass : undefined

    ,initComponent : function() {
        R.form.MutualChoiceField.superclass.initComponent.call(this);

        var name = Ext.id();

        var listeners = {
            'check' : {
                scope : this
                ,fn   : function() {
                    this.choice2Field.setDisabled(this.choice1Radio.getValue());
                }
            }
        };

        this.choice1Radio = new Ext.form.Radio({
            hideLabel       : true
            ,checked        : true
            ,labelSeparator : ''
            ,name           : name
            ,boxLabel       : this.choice1Label
            ,listeners      : listeners
        });

        this.choice2Radio = new Ext.form.Radio({
            hideLabel       : true
            ,name           : name
            ,labelSeparator : ''
            ,boxLabel       : this.choice2Label
            ,listeners      : listeners
        });
        this.choice2Field.disable();
        this.choice2Field.width = this.width;
    }

    // private
    ,onRender : function(ct, position) {
        R.form.MutualChoiceField.superclass.onRender.call(this, ct, position);

        // Ext.Container does not work in IE
        var ui = new Ext.Panel({
            layout    : 'anchor'
            ,renderTo : this.el
            ,items    : [ this.choice1Radio, this.choice2Radio, this.choice2Field ]
        });
    }

    ,setDisabled : function(v) {
        this.choice1Radio.setDisabled(v);
        this.choice2Radio.setDisabled(v);
        this.choice2Field.setDisabled(v);
    }

    ,initValue : function() {
        if (this.value !== undefined) {
            this.setValue(this.value);
        }
        this.originalValue = this.getValue();
    }

    /**
     * @return null if the first choice is selected
     */
    ,getValue : function() {
        var value = null;
        if (this.choice2Radio.getValue()) {
            value = this.choice2Field.getValue();
        }
        return value;
    }

    ,setValue : function(value) {
        if (value) {
            this.choice1Radio.setValue(false);
            this.choice2Radio.setValue(true);
            if (this.choice2Field) {
                this.choice2Field.setValue(value);
            }
        }
        else {
            this.choice1Radio.setValue(true);
            this.choice2Radio.setValue(false);
        }
    }
});
