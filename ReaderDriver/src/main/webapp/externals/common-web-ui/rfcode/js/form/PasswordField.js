/**
 * @class R.form.PasswordField
 * @extends Ext.Panel
 */
R.form.PasswordField = Ext.extend(Ext.Container, {

    autoEl : { tag: 'div' }

    /**
     * @cfg {String} confirmLabel
     * The label to use for the confirm password textfield
     */
    ,confirmLabel : 'Confirm Password'

    ,hideLabel : true
    ,isFormField : true

    ,constructor : function(config) {
        var id = config.id;
        var name = config.name;
        var validator = config.validator;

        delete config.id;
        delete config.name;
        delete config.validator;

        this.password = new Ext.form.TextField(Ext.applyIf({
            inputType      : 'password'
            ,isDirty       : Ext.emptyFn
            ,isFormField   : false
            ,selectOnFocus : true
            ,style         : 'margin-bottom: 4px'
            ,getErrorCt    : this.getErrorCt
            ,markInvalid   : null // so BasicForm does not add it to its field collection
        }, config));
        this.confirm = new Ext.form.TextField(Ext.applyIf({
            inputType      : 'password'
            ,isDirty       : Ext.emptyFn
            ,isFormField   : false
            ,selectOnFocus : true
            ,getErrorCt    : this.getErrorCt
            ,markInvalid   : null // so BasicForm does not add it to its field collection
        }, config));

        var labelSeparator = Ext.layout.FormLayout.prototype.labelSeparator || config.labelSeparator;
        var fieldLabel = config.fieldLabel || this.fieldLabel;
        var confirmLabel = config.confirmLabel || this.confirmLabel;

        R.form.PasswordField.superclass.constructor.call(this, {
            cls           : 'r-password-field'
            ,confirmLabel : config.confirmLabel || this.confirmLabel
            ,fieldLabel   : fieldLabel
            ,id           : id
            ,labelSeparator : labelSeparator
            ,layout       : 'table'
            ,layoutConfig : { columns: 2 }
            ,name         : name
            ,validator    : validator
            ,value        : config.value
            ,items        : [
                this.createLabel(fieldLabel + labelSeparator, true)
                ,this.password
                ,this.createLabel(confirmLabel + labelSeparator)
                ,this.confirm
            ]
        });
    }

    ,resetOriginalValue : function(value) {
        this.setValue('buerkulawashere');
        this.originalValue = 'buerkulawashere';
    }

    /**
     * Marks this field as having a value. Used to indicate to the user a
     * password has been set even though the UI does not know the actual
     * password value. getValue returns null if the password value does not
     * change after calling this method.
     */
    ,setHasValue : function() {
        this.setValue('buerkulawashere');
        this.originalValue = 'buerkulawashere';
    }

    /**
     * Returns true if the user has entered a value.
     * @return {Boolean} true if user entered a value
     */
    ,hasUserEnteredValue : function() {
        return this.password.getValue() !== 'buerkulawashere';
    }

    // override
    ,render : function() {
        for (var i = 0; i < this.items.getCount(); i++) {
            var item = this.items.get(i);
            if (item instanceof Ext.form.Label) {
                item.width = this.ownerCt.labelWidth + 5;
            }
        }
        R.form.PasswordField.superclass.render.apply(this, arguments);

        if (this.value !== undefined) {
            this.setHasValue();
        }
        else {
            this.originalValue = this.password.getValue() || '';
        }
    }

    // private
    ,createLabel : function(text, password) {
        return new Ext.form.Label({
            cls    : 'x-form-item'
            ,style : password ? 'margin-bottom: 4px' : ''
            ,html  : text
            ,width : 75
        });
    }

    // override
    ,getName : Ext.form.Field.prototype.getName

    // override
    ,disable : function() {
         R.form.PasswordField.superclass.disable.call(this);
         this.password.disable();
         this.confirm.disable();
    }

    // override - prevent label from getting disabled
    ,onDisable : Ext.emptyFn

    // override
    ,enable : function() {
         R.form.PasswordField.superclass.enable.call(this);
         this.password.enable();
         this.confirm.enable();
    }

    // override - prevent label from getting disabled
    ,onEnable : Ext.emptyFn

    // override
    ,getValue : function() {
        var v = this.password.getValue();
        if (v === 'buerkulawashere') {
            v = null;
        }
        return v;
    }

    // override
    ,setValue : function(value) {
        this.password.setValue(value);
        this.confirm.setValue(value);
    }

    // override
    ,initValue : function(value) {
        this.password.setValue(value);
        this.confirm.setValue(value);
        this.originalValue = value;
    }

    // override
    ,isDirty : function() {
        var ov = String(this.originalValue);
        return ov !== String(this.password.getValue()) || ov !== String(this.confirm.getValue());
    }

    // override
    ,validate : Ext.form.Field.prototype.validate

    // private - needed by validate
    ,processValue : function(value) {
        return value;
    }

    // private - needed by validate
    ,getRawValue : function() {
        return this.password.getRawValue();
    }

    // private
    ,validateValue : function() {
        var password = this.password.getValue();
        if (password !== this.confirm.getValue()) {
            this.markInvalid('Passwords do not match');
            return false;
        }
        else if (typeof this.validator === 'function') {
            var msg = this.validator(password);
            if (msg !== true) {
                this.markInvalid(msg);
                return false;
            }
        }
        return this.password.validate();
    }

    // override
    ,clearInvalid : function() {
        this.password.clearInvalid();
        this.confirm.clearInvalid();
    }

    // override
    ,markInvalid : function(msg) {
        if (this.rendered && !this.preventMark) {
            Ext.form.Field.prototype.markInvalid.call(this.password, msg);
            Ext.form.Field.prototype.markInvalid.call(this.confirm, msg);
        }
    }

    // override
    ,isValid : function() {
        // Always return true. Before the form is submitted it is validated
        // using validate() in which case an error message will display if
        // passwords are different. We want the save button to be enabled if
        // this field is dirty regardless of validation so we get a nice error
        // on trying to save.
        return true;
    }

    ,getErrorCt : function() {
        return this.el.findParent('td', 5, true);
    }

    ,reset : Ext.form.Field.prototype.reset
});
Ext.reg('r-password', R.form.PasswordField);
