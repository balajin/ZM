/**
 * @class R.form.StringListField
 * @extends R.ui.GridPanelField
 * A field for selecting a list of available strings.
 * @cfg {String} msgText (optional) Text to show as the message of the prompt dialog
 */
R.form.StringListField = Ext.extend(R.form.GridPanelField, {

    cls : 'string-list-field'

    ,constructor : function(config) {
        R.form.StringListField.superclass.constructor.call(this, Ext.apply({
            columns      : [ { dataIndex: 'value' } ]
            ,hideHeaders : true
            ,store       : new Ext.data.ArrayStore({
                expandData : true
                ,fields    : [ 'value' ]
                ,pruneModifiedRecords : true
            })
        }, config));
    }

    // override
    ,getValue : function() {
        return this.store.collect('value');
    }

    // override
    ,setValue : function(value) {
        this.store.removeAll();
        if (value) {
            this.store.loadData(value);
        }
    }

    // override
    ,reset : function() {
        this.store.commitChanges();
    }

    // override
    ,isDirty : function() {
        if (this.disabled || !this.editable) {
            return false;
        }

        var ov = this.originalValue || [];
        var store = this.store;
        var count = store.getCount();

        if (store.modified.length > 0 || ov.length !== count) {
            return true;
        }
        for (var i = 0; i < count; i++) {
            if (ov[i] !== store.getAt(i).data.value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a string to the field.
     */
    ,addString : function(text) {
        var store = this.store;
        var index, recs;

        if (text.length > 0) {
            index = store.findExact('value', text);

            if (index === -1) {
                recs = [ new Ext.data.Record({ value: text }) ];
                store.add(recs);
            }
            else {
                recs = [ store.getAt(index) ];
            }
            this.getSelectionModel().selectRecords(recs);
        }
    }

    /**
     * Edits an existing string.
     */
    ,editString : function(oldValue, newValue) {
        var store = this.store;
        var indexOldValue = store.findExact('value', oldValue);
        var indexNewValue, record;

        if (indexOldValue !== -1 && newValue.length > 0) {
            indexNewValue = store.findExact('value', newValue);
            record = store.getAt(indexOldValue);

            if (indexNewValue === -1) {
                record.set('value', newValue);
            }
            else {
                // don't add dup
                store.remove(record);
                record = store.getAt(indexNewValue);
            }
            this.getSelectionModel().selectRecords([record]);
            this.clearInvalid();
        }
    }

    // override
    ,onAddAction : function() {
        var cb = function(btn, text) {
            if (btn === 'ok') {
                this.addString(text.trim());
            }
        };
        Ext.Msg.prompt('Add Value', this.msgText || 'Value:', cb, this);
    }

    // override
    ,onEditAction : function(record) {
        Ext.Msg.show({
            title    : 'Edit Value'
            ,msg     : this.msgText || 'Value:'
            ,value   : record.get('value')
            ,width   : 300
            ,prompt  : true
            ,buttons : Ext.Msg.OKCANCEL
            ,icon    : Ext.Msg.QUESTION
            ,scope   : this
            ,fn      : function(btn, text) {
                if (btn === 'ok') {
                    this.editString(record.get('value'), text.trim());
                }
            }
        });
    }
});
Ext.reg('string-list', R.form.StringListField);
