(function() {
    var _mappers = {};
    var _initialized = false;

    var _Record = Ext.data.Record.create([
        { name : 'name' }, { name : 'value' }
    ]);

    /**
     * @class R.data.Tag
     * Represents a tag as returned by a tagprint URL.
     */
    R.data.Tag = function(config) {
        if (config instanceof Ext.data.Record) {
            this.attrs = {};

            for (var key in config.data) {
                this.attrs[key] = config.data[key];
            }
            this.attrs.taglinks = config.data.taglinks;
        }
        else {
            this.attrs = {
                taglinks : config.taglinks
            };
            Ext.apply(this.attrs, config.attributes);
        }
        this.id = config.id;
    };

    R.data.Tag.prototype = {
        /**
         * Returns the tag values as an array of Ext.data.Record objects
         * @method
         */
        records : function() {
            var mappers = getMappers();
            var recs = [];
            var attrs = this.attrs;
            var i, id, mapper, value;

            for (id in attrs) {
                mapper = mappers[id];
                value = attrs[id];
                if (mapper && mapper.record) {
                    mapper.record(recs, id, value);
                }
                else if (id !== 'tagid') {
                    recs.push(new _Record({ name: id, value: value }, id));
                }
            }

            // add tag field to records
            for (i = 0; i < recs.length; i++) {
                recs[i].data.tag = this;
            }
            return recs;
        }

        /**
         * Returns the renderer for the given tag record.
         * @method
         */
        ,renderer : function(value, metadata, record) {
            var mapper = getMappers()[record.get('type')];
            if (mapper && mapper.renderer) {
                return mapper.renderer.apply(mapper, arguments);
            }
        }
    };

    /**
     * Returns the label for the given tag attribute.
     * @method
     * @static
     * @param {String} attr The tag attribute
     */
    R.data.Tag.label = function(attr) {
        var mapper = getMappers()[attr];
        return mapper ? mapper.name : '';
    };

    /**
     * Returns a new renderer for the given tag attribute.
     * @method
     * @static
     * @param {String} attr The tag attribute
     */
    R.data.Tag.renderer = function(attr) {
        var mapper = getMappers()[attr];
        if (mapper && mapper.renderer) {
            // renderer scope must be mapper
            return mapper.renderer.createDelegate(mapper);
        }
    };

    var getMappers = function() {
        if (_initialized === false) {
            initialize();
            _initialized = true;
        }
        return _mappers;
    };

    var push = function(recs, type, id, name, value) {
        recs.push(new _Record({ name: name, value: value, type: type}, id));
    };

    var Mapper = function(config) {
        Ext.apply(this, config);
    };
    Mapper.prototype = {
        record : function(recs, id, value) {
            push(recs, id, id, this.name, value);
        }

        ,renderer : function(value) {
            if (Ext.isEmpty(value)) {
                value = '';
            }
            else if (this.units) {
                value = this.units.tolocal(value);
                value = value.toFixed(1);
                value = value + this.units.shortlabel();
            }
            else if (this.bool) {
                if (typeof value === 'string') {
                    value = value === 'true';
                }
                value = this.bool[value ? 1 : 0];
            }
            return value;
        }
    };

    // boolean value renderer info
    var tagstate = [ 'No', '<span style="color:red;">Yes</span>' ];
    var doorstate = [ 'Closed', 'Open' ];

    var map = function(id, v) {
        _mappers[id] = new Mapper(v);
    };

    var initialize = function() {
        var tags = R.ET.get('$zTag').leaves();
        var acs  = {};
        var ac, config, guid, zname;

        // find each tag attribute
        Ext.each(tags, function(tag) {
            Ext.each(tag.getETAs(), function(eta) {
                var ac = acs[eta.guid];
                if (!ac) {
                    acs[eta.guid] = R.AC.get(eta.guid);
                }
            });
        });

        // now create mapper for each attribute class
        for (guid in acs) {
            ac = acs[guid];
            zname = ac['$zName'];
            config = {
                name   : ac.name
                ,type  : ac.type
                ,units : ac.units
            };

            if (ac.type === 'bool') {
                if (zname === 'dooropen' || zname === 'dryopen') {
                    config.bool = doorstate;
                }
                else {
                    config.bool = tagstate;
                }
            }

            map(zname, config);
        }

        // renames Tag Location to Location
        map('locationzone', { name: 'Location' });

        // the rest are not in the datamodel
        map('irlocator', { name: 'Room Locator' });
        map('taggroupid', { name: 'Tag Group ID' });
        map('tagtype', { name: 'Tag Type' });

        map('confidencebyrule', {
            record : function(recs, id, value) {
                var rule;
                for (var rule in value) {
                    push(recs, 'confidencebyrule', rule, 'Rule: ' + rule, value[rule]);
                }
            }

            ,renderer : function(value) {
                return 'Confidence: ' + value;
            }
        });
        map('taglinks', {
            record : function(recs, id, value) {
                var i, link;
                if (value) {
                    for (i = 0; i < value.length; i++) {
                        link = value[i];
                        push(recs, 'taglinks', link.channelid, link.channelid, link.ssi);
                    }
                }
            }

            ,renderer : function(value) {
                return 'SSI: ' + value;
            }
        });
    };
})();
