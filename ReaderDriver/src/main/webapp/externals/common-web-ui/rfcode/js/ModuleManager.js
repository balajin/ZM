/**
 * @class R.ModuleManager
 */
R.ModuleManager = function() {

    var _list = new Ext.util.MixedCollection();
    var _aliases = {};

    return {
        /**
         * Returns the module with the given ID
         * @param {String} id
         */
        get: function(id) {
            return _aliases[id] || _list.get(id);
        }

        /**
         * Calls the function fn for each module in the order in which the
         * modules were registered.
         * @param {Function} fn The function to call
         * @param {Object} scope (optional)
         */
        ,each: function(fn, scope) {
            return _list.each(fn, scope);
        }

        /**
         * Registers a module.
         * @param {R.Module} module
         */
        ,register : function(module) {
            var id = module.id;
            if (_list.containsKey(id)) {
                alert('Module [' + id + '] already registered');
            }
            else {
                _list.add(id, module);

                if (module.alias) {
                    _aliases[module.alias] = module;
                }
            }
        }

        /**
         * Returns the first allowed module.
         */
        ,first : function() {
            return _list.find(function(m) {
                return m.isAllowed();
            });
        }

        ,debug : function() {
            var list = [];
            var i;
            for (i = 0; i < _list.getCount(); i++) {
                list.push(_list.itemAt(i));
            }
            list.sort(R.compare('id'));
            _.each(list, function(m) { m.debug(); });
        }
    };
}();

