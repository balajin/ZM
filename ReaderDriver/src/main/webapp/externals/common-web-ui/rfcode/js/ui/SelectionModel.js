/**
 * @class R.ui.SelectionModel
 * An object which presents a common read-only selection model interface to
 * clients. Wraps Ext.grid.RowSelectionModel, Ext.tree.DefaultSelectionModel and
 * Ext.tree.MultiSelectionModel classes.
 */
(function() {
    // single selection tree
    var stree = {
        getCount : function() {
            var node = this.sm.getSelectedNode();
            return node ? 1 : 0;
        }
        ,getSelected : function() {
            var node = this.sm.getSelectedNode();
            return node ? new Ext.data.Record(node.attributes) : null;
        }
        ,getSelections : function() {
            var result = [];
            var sel = this.getSelected();
            if (sel) {
                result.push(sel);
            }
            return result;
        }
    };

    // multi selection tree
    var mtree = {
        getCount : function() {
            return this.sm.getSelectedNodes().length;
        }
        ,getSelected : function() {
            var nodes = this.sm.getSelectedNodes();
            if (nodes.length > 0) {
                return new Ext.data.Record(nodes[0].attributes);
            }
        }
        ,getSelections : function() {
            var nodes = this.sm.getSelectedNodes();
            var result = [];
            if (nodes.length > 0) {
                for (var i = 0; i < nodes.length; i++) {
                    result.push(new Ext.data.Record(nodes[i].attributes));
                }
            }
            return result;
        }
    };

    // single object
    var sobj = {
        getCount : function() {
            return 1;
        }
        ,getSelected : function() {
            return this.selection;
        }
        ,getSelections : function() {
            return [ this.selection ];
        }
    };

    R.ui.SelectionModel = function(sm) {
        this.sm = sm;

        if (sm) {
            if (sm.getCount) {
                return sm;
            }
            else if (sm.getSelectedNode) {
                Ext.apply(this, stree);
            }
            else if (sm.getSelectedNodes) {
                Ext.apply(this, mtree);
            }
            else {
                Ext.apply(this, sobj);
                this.selection = new Ext.data.Record(sm);
            }
        }
        else {
            Ext.apply(this, sobj);
        }
    };
})();

R.ui.SelectionModel.prototype = {
    /**
     * Returns the wrapped selection model.
     */
    delegate : function() {
        return this.sm || this;
    }
};

