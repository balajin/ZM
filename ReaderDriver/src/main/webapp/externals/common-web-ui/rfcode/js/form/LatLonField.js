/**
 * @class R.form.LatLonCoordField
 * @extends Ext.form.Field
 * A field which allows a user to specify a GPS Coord
 */
R.form.LatLonField = Ext.extend(Ext.form.TextField, {
    /**
     * @return {Array} array containing the two coordinates
     */
    getValue : function() {
        var value = R.form.LatLonField.superclass.getValue.call(this);
        if (Ext.isString(value)) {
            value = R.AC.prototype.parseCoordinateString(value);
            if (Ext.isArray(value) && value.length == 2) {
                value[0] = this.roundNumber(value[0], 6);
                value[1] = this.roundNumber(value[1], 6);
            }
        }
        return value;
    }

    // private
    ,roundNumber : function(num, dec) {
        return Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
    }

    ,setValue : function(value) {
        var coords = value;
        if (value) {
            if (Ext.isString(value)) {
                coords = value.split(/[,; :]/);
            }
            if (Ext.isArray(coords) && coords.length === 2) {
                value = R.AC.prototype.latLonValueToString(coords[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(coords[1], "E", "W");
            }
            else {
                value = '';
            }
        }
        R.form.LatLonField.superclass.setValue.call(this, value);

        // save the original array so that if setValue is called before this
        // field is rendered everything still works
        this.value = coords;
    }

    ,isDirty : function() {
        return !_.isEqual(this.originalValue, this.getValue());
    }
});
