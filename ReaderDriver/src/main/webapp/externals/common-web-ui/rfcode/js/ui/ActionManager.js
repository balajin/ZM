/**
 * @class R.ui.ActionManager
 * Based on http://extjs.com/forum/showthread.php?t=66682
 */
R.ui.ActionManager = Ext.extend(Object, {
    defaultAction: 'action',
    separatorRank: {
        ' ': 1,
        '-': 2,
        'separator': 2,
        '->': 3
    },
    nonMenuSeparators: {
        ' ': '-',
        '->': '->'
    },

    constructor : function(config) {
         Ext.apply(this, config);

         var items = config.items;
         this.items = [];
         if (items) {
             this.add(items);
         }

         if (config.sm) {
             this.setSelectionModel(config.sm);
         }
    },

    add: function(action){
        if(arguments.length > 1 || Ext.isArray(action)){
            var args = arguments.length > 1 ? arguments : action;
            for(var i = 0, len = args.length; i < len; i++){
                this.add(args[i]);
            }
            return;
        }

        // save our selection model
        if (typeof action != 'string') {
            action.sm = this.sm;
            action.manager = this;
        }
        this.items.push(action);
        return action;
    },
    callEach: function(fnName, args) {
        var items = this.items;
        for(var i = 0, len = items.length; i < len; i++){
            var item = items[i], fn;
            if(typeof item != 'string' && (fn = item[fnName])){
                fn.apply(item, args);
            }
        }
    },
    setDisabled: function(){
        this.callEach('setDisabled', arguments);
    },
    setHidden: function(){
        this.callEach('setHidden', arguments);
    },
    update: function() {
        this.callEach('update', arguments);
    },
    findAction: function(target){
        var items = this.items;
        for(var i = 0, len = items.length; i < len; i++){
            var item = items[i];
            if(typeof item != 'string' && item[target]){
                return item;
            }
        }
    },
    queryActions: function(target, includeSeparators) {
        var result = [], items = this.items, rank = 0, separator;
        if (this.menu && target == 'cmenu') {
            result = this.createMenu();
        }
        else {
            for (var i = 0, len = items.length; i < len; i++){
                var item = items[i];
                if (typeof item == 'string'){
                    if (includeSeparators){
                        var irank = this.separatorRank[item] || 0;
                        if (rank < irank){
                            rank = irank;
                            separator = this.nonMenuSeparators[item] || item;
                        }
                    }
                }
                else if ((!target || item[target]) && item.isAllowed()) {
                    if (rank > 0 && result.length > 0) {
                        rank = 0;
                        result.push(separator);
                    }
                    result.push(item);
                }
            }
            if (target == 'cmenu') {
                result = new Ext.menu.Menu({
                    items: result
                });
            }
        }
        return result;
    },

    createMenu : function() {
         var items = this.items;
         var root = [];
         var groups = {};
         var menus = {};
         var i, j, id, item, group, menu, path, paths;

         // determine groups and menus
         for (i = 0; i < this.menu.length; i++) {
             item = this.menu[i];

             if (typeof item === 'string') {
                 groups[item] = [];
             }
             else {
                 menus[item.id] = {
                     menu : {
                         items : []
                     }
                     ,text : item.text
                 };
                 groups[item.path].push(menus[item.id]);
             }
         }
         // add actions to the correct group or menu
         for (i = 0; i < items.length; i++) {
             item = items[i];
             path = item.menubarPath;

             if (item['cmenu'] && path && item.isAllowed()) {
                 paths = path.split('/');
                 if (paths.length == 1) {
                     groups[path].push(item);
                 }
                 else {
                     menus[paths[0]].menu.items.push(item);
                 }
             }
         }

         // create items for context menu
         for (i = 0; i < this.menu.length; i++) {
             item = this.menu[i];

             if (typeof item === 'string') {
                 group = groups[item];
                 if (group.length > 0) {
                     // ensure menus within group have items
                     for (j = group.length-1; j >= 0; j--) {
                         if (group[j].menu && group[j].menu.items.length == 0) {
                             group.pop();
                         }
                     }
                     if (group.length > 0) {
                         if (root.length > 0) {
                             root.push('-');
                         }
                         root.push(group);
                     }
                 }
             }
         }

         return new Ext.menu.Menu({
             items : root
         });
    },

    hasActions : function(target) {
         var items = this.items;
         for(var i = 0, len = items.length; i < len; i++){
             var item = items[i];
             if (!target || item[target]) {
                 return true;
             }
         }
         return false;
    }
    ,executeDefault: function(){
        var action = this.findAction('def');
        if(action){
            action.execute();
        }
    },

    setSelectionModel : function(sm) {
         if (sm) {
             var items = this.items;

             sm = new R.ui.SelectionModel(sm);
             for(var i = 0, len = items.length; i < len; i++){
                 items[i].sm = sm;
             }
             this.sm = sm;
         }
    }
});

