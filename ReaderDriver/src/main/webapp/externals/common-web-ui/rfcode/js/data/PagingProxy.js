/**
 * @class R.data.PagingProxy
 * @extends Ext.data.DataProxy
 * A proxy which delegates to another proxy for loading the data but supports
 * local paging of the entire result set.
 */
R.data.PagingProxy = Ext.extend(Ext.data.DataProxy, {

    /**
     * @param {Ext.data.DataProxy} delegate The proxy which performs the actual
     * load
     */
    constructor : function(delegate) {
        var api = {};
        api[Ext.data.Api.actions.read] = true;
        R.data.PagingProxy.superclass.constructor.call(this, {
            api : api
        });
        this.delegate = delegate;
        this.lastParams = {};
    }

    // override
    ,doRequest : function(action, rs, params, reader, callback, scope, arg) {
        params = params || {};

        var newargs = {
            params    : params
            ,callback : callback
            ,scope    : scope
            ,arg      : arg
        };

        this.reader = reader;

        // reload data only when viewing first page or the start/limit params
        // are the same indicating user hit the paging reload button
        if (this.records && params.start > 0 &&
            (params.start !== this.lastParams.start || params.limit !== this.lastParams.limit)) {
            var result = {
                success : true
                ,records : this.records.slice(params.start, params.start+params.limit)
                ,totalRecords : this.records.length
            };
            callback.call(scope, result, arg, true);
        }
        else {
            this.delegate.doRequest(action, rs, params, reader, this.onLoad, this, newargs);
        }
        this.lastParams = params;
    }

    // private
    ,onLoad : function(result, arg, success) {
        var params = arg.params;

        // sort the entire result set before paging
        if (params.sort) {
            this.sort(result.records, params.sort, params.dir);
        }
        // page the data
        if (!Ext.isEmpty(params.start) && !Ext.isEmpty(params.limit)) {
            this.records = result.records;
            result.records = result.records.slice(params.start, params.start+params.limit);
        }

        arg.callback.call(arg.scope, result, arg.arg, success);
    }

    // private
    ,sort : function(records, f, dir) {
        dir = dir || 'ASC';
        var st = this.reader.recordType.getField(f).sortType;
        var dsc = (dir === 'DESC' ? -1 : 1);
        var fn = function(r1, r2){
            var v1 = st(r1.data[f]), v2 = st(r2.data[f]);
            return v1 > v2 ? dsc : (v1 < v2 ? -dsc : 0);
        };
        records.sort(fn);
    }
});
