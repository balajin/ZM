/**
 * @class R.AC
 * Represents an attribute class.
 * @cfg {Object} constraints
 * @cfg {Boolean} deletable
 * @cfg {String} description
 * @cfg {Object} guid
 * @cfg {Boolean} history
 * @cfg {Boolean} inherit_attributes
 * @cfg {String} name
 * @cfg {String} subtype
 * @cfg {String} type
 * @cfg {String} use
 */
R.AC = function(config) {
    if (config.units) {
        config.units = R.Unit.find(config.units);
    }

    // convert format values to the correct type so operator matching works
    // format values are stored as string values by the server
    var cons = config.constraints;
    if (cons && cons.format) {
        Ext.each(cons.format, function(f) {
            if (f.value) {
                var type = config.type;
                if (type === 'bool') {
                    f.value = (f.value === 'true') || (f.value === true);
                }
                else if (type === 'double' || type === 'enum' || type === 'long'
                         || type === 'date' || type === 'timestamp') {
                    f.value = Number(f.value);
                }
                // it's not a string after saving the AC on the UI, it's a
                // string when first loaded from the server
                else if (type === 'string-list' && typeof f.value === 'string') {
                    f.value = Ext.decode(f.value);
                }
            }
            if (f.fgcolor && f.fgcolor.charAt(0) !== '#') {
                f.fgcolor = '#' + f.fgcolor;
            }
            if (f.bgcolor && f.bgcolor.charAt(0) !== '#') {
                f.bgcolor = '#' + f.bgcolor;
            }
        }, this);
    }

    Ext.apply(this, config);

    if (this.subtype) {
        this.subtype = this.subtype.split(':');
    }
};

/**
 * List of attribute types
 */
R.AC.TYPES = [
    'bool'
    ,'date'
    ,'double'
    ,'entityref'
    ,'entityref-list'
    ,'enum'
    ,'lat-lon-coord'
    ,'long'
    ,'map'
    ,'mimeref'
    ,'password'
    ,'scoperef'
    ,'string'
    ,'string-list'
    ,'string-search'
    ,'timestamp'
    ,'typeref'
    ,'typeref-list'
    ,'typeref-path'
];

/**
 * @static
 */

R.AC.prototype = {

    isType : function(types) {
        if (Ext.isArray(types)) {
            return _.include(types, this.type);
        }
        return this.type === types;
    }

    ,createColumn : function(config) {
        var column = Ext.apply({}, config); // shallow copy it
        return Ext.applyIf(column, {
            dataIndex : this.guid
            ,id       : this.guid
            ,header   : this.name
            ,renderer : this.renderer()
        });
    }

    /**
     * Creates a TreeNode to represent this attribute class.
     * @param {Object} (optional) config Optional config object for tree node
     * @return {Ext.tree.TreeNode} the tree node
     */
    ,createTreeNode : function(config) {
        var opts = {
            id    : this.guid
            ,text : this.name
            ,leaf : true
        };
        if (config) {
            Ext.apply(opts, config);
        }
        return new Ext.tree.TreeNode(opts);
    }

    /**
     * Converts a value retrieved from the server into the browser's locale.
     * @param {Mixed} val The value to convert
     * @return {Number} the converted value
     */
    ,toLocalValue : function(val) {
        if (!Ext.isEmpty(val)) {
            if (this.units) {
                val = this.units.tolocal(val);
            }
            val = this.toNumber(val);
        }
        return val;
    }

    /**
     * Converts a value entered in the browser for sending to the server.
     * @param {Number/String} val The value to convert
     * @param {Boolean} noConvert If true do not do printf conversion
     * @return {Number} the converted value
     */
    ,fromLocalValue : function(val, noConvert) {
        if (!Ext.isEmpty(val)) {
            if (this.units) {
                val = this.units.fromlocal(val);
            }
            if (!noConvert) {
                val = this.toNumber(val);
            }
        }
        return val;
    }

    /**
     * Converts a value into a number if this attribute a double or a long.
     * @param {Number/String} val value to convert
     */
    ,toNumber : function(val) {
        var type = this.type;
        if (type === 'double') {
            if (this.printf) {
                return parseFloat(sprintf(this.printf, val));
            }
            return parseFloat(val);
        }
        else if (type === 'long') {
            return parseInt(sprintf('%.0f', val), 10);
        }
        return val;
    }

    /**
     * Checks to see if we're in the base locale already.
     * @return {Boolean} true if this units is the in the preferred locale
     */
    ,isBaseLocale : function() {
        if (this.units) {
            return this.units.isBaseLocale();
        }
        return true;
    }

    /**
     * Returns the number of significant digits for this attribute class
     * @return {Number} the number of significant digits
     */
    ,getSignificantDigits : function() {
        if (this.type === 'long') {
            return 0;
        } else if (this.printf) {
            var result = this.printf.match('^%\\.(\\d+)f$');
            if (result != null && result.length > 1) {
                return R.htmlDecode(result[1]);
            }
        }
        return null;
    }

    /**
     * Returns the value converted and floored to the nearest significant digit
     * for the base units
     * @return {Number} the floor
     */
    ,getFloorValue : function(val) {
        if (!this.isBaseLocale()) {
            val = this.fromLocalValue(val, true);
            var sigDigits = this.getSignificantDigits();
            if (sigDigits != null) {
                return Math.floor(val * Math.pow(10, sigDigits)) / Math.pow(10, sigDigits);
            }
        }
        return val;
    }
    /**
     * Returns the value converted and ceil'ed to the nearest significant digit
     * for the base units
     * @return {Number} the ceiling
     */
    ,getCeilingValue : function(val) {
        if (!this.isBaseLocale()) {
            val = this.fromLocalValue(val, true);
            var sigDigits = this.getSignificantDigits();
            if (sigDigits != null) {
                return Math.ceil(val * Math.pow(10, sigDigits)) / Math.pow(10, sigDigits);
            }
        }
        return val;
    }

    /**
     * Returns the label used to display this attribute class.
     * @param {Boolean} required if true return label which shows this attribute
     * is required
     */
    ,getDisplayLabel : function(required) {
        if (required === true) {
            return R.ui.required(this.name);
        }
        return this.name;
    }

    /**
     * Returns true if this attribute class has the the given subtype.
     * @return {Boolean}
     */
    ,hasSubType : function(type) {
        var sub = this.subtype;
        return sub && sub.indexOf(type) != -1;
    }

    /**
     * Returns true if this is a list type attribute
     * @return {Boolean}
     */
    ,isList : function() {
        return this.type.indexOf('-list') > 0;
    }

    /**
     * Returns true if this AC has one or more of the given tags.
     * @param {Array} tags List of tags to test. If null/undefined returns true.
     */
    ,hasTag : function(tags) {
        if (Ext.isArray(tags)) {
            return _.include(tags, this.use);
        }
        return this.use === tags;
    }

    /**
     * Returns true if this attribute is configurable (config or config-view)
     * @return {Boolean}
     */
    ,isConfigurable : function() {
        var use = this.use;
        return !use || use === 'config' || use === 'config-view';
    }

    ,isReadOnly : function() {
        return this.use === 'status' || this.use === 'info' || this.isExpressionAttribute();
    }

    /** Returns true if this attribute is a status attribute.
     * @return {Boolean}
     */
    ,isStatus : function() {
        return (this.use === 'status');
    }

    /**
     * Returns true if this is a system level attribute
     * @return {Boolean}
     */
    ,isSystem : function() {
        if (typeof this.system == 'undefined') {
            this.system = (this.guid == 'type' || this.guid == 'retired' || this.guid.indexOf('$') == 0);
        }
        return this.system;
    }

    /**
     * Returns true if this is a zone manager attribute
     * @return {Boolean}
     */
    ,isZoneManager : function() {
        var guid = this.guid;
        return guid.indexOf('$z') === 0;
    }

    /**
     * Returns the regex constraint as a string if it exists
     * @return {String}
     */
    ,getRegex : function() {
        if (this.constraints) {
            return this.constraints.regex;
        }
    }

    /**
     * Returns the minimum value constraints if it exists
     * @return {Number}
     */
    ,getMinConstraint : function() {
        if (this.constraints) {
            return this.constraints.min;
        }
    }

    /**
     * Returns the maximum value constraints if it exists
     * @return {Number}
     */
    ,getMaxConstraint : function() {
        if (this.constraints) {
            return this.constraints.max;
        }
    }

    /**
     * Returns the expression value constraint if it exists
     * @return {String}
     */
    ,getExpression : function() {
        if (this.constraints) {
            return this.constraints.expression;
        }
    }

    /**
     * @return {Boolean}
     */
    ,isExpressionAttribute : function() {
        return !Ext.isEmpty(this.constraints && this.constraints.expression);
    }

    /**
     * Returns the type ref constraints
     * @return {String}
     */
    ,getTypeRef : function() {
        if (this.constraints) {
            return this.constraints.typeref;
        }
    }

    /**
     * Returns true if this attribute class requires unique values
     * @return {Boolean}
     */
    ,isValueUnique : function() {
        if (this.constraints) {
            return this.constraints.value_unique;
        }
    }

    /**
     * Returns true if this attribute class is of type "typeref"
     * @return {Boolean}
     */
    ,isTypeRef : function() {
        return this.type === 'typeref';
    }

    ,isInheritAttributes : function() {
        return this.isTypeRef() && this.inherit_attributes;
    }

    /**
     * Returns true if this attribute class's value is a tag.
     * @return {Boolean}
     */
    ,isTagRef : function() {
        return this.type === 'entityref' && this.getTypeRef() === R.ET.TAG;
    }

    ,getMappedAC : function(guid) {
        var cons = this.constraints;
        if (this.isTagRef()) {
            return R.AC.get(cons && cons.attribute_map && cons.attribute_map[guid]);
        }
    }

    /**
     * Returns true if this attribute class is suitable for a user to create a
     * filter on.
     * @return {Boolean}
     */
    ,isFilterable : function() {
        return (!this.isZoneManager() &&
                this.type != 'password' && this.type != 'mimeref' &&
                (this.use == null || (this.use != 'hidden' && this.use != 'user-hidden' && this.use != 'config' && this.use != 'info')));
    }

    /**
     * @return {Boolean}
     */
    ,isUserHidden : function() {
        return (this.use === 'user-hidden');
    }

    /**
     * @return {Boolean}
     */
    ,isHidden : function() {
        var use = this.use;
        return use === 'hidden' || use === 'user-hidden';
    }

    /**
     * @return {Boolean}
     */
    ,isValidValue : function(val) {
        var rc = true;
        if (this.type === 'long' || this.type === 'double') {
            var cons = this.constraints;

            if (cons) {
                if (cons.min != null) {
                    rc = val >= cons.min;
                }

                if (rc && cons.max != null) {
                    rc = val <= cons.max;
                }
            }
        }
        return rc;
    }

    /**
     * Returns true if this attribute class supports graphs.
     * @return {Boolean}
     */
    ,isGraphable : function() {
        var type = this.type;
        return this.history && (type === 'double' || type === 'long' || type === 'enum' || type === 'bool');
    }

    /**
     * Returns true if this attribute class supports reports.
     * @return {Boolean}
     */
    ,isReportable : function() {
        return this.history && (this.isConfigurable() || this.use == 'status');
    }

    /**
     * Returns this attribute class as a JSON object
     * @return {Object}
     */
    ,toJSON : function() {
        return {
            guid                : this.guid
            ,name               : this.name
            ,type               : this.type
            ,subtype            : this.subtype
            ,description        : this.description
            ,deletable          : this.deletable
            ,history            : this.history
            ,retired            : this.retired
            ,constraints        : this.constraints
            ,values             : this.values
            ,printf             : this.printf
            ,inherit_attributes : this.inherit_attributes
            ,use                : this.use
            ,units              : this.units ? this.units.id : null
            ,restrictable       : this.restrictable
        };
    }

    /**
     * Formats a grid renderer based on this attribute's formatting rules.
     * @param {Object} value The data value for the cell
     * @param {Object} meta See ColumnModel#setRenderer()
     * @private
     */
    ,format : function(value, meta, record) {
        var fmt = this.findFormat(value);

        if (fmt) {
            if (record && record.isModified(this.guid)) {
                meta.attr = 'style="background: url(../ext/resources/images/default/grid/dirty.gif) no-repeat 0 0; color: ' + fmt.fgcolor + '; background-color: ' + fmt.bgcolor + ';"';
            }
            else {
                meta.attr = 'style="color: ' + fmt.fgcolor + '; background-color: ' + fmt.bgcolor + ';"';
            }
        }
        else {
            meta.attr = '';
        }
    }

    /**
     * Returns the format object corresponding to the given value.
     * @method
     */
    ,findFormat : function(value) {
        var cons = this.constraints;

        if (cons && cons.format) {
            var format = cons.format;
            var index = -1;

            // Find the first matching rule
            for (var i = 0; i < format.length; i++) {
                var op = format[i].operator;
                var v = format[i].value;

                if (!op || R.Op.matches(this, op, value, v)) {
                    return format[i];
                }
            }
        }
    }

    /**
     * Sends a request to delete an attribute class.
     */
    ,remove : function(callback, scope) {
        Ext.get(document.body).mask('Deleting...', 'x-mask-loading');

        R.dao.loadList([
            {
                url              : 'api/attributeclass/'+encodeURIComponent(this.guid)
                ,method          : 'DELETE'
                ,requestCallback : R.dao.deleteACResponse
                ,acGUID          : this.guid
                ,usercallback    : callback
                ,userscope       : scope
            }
        ]);
    }

    // override
    ,toString : function() {
        return this.name;
    }

    ,latLonValueToString : function(latLonValue, positiveSuffix, negativeSuffix) {

        return latLonValue;

        // For now commenting out - display value is the signed decimal format.
        //var suffix = positiveSuffix;
        //if (latLonValue < 0) {
        //    suffix = negativeSuffix;
        //}
        //
        //latLonValue = Math.abs(latLonValue);
        //var degrees = Math.floor(latLonValue);
        //var temp    = latLonValue - degrees;
        //temp       *= 60;
        //var minutes = Math.floor(temp);
        //temp        = temp - minutes;
        //temp       *= 60;
        //var seconds = Math.round(temp);
        //
        //if (degrees < 10) {
        //    degrees = '0' + degrees;
        //}
        //if (minutes < 10) {
        //    minutes = '0' + minutes;
        //}
        //if (seconds < 10) {
        //    seconds = '0' + seconds;
        //}
        //return degrees + '\u00B0 ' + minutes + "' " + seconds + '" ' + suffix;
    }

    /**
     * Parses a string and creates a two value array containing the GPS coordinate values.
     */
    ,parseCoordinateString : function(fmtstr) {
        var lat;
        var lon;

        if (fmtstr) {
            fmtstr = fmtstr.toUpperCase().trim();
            /* Prepare string - replace commas, slashes, colons, quotes, doublequotes, degrees with spaces */

            if (fmtstr.indexOf('N') > 0)
                fmtstr = fmtstr.replace("N", " N");  /* Replace N with padded N (force token) */
            if (fmtstr.indexOf('S') > 0)
                fmtstr = fmtstr.replace("S", " S");  /* Replace S with padded S (force token) */
            if (fmtstr.indexOf('E') > 0)
                fmtstr = fmtstr.replace("E", " E");  /* Replace E with padded E (force token) */
            if (fmtstr.indexOf('W') > 0)
                fmtstr = fmtstr.replace("W", " W");  /* Replace W with padded W (force token) */

            fmtstr = fmtstr.replace(/[,;:'"\/\u00B0]/g, ' ');
            fmtstr = fmtstr.trim();

            var tok    = fmtstr.split(' ');  /* Now, split at the spaces */

            var tokcnt = 0;
            for (var i = 0; i < tok.length; i++) {  /* Compact out the blanks */
                if (tok[i].length > 0) {
                    tok[tokcnt] = tok[i];
                    tokcnt++;
                }
            }

            /* If only 2 non-blank tokens, have to be unitless lat lon */
            if (tokcnt == 2) {
                lat = parseFloat(tok[0]);
                lon = parseFloat(tok[1]);
            }
            else if (tokcnt >= 4) {  /* Else, at least 4, and we expect N/S and E/W */
                var latlon = [ 0.0, 0.0 ];
                var intok  = 0;  /* Start on degrees */
                var idx    = 0;    /* Latitude is first */

                for (var i = 0; i < tokcnt; i++) {
                    if (idx > 1) {  /* Already done - extra tokens = bad */
                        return null;
                    }

                    if (tok[i] == "N" || tok[i] == "n" || tok[i] == "S" || tok == "s") {  /* If N or S */
                        if ((idx != 0) || (intok == 0)) {  /* If not in latitude, or no degrees, error */
                            return null;
                        }
                        idx = 1;    /* Advance to longitude */
                        intok = 0;  /* Start at degrees again */
                        if (tok[i] == "S" || tok[i] == "s") {  /* South? */
                            latlon[0] = -latlon[0];  /* Invert value */
                        }
                    }
                    else if (tok[i] == "E" || tok[i] == "e" || tok[i] == "W" || tok[i] == "w") {  /* If E or W */
                        if ((idx != 1) || (intok == 0)) {  /* If not in longitude, or no degrees, error */
                            return null;
                        }
                        idx = 2;  /* We're done */
                        intok = 0; /* Start at degrees again */
                        if (tok[i] == "W" || tok[i] == "w") {  /* West? */
                            latlon[1] = -latlon[1];  /* Invert value */
                        }
                    }
                    else if (intok == 0) {  /* Degrees? */
                        latlon[idx] = parseFloat(tok[i]);
                        intok++;  /* On to minutes, if any */
                    }
                    else if (intok == 1) {  /* Minutes? */
                        latlon[idx] += parseFloat(tok[i]) / 60.0;
                        intok++;  /* On to seconds, if any */
                    }
                    else if (intok == 2) {  /* Seconds? */
                        latlon[idx] += parseFloat(tok[i]) / 3600.0;
                        intok++;  /* Shouldn't be any more besides NSEW */
                    }
                    else {  /* We're on to a token we shouldn't have */
                        return null;
                    }
                }
                /* If we're done, but not in proper final state, error */
                if (idx != 2 || isNaN(latlon[0]) || isNaN(latlon[1])) {
                    return null;
                }
                return [ latlon[0], latlon[1] ];
            }
            if (isNaN(lat) || isNaN(lon)) {
                return null;
            }
            return [ lat, lon ];
        }
        return null;
    }
};

(function() {
    // renderers
    var _types = {};
    var _guids = {};

    var _acs = {};
    var _znames = {};

    Ext.apply(R.AC, {
        /**
         * Adds an attribute class to the global attribute class collection.
         * @static
         * @param {R.AC/Object} ac The attribute class to add
         * @return {R.AC} The attribute class
         */
        add : function(ac) {
            var zname = ac['$zName'];

            if (ac instanceof R.AC === false) {
                ac = new R.AC(ac);
            }
            _acs[ac.guid] = ac;
            if (zname) {
                _znames[zname] = ac;
            }

            // Check if the attribute class is a tag ref.  If it is
            // then for any mapped attribute see if there is a renderer
            // from the mapped-from attribute and apply it to the mapped-to
            // attribute.
            if (ac.type == 'entityref' && ac.getTypeRef() == '$tTag') {
                if (ac.constraints.attribute_map) {
                    // Iterate over the attribute map from attributes.
                    for (var tagAC in ac.constraints.attribute_map) {
                        var renderer = _guids[tagAC];
                        if (renderer) {
                            var mappedAC = ac.constraints.attribute_map[tagAC];
                            if (mappedAC && _guids[mappedAC] == null) {
                                _guids[mappedAC] = renderer;
                            }
                        }
                    }
                }
            }
            return ac;
        }

        /**
         * Removes an attribute class from the global attribute class collection.
         * @static
         * @param {String} guid The attribute class guid
         */
        ,remove : function(guid) {
            var ac = _acs[guid];
            if (ac) {
                delete _acs[guid];

                var zname = ac['$aZname'];
                if (zname) {
                    delete _znames[zname];
                }
            }
        }

        /**
         * Iterates over each attribute class calling the passed function with
         * each attribute, stopping if the function returns false.
         * @param {Function} fn Function
         * @param {Object} scope (optional) Object
         * @static
         */
        ,each : function(fn, scope) {
            var guid, ac;

            for (guid in _acs) {
                ac = _acs[guid];
                if (fn.call(scope || ac, ac) === false) {
                    return ac;
                }
            }
        }

        /**
         * Iterates over each attribute class returning an array of all the
         * values that pass a predicate.
         * @param {Function} fn Function
         * @param {Object} scope (optional) Object
         * @static
         */
        ,filter : function(fn, scope) {
            var guid, ac;
            var result = [];
            for (guid in _acs) {
                ac = _acs[guid];
                if (fn.call(scope || ac, ac)) {
                    result.push(ac);
                }
            }
            return result;
        }

        /**
         * Returns the attribute with the specified guid.
         * @param {String} guid The attribute class GUID
         * @return {R.AC} The attribute class
         * @static
         */
        ,get : function(guid) {
            return guid instanceof R.AC ? guid : _acs[guid];
        }

        /**
         * Returns an attribute class based on its zname.
         * @return {R.AC} The attribute class
         * @static
         */
        ,getByZName : function(zname) {
            return _znames[zname];
        }

        /**
         * Returns a list of all the attribute classes.
         * @return {Array} Array of R.AC objects
         * @static
         */
        ,list : function() {
            var result = [];
            R.AC.each(Array.prototype.push, result);
            return result;
        }

        /**
         * Returns true if the attribute class guid is referenced
         * in a tag attribute class, false otherwise.
         */
        ,isTagMapped : function(guid) {
            var mapped = false;
            var temp, ac;

            // Iterate over all attribute classes
            for (temp in _acs) {
                ac = _acs[temp];
                // Check if this is a tag ref attribute class
                if (ac.type == 'entityref' && ac.getTypeRef() == '$tTag') {
                    if (ac.constraints.attribute_map) {
                        // Iterate over the attribute map and check if
                        // the given attribute is mapped to a tag attribute
                        for (tagAC in ac.constraints.attribute_map) {
                            if (ac.constraints.attribute_map[tagAC] == guid) {
                                mapped = true;
                                break;
                            }
                        }
                    }
                }
                if (mapped) {
                    break;
                }
            }
            return mapped;
        }

        /**
         * Returns a renderer function based on the type of the given AC.
         * Normally the attribute class's renderer is based on the GUID and then
         * the type.
         */
        ,getRendererByType : function(ac) {
            return _types[ac.type](ac);
        }

        /**
         * Registers an attribute class type renderer.
         * @param {String} type The attribute class type (eg. bool)
         * @param {Function} fn Function which returns a renderer function
         * @static
         */
        ,registerTypeRenderer : function(type, fn) {
            _types[type] = fn;
        }

        /**
         * Registers an attribute class GUID renderer.
         * @param {String} type The attribute class guid (eg. $aEnabled)
         * @param {Function} fn Function which returns a renderer function
         * @static
         */
        ,registerGuidRenderer : function(guid, fn) {
            _guids[guid] = fn;
        }
    });

    /**
     * Returns a function for renderering values of this attribute class on
     * a grid.
     * @return {Function} A grid renderer function
     */
    R.AC.prototype.renderer = function() {
        var renderer = _guids[this.guid] || _types[this.type];
        if (renderer) {
            renderer = renderer(this);
        }

        if (this.constraints && this.constraints.format) {
            var format = this.format;
            var ac = this;
            return function(value, meta) {
                var result = value;
                if (renderer) {
                    result = renderer.apply(ac, arguments);
                }
                if (meta) {
                    format.apply(ac, arguments);
                }
                return result;
            };
        }
        return renderer;
    };

    Ext.apply(_types, {
        'bool' : function(ac) {
            return function(value) {
                var values = ac.values;

                if (value === true || value === 'true') {
                    return (values && values[1]) || 'Yes';
                }
                else if (value === false || value === 'false') {
                    return (values && values[0]) || 'No';
                }
                return '';
            };
        }

        ,'date' : function() {
            return function(value) {
                return typeof value == 'number' ? R.utcToDateString(value) : '';
            };
        }

        ,'double' : function(attr) {
            var printf;
            var units = attr.units;

            if (attr.printf) {
                printf = function(val) {
                    return sprintf(attr.printf, val);
                };
            }

            return function(value) {
                if (value !== null) {
                    // If this is a string parse a float.  This happens when
                    // newly modified values are trying to be displayed by the
                    // renderer (ie. default value on asset types)
                    if (typeof value === 'string' && value.length > 0) {
                        value = parseFloat(value);
                    }
                    if (typeof value === 'number') {
                        if (units) {
                            value = units.tolocal(value);
                        }
                        if (printf) {
                            value = printf(value);
                        }
                        if (units) {
                            value += units.shortlabel();
                        }
                    }
                    return value;
                }
                return '';
            };
        }

        ,'long' : function(attr) {
            var units = attr.units;

            return function(value) {
                if (Ext.isEmpty(value)) {
                    value = '';
                }
                else {
                    // If this is a string parse an int. This happens when
                    // newly modified values are trying to be displayed by the
                    // renderer (ie. default value on asset types)
                    if (typeof value === 'string') {
                        value = parseInt(value, 10);
                    }
                    if (units) {
                        value = units.tolocal(value).toFixed() + units.shortlabel();
                    }
                }
                return value;
            };
        }

        ,'enum' : function(attr) {
            return function(value) {
                return typeof value === 'number' ?  attr.values[value] : value;
            };
        }

        ,'string' : function(attr) {
            if (attr.subtype === 'url') {
                return function(value, meta) {
                    if (value) {
                        var fmt = attr.findFormat(value);

                        // Make sure we have the string value inside the
                        // attribute value escaped
                        var link = R.recurseHTMLEncode(value);
                        if (fmt) {
                            // format for link must be applied to anchor tag and not
                            // its container like the rest of the attributes
                            return String.format('<a href="' + link +
                                                 '" target="_blank" style="color: ' + fmt.fgcolor +
                                                 '">' + link + '</a>');
                        }
                        else {
                            return String.format('<a href="' + link + '" target="_blank">' + link + '</a>');
                        }
                    }
                }
            }
        }

        ,'timestamp' : function() {
            return function(value) {
                return typeof value === 'number' ? R.utcToTimestampString(value) : '';
            };
        }

        ,'typeref' : function() {
            return function(value) {
                if (value != null) {
                    var et = R.ET.get(value);
                    if (et != null) {
                        return et.name;
                    }
                    else if (R.ET.hidden(value)) {
                        // occurs when user views reports generated with different
                        // permissions than themselves - for example, an admin
                        // viewing a report generated by a non-admin user
                        return '<hidden>';
                    }
                }
                return value;
            };
        }

        ,'entityref' : function() {
            return function(value, col, record) {
                if (value != null && col != null) {
                    if (record.data && record.data[col.id+'::name']) {
                        value = record.data[col.id+'::name'];
                    }
                }
                return value;
            };
        }

        ,'entityref-list' : function() {
            return function(value, col, record) {
                if (value != null && col != null) {
                    if (record.data && record.data[col.id+'::name']) {
                        value = record.data[col.id+'::name'];
                    }
                }
                return value;
            };
        }

        ,'typeref-list' : function() {
            return function(value) {
                if (value != null && value.length > 0) {
                    var list = [];
                    var hidden = false;

                    for (var i = 0; i < value.length; i++) {
                        var guid = value[i];
                        var et = R.ET.get(guid);

                        if (et != null) {
                            list.push(et.name);
                        }
                        else if (R.ET.hidden(guid)) {
                            if (!hidden) {
                                hidden = true;
                                list.push('<hidden>');
                            }
                        }
                        // The last two else cases handle the situation
                        // where a typeref-list attribute is being
                        // rendered with resolved names as in the case
                        // of a live report
                        else if (guid == '<hidden>') {
                            if (!hidden) {
                                hidden = true;
                                list.push(guid);
                            }
                        }
                        else {
                            list.push(guid);
                        }
                    }
                    return list.join(',');
                }
                return '';
            };
        }

        ,'typeref-path' : function() {
            return function(value) {
                var dispVal = '';
                if (value && value.length > 0) {
                    if (value.indexOf('|') == 0) {
                        value = value.substring(1);

                        if (value.length > 0) {
                            if (value.lastIndexOf('|') == (value.length - 1)) {
                                value = value.substring(0, value.length-1);
                            }
                        }
                        var ets = value.split('|');
                        if (ets && ets.length > 0) {
                            for (var i = 0; i < ets.length; i++) {
                                var et = R.ET.get(ets[i]);
                                if (et) {
                                    if (dispVal.length > 0) {
                                        dispVal += ' / ';
                                    }
                                    dispVal += et.name;
                                }
                                else if (ets[i] == '<hidden>') {
                                    dispVal = '<hidden>';
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        dispVal = value;
                    }
                }
                return dispVal;
            };
        }

        ,'lat-lon-coord' : function() {
            return function(value) {
                if (Ext.isArray(value) && value.length == 2) {
                    return R.AC.prototype.latLonValueToString(value[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(value[1], "E", "W");
                }
                return '';
            }
        }
    });
})();

// Common attribute class GUIDs
Ext.apply(R.AC, {
    ENABLED               : '$aEnabled'
    ,HOSTNAME             : '$aHostname'
    ,NAME                 : '$aName'
    ,PORT_NUM             : '$aPortNum'
    ,READER_CHANNEL_LABEL : '$zReaderChannelLabel'
    ,READER               : '$aReader'
    ,RULE_TAG_LIST        : "$zRuleRefTagList"
});

