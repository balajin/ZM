/**
 * @class R.ET
 * An entity type object. An entity type received from the server has the
 * following properties:<ul>
 * <li>class: entity_type/li>
 * <li>guid: GUID of the entity type</li>
 * <li>name: name</li>
 * <li>parent: GUID of the entity type's parent</li>
 * <li>deletable: whether entity type is deletable</li>
 * <li>description: description</li>
 * <li>attributes: array of entity type attributes</li>
 *    <ul>
 *    <li>class: entity_attribute</li>
 *    <li>guid: attribute GUID</li>
 *    <li>deletable: true if user can remove this attribute from the entity type</li>
 *    <li>required: true if required when creating/updating entity</li>
 *    <li>isStatic: true if attribute is static</li>
 *    <li>category: category of the attribute</li>
 *    <li>sortPriority: sort order for displaying the attributes in the UI</li>
 *    </ul>
 * </ul>
 *
 */
R.ET = function(config) {
    var attrs, i;

    Ext.apply(this, config);

    Ext.applyIf(this, {
        attributes : []
        ,children  : []
    });

    attrs = this.attributes;
    for (i = 0; i < attrs.length; i++) {
        attrs[i].sortPriority = attrs[i].sortPriority || 0;
    }
};

/**
 * Returns true if the guid corresponds to a hidden entity type (ie. user does
 * not have access).
 * @method
 * @static
 * @param {String} guid The entity type guid
 */
R.ET.hidden = function(guid) {
    var i = guid.lastIndexOf('__hidden__');
    return (i !== -1) && (i === guid.length - 10);
};

R.ET.prototype = {
    /**
     * Returns a list of all the children of this entity type which have no
     * children of their own.
     * @return {Array} An array of R.ET objects
     */
    leaves : function() {
        var result = arguments[0] || [];
        for (var i = 0; i < this.children.length; ++i) {
            var child = this.children[i];

            if (child.children.length > 0) {
                child.leaves(result);
            }
            else {
                result.push(child);
            }
        }
        return result;
    }

    /**
     * Does a depth first traversal of the children of this entity type
     * @param {Function} fn The function to apply to each child
     * @param {Object} scope Optional scope
     */
    ,depthTraverse : function(fn, scope) {
        var children = this.children;
        var child;
        for (var i = 0; i < children.length; i++) {
            child = children[i];

            fn.call(scope, child);
            if (child.children.length > 0) {
                child.depthTraverse(fn, scope);
            }
        }
    }

    /**
     * Test if this entity type is a child/descendent of the given entity type
     * GUID. Return true if this entity type is equal to or a descendent of the
     * given GUID.
     * @param {String} guid The GUID of the parent entity type
     * @return {Boolean}
     */
    ,isDescendent : function(guid) {
        var et = this;
        while (et.guid !== guid && et.parent) {
            et = R.ET.get(et.parent);
        }
        return et.guid === guid;
    }

    /**
     * Returns all the descendent ET's of this entity type in an array.
     * @return {Array}
     */
    ,descendents : function() {
        var result = arguments[0] || [];
        var children = this.children;
        var i, child;

        for (i = 0; i < children.length; i++) {
            child = children[i];
            result.push(child);
            child.descendents(result);
        }
        return result;
    }


    /**
     * Returns true if this type is $tAsset or a child of $tAsset.
     */
    ,isAsset : function() {
        return this.isDescendent(R.ET.ASSET);
    }

    /**
     * Returns the entity type attribute with the given GUID in this entity
     * type or its parents.
     */
    ,getETA : function(guid) {
        var parent, eta, etas = this.attributes;

        for (var i = 0; i < etas.length; i++) {
            eta = etas[i];
            if (eta.guid === guid) {
                return eta;
            }
        }

        if (!this.parent) {
            return null;
        }

        // check parents
        parent = R.ET.get(this.parent);
        return parent.getETA(guid);
    }

    /**
     * Returns the entity type attributes in this entity type and its parents.
     * @param {Function} filter (optional) If provided, only ETAs which pass the
     * filter are returned.
     */
    ,getETAs : function(filter) {
        var result = arguments[1] || [];
        var map = arguments[2] || {};
        var etas = this.attributes;
        var len = etas.length;
        var eta, map, parent, result;

        for (var i = 0; i < len; i++) {
            eta = etas[i];
            if (map[eta.guid] !== true && (!filter || filter(eta))) {
                map[eta.guid] = true;
                result.push(eta);
            }
        }

        // try parent
        if (this.parent) {
            parent = R.ET.get(this.parent);
            result = parent.getETAs(filter, result, map);
        }

        return result;
    }

    /**
     * Returns the default values for all the ETAs of this entity type.
     * @return {Object} values The default values
     */
    ,getDefaultValues : function() {
        var find = function(et, result) {
            var etas = et.getETAs();
            var ac, eta, i;
            for (i = 0; i < etas.length; i++) {
                eta = etas[i];

                if (!result.hasOwnProperty(eta.guid)) {
                    result[eta.guid] = eta.value;

                    if (eta.value) {
                        ac = R.AC.get(eta.guid);
                        if (ac.isInheritAttributes()) {
                            find(R.ET.get(eta.value), result);
                        }
                    }
                }
            }
            return result;
        };
        return _.reduce(find(this, {}), function(memo, value, key) {
            if (Ext.isDefined(value)) {
                memo[key] = value;
            }
            return memo;
        }, {});
    }

    /**
     * Returns the attribute class objects corresponding to the entity type
     * attributes for this entity type. Including all parent and child entity
     * types.
     */
    ,getACs : function() {
        var aces = {}, result = [];

        function children(type) {
            var ac, eta;

            // our attributes
            for (var i = 0; i < type.attributes.length; i++) {
                eta = type.attributes[i];
                ac  = R.AC.get(eta.guid);

                aces[ac.guid] = ac;
            }

            // child attributes
            for (var i = 0; i < type.children.length; i++) {
                children(type.children[i]);
            }
        };
        children(this);

        function parent(type) {
            for (var i = 0; i < type.attributes.length; i++) {
                eta = type.attributes[i];
                ac  = R.AC.get(eta.guid);

                aces[ac.guid] = ac;
            }

            if (type.parent) {
                parent(R.ET.get(type.parent));
            }
        };
        parent(this);

        for (var k in aces) {
            result.push(aces[k]);
        }
        return result;
    }

    // private
    ,toJSON : function() {
        var i, eta, etas = this.attributes;

        var data = {
            deletable     : this.deletable
            ,description  : this.description
            ,restrictable : this.restrictable
            ,guid         : this.guid
            ,name         : this.name
            ,parent       : this.parent
            ,attributes   : []
        };
        for (i = 0; i < etas.length; i++) {
            eta = etas[i];

            data.attributes.push({
                category      : eta.category
                ,deletable    : eta.deletable
                ,guid         : eta.guid
                ,isStatic     : eta.isStatic
                ,required     : eta.required
                ,sortPriority : eta.sortPriority
                ,value        : eta.value
            });
        }
        return data;
    }

    /**
     * Loads all the entities which are of this entity type.
     * @param {Function} callback to call when load completes
     * @param {Object} scope to use for callback
     */
    ,getEntities : function(options) {
        Ext.Ajax.request({
            url       : 'api/entity?type=' + encodeURIComponent(this.guid)
            ,method   : 'GET'
            ,scope    : this
            ,callback : function(opts, success, response) {
                var entities;
                if (success) {
                    entities = R.decode(response.responseText);
                }

                Ext.callback(options.callback, options.scope, [ success, entities, this ]);

                if (success) {
                    Ext.callback(options.success, options.scope, [ entities ]);
                }
                else {
                    Ext.callback(options.failure, options.scope, [ response ]);
                }
            }
        });
    }

    /**
     * Creates this entity type on the server
     */
    ,create : function(callback, scope) {
        Ext.Ajax.request({
            url : 'api/entitytype'
            ,method : 'POST'
            ,jsonData : this.toJSON()
            ,callback : function(opts, success, response) {
                var dao = R.dao;
                dao.handleCreateOrUpdateET(opts, success, response, callback, scope);
            }
        });
    }

    /**
     * Updates this entity type on the server
     * @param {Array} array of optional mimeref form file input fields
     */
    ,update : function(callback, scope, files) {
        var i, opts, form;
        var isUpload = files && files.length > 0;
        var json = this.toJSON();

        files = files || [];

        // ajax request options
        opts = {
            url       : 'api/entitytype/' + this.guid
            ,method   : 'PUT'
            ,isUpload : isUpload
            ,jsonData : json
            ,callback : function(opts, success, response) {
                var dao = R.dao;

                // destroy the form used to upload
                Ext.destroy(opts.form);

                dao.handleCreateOrUpdateET(opts, success, response, callback, scope);
            }
        };

        // need a form if we are uploading mimeref attribute values
        if (isUpload) {
            form = Ext.getBody().createChild({
                tag     : 'form'
                ,method : 'post'
                ,cls    : 'x-hidden'
                ,id     : Ext.id()
            });
            for (i = 0; i < files.length; i++) {
                form.appendChild(files[i]);
            }
            Ext.apply(opts, {
                form      : form
                ,isUpload : true
                ,method   : 'POST'
                ,params   : { 'json' : Ext.encode(json) }
            });
        }

        Ext.Ajax.request(opts);
    }

    // override
    ,toString : function() {
        return this.name;
    }
};

(function() {
    var _ets = {};
    var _listeners = new Ext.util.Observable();

    _listeners.addEvents(
        /**
         * @event add
         * Fires when an entity type is added
         * @param {R.ET} et The new entity type
         */
        'add'

        /**
         * @event remove
         * Fires when an entity is removed
         * @param {R.ET} et The removed entity type
         */
        ,'remove'
    );

    Ext.apply(R.ET, {
        /**
         * Adds an entity type to the global entity type collection.
         * @static
         * @param {Object} et The entity type to add
         * @return {R.ET} The entity type
         */
        add : function(et) {
            var parent;
            var existing = _ets[et.guid];

            if (existing) {
                et = Ext.apply(existing, et);
            }
            else {
                et = new R.ET(et);
                _ets[et.guid] = et;
            }
            // add to its parent
            if (et.parent) {
                parent = _ets[et.parent];

                if (!parent) {
                    parent = new R.ET({ guid: et.parent });
                    _ets[et.parent] = parent;
                }
                var children = parent.children;
                var found = false;
                for (var i = 0; i < children.length; i++) {
                    if (children[i].guid === et.guid) {
                        found = true;
                        children[i] = et;
                        break;
                    }
                }
                if (!found) {
                    parent.children.push(et);
                }
            }

            _listeners.fireEvent('add', et);

            return et;
        }

        /**
         * Removes an entity type from the global entity type collection.
         * @static
         * @param {String} guid
         */
        ,remove : function(guid) {
            var et = _ets[guid];
            if (et) {
                delete _ets[guid];

                // Remove the et from its parent's children
                if (et.parent) {
                    var children = _ets[et.parent].children;
                    for (var i = 0; i < children.length; i++) {
                        if (children[i].guid === guid) {
                            children.splice(i, 1);
                            break;
                        }
                    }
                }

                _listeners.fireEvent('remove', et);
            }
        }

        /**
         * Iterates over each entity type calling the passed function with each
         * type, stopping if the function returns false.
         * @param {Function} fn Function
         * @param {Object} scope (optional) Object
         * @static
         */
        ,each : function(fn, scope) {
            var guid, et;
            for (guid in _ets) {
                et = _ets[guid];
                if (fn.call(scope || et, et) === false) {
                    return et;
                }
            }
        }

        /**
         * Returns the entity type with the specified guid.
         * @param {String/Array} guid The entity type GUID or array of GUIDs
         * @return {R.ET/Array} The entity type or array of entity types
         * @static
         */
        ,get : function(guid) {
            var i, et, result;
            if (Ext.isString(guid)) {
                result = _ets[guid];
            }
            else if (guid instanceof R.ET) {
                result = guid;
            }
            else if (Ext.isArray(guid)) {
                result = [];
                for (i = 0; i < guid.length; i++) {
                    et = R.ET.get(guid[i]);
                    if (et) {
                        result.push(et);
                    }
                }
            }
            return result;
        }

        /**
         * Returns a copy of the entity type with the specified guid. This does
         * a shallow copy only. Changing the values of any children or
         * attributes will affect the original ET.
         * @param {String} guid The entity type GUID
         * @return {R.ET} The entity type
         * @static
         */
        ,copy : function(guid) {
            var et = R.ET.get(guid);
            if (et) {
                et = new R.ET(et);
                et.children = et.children.slice(0);
                et.attributes = et.attributes.slice(0);
            }
            return et;
        }

        /**
         * @static
         */
        ,on : function(eventName, handler, scope, options) {
            _listeners.on(eventName, handler, scope, options);
        }

        /**
         * @static
         */
        ,un : function(eventName, handler, scope) {
            _listeners.un(eventName, handler, scope);
        }
    });
})();

// Entity Type Constants
Ext.apply(R.ET, {
    READER          : '$tReader'
    ,READER_CHANNEL : '$tReaderChannel'
    ,RULE           : '$tRule'
    ,TAG            : '$tTag'
    ,TAG_GROUP      : '$tTagGroup'
});

