R.tree.TreeActions = Ext.extend(R.ui.PanelActions, {

    init: function(p) {
        var mgr = this.mgr;
        var tree = mgr.tree || p;

        R.tree.TreeActions.superclass.init.call(this, p, tree.getSelectionModel());

        tree.on('dblclick', mgr.executeDefault, mgr);
        tree.getSelectionModel().on('selectionchange', mgr.update, mgr, {buffer: 50});

        if (mgr.hasActions('cmenu')) {
            tree.on('contextmenu', this.onContextMenu, this);
        }

        this.tree = tree;
    }

    // private
    ,onContextMenu: function(node, e) {
        // lazily create the menu
        if (!this.cmenu) {
            this.cmenu = this.mgr.queryActions('cmenu', true);
            if (this.cmenu) {
                this.tree.on('destroy', function() { Ext.destroy(this.cmenu); }, this);
            }
        }

        e.stopEvent();
        this.mgr.sm.delegate().select(node);
        this.cmenu.cascade(function(item) {
            var act = item.baseAction;
            act && act.beforeMenuShow();
        });
        this.cmenu.showAt(e.getXY());
    }
});

