/**
 * @class R.ui.Dialog
 * @extends R.ui.Window
 * A simple dialog class which displays a CLOSE button or OK/CANCEL buttons.
 * @cfg {Object} buttons OK or OKCANCEL. Default is OKCANCEL.
 * @cfg {Ext.Component} focusField Initial field to focus, if not set, the first
 * field is focused
 */
R.ui.Dialog = Ext.extend(R.ui.Window, {

    /**
     * @cfg {Boolean} okOnEnter False to disable the enter key from sending the ok
     * event (defaults to true)
     */
    okOnEnter : true

    // override
    ,initComponent : function() {
        this.buttons = this.buttons || R.ui.Dialog.OKCANCEL;

        Ext.apply(this, {
            buttons      : this.createButtons()
            ,closeAction : 'close'
            ,modal       : true
        });

        R.ui.Dialog.superclass.initComponent.call(this);

        this.addEvents(
            /**
             * @event ok
             * Fires when the ok button is clicked. Return false to stop the ok.
             * @param {R.ui.Dialog} this
             */
            'ok'

            /**
             * @event cancel
             * Fires when the cancel button is clicked. Return false to stop the cancel.
             * @param {R.ui.Dialog} this
             */
            ,'cancel'
        );
    }

    /**
     * Called to create the buttons array.
     * @return {Array} buttons
     */
    ,createButtons : function() {
        return this.buttons.create(this);
    }

    /**
     * Sets the ok button disabled state.
     */
    ,setOkButtonDisabled : function(v) {
        this.buttons[0].setDisabled(v);
    }

    /**
     * Fires an ok event and closes this window.
     * @param {Array} (optional) Extra parameters sent to the 'ok' event
     */
    ,okWithArgs : function() {
        var args = [ 'ok', this ];
        var extra = this.okEventArgs();

        if (arguments.length > 0) {
            args = args.concat(_.toArray(arguments));
        }
        if (Ext.isArray(extra)) {
            args = args.concat(extra);
        }
        if (this.fireEvent.apply(this, args) !== false) {
            this.close();
        }
    }

    /**
     * Fires an ok event and closes this window.
     */
    ,ok : function() {
        this.okWithArgs();
    }

    /**
     * Allows a subclass to provide additional arguments to the ok event.
     * @return {Array} arguments Additional arguments for the ok event.
     */
    ,okEventArgs : Ext.isEmpty

    /**
     * Fires a cancel event and closes this window.
     */
    ,cancel : function() {
        if (this.fireEvent('cancel', this) !== false) {
            this.close();
        }
    }

    // override
    ,onRender : function() {
        R.ui.Dialog.superclass.onRender.apply(this, arguments);

        if (this.focusField) {
            this.focusField.focus(false, 10);
        }
        else {
            // focus first field
            var first = function(f) {
                if (f.isFormField && !f.hidden && !f.disabled && f.xtype !== 'checkbox') {
                    f.focus(false, 10);
                    return true;
                }
                if (f.items && f.items.find) {
                    return f.items.find(first);
                }
                return false;
            };
            if (this.items) {
                this.items.find(first);
            }
        }

        // enter key
        if (this.okOnEnter) {
            new Ext.KeyNav(this.id, {
                scope  : this
                ,enter : this.ok
            });
        }
    }

    // override
    ,focus : Ext.emptyFn
});

Ext.apply(R.ui.Dialog, {
    /**
     * Button config that displays a single OK button.
     * @static
     */
    OK : {
        create : function(dialog) {
            return [
                {
                    cls      : 'button-check x-btn-text-icon'
                    ,text    : 'Close'
                    ,scope   : dialog
                    ,handler : dialog.ok
                }
            ];
        }
    }

    /**
     * Button config that displays a OK and Cancel buttons
     * @static
     */
    ,OKCANCEL : {
        create : function(dialog) {
            return [
                {
                    cls      : 'button-check x-btn-text-icon'
                    ,text    : 'OK'
                    ,scope   : dialog
                    ,handler : dialog.ok
                }
                ,{
                    cls      : 'button-x x-btn-text-icon'
                    ,text    : 'Cancel'
                    ,scope   : dialog
                    ,handler : dialog.cancel
                }
            ];
        }
    }
});
