/**
 * @class R.form.LabelField
 * A field which displays text.
 */
R.form.LabelField = Ext.extend(Ext.form.Field, {

    /**
     * @cfg {String/Object} autoCreate A DomHelper element spec, or true for a
     * default element spec (defaults to {tag: "div"}
     */
    defaultAutoCreate : {tag: 'div'}

    /**
     * @cfg {String} fieldClass The default CSS clas for the field (defaults
     * to "x-form-field rf-labelfield"
     */
    ,fieldClass : 'x-form-field'

    // without this the labels don't line up properly with the field label
    ,style : 'padding-top: 2px'

    ,value: undefined

    ,constructor : function(config) {
        if (config.guid && R.AC) {
            var ac = R.AC.get(config.guid);
            if (ac && ac.guid) {
                if (ac.units) {
                    this.units = ac.units;
                }
                if (ac.printf) {
                    this.printf = ac.printf;
                }
                if (ac.type) {
                    this.type = ac.type;
                }
            }
        }
        R.form.LabelField.superclass.constructor.call(this, config);
    }

    ,onRender: function(ct, position) {
        R.form.LabelField.superclass.onRender.call(this, ct, position);

        if (!this.el) {
            var cfg = this.getAutoCreate();
            if (!cfg.name) {
                cfg.name = this.name || this.id;
            }
            this.el = ct.createChild(cfg, position);
        }
        this.el.addClass([this.fieldClass, this.cls]);
        this.initValue();
    }

    ,initValue: function() {
        if (!Ext.isEmpty(this.value)) {
            this.setValue(this.value);
        }
    }

    ,isDirty: function() {
        return false;
    }

    ,getRawValue: function() {
        // this.el is not there for "invisible" fields
        if (this.rendered) {
            return this.el.dom.innerHTML;
        }
        return null;
    }

    ,setRawValue: function(v) {
        if (this.rendered) {
            if (v !== null) {
                if (this.units) {
                    v = this.units.tolocal(v);
                }
                if (this.printf && typeof v === 'number') {
                    var decimals = parseFloat(this.printf.substring(this.printf.indexOf("%.") + 2, this.printf.indexOf("f")));
                    v = v.toFixed(decimals);
                }
                // If this is an integer, truncate the decimals down to 0
                if (this.type === 'long') {
                    // Make sure we're dealing with a number
                    if (typeof v === 'string') {
                        v = parseInt(v);
                    }
                    v = v.toFixed(0);
                }
                if (this.units) {
                    v = v + ' ' + this.units.shortlabel();
                }
                this.el.dom.innerHTML = Ext.util.Format.htmlEncode(v);
            }
            else {
                this.el.dom.innerHTML = '';
            }
        }
    }

    ,getValue : function() {
        return this.getRawValue();
    }

    ,setValue : function(v) {
        this.value = v;
        this.setRawValue(this.value);
    }
});
Ext.reg('r-label', R.form.LabelField);


