/**
 * The application entry point.
 * @class R.App
 * @singleton
 * @cfg {R.AppFactory} factory The factory used to generate various UI elements
 * @cfg {String} view The view to select
 */
R.App = function() {

    var showModule = function(module, params) {
        var north = this.north;
        var center = this.center;
        var navigation = this.navigation;
        var current = navigation.getSelectedModuleID();

        // hide the previous mask in case user selects a module before the
        // current module has fired a refresh event
        if (this.mask) {
            this.mask.hide();
        }

        var proceed = function() {
            // create module component
            var ui = module.createUI();
            ui.on('afterlayout', function() {
                // because we defer the afterlayout event it's possible we cleanup
                // the module before it gets a chance to be displayed
                if (module.isVisible() && this.fireEvent('beforemodulerefresh', this, module) !== false) {
                    this.mask = R.ui.Mask.show({
                        waitMsg        : 'Loading: ' + module.label()
                        ,waitMsgDelay  : 200
                        ,waitMsgTarget : ui.el
                    });
                    module.on('refresh', function() {
                        var current = this.navigation.getSelectedModuleID();
                        if (current === module.id) {
                            this.mask.hide();
                            delete this.mask;
                        }
                        this.fireEvent('moduleselect', this, module);
                    }, this, { single: true });
                    refresh(module, params);
                }
            }, this, { single: true, delay: 1 });

            var title = this.title + ': ' + module.label();

            // north
            if (north) {
                if (!this.titleEl) {
                    this.titleEl = north.el.child('.module-header-label');
                }
                if (this.titleEl) {
                    this.titleEl.dom.innerHTML = title;
                }
                north.removeClass(current);
                north.addClass(module.id);
                north.doLayout();
            }
            // center
            center.removeAll(false); // components destroyed by Module.cleanup
            center.removeClass(current);
            center.addClass(module.id);
            center.add(ui);
            center.doLayout();

            // update the browser title to include module label
            document.title = title;
        }.createDelegate(this);

        navigation.select(module, params, proceed);
    };

    var refresh = function(module, params) {
        module.initialize();
        module.initialize = Ext.emptyFn; // next initialize call does nothing now
        module.refresh(params);
    };

    // Used to ignore a history event when the user cancels moving away from
    // the current module. Removing the history listener temporarily does not
    // work because the history is handled by a timer. The bad sequence is
    // remove listener, call Ext.History.back(), add listener but the add
    // listener occurs before the timer triggers which fires the history change.
    var _ignoreOnHistoryChange = false;

    var onHistoryChange = function(token) {
        if (_ignoreOnHistoryChange) {
            _ignoreOnHistoryChange = false;
            return;
        }

        var tokens = token ? token.split(':') : [];
        var params = {};

        // split name=value pairs
        for (var i = 0; i < tokens.length; i++) {
            var pair = tokens[i].split('=');
            var name = pair[0];
            var value = (pair.length) > 0 ? pair[1] : '';

            if (name !== 'view') {
                value = Ext.state.Provider.prototype.decodeValue(decodeURIComponent(value));
            }
            params[name] = value;
        }

        var mid = params['view'];
        delete params.view;

        // update the UI
        var current = R.ModuleManager.get(this.navigation.getSelectedModuleID());
        var module  = R.ModuleManager.get(mid);
        var app     = this;

        params = params || {};

        // ensure module exists
        if (!module || !module.isAllowed()) {
            // find first allowed module
            module = R.ModuleManager.first();
        }

        if (current) {
            if (current.id === module.id) {
                this.navigation.select(module, params, function() {
                    refresh(module, params);
                });
            }
            else {
                current.hide(function(proceed) {
                    if (proceed) {
                        current.cleanup();
                        showModule.call(app, module, params);
                    }
                    else {
                        // the user cancelled the history change, therefore
                        // go back in history to ensure we are at the correct
                        // place in history
                        _ignoreOnHistoryChange = true;
                        Ext.History.back();
                    }
                });
            }
        }
        else {
            showModule.call(app, module, params);
        }
    };

    var createNorth = function(factory) {
        var header = factory.header();
        var items = [
            {
                cls    : 'module-header-label'
                ,xtype : 'label'
            }
            ,{
                flex   : 1
                ,xtype : 'spacer'
            }
        ];

        if (header) {
            items = items.concat(header);
        }
        items.push({
                autoEl : 'div'
                ,cls   : 'module-header-logo'
                ,xtype : 'box'
            }
            ,{
                width  : 8
                ,xtype : 'spacer'
            }
        );

        return new Ext.Panel({
            bodyStyle : 'padding-right: 10px'
            ,frame    : true
            ,header   : true
            ,height   : Ext.isIE ? 52 : 50
            ,id       : 'north'
            ,layout   : {
                align     : 'middle'
                ,innerCls : 'x-box-inner module-header'
                ,type     : 'hbox'
            }
            ,region : 'north'
            ,items  : items
        });
    };

    var _impl = Ext.extend(Ext.util.Observable, {

        constructor : function() {
            Ext.util.Observable.prototype.constructor.call(this);

            this.addEvents(
                /**
                 * @event beforemodulerefresh
                 * Fires before a module is refreshed. Return false to stop the refresh.
                 * @param {R.App} this
                 * @param {Module} the module
                 */
                'beforemodulerefresh'

                /**
                 * @event moduleselect
                 * Fires when a module is selected and after it has refreshed.
                 * @param {R.App} this
                 * @param {Module} the module
                 */
                ,'moduleselect'

                /**
                 * @event beforestart
                 * Fires during the start function before UI creation.
                 * @param {R.App} this
                 */
                ,'beforestart'
            );
        }

        /**
         * Navigates to the module with the given ID.
         * @param {String} mid The module ID
         * @param {Object} params Name/value pairs passed as arguments to the
         * @return {Boolean} true if selected module and parameters differs
         * from the current values
         */
        ,select : function(mid, params) {
            var tokens = [ 'view=' + mid ];
            var names = [];
            var name, value;

            // access the keys in sorted order to ensure the history does not
            // change if the new params are the same as the old params
            for (name in params) {
                names.push(name);
            }
            names.sort();
            for (var i = 0; i < names.length; i++) {
                name = names[i];
                value = params[name];
                if (!Ext.isEmpty(value)) {
                    if (name !== 'view') {
                        value = Ext.state.Provider.prototype.encodeValue(value);
                    }
                    tokens.push(name + '=' + value);
                }
            }

            var token = tokens.join(':');
            var changed = (token !== Ext.History.getToken());
            if (changed) {
                Ext.History.add(token);
            }
            return changed;
        }

        /**
         * Starts the application. Must be called from Ext.onReady
         * @method
         */
        ,start : function(config) {
            var factory = config.factory;

            Ext.apply(this, config);

            this.id = factory.id;

            this.fireEvent('beforestart', this);

            Ext.History.init(function() {
                this.navigation = factory.navigation();
                this.navigation.on('selectionchange', function(mid) {
                    Ext.History.add('view=' + mid);
                }, this);
                R.ModuleManager.each(function(module) {
                    if (module.isAllowed()) {
                        this.navigation.add(module);
                    }
                }, this);

                // module components are placed in here
                this.center = new Ext.Container({
                    id      : 'center'
                    ,layout : 'fit'
                    ,region : 'center'
                });

                if (!factory.hideModuleHeader) {
                    this.north = createNorth(factory);
                }

                this.status = factory.status({
                    region : 'south'
                });

                var navUI = this.navigation.createUI({
                    id      : 'navigation'
                    ,region : 'west'
                    ,split  : true
                    ,width  : 220
                });

                var items = [];
                if (this.north) {
                    items.push(this.north);
                }
                if (navUI) {
                    items.push(navUI);
                }
                items.push(this.center);

                if (this.status) {
                    items.push(this.status);
                }

                new Ext.Viewport({
                    layout : 'border'
                    ,items : items
                });

                Ext.History.on('change', onHistoryChange, this);
                var token = Ext.History.getToken();
                if (token) {
                    onHistoryChange.call(this, token);
                }
                else {
                    Ext.History.add('view=' + (this.view || R.ModuleManager.first().id));
                }
            }, this);
        }
    });

    return new _impl();
}();
