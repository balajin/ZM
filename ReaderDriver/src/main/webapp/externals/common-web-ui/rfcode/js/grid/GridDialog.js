/**
 * @class R.grid.GridDialog
 * @extends R.ui.Dialog
 * A dialog which displays a grid. Fires the ok event when the ok button is
 * clicked or a row is double selected.
 * @cfg {Object} gridConfig The config object used to create the grid.
 */
R.grid.GridDialog = Ext.extend(R.ui.Dialog, {
    constructor : function(config) {
        this.grid = new Ext.grid.GridPanel(Ext.apply({
            sm          : new Ext.grid.RowSelectionModel({singleSelect: true})
            ,viewConfig : {
                forceFit : true
            }
        }, config.gridConfig));

        delete config.gridConfig;

        R.grid.GridDialog.superclass.constructor.call(this, Ext.apply({
            layout       : 'fit'
            ,maximizable : true
            ,modal       : true
            ,items       : this.grid
        }, config));

        this.setOkButtonDisabled(true);

        this.grid.on('rowdblclick', this.ok, this);
        this.grid.getSelectionModel().on('selectionchange', this.onSelectionChange, this);
    }

    // private
    ,onSelectionChange : function(sm) {
        this.setOkButtonDisabled(!sm.hasSelection());
    }

    // override
    ,okEventArgs : function() {
        var record = this.grid.getSelectionModel().getSelected();
        return [ record ];
    }
});
