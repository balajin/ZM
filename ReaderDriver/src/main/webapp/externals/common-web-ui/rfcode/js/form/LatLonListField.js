
/**
 * @class R.form.LatLonListField
 * @extends R.form.GridPanelField
 */
R.form.LatLonListField = Ext.extend(R.form.GridPanelField, {

    // private
    Record : Ext.data.Record.create([
        { name  : 'latDisplay' }
        ,{ name : 'lonDisplay' }
    ])

    /**
     * constructor
     */
    ,constructor : function(config) {
        var store = new Ext.data.ArrayStore({
            fields : [ 'latDisplay', 'lonDisplay' ]
        });
        this.descr = new R.form.LabelField({
            labelSeparator : ''
            ,value         : ''
        });

        R.form.LatLonListField.superclass.constructor.call(this, Ext.apply({
            columns  : [
                 {  id : 'latDisplay', header : 'Latitude',  width : 100, dataIndex : 'latDisplay', sortable : false }
                 ,{ id : 'lonDisplay', header : 'Longitude', width : 100, dataIndex : 'lonDisplay', sortable : false }
            ]
            ,store   : store
            ,bbar    : [ this.descr ]
        }, config));
    }

    /**
     * getValue
     */
    ,getValue : function() {
        var result = null;
        var count = this.store.getCount();

        if (count > 0) {
            result = [];

            for (var i = 0; i < count; i++) {
                var record = this.store.getAt(i);
                result.push( [ record.get('lat'), record.get('lon') ] );
            }
        }
        return result;
    }

    /**
     * setValue
     */
    ,setValue : function(value) {
        this.store.removeAll();

        if (value) {
            var records = [];
            for (var i = 0; i < value.length; i++) {
                var coord = value[i];

                var latDisplay = R.AC.prototype.latLonValueToString(coord[0], "N", "S");
                var lonDisplay = R.AC.prototype.latLonValueToString(coord[1], "E", "W");

                records.push(new Ext.data.Record({ latDisplay : latDisplay, lonDisplay : lonDisplay, lat : coord[0], lon : coord[1] }));
            }
            this.store.add(records);
        }
        this.updateMessage();
    }

    /**
     * override
     */
    ,isDirty : function() {
        if (this.disabled || !this.editable) {
            return false;
        }

        var store = this.store;
        var count = store.getCount();
        var ov    = this.originalValue;
        var i, o1, o2;

        if (count !== (ov ? ov.length : 0)) {
            return true;
        }

        if (ov) {
            for (i = 0; i < ov.length; i++) {
                o1 = ov[i];
                o2 = store.getAt(i).data;

                if (o1[0] != o2.lat || o1[1] != o2.lon) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * private
     */
    ,onAddAction : function() {
        this.showDialog(true);
    }

    /**
     * private
     */
    ,onEditAction : function() {
        this.showDialog(false);
    }

    /**
     * private
     */
    ,updateMessage : function() {
        var count = this.store.getCount();
        if (count  > 0) {
            if (count == 1) {
                var record = this.store.getAt(0);
                this.descr.setValue('Location: Circle with 100 meter radius centered at ' + record.get('latDisplay') + ' ' + record.get('lonDisplay'));
            }
            else if (count == 2) {
                this.descr.setValue('Location: Bounded rectangle');
            }
            else {
                this.descr.setValue('Location: ' + count + ' sided polygon');
            }
        }
        else {
            this.descr.setValue('');
        }
    }

    /**
     * private
     */
    ,showDialog : function(isAdd, curVal) {

        var record = undefined;
        var msgTitle;

        if (isAdd) {
            record = null;
            msgTitle = 'New GPS Coordinate';
        }
        else {
            msgTitle = 'Edit GPS Coordinate';
            var temp = this.getSelectionModel().getSelected();
            if (temp) {
                record = temp;

                if (!Ext.isDefined(curVal)) {
                    curVal = record.get('latDisplay') + ' ' + record.get('lonDisplay');
                }
            }
        }

        if (Ext.isDefined(record)) {
            Ext.Msg.prompt(msgTitle, 'GPS Coordinate:', function(btn, text) {
                if (btn == 'ok'){
                    // process text value and close...
                    var coords = R.AC.prototype.parseCoordinateString(text);
                    if (coords) {

                        var latDisplay = R.AC.prototype.latLonValueToString(coords[0], "N", "S");
                        var lonDisplay = R.AC.prototype.latLonValueToString(coords[1], "E", "W");

                        if (record) {
                            record.set('lat', coords[0]);
                            record.set('latDisplay', latDisplay);
                            record.set('lon', coords[1]);
                            record.set('lonDisplay', lonDisplay);
                        }
                        else {
                            var records = [];
                            records.push(new Ext.data.Record({ latDisplay : latDisplay, lonDisplay : lonDisplay, lat : coords[0], lon : coords[1] }));
                            this.store.add(records);
                        }
                        this.updateMessage();
                    }
                    else {
                        Ext.Msg.alert(msgTitle, 'Could not parse gps coordinate: ' + text, function() {
                            this.showDialog(isAdd, text);
                        }, this);
                    }
                }
            }, this, false, curVal);
        }
    }

    /**
     * private
     */
    ,onDeleteAction : function() {
        var record = this.getSelectionModel().getSelected();
        if (record) {
            // removed record.
            this.store.remove(record);
            this.updateMessage();
        }
    }
});
