/**
 * @class R.form.DualTreeField
 * @extends Ext.Container
 * DualTreeField is a field which allows a user to add and remove items
 * from a left tree and a right tree.
 */
R.form.DualTreeField = Ext.extend(Ext.Container, {
    /**
     * @cfg {String} fromTitle
     * The title used for the available items tree
     */
    /**
     * @cfg {String} toTitle
     * The title used for the selected items tree
     */
    /**
     * @cfg {Ext.tree.TreeNode} root (optional)
     * The root node for the left tree.
     */

    autoEl : 'div'
    ,focusClass : undefined
    ,isFormField : true
    ,msgTarget : 'side'

    ,constructor : function(config) {
        this.moveRightButton = new Ext.Button({
            cls       : 'button-move-right x-btn-icon'
            ,handler  : this.moveRight
            ,scope    : this
            ,tooltip  : 'Add'
            ,minWidth : 30
            ,disabled : true
            ,margins  : '0 0 4 0'
        });
        this.moveLeftButton = new Ext.Button({
            cls       : 'button-move-left x-btn-icon'
            ,xtype    : 'button'
            ,handler  : this.moveLeft
            ,scope    : this
            ,tooltip  : 'Remove'
            ,minWidth : 30
            ,disabled : true
            ,margins  : '4 0 0 0'
        });

        /**
         * The left tree.
         * @type Ext.tree.TreePanel
         * @property ltTree
         */
        this.ltTree = new R.tree.FilterTreePanel({
            cls          : 'dual-tree-field-tree'
            ,flex        : 1
            ,frame       : false
            ,hideExpandCollapse : true
            ,root        : config.root || new Ext.tree.TreeNode()
            ,rootVisible : false
            ,selModel    : new Ext.tree.MultiSelectionModel()
            ,sort        : true
            ,title       : config.fromTitle || 'Available'
            ,height      : 250 //temp fix, need to make height fit
            ,listeners   : {
                'dblclick' : { scope: this, fn: this.moveRight }
            }
        });
        this.ltTree.getSelectionModel().on('selectionchange', function(selmodel, nodes) {
            this.moveRightButton.setDisabled(!nodes || nodes.length === 0);
        }, this);

        /**
         * The right tree.
         * @type Ext.tree.TreePanel
         * @property rtTree
         */
        this.rtTree = new R.tree.FilterTreePanel({
            cls          : 'dual-tree-field-tree'
            ,flex        : 1
            ,frame       : false
            ,hideExpandCollapse : true
            ,root        : config.rtRoot || new Ext.tree.TreeNode()
            ,rootVisible : false
            ,selModel    : new Ext.tree.MultiSelectionModel()
            ,title       : config.toTitle || 'Selected'
            ,height      : 250 //temp fix, need to make height fit
            ,listeners   : {
                'dblclick' : { scope: this, fn: this.moveLeft }
            }
        });
        this.rtTree.getSelectionModel().on('selectionchange', function(selmodel, nodes) {
            var disable = !nodes || nodes.length === 0;
            if (!disable) {
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].getDepth() > 1) {
                        disable = true;
                        break;
                    }
                }
            }
            this.moveLeftButton.setDisabled(disable);
        }, this);
        new Ext.tree.TreeSorter(this.rtTree);

        R.form.DualTreeField.superclass.constructor.call(this, Ext.apply({
            layout : {
                align : 'stretch'
                ,type : 'hbox'
            }
            ,items : [
                this.ltTree
                // add/remove buttons
                ,{
                    autoEl  : 'div'
                    ,layout : {
                        align : 'center'
                        ,pack : 'center'
                        ,type : 'vbox'
                    }
                    ,style  : 'margin: 0 4 0 4'
                    ,width  : 30
                    ,xtype  : 'container'
                    ,items  : [
                        this.moveRightButton, this.moveLeftButton
                    ]
                }
                ,this.rtTree
            ]
        }, config));

        // cache the original hierarchy
        // key = node ID, value = node
        this.cache = {};
        this.root = this.copyNode(this.ltTree.getRootNode(), true);
        this.root.cascade(function(node) {
            var parent = node.parentNode;
            if (parent) {
                this.cache[node.id] = node;
            }
        }, this);
        // remove link to invisible root
        for (var i = 0; i < this.root.childNodes.length; i++) {
            this.root.childNodes[i].parentNode = null;
        }
    }

    ,focus : function() {
        if (this.ltTree) {
            this.ltTree.focus();
        }
    }

    ,onDestroy : function() {
        Ext.destroy(this.ltTree, this.rtTree, this.panel);
        R.form.DualTreeField.superclass.onDestroy.call(this);
    }

    ,afterRender : function(){
        R.form.DualTreeField.superclass.afterRender.call(this);
        this.initValue();
    }

    ,initValue : function() {
        this.setValue(this.value || []);
        this.originalValue = this.value;
    }

    ,getValue : function() {
        return this.value;
    }

    ,setValue : function(value) {
        var ltTree = this.ltTree;
        var ltRoot = this.ltTree.getRootNode();
        var rtTree = this.rtTree;
        var rtRoot = this.rtTree.getRootNode();

        ltRoot.beginUpdate();
        rtRoot.beginUpdate();

        // clear the current selection
        ltRoot.removeChildren();
        rtRoot.removeChildren();

        // add nodes to right tree
        for (var i = 0; i < value.length; i++) {
            var node = this.cache[value[i]];
            if (node) {
                rtRoot.appendChild(this.copyNode(node, true));
            }
        }

        // add nodes to left tree
        var children = this.root.childNodes;
        for (var i = 0; i < children.length; i++) {
            ltRoot.appendChild(this.copyNode(children[i], true));
        }

        // remove nodes in left tree contained in right tree
        for (var i = 0; i < value.length; i++) {
            var node = ltTree.getNodeById(value[i]);
            if (node) {
                node.remove();
                node.destroy();
            }
        }

        // expand left tree node if root contains a single child
        if (ltRoot.childNodes.length === 1) {
            ltRoot.childNodes[0].expanded = true;
        }

        ltRoot.endUpdate();
        rtRoot.endUpdate();

        this.value = value;
    }

    // private
    ,valueChanged : function() {
        this.value = R.map(this.rtTree.getRootNode().childNodes, function(node) {
            return node.id;
        });
    }

    // private
    ,copyNode : function(node, recurse) {
        var copy = new Ext.tree.TreeNode(node.attributes);
        if (recurse) {
            var children = node.childNodes;
            for (var i = 0; i < children.length; i++) {
                copy.appendChild(this.copyNode(children[i], true));
            }
        }
        return copy;
    }

    // private
    ,moveRight : function() {
        var nodes = this.ltTree.getSelectionModel().getSelectedNodes();
        if (Ext.isArray(nodes) && nodes.length > 0) {
            this.onMoveRight(nodes);
        }
    }

    // private
    ,moveLeft : function() {
        if (!this.moveLeftButton.disabled) {
            var nodes = this.rtTree.getSelectionModel().getSelectedNodes();
            if (Ext.isArray(nodes) && nodes.length > 0) {
                this.onMoveLeft(nodes);
            }
        }
    }

    // private
    ,isAncestor : function(parent, node) {
        var node = this.cache[node.id];
        while (node) {
            if (node.id === parent.id) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    }

    /**
     * Called when the user chooses to move nodes from the left tree to the
     * right tree.
     * @param {Array} nodes Array of selected nodes in the left tree
     * @method
     */
    ,onMoveRight : function(selected) {
        var ltRoot = this.ltTree.getRootNode();
        var rtTree = this.rtTree;
        var rtRoot = this.rtTree.getRootNode();
        var ltNode, rtNode, ltParent, rtParent, children;

        ltRoot.beginUpdate();
        rtRoot.beginUpdate();
        for (var i = selected.length-1; i >= 0; i--) {
            ltNode = selected[i];

            // remove rtNode if its a child of node we are moving to right
            children = rtRoot.childNodes;
            for (var j = children.length-1; j >= 0; j--) {
                rtNode = children[j];
                if (this.isAncestor(ltNode, rtNode)) {
                    rtNode.remove();
                    rtNode.destroy();
                }
            }

            // check if node already in tree
            rtNode = rtTree.getNodeById(ltNode.id);
            if (rtNode) {
                rtParent = rtNode.parentNode;
            }
            else {
                // check if node's parent is already in the tree
                ltParent = ltNode.parentNode;
                rtParent = rtTree.getNodeById(ltParent.id);

                if (!rtParent) {
                    rtParent = rtRoot;
                }
            }
            rtParent.appendChild(this.copyNode(this.cache[ltNode.id], true));

            ltNode.remove();
            ltNode.destroy();
        }
        rtRoot.endUpdate();
        ltRoot.endUpdate();
        this.valueChanged();
    }

    /**
     * Called when the user chooses to move nodes from the to tree to the from
     * tree.
     * @param {Array} nodes Array of selected nodes in the right tree
     * @method
     */
    ,onMoveLeft : function(selected) {
        var ltTree = this.ltTree;
        var ltRoot = this.ltTree.getRootNode();
        var rtRoot = this.rtTree.getRootNode();

        var ltNode, rtNode, chNode, ltParent, chParent;

        ltRoot.beginUpdate();
        rtRoot.beginUpdate();
        for (var i = selected.length-1; i >= 0; i--) {
            rtNode = selected[i];

            // find original parent (use cache so we get original parent)
            chNode = this.cache[rtNode.id];
            chParent = chNode.parentNode;

            if (chParent) {
                ltParent = ltTree.getNodeById(chParent.id);
            }
            else {
                ltParent = ltRoot;
            }
            ltParent.appendChild(this.copyNode(chNode, true));

            rtNode.remove();
            rtNode.destroy();
        }
        rtRoot.endUpdate();
        ltRoot.endUpdate();
        this.valueChanged();
    }


    // Required by Ext.form.Field
    ,markInvalid : Ext.form.Field.prototype.markInvalid
    ,getMessageHandler : Ext.form.Field.prototype.getMessageHandler
    ,setActiveError : Ext.form.Field.prototype.setActiveError
    ,getErrorCt : Ext.form.Field.prototype.getErrorCt
    ,alignErrorIcon : Ext.form.Field.prototype.alignErrorIcon
    ,isDirty : Ext.form.Field.prototype.isDirty
});
