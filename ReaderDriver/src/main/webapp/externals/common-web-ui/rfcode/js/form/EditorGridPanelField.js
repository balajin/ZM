/**
 * @class R.form.EditorGridPanelField
 * @extends Ext.grid.EditorGridPanel
 * An EditorGridPanel which acts like a form field. It is up to subclasses to
 * provide getValue() and setValue() functions.
 */
R.form.EditorGridPanelField = Ext.extend(Ext.grid.EditorGridPanel, {

    isFormField : true

    ,msgTarget : 'side'

    ,constructor : function(config) {
        var anchor = Ext.isEmpty(config.width) ? '-20' : null
        R.form.EditorGridPanelField.superclass.constructor.call(this, Ext.apply({
            anchor      : anchor
            ,height     : this.height || 120
            ,viewConfig : { forceFit: true }
        }, config));
    }

    // override
    ,afterRender : function() {
        R.form.EditorGridPanelField.superclass.afterRender.call(this);
        this.initValue();
    }

    // override
    ,initValue : function() {
        this.originalValue = this.getValue();
    }

    // override
    ,getValue : function() {
        var value = [];
        this.store.each(function(r) {
            value.push(Ext.apply({}, r.data));
        });
        return value;
    }

    // override
    ,setValue : function(v) {
        this.store.removeAll();
        if (v) {
            this.store.modified = [];
            this.store.loadData(v);
        }
    }

    // override
    ,isValid : function() {
        return true;
    }

    // override
    ,validate : function() {
        this.clearInvalid();
        return true;
    }

    // override
    ,isDirty : function() {
        var modified = this.store.modified;
        return modified && modified.length > 0;
    }

    // override
    ,getName : Ext.form.Field.prototype.getName

    // override
    ,markInvalid : Ext.form.Field.prototype.markInvalid

    // override
    ,clearInvalid : function() {
        // prevents the 'valid' event from getting fired if field is already valid
        if (this.el && this.el.hasClass(this.invalidClass)) {
            Ext.form.Field.prototype.clearInvalid.call(this);
        }
    }

    // override
    ,getErrorCt : Ext.form.Field.prototype.getErrorCt

    // override
    ,alignErrorIcon : Ext.form.Field.prototype.alignErrorIcon

    // override
    ,onDisable : Ext.emptyFn

    // override
    ,onEnable : Ext.emptyFn
});
