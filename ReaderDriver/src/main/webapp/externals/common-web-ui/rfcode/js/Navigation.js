/**
 * @class R.Navigation
 */
R.Navigation = Ext.extend(Ext.util.Observable, {
    constructor : function() {
        R.Navigation.superclass.constructor.call(this);

        this.addEvents(
            /**
             * Fired when the selected module changes.
             * @event selectionchange
             */
            'selectionchange'
        );
    }

    /**
     * Returns the component representing the navigation.
     * @return {Ext.Component} navigation
     */
    ,createUI : Ext.emptyFn

    /**
     * Adds the given module to the navigation UI.
     * @param {R.Module} module
     */
    ,add : Ext.emptyFn

    /**
     * Returns the selected module ID.
     * @return {String} module ID
     */
    ,getSelectedModuleID : function() {
        return this.selected;
    }

    /**
     * Selects the given module. Does not fire a selectionchange event.
     * @param {R.Module} module
     * @param {Mixed} params Name/value pairs used as arguments eventually
     * passed to the selected module in its refresh method.
     * @param {Function} proceed Function which must be called when the
     * navigation is ready to proceed with selecting the module.
     */
    ,select : function(module, params, proceed) {
        this.selected = module.id;
        proceed();
    }
});
