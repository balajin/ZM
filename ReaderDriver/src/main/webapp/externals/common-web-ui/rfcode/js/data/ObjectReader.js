/**
 * @class R.data.ObjectReader
 * @extends Ext.data.JsonReader
 * Reader which knows how to interpret json from an object. Each property of the
 * input corresponds to a row in the data.
 * <pre><code>
key : {
    attributes : {
        foo : 'bar'
    },
    id : key
}
</code></pre>
 * @cfg {Function} transform (optional) A function which transforms the data
 * before creating a record
 */
R.data.ObjectReader = Ext.extend(Ext.data.JsonReader, {
    constructor : function(meta, recordType) {
        meta = meta || {};
        if (!meta.fields) {
            meta.fields = [ 'id' ];
        }
        R.data.ObjectReader.superclass.constructor.call(this, meta, recordType);

        if (meta.transform) {
            this.transform = meta.transform;
        }
        this.scope = meta.scope;
    }

    // override
    ,read : function(response) {
        var o = Ext.decode(response.responseText);
        if(!o) {
            throw {message: "ObjectReader.read: Json object not found"};
        }
        return this.readRecords(o);
    }

    // override
    ,readRecords : function(objs) {
        var recordType = this.recordType;
        var records = [];
        var success = objs.__result !== 'error';

        // used by Ext.data.Store
        this.onMetaChange(this.meta, this.recordType, objs);

        if (success) {
            for (var key in objs) {
                if (objs.hasOwnProperty(key)) {
                    var obj = objs[key];
                    records.push(new recordType(Ext.apply({
                        id : key
                    }, this.transform.call(this.scope, obj.attributes || obj)), key));
                }
            }
        }
        return {
            records : records
            ,success : success
            ,totalRecords : records.length
        };
    }

    ,transform : function(attrs) {
        return attrs;
    }
});
