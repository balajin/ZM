/**
 * @class R.ui.PanelActions
 * @cfg {Boolean} tbarAddFirst If true add the actions to the front of the top
 * toolbar
 * @cfg {Boolean} tbarAddFirst If true add the actions to the front of the
 * bottom toolbar
 */
R.ui.PanelActions = Ext.extend(Object, {
    constructor: function(config) {
        this.mgr = new R.ui.ActionManager(config);
    }

    ,init: function(p, sm) {
        var mgr = this.mgr;
        var tbar = mgr.queryActions('tbar', true);
        var bbar = mgr.queryActions('bbar', true);
        var btns = mgr.queryActions('button');
        var akeys = mgr.queryActions('key');
        var tbarAddFirst = this.mgr.tbarAddFirst;

        if (typeof p.getSelectionModel === 'function') {
            sm = p.getSelectionModel();
        }
        mgr.setSelectionModel(sm);

        if(tbar.length){
            var topToolbar = p.topToolbar;
            if(!topToolbar){
                p.topToolbar = p.createToolbar(tbar);
                p.elements += ',tbar';
            }
            else if(Ext.isArray(topToolbar)){
                p.topToolbar = tbarAddFirst ? tbar.concat(topToolbar) : topToolbar.concat(tbar);
            }
            else if(topToolbar.add){
                if (tbarAddFirst) {
                    topToolbar.insertButton(0, tbar);
                }
                else {
                    topToolbar.add.apply(topToolbar, tbar);
                }
            }
            else if(Ext.isArray(topToolbar.items)) {
                if (tbarAddFirst) {
                    topToolbar.items = tbar.concat(topToolbar.items);
                }
                else {
                    topToolbar.items = topToolbar.items.concat(tbar);
                }
            }
        }
        if(bbar.length){
            var bottomToolbar = p.bottomToolbar;
            if(!bottomToolbar){
                p.bottomToolbar = p.createToolbar(bbar);
                p.elements += ',bbar';
            }
            else if(Ext.isArray(bottomToolbar)){
                p.bottomToolbar = bbarAddFirst ? bbar.concat(bottomToolbar) : bottomToolbar.concat(bbar);
            }
            else if(bottomToolbar.add){
                if (bbarAddFirst) {
                    bottomToolbar.insertButton(0, bbar);
                }
                else {
                    bottomToolbar.add.apply(bottomToolbar, bbar);
                }
            }
            else if(Ext.isArray(bottomToolbar.items)) {
                if (bbarAddFirst) {
                    bottomToolbar.items = bbar.concat(bottomToolbar.items);
                }
                else {
                    bottomToolbar.items = bottomToolbar.items.concat(bbar);
                }
            }
        }
        if(btns.length){
            var buttons = p.buttons;
            if(!buttons){
                buttons = [];
            }
            for (var i = 0; i < btns.length; i++) {
                if(btns[i].render){ // button instance
                    btns[i].ownerCt = this;
                    buttons.push(btns[i]);
                }
                else{
                    btns[i].initialConfig.minWidth = p.minButtonWidth;
                    buttons.push(new Ext.Button(btns[i]));
                }
            }
            p.createFbar(buttons);
        }
        if(akeys.length){
            var keys = p.keys;
            if(!keys){
                keys = p.keys = [];
            }
            for(var i = 0, len = akeys.length; i < len; i++){
                var action = akeys[i], key = action.key;
                if (Ext.isArray(key) || typeof key === 'string') {
                    key = {key: key};
                }
                keys.push(Ext.apply({
                    handler: action.execute,
                    scope: action
                }, key));
            }
        }
        p.on('render', mgr.update, mgr, {single: true});

        this.mgr.component = p;
    }
});

