/**
 * @class R.form.SaveFormPlugin
 * Adds a save button and a validation status to a form's bottom toolbar. Or
 * hooks into a dialog's (R.ui.Dialog) ok button to submit the form.
 * @cfg {Function} onSubmit Returns a configuration object used by the
 * form's submit action. If the FormPanel contains an onSubmit function this
 * plugin will use it.
 * @cfg {Object} scope (optional) Scope for the onSubmit function
 * @cfg {Object} options Options passed to the form submit function.
 * @cfg {Boolean} showStatusBar False to not show a status bar for form validation
 */
R.form.SaveFormPlugin = function(config) {
    Ext.apply(this, config || {});

    this.options = this.options || {};
    this.options.method = this.options.method || 'POST';

    this.showStatusBar = Ext.value(this.showStatusBar, true);
};

R.form.SaveFormPlugin.prototype = {

    init : function(panel) {
        var items = [];

        if (panel instanceof R.ui.Dialog) {
            panel.on('ok', this.onSaveFormButtonClick, this);
            this.dialog = panel;
        }
        else {
            /**
             * The save button
             * @property saveButton
             */
            this.saveButton = new R.ui.SaveButton({
                disabled : true
                ,text    : 'Save Changes'
                ,scope   : this
                ,handler : this.onSaveFormButtonClick
            });
            items.push(this.saveButton);
        }

        if (this.showStatusBar) {
            this.validation = new Ext.ux.ValidationStatus();
            this.status = new Ext.StatusBar({
                defaultText  : '&nbsp;'
                ,statusAlign : 'right'
                ,items       : items
                ,plugins     : this.validation
            });

            panel.toolbars.push(this.status);
            panel.bottomToolbar = this.status;
            if (panel.elements.indexOf('bbar') === -1) {
                panel.elements += ',bbar';
            }
        }
        if (this.formpanel) {
            var formpanel = this.formpanel;
            delete this.formpanel;
            this.setFormPanel(formpanel);
        }
        else if (panel.form) {
            this.setFormPanel(panel);
        }
    }

    /**
     * Sets the form panel for this plugin. Not necessary if the formpanel and
     * the plugin's panel are the same.
     * @param {Ext.form.FormPanel} formpanel the Ext.form.FormPanel
     */
    ,setFormPanel : function(formpanel) {
        if (this.formpanel) {
            // remove listeners
            this.formpanel.stopMonitoring();
            this.formpanel.form.un('actioncomplete', this.onActionComplete, this);
            this.formpanel.un('clientvalidation', this.onClientValidation, this);
        }
        formpanel.form.on('actioncomplete', this.onActionComplete, this);
        formpanel.on('clientvalidation', this.onClientValidation, this);
        formpanel.startMonitoring();
        this.formpanel = formpanel;

        if (this.status) {
            this.formpanel.on('render', function() {
                this.validation.setFormPanel(formpanel);
            }, this, { single: true });
        }

        // update it's submit button to work properly
        var form = this.formpanel.form;
        var onSubmit = this.onSubmit;
        var scope = this.scope || this;
        var pluginOpts = this.options;

        if (this.formpanel.onSubmit) {
            onSubmit = this.formpanel.onSubmit;
            scope = this.formpanel;
        }

        form.submit = function(options) {
            // custom options function
            var config = onSubmit.call(scope, form) || {};

            // by default, include the form values
            if (Ext.isEmpty(config.data)) {
                config.data = form.getValues();
            }

            // options passed to constructor
            Ext.applyIf(config, pluginOpts);

            // options passed to submit method
            if (options) {
                Ext.apply(config, options);
            }

            Ext.form.BasicForm.prototype.submit.call(form, config);
        };
    }

    // private
    ,onClientValidation : function(formpanel, valid) {
        var disabled = !(formpanel.form.isDirty() && valid);
        if (this.saveButton) {
            this.saveButton.setDisabled(disabled);
        }
        else {
            this.dialog.setOkButtonDisabled(disabled);
        }
    }

    // private
    ,onActionComplete : function(form, action) {
        if (action.type === 'submit') {
            if (this.saveButton) {
                if (!this.status.isDestroyed) {
                    this.status.setStatus({
                        clear    : true
                        ,iconCls : 'r-status-check'
                        ,text    : 'Changes saved'
                    });
                }
            }
            else {
                this.dialog.close();
            }
        }
    }

    /**
     * Called when the form is submitted. By default the form is submitted as a
     * POST using this plugin's url property and the form's data
     * (BasicForm.getValues).
     * @param {Ext.form.BasicForm} form The form
     * @return {Object} the config object
     */
    ,onSubmit : function() {
        return {
            url : this.url
        };
    }

    /**
     * Called when the panel's save button or the dialog's ok button is clicked.
     * By default submits the form.
     * @method
     */
    ,onSaveFormButtonClick : function() {
/*         this.formpanel.form.items.each(function(f) {                               */
/*             if (f.isDirty()) {                                                     */
/*                 console.log('dirty', f.fieldLabel, f.originalValue, f.getValue()); */
/*             }                                                                      */
/*         });                                                                        */
/*         return false; */
        this.formpanel.form.submit();
        return false;
    }
};

