/**
 * @class R.form.Controller
 * A controller represents the UI for an attribute class. Do not directly
 * instantiate a controller. Instead use the R.form.FieldFactory.create()
 * function.
 * @property {R.AC} ac The attribute class
 * @property {String} type One of write, read or query.
 */
R.form.Controller = function(config) {
    if (config) {
        Ext.apply(this, config);
    }
};

R.form.Controller.prototype = {
    /**
     * Creates and returns the field used by this controller.
     * @param {Object} config the configuration for the field
     */
    create : function(config) {
        return (this.field = this.onCreate(config));
    }

    /**
     * Template method called to create this controller's field.
     * @param {Object} config the configuration for the field containing the
     * following properties:<ul>
     * <li><b>guid</b>The attribute class GUID</li>
     * <li><b>required</b>True if field is required
     * </ul>
     */
    ,onCreate : function(config) {
        return new Ext.form.DisplayField(config);
    }

    /**
     * @return {Boolean} True if this controller has a value
     */
    ,hasValue : function() {
        return this.field && Ext.isDefined(this.field.getValue());
    }

    /**
     * Converts this controller's field value to JSON format required by an
     * entity.
     * @param {Object} params - the object to add the value to
     */
    ,toJson : function(params) {
        if (!this.field) {
            alert('missing field ' + this.ac.name);
        }
        params[this.ac.guid] = this.ac.fromLocalValue(this.field.getValue());
    }

    /**
     * Sets the value on this controller's fields. The given value is the value
     * as encoded by the entity
     */
    ,setValue : function(value) {
        if (this.field) {
            var result = this.ac.toLocalValue(value);
            // ExtJS workaround for setValue when value has more precision than the field
            // should have.  We need to fixPrecision before setting the value.
            if (this.field.fixPrecision) {
                result = this.field.fixPrecision(result);
            }
            result = this.onSetValue(result);
            if (Ext.isDefined(result)) {
                this.field.setValue(result);
            }
        }
    }

    /**
     * Template method called to return the value used by field.setValue().
     * @param {Mixed} the field's value. If undefined, field.setValue is not
     * called.
     */
    ,onSetValue : function(value) {
        return value;
    }

    /**
     * Returns true if the this controller's field is dirt.y
     */
    ,isDirty : function() {
        return this.field && this.field.isDirty();
    }

    /**
     * Returns true if this controller's value is sent during a form submit and
     * therefore toJson is called on this controller.
     */
    ,isSubmittable : function() {
        return this.type === 'write' && this.field && this.field.isVisible();
    }

    /**
     * Sets the enabled state of this controller's fields.
     */
    ,setDisabled : function(disabled) {
        if (this.field) {
            this.field.setDisabled(disabled);
        }
    }

    /**
     * Attaches an event handler to this controller's field.
     */
    ,on : function() {
        if (this.field) {
            this.field.on.apply(this.field, arguments);
        }
    }

    /**
     * Allows a controller to form dependencies on the other controllers within
     * the current UI. Called after all controllers have been constructed.
     * @param {Ext.util.MixedCollection} collection of controllers
     */
    ,onInitComplete : Ext.emptyFn
};

// private
R.form.Controller.__HIDDEN = {
    onCreate : function(config) {
        return new Ext.form.Hidden(config);
    }
};

/**
 * Returns a controller which displays a hidden field
 * @return {Object} controller
 */
R.form.Controller.createHidden = function() {
    return R.form.Controller.__HIDDEN;
};

/**
 * Converts a controller to one which supports an attribute value type attribute
 * class. For example, a filter attribute/operator/value triplet. The attribute
 * must have a subtype of "attribute_value".
 * @param {Object} toConvert The controller to convert.
 * @return {Object} controller Converted controller
 */
R.form.Controller.toAttributeValue = function(toConvert) {
    return Ext.applyIf({
        onCreate : function(config) {
            var ac = this.ac;
            var me = this;

            if (ac.hasSubType('attribute_value')) {
                // save the config so we can correctly generate the controller
                // based on the selected attribute
                this.initialConfig = config;

                return new Ext.Container(Ext.apply({
                    isFormField : true
                    ,layout  : 'fit'
                    ,items   : this.createValueFieldIfNoAttribute(this)
                    ,isDirty : function() {
                        return me.controller && me.controller.isDirty();
                    }
                    ,resetOriginalValue : function() {
                        if (me.controller) {
                            me.controller.field.resetOriginalValue();
                        }
                    }
                    ,getValue : function() {
                        if (me.controller) {
                            return me.controller.field.getValue();
                        }
                    }
                    ,setValue : function(value) {
                        if (me.controller) {
                            me.controller.setValue(value);
                        }
                        else {
                            me.value = value;
                        }
                    }
                    ,setDisabled : function(v) {
                        Ext.Container.prototype.setDisabled.call(this, v);
                        if (me.controller) {
                            me.controller.setDisabled(v);
                        }
                    }
                }, config));
            }
            return toConvert.onCreate.call(this, config);
        }

        ,setValue : function(value) {
            if (toConvert.setValue) {
                toConvert.setValue.call(this, value);
            }
            else {
                R.form.Controller.prototype.setValue.call(this, value);
            }

            if (this.value_con) {
                _.each(this.value_con, function(controller) {
                    controller.onConditionalAttributeSelect(value);
                });
            }
        }

        ,toJson : function(params) {
            var json, op, type;
            var result;

            if (this.ac.hasSubType('attribute_value')) {
                // this.controller represents the child controller based on the
                // selected attribute (eg. this.controller.ac = $aAssetTemperature
                // and this.ac = $aJMXDomainFilterAttributeValue)
                if (this.controller) {
                    type = this.controller.ac.type;

                    if (type == 'double' || type === 'long') {
                        op = this.operator_con && this.operator_con.field.getValue();
                        if (op === 'gt') {
                            result = this.controller.ac.getFloorValue(this.controller.field.getValue());
                        }
                        else if (op === 'lt') {
                            result = this.controller.ac.getCeilingValue(this.controller.field.getValue());
                        }
                    }

                    if (!Ext.isDefined(result)) {
                        json = {};
                        this.controller.toJson(json);
                        // field.getValue() -> the attribute guid (eg. First Report Attribute)
                        result = json[this.controller.ac.guid];
                    }
                }
                params[this.ac.guid] = result;
            }
            else {
                toConvert.toJson.call(this, params);
            }
        }

        // override
        ,onInitComplete : function(controllers) {
            var constraints = this.ac.constraints;
            var attribute_con, operator_con;

            if (this.ac.hasSubType('attribute_value')) {
                // find the controllers which depend on this value controller
                controllers.each(function(con) {
                    var guid = con.ac.guid;
                    if (guid === constraints.attribute_field) {
                        attribute_con = con;
                    }
                    else if (guid === constraints.operator_field) {
                        operator_con = con;
                    }
                }, this);

                this.operator_con = operator_con;

                if (attribute_con) {
                    this.attribute_con = attribute_con;

                    // attribute_con has to update the attribute field when its
                    // value changes (see setValue). Support multiple value
                    // controllers (eg. Dashboard lower and upper bound attributes)
                    if (!attribute_con.value_con) {
                        attribute_con.value_con = [];
                    }
                    attribute_con.value_con.push(this);

                    if (attribute_con.field) {
                        if (attribute_con.type === 'write') {
                            attribute_con.on('select', function(combo, record) {
                                this.onConditionalAttributeSelect(record && record.data.guid);
                            }, this);
                            attribute_con.on('change', function(combo, newValue) {
                                // occurs when user types in combo and then
                                // leaves the field
                                this.onConditionalAttributeSelect(newValue);
                            }, this);
                        }
                    }
                }

                // eg. Humidity Alert Threshold, attribute is static, but
                // operator and value is writable
                if (operator_con && operator_con.type === 'write' && operator_con.field) {
                    operator_con.field.on('select', function(combo, record) {
                        this.onConditionalOperatorSelect(record && record.data.value);
                    }, this);
                }
            }
        }

        // update the list of available operators based on the selected attribute
        // scope is the 'attribute_value' type controller
        // guid = the selected attribute guid
        ,onConditionalAttributeSelect : function(guid) {
            var ac;

            if (!this.controller || this.controller.ac.guid !== guid) {
                ac = R.AC.get(guid);

                this.updateValueField(ac);

                if (this.operator_con) {
                    this.updateOperatorField(ac);
                    this.onConditionalOperatorSelect(this.operator_con.field.getValue());
                }
            }
        }

        // scope is the 'attribute_value' type controller
        ,onConditionalOperatorSelect : function(id) {
            var op = id && R.AC.get(id);
            this.setDisabled((id === 'def' || id === 'undef'));
        }

        ,createValueFieldIfNoAttribute : function(controller) {
            if (controller.type === 'write') {
                return new Ext.form.TextField({ disabled: true });
            }
            return new Ext.form.DisplayField();
        }

        // update operator field base on selected attribute
        // scope is the 'attribute' type controller
        ,updateOperatorField : function(ac) {
            var field = this.operator_con.field;
            var op, store;

            if (this.operator_con.type === 'write') {
                store = field.store;
                op    = field.getValue();

                store.removeAll();

                if (ac) {
                    store.add(R.Op.findByType(ac.type));

                    if (store.find('value', op) !== -1) {
                        field.setValue(op);
                    }
                    else {
                        field.setValue('');
                    }
                }
                field.setDisabled(!ac);
            }
        }

        // update value field base on selected attribute
        // scope is the 'attribute' type controller
        ,updateValueField : function(ac) {
            var field = this.field;
            var controller;

            field.removeAll(true);
            if (ac) {
                controller = this.factory.create(ac, this.type);
                field.add(controller.create(Ext.apply({}, this.initialConfig)));
            }
            else {
                field.add(this.createValueFieldIfNoAttribute(this));
            }
            field.doLayout();

            this.controller = controller;

            if (Ext.isDefined(this.value)) {
                this.setValue(this.value);
            }
        }
    }, toConvert);
};

/**
 * Creates a controller configuration to represent a writable typeref.
 * @param {R.ET/String} et Root of the entity type tree
 * @param {Array} exclude An array of entity types to exclude
 * @param {String} mode One of 'all', 'leaf', or 'notroot'
 */
R.form.Controller.createTypeRef = function(options) {
    options = options || {};

    return {
        // override
        onCreate : function(config) {
            var constraints = this.ac.constraints;
            var typeref_select = constraints.typeref_select || 'all';
            var et = R.ET.get(options.et) || R.ET.get(constraints.typeref);

            if (options.mode) {
                typeref_select = options.mode;
            }

            // We have to protect against the pathogical case where the admin has
            // given access to an typeref-list attribute but not the root type for
            // that attribute for a non admin user. If this is the case, then
            // simply create the control and set it disabled.
            if (et) {
                if (typeref_select === 'leaf') {
                    return new Ext.form.ComboBox(Ext.apply({
                        clearable      : !config.required
                        ,editable      : false
                        ,displayField  : 'name'
                        ,valueField    : 'guid'
                        ,mode          : 'local'
                        ,triggerAction : 'all'
                        ,store         : new Ext.data.JsonStore({
                            data      : et.leaves()
                            ,fields   : [ 'guid', 'name' ]
                            ,sortInfo : { field: 'name' }
                        })
                    }, config));
                }
                return new R.form.TreeComboBox(Ext.apply({
                    root : AM.tree.treeNodeForET({
                        exclude  : options.exclude
                        ,guid    : et.guid
                        ,usePath : et.isDescendent(R.ET.LOCATION)
                    })
                    ,rootVisible : typeref_select === 'all'
                    ,showDelete  : !config.required
                }, config));
            }
            else {
                return new R.form.TreeComboBox(Ext.apply({
                    disabled    : true
                    ,showDelete : !config.required
                    ,treeParams : {
                        height  : 210
                        ,root   : new Ext.tree.TreeNode({
                            iconCls : 'tree-node-no-icon'
                            ,id     : 'dummy'
                            ,leaf   : true
                            ,text   : '<hidden>'
                        })
                        ,rootVisible : true
                    }
                }, config));
            }
        }
    };
};
