//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2007-2008. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//Class Name:          locationsAndRules-en.js
//
//Written By:          Dan Koester
//------------------------------ tabstop = 4 ----------------------------------

var locationsAndRules = {
    'lrNewLocation'             : 'New Location',
    'lrNewRule'                 : 'New Rule',
    'lrLocationsAndRules'       : 'Locations & Rules',
    'lrSaveLocationOrRule'      : 'Save Location or Rule',
    'lrEditLocation'            : 'Edit Location',
    'lrEditRule'                : 'Edit Rule',
    'lrNewLocation'             : 'New Location',
    'lrNewRule'                 : 'New Rule',
    'lrDeleteLocation'          : 'Delete Location',
    'lrDeleteLocMessage'        : 'Are you sure you want to delete {0}?',
    'lrDeleteRule'              : 'Delete Rule',
    'lrDeleteRuleMessage'       : 'Are you sure you want to delete {0}?',
    'lrNotConnectedRanger'      : 'Zone Manager does not match selected readers.',
    'lrNotConnectedRangerMsg'   : 'All Reader Channels must be connected to the same Zone Manager for each rule.'
};

new RFC_nls().registerBundle('locationsAndRules', locationsAndRules);