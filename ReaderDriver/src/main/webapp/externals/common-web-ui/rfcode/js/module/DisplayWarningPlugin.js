/**
 * @class R.module.DisplayWarningPlugin
 * Plugin used to display a warning banner for a module.
 */
R.module.DisplayWarningPlugin = function(config) {
    /**
     * @cfg {String} message
     * The warning message to display in the banner
     */
    this.message = config.message;
};

R.module.DisplayWarningPlugin.prototype = {
    init : function(module) {
        var onCreateUI = module.onCreateUI;

        module.onCreateUI = this.onCreateUI.createDelegate(this);
        module.setWarningVisible = this.setWarningVisible.createDelegate(this);

        this.module = module;
        this.moduleOnCreateUI = onCreateUI;
    }

    // private
    ,onCreateUI : function() {
        var ui = this.moduleOnCreateUI.call(this.module);
        ui.flex = 1;

        this.warning = new Ext.BoxComponent({
            autoEl : {
                html : '<div class="module-warning-banner"><b>Warning: </b>' + this.message + '</div>'
            }
            ,hidden : true
        });

        return new Ext.Container({
            layout : {
                align : 'stretch'
                ,type : 'vbox'
            }
            ,items : [ this.warning, ui ]
        });
    }

    /**
     * Sets the visibility of the warning banner.
     */
    ,setWarningVisible : function(visible) {
        this.warning.setVisible(visible);
        this.module.component.doLayout();
    }

    // override
    ,onCleanup : function() {
        Ext.destroy(this.warning);
        delete this.warning;
    }
};
