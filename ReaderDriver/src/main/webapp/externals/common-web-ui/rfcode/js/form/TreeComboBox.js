/**
 * @class R.form.TreeComboBox
 * @extends Ext.form.TwinTriggerField
 */
R.form.TreeComboBox = Ext.extend(Ext.form.TwinTriggerField, {

    editable       : true

    /**
     * @cfg {Boolean} resizable True to allow a user to resize the tree
     */
    ,resizable     : true

    ,selectOnFocus : false

    ,trigger1Class : 'x-form-clear-trigger'

    ,trigger2Class : 'x-form-trigger'

    ,triggerClass  : 'x-form-trigger'

    ,initComponent : function() {
        if (this.showUp) {
            this.trigger1Class = 'r-form-up-trigger';
        }
        Ext.form.TwinTriggerField.superclass.initComponent.call(this);

        // create the tree
        this.getTree();

        if (this.showDelete || this.showUp) {
            this.onTrigger2Click = this.onTriggerClick;
            this.triggerConfig = {
                tag  : 'span'
                ,cls : 'x-form-twin-triggers'
                ,cn  : [
                    { tag  : 'img', src : Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' + this.trigger1Class }
                    ,{ tag : 'img', src : Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' + this.trigger2Class }
                ]
            };
        }
        else {
            this.triggerConfig = {
                tag  : 'span'
                ,cls : 'x-form-twin-triggers'
                ,cn  : [
                    { tag: 'img', src : Ext.BLANK_IMAGE_URL, cls: 'x-form-trigger ' + this.trigger2Class }
                ]
            };
            this.onTrigger1Click = this.onTriggerClick;
        }

        if (this.editable) {
            this.enableKeyEvents = true;
            this.on('keyup', this.onOurKeyUp, this, { buffer: 150 });
        }
        this.on('blur', this.collapse, this);
    }

    ,onOurKeyUp : function(field, e) {
        var tree = this.getTree();
        var node = tree.getSelectionModel().getSelectedNode();
        var root, re, find;

        if (e.getKey() == e.ENTER) {
            this.collapse();
            this.fireEvent('select', this, node);
        }
        else {
            root = tree.getRootNode();

            // find any node which contains with the value
            re = new RegExp(Ext.escapeRe(field.getRawValue()), 'i');
            find = function(node) {
                return re.test(node.text);
            };

            if (tree.rootVisible && find(root)) {
                node = root;
            }
            else {
                node = root.findChildBy(find, this, true);
            }
            if (node) {
                node.ensureVisible();
                node.select();
                // ensure we return correct value
                this.value = node.id;
            }
            this.onTriggerClick();
            this.focus();
        }
    }

    // override
    ,onDestroy : function() {
        R.form.TreeComboBox.superclass.onDestroy.call(this);
        Ext.destroy(this.resizer, this.tree);
    }

    // override
    ,onTriggerClick : function() {
        if (this.disabled) {
            return;
        }

        if (this.tree.isVisible()) {
            this.tree.hide();
        }
        else {
            this.getTree().show();
            this.getTree().getEl().alignTo(this.wrap, 'tl-bl?');
        }
    }

    // override
    ,initValue : function() {
        this.setValue(this.value);
        this.originalValue = this.getValue();
    }

    ,onTrigger1Click: function() {
        this.getTree().hide();

        if (this.showDelete) {
            this.setValue(null);
            this.fireEvent('select', this);
        }
        else if (this.showUp) {
            var value = this.getValue();

            if (value) {
                var node = this.tree.getRootNode().findChild('id', value, true);
                if (node && node.parentNode) {
                    this.setValue(node.parentNode.id);
                    this.fireEvent('select', this, node);
                }
            }
        }
    }

    ,getTree: function() {
        if (!this.tree) {
            if (!this.treeWidth) {
                this.treeWidth = Math.max(325, this.width || 325);
            }
            if (!this.treeHeight) {
                this.treeHeight = 250;
            }
            this.tree = new Ext.tree.TreePanel({
                animate      : false
                ,cls         : 'r-treecombobox'
                ,renderTo    : Ext.getBody()
                ,root        : this.root || new Ext.tree.TreeNode({children: this.children})
                ,rootVisible : (typeof this.rootVisible != 'undefined') ? this.rootVisible : (this.root ? true : false)
                ,floating    : true
                ,autoScroll  : true
                ,hidden      : true
                ,minWidth    : 200
                ,minHeight   : 200
                ,width       : this.treeWidth
                ,height      : this.treeHeight
                ,listeners   : {
                    hide   : this.onTreeHide
                    ,show  : this.onTreeShow
                    ,click : this.onTreeNodeClick
                    ,expandnode : this.onTreeNodeExpand
                    ,scope : this
                }
            });

            if (this.sort != false) {
                // call doSort, I (Richie) think there is a bug when using renderTo
                // in conjunction with TreeSorter
                new Ext.tree.TreeSorter(this.tree, {
                    sortType : function(text) {
                        return text.toLowerCase()
                    }
                }).doSort(this.tree.getRootNode());
            }
            this.relayEvents(this.tree.loader, ['beforeload', 'load', 'loadexception']);
            if(this.resizable){
                this.resizer = new Ext.Resizable(this.tree.getEl(),  {
                    handles : 'se'
                    ,pinned : true
                    ,tree   : this.tree
                    ,resizeElement : this.resizerAction
                });
            }
            this.tree.on('beforeshow', function() {
                if (this.rootExpand == null || this.rootExpand) {
                    var root = this.tree.getRootNode();
                    root.expand();
                }
            }, this, { single: true });
        }
        return this.tree;
    }

    ,resizerAction : function() {
        // we do not want Ext.Resizable sizing the tree's el, otherwise the
        // tree uses the new height when calculating its frame height
        // http://www.sencha.com/forum/showthread.php?98976-CLOSED-Ext.Resizable-bug&highlight=ext.resizable
        var box = this.proxy.getBox();
        this.proxy.hide();
        this.tree.setSize(box.width, box.height);
        return box;
    }

    ,onTreeShow: function() {
        Ext.getDoc().on('mousewheel', this.collapseIf, this);
        Ext.getDoc().on('mousedown', this.collapseIf, this);
        if (this.value) {
            var n = this.tree.root.findChild('id', this.value, true);
            if (n) {
                n.ensureVisible();
                n.select();
            }
        }
    }

    ,onTreeHide: function() {
        Ext.getDoc().un('mousewheel', this.collapseIf, this);
        Ext.getDoc().un('mousedown', this.collapseIf, this);
    }

    ,collapseIf : function(e){
        if(!e.within(this.wrap) && !e.within(this.getTree().getEl())){
            this.collapse();
        }
    }

    ,collapse: function() {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        var value = this.value;
        if (node) {
            this.setValue(node.id);
        }

        if (!this.isDestroyed) {
            this.getTree().hide();
        }
    }

    // private
    ,validateBlur : function(){
        return !this.tree || !this.tree.isVisible();
    }

    // override
    ,setValue: function(v) {
        var root = this.tree.getRootNode();
        var node = root.id === v ? root : root.findChild('id', v, true);
        R.form.TreeComboBox.superclass.setValue.call(this, node ? node.text : null);
        // save the node ID, since the superclass sets it to the node's text
        // use null and not undefined for the value. In order to clear an entity
        // value we have to send null to the server.
        this.value = v || null;
    }

    // override
    ,getValue: function() {
        return this.value;
    }

    ,onTreeNodeClick: function(node, e) {
        R.form.TreeComboBox.superclass.setValue.call(this, node.text);
        this.value = node.id;
        this.getTree().hide();
        this.fireEvent('select', this, node);
        this.focus();
    }

    ,onTreeNodeExpand : function(node) {
        if (Ext.isIE) {
            // Fixes rendering of scrollbar when tree expands beyond container
            this.show();
        }
    }
});
Ext.reg('r-treecombo', R.form.TreeComboBox);
