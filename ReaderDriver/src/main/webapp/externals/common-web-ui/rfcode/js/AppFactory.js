/**
 * Factory for creating various components required by the application.
 * @class R.AppFactory
 * @cfg {Boolean} hideModuleHeader If true, the header displaying the current
 * module is not shown. Default is false.
 */
R.AppFactory = function(config) {
    Ext.apply(this, config);
};

R.AppFactory.prototype = {
    /**
     * @return {R.Navigation} navigation Component used for navigating the
     * application
     */
    navigation : Ext.emptyFn

    /**
     * @return {Ext.Component} status The status bar. If null, application has
     * no status bar.
     * @method
     */
    ,status : Ext.emptyFn

    /**
     * Returns an optional array of components to add to the header
     * @return {Array} components An array of components to add to the header
     * to the left of the logo.
     */
    ,header : Ext.emptyFn
};
