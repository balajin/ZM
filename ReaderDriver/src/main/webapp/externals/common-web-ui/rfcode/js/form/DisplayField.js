/**
 * @class R.form.DisplayField
 * @extends Ext.form.DisplayField
 * Same as Ext.form.DisplayField except with the option to have the displayed
 * value differ from the value of the field as returned by getValue().
 * @cfg {Function} toText A function to convert a value to a suitable text representation
 */
 R.form.DisplayField = Ext.extend(Ext.form.DisplayField, {

    /**
     * Converts the given value into a text representation.
     * @return {String} text The text representation of the value
     */
    toText : function(value) {
        return value;
    }

    // override
    ,getValue : function() {
        return this.value;
    }

    // override
    ,setValue : function(value) {
        this.setRawValue(this.toText(value));
        this.value = value;
        return this;
    }
});

