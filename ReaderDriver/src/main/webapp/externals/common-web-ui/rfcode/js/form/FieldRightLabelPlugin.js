/**
 * @class R.form.FieldRightLabelPlugin
 * Plugin which adds a label to the right of a form field layed out in a
 * FormLayout.
 * @cfg {String} label The label to place to the right of the field
 */
R.form.FieldRightLabelPlugin = function(config) {
    Ext.apply(this, config);
};

R.form.FieldRightLabelPlugin.prototype = {
    init : function(field) {
        if (!Ext.isEmpty(this.label)) {
            var onRender = field.onRender;
            var onResize = field.onResize;
            var label    = this.label;

            field.onRender = function(ct, position) {
                onRender.call(this, ct, position);

                if (!this.labelel) {
                    if (!this.wrap) {
                        this.wrap = this.el.wrap({cls: "x-form-field-wrap"});
                    }

                    var padding = field.viewOnly ? 17 : 0;

                    this.labelel = this.wrap.createChild({
                        tag    : 'span'
                        ,html  : label
                        ,style : 'padding-left: 2px; position: absolute; right: ' + padding + 'px; top: 3px;'
                    });

                    // fixes Ext.ux.form.Spinner
                    if (this.splitter) {
                        Ext.destroy(this.splitter);
                    }

                    this.on('destroy', function() {
                        Ext.destroy(this.labelel);
                        this.wrap.remove();
                    });
                }
            };

            field.onResize = function(w, h) {
                onResize.call(this, w, h);

                this.el.setWidth(this.el.getWidth() - this.labelel.getWidth());
                this.wrap.setWidth(w);
            }

            // handle msgTarget icon
            field.alignErrorIcon = function() {
                this.errorIcon.alignTo(this.labelel, 'tl-tr', [2, -3]);
            };
        }
    }
};
