Ext.QuickTips.init();

Ext.ns('R', 'R.data', 'R.form', 'R.grid', 'R.module', 'R.tree', 'R.ui');

Ext.StatusBar = Ext.ux.StatusBar;

/**
 * Returns a proper URL for contacting the device.
 * @method
 */
R.url = function(url) {
    alert('R.url is not implemented: ' + url);
}

R.returnTrue = function() {
    return true;
};

R.offsetFromUserTimeZone = 0;

/**
 * Ensures the constructor function implements all necessary functions to make
 * it a proper Ext.form.Field
 */
R.form.toField = function(cons) {
    var defs = [
        'alignErrorIcon'
        ,'clearInvalid'
        ,'getErrorCt'
        ,'getMessageHandler'
        ,'getName'
        ,'isDirty'
        ,'markInvalid'
        ,'resetOriginalValue'
        ,'setActiveError'
        ,'unsetActiveError'
    ];
    var trues = [ 'isValid', 'validate' ];
    var proto = cons.prototype;

    _.each(defs, function(fn) {
        if (!proto[fn]) {
            proto[fn] = Ext.form.Field.prototype[fn];
        }
    });
    _.each(trues, function(fn) {
        if (!proto[fn]) {
            proto[fn] = R.returnTrue;
        }
    });
    return cons;
};

/**
 * The locale of the browser
 * @type String
 */
R.locale = navigator.language || navigator.userLanguage;
if (R.locale !== null) {
    R.locale = R.locale.toLowerCase();
    R.locale = R.locale.replace(/-/g, '_');
}
else {
    R.locale = 'en_us';
}

/**
 * True if we are in a metric locale.
 * @type {Boolean}
 */
R.metric = (R.locale !== 'en_us');

/**
 * Converts a UTC timestamp into a formatted date string.
 */
R.utcToDateString  = function(value) {
    return new Date(value - R.offsetFromUserTimeZone).format('Y-m-d');
};

/**
 * Converts a UTC timestamp into a formatted timestamp string.
 */
R.utcToTimestampString = function(value) {
    return new Date(value - R.offsetFromUserTimeZone).format('Y-m-d H:i:s');
};

(function() {
    var buildURL = function(url, params) {
        var i, k, v, ek;
        var count  = 0;

        // figure out url
        if (params) {
            for (k in params) {
                v = params[k];

                if (!Ext.isEmpty(v)) {
                    if (count === 0) {
                        url += '?';
                    }

                    ek = encodeURIComponent(k);

                    if (Ext.isArray(v)) {
                        for (i = 0; i < v.length; i++) {
                            if (count > 0) {
                                url += '&';
                            }
                            url += ek + '=' + encodeURIComponent(v[i]);
                            count++;
                        }
                    }
                    else {
                        if (count > 0) {
                            url += '&';
                        }
                        url += ek + '=' + encodeURIComponent(params[k]);
                        count++;
                    }
                }
            }
        }

        // special case for AssetManager
        if (url.indexOf('api/configexport') !== -1) {
            url += (count === 0 ? '?' : '&') + 'dateformat=iso8601';
        }
        return url;
    };

    /**
     * Downloads a file from the server.
     * @param {Object} options An object which may contain the following properties:<ul>
     * <li><b>url</b> : String (required) <div class="sub-desc">The URL
     * containing the file to download.</div></li>
     * <li><b>params</b> : Object (optional) <div class="sub-desc">An object
     * containing URL parameters.</div></li>
     * </ul>
     */
    R.download = function(options) {
        var iframe = Ext.get('download-iframe');
        var url    = buildURL(options.url, options.params);

        if (iframe) {
            iframe.dom.src = url;
        }
        else {
            iframe = Ext.DomHelper.append(document.body, {
                tag          : 'iframe'
                ,id          : 'download-iframe'
                ,frameBorder : 0
                ,width       : 0
                ,height      : 0
                ,css         : 'display:none;visibility:hidden;height:0px'
                ,src         : url
            });
        }
    };
})();

/**
 * Returns true if the given object contains the specified property.
 * @param {Object} obj Object to test
 * @param {Object} key (optional) If not passed then this function returns true
 * if the object contains at least one property.
 */
R.hasProperty = function(obj, key) {
    if (key) {
        return obj.hasOwnProperty(key);
    }
    else {
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                return true;
            }
        }
    }
    return false;
};

/**
 * Returns a simple map in which the key is the item in the array and the value
 * is true.
 * @return {Object} map containing key/true pairs
 */
R.toMap = function(arr) {
    var len = arr.length;
    var result = {};
    for (var i = 0; i < len; i++) {
        result[arr[i]] = true;
    }
    return result;
};

/**
 * Creates a new array with the results of calling the function on every element
 * in the array.
 */
R.map = function(arr, fun, scope) {
    var len = arr.length;
    var result = new Array(len);
    for (var i = 0; i < len; i++) {
        result[i] = fun.call(scope, arr[i], i, arr);
    }
    return result;
};

/**
 * Returns the first element in the given array in which the given function
 * returns true. Returns null if no element passes the test.
 * @param {Array} arr The array
 * @param {Function} fun The function
 * @param {Object} scope (optional) The scope
 * @method
 */
R.find = function(arr, fun, scope) {
    if (!arr) {
        console.trace();
    }
    var len = arr.length;
    for (var i = 0; i < len; i++) {
        if (fun.call(scope, arr[i]) === true) {
            return arr[i];
        }
    }
    return null;
};

R.each = function(arr, fun, scope) {
    if (!arr) {
        console.trace();
    }
    var len = arr.length;
    if (typeof fun != "function")
      throw new TypeError();

    for (var i = 0; i < len; i++) {
      if (i in arr)
        fun.call(scope, arr[i], i, arr);
    }
};

R.i18n = new RFC_nls();

R.locale = navigator.language || navigator.userLanguage;
if (R.locale !== null) {
    R.locale = R.locale.toLowerCase();
    R.locale = R.locale.replace(/-/g, '_');
}
else {
    R.locale = 'en_us';
}

R.metric = (R.locale !== 'en_us');

// create function for translating strings
function tr(key) {
    return R.i18n.getString.apply(R.i18n, arguments);
};

/**
 * Returns a comparison function for use by the sort function. If key is passed
 * then the comparison function compares the arguments by comparing the
 * property of the object given by key. If key is not passed, the comparison
 * is done by using '<' and '>' on the arguments.
 * @param {String/Function} key (optional) The property in the object to use for
 * the comparison. If key is a function, then the function is called on the object
 * to obtain the values to compare.
 * @param {Mixed} def (optional) The default value to use if object does not contain a key property
 */
(function() {
    var compare = function(a, b) {
        if (typeof a === 'string') {
            a = a.toLowerCase();
        }
        if (typeof b === 'string') {
            b = b.toLowerCase();
        }
        if (a > b) {
            return 1;
        }
        if (a < b) {
            return -1;
        }
        return 0;
    };
    R.compare = function(key, def) {
        if (typeof key === 'function') {
            return function(a, b) {
                var v1 = key.call(a, a), v2 = key.call(b, b);
                return compare(Ext.isEmpty(v1) ? def : v1, Ext.isEmpty(v2) ? def : v2);
            };
        }
        else if (key) {
            return function(a, b) {
                var v1 = a[key], v2 = b[key];
                return compare(Ext.isEmpty(v1) ? def : v1, Ext.isEmpty(v2) ? def : v2);
            };
        }
        return compare;
    };
})();

/**
 * Iterates an array calling the passed function with each item, stopping if
 * the function returns false.
 * @param {Function} fn The function to call
 * @param {Object} (Optional) scope The scope to call the function
 * @method
 */
if (Array.prototype.each) {
    alert('Warning!! Array.prototype.each already defined');
}
Array.prototype.each = function(fn, scope) {
    var len = this.length >>> 0;
    if (typeof fn != "function")
        throw new TypeError();

    for (var i = 0; i < len; i++) {
        if (i in this)
            fn.call(scope || this[i], this[i], i, this);
    }
};

/**
 * Array methods implemented in newer version of javascript.
 * https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/filter
 */

// https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array
if (!Array.prototype.every)
{
  Array.prototype.every = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this &&
          !fun.call(thisp, this[i], i, this))
        return false;
    }

    return true;
  };
}

// https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/filter
if (!Array.prototype.filter)
{
  Array.prototype.filter = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array();
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
      {
        var val = this[i]; // in case fun mutates this
        if (fun.call(thisp, val, i, this))
          res.push(val);
      }
    }

    return res;
  };
}

// https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/map
if (!Array.prototype.map)
{
  Array.prototype.map = function(fun /*, thisp*/)
  {
    var len = this.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var res = new Array(len);
    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in this)
        res[i] = fun.call(thisp, this[i], i, this);
    }

    return res;
  };
}

// https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/some
if (!Array.prototype.some)
{
  Array.prototype.some = function(fun /*, thisp*/)
  {
    var i = 0,
        len = this.length >>> 0;

    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (; i < len; i++)
    {
      if (i in this &&
          fun.call(thisp, this[i], i, this))
        return true;
    }

    return false;
  };
}

function sprintf( ) {
    var regex = /%%|%(\d+\$)?([-+#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuidfegEG])/g;
    var a = arguments, i = 0, format = a[i++];

    // pad()
    var pad = function(str, len, chr, leftJustify) {
        var padding = (str.length >= len) ? '' : Array(1 + len - str.length >>> 0).join(chr);
        return leftJustify ? str + padding : padding + str;
    };

    // justify()
    var justify = function(value, prefix, leftJustify, minWidth, zeroPad) {
        var diff = minWidth - value.length;
        if (diff > 0) {
            if (leftJustify || !zeroPad) {
            value = pad(value, minWidth, ' ', leftJustify);
            } else {
            value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
            }
        }
        return value;
    };

    // formatBaseX()
    var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
        // Note: casts negative numbers to positive ones
        var number = value >>> 0;
        prefix = prefix && number && {'2': '0b', '8': '0', '16': '0x'}[base] || '';
        value = prefix + pad(number.toString(base), precision || 0, '0', false);
        return justify(value, prefix, leftJustify, minWidth, zeroPad);
    };

    // formatString()
    var formatString = function(value, leftJustify, minWidth, precision, zeroPad) {
        if (precision != null) {
            value = value.slice(0, precision);
        }
        return justify(value, '', leftJustify, minWidth, zeroPad);
    };

    // finalFormat()
    var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
        if (substring == '%%') return '%';

        // parse flags
        var leftJustify = false, positivePrefix = '', zeroPad = false, prefixBaseX = false;
        for (var j = 0; flags && j < flags.length; j++) switch (flags.charAt(j)) {
            case ' ': positivePrefix = ' '; break;
            case '+': positivePrefix = '+'; break;
            case '-': leftJustify = true; break;
            case '0': zeroPad = true; break;
            case '#': prefixBaseX = true; break;
        }

        // parameters may be null, undefined, empty-string or real valued
        // we want to ignore null, undefined and empty-string values
        if (!minWidth) {
            minWidth = 0;
        }
        else if (minWidth == '*') {
            minWidth = +a[i++];
        }
        else if (minWidth.charAt(0) == '*') {
            minWidth = +a[minWidth.slice(1, -1)];
        }
        else {
            minWidth = +minWidth;
        }

        // Note: undocumented perl feature:
        if (minWidth < 0) {
            minWidth = -minWidth;
            leftJustify = true;
        }

        if (!isFinite(minWidth)) {
            throw new Error('sprintf: (minimum-)width must be finite');
        }

        if (!precision) {
            precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type == 'd') ? 0 : void(0);
        }
        else if (precision == '*') {
            precision = +a[i++];
        }
        else if (precision.charAt(0) == '*') {
            precision = +a[precision.slice(1, -1)];
        }
        else {
            precision = +precision;
        }

        // grab value using valueIndex if required?
        var value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

        switch (type) {
            case 's': return formatString(String(value), leftJustify, minWidth, precision, zeroPad);
            case 'c': return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
            case 'b': return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'o': return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'x': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'X': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase();
            case 'u': return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
            case 'i':
            case 'd': {
                        var number = parseInt(+value);
                        var prefix = number < 0 ? '-' : positivePrefix;
                        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
                        return justify(value, prefix, leftJustify, minWidth, zeroPad);
                    }
            case 'e':
            case 'E':
            case 'f':
            case 'F':
            case 'g':
            case 'G':
                        {
                        var number = +value;
                        var prefix = number < 0 ? '-' : positivePrefix;
                        var method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
                        var textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
                        value = prefix + Math.abs(number)[method](precision);
                        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
                    }
            default: return substring;
        }
    };
    return format.replace(regex, doFormat);
}

