/**
 * @class R.ui.Action
 * @extends Ext.Action
 */
R.ui.Action = Ext.extend(Ext.Action, {
    actionProps: [
        'handler',
        'scope',
        'tbar',
        'bbar',
        'button',
        'cmenu',
        'key',
        'def',
        'menubarPath',
        'sm', // selection model
        'isAllowed'
    ]

    ,constructor: function(config){
        config = config || {};

        var props = this.actionProps;
        for(var i = 0, len = props.length; i < len; i++){
            var prop = props[i];
            if (config[prop]) {
                this[prop] = config[prop];
            }
            delete config[prop];
        }
        if (config.update) {
            this.update = config.update;
        }

        R.ui.Action.superclass.constructor.call(this, Ext.apply({
            scope     : this
            ,handler  : this.execute
            ,minWidth : 75
        }, config));
    }

    ,execute: function() {
        this.update();

        if (!this.isDisabled()) {
            this.handler.apply(this.scope || this, arguments);
        }
    }

    ,update: Ext.emptyFn

    /**
     * Called before this action's context menu is shown.
     */
    ,beforeMenuShow : Ext.emptyFn

    /**
     * @return {Boolean} false to prevent this action from showing up
     */
    ,isAllowed : function() {
        return true;
    }

    /**
     * Returns a function which only calls the given function if this action's
     * component is still valid. Useful for asynchronous callbacks.
     * @param {Function} fn The function to wrap
     */
    ,callback : function(fn) {
        var valid = function() {
            return !this.manager.component.isDestroyed;
        };
        return fn.createInterceptor(valid, this);
    }
});

