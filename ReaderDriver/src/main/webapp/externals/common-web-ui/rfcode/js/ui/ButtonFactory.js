/*
 * Defines functions which create button configuration objects. Useful for
 * creating the buttons array in a Ext.Panel
 */
(function() {
    // see templates/css/button.css and templates/icons/button

    var btns = [
         [ 'copy', 'CopyButton', 'rfcopybutton' ]
        ,[ 'delete', 'DeleteButton', 'rfdeletebutton' ]
        ,[ 'edit', 'EditButton', 'rfeditbutton' ]
        ,[ 'export', 'ExportButton', 'rfexportbutton' ]
        ,[ 'new', 'NewButton', 'rfnewbutton' ]
        ,[ 'play', 'PlayButton', 'rfplaybutton' ]
        ,[ 'pause', 'PauseButton', 'rfpausebutton' ]
        ,[ 'save', 'SaveButton', 'rfsavebutton' ]
        ,[ 'test', 'TestButton', 'rftestbutton' ]
        ,[ 'upload', 'UploadButton', 'rfuploadbutton' ]
        ,[ 'view', 'ViewButton', 'rfviewbutton' ]
        ,[ 'group', 'GroupButton', 'rfgroupbutton' ]
    ];

    R.each(btns, function(btn) {
        var name  = 'button-' + btn[0];
        var func  = btn[1];
        var xtype = btn[2];
        var clazz = Ext.extend(Ext.Button, {
            cls   : name + ' x-btn-text-icon'
            ,text : tr(name)
        });

        R.ui[func] = clazz;
        Ext.reg(xtype, clazz);
    });
})();

// comments below are for generating documentation with ext-doc

/**
 * @class R.ui.CopyButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.DeleteButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.EditButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.ExportButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.NewButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.PlayButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.PauseButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.SaveButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.TestButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.UploadButton
 * @extends Ext.Button
 */

/**
 * @class R.ui.ViewButton
 * @extends Ext.Button
 */



