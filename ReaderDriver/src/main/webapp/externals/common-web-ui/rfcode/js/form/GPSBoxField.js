/**
 * @class R.form.GPSBoxField
 * @extends Ext.form.Field
 * A field which allows a user to specify a GPS bounding box.
 */
R.form.GPSBoxField = Ext.extend(Ext.form.Field, {

    defaultAutoCreate : { tag: 'div' }

    ,focusClass : undefined

    ,constructor : function(config) {

        this.topLeft = new Ext.form.TextField(Ext.applyIf({
            emptyText      : 'Top left coordinate'
            ,isDirty       : Ext.emptyFn
            ,isFormField   : false
            ,selectOnFocus : true
            ,getErrorCt    : this.getErrorCt
            ,markInvalid   : null // so BasicForm does not add it to its field collection
            ,height        : config.textHeight
        }, config));

        this.bottomRight = new Ext.form.TextField(Ext.applyIf({
            emptyText      : 'Bottom right coordinate'
            ,isDirty       : Ext.emptyFn
            ,isFormField   : false
            ,selectOnFocus : true
            ,getErrorCt    : this.getErrorCt
            ,markInvalid   : null // so BasicForm does not add it to its field collection
            ,height        : config.textHeight
        }, config));

        R.form.GPSBoxField.superclass.constructor.call(this, config);
    }

    // private
    ,onRender : function(ct, position) {
        R.form.GPSBoxField.superclass.onRender.call(this, ct, position);

        var ui = new Ext.Container({
            layout        : 'table'
            ,renderTo     : this.el
            ,layoutConfig : { columns : (this.rows == 2 ? 1 : 3) }
            ,items        : [ 
                this.topLeft
                ,new Ext.form.Label({
                    cls    : 'x-form-item'
                    ,html  : ''
                    ,width : 5
                })
                ,this.bottomRight 
            ]
        });
    }

    ,setDisabled : function(v) {
    }

    ,initValue : function() {
        if (this.value !== undefined) {
            this.setValue(this.value);
        }
        this.originalValue = this.getValue();
    }

    /**
     * @return null if the first choice is selected
     */
    ,getValue : function() {
        var value = null;

        var tlCoords = R.AC.prototype.parseCoordinateString(this.topLeft.getValue());
        var brCoords = R.AC.prototype.parseCoordinateString(this.bottomRight.getValue());

        if (Ext.isArray(tlCoords) && Ext.isArray(brCoords)) {
            value = tlCoords[0] + ',' + tlCoords[1] + ',' + brCoords[0] + ',' + brCoords[1];
        }
        return value;
    }

    ,setValue : function(value) {
        if (value) {
            var coords = value.split(',');
            if (Ext.isArray(coords) && coords.length == 4) {
                this.topLeft.setValue(R.AC.prototype.latLonValueToString(coords[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(coords[1], "E", "W"));
                this.bottomRight.setValue(R.AC.prototype.latLonValueToString(coords[2], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(coords[3], "E", "W"));
            }
        }
    }
});
