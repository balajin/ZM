/**
 * Utility class which uses Ext.MessageBox to generate message boxes.
 * @class R.ui.Msg
 * @singleton
 */
R.ui.Msg = function() {
    return {
        /**
         * Displays a message box with "Save", "Don't Save", and "Cancel"
         * buttons. The options may define callback functions named 'save',
         * 'dontSave' and 'cancel' for the corresponding button callbacks. You
         * may also define one callback function ('handler' option) for all
         * buttons.
         */
        save : function(options) {
            Ext.Msg.show({
                title    : 'Save Changes'
                ,msg     : 'You have unsaved changes.<br>Would you like to save your changes?'
                ,icon    : Ext.Msg.QUESTION
                ,buttons : {
                    yes     : 'Save'
                    ,no     : 'Don\'t Save'
                    ,cancel : Ext.Msg.CANCEL
                }
                ,scope   : R.ui.Msg
                ,fn      : function(btn) {
                    // call the button handler
                    var handler = options.cancel;
                    var arg     = 'cancel';

                    if (btn == 'yes') {
                        handler = options.save;
                        arg     = 'save'
                    }
                    else if (btn == 'no') {
                        handler = options.dontSave;
                        arg     = 'dontSave'
                    }

                    if (typeof handler == 'function') {
                        handler.call(options.scope || this);
                    }

                    if (options.handler) {
                        options.handler.call(options.scope || this, arg);
                    }
                }
            });
        }

        /**
         * Displays a message box with "Delete" and "Don't Delete" buttons.
         * The options may define callback functions named 'remove' and
         * 'cancel' for the corresponding button callbacks.
         */
        ,remove : function(options) {
            Ext.Msg.show({
                title    : options.title ? options.title : 'Delete'
                ,msg     : options.msg ? options.msg : 'Do you really want to delete the selected object?'
                ,icon    : Ext.Msg.QUESTION
                ,buttons : {
                    ok      : options.ok ? options.ok : 'Delete'
                    ,cancel : options.cancel ? options.cancel : 'Don\'t Delete'
                }
                ,scope   : R.ui.Msg
                ,fn      : function(btn) {
                    // call the button handler
                    var handler = options.cancel;
                    if (btn == 'ok') {
                        handler = options.remove;
                    }

                    if (typeof handler == 'function') {
                        handler.call(options.scope || this);
                    }
                }
            });
        }
    }
}();

