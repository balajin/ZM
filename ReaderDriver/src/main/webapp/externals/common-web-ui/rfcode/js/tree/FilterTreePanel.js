/**
 * TreePanel which displays a textfield to filter the tree nodes displayed by
 * the panel.
 * @class R.tree.FilterTreePanel
 * @extends Ext.tree.TreePanel
 * @cfg {Boolean} sort If true, use a TreeSorter to sort the nodes based on
 * their text attribute
 * @cfg {Boolean} hideExpandCollapse If true, do not show the expand and
 * collapse buttons
 */
R.tree.FilterTreePanel = Ext.extend(Ext.tree.TreePanel, {

    // override ExtJS default
    animate : false

    // override ExtJS default
    ,autoScroll : true

    // override ExtJS default
    ,frame : true

    ,constructor : function(config) {
        config = Ext.apply({
            tbar : [
                'Filter'
                ,{
                    enableKeyEvents : true
                    ,filter         : new Ext.ux.tree.TreeFilterX(this, {
                        autoClear : true
                    })
                    ,triggerClass : 'x-form-clear-trigger'
                    ,width        : '100%'
                    ,xtype        : 'trigger'
                    ,listeners    : {
                        'keyup' : {
                            buffer : 150
                            ,scope : this
                            ,fn    : function(field, e) {
                                if (Ext.EventObject.ESC == e.getKey()) {
                                    field.onTriggerClick();
                                }
                                else {
                                    var search = Ext.escapeRe(field.getRawValue());
                                    var node, nodes, selmodel;

                                    field.filter.filter(new RegExp(search, 'i'));

                                    // remove hidden nodes from selection
                                    selmodel = this.getSelectionModel();
                                    if (typeof selmodel.getSelectedNodes === 'function') {
                                        nodes = selmodel.getSelectedNodes();
                                        for (var i = 0; i < nodes.length; i++) {
                                            node = nodes[i];

                                            if (node.hidden) {
                                                selmodel.unselect(node);
                                            }
                                        }
                                    }
                                    else {
                                        node = selmodel.getSelectedNode();
                                        if (node && node.hidden) {
                                            selmodel.clearSelections();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ,onTriggerClick : function() {
                        this.setValue('');
                        this.filter.clear();
                        this.focus();
                    }
                }
            ]
        }, config);

        // override ExtJS default
        Ext.applyIf(config, {
            header : true
        });

        R.tree.FilterTreePanel.superclass.constructor.call(this, config);

        if (config.sort) {
            new Ext.tree.TreeSorter(this, {
                folderSort : true
                ,sortType  : function(text) {
                    return text.toLowerCase()
                }
            });
        }

        if (config.treeToggle) {
            this.addEvents(
                // raw events
                /**
                 *
                 */
                'treeToggle'
            );
        }
    }

    // override
    ,onRender : function() {
        R.tree.FilterTreePanel.superclass.onRender.apply(this, arguments);

        // add another toolbar below the search field
        var items = [];
        if (this.hideExpandCollapse !== true) {
            items.push({
                cls      : 'button-expand-all x-btn-icon'
                ,tooltip : 'Expand All'
                ,xtype   : 'button'
                ,handler : function() { this.root.expand(true); }
                ,scope   : this
            });
            items.push(' ');
            items.push({
                cls      : 'button-collapse-all x-btn-icon'
                ,tooltip : 'Collapse All'
                ,xtype   : 'button'
                ,handler : function() { this.root.collapse(true); }
                ,scope   : this
            });
        }

        if (this.treeToggle) {
            if (items.length > 0) {
                items.push('-');
            }
            items.push({
                tooltip       : 'Toggle between logical and physical'
                ,xtype        : 'checkbox'
                ,boxLabel     : 'Logical View'
                ,handler      : function(checkbox, checked) { this.fireEvent('treeToggle', this, checked); }
                ,scope        : this
                ,autoWidth    : true
            });
        }
        if (items.length > 0) {
            new Ext.Toolbar({
                renderTo : this.tbar
                ,items   : items
            });
        }
    }

    /**
     * Removes all the nodes from the root node of this tree.
     * @method
     */
    ,removeAllNodes: function() {
        var rootNode = this.getRootNode();

        // Sometimes when clicking quickly between tree nodes in the AM navigation
        // trees will begin to get created then get removed from the DOM before
        // The root node becomes available
        if (rootNode) {
            rootNode.removeChildren(true);
        }
    }
});
Ext.reg('r-filtertreepanel', R.tree.FilterTreePanel);
