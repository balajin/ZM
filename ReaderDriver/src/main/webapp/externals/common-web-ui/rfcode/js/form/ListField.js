/**
 * A field which allows a user to select a list of items (eg. string-list type
 * attribute).
 * @class R.form.ListField
 * @extends Ext.form.TriggerField
 * @cfg {String} displayField Name of the field to use for displaying an item in the list
 * @cfg {String} valueField Name of the field to use getting the value of an item
 * in the list
 * @see R.ui.DualListPanel
 */
R.form.ListField = Ext.extend(Ext.form.TriggerField, {
    // override
    defaultAutoCreate : { tag: 'textarea', rows: 4 }

    // override
    ,editable : false

    // override
    ,triggerClass : 'x-form-win-trigger'

    // override
    ,initComponent : function() {
        R.form.ListField.superclass.initComponent.call(this);

        // auto-configure store if necessary
        if (Ext.isArray(this.store)) {
            if (Ext.isArray(this.store[0])){
                this.store = new Ext.data.ArrayStore({
                    fields : ['value', 'text']
                    ,data  : this.store
                });
                this.valueField = 'value';
            }
            else {
                this.store = new Ext.data.ArrayStore({
                    fields      : ['text']
                    ,data       : this.store
                    ,expandData : true
                });
                this.valueField = 'text';
            }
            this.displayField = 'text';
        }

        this.addEvents(
            /**
             * @event datachanged
             * Fires when the values in the list have changed.
             * @param {R.ui.ListField} this
             */
            'datachanged'
        );
    }

    // override
    ,getValue : function() {
        return this.value || [];
    }

    // override
    ,setValue : function(v) {
        this.value = v;
        if (this.rendered) {
            var labels = [];
            var index;
            if (Ext.isArray(v)) {
                for (var i = 0; i < v.length; i++) {
                    index = this.store.findExact(this.valueField, v[i]);
                    if (index >= 0) {
                        labels.push(this.store.getAt(index).get(this.displayField));
                    }
                    else if (this.addIfAbsent) {
                        var records = [ new Ext.data.Record({ text : v[i] }) ];
                        labels.push(v);
                        this.store.add(records);
                    }
                }
            }
            if (labels.length > 0) {
                this.setRawValue(labels.join('\n'));
            }
            else {
                this.setRawValue(null);
            }
            this.applyEmptyText();
        }
        this.fireEvent('datachanged', this);
    }

    // private
    ,onTriggerClick : function() {
        if (this.disabled) {
            return;
        }
        var field = new R.form.DualListField({
            displayField : this.displayField
            ,reordering  : this.reordering
            ,store       : this.store
            ,value       : this.value
            ,valueField  : this.valueField
        });
        var win = new R.ui.Dialog({
            title      : this.fieldLabel
            ,width     : 530
            ,height    : 298
            ,minWidth  : 300
            ,minHeight : 200
            ,layout    : 'fit'
            ,items     : field
        });
        win.on('ok', function() {
            var val = field.getValue();
            if (!(Ext.isArray(val) && val.length > 0)) {
                val = null;
            }
            this.setValue(val);
            this.fireEvent('datachanged', this);
        }, this);
        win.show();
    }
});
