/**
 * @class R.grid
 * @singleton
 * Initializes the R.grid namespace with various utility methods.
 */
(function() {
    var move = function(grid, up) {
        var store    = grid.store;
        var copy     = store.getRange();
        var sm       = grid.getSelectionModel();
        var selected = [];
        var record;

        if (up) {
            for (var i = 1; i < copy.length; i++) {
                record = copy[i];

                if (sm.isSelected(i)) {
                    var tmp = copy[i-1];
                    copy[i-1] = record;
                    copy[i] = tmp;
                    selected.push(record);
                }
            }
        }
        else {
            for (var i = copy.length-2; i >= 0; i--) {
                record = copy[i];

                if (sm.isSelected(i)) {
                    var tmp = copy[i+1];
                    copy[i+1] = record;
                    copy[i] = tmp;
                    selected.push(record);
                }
            }
        }
        store.removeAll();
        store.add(copy);
        sm.selectRecords(selected);
    };

    Ext.apply(R.grid, {

        /**
         * Moves the selected rows of the given grid down by one index.
         * @param {Object} grid The grid to modify
         */
        down : function(grid) {
            move(grid, false);
        }

        /**
         * Moves the selected rows of the given grid up by one index.
         * @param {Object} grid The grid to modify
         */
        ,up : function(grid) {
            move(grid, true);
        }
    });
})();
