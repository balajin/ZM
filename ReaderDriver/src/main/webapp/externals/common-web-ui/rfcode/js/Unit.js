/**
 * @class R.Unit
 * Represents different units of measurement.
 * @cfg {String} id
 * @cfg {Boolean} metric True if units is metric
 * @cfg {String} l The long label
 * @cfg {String} sl The short label
 * @cfg {String} alt ID of the alternative units to convert to/from metric
 * @cfg {Number} scale The scale factor used to convert to alternative units (value * scale + add)
 * @cfg {Number} add The additive factor used to convert to alternative units (value * scale + add)
 */
(function() {
    var list = {};

    R.Unit = function(config) {
        config.metric = config.metric || false;
        config.server = config.server || false;
        config.sl = config.sl || config.l;

        Ext.apply(this, config);

        list[this.id] = this;
    };

    /**
     * Returns the R.Unit object with the given ID
     * @method
    */
    R.Unit.find = function(id) {
        return list[id];
    };

    /**
     * Returns a store containing all the unit data.
     * @return {Ext.data.Store} The store
     */
    R.Unit.store = function() {
        var data = [];
        for (var id in list) {
            data.push([id, list[id].l]);
        }
        return new Ext.data.ArrayStore({
            autoDestroy : true
            ,fields   : ['id', 'label']
            ,data     : data
            ,sortInfo : {
                field : 'label'
            }
        });
    };
})();

R.Unit.prototype = {
    /**
     * Converts this units to the local units (eg. celsius -> fahrenheit)
     * @param {Number} val The number to convert
     */
    tolocal : function(val) {
        if (R.metric === this.metric || !this.alt) {
            return val;
        }
        return val * this.scale + this.add;
    }

    /**
     * Converts a local value into a value of this units.
     * @param {Number} val The number to convert
     */
    ,fromlocal : function(val) {
        var u;
        if (R.metric === this.metric || !this.alt) {
            return val;
        }
        u = R.Unit.find(this.alt);
        return val * u.scale + u.add;
    }

    /**
     * Checks to see if we're in the base locale already.
     * @return {Boolean} true if this units is the in the preferred locale
     */
    ,isBaseLocale : function() {
        return (R.metric === this.metric || !this.alt);
    }

    /**
     * Returns the display label for this unit.
     */
    ,label : function() {
        return this._get().l;
    }

    /**
     * Returns the short label for this unit.
     */
    ,shortlabel : function() {
        return this._get().sl;
    }

    /**
     * Converts the value to the local format and adds the short label.
     * @param {Number} val number to convert
     * @param {Number} digits optional number of digits after the decimal point
     * of the converted value to display
     * @return {String} converted value plus units short label
     */
    ,toShortLocal : function(val, digits) {
        var s = this.tolocal(val);
        if (digits >= 0) {
            s = s.toFixed(digits);
        }
        return s + this.shortlabel();
    }

    ,_get : function() {
        var u = this;
        if (R.metric !== this.metric && u.alt) {
            u = R.Unit.find(u.alt);
        }
        return u;
    }
};

new R.Unit({
    id      : 'amps'
    ,metric : true
    ,l      : 'Amps'
    ,sl     : ' A'
});

new R.Unit({
    id      : 'celsius'
    ,metric : true
    ,l      : 'Celsius'
    ,sl     : '\u00B0 C'
    ,alt    : 'fahrenheit'
    ,scale  : 1.8
    ,add    : 32
});

new R.Unit({
    id      : 'celsiusdiff'
    ,metric : true
    ,l      : 'Celsius (delta)'
    ,sl     : '\u00B0 C'
    ,alt    : 'fahrenheitdiff'
    ,scale  : 1.8
    ,add    : 0
});

new R.Unit({
    id      : 'dbm'
    ,metric : true
    ,l      : ' dBm'
});

new R.Unit({
    id      : 'db'
    ,metric : true
    ,l      : ' dB'
});

new R.Unit({
    id      : 'degrees'
    ,metric : true
    ,l      : '\u00B0'
});

new R.Unit({
    id      : 'fahrenheit'
    ,l      : 'Fahrenheit'
    ,sl     : '\u00B0 F'
    ,alt    : 'celsius'
    ,scale  : 0.5556
    ,add    : -17.7778
});

new R.Unit({
    id      : 'fahrenheitdiff'
    ,l      : 'Fahrenheit (delta)'
    ,sl     : '\u00B0 F'
    ,alt    : 'celsiusdiff'
    ,scale  : 0.5556
    ,add    : 0
});

new R.Unit({
    id      : 'feet'
    ,metric : false
    ,l      : 'feet'
    ,sl     : ' ft'
    ,alt    : 'meters'
    ,scale  : 0.3048
    ,add    : 0
});

new R.Unit({
    id      : 'inches'
    ,metric : false
    ,l      : 'inches'
    ,sl     : ' in'
    ,alt    : 'centimeters'
    ,scale  : 2.54
    ,add    : 0
});

new R.Unit({
    id      : 'feetpersec'
    ,metric : false
    ,l      : 'feet/second'
    ,sl     : ' ft/s'
    ,alt    : 'meterspersec'
    ,scale  : 0.3048
    ,add    : 0
});

new R.Unit({
    id      : 'hours'
    ,metric : true
    ,l      : 'hours'
    ,sl     : ' hours'
});

new R.Unit({
    id      : 'inH2O'
    ,l      : 'Inches H2O'
    ,sl     : '" H2O'
    ,alt    : 'pa'
    ,scale  : 249.074
    ,add    : 0
});

new R.Unit({
    id      : 'kilograms'
    ,metric : true
    ,l      : 'Kilograms'
    ,sl     : 'kg'
    ,alt    : 'pounds'
    ,scale  : 2.20462262
    ,add    : 0
});

new R.Unit({
    id      : 'kPa'
    ,metric : true
    ,l      : 'kPa'
    ,sl     : ' kPa'
    ,alt    : 'psi'
    ,scale  : .14503773773
    ,add    : 0
});

new R.Unit({
    id      : 'meters'
    ,metric : true
    ,l      : 'meters'
    ,sl     : ' m'
    ,alt    : 'feet'
    ,scale  : 3.2808399
    ,add    : 0
});

new R.Unit({
    id      : 'centimeters'
    ,metric : true
    ,l      : 'centimeters'
    ,sl     : ' cm'
    ,alt    : 'inches'
    ,scale  : 0.393700787
    ,add    : 0
});

new R.Unit({
    id      : 'meterspersec'
    ,metric : true
    ,l      : 'meters/second'
    ,sl     : ' m/s'
    ,alt    : 'feetpersec'
    ,scale  : 3.2808399
    ,add    : 0
});

new R.Unit({
    id      : 'minutes'
    ,metric : true
    ,l      : 'minutes'
    ,sl     : ' min.'
});

new R.Unit({
    id      : 'pa'
    ,metric : true
    ,l      : 'Pascals'
    ,sl     : ' Pa'
    ,alt    : 'inH2O'
    ,scale  : 0.0040149
    ,add    : 0
});

new R.Unit({
    id      : 'percent'
    ,metric : true
    ,l      : '%'
});

new R.Unit({
    id      : 'pounds'
    ,metric : false
    ,l      : 'Pounds'
    ,sl     : 'lb'
    ,alt    : 'kilograms'
    ,scale  : 0.45359237
    ,add    : 0
});

new R.Unit({
    id      : 'psi'
    ,l      : 'psi'
    ,sl     : ' psi'
    ,alt    : 'kPa'
    ,scale  : 6.894757293
    ,add    : 0
});

new R.Unit({
    id      : 'rh'
    ,metric : true
    ,l      : '% RH'
    ,sl     : '% RH'
});

new R.Unit({
    id      : 'seconds'
    ,metric : true
    ,l      : 'seconds'
    ,sl     : ' secs'
});

new R.Unit({
    id      : 'vac'
    ,metric : true
    ,l      : 'Volts (AC)'
    ,sl     : ' V AC'
});

new R.Unit({
    id      : 'voltamphours'
    ,metric : true
    ,l      : 'Volt-Amp-Hours'
    ,sl     : ' VA-h'
});

new R.Unit({
    id      : 'voltamps'
    ,metric : true
    ,l      : 'Volt-Amps'
    ,sl     : ' VA'
});

new R.Unit({
    id      : 'watts'
    ,metric : true
    ,l      : 'Watts'
    ,sl     : ' W'
});

new R.Unit({
    id      : 'watthours'
    ,metric : true
    ,l      : 'Watt-Hours'
    ,sl     : ' W-h'
});

new R.Unit({
    id      : 'cfm'
    ,l      : 'Cubic Feet/Minute'
    ,sl     : ' CFM'
    ,alt    : 'm3ph'
    ,scale  : 0.5885778
    ,add    : 0
});

new R.Unit({
    id      : 'm3ph'
    ,l      : 'Cubic Meters/Hour'
    ,sl     : ' m\u00B3/hr'
    ,alt    : 'cfm'
    ,scale  : 1.6990106
    ,add    : 0
});


