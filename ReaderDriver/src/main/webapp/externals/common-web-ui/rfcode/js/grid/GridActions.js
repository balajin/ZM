R.grid.GridActions = Ext.extend(R.ui.PanelActions, {

    init: function(p) {
        var mgr = this.mgr;
        var grid = mgr.grid || p;

        R.grid.GridActions.superclass.init.call(this, p, grid.getSelectionModel());

        mgr.grid = grid;

        grid.on('rowdblclick', mgr.executeDefault, mgr);
        grid.store.on('add', mgr.update, mgr, {buffer: 50});
        grid.store.on('clear', mgr.update, mgr, {buffer: 50});
        grid.store.on('datachanged', mgr.update, mgr, {buffer: 50});
        grid.store.on('remove', mgr.update, mgr, {buffer: 50});
        grid.getSelectionModel().on('selectionchange', mgr.update, mgr, {buffer: 50});

        if (this.mgr.hasActions('cmenu')) {
            grid.on('cellcontextmenu', this.onRowContextMenu, this);
        }

        this.grid = grid;
    }

    // private
    ,onRowContextMenu: function(grid, row, col, e) {
         if (!this.cmenu) {
             this.cmenu = this.mgr.queryActions('cmenu', true);
             if (this.cmenu) {
                 grid.on('destroy', function() { Ext.destroy(this.cmenu); }, this);
             }
         }

         e.stopEvent();
         var selModel = grid.getSelectionModel();
         if (!selModel.isSelected(row)) {
             selModel.selectRow(row);
         }
         this.cmenu.cascade(function(item) {
             var act = item.baseAction;
             act && act.beforeMenuShow(grid, row, col);
         });
         this.cmenu.showAt(e.getXY());
    }
});

