/**
 * @class R.form.DefaultNumberField
 * @extends Ext.Panel
 * A field which provides a checkbox and a number or a spinner field. The
 * checkbox represents using the default value. If checked, the number field is
 * disabled.
 */
R.form.DefaultNumberField = Ext.extend(Ext.Container, {

    /**
     * @cfg {Boolean} allowDecimals False to disallow decimal values (defaults to true)
     */
    allowDecimals : true

    ,isFormField : true

    ,constructor : function(config) {
        var allowDecimals = Ext.value(config.allowDecimals, this.allowDecimals);
        var fieldLabel = config.fieldLabel || this.fieldLabel;
        var id = config.id;
        var name = config.name;
        var plugins = config.plugins;
        var width = config.width;

        delete config.id;
        delete config.name;
        delete config.plugins;
        delete config.width;

        this.checkbox = new Ext.form.Checkbox(Ext.applyIf({
            boxLabel       : 'Use global'
            ,isDirty       : Ext.emptyFn
            ,isFormField   : false
            ,selectOnFocus : true
            ,style         : 'margin-bottom: 4px'
            ,getErrorCt    : this.getErrorCt
            ,markInvalid   : null // so BasicForm does not add it to its field collection
            ,listeners     : {
                scope  : this
                ,check : function(cb, checked) {
                    this.numfield.setDisabled(checked);
                }
            }
        }, config));

        if (allowDecimals) {
            this.numfield = new Ext.form.NumberField(Ext.applyIf({
                isDirty        : Ext.emptyFn
                ,isFormField   : false
                ,margins       : '0 0 0 4'
                ,selectOnFocus : true
                ,getErrorCt    : this.getErrorCt
            }, config));
        }
        else {
            this.numfield = new Ext.ux.form.Spinner(Ext.applyIf({
                isDirty      : Ext.emptyFn
                ,isFormField : false
                ,margins     : '0 0 0 4'
                ,strategy    : {
                    maxValue  : config.maxValue
                    ,minValue : config.minValue
                    ,xtype    : 'number'
                }
                ,getErrorCt   : this.getErrorCt
            }, config));
        }

        R.form.DefaultNumberField.superclass.constructor.call(this, Ext.apply({
            fieldLabel : fieldLabel
            ,id        : id
            ,layout    : 'hbox'
            ,name      : name
            ,value     : config.value
            ,width     : width
            ,items     : [ this.checkbox, this.numfield ]
            ,plugins   : plugins
        }, config));
        this.on('afterlayout', this.onAfterLayout, this);

        if (this.value) {
            this.setValue(this.value);
        }
    }

    // private
    ,onAfterLayout : function() {
        // -4 to take into account the margins
        this.numfield.setWidth(this.getWidth() - this.checkbox.getWidth() - 4);
    }
    // override
    ,getName : Ext.form.Field.prototype.getName

    // override
    ,disable : function() {
         R.form.DefaultNumberField.superclass.disable.call(this);
         this.checkbox.disable();
         this.numfield.disable();
    }

    // override - prevent label from getting disabled
    ,onDisable : Ext.emptyFn

    // override
    ,enable : function() {
         R.form.DefaultNumberField.superclass.enable.call(this);
         this.checkbox.enable();
         this.numfield.enable();
    }

    // override - prevent label from getting disabled
    ,onEnable : Ext.emptyFn

    // override
    ,getValue : function() {
        if (this.checkbox.getValue()) {
            return -1;
        }
        return Ext.value(this.numfield.getValue(), -1);
    }

    // override
    ,setValue : function(value) {
        var def = Ext.value(value, -1) < 0;
        this.checkbox.setValue(def);
        this.numfield.setDisabled(def);

        this.numfield.setValue(def ? '' : value);
    }

    ,resetOriginalValue : function() {
        this.originalValue = this.getValue();
    }

    // override
    ,initValue : function(value) {
        this.numfield.setValue(value);
        this.originalValue = value;
    }

    // override
    ,isDirty : function() {
        return this.originalValue !== this.getValue();
    }

    // override
    ,validate : function() {
        return this.numfield.validate();
    }

    // private - needed by validate
    ,processValue : function(value) {
        return value;
    }

    // private - needed by validate
    ,getRawValue : function() {
        return this.numfield.getRawValue();
    }

    // override
    ,clearInvalid : function() {
        this.numfield.clearInvalid();
    }

    // override
    ,markInvalid : function(msg) {
        if (this.rendered && !this.preventMark) {
            this.numfield.markInvalid(msg);
        }
    }

    // override
    ,isValid : function() {
        return true;
    }

    ,getErrorCt : function() {
        return this.el.findParent('td', 5, true);
    }

    ,reset : Ext.form.Field.prototype.reset
});

