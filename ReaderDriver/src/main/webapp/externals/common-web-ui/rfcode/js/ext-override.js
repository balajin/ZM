Ext.PagingToolbar.prototype.cls = 'r-paging-toolbar';

// change the default. autoDestroy false causes too many problems when the
// majority if not all of our stores are bound to only one component
Ext.data.Store.prototype.autoDestroy = true;

Ext.override(Ext.form.BasicForm, {

    // adds a form attribute to each field within the form
    add : function() {
        for (var i = 0; i < arguments.length; i++) {
            arguments[i].form = this;
        }
        this.items.addAll(Array.prototype.slice.call(arguments, 0));
        return this;
    }

    // calls getValue() instead of Ajax.serializeForm
    ,getValues : function() {
        var data = {};
        this.items.each(function(f) {
            if (f.name) {
                data[f.name] = f.getValue();
            }
        });
        return data;
    }

    /**
     * Clears all fields within this form.
     */
    ,clear : function() {
        var data = {};
        this.items.each(function(f) {
            if (f.name) {
                data[f.name] = null;
            }
        });
        this.setValues(data);
    }

    // Override invalid functions to add an error element when an errors object
    // contains an error with no field ID (eg. a database connection problem)
    ,markInvalid : Ext.form.BasicForm.prototype.markInvalid.createInterceptor(function(errors) {
        for (var i = errors.length-1; i >= 0; i--) {
            var err = errors[i];
            if (err.field && !err.id) {
                err.id = err.field;
            }
            if (err.message && !err.msg) {
                err.msg = err.message;
            }

            if (!err.id) {
                if (!this.errorEl) {
                    this.errorEl = Ext.DomHelper.insertBefore(this.el.first(), {
                        cls    : 'r-form-invalid-msg'
                        ,style : 'padding-bottom: 5px;'
                        ,tag   : 'div'
                    });
                }
                this.errorEl.innerHTML = err.msg;

                // remove it, otherwise BasicForm findField returns the
                // first field when id is undefined and will set the field's
                // error to object error
                errors.splice(i, 1);
                break;
            }
        }
    })

    // override
    ,clearInvalid : Ext.form.BasicForm.prototype.clearInvalid.createInterceptor(function() {
        Ext.destroy(Ext.fly(this.errorEl));
        delete this.errorEl;
    })

    // override
    ,destroy : Ext.form.BasicForm.prototype.destroy.createInterceptor(function() {
        Ext.destroy(Ext.fly(this.errorEl));
    })
});

Ext.override(Ext.form.Field, {
    msgTarget : 'side'

    ,resetOriginalValue : function() {
        this.originalValue = this.getValue();
    }

    ,onRemoved : Ext.form.Field.prototype.onRemoved.createInterceptor(function() {
        if (this.ownerCt) {
            // Fix bug in ExtJS in which it does not remove these listeners.
            // The listeners are added by Ext.form.MessageTargets.side
            this.ownerCt.un('afterlayout', this.alignErrorIcon, this);
            this.ownerCt.un('expand', this.alignErrorIcon, this);
        }
        return true;
    })
});
if (Ext.ux && Ext.ux.StatusBar) {
    Ext.override(Ext.ux.StatusBar, {
        clearStatus : Ext.ux.StatusBar.prototype.clearStatus.createInterceptor(function() {
            return !this.isDestroyed;
        })
    });
}

// Adds a wordWrap option
// http://www.extjs.com/forum/showthread.php?p=248174
Ext.override(Ext.form.TextArea, {
    initComponent: Ext.form.TextArea.prototype.initComponent.createSequence(function() {
        Ext.applyIf(this, {wordWrap: true});
    }),

    onRender: Ext.form.TextArea.prototype.onRender.createSequence(function(ct, position){
        this.el.setOverflow('auto');
        if (this.wordWrap === false) {
            if (!Ext.isIE) {
                this.el.set({wrap:'off'})
            }
            else {
                this.el.dom.wrap = 'off';
            }
        }
        if (this.preventScrollbars === true) {
            this.el.setStyle('overflow', 'hidden');
        }
    })
});

Ext.override(Ext.form.ComboBox, {
    // http://www.extjs.com/forum/showthread.php?p=431959
    // blur would sometimes fire after the combo was destroyed (eg. combo in a
    // window)
    beforeBlur : Ext.form.ComboBox.prototype.beforeBlur.createInterceptor(function() {
        return !!this.store;
    })
});

Ext.grid.CheckColumn = function(config){
    Ext.apply(this, config);
    if(!this.id){
        this.id = Ext.id();
    }
    this.renderer = this.renderer.createDelegate(this);
};

Ext.grid.CheckColumn.prototype ={
    init : function(grid){
        this.grid = grid;
        this.grid.on('render', function(){
            var view = this.grid.getView();
            view.mainBody.on('mousedown', this.onMouseDown, this);
        }, this);
    },

    onMouseDown : function(e, t){
        if (!this.grid.disabled && t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
            e.stopEvent();
            var index = this.grid.getView().findRowIndex(t);
            var record = this.grid.store.getAt(index);
            record.set(this.dataIndex, !record.data[this.dataIndex]);
        }
    },

    renderer : function(v, p, record){
        p.css += ' x-grid3-check-col-td';
        return '<div class="x-grid3-check-col'+(v?'-on':'')+' x-grid3-cc-'+this.id+'">&#160;</div>';
    }
};

/**
 * Removes and destroys all the children from the node.
 * @class Ext.tree.TreeNode
 * @method
 */
Ext.tree.TreeNode.prototype.removeChildren = function() {
    var child = this.firstChild;
    while (child) {
        child.remove();
        child.destroy();
        child = this.firstChild;
    }
};

Ext.override(Ext.tree.DefaultSelectionModel, {
    onNodeClick : Ext.tree.DefaultSelectionModel.prototype.onNodeClick.createInterceptor(function() {
        return !this.tree.isDestroyed;
    })
});

// bug 4351 - Ext.ux.form.DateTime is not calling the super show/hide functions
// causing problems when the field is placed within a card layout
if (Ext.ux && Ext.ux.form && Ext.ux.form.DateTime) {
    Ext.override(Ext.ux.form.DateTime, {
        setVisible : Ext.form.Field.prototype.setVisible

        ,show:function() {
            Ext.ux.form.DateTime.superclass.show.call(this);
            this.df.show();
            this.tf.show();
            return this;
        }

        ,hide:function() {
            Ext.ux.form.DateTime.superclass.hide.call(this);
            this.df.hide();
            this.tf.hide();
            return this;
        }

        ,setSize:function(w, h) {
            if(!w) {
                return;
            }
            // rfcode - begin
            if(typeof w == 'object'){
                h = w.height;
                w = w.width;
            }
            // rfcode - end
            if('below' === this.timePosition) {
                this.df.setSize(w, h);
                this.tf.setSize(w, h);
                if(Ext.isIE) {
                    this.df.el.up('td').setWidth(w);
                    this.tf.el.up('td').setWidth(w);
                }
            }
            else {
                this.df.setSize(w - this.timeWidth - 4, h);
                this.tf.setSize(this.timeWidth, h);

                if(Ext.isIE) {
                    this.df.el.up('td').setWidth(w - this.timeWidth - 4);
                    this.tf.el.up('td').setWidth(this.timeWidth);
                }
            }
        }
    });
}

// http://www.sencha.com/forum/showthread.php?117967-3.3.1-TreeSorter-creates-global-variables
Ext.tree.TreeSorter = Ext.extend(Object, {
    constructor: function(tree, config) {
        Ext.apply(this, config);
        tree.on({
            scope: this,
            beforechildrenrendered: this.doSort,
            append: this.updateSort,
            insert: this.updateSort,
            textchange: this.updateSortParent
        });

        var desc = this.dir && this.dir.toLowerCase() == 'desc',
            prop = this.property || 'text',
            sortType = this.sortType,
            folderSort = this.folderSort,
            caseSensitive = this.caseSensitive === true,
            leafAttr = this.leafAttr || 'leaf';

        if(Ext.isString(sortType)){
            sortType = Ext.data.SortTypes[sortType];
        }
        this.sortFn = function(n1, n2){
            var attr1 = n1.attributes,
                attr2 = n2.attributes;

            if(folderSort){
                if(attr1[leafAttr] && !attr2[leafAttr]){
                    return 1;
                }
                if(!attr1[leafAttr] && attr2[leafAttr]){
                    return -1;
                }
            }
            var prop1 = attr1[prop],
                prop2 = attr2[prop],
                v1 = sortType ? sortType(prop1) : (caseSensitive ? prop1 : prop1.toUpperCase()),
                v2 = sortType ? sortType(prop2) : (caseSensitive ? prop2 : prop2.toUpperCase());

            if(v1 < v2){
                return desc ? 1 : -1;
            }else if(v1 > v2){
                return desc ? -1 : 1;
            }
            return 0;
        };
    },

    doSort : function(node){
        node.sort(this.sortFn);
    },

    updateSort : function(tree, node){
        if(node.childrenRendered){
            this.doSort.defer(1, this, [node]);
        }
    },

    updateSortParent : function(node){
        var p = node.parentNode;
        if(p && p.childrenRendered){
            this.doSort.defer(1, this, [p]);
        }
    }
});

/**
 * Override to allow border layout components to express their size using proportions rather
 * than fixed widths and heights.
 */

Ext.override(Ext.layout.BorderLayout, {

    // private
    onLayout : function(ct, target){
        var collapsed, i, c, pos, items = ct.items.items, len = items.length;
        if(!this.rendered){
            collapsed = [];
            for(i = 0; i < len; i++) {
                c = items[i];
                pos = c.region;
                if(c.collapsed){
                    collapsed.push(c);
                }
                c.collapsed = false;
                if(!c.rendered){
                    c.render(target, i);
                    c.getPositionEl().addClass('x-border-panel');
                }
                this[pos] = pos != 'center' && c.split ?
                    new Ext.layout.BorderLayout.SplitRegion(this, c.initialConfig, pos) :
                    new Ext.layout.BorderLayout.Region(this, c.initialConfig, pos);
                this[pos].render(target, c);
            }
            this.rendered = true;
        }

        var size = this.getLayoutTargetSize();
        if(size.width < 20 || size.height < 20){ // display none?
            if(collapsed){
                this.restoreCollapsed = collapsed;
            }
            return;
        }else if(this.restoreCollapsed){
            collapsed = this.restoreCollapsed;
            delete this.restoreCollapsed;
        }

        var w = size.width, h = size.height,
            centerW = w, centerH = h, centerY = 0, centerX = 0,
            n = this.north, s = this.south, west = this.west, e = this.east, c = this.center,
            b, m, totalWidth, totalHeight;
        if(!c && Ext.layout.BorderLayout.WARN !== false){
            throw 'No center region defined in BorderLayout ' + ct.id;
        }

        if(n && n.isVisible()){
            b = n.getSize();
            m = n.getMargins();
            b.width = w - (m.left+m.right);
            b.x = m.left;
            b.y = m.top;
            centerY = b.height + b.y + m.bottom;
            centerH -= centerY;

            if (typeof n.height == 'string') {
                var p = n.height.match(/(\d+)%/);
                if (p[1]) {
                    n.height = parseInt(p[1], 10) * .01 * size.height;
                    b.height = n.height;
                }
            }
            n.applyLayout(b);
        }
        if(s && s.isVisible()){
            b = s.getSize();
            m = s.getMargins();
            b.width = w - (m.left+m.right);
            b.x = m.left;
            totalHeight = (b.height + m.top + m.bottom);
            b.y = h - totalHeight + m.top;
            centerH -= totalHeight;

            if (typeof s.height == 'string') {
                var p = s.height.match(/(\d+)%/);
                if (p[1]) {
                    s.height = parseInt(p[1], 10) * .01 * size.height;
                    b.height = s.height;
                }
            }
            s.applyLayout(b);
        }
        if(west && west.isVisible()){
            b = west.getSize();
            m = west.getMargins();
            b.height = centerH - (m.top+m.bottom);
            b.x = m.left;
            b.y = centerY + m.top;
            totalWidth = (b.width + m.left + m.right);
            centerX += totalWidth;
            centerW -= totalWidth;

            if (typeof west.width == 'string') {
                var p = west.width.match(/(\d+)%/);
                if (p[1]) {
                    west.width = parseInt(p[1], 10) * .01 * size.width;
                    b.width = west.width;
                }
            }
            west.applyLayout(b);
        }
        if(e && e.isVisible()){
            b = e.getSize();
            m = e.getMargins();
            b.height = centerH - (m.top+m.bottom);
            totalWidth = (b.width + m.left + m.right);
            b.x = w - totalWidth + m.left;
            b.y = centerY + m.top;
            centerW -= totalWidth;

            if (typeof e.width == 'string') {
                var p = e.width.match(/(\d+)%/);
                if (p[1]) {
                    e.width = parseInt(p[1], 10) * .01 * size.width;
                    b.width = e.width;
                }
            }
            e.applyLayout(b);
        }
        if(c){
            m = c.getMargins();
            var centerBox = {
                x: centerX + m.left,
                y: centerY + m.top,
                width: centerW - (m.left+m.right),
                height: centerH - (m.top+m.bottom)
            };
            c.applyLayout(centerBox);
        }
        if(collapsed){
            for(i = 0, len = collapsed.length; i < len; i++){
                collapsed[i].collapse(false);
            }
        }
        if(Ext.isIE && Ext.isStrict){ // workaround IE strict repainting issue
            target.repaint();
        }
        // Putting a border layout into an overflowed container is NOT correct and will make a second layout pass necessary.
        if (i = target.getStyle('overflow') && i != 'hidden' && !this.adjustmentPass) {
            var ts = this.getLayoutTargetSize();
            if (ts.width != size.width || ts.height != size.height){
                this.adjustmentPass = true;
                this.onLayout(ct, target);
            }
        }
        delete this.adjustmentPass;
    }
});
