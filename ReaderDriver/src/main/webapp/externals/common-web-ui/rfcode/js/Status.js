/**
 * The main status bar for the application
 * @class R.Status
 */
R.Status = Ext.extend(Object, {
    constructor : function(config) {
        R.Status.superclass.constructor.call(this);

        this.left = this.createInfoPanel({
            id       : 'app-sb-west'
            ,links   : config.left
            ,region  : 'west'
            ,autoWidth : true //for fixing width in Chrome
        });

        this.right = this.createInfoPanel({
            id       : 'app-sb-east'
            ,links   : config.right
            ,region  : 'east'
        });

        this.center = new Ext.Container({
            id      : 'app-sb-center'
            ,region : 'center'
            ,html   : (config.center || []).join('')
        });
    }

    // private
    ,createInfoPanel : function(config) {
        var links = config.links || [];
        var tbar  = [];
        var i, link;

        if (!Ext.isArray(links)) {
            links = links.items;
            delete config.links.items;
            Ext.apply(config, config.links);
        }

        for (i = 0; i < links.length; i++) {
            link = links[i];
            if (link === '-') {
                tbar.push('<span class="xtb-sep">|</span>');
            }
            else if (link.href) {
                tbar.push('<a href="' + link.href + '">' + link.text + '</a>');
            }
            else if (link.onclick) {
                tbar.push('<a href="javascript:' + link.onclick + 'void(0)">' + link.text + '</a>');
            }
            else {
                tbar.push(link.text);
            }
        }

        if (tbar.length > 0) {
            return new Ext.Container(Ext.apply({
                html : tbar.join('')
            }, config));
        }
    }

    /**
     * Returns the component representing the status bar.
     * @return {Ext.Component} statusBar
     */
    ,createUI : function(config) {
        var items = [];
        if (this.left) {
            items.push(this.left);
        }
        items.push(this.center);
        if (this.right) {
            items.push(this.right);
        }
        this.ui = new Ext.Container(Ext.apply(config, {
            id      : 'app-sb'
            ,layout : 'border'
            ,border : false
            ,height : 24
            ,items  : items
        }));
        return this.ui;
    }
});
