/**
 * @class R.Ajax
 */
R.Ajax = {
    /**
     * Queues multiple requests. Each request waits until the previous request
     * is complete before continuning.
     */
    queue : function(config) {
        var reqs      = config.requests;
        var index     = 0;
        var options   = [];
        var responses = [];
        var method    = (config.method || 'POST');
        var decode    = !!config.decode;
        var mask;

        var request = function() {
            var req = reqs[index];

            if (typeof req === 'string') {
                req = { url: req };
            }
            index++;
            Ext.Ajax.request(Ext.apply({
                callback : callback
                ,method  : method
            }, req));
        };

        var callback = function(o, success, response) {
            var txt = response.responseText;
            var fn, result;

            if (decode) {
                if (txt && response.status !== 401) {
                    result = Ext.decode(txt);
                    responses.push(result);
                }
                else {
                    responses.push({});
                }
            }
            else {
                responses.push(response);
            }
            options.push(o);

            if (!success || (decode && (!result || result.__result === 'error'))) {
                success = false;
                fn = config.failure || Ext.emptyFn;
            }
            else if (index === reqs.length) {
                fn = config.success || Ext.emptyFn;
            }
            else if (index < reqs.length) {
                request();
            }

            if (fn) {
                if (mask) {
                    mask.hide();
                }
                Ext.callback(config.callback, config.scope, [options, success, responses]);
                Ext.callback(fn, config.scope, [responses, options]);
            }
        };

        mask = R.ui.Mask.show(config);
        request();
    }

    /**
     * Same as Ext.Ajax except supports R.ui.Mask parameters.
     */
    ,request : function(config) {
        var mask = R.ui.Mask.show(config);
        if (mask) {
            var success = config.success;
            var failure = config.failure;
            var scope   = config.scope;

            // We need to hide the mask before the callers callbacks are called
            // otherwise using Ext.Msg will fail since its mask gets hidden
            // by us. Calling config.callback is too late since it is called
            // after the success and failure functions.
            config = Ext.applyIf({
                success : function() {
                    mask.hide();
                    Ext.callback(success, scope, arguments);
                }
                ,failure : function() {
                    mask.hide();
                    Ext.callback(failure, scope, arguments);
                }
            }, config);
        }
        return Ext.Ajax.request(config);
    }
};

