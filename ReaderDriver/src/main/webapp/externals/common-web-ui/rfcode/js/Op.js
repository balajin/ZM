/**
 * @class R.Op
 * Represents an operator.
 */
R.Op = {};

R.Op.Record = Ext.data.Record.create([ 'value', 'text' ]);

/**
 * Returns an array of R.Op.Record objects (eg. { 'eq', '=' }) corresponding to
 * the operators the given type of attribute class supports.
 * @param {String} type - type of attribute (eg. 'bool', 'double')
 * @param {Boolean} isFilter - If true, def and undef are not returned in the
 * list of operators.
 * @static
 */
R.Op.findByType = function(type, isFilter) {
    var r = [];

    if (R.AC.TYPES.indexOf(type) === -1 && type !== 'all') {
        throw new Error('Unknown attribute class type: ' + type);
    }

    function a(op) {
        r.push(new R.Op.Record(R.Op.get(op)));
    };

    if (type === 'bool') {
        a('eq');
        a('ne');
    }
    else if (type === 'enum' || type === 'double' || type === 'long') {
        a('eq');
        a('ne');
        a('gt');
        a('lt');
        a('ge');
        a('le');
    }
    else if (type === 'string') {
        a('eq');
        a('ne');
        a('contains');
        a('doesnotcontain');
        a('startswith');
    }
    else if (type === 'string-list') {
        a('eq');
        a('ne');
        a('contains');
    }
    else if (type === 'string-search') {
        a('contains');
    }
    else if (type === 'entityref') {
        a('eq');
        a('ne');
    }
    else if (type === 'entityref-list') {
        a('eq');
        a('ne');
        a('contains');
    }
    else if (type === 'lat-lon-coord') {
        a('inbox');
        a('notinbox');
    }
    else if (type === 'typeref' || type === 'typeref-path' || type === 'typeref-list') {
        a('eq');
        a('ne');
        a('in');
    }
    else if (type === 'date' || type === 'timestamp') {
        if (type === 'date') {
            a('eq');
        }
        a('ne');
        a('gt');
        a('lt');
        a('ge');
        a('le');
    }
    else if (type === 'all') {
        a('eq');
        a('ne');
        a('gt');
        a('lt');
        a('ge');
        a('le');
        a('contains');
        a('startswith');
    }

    if (!isFilter) {
        a('def');
        a('undef');
    }
    return r;
};

(function() {
    var ops = {};

    /**
     * Returns the operator for the given value (eg. lt or eq)
     * @param {String} op The ID of the operator to get
     */
    R.Op.get = function(op) {
        return ops[op];
    };

    /**
     * Returns true if the expression "lt op rt" returns true.
     * @param {R.AC} ac The attribute class
     * @param {String} op The ID of the operator
     * @param {Mixed} lt The left hand side of the expression
     * @param {Mixed} rt The right hand side of the expression
     */
    R.Op.matches = function(ac, op, lt, rt) {
        return ops[op].matches(ac, lt, rt);
    }

    var toString = function() {
        return this.text;
    };

    var add = function(op) {
        op.toString = toString;
        ops[op.value] = op;
    };

    var bool = function(v) {
        if (v === 'true' || v === true) {
            return true;
        }
        return false;
    };

    var contains = function(ac, lt, rt) {
        if (typeof lt === 'string') {
            return lt.indexOf(rt) !== -1;
        }
        else if (Ext.isArray(lt)) {
            if (typeof rt === 'string') {
                return lt.indexOf(rt) !== -1;
            }
            else if (Ext.isArray(rt)) {
                // make sure lt contains each element in rt
                for (var i = 0; i < rt.length; i++) {
                    if (lt.indexOf(rt[i]) === -1) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    };

    add({
        text   : '='
        ,value : 'eq'
        ,matches : function(ac, lt, rt) {
            var type = ac.type;

            if (type === 'bool') {
                return bool(lt) === bool(rt);
            }
            else if (type === 'date') {
                return rt >= lt && rt < (lt + 86400000);
            }
            else if (type === 'timestamp') {
                return rt >= lt && rt < (lt + 60000);
            }
            return String(lt) === String(rt);
        }
    });
    add({
        text : '!='
        ,value : 'ne'
        ,matches : function(ac, lt, rt) {
            if (Ext.isEmpty(lt) || Ext.isEmpty(rt)) {
                return false;
            }
            return String(lt) !== String(rt);
        }
    });
    add({
        text : '>'
        ,value : 'gt'
        ,matches : function(ac, lt, rt) {
            return lt > rt;
        }
    });
    add({
        text : '<'
        ,value : 'lt'
        ,matches : function(ac, lt, rt) {
            return lt < rt;
        }
    });
    add({
        text : '>='
        ,value : 'ge'
        ,matches : function(ac, lt, rt) {
            return lt >= rt;
        }
    });
    add({
        text : '<='
        ,value : 'le'
        ,matches : function(ac, lt, rt) {
            return lt <= rt;
        }
    });
    add({
        text : 'Contains'
        ,value : 'contains'
        ,matches : contains
    });
    add({
        text : 'Does Not Contain'
        ,value : 'doesnotcontain'
        ,matches : function(ac, lt, rt) {
            return !contains(ac, lt, rt);
        }
    });
    add({
        text : 'Starts With'
        ,value : 'startswith'
        ,matches : function(ac, lt, rt) {
            return typeof lt === 'string' && lt.indexOf(rt) === 0;
        }
    });
    add({
        text : 'In'
        ,value : 'in'
        ,matches : function(ac, lt, rt) {
            return lt < rt;
        }
    });
    add({
        text : 'In Box'
        ,value : 'inbox'
        ,matches : function(ac, lt, rt) {
            return true;
        }
    });
    add({
        text : 'Not In Box'
        ,value : 'notinbox'
        ,matches : function(ac, lt, rt) {
            return true;
        }
    });
    add({
        text : 'Has Value'
        ,value : 'def'
        ,matches : function(ac, lt) {
            return !Ext.isEmpty(lt, true);
        }
    });
    add({
        text : 'Does Not Have Value'
        ,value : 'undef'
        ,matches : function(ac, lt) {
            return Ext.isEmpty(lt, true);
        }
    });
    // pseudo operator for doing alert time range queries
    add({
        text   : 'Range'
        ,value : 'range'
        ,matches : function(ac, lt, rt) {
            return true;
        }
    });
})();
