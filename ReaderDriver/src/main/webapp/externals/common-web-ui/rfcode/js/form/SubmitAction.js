/**
 * @class R.form.SubmitAction
 * @extends Ext.form.Action.Submit
 * Extends the submit action to support multiple requests.
 */
R.form.SubmitAction = Ext.extend(Ext.form.Action.Submit, {
    // override
    run : function() {
        var o = this.options;
        var isUpload = this.form.fileUpload;

        this.form.clearInvalid();

        if (o.clientValidation === false || this.form.isValid()) {
            var method = this.getMethod();

            if (o.requests) {
                R.Ajax.queue(Ext.apply(this.createCallback(o), {
                    requests : o.requests
                }));
            }
            else {
                R.Ajax.request(Ext.apply(this.createCallback(o), {
                    form      : isUpload ? this.form.el.dom : null
                    ,isUpload : isUpload
                    ,method   : method
                    ,params   : o.data || this.form.getValues()
                    ,url      : this.getUrl(method !== 'POST')
                }));
            }
        }
        else if (o.clientValidation !== false){ // client validation failed
            this.failureType = Ext.form.Action.CLIENT_INVALID;
            this.form.afterAction(this, false);
        }
    }

    // override
    ,processResponse : function(response) {
        var result = { success: true };
        if (this.options.requests) {
            var i;
            for (i = 0; i < response.length; i++) {
                this.doSingleResponse(result, response[i]);
            }
        }
        else {
            this.doSingleResponse(result, response);
        }
        this.result = result;
        return result;
    }

    // private
    ,doSingleResponse : function(result, response) {
        if (!response.responseText) {
            return true;
        }

        var json = Ext.decode(response.responseText);
        if (json.__result === 'error') {
            result.errors = [ { msg: json.__msg } ];
            result.success = false;
        }
    }

    // override
    ,success : function(response) {
        var result = this.processResponse(response);
        if (result === true || result.success){
            // on success set the field original values to the latest values
            // so isDirty works correctly
            this.form.clearInvalid();
            this.form.setValues(this.form.getValues());
            this.form.afterAction(this, true);
            return;
        }
        if (result.errors) {
            this.form.markInvalid(result.errors);
            this.failureType = Ext.form.Action.SERVER_INVALID;
        }
        this.form.afterAction(this, false);
    }
});
Ext.form.Action.ACTION_TYPES['submit'] = R.form.SubmitAction;
