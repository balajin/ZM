/**
 * @class R.form.DualListField
 * @extends Ext.Container
 * DualListField is a field which allows a user to add and remove items
 * from a left list and a right list. It also supports an optional set of
 * buttons to reorder the items in the right list (ie. the selected items).
 * @cfg {Ext.data.Store} store The store containing all of the available list
 * of items
 */
R.form.DualListField = R.form.toField(Ext.extend(Ext.Container, {
    /**
     * @cfg {String} fromTitle
     * The title used for the available items
     */
    /**
     * @cfg {String} toTitle
     * The title used for the selected items
     */

    /**
     * @cfg {String} displayField
     * The underlying data field name to bind to this panel to use for
     * displaying each item. The default is 'text'.
     */
    displayField : 'text'

    /**
     * @cfg {String} valueField
     * The underlying data field name to bind to this panel to use for the
     * value of each item. The default is 'value'.
     */
    ,valueField : 'value'

    /**
     * @cfg {Boolean} reordering
     * If true this panel will show buttons allowing the user to reorder the
     * list of selected items. Otherwise, only the move left and move right
     * buttons are shown. Default is false.
     */
    ,reordering : false

    ,autoEl      : 'div'
    ,focusClass  : undefined
    ,isFormField : true
    ,msgTarget   : 'side'

    ,constructor : function(config) {
        var that = this;
        var buttons = [];
        var button = function(config) {
            var btn = new Ext.Button(Ext.apply(config, {
                cls       : config.cls + ' x-btn-icon'
                ,disabled : true
                ,minWidth : 30
                ,scope    : that
            }));
            buttons.push(btn);
            return btn;
        };

        // buttons created same order as shown in UI
        if (config.reordering) {
            this.moveTopButton = button({
                cls       : 'button-move-top'
                ,handler  : this.moveTop
                ,tooltip  : 'Move to Top'
                ,disabled : true
            });
            this.moveUpButton = button({
                cls       : 'button-move-up'
                ,handler  : this.moveUp
                ,tooltip  : 'Move Up'
                ,disabled : true
                ,margins  : '4 0 12 0'
            });
        }
        this.moveRightButton = button({
            cls      : 'button-move-right'
            ,handler : this.moveRight
            ,tooltip : 'Add'
        });
        this.moveLeftButton = button({
            cls      : 'button-move-left'
            ,handler : this.moveLeft
            ,tooltip : 'Remove'
            ,margins : '4 0 0 0'
        });
        if (config.reordering) {
            this.moveDownButton = button({
                cls       : 'button-move-down'
                ,handler  : this.moveDown
                ,tooltip  : 'Move Down'
                ,disabled : true
                ,margins  : '12 0 0 0'
            });
            this.moveBottomButton = button({
                cls       : 'button-move-bottom'
                ,handler  : this.moveBottom
                ,tooltip  : 'Move To Bottom'
                ,disabled : true
                ,margins  : '4 0 0 0'
            });
        }
        // auto-configure store if necessary
        if (Ext.isArray(config.store)) {
            if (Ext.isArray(config.store[0])){
                config.store = new Ext.data.SimpleStore({
                    fields : ['value', 'text']
                    ,data  : config.store
                });
                config.valueField = 'value';
            }
            else {
                config.store = new Ext.data.SimpleStore({
                    fields      : ['text']
                    ,data       : config.store
                    ,expandData : true
                });
                config.valueField = 'text';
            }
            config.displayField = 'text';
        }

        // needed by this.createGrid
        this.displayField = config.displayField;

        this.ltGrid = this.createGrid({
            store  : new Ext.data.Store({
                fields : config.store.recordType.prototype.fields
                ,sortInfo : { field : config.displayField }
            })
            ,title : config.fromTitle || 'Available'
            ,listeners   : {
                'rowdblclick' : { scope: this, fn: this.moveRight }
            }
        });
        this.ltGrid.getSelectionModel().on('selectionchange', function(selmodel) {
            this.moveRightButton.setDisabled(!selmodel.hasSelection());
        }, this);

        this.rtGrid = this.createGrid({
            store  : new Ext.data.Store({
                fields : config.store.recordType.prototype.fields
                ,sortInfo : config.reordering ? null : { field : config.displayField }
            })
            ,title : config.toTitle || 'Selected'
            ,listeners   : {
                'rowdblclick' : { scope: this, fn: this.moveLeft }
            }
        });
        this.rtGrid.getSelectionModel().on('selectionchange', function(selmodel) {
            var disabled = !selmodel.hasSelection();

            this.moveLeftButton.setDisabled(disabled);

            if (config.reordering) {
                this.moveTopButton.setDisabled(disabled);
                this.moveUpButton.setDisabled(disabled);
                this.moveDownButton.setDisabled(disabled);
                this.moveBottomButton.setDisabled(disabled);
            }
        }, this);

        R.form.DualListField.superclass.constructor.call(this, Ext.apply({
            layout : {
                align : 'stretch'
                ,type : 'hbox'
            }
            ,items    : [
                this.ltGrid
                ,{
                    autoEl  : 'div'
                    ,layout : {
                        align : 'center'
                        ,pack : 'center'
                        ,type : 'vbox'
                    }
                    ,style  : 'margin: 0 4 0 4'
                    ,width  : 30
                    ,xtype  : 'container'
                    ,items  : [ buttons ]
                    ,style  : 'text-align:center;'
                }
                ,this.rtGrid
            ]
        }, config));
    }

    // private
    ,createGrid : function(config) {
        var store = config.store;
        return new Ext.grid.GridPanel(Ext.apply({
            cls          : 'dual-list-field-grid'
            ,columns     : [ { id: this.displayField, dataIndex: this.displayField } ]
            ,flex        : 1
            ,frame       : false
            ,header      : true
            ,hideHeaders : true
            ,viewConfig  : {
                forceFit : true
            }
            ,tbar : [
                'Filter'
                ,{
                    enableKeyEvents : true
                    ,triggerClass   : 'x-form-clear-trigger'
                    ,xtype          : 'trigger'
                    ,listeners      : {
                        'keyup' : {
                            buffer : 150
                            ,scope : this
                            ,fn    : function(field, e) {
                                if (Ext.EventObject.ESC == e.getKey()) {
                                    field.onTriggerClick();
                                }
                                else {
                                    store.filter(this.displayField, field.getRawValue(), true);
                                }
                            }
                        }
                    }
                    ,onTriggerClick : function() {
                        this.setValue('');
                        store.clearFilter();
                        this.focus();
                    }

                }
            ]
        }, config));
    }

    // private
    ,clearRightFilter : function() {
        var grid = this.rtGrid;
        grid.getTopToolbar().items.each(function(item) {
            if (item.xtype === 'trigger') {
                item.setValue('');
                return false;
            }
        });
        grid.store.clearFilter();
    }

    ,focus : function() {
        if (this.ltGrid) {
            this.ltGrid.focus();
        }
    }

    ,onDestroy : function() {
        Ext.destroy(this.ltGrid, this.rtGrid, this.panel);
        R.form.DualListField.superclass.onDestroy.call(this);
    }

    ,afterRender : function(){
        R.form.DualListField.superclass.afterRender.call(this);
        this.initValue();
    }

    ,initValue : function() {
        var value = this.value;
        delete this.value; // ensures we correctly fill available list in setValue
        this.setValue(value || []);
        this.originalValue = this.getValue();
    }

    /**
     * Returns the values (ie. the value of valueField) of the selected items.
     */
    ,getValue : function() {
        return this.value;
    }

    ,setValue : function(value) {
        if (value === this.value) {
            return;
        }

        var ltStore = this.ltGrid.store;
        var rtStore = this.rtGrid.store;
        var valueField = this.valueField;
        var selected = {};
        var toAdd = [];

        // clear the current selection
        ltStore.removeAll();
        rtStore.removeAll();

        // insert right list
        var add = function(guid) {
            var index = this.store.findExact(valueField, guid);
            if (index >= 0) {
                selected[guid] = true;
                toAdd.push(this.store.getAt(index).copy());
            }
        };
        if (Ext.isArray(value)) {
            R.each(value, add, this);
        }
        else if (!Ext.isEmpty(value)) {
            add.call(this, value);
        }
        rtStore.add(toAdd);
        if (!this.reordering) {
            rtStore.sort(this.displayField, 'ASC');
        }

        // insert left list
        toAdd = [];
        this.store.each(function(record) {
            if (selected[record.get(valueField)] !== true) {
                toAdd.push(record.copy());
            }
        }, this);
        ltStore.add(toAdd);
        ltStore.sort(this.displayField, 'ASC');

        this.value = value;
    }

    // private
    ,valueChanged : function() {
        var value = [];
        var field = this.valueField;
        this.rtGrid.store.each(function(record) {
            value.push(record.get(field));
        });
        this.value = value;
    }

    // private
    ,moveUp : function() {
        this.moveUpDown(true);
    }

    // private
    ,moveRight : function() {
        this.clearRightFilter();
        this.move(this.ltGrid, this.rtGrid);
    }

    // private
    ,moveLeft : function() {
        this.move(this.rtGrid, this.ltGrid);
    }

    // private
    ,move : function(src, dst) {
        var srcsm   = src.getSelectionModel();
        var records = srcsm.getSelections();
        if (records.length > 0) {
            if (records.length < 10 || src.store.isFiltered()) {
                R.each(records, src.store.remove, src.store);
            }
            else {
                // rebuilding the whole list is faster
                var newsrc   = [];
                var srcstore = src.store;
                var count    = srcstore.getCount();
                for (var i = 0; i < count; i++) {
                    var r = srcstore.getAt(i);
                    if (!srcsm.isSelected(r)) {
                        newsrc.push(r);
                    }
                }
                srcstore.removeAll();
                srcstore.add(newsrc);
            }
            dst.store.add(records);

            var si = dst.store.sortInfo;
            if (si) {
                dst.store.sort(si.field, si.direction);
            }
        }
        this.valueChanged();
    }

    // private
    ,moveDown : function() {
        this.moveUpDown(false);
    }

    ,moveUpDown : function(up) {
        this.clearRightFilter();
        if (up) {
            R.grid.up(this.rtGrid);
        }
        else {
            R.grid.down(this.rtGrid);
        }
        this.valueChanged();
    }

    // private
    ,moveTop : function() {
        this.moveTopBottom(true);
    }

    // private
    ,moveBottom : function() {
        this.moveTopBottom(false);
    }

    ,moveTopBottom : function(top) {
        this.clearRightFilter();

        var store = this.rtGrid.store;
        var records = this.rtGrid.getSelectionModel().getSelections();
        R.each(records, store.remove, store);
        if (top) {
            store.insert(0, records.reverse());
        }
        else {
            store.add(records);
        }
        this.rtGrid.getSelectionModel().selectRecords(records);
        this.valueChanged();
    }
}));
