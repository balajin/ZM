/*
 * Ext JS Library 2.2.1
 * Copyright(c) 2006-2009, Ext JS, LLC.
 * licensing@extjs.com
 *
 * http://extjs.com/license
 */

/**
 * @class Ext.ux.ValidationStatus
 * A {@link Ext.StatusBar} plugin that provides automatic error notification when the
 * associated form contains validation errors.
 * @extends Ext.Component
 * @constructor
 * Creates a new ValiationStatus plugin
 * @param {Object} config A config object
 */
Ext.ux.ValidationStatus = function(config) {
    Ext.apply(this, config);
};

Ext.ux.ValidationStatus.prototype = {

    errorIconCls : 'x-status-error',

    errorListCls : 'x-status-error-list',

    validIconCls : 'x-status-valid',

    showText : 'The form has errors (click for details...)',

    hideText : 'Click again to hide the error list',

    submitText : 'Saving...',

    // private
    init : function(sb){
        sb.on('render', function(){
            this.statusBar = sb;
            this.errors = new Ext.util.MixedCollection();
            this.listAlign = (sb.statusAlign=='right' ? 'br-tr?' : 'bl-tl?');

            if (this.formpanel) {
                var formpanel = this.formpanel;
                delete this.formpanel;
                this.setFormPanel(formpanel);
            }
        }, this, {single:true});

        sb.on({
            scope: this,
            afterlayout:{
                single: true,
                fn: function(){
                    // Grab the statusEl after the first layout.
                    sb.statusEl.getEl().on('click', this.onStatusClick, this, {buffer:200});
                }
            },
            beforedestroy:{
                single: true,
                fn: this.onDestroy
            }
        });
    },

    setFormPanel : function(formpanel) {
         if (this.form) {
             this.stopMonitoring();
         }

         if (typeof formpanel === 'string') {
             formpanel = Ext.getCmp(formpanel);
             this.form = formpanel.getForm();
         }
         else if (!Ext.isEmpty(formpanel)) {
             this.form = formpanel.form;
         }
         formpanel.on('destroy', this.stopMonitoring, this);
         this.clearErrors();
         this.startMonitoring();
    },

    // private
    startMonitoring : function(){
        this.form.items.each(function(f){
            f.on('invalid', this.onFieldValidation, this);
            f.on('valid', this.onFieldValidation, this);
        }, this);
    },

    // private
    stopMonitoring : function(){
        if (this.form) {
            this.form.items.each(function(f){
                f.un('invalid', this.onFieldValidation, this);
                f.un('valid', this.onFieldValidation, this);
            }, this);
        }
    },

    // private
    onFieldValidation : function(f, msg){
        if(msg){
            this.errors.add(f.id, {field:f, msg:msg});
        }
        else{
            this.errors.removeKey(f.id);
        }
        this.updateStatus(f);
    },

    // private
    updateErrorList : function(){
        if(this.errors.getCount() > 0){
            var msg = '<ul>';
            this.errors.each(function(err){
                if (err.field) {
                    msg += ('<li id="x-err-'+ err.field.id +'"><a href="#">' + err.msg + '</a></li>');
                }
                else {
                    msg += ('<li><a href="#">' + err.msg + '</a></li>');
                }
            }, this);
            this.getMsgEl().update(msg+'</ul>');
        }else{
            this.getMsgEl().update('');
        }
    },

    updateStatus : function() {
         this.updateErrorList();
         if(this.errors.getCount() > 0){
             if(this.statusBar.getText() != this.showText){
                 this.statusBar.setStatus({text:this.showText, iconCls:this.errorIconCls});
             }
         }
         else{
             this.statusBar.clearStatus().setIcon(this.validIconCls);
         }
    },

    // private
    getMsgEl : function(){
        if(!this.msgEl){
            this.msgEl = Ext.DomHelper.append(Ext.getBody(), {
                cls: this.errorListCls+' x-hide-offsets'
            }, true);

            this.msgEl.on('click', function(e){
                var t = e.getTarget('li', 10, true);
                if (t){
                    var c = Ext.getCmp(t.id.split('x-err-')[1]);
                    if (c) {
                        c.focus();
                    }
                    this.hideErrors();
                }
            }, this, {stopEvent:true}); // prevent anchor click navigation
        }
        return this.msgEl;
    },

    // private
    showErrors : function(){
        this.updateErrorList();
        this.getMsgEl().alignTo(this.statusBar.getEl(), this.listAlign).slideIn('b', {duration:.3, easing:'easeOut'});
        this.statusBar.setText(this.hideText);
    },

    // private
    hideErrors : function(){
        var el = this.getMsgEl();
        if(el.isVisible()){
            el.slideOut('b', {duration:.2, easing:'easeIn'});
            this.statusBar.setText(this.showText);
        }
    },

    clearErrors : function() {
        if (this.statusBar && this.statusBar.statusEl) {
            this.hideErrors();
            this.errors.clear();
            this.updateStatus();
        }
    },

    addError : function(id, field, msg) {
        this.errors.add(id, {field:field, msg:msg});
        this.updateStatus();
    },

    // private
    onStatusClick : function(){
        if(this.getMsgEl().isVisible()){
            this.hideErrors();
        }
        else if(this.errors.getCount() > 0){
            this.showErrors();
        }
    },

    // private
    onDestroy : function(){
        this.stopMonitoring();
        this.statusBar.statusEl.un('click', this.onStatusClick, this);
        Ext.destroy(this.msgEl);
    }
};