/**
 * @class R.ui.Mask
 * Either masks an element or displays a wait message box.
 * @cfg {String} waitMsg (optional) The message to be displayed by the mask.
 * @cfg {Number} waitMsgDelay (optional) The amount of time (milliseconds)
 * to wait before displaying the mask.
 * @cfg {Mixed} waitMsgTarget (optional) By default wait messages are
 * displayed with Ext.MessageBox.wait. You can target a specific element by
 * passing it or its id or mask the form itself by passing in true.
 * @cfg {String} waitTitle (optional) The title to displayed by a call to
 * Ext.MessageBox.wait.
 */
R.ui.Mask = function(config) {
    this.waitMsg = config.waitMsg;
    this.waitMsgDelay = config.waitMsgDelay;
    this.waitMsgTarget = config.waitMsgTarget;
    this.waitTitle = config.waitTitle;
};

/**
 * Creates and shows a new mask.
 * @static
 * @return {Object} The mask if it was created.
 */
R.ui.Mask.show = function(config) {
    if (config && config.waitMsg) {
        var mask = new R.ui.Mask(config);
        mask.show();
        return mask;
    }
};

R.ui.Mask.prototype = {
    show : function() {
        var opts  = this;
        var run = function() {
            if (!this.started) {
                // Fixes a problem in IE. Occasionally after calling task.cancel
                // IE would still call this run function
                return;
            }

            if (opts.waitMsgTarget === true) {
                Ext.getBody().mask(opts.waitMsg, 'x-mask-loading');
            }
            else if (opts.waitMsgTarget) {
                opts.waitMsgTarget = Ext.get(opts.waitMsgTarget);
                opts.waitMsgTarget.mask(opts.waitMsg, 'x-mask-loading');
            }
            else {
                Ext.Msg.wait(opts.waitMsg, opts.waitTitle || opts.waitTitle || 'Please Wait...');
            }
        };

        this.started = true;

        if (this.waitMsgDelay) {
            this.task = new Ext.util.DelayedTask(run, this);
            this.task.delay(this.waitMsgDelay);
        }
        else {
            run.call(this);
        }
    }

    ,hide : function() {
        this.started = false;

        if (this.task) {
            this.task.cancel();
            delete this.task;
        }
        if (this.waitMsgTarget === true) {
            Ext.getBody().unmask();
        }
        else if (this.waitMsgTarget) {
            this.waitMsgTarget.unmask();
        }
        else {
            Ext.Msg.hide();
        }
    }
};
