/**
 * @class R.ui
 * @singleton
 * Initializes the R.ui namespace with various utility methods.
 */
Ext.apply(R.ui, {
    /**
     * Adds a stylized '*' to the returned value.
     */
    required : function(value) {
        return value + '<span class="r-required-field">*</span>';
    }

    /**
     * Shows a window with information about our application.
     */
    ,showAboutWindow : function() {
        var win = new R.ui.Dialog({
            plain       : true
            ,modal      : true
            ,bodyBorder : false
            ,buttons    : R.ui.Dialog.OK
            ,frame      : true
            ,resizable  : true
            ,header     : false
            ,layout     : {
                align : 'stretch'
                ,type : 'vbox'
            }
            ,title      : 'About'
            ,items      : [
                {
                    cls     : 'about-dialog-image'
                    ,width  : 320
                    ,height : 240
                    ,region : 'center'
                    ,xtype  : 'box'
                }
                ,{
                    cls     : 'about-dialog-text'
                    ,flex   : 1
                    ,html   : '<div style="padding-top: 6px;text-align: center;">' +
                        'Version ' + R.App.version.release +
                        '<br/>Build: ' + R.App.version.build +
                        '<br/>Copyright &copy; 2008-2016 RF Code, Inc.'
                    ,region : 'south'
                    ,xtype  : 'box'
                }
            ]
        });
        if (Ext.isIE) {
            win.setSize(332, 359);
        }
        else {
            win.setSize(334, 357);
        }
        win.show();
    }
});
