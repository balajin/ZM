/**
 * @class R.form.LoadAction
 * @extends Ext.form.Action.Load
 * Extends the load action to work with our JSON. Adds an optional transform
 * function to munge the action's options. The transform function is passed the
 * data returned from the load's Ajax request and returns a new data object to
 * be used by the form.
 */
R.form.LoadAction = Ext.extend(Ext.form.Action.Load, {

    // override
    run : function() {
        var o = this.options;

        if (o.requests) {
            R.Ajax.queue(Ext.apply(this.createCallback(o), {
                requests : o.requests
            }));
        }
        else {
            R.form.LoadAction.superclass.run.call(this);
        }
    }

    // override
    ,processResponse : function(response) {
        var requests = this.options.requests;
        var i, r, req;

        if (requests) {
            this.result = { data: {} };
            for (i = 0; i < response.length; i++) {
                req = requests[i];
                r = this.doSingleResponse(response[i], req.transform, req.isArray);
                this.result.success = r.success;
                if (!r.success) {
                    break;
                }
                Ext.apply(this.result.data, r.data);
            }
        }
        else {
            this.result = this.doSingleResponse(response);
        }
        return this.result;

    }

    // override
    ,doSingleResponse : function(response, transform, isArray) {
        var data = null, success = true;

        transform = transform || this.options.transform;
        isArray = isArray || this.options.isArray;

        if (this.form.reader) {
            var rs = this.form.reader.read(response);

            if (rs.records && rs.records[0]) {
                if (isArray) {
                    data = [];
                    for (var i = 0; i < rs.totalRecords; i++) {
                        data.push(rs.records[i].data);
                    }
                }
                else {
                    data = rs.records[0].data;
                }
            }
            success = rs.success;
        }
        else {
            data = Ext.decode(response.responseText);
        }
        if (typeof transform === 'function') {
            data = transform.call(this.options.scope, data);
        }
        // wrap JSON into format extjs expects
        return {
            success : success
            ,data   : data
        };
    }
});
Ext.form.Action.ACTION_TYPES['load'] = R.form.LoadAction;
