/**
 * @class R.grid.RowToolTipPlugin
 * A GridPanel plugin which displays a tooltip for the currently hovered row.
 * @cfg {Function} onTipRender An Ext.Updater.BasicRenderer render function. The scope
 * used to call the function contains a reference to the tooltip (this.tip).
 * @cfg {Function} onTipUpdate A function which returns a config object used
 * by Ext.Updater to update the tooltip contents.
 */
R.grid.RowToolTipPlugin = function(config) {
    Ext.apply(this, config);
};

R.grid.RowToolTipPlugin.prototype = {
    init : function(grid) {
        this.grid = grid;

        grid.on('render', this.onRender, this);
    }

    // private
    ,onRender : function() {
        this.tip = new Ext.ToolTip({
            autoHide   : false
            ,delegate  : '.x-grid3-row'
            ,layout    : 'fit'
            ,renderTo  : document.body
            ,showDelay : 1000
            ,target    : this.grid.getView().scroller
            ,width     : 420
            ,getTargetXY : this.getTargetXY
        });
        this.tip.getUpdater().setRenderer({ render: this.onTipRender, tip: this.tip });
        this.tip.on('beforeshow', this.onBeforeShow, this);

        this.grid.on('destroy', this.tip.destroy, this.tip);
        this.grid.store.on('update', this.onUpdate, this);

    }

    // private
    ,getTargetXY : function() {
        // tooltip anchored to top right corner of grid
        return this.target.getAnchorXY('t');
    }

    // private
    ,onBeforeShow : function() {
        var record = this.grid.store.getAt(this.tip.triggerElement.rowIndex);
        if (record) {
            var updater = this.tip.getUpdater();
            updater.abort();
            updater.update(this.onTipUpdate(record));
        }
    }

    // private
    ,onUpdate : function(store, record) {
        // store was updated, check if it affected the current tooltip
        var trigger = this.tip.triggerElement;
        if (trigger && this.tip.isVisible()) {
            var tipped = store.getAt(trigger.rowIndex);
            if (tipped && record.id === tipped.id) {
                this.onBeforeShow();
            }
        }
    }
};

