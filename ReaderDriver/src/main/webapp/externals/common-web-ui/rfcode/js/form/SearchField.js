/**
 * @class R.form.SearchField
 * @extends Ext.form.ComboBox
 * @cfg {Function} lookup (optional) An optional function which updates this
 * field's store with values looked up from the device.
 */
R.form.SearchField = Ext.extend(Ext.form.ComboBox, {
    trigger1Class  : 'x-form-clear-trigger'
    ,trigger2Class : 'x-form-search-trigger'

    ,enableKeyEvents : true

    ,initComponent : function() {
        Ext.apply(this, {
            triggerConfig : {
                tag:'span', cls:'x-form-twin-triggers', cn:[
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class},
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger2Class}
                ]
            }
            ,valueField : null
        });

        if (this.lookup) {
            var field = this;

            Ext.apply(this, {
                validationEvent : 'keyup'
                ,validateOnBlur : false
                ,validator      : function(value) {
                    if (value !== field.lastlookup && field.lookup) {
                        field.lookup.call(field, value);
                        field.lastlookup = value;
                    }
                    return true;
                }
            });

            this.initialLookup = this.lookup;
        }

        R.form.SearchField.superclass.initComponent.call(this);

        this.on('specialkey', function(f, e) {
            if (e.getKey() === e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);

        this.addEvents(
            /**
             * @event search
             * Fires when the search trigger is clicked or the user selects an item
             * @param {R.form.SearchField} field This search field
             * @param {Object} value The selected value
             */
            'search'
        );
    }

    ,getTrigger : Ext.form.TwinTriggerField.prototype.getTrigger

    ,initTrigger : Ext.form.TwinTriggerField.prototype.initTrigger

    ,onTrigger1Click : Ext.form.ComboBox.prototype.clearValue

    ,onTrigger2Click : function() {
        if (this.isExpanded()) {
            this.collapse();
        }
        this.el.focus();
        this.fireEvent('search', this, this.getRawValue());
    }

    ,onSelect : function() {
        // remove validator so we don't do a lookup on the selected value
        this.lookup = null;
        R.form.SearchField.superclass.onSelect.apply(this, arguments);
        this.onTrigger2Click();
        this.lookup = this.initialLookup;
    }

    ,setValue : function(v) {
        // 1. SearchField performs lookup
        // 2. lookup marks field invalid
        // 3. User moves to another field causing blur event
        // 4. blur event calls setValue and clearInvalid
        // We want to prevent clearInvalid in this case to keep field invalid
        if (v !== this.getValue()) {
            this.lookup = null;
            R.form.SearchField.superclass.setValue.call(this, v);
            this.lookup = this.initialLookup;
            this.lastlookup = v;
            this.store.removeAll();
        }
    }
});
