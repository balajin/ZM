// https://extjs.com/forum/showthread.php?t=6099&page=2
/**
 * @class R.form.ButtonField
 * @extends Ext.form.Field
 * A button which works properly as a form field.
 */
R.form.ButtonField = Ext.extend(Ext.form.Field, {
    defaultAutoCreate  : {
        tag: 'div'
    },

    // override
    initComponent : function() {
        R.form.ButtonField.superclass.initComponent.call(this);
        this.addEvents(
            /**
             * @event click
             * Fires when the ok button is clicked. Return false to stop the ok.
             * @param {R.form.ButtonField}
             */
            'click');
    }

    // override
    ,onRender: function (ct, position) {
         if (!this.el){
             var cfg = this.getAutoCreate();
             if (!cfg.name){
                 cfg.name = this.name || this.id;
             }
             if (this.inputType){
                 cfg.type = this.inputType;
             }
             this.el = ct.createChild(cfg, position);
         }

         this.button = new Ext.Button({
             cls       : this.cls || null
             ,disabled : this.disabled
             ,iconCls  : this.iconCls || null
             ,minWidth : this.minWidth
             ,renderTo : this.el
             ,text     : this.text
             ,scope    : this.scope || this
             ,handler  : this.handler || Ext.emptyFn
         });
         this.button.on('click', function() { this.fireEvent('click', this); }, this);
    },

    // override
    disable : function() {
         R.form.ButtonField.superclass.disable.call(this);
         if (this.button) {
             this.button.disable();
         }
    },

    // override
    enable : function() {
         R.form.ButtonField.superclass.enable.call(this);
         // ButtonField might not be rendered yet
         if (this.button) {
            this.button.enable();
         }
    },

    // override
    setText : function(text) {
        if (this.button) {
            this.button.setText(text);
        }
        this.text = text;
    },

    getValue : Ext.emptyFn,
    setValue : Ext.emptyFn,
    initValue : Ext.emptyFn
});
Ext.reg('r-buttonfield', R.form.ButtonField);
