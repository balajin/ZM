/**
 * @class R.ui.Window
 * @extends Ext.Window
 */
R.ui.Window = Ext.extend(Ext.Window, {
    cls : 'r-window'

    // override
    ,doLayout : function() {
        R.ui.Window.superclass.doLayout.apply(this, arguments);

        // ensure window is not larger than the viewport
        var orig = this.getSize();
        var size = {};
        var vw = Ext.lib.Dom.getViewportWidth();
        var vh = Ext.lib.Dom.getViewportHeight();

        if (orig.width > vw) {
            size.width = vw;
        }
        if (orig.height > vh) {
            size.height = vh;
        }
        if (orig.width != size.width || orig.height != size.height) {
            this.setSize(size);
        }
    }
});


