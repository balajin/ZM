/**
 * @class R.ui.TagListField
 * @extends Ext.form.TriggerField
 */
R.ui.TagListField = Ext.extend(Ext.form.TriggerField,  {

    /**
     * An additional CSS class used to style the trigger button.  The trigger will always get the
     * class 'x-form-trigger' and triggerClass will be <b>appended</b> if specified (defaults to 'x-form-date-trigger'
     * which displays a calendar icon).
     */
    triggerClass : 'x-form-win-trigger'

    // private
    ,defaultAutoCreate : { tag: 'textarea', rows: 4 }

    ,readOnly : true

    // private
    ,validateValue : function(value){
        if (value.length < 1 || value === this.emptyText){ // if it's blank
            if (this.allowBlank){
                this.clearInvalid();
                return true;
            }else{
                this.markInvalid(this.blankText);
                return false;
            }
        }
        return true;
    }

    ,getValue : function() {
        if (this.value) {
            if (typeof this.value == 'string') {
                return this.value;
            }
            else {
                var list = '';
                for (var i = 0; i < this.value.length;i++) {
                    list += this.value[i] + ',';
                }
                if (list.length > 0) {
                    list = list.substring(0, list.length - 1);//truncate trailing comma
                }
                return list;
            }
        }
        return [];
    }

    ,setValue : function(value) {
        if (this.rendered) {
            if (typeof value == 'string') {
                this.setRawValue(value.split(',').join('\n'));
            }
            else {
                var channel, labels;
                if (this.channels) {
                    labels = [];
                    for (var i = 0; i < value.length; i++) {
                        labels.push(v[i]);
                    }
                }
                else {
                    labels = value;
                }
                labels.sort(R.compare());
                this.setRawValue(labels.join('\n'));
            }
        }
        this.value = value;
    }

    // private
    ,onTriggerClick : function(e) {
        if (this.disabled){
            return;
        }

        var window = new R.ui.Dialog({
            title      : this.fieldLabel
            ,width     : 350
            ,height    : 350
            ,minWidth  : 300
            ,minHeight : 250
            ,layout    : 'fit'
            ,items     : {
                xtype         : 'taglistselector'
                ,id           : 'selector'
                ,initialValue : this.getValue()
            }
        });
        window.on('ok', function() {
            this.setValue(window.findById('selector').getValue());
        }, this);
        window.show();
    }
});

