/**
 * @class R.form.FormPanel
 * @extends Ext.form.FormPanel
 */
R.form.FormPanel = Ext.extend(Ext.form.FormPanel, {
    autoScroll : true

    ,buttonAlign : 'left'

    ,constructor : function(config) {
        R.form.FormPanel.superclass.constructor.call(this, Ext.apply({
            trackResetOnLoad : true
        }, config));
    }

    /**
     * Displays this form within a dialog.
     */
    ,showInDialog : function(config) {
        var win = new R.ui.Dialog(Ext.apply({
            layout : 'fit'
            ,items : this
        }, config));

        var status = this.status;

        // form events
        var failed = function(form, action) {
            if (status) {
                status.setText(action.result.errors[0].msg);
            }
        };
        var complete = function() {
            this.form.un('actionfailed', failed, this);
            win.close();
        };
        this.form.on('actioncomplete', complete, this, { single: true });
        this.form.on('actionfailed', failed, this);

        // window events
        win.on('ok', function() {
            var isValid = true;

            this.form.items.each(function(f) {
                if (!f.validate()) {
                    isValid = false;
                }
            });
            if (isValid) {
                this.submit();
            }
            return false;
        }, this);
        win.show();
    }
});

