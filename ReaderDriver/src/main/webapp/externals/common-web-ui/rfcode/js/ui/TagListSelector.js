/**
 * Module to allow a user to edit assets
 */
R.ui.TagListSelector = Ext.extend(Ext.Panel, {
    constructor: function(config) {
        this.tagField = new Ext.form.TextField();

        this.selectionsStore = new Ext.data.SimpleStore({
            fields: ['tagid']
        });

        this.deleteButton = new R.ui.DeleteButton( {
            handler     : this.handleDeleteButtonClick
            ,scope      : this
            ,disabled   : true
        } );

        this.toGrid = new Ext.grid.GridPanel({
            store           : this.selectionsStore
            ,autoLoadStore  : false
            ,autoScroll     : true
            ,cm             : new Ext.grid.ColumnModel( [
                {
                    dataIndex       : 'tagid'
                    ,header         : 'Tags'
                    ,id             : 'tagid'
                }
            ] )
            ,showPaging     : true
            ,frame          : true
            ,header         : true
            ,tbar           : [
                this.deleteButton
            ]
            ,autoExpandColumn : 'tagid'
            ,sm             : new Ext.grid.RowSelectionModel({singleSelect:false})
            ,showPaging     : false
            ,bbar           : [ this.deleteButton ]
            ,tbar           : [
                'Tag ID:'
                ,this.tagField
                ,' '
                ,new R.ui.NewButton({
                    text      : 'Add'
                    ,handler  : this.handleAddButtonClick
                    ,scope    : this
                    ,tooltip  : 'Add Tag To List'
                })
            ]
        });

        this.toGrid.getSelectionModel().on('selectionchange', function() {
            if (this.toGrid.getSelectionModel().getSelections().length > 0) {
                this.deleteButton.enable();
            }
            else {
                this.deleteButton.disable();
            }
        }, this);

        R.ui.TagListSelector.superclass.constructor.call(this, Ext.apply(config, {
            cls     : 'rf_form'
            ,layout : 'fit'
            ,items  : this.toGrid
        }));
        if (config.initialValue) {
            this.setValue(config.initialValue);
        }
    }

    ,handleAddButtonClick : function() {
        var value = this.tagField.getValue();
        if (value) {
            var alreadyAdded = false;
            this.toGrid.store.each( function(record) {
                if (record.data.tagid == value) {
                    alreadyAdded = true;
                    return(false);
                }
            }, this);
            if (!alreadyAdded) {
                var record = new Ext.data.Record( {
                    tagid        : value
                } );
                this.toGrid.store.add(record);
                this.tagField.setValue('');
            }
            else {
                Ext.Msg.show({
                    title     : 'Error'
                    ,msg      : 'Tag Entry Already Exists'
                    ,buttons  : Ext.MessageBox.OK
                    ,icon     : Ext.MessageBox.WARNING
                });
            }
        }
    }

    ,handleDeleteButtonClick : function() {
        var selectedRecords = this.toGrid.getSelectionModel().getSelections();
        for (var i = 0 ; i < selectedRecords.length ; i++) {
            this.toGrid.store.remove(selectedRecords[i]);
        }
    }

    ,setValue : function(values) {
        if (typeof values == 'string') {
            values = values.split(',');
        }
        var records = [];
        for (var i = 0; i < values.length; i++) {
            records.push(new Ext.data.Record( {
                tagid : values[i]
            }));
        }
        this.toGrid.store.add(records);
    }

    ,getSelectedValues : function() {
        var values = [];
        this.toGrid.store.each(function(record) {
            values.push({ tagid: record.data['tagid'], $aName: record.data['tagid'] });
        });
        return values;
    }

    /**
     * Return an array of the selected tag ids
     */
    ,getValue : function() {
        var list = '';
        var values = this.getSelectedValues();
        for (var i = 0; i < values.length; i++) {
            list += values[i].tagid + ',';
        }
        if (list.length > 0) {
            list = list.substring(0, list.length - 1);//truncate trailing comma
        }
        return list;
    }
});
Ext.reg('taglistselector', R.ui.TagListSelector);
