/**
 * @class R.Module
 * @cfg {String} id The unique id of this module.
 * @cfg {String} category The category of this module. Modules with the same
 * category are grouped together within the UI.
 * @cfg {String} label Label for displaying this module
 */
R.Module = Ext.extend(Ext.util.Observable, {
    constructor : function(config) {
        if (typeof config.label === 'string') {
            config._label = config.label;
            delete config.label;
        }
        Ext.apply(this, config);
        R.Module.superclass.constructor.call(this);

        this.addEvents(
            /**
             * @event refresh
             * Fires when the module has refreshed.
             * @param {R.Module}
             */
            'refresh'
        );

        if (this.plugins) {
            if (Ext.isArray(this.plugins)) {
                Ext.each(this.plugins, this.initPlugin, this);
            }
            else {
                this.plugins = [this.initPlugin(this.plugins)];
            }
        }
    }

    // private
    ,initPlugin : function(p) {
        p.init(this);
        return p;
    }

    ,label : function() {
        return this._label;
    }

    /**
     * Called the first time this module is selected.
     * @method
     */
    ,initialize : Ext.emptyFn

    /**
     * Return true if this module should be shown.
     * @return {Boolean} True if the module is allowed to be shown
     */
    ,isAllowed : function() {
        return true;
    }

    /**
     * Creates the UI for this module.
     * @method
     */
    ,createUI : function() {
        /**
         * The UI component representing this module as returned by onCreateUI
         * @property component
         */
        return (this.component = this.onCreateUI());
    }

    /**
     * Subclasses must implement this function. Returns the component
     * representing this module.
     * @method
     */
    ,onCreateUI : Ext.emptyFn

    /**
     * Called to refresh all the data for this module's component. Subclasses
     * must fire the refresh event when they are finished refreshing.
     * @param {Object} params (optional) Name/value pairs used as arguments to
     * the module. Each module handles its own parameters.
     * @method
     */
    ,refresh : function(params) {
        this.fireEvent('refresh', this);
    }

    /**
     * Called when the user has chosen to hide this module. The callback
     * function must be called with a single boolean parameter indicating
     * whether to proceed (true) with hiding the module or keep the current
     * module visible (false).
     * @param {Function} proceed The callback function
     */
    ,hide : function(proceed) {
        proceed(true);
    }

    /**
     * Called after this module has been hidden.
     */
    ,cleanup : function() {
        var key, obj;

        _.each(this.plugins, function(plugin) {
            if (plugin.onCleanup) {
                plugin.onCleanup();
            }
        });

        this.onCleanup();

        // destroy each component within the module
        for (key in this) {
            if (this.hasOwnProperty(key)) {
                obj = this[key];

                if (obj && typeof obj === 'object') {
                    if(typeof obj.destroy == 'function') {
                        obj.destroy();
                        delete this[key];
                    }
                    else if (obj.dom) {
                        obj.removeAllListeners();
                        obj.remove();
                        delete this[key];
                    }
                }
            }
        }
    }

    ,onCleanup : Ext.emptyFn

    /**
     * Returns a function which only calls the given function if this module
     * is still visible. Especially useful during a module refresh which
     * performs asynchronous calls.
     * @param {Function} fn The function to wrap
     */
    ,callback : function(fn) {
        return fn.createInterceptor(this.isVisible, this);
    }

    /**
     * Returns true if this module is visible.
     * @return {Boolean}
     */
    ,isVisible : function() {
        return !!(this.component && this.component.isVisible());
    }

    /**
     * Performs a console.dir on this module omitting function references and
     * various common properties (eg. category, events)
     */
    ,debug : function() {
        var ignore = R.toMap([ '_label', 'alias', 'category', 'checkPermission',
                             'events', 'feature', 'roles', 'stateId' ]);
        var p = {};
        for (var key in this) {
            if (this.hasOwnProperty(key) && typeof this[key] !== 'function' &&
                !ignore[key]) {
                p[key] = this[key];
            }
        }
        console.log(this.id, p);
    }

    ,toString : function() {
        return this.label();
    }
});

