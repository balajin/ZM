/**
 * @class R.ui.SingleAction
 * @extends R.ui.Action
 * An action which performs an update on a single item.
 */
R.ui.SingleAction = Ext.extend(R.ui.Action, {

    update : function() {
        this.setDisabled(this.sm.getCount() != 1);
    }

    ,getSelected : function() {
        return this.sm.getSelected();
    }
});

