/**
 * @class R.form.TextAreaWithButtons
 * @extends Ext.form.Field
 * A field containing a textarea with a row of buttons. The buttons generally
 * show a dialog which inserts text into the text area.
 * @param {Array} buttons An array of buttons to add to the button bar
 */
R.form.TextAreaWithButtons = Ext.extend(Ext.form.Field, {
    defaultAutoCreate : { tag: 'div' }

    ,cls : 'r-textarea-with-buttons'

    // don't show focus on this field
    ,focusClass : undefined

    ,hideLabel : true

    // override
    ,onRender : function(ct, position) {
        R.form.TextAreaWithButtons.superclass.onRender.call(this, ct, position);

        this.textarea = new Ext.form.TextArea({
            // -20 for error message, -28 for buttons
            anchor     : '-20 -28'
            ,disabled  : this.disabled
            ,region    : 'center'
            ,value     : this.value
            ,validator : this.validator
        });

        // need to save the selection before the textarea
        // is deactivated, otherwise insertAtCursor fails
        if (Ext.isIE) {
            this.textarea.on('render', function(c) {
                c.getEl().on('beforedeactivate', function() {
                    this.range = document.selection.createRange();
                }, this);
            }, this);
        }

        new Ext.Container({
            height    : 180
            ,layout   : 'anchor'
            ,renderTo : this.el
            ,items    : [
                this.textarea
                ,{
                    anchor        : '100%'
                    ,layout       : 'table'
                    ,layoutConfig : { columns: this.buttons.length }
                    ,region       : 'south'
                    ,xtype        : 'container'
                    ,items        : this.buttons
                }
            ]
        });
    }

    // override
    ,focus : function() {
        this.textarea.focus();
    }

    // override
    ,initValue : function() {
        this.originalValue = this.getValue();
    }

    // override
    ,getValue : function() {
        var value = this.value;
        if (this.textarea) {
            value = this.textarea.getValue();
            if (Ext.isIE) {
                // IE loves carriage returns, the dirty bastard!
                value = value.replace(/\r/g, '');
            }
        }
        return value;
    }

    // override
    ,setValue : function(value) {
        if (this.textarea) {
            this.originalValue = value;
            this.textarea.setValue(value);
        }
        else {
            this.value = value;
        }
    }

    // override
    ,isValid : function() {
        return true;
    }

    // override
    ,isDirty : function() {
        return String(this.getValue()) !== String(this.originalValue);
    }

    // override
    ,markInvalid : function(msg) {
        this.textarea.markInvalid(msg);
        this.fireEvent('invalid', this, msg);
    }

    // override
    ,clearInvalid : function() {
        this.textarea.clearInvalid();
        this.fireEvent('valid', this);
    }

    // override
    ,onEnable : function() {
        if (this.textarea) {
            this.textarea.enable();
        }
        R.form.TextAreaWithButtons.superclass.onEnable.call(this);
    }

    // override
    ,onDisable : function() {
        if (this.textarea) {
            this.textarea.disable();
        }
        R.form.TextAreaWithButtons.superclass.onDisable.call(this);
    }

    // private
    ,insertAtCursor : function(value) {
        var field = this.textarea.el.dom;
        //IE support
        if (document.selection) {
            field.focus();

            if (this.range) {
                this.range.text = value;
                this.range.select();
                delete this.range;
            }
        }
        //Mozilla/Firefox/Netscape 7+ support
        else if (field.selectionStart || field.selectionStart == '0') {
            field.focus();

            //Here we get the start and end points of the
            //selection. Then we create substrings up to the
            //start of the selection and from the end point
            //of the selection to the end of the field value.
            //Then we concatenate the first substring, value,
            //and the second substring to get the new value.
            var startPos = field.selectionStart;
            var endPos = field.selectionEnd;
            field.value = field.value.substring(0, startPos) + value + field.value.substring(endPos, field.value.length);
            field.setSelectionRange(endPos+value.length, endPos+value.length);
        }
        else {
            field.value += value;
        }
    }

    /**
     * Shows a dialog containing a grid panel.
     * @param {Object} config The window configuration object
     * @param {Ext.grid.GridPanel} grid The grid panel
     * @param {Function} transform Optional transform function which receives
     * the selected grid record as a parameter and returns the value to insert
     * into the textarea.
     */
    ,showDialog : function(config, grid, transform) {
        var win = new R.ui.Dialog(Ext.apply({
            layout       : 'fit'
            ,maximizable : true
            ,modal       : true
            ,items       : grid
        }, config));

        win.setOkButtonDisabled(true);

        grid.on('rowdblclick', win.ok, win);
        grid.on('rowclick', win.setOkButtonDisabled.createDelegate(win, [false]));

        win.on('ok', function() {
            var record = grid.getSelectionModel().getSelected();
            if (record) {
                var value = record.data.value;
                this.insertAtCursor(transform ? transform.call(this, value) : value);
            }
        }, this);

        win.show();
    }
});
