/**
 * @class R.form.PortField
 * A field for entering a socket port number.
 */
R.form.PortField = Ext.extend(Ext.ux.form.Spinner, {

    allowDecimals : false

    ,allowNegative : 0

    ,decimalPrecision : 0

    ,strategy : {
        maxValue  : 65535
        ,minValue : 0
        ,xtype    : 'number'
    }
});
Ext.reg('r-portfield', R.form.PortField);


