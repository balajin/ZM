/**
 * @class R.form.FilterTreePanelField
 * @extends R.tree.FilterTreePanel
 */
R.form.FilterTreePanelField = Ext.extend(R.tree.FilterTreePanel, {
    /**
     * @cfg {Boolean} allowBlank Specify <tt>false</tt> to validate that the value's length is > 0 (defaults to
     * <tt>true</tt>)
     */
    allowBlank : true

    /**
     * @cfg {String} blankText The error text to display if the <b><tt>{@link #allowBlank}</tt></b> validation
     * fails (defaults to <tt>'This field is required'</tt>)
     */
    ,blankText   : Ext.form.TextField.prototype.blankText

    ,isFormField : true
    ,msgTarget   : Ext.form.Field.prototype.msgTarget

    ,constructor : function(config) {
        R.form.FilterTreePanelField.superclass.constructor.call(this, config);

        this.getSelectionModel().on('selectionchange', function(sm) {
            var node = sm.getSelectedNode();
            this.value = (node && node.id);
            if (!Ext.isEmpty(this.value)) {
                this.clearInvalid();
            }
        }, this);
    }

    // override
    ,afterRender : function() {
        R.form.FilterTreePanelField.superclass.afterRender.call(this);

        this.setValue(this.value);
        this.originalValue = this.getValue();
    }

    // override
    ,getValue : function() {
        return this.value;
    }

    // override
    ,setValue : function(value) {
        if (value) {
            var node = this.getNodeById(value);
            if (node) {
                this.getSelectionModel().select(node);
            }
        }
        this.value = value;
    }

    // override
    ,getRawValue : function() {
        return this.value;
    }

    // override
    ,getErrors : function() {
        var errors = [];
        if (!this.allowBlank && Ext.isEmpty(this.value)) {
            errors.push(this.blankText);
        }
        return errors;
    }

    // Required by Ext.form.Field
    ,alignErrorIcon    : Ext.form.Field.prototype.alignErrorIcon
    ,clearInvalid      : Ext.form.Field.prototype.clearInvalid
    ,isDirty           : Ext.form.Field.prototype.isDirty
    ,isValid           : Ext.form.Field.prototype.isValid
    ,getErrorCt        : Ext.form.Field.prototype.getErrorCt
    ,getMessageHandler : Ext.form.Field.prototype.getMessageHandler
    ,getName           : Ext.form.Field.prototype.getName
    ,markInvalid       : Ext.form.Field.prototype.markInvalid
    ,processValue      : Ext.form.Field.prototype.processValue
    ,setActiveError    : Ext.form.Field.prototype.setActiveError
    ,unsetActiveError  : Ext.form.Field.prototype.unsetActiveError
    ,validate          : Ext.form.Field.prototype.validate
    ,validateValue     : Ext.form.Field.prototype.validateValue
});

