/**
 * A factory which creates UI components for entity attributes.
 * @class R.form.FieldFactory
 */
R.form.FieldFactory = function(config) {
    if (config) {
        Ext.apply(this, config);
    }

    /**
     * The map containing controllers based on the attribute type.
     * @private
     */
    this.type = {
        read: {}, write: {}, query: {}
    };

    /**
     * The map containing controllers based on the attribute guid. GUID based
     * controllers have precedence over type based controllers.
     * @private
     */
    this.guid = {
        read: {}, write: {}, query: {}
    };

    if (R.form.DefaultFieldFactory) {
        this.parent = R.form.DefaultFieldFactory;
    }
};

R.form.FieldFactory.prototype = {
    /**
     * The parent factory. Allows chaining factories. This factory is tried
     * before asking its parent for a controller. By default a factory's
     * parent is set to the default factory
     */
    parent : null

    /**
     * Registers a field by its attribute type (eg. 'enum'). The field is a
     * configuration object used to create a new FieldController.
     * @param {String} acType The attribute type
     * @param {String} type One of 'read', 'write', or 'query'
     * @param {R.form.Controller) controller The controller for the key
     */
    ,registerType : function(acType, type, controller) {
        this.assertType(type);
        this.type[type][acType] = controller;
    }

    /**
     * Registers a field by its attribute GUID (eg. '$aName'). The field is a
     * configuration object used to create a new FieldController.
     * @param {String} guid The attribute GUID. Or an array of attribute GUIDs
     * @param {String} type One of 'read', 'write', or 'query'
     * @param {R.form.Controller) controller The controller for the key
     */
    ,registerGuid : function(guid, type, controller) {
        var i;

        this.assertType(type);

        if (Ext.isArray(guid)) {
            for (i = 0; i < guid.length; i++) {
                this.guid[type][guid[i]] = controller;
            }
        }
        else {
            this.guid[type][guid] = controller;
        }
    }

    /**
     * Registers a read and write controller for an attribute class of type
     * enum. Useful to display different text for each enumerated value than
     * the default choice in the associated attribute class as determined by
     * the values property of the attribute class.
     * @param {String} guid The attribute GUID. Or an array of attribute GUIDs
     * @param {R.form.Controller) controller The controller for the guid
     * @param {Array} text Array of text corresponding to each enumerated value
     */
    ,registerEnum : function(guid, text) {
        this.registerGuid(guid, 'read', {
            onCreate : function(config) {
                return new R.form.DisplayField(Ext.apply({
                    toText : function(value) {
                        return Ext.isNumber(value) ? text[value] : value;
                    }
                }, config));
            }
        });

        this.registerGuid(guid, 'write', {
            onCreate : function(config) {
                var store = _.map(text, function(value, index) {
                    return [ index, value ];
                });

                return new Ext.form.ComboBox(Ext.apply({
                    editable       : false
                    ,lazyInit      : false
                    ,store         : store
                    ,mode          : 'local'
                    ,triggerAction : 'all'
                    ,value         : 0
                }, config));
            }
        });
    }

    /**
     * Returns a copy of the controller configuration associated with an
     * attribute of the given type.
     * @param {String} acType The attribute type
     * @param {String} type One of 'read', 'write', or 'query'
     */
    ,byType : function(acType, type) {
        this.assertType(type);

        var con = this.type[type][acType];
        if (con) {
            return Ext.apply({}, con);
        }
        return this.parent && this.parent.byType(acType, type);
    }

    /**
     * Returns a copy of the controller configuration associated with an
     * attribute of the given guid.
     * @param {String} guid The attribute guid
     * @param {String} type One of 'read', 'write', or 'query'
     */
    ,byGuid : function(guid, type) {
        this.assertType(type);

        var con = this.guid[type][guid];
        if (con) {
            return Ext.apply({}, con);
        }
        return this.parent && this.parent.byGuid(guid, type);
    }

    // private
    ,assertType : function(type) {
        if (type !== 'read' && type !== 'write' && type !== 'query') {
            throw new Error('FieldFactory function called with invalid type: ' + type);
        }
    }

    /**
     * Creates a controller for the given attribute class.
     * @param {R.AC} ac The attribute class
     * @param {String} type The type of controller (read, write or query)
     */
    ,create : function(ac, type) {
        var guid = ac.guid;
        var controller;

        type = type || 'write';

        this.assertType(type);

        // guid controller has higher precedence
        controller = this.guid[type][guid] || this.type[type][ac.type];

        if (type === 'query' && !controller) {
            // if no query default to the write type
            controller = this.guid['write'][guid] || this.type['write'][ac.type];
        }

        if (controller) {
            controller = new R.form.Controller(controller);
        }
        else if (this.parent) {
            // check parent factory
            controller = this.parent.create(ac, type);
        }
        if (!controller) {
            controller = new R.form.Controller(this.type[type === 'query' ? 'write' : type]['string']);
        }

        controller.ac = ac;
        controller.type = type;
        controller.factory = this;
        return controller;
    }
};

/**
 * The factory which contains all the default controllers for the builtin
 * attribute types.
 */
R.form.FieldFactory.DefaultFactory = function() {
    var factory = new R.form.FieldFactory();

    // bool
    factory.registerType('bool', 'write', {
        onCreate : function(config) {
            if (this.type === 'query' || (this.ac.values && this.ac.values.length > 0)) {
                var values = this.ac.values;
                var on = 'Yes';
                var off = 'No';

                if (values && values.length > 0) {
                    on = values[1];
                    off = values[0];
                }
                return new Ext.form.ComboBox(Ext.apply({
                    forceSelection : true
                    ,editable      : false
                    ,mode          : 'local'
                    ,selectOnFocus : true
                    ,store         : [ [ true, on ], [ false, off ] ]
                    ,triggerAction : 'all'
                }, config));
            }
            return new Ext.form.Checkbox(Ext.apply(config, {
                // Remove any anchor properties for the checkbox since they make
                // IE8 center the checkbox itself in the field
                anchor : null
            }));
        }

        ,onSetValue : function(value) {
            if (typeof value === 'string') {
                return (value === 'true');
            }
            return value;
        }
    });

    // bool - readonly
    factory.registerType('bool', 'read', {
        onSetValue : function(value) {
            var renderer = this.ac.renderer();

            if (renderer) {
                value = renderer(value);
            }
            else {
                if (value === true || value === 'true') {
                    value = 'Yes';
                }
                else if (value === false || value === 'false') {
                    value = 'No'
                }
                else {
                    value = null;
                }
            }
            return value;
        }
    });

    // date
    factory.registerType('date', 'write', {
        onCreate : function(config) {
            return R.ui.createDateField(config);
        }

        ,onSetValue : function(value) {
            if (Ext.isString(value)) {
                // possible when using conditional attribute fields
                value = parseInt(value, 10);
            }
            if (Ext.isNumber(value)) {
                value -= R.offsetFromUserTimeZone;
                value = new Date(value);
            }
            return value;
        }

        ,toJson : function(params) {
            var value = this.field.getValue();
            var date = null;

            if (value) {
                // Normalize the date value to the start of the day gmt.
                date = value.dateFormat('U') + value.dateFormat('Z');
                // Convert the date value to milliseconds
                date *= 1000;
                // Now make the time the midpoint of the day gmt.
                date += 43200000;
            }
            params[this.ac.guid] = date;
        }
    });

    // date - readonly
    factory.registerType('date', 'read', {
        onSetValue : function(value) {
            if (Ext.isString(value)) {
                // possible when using conditional attribute fields
                value = parseInt(value, 10);
            }
            if (value) {
                return R.utcToDateString(value);
            }
        }
    });

    // double
    factory.registerType('double', 'write', {
        onCreate : function(config, attributeType) {
            var ac = this.ac;
            var cons = ac.constraints;

            config.autoCreate = {
                tag         : 'input',
                type        : 'text',
                maxlength   : 15
            };
            if (cons) {
                if (Ext.isNumber(cons.min)) {
                    config.minValue = cons.min;
                    if (cons.min >= 0) {
                        config.allowNegative = false;
                    }
                }
                if (Ext.isNumber(cons.max)) {
                    config.maxValue = cons.max;
                }
            }

            if (ac.units) {
                config.plugins = new R.form.FieldRightLabelPlugin({
                    label : ac.units.label()
                });
            }

            if (ac.printf) {
                var precision = ac.printf.substring((ac.printf.indexOf('.') + 1), ac.printf.lastIndexOf('f'));
                config.decimalPrecision = precision;
                config.validator = function(value) {
                    // If allow negative, check if the value is just the negative
                    // sign.  If it is, allow.
                    if (config.allowNegative && value == '-') {
                        return true;
                    }
                    // compare the raw value with the cooked value.  If they
                    // differ, then update the field to show the cooked value
                    // so the user gets immediate feedback the field will
                    // return when the field returns the value.
                    if (value != this.getValue()) {
                        this.setValue(this.getValue());
                    }
                    return true;
                };
            }
            else {
                config.decimalPrecision = 10;
            }
            return new Ext.form.NumberField(config);
        }

        ,toJson : function(params) {
            var value = this.getUnconvertedValue();
            if (Ext.isNumber(value)) {
                value = this.ac.fromLocalValue(value);
            }
            params[this.ac.guid] = value;
        }

        ,getUnconvertedValue : function() {
            var raw = this.field.getRawValue();
            if (raw) {
                raw = raw.trim();
            }
            if (raw == null || raw.length == 0) {
                return null;
            }
            return this.field.getValue();
        }
    });

    // double - readonly
    factory.registerType('double', 'read', {
        onSetValue : function(value) {
            var ac = this.ac;
            var printf = ac.printf;

            if (Ext.isNumber(value)) {
                if (printf) {
                    value = value.toFixed(parseFloat(printf.substring(printf.indexOf('%.') + 2, printf.indexOf('f'))));
                }
                if (ac.units) {
                    value = value + ' ' + ac.units.shortlabel();
                }
            }
            return value;
        }
    });

    // enum - writable
    factory.registerType('enum', 'write', {
        onCreate : function(config) {
            var ac  = this.ac;
            var val = null;

            // If editing a new entity (entity does not exist),
            // then check if a default value should be supplied
            // to the field.
            if (!Ext.isDefined(config.entity)) {
                // Check if default value
                if (Ext.isDefined(config.defaultValue)) {
                    val = config.defaultValue;
                }
                // Check if value required, if it is, then
                // default to the first value
                else if (config.required) {
                    val = ac.values[0];
                }
            }
            return new Ext.form.ComboBox(Ext.apply(config, {
                editable       : false
                ,clearable     : !config.required
                ,lazyInit      : false
                ,store         : ac.values
                ,mode          : 'local'
                ,triggerAction : 'all'
                ,value         : val
            }));
        }

        ,onSetValue : function(value) {
            return this.ac.values[value];
        }

        ,toJson : function(params) {
            var val = this.ac.values.indexOf(this.field.getValue());
            if (val != -1) {
                params[this.ac.guid] = val;
            }
            else {
                params[this.ac.guid] = null;
            }
        }
    });

    // enum - readonly
    factory.registerType('enum', 'read', {
        onCreate : function(config) {
            return new Ext.form.DisplayField(config);
        },

        onSetValue : function(value) {
            if (!Ext.isEmpty(value)) {
                return this.ac.values[value];
            }
        }
    });

    // lat-lon-coord write
    factory.registerType('lat-lon-coord', 'write', {
        onCreate : function(config) {
            return new R.form.LatLonField(config);
        }
    });

    // lat-lon-coord
    factory.registerType('lat-lon-coord', 'read', {
        onCreate : function(config) {
            return new Ext.form.DisplayField(config);
        }

        ,onSetValue : function(value) {
            if (Ext.isArray(value) && value.length === 2) {
                value = R.AC.prototype.latLonValueToString(value[0], "N", "S") + ', ' + R.AC.prototype.latLonValueToString(value[1], "E", "W");
            }
            return value;
        }
    });

    // lat-lon-coord-list
    factory.registerType('lat-lon-coord-list', 'write', {
        onCreate : function(config) {
            return new R.form.LatLonListField(Ext.apply(config, {
                hideLabel : true
                ,width    : 410
                ,height   : 150
            }));
        }

        ,toJson : function(params) {
            var v = this.field.getValue();
            if (v) {
                params[this.ac.guid] = v;
            }
        }
    });

    // long
    factory.registerType('long', 'write', {
        onCreate : function(config) {
            var ac = this.ac;
            var min = ac.getMinConstraint();
            var field;

            if (ac.units) {
                config.plugins = new R.form.FieldRightLabelPlugin({
                    label : ac.units.label()
                });
            }

            field = new Ext.ux.form.Spinner(Ext.apply(config, {
                allowBlank : (config.required !== true)
                ,strategy  : {
                    decimalPrecision: 0
                    ,maxValue : ac.getMaxConstraint()
                    ,minValue : ac.getMinConstraint()
                    ,xtype    : 'number'
                }
            }));
            field.on('spin', function() {
                this.fireEvent('change', this, this.getValue(), null);
            }, field);

            return field;
        }

        ,toJson : function(params) {
            var value = this.getUnconvertedValue();
            if (Ext.isNumber(value)) {
                value = this.ac.fromLocalValue(value);
            }
            params[this.ac.guid] = value;
        }

        ,getUnconvertedValue : function() {
            var value = this.field.getRawValue();
            if (value.length > 0) {
                value = parseInt(this.field.getValue(), 10);
            }
            else {
                value = null;
            }
            return value;
        }
    });

    // long - readonly
    factory.registerType('long', 'read', {
        onSetValue : function(value) {
            var ac = this.ac;
            if (!Ext.isEmpty(value)) {
                value = value.toFixed(0);
                if (ac.units) {
                    value = value + ' ' + ac.units.shortlabel();
                }
            }
            return value;
        }
    });

    // password
    factory.registerType('password', 'write', {
        onCreate : function(config, attributeType) {
            return new R.form.PasswordField(config);
        }

        ,toJson : function(params) {
            var value = this.field.getValue();
            if (value === '') {
                params[this.ac.guid] = null;
            }
            else if (this.field.hasUserEnteredValue()) {
                params[this.ac.guid] = this.field.getValue();
            }
        }

        ,setValue : function(value) {
            if (Ext.isEmpty(value)) {
                this.field.setValue(value);
            }
            else {
                this.field.setHasValue();
            }
        }
    });

    // password - readonly
    factory.registerType('password', 'read', {
        onSetValue : function(value) {
            return '**********';
        }
    });

    // scoperef
    factory.registerType('scoperef', 'write', {
        onCreate : function(config) {
            // TODO: data drive in future with constraints on the attribute class
            var field = new R.form.TreeComboBox(Ext.apply({
                root : AM.tree.treeNodeForET({
                    guid: R.ET.LOCATION
                })
                ,showDelete : !config.required
            }, config));
            field.getValue = function() {
                var value = R.form.TreeComboBox.prototype.getValue.call(this);
                return value && (R.AC.LOCATION + '=' + value);
            }
            field.setValue = function(value) {
                if (value && value.indexOf('=') !== -1) {
                    value = value.substring(value.indexOf('=') + 1);
                }
                R.form.TreeComboBox.prototype.setValue.call(this, value);
            }
            return field;
        }
    });

    // string
    factory.registerType('string', 'write', R.form.Controller.toAttributeValue({
        onCreate : function(config) {
            var ac     = this.ac;
            var values = ac.values;
            var controller = this;
            var field;

            if (ac.hasSubType('macro')) {
                this.macros = this.factory.macros;
            }

            if (ac.hasSubType('attribute')) {
                // add spacing to group attribute/operator/value fields together
                if (ac.constraints && ac.constraints.operator_field) {
                    config.itemCls = 'r-form-item-conditional-attribute';
                }

                field = new RFCode.ui.AttributeClassCombo(Ext.apply({
                    clearable : !config.required
                }, config));
                field.load();
                return field;
            }
            else if (ac.hasSubType('operator')) {
                return new Ext.form.ComboBox(Ext.apply(config, {
                    clearable      : true
                    ,disabled      : true
                    ,displayField  : 'text'
                    ,editable      : false
                    ,mode          : 'local'
                    ,selectOnFocus : true
                    ,store         : new Ext.data.ArrayStore({
                        fields : ['name', 'value']
                        ,data  : []
                    })
                    ,triggerAction : 'all'
                    ,valueField    : 'value'
                }));
            }
            else if (ac.hasSubType('color')) {
                return new Ext.ux.form.ColorPickerField(config);
            }
            else if (ac.hasSubType('large')) {
                if (this.macros) {
                    return this.createLargeMacroField(config);
                }
                else {
                    config.anchor = '-20px';
                    config.hideLabel = true;
                    config.height = 120;
                    return new Ext.form.TextArea(config);
                }
            }
            else if (ac.hasSubType('schedule')) {
                return new RFCode.ui.CronField(config);
            }
            else if (ac.hasSubType('schedulemask')) {
                return new R.ui.ScheduleField(config);
            }
            else if (values && values.length > 0) {
                return new Ext.form.ComboBox(Ext.apply(config, {
                    mode          : 'local'
                   ,clearable     : !config.required
                   ,triggerAction : 'all'
                   ,editable      : true
                   ,selectOnFocus : true
                   ,store         : values
                   ,typeAhead     : true
               }));
            }
            else if (this.macros) {
                return this.createSmallMacroField(config);
            }

            return new Ext.form.TextField(config);
        }

        // override
        ,toJson : function(params) {
            var result = this.field.getValue();
            if (!result && this.ac.hasSubType('operator')) {
                result = null;
            }
            params[this.ac.guid] = result;
        }

        ,createSmallMacroField : function(config) {
            var controller = this;

            return new Ext.form.TriggerField(Ext.apply({
                triggerClass : 'x-form-win-trigger'

                ,onTriggerClick : function() {
                    controller.promptForMacro(function(value) {
                        var field = this.field;
                        field.setValue(field.getValue() + value);
                    });
                }
            }, config));
        }

        ,createLargeMacroField : function(config) {
            return new R.form.TextAreaWithButtons(Ext.apply({
                buttons : [
                    {
                        text     : 'Insert Macro'
                        ,xtype   : 'button'
                        ,scope   : this
                        ,handler : function() {
                            this.promptForMacro(function(value) {
                                this.field.insertAtCursor(value);
                            });
                        }
                    }
                ]
            }, config));
        }

        // scope is controller
        ,promptForMacro : function(callback) {
            var grid = {
                autoExpandColumn : 'desc'
                ,columns : [
                    { dataIndex: 'value', header: 'Macro', width: 150, sortable: true }
                    ,{ dataIndex: 'desc', header: 'Description', width: 300, sortable: true }
                ]
                ,sm : new Ext.grid.RowSelectionModel({singleSelect:true})
                ,store : new Ext.data.ArrayStore({
                    fields : [ 'value', 'desc' ]
                    ,data  : this.macros
                    ,sortInfo : { field: 'value' }
                })
                ,viewConfig : { forceFit: true }
            };

            var win = new R.grid.GridDialog({
                height : 300
                    ,title : 'Select Macro'
                    ,width : 600
                    ,gridConfig : grid
            });
            win.on('ok', function(win, record) {
                var value = record.data.value;
                if (value === 'SOURCE.attribute') {
                    this.promptForMacroSourceAttribute(callback);
                }
                else {
                    callback.call(this, '${' + value + '}');
                }
            }, this);
            win.show();
        }

        // scope is controller
        ,promptForMacroSourceAttribute : function(callback) {
            // create the data store
            var store = new Ext.data.JsonStore({
                data      : R.AC.filter(function(ac) {
                    return ac.isFilterable();
                })
                ,fields   : [ 'name', 'description', 'guid' ]
                ,sortInfo : { field: 'name', direction: 'ASC' }
            });

            var grid = {
                autoExpandColumn : 'desc'
                ,columns         : [
                    { dataIndex: 'name', header: 'Name', sortable: true }
                    ,{ dataIndex: 'description', header: 'Description', sortable: true }
                ]
                ,sm         : new Ext.grid.RowSelectionModel({singleSelect:true})
                ,store      : store
                ,viewConfig : {
                    forceFit : true
                }
            };

            var win = new R.grid.GridDialog({
                height : 500
                ,title : 'Select Attribute'
                ,width : 600
                ,gridConfig : grid
                ,tbar       : [
                    'Filter'
                    ,new R.ui.StoreFilterField({
                        filterField : 'name'
                        ,store      : store
                        ,width      : 200
                    })
                ]
            });
            win.on('ok', function(win, record) {
                callback.call(this, '${SOURCE.' + record.data.guid + '}');
            }, this);
            win.show();
        }
    }));

    // string - readonly
    factory.registerType('string', 'read', R.form.Controller.toAttributeValue({
        onCreate : function(config) {
            var ac = this.ac;

            if (ac.hasSubType('attribute')) {
                return new R.form.DisplayField(Ext.apply({
                    toText : function(value) {
                        var ac = R.AC.get(value);
                        return ac && ac.name;
                    }
                }, config));
            }
            else if (ac.hasSubType('operator')) {
                return new R.form.DisplayField(Ext.apply({
                    toText : function(value) {
                        var op = R.Op.get(value);
                        return op && op.text;
                    }
                }, config));
            }
            else if (ac.hasSubType('url')) {
                return new Ext.form.DisplayField(Ext.apply({
                    defaultAutoCreate : {
                        tag     : 'a'
                        ,href   : ''
                        ,html   : ''
                        ,target : '_blank'
                    }
                    ,listeners : {
                        render : function() {
                            this.setUrl(this.value);
                        }
                    }
                    ,setUrl : function(url) {
                        var dom;
                        if (url) {
                            dom = this.el.parent().child('a').dom;
                            dom.href = url;
                            dom.innerHTML = url;
                        }
                    }
                }, config));
            }
            return new Ext.form.DisplayField(config);
        }

        ,setValue : function(value) {
            R.form.Controller.prototype.setValue.call(this, value);

            if (this.ac.hasSubType('url') && this.field.rendered) {
                this.field.setUrl(value);
            }
        }
    }));

    // string-list
    factory.registerType('string-list', 'write', {
        onCreate : function(config) {
            var ac = this.ac;

            if (ac.hasSubType('attribute')) {
                return new R.ui.createAttributeListField(R.AC.filter(function(ac) {
                    return ac.isFilterable();
                }), config);
            }
            else if (ac.hasSubType('coord-list')) {
                return new Ext.form.TextArea(config);
            }
            return new R.form.ListField(Ext.apply(config, {
                store : this.ac.values
            }));
        }

        ,onSetValue : function(v) {
            if (this.ac.subtype === 'coord-list') {
                if (typeof v === 'string') {
                    v = v.replace(',', '\n');
                }
                else if (v) {
                    v = v.join('\n');
                }
            }
            return v;
        }

        ,toJson : function(params) {
            var value = this.field.getValue();
            if (this.ac.subtype === 'coord-list') {
                value = value.replace(/\r\n|\n/g, ',').replace(/,+/, ',');
            }
            params[this.ac.guid] = value;
        }
    });

    // string-list - readonly
    factory.registerType('string-list', 'read', {
        onSetValue : function(value) {
            var labels = [];
            var ac, i;

            if (value) {
                if (this.ac.hasSubType('attribute')) {
                    for (i = 0; i < value.length; i++) {
                        ac = R.AC.get(value[i]);
                        if (ac) {
                            labels.push(ac.name);
                        }
                    }
                }
                else {
                    labels = value;
                }
            }
            return labels.join(',');
        }
    });

    // timestamp write
    factory.registerType('timestamp', 'write', {
        onCreate : function(config) {
            return R.ui.createTimestampField(config);
        }

        ,onSetValue : function(value) {
            if (Ext.isString(value)) {
                // possible when using conditional attribute fields
                value = parseInt(value, 10);
            }
            if (Ext.isNumber(value)) {
                value -= R.offsetFromUserTimeZone;
                value = new Date(value);
            }
            return value;
        }

        ,toJson : function(params) {
            var v = this.field.getValue();
            v = v ? v.dateFormat('U') * 1000 : null;
            if (v) {
                v += R.offsetFromUserTimeZone;
            }
            params[this.ac.guid] = v;
        }
    });

    // timestamp read
    factory.registerType('timestamp', 'read', {
        onSetValue : function(value) {
            if (Ext.isString(value)) {
                // possible when using conditional attribute fields
                value = parseInt(value, 10);
            }
            if (value) {
                return R.utcToTimestampString(value);
            }
        }
    });

    // typeref
    factory.registerType('typeref', 'write', R.form.Controller.createTypeRef());

    // typeref - readonly
    factory.registerType('typeref', 'read', {
        onCreate : function(config) {
            return new R.form.DisplayField(Ext.apply({
                toText : this.toText
            }, config));
        }

        ,toText : function(value) {
            var et = R.ET.get(value);
            return (et ? et.name : value);
        }
    });

    // typeref-list write
    factory.registerType('typeref-list', 'write', {
        onCreate : function(config) {
            var con;

            this.operator = config.operator;

            if (config.operator === 'in') {
                con = this.factory.byType('typeref', 'write');
                return con.onCreate.call(this, config);
            }
            else {
                var ac   = this.ac;
                var et   = R.ET.get(ac.constraints.typeref);
                var opts = {
                   allowBlank     : true
                   ,selectOnFocus : true
                };
                // We have to protect against the pathogical case where the admin
                // has given access to an typeref-list attribute but not the root
                // type for that attribute for a non admin user.  If this is the
                // case, then simply create the control and set it disabled.
                if (et) {
                    opts.etGUID = ac.constraints.typeref;
                }
                else {
                    opts.disabled = true;
                }

                if (this.type === 'query') {
                    opts.rows = 1;
                }
                return new R.ui.EntityTypeListField(Ext.apply(config, opts));
            }
        }

        ,onSetValue : function(value) {
            // string when used in conditional attributes
            if (this.operator !== 'in' && Ext.isString(value)) {
                value = Ext.decode(value);
            }
            return value;
        }
    });

    // typeref-list read
    factory.registerType('typeref-list', 'read', {
        onSetValue : function(value) {
            // string when used in conditional attributes
            if (Ext.isString(value)) {
                value = Ext.decode(value);
            }
            if (value) {
                return R.ET.get(value).join(',');
            }
            return '';
        }
    });

    // typeref-list query
    factory.registerType('typeref-list', 'query', factory.byType('typeref-list', 'write'));

    // typeref-path - readonly
    factory.registerType('typeref-path', 'read', {
        onSetValue : function(value) {
            if (value) {
                return R.ET.get(value.split('|')).join(' / ');
            }
            return '';
        }
    });

    return factory;
}();

R.form.DefaultFieldFactory = R.form.FieldFactory.DefaultFactory;
