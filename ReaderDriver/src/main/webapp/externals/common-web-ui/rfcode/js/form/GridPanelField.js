/**
 * @class R.form.GridPanelField
 * @extends Ext.grid.GridPanel
 * A GridPanel which acts like a form field. It is up to subclasses to provide
 * getValue() and setValue() functions.
 * @cfg {Boolean} editable True to show buttons to edit the list (default: true)
 */
R.form.GridPanelField = Ext.extend(Ext.grid.GridPanel, {

    buttonAlign : 'left'

    ,isFormField : true

    ,msgTarget : 'side'

    ,constructor : function(config) {
        var editable = config.editable === false ? false : true;

        var plugins;
        if (editable) {
            var actions = config.actions;

            if (!actions) {
                actions = [
                    this.createAddAction()
                    ,this.createEditAction()
                    ,this.createDeleteAction()
                    ,this.createUpAction()
                    ,this.createDownAction()
                ];
                if (Ext.isDefined(config.sortID)) {
                    actions.push(this.createSortAscAction());
                    actions.push(this.createSortDescAction());
                }
            }
            plugins = [
                new R.grid.GridActions({
                    items : actions
                })
            ];
        }

        var anchor = editable ? '-20' : '-10'; // room for error icon
        if (!Ext.isEmpty(config.width)) {
            anchor = null;
        }
        R.form.GridPanelField.superclass.constructor.call(this, Ext.apply({
            anchor      : anchor
            ,editable   : editable
            ,height     : this.height || 120
            ,plugins    : plugins
            ,viewConfig : { forceFit: true }
        }, config));

        this.addEvents(
            /**
             * @event datachanged
             * Fires when the values in the grid have changed.
             * @param {R.ui.GridPanelField} this
             */
            'datachanged'
        );

        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    }

    // private
    ,onStoreChange : function() {
        this.fireEvent('datachanged', this);
    }

    // private
    ,afterRender : function() {
        R.form.GridPanelField.superclass.afterRender.call(this);
        this.initValue();
    }

    // Required by Ext.form.Field
    ,getName : Ext.form.Field.prototype.getName

    // Required by Ext.form.Field
    ,initValue : function() {
        this.originalValue = this.getValue();
    }

    // Required by Ext.form.Field
    ,isValid : function() {
        return true;
    }

    // Required by Ext.form.Field
    ,validate : function() {
        this.clearInvalid();
        return true;
    }

    ,resetOriginalValue : function() {
        this.originalValue = this.getValue();
    }

    // Required by Ext.form.Field
    ,reset : Ext.form.Field.prototype.reset
    ,markInvalid : Ext.form.Field.prototype.markInvalid
    ,getMessageHandler : Ext.form.Field.prototype.getMessageHandler
    ,setActiveError : Ext.form.Field.prototype.setActiveError

    // override
    ,clearInvalid : function() {
        // prevents the 'valid' event from getting fired if field is already valid
        if (this.el && this.el.hasClass(this.invalidClass)) {
            Ext.form.Field.prototype.clearInvalid.call(this);
        }
    }

    // Required by Ext.form.Field
    ,getErrorCt : Ext.form.Field.prototype.getErrorCt

    // Required by Ext.form.Field
    ,alignErrorIcon : Ext.form.Field.prototype.alignErrorIcon

    // Required by Ext.form.Field
    ,isDirty : Ext.form.Field.prototype.isDirty

    /**
     * Called when the add button is clicked. Default implementation is empty.
     * @method
     */
    ,onAddAction : Ext.emptyFn

    /**
     * Called when the edit button is clicked. Default implementation is empty.
     * @param {Ext.data.Record} record The record to edit
     */
    ,onEditAction : Ext.emptyFn

    /**
     * Called when the delete button is clicked. The default implementation
     * removes the selected records.
     * @param {Array} records Array of Ext.data.Record objects to delete
     */
    ,onDeleteAction : function(records) {
        Ext.each(records, this.store.remove, this.store);
    }

    // override
    ,onDisable : function() {
        this.setActionsDisabled(true);
    }

    // override
    ,onEnable : function() {
        this.setActionsDisabled(false);
    }

    // private
    ,setActionsDisabled : function(v) {
        if (this.plugins) {
            var items = this.plugins[0].mgr.items;
            for (var i = 0; i < items.length; i++) {
                items[i].disabled = v;
                items[i].update();
            }
        }
    }

    // private
    ,createAddAction : function() {
        return new R.ui.Action({
            button   : true
            ,cls     : 'button-new x-btn-text-icon'
            ,text    : 'Add'
            ,scope   : this
            ,handler : this.onAddAction
            ,update  : function() {
                this.setDisabled(this.disabled);
            }
        });
    }

    // private
    ,createEditAction : function() {
        return new R.ui.SingleAction({
            button   : true
            ,cls     : 'button-edit x-btn-text-icon'
            ,def     : true
            ,text    : 'Edit'
            ,scope   : this
            ,handler : function() {
                this.onEditAction(this.getSelectionModel().getSelected());
            }
            ,update  : function() {
                this.setDisabled(this.disabled || this.sm.getCount() !== 1);
            }
        });
    }

    // private
    ,createDeleteAction : function() {
        return new R.ui.MultiAction({
            button   : true
            ,cls     : 'button-delete x-btn-text-icon'
            ,text    : 'Delete'
            ,scope   : this
            ,handler : function() {
                this.onDeleteAction(this.getSelectionModel().getSelections());
            }
            ,update : function() {
                this.setDisabled(this.disabled || this.sm.getCount() < 1);
            }
        });
    }

    // private
    ,createUpAction : function() {
        var grid = this;
        return new R.ui.SingleAction({
            button   : true
            ,cls     : 'button-move-up x-btn-text-icon'
            ,text    : 'Up'
            ,scope   : this
            ,handler : function() {
                R.grid.up(this);
            }
            ,update : function() {
                var record = grid.getSelectionModel().getSelected();
                this.setDisabled(this.disabled || grid.store.indexOf(record) <= 0);
            }
        });
    }

    // private
    ,createDownAction : function() {
        var grid = this;
        return new R.ui.SingleAction({
            button   : true
            ,cls     : 'button-move-down x-btn-text-icon'
            ,text    : 'Down'
            ,scope   : this
            ,handler : function() {
                R.grid.down(this);
            }
            ,update : function() {
                var store  = grid.store;
                var record = grid.getSelectionModel().getSelected();
                var count  = store.getCount();

                this.setDisabled(this.disabled || !record || store.indexOf(record) === count-1);
            }
        });
    }

    ,createSortAscAction : function() {
        var grid = this;
        return new R.ui.SingleAction({
            button   : true
            ,cls     : 'button-move-down x-btn-text-icon'
            ,text    : 'Sort Asc'
            ,scope   : this
            ,handler : function() {
                if (grid.sortID) {
                    grid.store.sort(grid.sortID, 'ASC')
                }
            }
            ,update : function() {
                this.setDisabled(grid.store.getCount() < 2);
            }
        });
    }

    ,createSortDescAction : function() {
        var grid = this;
        return new R.ui.SingleAction({
            button   : true
            ,cls     : 'button-move-up x-btn-text-icon'
            ,text    : 'Sort Desc'
            ,scope   : this
            ,handler : function() {
                if (grid.sortID) {
                    grid.store.sort(grid.sortID, 'DESC')
                }
            }
            ,update : function() {
                this.setDisabled(grid.store.getCount() < 2);
            }
        });
    }
});
