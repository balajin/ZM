/**
 * @class R.module.TreeMasterDetailModule
 * @extends R.Module
 * A module which displays a tree of items (master) and a details panel to
 * add/edit items to the tree.
 * @cfg {String} deleteURL The URL used to delete a record
 * @cfg {String} nameField The underlying data field name to use for displaying
 * a representation of a record in the tree.
 */
R.module.TreeMasterDetailModule = Ext.extend(R.Module, {

    constructor : function(config) {
        R.module.TreeMasterDetailModule.superclass.constructor.call(this, config);

        this.addEvents(
            /**
             * @event selectionchange
             * Fired after the selected record changes
             * @param {Ext.data.Record} The selected record
             */
            'selectionchange'
        );

        /**
         * ID of the last selected node.
         * @property selection
         */
        this.selection = null;

        this.on('refresh', function() {
            if (this.selection) {
                this.select(this.selection);
            }
        }, this);
    }

    // override
    ,onCreateUI : function() {
        /**
         * The component representing the details
         * @type Ext.Component
         * @property detail
         */
        this.detail = this.createDetailComponent();
        this.detail.region = 'center';

        this.store = this.initStore();

        /**
         * The tree component
         * @type Ext.tree.TreePanel
         * @property tree
         */
        var selModel = new Ext.tree.DefaultSelectionModel();
        selModel.onNodeClick = this.onNodeClick;
        selModel.on('selectionchange', function(sm) {
            var node = sm.getSelectedNode();
            var record;

            if (node) {
                record = node.attributes.record;
                this.selection = node.id;
            }
            else {
                delete this.selection;
            }
            this.onSelectionChange(record);
            this.fireEvent('selectionchange', record);
        }, this);

        this.tree = new R.tree.FilterTreePanel({
            frame   : true
            ,header : true
            ,region : 'west'
            ,root   : new Ext.tree.TreeNode()
            ,rootVisible : false
            ,selModel : selModel
            ,split  : true
            ,width  : 220
        });

        // optional actions
        var actions = this.createActions();
        var plugins;
        if (actions) {
            plugins = new R.tree.TreeActions({
                tree   : this.tree
                ,items : actions
            });
        }

        return new Ext.Panel({
            layout   : 'border'
            ,items   : [ this.tree, this.detail ]
            ,plugins : plugins
        });
    }

    // private - used by the tree's selection model
    ,onNodeClick : function(node, e) {
        if (e.ctrlKey && this.isSelected(node)){
            this.unselect(node);
        }
        else {
            this.select(node);
        }
    }

    /**
     * Selects the tree node with the given ID.
     * @param {String} id The ID of the tree node to select
     */
    ,select : function(id) {
        var node = this.tree.getNodeById(id);
        if (node) {
            this.tree.expandPath(node.getPath());
            this.tree.getSelectionModel().select(node);
        }
    }

    // private
    ,initStore : function() {
        var store = this.createStore();
        store.on('load', this.onStoreLoad, this);
        store.on('clear', function() {
            this.tree.getRootNode().removeChildren();
        }, this);
        store.on('add', function(store, records) {
            var root = this.tree.getRootNode();
            Ext.each(records, this.appendChild, this);
        }, this);
        store.on('remove', function(store, record) {
            var node = this.tree.getNodeById(record.id);
            if (node) {
                node.remove();
            }
        }, this);
        return store;
    }

    /**
     * Called when the store loads.
     * @param {Ext.data.Store} store The store
     * @param {Ext.data.Record} records An array of records
     */
    ,onStoreLoad : function(store, records) {
        var tree     = this.tree;
        var sm       = tree.getSelectionModel();
        var root     = new Ext.tree.TreeNode();
        var selected = sm.getSelectedNode();
        var nodes    = {};

        var append = function(record) {
            var node = nodes[record.id];
            if (!node) {
                var pid = record.data.parent;
                var parent = pid ? append.call(this, store.getById(pid)) : root;

                node = new Ext.tree.TreeNode(this.onCreateNode({
                    id      : record.id
                    ,leaf   : false
                    ,text   : record.get(this.nameField)
                    ,record : record
                }));
                // call registerNode so tree.getNodeById works properly
                // even for collapsed nodes. Normally registerNode is not
                // called until the node is rendered
                tree.registerNode(node);
                nodes[record.id] = node;
                parent.appendChild(node);
            }
            return node;
        };
        store.each(append, this);

        tree.setRootNode(root);

        if (selected) {
            selected = tree.getNodeById(selected.id);
            if (selected) {
                sm.select(selected);
            }
        }
    }

    // private
    ,appendChild : function(record) {
        var tree  = this.tree;
        var store = this.store;
        var node, parent;

        if (record) {
            node = tree.getNodeById(record.id);
            if (node) {
                if (node.attributes.record != record) {
                    node.setText(record.get(this.nameField));
                    Ext.applyIf(record.data, node.attributes.record.data);
                    node.attributes.record = record;
                }
            }
            else {
                parent = this.appendChild(store.getById(record.data.parent));
                parent.appendChild(new Ext.tree.TreeNode(this.onCreateNode({
                    id      : record.id
                    ,leaf   : false
                    ,text   : record.get(this.nameField)
                    ,record : record
                })));
            }
            return node;
        }
        return tree.getRootNode();
    }

    /**
     * Returns the configuration object for creating a new tree node.
     * @param {Object} config The tree node config object
     */
    ,onCreateNode : function(config) {
        return config;
    }

    // override
    ,refresh : function() {
        var store = this.store;
        store.on('load', this.callback(function() {
            this.fireEvent('refresh', this);
        }), this, { single: true });
        store.load();
    }

    // override
    ,onCleanup : function() {
        Ext.destroy(this.store);
        delete this.store;
    }

    /**
     * Returns an array of action objects used by R.tree.TreeActions
     * @return {Array} actions Array of R.ui.Action objects
     */
    ,createActions : Ext.emptyFn

    /**
     * Returns a component used to display the details of the selected record.
     * @param {Object} config Configuration options
     * @return {Ext.Component}
     */
    ,createDetailComponent : Ext.emptyFn

    /**
     * Returns a store used by the grid.
     * @return {Ext.data.Store}
     */
    ,createStore : Ext.emptyFn

    /**
     * Called when the selected record changes
     * @param {Ext.data.Record} record The selected record or undefined if no
     * record selected.
     */
    ,onSelectionChange : Ext.emptyFn
});
