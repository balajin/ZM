/*
 * Ext.ux.grid.PageSizer
 *
 * Dynamically sets the PageSize config of a PagingToolbar
 *
 * Originally downloaded from extjs.com forums.  Modified for RF Code use.
 */

Ext.namespace("Ext.ux.grid");

Ext.ux.grid.PageSizer = function(config) {
    Ext.apply(this, config);
};

Ext.extend(Ext.ux.grid.PageSizer, Ext.util.Observable, {
    /**
     * @cfg {Array} sizes
     * An array of pageSize choices to populate the comboBox with
     */
    sizes : [ 10, 25, 50 ],

    /**
     * @cfg {String} beforeText
     * Text to display before the comboBox
     */
    beforeText : 'Rows:',

    /**
     * @cfg {Mixed} addBefore
     * Toolbar item(s) to add before the PageSizer
     */
    addBefore : '-',

    /**
     * Toolbar item(s) to be added after the PageSizer
     */
    addAfter : null,

    init : function(PagingToolbar) {
        this.PagingToolbar = PagingToolbar;

        PagingToolbar.store.on('load', function(store) {
            // update combo based on options passed to store.load method
            if (store.lastOptions && store.lastOptions.params) {

                var p     = store.lastOptions.params;
                var limit = p[store.paramNames.limit];

                for (var i = 0; i < this.sizes.length; i++) {
                    if (String(limit) === String(this.sizes[i])) {
                        this.combo.setValue(limit);
                        return;
                    }
                }

                if (this.sizes[0] === 'Fit') {
                    this.combo.setValue('Fit');
                }
            }
        }, this);

        PagingToolbar.on("render", this.onRender, this);

        PagingToolbar.addEvents(
            /**
             * Fired when the selected module changes.
             * @event selectionchange
             */
            'pagesizechanged'
        );
    },

    isFit : function() {
        var v = this.combo.getValue();
        return v === 'fit' || v === 'Fit';
    },

    onSelect : function(c) {
        var value = c.getValue();
        var tb = this.PagingToolbar;

        if (value == 'Fit' && tb.fitPageSize) {
            value = tb.fitPageSize;
        }
        else if (typeof value === 'string') {
            value = parseInt(value);
        }
        tb.pageSize = value;

        if (tb.fireEvent('pagesizechanged', tb, c.getValue()) !== false) {
            tb.doRefresh();
        }
    },

    onRender : function() {
        var config = {
            maskRe : /^d*$/,
            store : this.sizes,
            displayField : 'pageSize',
            typeAhead : true,
            mode : 'local',
            emptyText : this.pageSize,
            triggerAction : 'all',
            selectOnFocus : true,
            value : this.PagingToolbar.pageSize,
            width : 50
        };

        Ext.apply(config, this.comboCfg)
        this.combo = new Ext.form.ComboBox(config);
        this.combo.on("select", this.onSelect, this);

        if (this.addBefore) {
            this.PagingToolbar.add(this.addBefore)
        };

        this.PagingToolbar.add(this.beforeText, this.combo);

        if (this.addAfter) {
            this.PagingToolbar.add(this.addAfter)
        };

/*         this.combo.getEl().on('keydown', function(e) { */
/*                     var key = e.getKey();              */
/*                     switch (key) {                     */
/*                         case Ext.EventObject.ENTER :   */
/*                             this.onSelect(this.combo); */
/*                     }                                  */
/*                 }, this);                              */
    }
});



