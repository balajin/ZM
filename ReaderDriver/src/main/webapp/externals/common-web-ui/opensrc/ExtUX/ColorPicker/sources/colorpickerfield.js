/**
 * @class Ext.ux.form.ColorPickerField
 * @extends Ext.form.TriggerField
 * This class makes Ext.ux.ColorPicker available as a form field.
 * @license: BSD
 * @author: Robert B. Williams (extjs id: vtswingkid)
 * @author: Tobias Uhlig (extjs id: tobiu)
 * @author: Jerome Carbou (extjs id: jcarbou)
 * @constructor
 * Creates a new ColorPickerField
 * @param {Object} config Configuration options
 * @version 1.1.2
 */

Ext.namespace("Ext.ux.menu", "Ext.ux.form");

Ext.ux.menu.ColorMenu = Ext.extend(Ext.menu.Menu, {
    enableScrolling : false,
    hideOnClick     : true,
    initComponent : function(){
        Ext.apply(this, {
            plain         : true,
            showSeparator : false,
            items: this.picker = new Ext.ux.ColorPicker(this.initialConfig)
        });
        Ext.ux.menu.ColorMenu.superclass.initComponent.call(this);
        this.relayEvents(this.picker, ['select']);
        this.on('select', this.menuHide, this);
        if (this.handler) {
            this.on('select', this.handler, this.scope || this)
        }
    },
    menuHide: function(){
        if (this.hideOnClick) {
            this.hide(true);
        }
    }
});

Ext.ux.form.ColorPickerField = Ext.extend(Ext.form.TwinTriggerField,  {

    editMode : 'picker',

    initComponent : function(){
        this.editable = !this.hideHtmlCode;
        Ext.ux.form.ColorPickerField.superclass.initComponent.call(this);
        this.picker=-1;
        switch (this.editMode){
            case 'picker' :
                this.trigger1Class='x-form-colorfield-picker';
                this.triggerConfig = {
                    tag:'span', cls:'x-form-twin-triggers', cn:[
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class}
                ]};
                this.picker=0;
                break;
            case 'palette' :
                this.trigger1Class='x-form-colorfield-palette';
                this.triggerConfig = {
                    tag:'span', cls:'x-form-twin-triggers', cn:[
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class}
                ]};
                this.palette=0;
                break;
            default :
                this.trigger1Class='x-form-colorfield-palette';
                this.trigger2Class='x-form-colorfield-picker';
                this.triggerConfig = {
                    tag:'span', cls:'x-form-twin-triggers', cn:[
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger1Class},
                    {tag: "img", src: Ext.BLANK_IMAGE_URL, cls: "x-form-trigger " + this.trigger2Class}
                ]};
                this.palette=0;
                this.picker=1;
        }

        this.menus =[];
        if (this.palette>=0) {
            this.menus[this.palette] = new Ext.menu.ColorMenu({
            listeners : {
                select: function(m, c){
                    this.setValue('#' + c);
                    this.focus.defer(10, this);
                }
                ,scope : this
            }
            });
        }

        if (this.picker>=0) {
            this.menus[this.picker] = new Ext.ux.menu.ColorMenu({
                opacity:this.opacity,
                listeners : {
                    select: function(m, c){
                        c = c.replace('#', '');
                        this.setValue('#'+c.toUpperCase());
                        this.focus.defer(10, this);
                    }
                    ,scope : this
                }
            });
        }
    }

    /**
     * Get the color value.  If no color is selected (a zero length string),
     * return null.
     */
    ,getValue : function() {
        var value = Ext.ux.form.ColorPickerField.superclass.getValue.apply(this);
        if (value) {
            value = value.trim();
            if (value.length == 0) {
                value = null;
            }
            else {
                // Perform check to make sure the value is in the form '#XXXXXX'
                if (value.indexOf('#') != 0) {
                    value = '#' + value;
                }
                var temp = value;
                var temp = this.splitAphaRgbHex(temp.replace('#', ''));
                if (temp == null) {
                    value = null;
                }
            }
        }
        return value;
    }

    ,setValue : function(v){
        Ext.ux.form.ColorPickerField.superclass.setValue.apply(this, arguments);
        var hideStyle = this.hideHtmlCode ? 'cursor:default;' : '';
        if (v) {
            v = this.splitAphaRgbHex(v.replace('#', ''));
        }

        if (v) {
            var bg = v.rgbHex;
            var fg = this.rgbToHex(this.invert(this.hexToRgb(v.rgbHex)));
            hideStyle += 'color: #' + (this.hideHtmlCode ?  bg : fg) + ";";

            if (this.el) {
                this.el.applyStyles('background: #' + bg + ';'+hideStyle);
            }
        } else if (this.el) {
            this.el.applyStyles('background: #ffffff; color: #ffffff;'+hideStyle);
        }
    }

    ,onDestroy : function(){
        Ext.destroy(this.menus,this.wrap);
        Ext.ux.form.ColorPickerField.superclass.onDestroy.call(this);
    }
    ,onBlur : function(){
        Ext.ux.form.ColorPickerField.superclass.onBlur.call(this);
        this.setValue(this.getValue());
    }
    ,onTrigger1Click : function(){
        if(this.disabled){
            return;
        }
        this.menus[0].show(this.el, "tl-bl?");
        if (this.picker==0) {
            this.menus[0].picker.setColor(this.getValue());
        }
    }
    ,onTrigger2Click : function(){
        if(this.disabled){
            return;
        }
        this.menus[1].show(this.el, "tl-bl?");
        if (this.picker==1) {
            this.menus[1].picker.setColor(this.getValue());
        }
    }
    ,hexToRgb: Ext.ux.ColorPicker.prototype.hexToRgb
    ,rgbToHex: Ext.ux.ColorPicker.prototype.rgbToHex
    ,decToHex: Ext.ux.ColorPicker.prototype.decToHex
    ,hexToDec: Ext.ux.ColorPicker.prototype.hexToDec
    ,getHCharPos: Ext.ux.ColorPicker.prototype.getHCharPos
    ,invert: Ext.ux.ColorPicker.prototype.invert
    ,splitAphaRgbHex: Ext.ux.ColorPicker.prototype.splitAphaRgbHex
});
Ext.reg("colorpickerfield", Ext.ux.form.ColorPickerField);
