package com.rfcode.spm;

import java.util.Date;

import org.w3c.dom.Node;

public class CDUAction {
	// Name of the user who took the action
	public final String userName;
	// Time the action was taken
	public final Date time;
	// IP address of the user at time the action was taken
	public final String ipaddress;
	// Action descriptive text
	public final String message;
	// User provided reason for performing the action
	public final String reason;
	
	public CDUAction(Node n) {
		// Name of the user who took the action
		this.userName = SPMInterface.getStringFromNode(n, "userName", "");
		// Time the action was taken
		this.time = SPMInterface.getDateFromNode(n, "time", null);
		// IP address of the user at time the action was taken
		this.ipaddress = SPMInterface.getStringFromNode(n, "ipaddress", "");
		// Action descriptive text
		this.message = SPMInterface.getStringFromNode(n, "message", "");
		// User provided reason for performing the action
		this.reason = SPMInterface.getStringFromNode(n, "reason", "");
	}
}
