package com.rfcode.spm;

import org.w3c.dom.Node;

// Details for sensor (temperature) on CDU
public class CDUSensor {
	// ID of the sensor
	public final long id;
	// Name for sensor
	public final String name;
	// Generated name for sensor
	public final String absName;
	// User notes for sensor
	public final String notes;
	// ID of environmental monitoring sensor it is attached to
	public final long envMonId;
	// Sensor status (found, notFound, lost, noComm)
	public final String status;
	// Current temperature (C)
	public final double temp;
	// Temperature status (normal, notFound, reading, tempLow, tempHigh, readError, lost, noComm)
	public final String tempStatus;
	// Current humidity reading (%)
	public final double humid;
	// Humidity status (normal, notFound, reading, humidLow, humidHigh, readError, lost, noComm)
	public final String humidStatus;
	// Low temperature threshold (C)
	public final double tempLowThresh;
	// High temperature threshold (C)
	public final double tempHighThresh;
	// Low humidity threshold (%)
	public final double humidLowThresh;
	// High humidity threshold (%)
	public final double humidHighThresh;
	// Temperature scale (0=C, 1=F)
	public final int tempScale;
	
	public CDUSensor(Node n) {
		// ID of the sensor
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// Name for sensor
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Generated name for sensor
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User notes for sensor
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// ID of environmental monitoring sensor it is attached to
		this.envMonId = SPMInterface.getLongFromNode(n, "envMonId", -1);
		// Sensor status (normal, noComm)
		this.status = SPMInterface.getStringFromNode(n, "status", "");
		// Current temperature (C)
		this.temp = SPMInterface.getDoubleFromNode(n, "temp", Double.MIN_VALUE);
		// Temperature status (normal, notFound, reading, tempLow, tempHigh, readError, lost, noComm)
		this.tempStatus = SPMInterface.getStringFromNode(n, "tempStatus", "");
		// Current humidity reading (%)
		this.humid = SPMInterface.getDoubleFromNode(n, "humid", -1);
		// Humidity status (normal, notFound, reading, humidLow, humidHigh, readError, lost, noComm)
		this.humidStatus = SPMInterface.getStringFromNode(n, "humidStatus", "");
		// Low temperature threshold (C)
		this.tempLowThresh = SPMInterface.getDoubleFromNode(n, "tempLowThresh", Double.MIN_VALUE);
		// High temperature threshold (C)
		this.tempHighThresh = SPMInterface.getDoubleFromNode(n, "tempHighThresh", Double.MIN_VALUE);
		// Low humidity threshold (%)
		this.humidLowThresh = SPMInterface.getDoubleFromNode(n, "humidLowThresh", Double.MIN_VALUE);
		// High humidity threshold (%)
		this.humidHighThresh = SPMInterface.getDoubleFromNode(n, "humidHighThresh", Double.MIN_VALUE);
		// Temperature scale (0=C, 1=F)
		this.tempScale = SPMInterface.getIntFromNode(n, "tempScale", -1);
	}
}
