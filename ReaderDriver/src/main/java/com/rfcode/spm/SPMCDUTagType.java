package com.rfcode.spm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.PduSensorTagSubType;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.drivers.readers.mantis2.PduBreakerTagSubType;
import com.rfcode.drivers.readers.mantis2.PduFeedLineTagSubType;
import com.rfcode.drivers.readers.mantis2.PduOutletTagSubType;
import com.rfcode.drivers.readers.mantis2.PduPhaseTagSubType;
import com.rfcode.drivers.readers.mantis2.SubTag;
import com.rfcode.drivers.readers.mantis2.TagSubGroup;

public class SPMCDUTagType extends AbstractTagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "spmCDU";
    
    public static final String PDUDEFLABEL_ATTRIB = "defLabel";
    public static final String PDUDEFLABEL_ATTRIB_LABEL = "Default Label";
    
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID,
		PduOutletTagSubType.TAGTYPEID,
		PduFeedLineTagSubType.TAGTYPEID,
		PduSensorTagSubType.TAGTYPEID
	};

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
    	PDUDEFLABEL_ATTRIB,
    	MantisIITagType.PDUDISCONNECT_ATTRIB,
    	MantisIITagType.PDUMODEL_ATTRIB,
    	MantisIITagType.PDUSERIAL_ATTRIB,
    	MantisIITagType.PDUTRUEPOWER_ATTRIB,
    	MantisIITagType.PDUAPPPOWER_ATTRIB,
    	MantisIITagType.PDUPWRFACT_ATTRIB,
    	MantisIITagType.PDUWATTHOURS_ATTRIB,
    	MantisIITagType.PDUAPPPWRHOURS_ATTRIB,
    	MantisIITagType.PDUWATTHOURSTS_ATTRIB,
    	MantisIITagType.PDUSERIALLIST_ATTRIB,
    	MantisIITagType.PDUMODELLIST_ATTRIB,
    	MantisIITagType.PDUTOWERDISCONNECT_ATTRIB,
    	MantisIITagType.PDUDISCONNECTEDTOWERS_ATTRIB,
    	MantisIITagType.PDUTOWERCOUNT_ATTRIB
	};
    private static final String[] TAGATTRIBLABS = {
    	PDUDEFLABEL_ATTRIB_LABEL,
    	MantisIITagType.PDUDISCONNECT_ATTRIB_LABEL,
    	MantisIITagType.PDUMODEL_ATTRIB_LABEL,
    	MantisIITagType.PDUSERIAL_ATTRIB_LABEL,
    	MantisIITagType.PDUTRUEPOWER_ATTRIB_LABEL,
    	MantisIITagType.PDUAPPPOWER_ATTRIB_LABEL,
    	MantisIITagType.PDUPWRFACT_ATTRIB_LABEL,
    	MantisIITagType.PDUWATTHOURS_ATTRIB_LABEL,
    	MantisIITagType.PDUAPPPWRHOURS_ATTRIB_LABEL,
    	MantisIITagType.PDUWATTHOURSTS_ATTRIB_LABEL,
    	MantisIITagType.PDUSERIALLIST_ATTRIB_LABEL,
    	MantisIITagType.PDUMODELLIST_ATTRIB_LABEL,
    	MantisIITagType.PDUTOWERDISCONNECT_ATTRIB_LABEL,
    	MantisIITagType.PDUDISCONNECTEDTOWERS_ATTRIB_LABEL,
    	MantisIITagType.PDUTOWERCOUNT_ATTRIB_LABEL };

    private static final int PDUDEFLABEL_ATTRIB_INDEX = 0;
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1;
    private static final int PDUMODEL_ATTRIB_INDEX = 2;
    private static final int PDUSERIAL_ATTRIB_INDEX = 3;
	private static final int PDUTRUEPOWER_ATTRIB_INDEX = 4;
	private static final int PDUAPPPOWER_ATTRIB_INDEX = 5;
	private static final int PDUPWRFACTOR_ATTRIB_INDEX = 6;
	private static final int PDUWATTHOURS_ATTRIB_INDEX = 7;
	private static final int PDUAPPPWRHOURS_ATTRIB_INDEX = 8;
	private static final int PDUWATTHOURSTS_ATTRIB_INDEX = 9;
	private static final int PDUSERIALLIST_ATTRIB_INDEX = 10;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 11;
	private static final int PDUTOWERDISCONNECT_ATTRIB_INDEX = 12;
	private static final int PDUDISCONNECTEDTOWERS_ATTRIB_INDEX = 13;
	private static final int PDUTOWERCOUNT_ATTRIB_INDEX = 14;

    private static final double DEWP_A = 17.271;
    private static final double DEWP_B = 237.7;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        null, Boolean.FALSE, "", "", 
		null, null, null, null, null, null, null, null, Boolean.FALSE, 
		new ArrayList<Long>(), null };
	
	public SPMCDUTagType() {
        setID(TAGTYPEID); /* Set our tag type */
        setTagGroupAttributes(SPMCDUTagGroup.GROUPATTRIBLIST);
        setLabel("SPM Server CDU Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
	}

	@Override
	public int getVersion() {
		return 1;
	}

	@Override
	public boolean isSubType() {
		return false;
	}

	@Override
	public String[] getSubTypes() {
		return SUBTYPES;
	}

	@Override
	public TagGroup createTagGroup() {
		return new SPMCDUTagGroup(this);
	}

	SPMCDUTag makeTag(SPMCDUTagGroup grp, String guid) {
		return new SPMCDUTag(grp, guid);	
	}

	public void setTagOffline(SPMCDUTag spmcduTag) {
	}

	static class CDUState {
		long baseTotalEnergy = 0;
		long baseTotalEnergyTimestamp = 0;
		long lastTotalEnergy = 0;
		long appPowerAccum = 0;
		boolean isMetric;
		
		Map<String, CDUOutletState> outlets = new HashMap<String, CDUOutletState>();	// Outlets by absName
		Map<String, CDUInfeedState> infeeds = new HashMap<String, CDUInfeedState>();	// Infeeds by absname
		Map<String, CDUPhaseState> phases = new HashMap<String, CDUPhaseState>();	// Phases by absname
		Map<String, CDUSensorState> sensors = new HashMap<String, CDUSensorState>();	// Sensors by absname

        public void cleanup() {
            if (outlets != null) {
                for (CDUOutletState os : outlets.values()) {
    				if (os.tag != null) {    
    					os.tag.lockUnlockTag(false);	/* Unlock it */
                        os.tag = null;
				    }
                }
                outlets.clear();
                outlets = null;
            }
            if (infeeds != null) {
                for (CDUInfeedState is : infeeds.values()) {
    				if (is.tag != null) {    
    					is.tag.lockUnlockTag(false);	/* Unlock it */
                        is.tag = null;
				    }
                }
                infeeds.clear();
                infeeds = null;
            }
            if (phases != null) {
                for (CDUPhaseState ps : phases.values()) {
    				if (ps.tag != null) {    
    					ps.tag.lockUnlockTag(false);	/* Unlock it */
                        ps.tag = null;
				    }
                }
                phases.clear();
                phases = null;
            }
            if (sensors != null) {
                for (CDUSensorState ss : sensors.values()) {
    				if (ss.tag != null) {    
    					ss.tag.lockUnlockTag(false);	/* Unlock it */
                        ss.tag = null;
				    }
                }
                sensors.clear();
                sensors = null;
            }
        }                    
	};
	
	private static class CDUOutletState {
		SubTag tag;	// Tag for the outlet
		boolean marked;	// Marker - used to detect whether outlet was updated or not
		long baseTotalEnergy;	// Base watt hours
		long baseTotalEnergyTimestamp = 0;
		long lastTotalEnergy = 0;
		long appPowerAccum = 0;
	}
	
	private static class CDUInfeedState { 
		SubTag tag;	// Tag for the outlet
		boolean marked;	// Marker - used to detect whether outlet was updated or not
	}

	private static class CDUPhaseState { 
		SubTag tag;	// Tag for the outlet
		boolean marked;	// Marker - used to detect whether outlet was updated or not
	}

	private static class CDUSensorState { 
		SubTag tag;	// Tag for the outlet
		boolean marked;	// Marker - used to detect whether outlet was updated or not
	}

	private CDUTower getTower(CDUDetails cdu, long id) {
		for (int i = 0; i < cdu.tower.length; i++) {
			if (cdu.tower[i].id == id) {
				return  cdu.tower[i];
			}
		}
		return null;
	}

	private int getTowerIndex(CDUDetails cdu, CDUOutlet out) {
		for (int i = 0; i < cdu.tower.length; i++) {
			if (cdu.tower[i].id == out.towerId) {
				return i;
			}
		}
		return 0;
	}
	private int getTowerIndex(CDUDetails cdu, CDUInfeed feed) {
		for (int i = 0; i < cdu.tower.length; i++) {
			if (cdu.tower[i].id == feed.towerId) {
				return i;
			}
		}
		return 0;
	}

	// Get line index (not feed set)
	private int getFeedIndex(CDUDetails cdu, CDUTower tow, CDUOutlet out) {
		if (tow == null) return 0;
		for (int i = 0; i < cdu.infeed.length; i++) {
			if ((cdu.infeed[i].towerId == tow.id) && (cdu.infeed[i].id == out.infeedId)) {
				return i;
			}
		}
		return 0;
	}

	private static final String[] idxToLine = { "L1-N", "L2-N", "L3-N" };
	private String getPhaseID(CDUDetails cdu, CDUTower tow, CDUInfeed feed, CDUOutlet out) {
		if ((tow == null) || (!tow.threePhase())) {
			return "L1-N";
		}
		for (int i = 0; i < cdu.infeed.length; i++) {
			if (cdu.infeed[i].id == out.infeedId) {
				String s = cdu.infeed[i].lineToLineID;
				if (s.equals(""))
					s = cdu.infeed[i].phaseID;
				if (s.equals(""))
					s = cdu.infeed[i].lineID;
				if (s.equals("")) {
					int idx = 0;
					if (cdu.infeed[i].absName.length() > 1) {
						idx = (cdu.infeed[i].absName.charAt(1) - 'A');
					}
					if ((idx >= 0) && (idx < idxToLine.length)) {
						s = idxToLine[idx];
					}
				}
				int idx = s.indexOf(":");
				if (idx >= 0)
					s = s.substring(idx+1);
				return s;
			}
		}
		return "L1-N";
	}

	private String getPhaseID(CDUDetails cdu, CDUTower tow, CDUInfeed feed) {
		if ((tow == null) || (!tow.threePhase())) {
			return "L1-N";
		}
		String s = feed.lineToLineID;
		if (s.equals(""))
			s = feed.phaseID;
		if (s.equals(""))
			s = feed.lineID;
		if (s.equals("")) {
			int idx = 0;
			if (feed.absName.length() > 1) {
				idx = (feed.absName.charAt(1) - 'A');
			}
			if ((idx >= 0) && (idx < idxToLine.length)) {
				s = idxToLine[idx];
			}
		}
		int idx = s.indexOf(":");
		if (idx >= 0)
			s = s.substring(idx+1);
		return s;
	}

	private void processOutlets(SPMCDUTag tag, CDUState cdu, CDUDetails cdudet) {
		CDUOutlet[] outlets = cdudet.outlet;
		TagSubGroup tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
		for (int i = 0; i < outlets.length; i++) {
			CDUOutlet out = outlets[i];
			CDUOutletState state = cdu.outlets.get(out.absName);
			int towerIndex = getTowerIndex(cdudet, out);	// Get index of tower for outlet
			CDUTower tow = (towerIndex < cdudet.tower.length)?cdudet.tower[towerIndex]:null;
			int feedIndex = getFeedIndex(cdudet, tow, out);
			CDUInfeed feed = cdudet.infeed[feedIndex];
			String phaseID = getPhaseID(cdudet, tow, feed, out);
			if (state == null) {	// New?
				state = new CDUOutletState();
				cdu.outlets.put(out.absName, state);
				state.tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("outlet%s", out.absName), false);
				state.tag.lockUnlockTag(true);
			}
			Double volts = (out.voltage >= 0) ? out.voltage : null;
			Double amps = (out.load >= 0) ? out.load : null;
			Double truepower = null;
			Double powfact = null;
			Double apppower = null;
			Double tot_wh = null;
			Double tot_vah = null;
			Long wh_ts = null;
			if (out.powerSense()) {
				truepower = (double)out.power;
				powfact = 100.0 * out.powerFactor;
				apppower = (double)out.apparentPower;
				if ((state.baseTotalEnergyTimestamp == 0) || (out.wattHours < state.baseTotalEnergy)) {
					state.baseTotalEnergy = state.lastTotalEnergy = out.wattHours;
					state.baseTotalEnergyTimestamp = System.currentTimeMillis();
					state.appPowerAccum = 0;
				}
				wh_ts = state.baseTotalEnergyTimestamp;
				tot_wh = (double)(out.wattHours - state.baseTotalEnergy) * 1000.0;
				if (out.powerFactor > 0.0) {
					state.appPowerAccum += Math.round((out.wattHours - state.lastTotalEnergy) / out.powerFactor);
					tot_vah = (double) state.appPowerAccum;
				}
				state.lastTotalEnergy = out.wattHours;
			}
			Long towerid = (long)(towerIndex + 1);
			String outlabel = out.absName;
			Long feedsetid = Long.valueOf(1);	// One per tower (need to see if this is true)
			Boolean sw = null;
			if (out.powerControl()) {
				sw = (out.status.indexOf("on") >= 0);
			}
			Integer bankid = null;
			// Update values
			PduOutletTagSubType.updateTag(state.tag, volts, truepower, powfact, apppower, amps, phaseID, tot_wh, wh_ts, 
					tot_vah, towerid, outlabel, feedsetid, sw, bankid);

			state.marked = true;
		}
		
		// Make pass to clean up unmarked (removed) outlets */
		ArrayList<String> to_delete = null;
		for (String k : cdu.outlets.keySet()) {
			CDUOutletState state = cdu.outlets.get(k);
			if (state.marked == false) {
				if (to_delete == null) {
					to_delete = new ArrayList<String>();
				}
				to_delete.add(k);
			}
			state.marked = false;
		}
		if (to_delete != null) {
			for (String k : to_delete) {
				CDUOutletState state = cdu.outlets.remove(k);
				if (state.tag != null) {
					state.tag.lockUnlockTag(false);	/* Unlock it */
					state.tag = null;
				}
			}
		}
	}
	
	private void processInfeeds(SPMCDUTag tag, CDUState cdu, CDUDetails cdudet) {
		CDUInfeed[] infeeds = cdudet.infeed;
		TagSubGroup tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduFeedLineTagSubType.TAGTYPEID);
		for (int i = 0; i < infeeds.length; i++) {
			CDUInfeed feed = infeeds[i];
			CDUInfeedState state = cdu.infeeds.get(feed.absName);
			if (state == null) {	// Not defined yet?
				state = new CDUInfeedState();
				cdu.infeeds.put(feed.absName, state);
				state.tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("feedline%s", feed.absName), false);
				state.tag.lockUnlockTag(true);
			}
			state.marked = true;
			Double amps = (feed.load >= 0)?feed.load:null;	// Get amps, if available
			String config = null;
			Long towerindex = (long) getTowerIndex(cdudet, feed) + 1;
			Boolean overload = (feed.loadSense()?feed.loadStatus.equalsIgnoreCase("overload"):null);
			Boolean loadwarn = (feed.loadSense()?feed.loadStatus.equalsIgnoreCase("loadHigh"):null);;
			Long feedsetid = 1L;	// Only one, for now
			// Update values
			PduFeedLineTagSubType.updateTag(state.tag, amps, config, towerindex, overload, loadwarn, feedsetid);
		}

		ArrayList<String> to_delete = null;
		for (String k : cdu.infeeds.keySet()) {
			CDUInfeedState state = cdu.infeeds.get(k);
			if (state.marked == false) {
				if (to_delete == null) {
					to_delete = new ArrayList<String>();
				}
				to_delete.add(k);
			}
			state.marked = false;
		}
		if (to_delete != null) {
			for (String k : to_delete) {
				CDUInfeedState state = cdu.infeeds.remove(k);
				if (state.tag != null) {
					state.tag.lockUnlockTag(false);	/* Unlock it */
					state.tag = null;
				}
			}
		}
	}

	private void processPhases(SPMCDUTag tag, CDUState cdu, CDUDetails cdudet) {
		CDUInfeed[] infeeds = cdudet.infeed;
		TagSubGroup tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
		Double tottruepower = 0.0;
		Double totapppower = 0.0;
		for (int i = 0; i < infeeds.length; i++) {
			CDUInfeed feed = infeeds[i];
			CDUPhaseState state = cdu.phases.get(feed.absName);
			String config = getPhaseID(cdudet, getTower(cdudet, feed.towerId), feed);
			if (config.equals("N")) {	// Not a phase
				continue;
			}
			if (state == null) {	// Not defined yet?
				state = new CDUPhaseState();
				cdu.phases.put(feed.absName, state);
				state.tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("phase%s", feed.absName), false);
				state.tag.lockUnlockTag(true);
			}
			state.marked = true;
			Double voltsn = null;
			Double voltsp = null;
			if (feed.lineToLineID.equals("")) { // Not Lx-Ly
				voltsn = (feed.voltage >= 0)?feed.voltage:null;
			}
			else {
				voltsp = (feed.voltage >= 0)?feed.voltage:null;
				voltsn = (feed.phaseVoltage >= 0)?(double)feed.phaseVoltage:null;
			}
			Double truepower = (feed.power >= 0)?(double)feed.power:null;
			Double powfact = (feed.powerFactor >= 0)?(double)feed.powerFactor * 100.0:null;
			Double apppower = (feed.apparentPower >= 0)?(double)feed.apparentPower:null;
			Double amps = (feed.load >= 0)?feed.load:null;
			
			if ((apppower == null) && (amps != null) && (voltsp != null)) {
				apppower = amps * voltsp;
			}
			if ((apppower == null) && (amps != null) && (voltsn != null)) {
				apppower = amps * voltsn;
			}
			if ((powfact == null) && (apppower != null) && (truepower != null)) {
				if (apppower > 0) {
					powfact = 100.0 * truepower / apppower;
				}
			}
			/* Accumulate total true power, if we have it from all phases */
			if (truepower == null) {
				tottruepower = null;
			}
			else if (tottruepower != null) {
				tottruepower = tottruepower + truepower;
			}
			/* Accumulate total apparent power, if we have it from all phases */
			if (apppower == null) {
				totapppower = null;
			}
			else if (totapppower != null) {
				totapppower = totapppower + apppower;
			}
			
			Double tot_wh = null;
			Long wh_ts = null;
			Double tot_vah = null;
			Long towerindex = (long) getTowerIndex(cdudet, feed) + 1;
			Long feedsetid = 1L;	// Only one, for now
			// Update values
			PduPhaseTagSubType.updateTag(state.tag, voltsn, voltsp, truepower, powfact, apppower, amps, tot_wh, wh_ts, tot_vah, config, towerindex, feedsetid);
		}

		ArrayList<String> to_delete = null;
		for (String k : cdu.phases.keySet()) {
			CDUPhaseState state = cdu.phases.get(k);
			if (state.marked == false) {
				if (to_delete == null) {
					to_delete = new ArrayList<String>();
				}
				to_delete.add(k);
			}
			state.marked = false;
		}
		if (to_delete != null) {
			for (String k : to_delete) {
				CDUPhaseState state = cdu.phases.remove(k);
				if (state.tag != null) {
					state.tag.lockUnlockTag(false);	/* Unlock it */
					state.tag = null;
				}
			}
		}
		cdudet.totalphasepower = tottruepower;
		cdudet.totalphaseapppower = totapppower;
	}
	
	private void processSensors(SPMCDUTag tag, CDUState cdu, CDUDetails cdudet) {
		CDUSensor[] sensors = cdudet.sensor;
		TagSubGroup tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduSensorTagSubType.TAGTYPEID);
		for (int i = 0; i < sensors.length; i++) {
			CDUSensor sensor = sensors[i];
			CDUSensorState state = cdu.sensors.get(sensor.absName);
			if (state == null) {	// Not defined yet?
				state = new CDUSensorState();
				cdu.sensors.put(sensor.absName, state);
				state.tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("sensor%s", sensor.absName), false);
				state.tag.lockUnlockTag(true);
			}
			state.marked = true;
			Double temp = null;
			if (sensor.tempStatus.equals("notFound") || sensor.tempStatus.equals("none")) {
				// No value
			}
			else if (!cdu.isMetric)
				temp = Math.round(10.0 * (sensor.temp - 32.0) / 1.8) / 10.0;
			else
				temp = sensor.temp;
			Double humid = null;
			Double dew = null;
			if (sensor.humidStatus.equals("notFound") || sensor.humidStatus.equals("none")) {
				
			}
			else if (sensor.humid >= 0) {
				humid = Double.valueOf(sensor.humid);
				dew = (DEWP_A * temp / (DEWP_B + temp)) + Math.log(humid / 100.0);
				dew = DEWP_B * dew / (DEWP_A - dew);
				/* Apply rounding to dew point */
				dew = Math.rint(dew * 10.0) * 0.1;
			}
			// Update values
			PduSensorTagSubType.updateTag(state.tag, temp, humid, dew);
		}

		ArrayList<String> to_delete = null;
		for (String k : cdu.sensors.keySet()) {
			CDUSensorState state = cdu.sensors.get(k);
			if (state.marked == false) {
				if (to_delete == null) {
					to_delete = new ArrayList<String>();
				}
				to_delete.add(k);
			}
			state.marked = false;
		}
		if (to_delete != null) {
			for (String k : to_delete) {
				CDUSensorState state = cdu.sensors.remove(k);
				if (state.tag != null) {
					state.tag.lockUnlockTag(false);	/* Unlock it */
					state.tag = null;
				}
			}
		}
	}

	boolean processUpdate(SPMCDUTag tag, CDUDetails cdu, boolean isMetric) {
		boolean update = false;
		CDUState state = tag.state;
		if (state == null) {
			state = tag.state = new CDUState(); 
		}
		tag.state.isMetric = isMetric;
		// Tower data
		String model = "";
		String serial = "";
		List<String> modlst = new ArrayList<String>();
		List<String> serlst = new ArrayList<String>();
		long totalEnergy = 0;
		for (int i = 0; i < cdu.tower.length; i++) {
			serlst.add(cdu.tower[i].serial);
			modlst.add(cdu.tower[i].model);
			totalEnergy += cdu.tower[i].totalEnergy;
			if (model.equals("")) {
				model = cdu.tower[i].model;
			}
			if (serial.equals("")) {
				serial = cdu.tower[i].serial;
			}
		}
		if (serial.equals("")) {
			serial = cdu.serial;
		}
		update |= SPMCDUTagType.updateString(tag, PDUDEFLABEL_ATTRIB_INDEX, cdu.name);
		update |= SPMCDUTagType.updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, cdu.status.equals("unreachable"));
		// Update model
		update |= SPMCDUTagType.updateString(tag, PDUMODEL_ATTRIB_INDEX, (cdu.tower.length > 0) ? cdu.tower[0].model : "");
		update |= SPMCDUTagType.updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, modlst);
		// Update serial
		update |= SPMCDUTagType.updateString(tag, PDUSERIAL_ATTRIB_INDEX, (cdu.tower.length > 0) ? cdu.tower[0].serial : "");
		update |= SPMCDUTagType.updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, serlst);
		// Add tower count
		update |= SPMCDUTagType.updateInt(tag, PDUTOWERCOUNT_ATTRIB_INDEX, cdu.tower.length);
		// Add total power
		update |= SPMCDUTagType.updateInt(tag, PDUTRUEPOWER_ATTRIB_INDEX, cdu.totalPower);
		// Add overall power factor
		if (cdu.powerFactor >= 0.0) {
			update |= SPMCDUTagType.updateDouble(tag, PDUPWRFACTOR_ATTRIB_INDEX, 100.0 * cdu.powerFactor);
		}
		else {
			update |= SPMCDUTagType.updateNull(tag, PDUPWRFACTOR_ATTRIB_INDEX);
		}
		Double apppower = null;
		if (cdu.powerFactor > 0.0) {
			apppower = (double) Math.round(cdu.totalPower / cdu.powerFactor);
			update |= SPMCDUTagType.updateDouble(tag, PDUAPPPOWER_ATTRIB_INDEX, apppower);
		}
		else {
			update |= SPMCDUTagType.updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX);
		}
		// Check total energy - see if first value (or decrease) for reset
		if (totalEnergy > 0) {		
			if ((state.baseTotalEnergyTimestamp == 0) || (totalEnergy < state.baseTotalEnergy)) {
				state.baseTotalEnergy = state.lastTotalEnergy = totalEnergy;
				state.baseTotalEnergyTimestamp = System.currentTimeMillis();
				state.appPowerAccum = 0;
			}
			if (cdu.powerFactor > 0.0) {
				state.appPowerAccum += Math.round((totalEnergy - state.lastTotalEnergy) / cdu.powerFactor);
			}
			update |= SPMCDUTagType.updateDouble(tag, PDUWATTHOURS_ATTRIB_INDEX, (totalEnergy - state.baseTotalEnergy) * 1000.0);
			update |= SPMCDUTagType.updateLong(tag, PDUWATTHOURSTS_ATTRIB_INDEX, state.baseTotalEnergyTimestamp);
			update |= SPMCDUTagType.updateDouble(tag, PDUAPPPWRHOURS_ATTRIB_INDEX, state.appPowerAccum * 1000.0);			
			state.lastTotalEnergy = totalEnergy;
		}
		else {
			update |= SPMCDUTagType.updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX);
			update |= SPMCDUTagType.updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX);
			update |= SPMCDUTagType.updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX);
			state.baseTotalEnergyTimestamp = 0;
			state.baseTotalEnergy = 0;
			state.appPowerAccum = 0;
		}
		processOutlets(tag, state, cdu);
		processInfeeds(tag, state, cdu);
		processPhases(tag, state, cdu);
		processSensors(tag, state, cdu);

		// Update total power, if phase total is higher
		if ((cdu.totalphasepower != null) && (cdu.totalPower < cdu.totalphasepower)) {
			update |= SPMCDUTagType.updateInt(tag, PDUTRUEPOWER_ATTRIB_INDEX, cdu.totalphasepower.intValue());
		}
		// Update total apparent power, if phase total is higher
		if ((cdu.totalphaseapppower != null) && ((apppower == null) || (apppower < cdu.totalphaseapppower))) {
			update |= SPMCDUTagType.updateDouble(tag, PDUAPPPOWER_ATTRIB_INDEX, cdu.totalphaseapppower);
			if ((cdu.totalphasepower != null) && (cdu.totalphaseapppower > 0.0)) {	// Update power factor too, if we can
				update |= SPMCDUTagType.updateDouble(tag, PDUPWRFACTOR_ATTRIB_INDEX, Math.round(100.0 * (cdu.totalphasepower / cdu.totalphaseapppower)));
			}
		}

		return update;
	}
	
    /**
     * Update boolean attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new boolean value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateBoolean(SPMCDUTag tag, int index,
        boolean val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Boolean)) || (((Boolean) o).booleanValue() != val) ||
            tag.isDefaultFlagged(index)) {

            /* Use common references - save memory */
	        if (val)
	            obj[index] = Boolean.TRUE;
	        else
	            obj[index] = Boolean.FALSE;
	        tag.clearDefaultFlag(index);
            return true;
		}
        else {
        	return false;
        }
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateInt(SPMCDUTag tag, int index, int val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Integer)) || (((Integer) o).intValue() != val) || tag.isDefaultFlagged(index)) {
            obj[index] = Integer.valueOf(val);
            tag.clearDefaultFlag(index);
            return true;
        }
        else {
        	return false;
        }
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateLong(SPMCDUTag tag, int index, long val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Long)) || (((Long) o).longValue() != val) ||
            tag.isDefaultFlagged(index)) {
            obj[index] = Long.valueOf(val);
	        tag.clearDefaultFlag(index);
            return true;
		}
        else {
        	return false;
        }
    }
    /**
     * Update double attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new double value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateDouble(SPMCDUTag tag, int index, double val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Double))
            || (((Double) o).doubleValue() != val) ||
            tag.isDefaultFlagged(index)) {
            obj[index] = Double.valueOf(val);
	        tag.clearDefaultFlag(index);
            return true;
        }
        else {
        	return false;
        }
    }
    /**
     * Update string attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateString(SPMCDUTag tag, int index, String val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof String)) || (!((String) o).equals(val)) || tag.isDefaultFlagged(index)) {
            obj[index] = val;
            tag.clearDefaultFlag(index);
            return true;
        }
        else {
        	return false;
        }
    }
    /**
     * Update string list attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string list value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateStringList(SPMCDUTag tag, int index, List<String> val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof List)) || (!((List) o).equals(val)) || tag.isDefaultFlagged(index)) {
            obj[index] = val;
            tag.clearDefaultFlag(index);
            return true;
		}
        else {
        	return false;
        }
    }
    /**
     * Update long list attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new long list value
     * @return true if value changed, false if unchanged
     */
    @SuppressWarnings("rawtypes")
	protected static final boolean updateLongList(SPMCDUTag tag, int index, List<Long> val) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof List)) || (!((List) o).equals(val)) || tag.isDefaultFlagged(index)) {
            obj[index] = val;
            tag.clearDefaultFlag(index);
            return true;
		}
        else {
        	return false;
        }
    }
    /**
     * Update attribute value at given index to null, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateNull(SPMCDUTag tag, int index) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o != null) || tag.isDefaultFlagged(index)) {
            obj[index] = null;
            tag.clearDefaultFlag(index);
            return true;
		}
        else {
        	return false;
        }
    }

}
