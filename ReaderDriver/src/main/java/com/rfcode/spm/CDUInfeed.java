package com.rfcode.spm;

import org.w3c.dom.Node;

// Details for infeed on CDU
public class CDUInfeed {
	// Identifier of the infeed
	public final long id;
	// User assigned name of the infeed
	public final String name;
	// Generated name for infeed
	public final String absName;
	// User notes for infeed
	public final String notes;
	// ID of tower for infeed
	public final long towerId;
	// Infeed capabilities (0x8000 (onSense) 0x4000 (loadSense) 0x2000 (powerControl) 0x1000 (failSafe) 0x0800 (defaultOff) 0x0400 (voltageSense) 0x0200 (powerSense))
	public final int capabilities;
	public static final int CAPABILITIES_ONSENSE = 0x8000;
	public static final int CAPABILITIES_LOADSENSE = 0x4000;
	public static final int CAPABILITIES_POWERCONTROL = 0x2000;
	public static final int CAPABILITIES_FAILSAFE = 0x1000;
	public static final int CAPABILITIES_DEFAULTOFF = 0x0800;
	public static final int CAPABILITIES_VOLTAGESENSE = 0x0400;
	public static final int CAPABILITIES_POWERSENSE = 0x0200;
	// Infeed status (on, off, ...)
	public final String status;
	// Infeed load status
	public final String loadStatus;
	// Current infeed load (amps)
	public final double load;
	// User assigned high load threshold (amps)
	public final int loadHighThresh;
	// Current infeed voltage (volts)
	public final double voltage;
	// Current infeed power (watts)
	public final int power;
	// Current infeed load capacity (amps)
	public final int capacity;
	// Current infeed apparent power consumption (volt-amps)
	public final int apparentPower;
	// Current infeed power factor
	public final double powerFactor;
	// Current infeed crest factor
	public final double crestFactor;
	// Energy consumption of the infeed (watt-hours)
	public final long wattHours;
	// The characterization of the phase relation between the voltage and current of the input feed phase.
	public final long reactance;
	// The voltage measured for the input feed phase. A non-negative value indicates the voltage in Volts.
	public final long phaseVoltage;
	// The current measured for the input feed phase. A non-negative value indicates the measured load in Amps.
	public final long phaseCurrent;
	// The used percentage of the input feed line load capacity (infeedLoadValue / infeedCapacity x 100). A non-negative value indicates the percentage of capacity used.
	public final double capacityUsed;
	// The ID of the input feed line.
	public final String lineID;
	// The line-to-line ID of the input feed.
	public final String lineToLineID;
	// The ID of the input feed phase.
	public final String phaseID;
	// The apparent power capacity of the input feed circuit. A non-negative value indicates the maximum apparent power in Volt-Amps.
	public final long VACapacity;
	// The used percentage of the input feed circuit apparent power capacity (infeedApparentPower / infeedVACapacity x 100). A non-negative value indicates the percentage of capacity used.
	public final double VACapacityUsed;
	
	public CDUInfeed(Node n) {
		// Identifier of the infeed
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// User assigned name of the infeed
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Generated name for infeed
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User notes for infeed
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// ID of tower for infeed
		this.towerId = SPMInterface.getLongFromNode(n, "towerId", -1);
		// Infeed capabilities (0x8000 (onSense) 0x4000 (loadSense) 0x2000 (powerControl) 0x1000 (failSafe) 0x0800 (defaultOff) 0x0400 (voltageSense) 0x0200 (powerSense))
		this.capabilities = SPMInterface.getIntFromNode(n, "capabilities", -1);
		// Infeed status (on, off, ...)
		this.status = SPMInterface.getStringFromNode(n, "status", "");
		// Infeed load status
		this.loadStatus = SPMInterface.getStringFromNode(n, "loadStatus", "");
		// Current infeed load (amps)
		this.load = SPMInterface.getDoubleFromNode(n, "load", -1);
		// User assigned high load threshold (amps)
		this.loadHighThresh = SPMInterface.getIntFromNode(n, "loadHighThresh", -1);
		// Current infeed voltage (volts)
		this.voltage = SPMInterface.getDoubleFromNode(n, "voltage", -1);
		// Current infeed power (watts)
		this.power = SPMInterface.getIntFromNode(n, "power", -1);
		// Current infeed load capacity (amps)
		this.capacity = SPMInterface.getIntFromNode(n, "capacity", -1);
		// Current infeed apparent power consumption (volt-amps)
		this.apparentPower = SPMInterface.getIntFromNode(n, "apparentPower", -1);
		// Current infeed power factor
		this.powerFactor = SPMInterface.getDoubleFromNode(n, "powerFactor", -1);
		// Current infeed crest factor
		this.crestFactor = SPMInterface.getDoubleFromNode(n, "crestFactor", -1);
		// Energy consumption of the infeed (watt-hours)
		this.wattHours = SPMInterface.getLongFromNode(n, "wattHours", -1);
		// The characterization of the phase relation between the voltage and current of the input feed phase.
		this.reactance = SPMInterface.getLongFromNode(n, "reactance", -1);
		// The voltage measured for the input feed phase. A non-negative value indicates the voltage in Volts.
		this.phaseVoltage = SPMInterface.getLongFromNode(n, "phaseVoltage", -1);
		// The current measured for the input feed phase. A non-negative value indicates the measured load in Amps.
		this.phaseCurrent = SPMInterface.getLongFromNode(n, "phaseCurrent", -1);
		// The used percentage of the input feed line load capacity (infeedLoadValue / infeedCapacity x 100). A non-negative value indicates the percentage of capacity used.
		this.capacityUsed = SPMInterface.getDoubleFromNode(n, "capacityUsed", -1);
		// The ID of the input feed line.
		this.lineID = SPMInterface.getStringFromNode(n, "lineID", "");
		// The line-to-line ID of the input feed.
		this.lineToLineID = SPMInterface.getStringFromNode(n, "lineToLineID", "");
		// The ID of the input feed phase.
		this.phaseID = SPMInterface.getStringFromNode(n, "phaseID", "");
		// The apparent power capacity of the input feed circuit. A non-negative value indicates the maximum apparent power in Volt-Amps.
		this.VACapacity = SPMInterface.getLongFromNode(n, "VACapacity", -1);
		// The used percentage of the input feed circuit apparent power capacity (infeedApparentPower / infeedVACapacity x 100). A non-negative value indicates the percentage of capacity used.
		this.VACapacityUsed = SPMInterface.getDoubleFromNode(n, "VACapacityUsed", -1);
	}
	public boolean onSense() {
		return (capabilities & CAPABILITIES_ONSENSE) != 0;
	}
	public boolean loadSense() {
		return (capabilities & CAPABILITIES_LOADSENSE) != 0;
	}
	public boolean powerControl() {
		return (capabilities & CAPABILITIES_POWERCONTROL) != 0;
	}
	public boolean failSafe() {
		return (capabilities & CAPABILITIES_FAILSAFE) != 0;
	}
	public boolean defaultOff() {
		return (capabilities & CAPABILITIES_DEFAULTOFF) != 0;
	}
	public boolean voltageSense() {
		return (capabilities & CAPABILITIES_VOLTAGESENSE) != 0;
	}
	public boolean powerSense() {
		return (capabilities & CAPABILITIES_POWERSENSE) != 0;
	}
}
