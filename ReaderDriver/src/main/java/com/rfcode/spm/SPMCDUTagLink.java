package com.rfcode.spm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Map;

import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.ranger.RangerProcessor;

public class SPMCDUTagLink implements TagLink {
    /* Instance variables */
    private final SPMCDUTag our_tag;
    private final SPMReaderChannel our_channel;
    private boolean linkLost = false;
    
    /* Period for timeout processing is 1 second */
    private static final int TIMEOUT_PERIOD = 1000;

    /*
     * Exception table - cache of Integers, keyed by TagLink, for TagLinks that
     * have started to age-out (had one or more SSI_VALUE_NA reports). Used to
     * avoid burdening instance data with variables that most TagLinks don't
     * need most of the time.
     */
    private static IdentityHashMap<SPMCDUTagLink, ExceptionState> link_exceptions = new IdentityHashMap<SPMCDUTagLink, ExceptionState>();

    /**
     * Exception state holder - used for handling variables only needed for
     * TagLinks that are in some form of exception processing, such as a missing
     * SSI age-out
     */
    private static class ExceptionState {
        int ageout_cnt; /* Number of seconds remaining before age-out */
    }

    /**
     * Simple handler object for periodic age-out processing
     */
    private static class OurAgeOutHandler implements Runnable {
        SessionFactory factory;
        /**
         * Method to be run when action is executed by ranger processor thread
         */
        public void run() {
            handleAgeOut(factory);
        }
    }
    private static OurAgeOutHandler age_handler = new OurAgeOutHandler();

    public SPMCDUTagLink(SPMCDUTag tag, SPMReaderChannel chan) {
    	our_tag = tag;
    	our_channel = chan;
        our_channel.addLink(this);
    }

	@Override
	public Tag getTag() {
		return our_tag;
	}

	@Override
	public ReaderChannel getChannel() {
		return our_channel;
	}

	@Override
	public int readTagLinkAttributes(Object[] val, int start, int count) {
		for (int i = 0; i < count; i++) {
			val[i] = null;
		}
		return count;
	}

	@Override
	public Object readTagLinkAttribute(int index) {
		return null;
	}

	@Override
	public Object readTagLinkAttribute(String id) {
		return null;
	}

	@Override
	public Map<String, Object> getTagLinkAttributes() {
		return Collections.emptyMap();
	}

	@Override
	public boolean isLinkLost() {
		return linkLost;
	}

	@Override
	public int getTagLinkAttributeCount() {
		return 0;
	}

	@Override
	public void cleanup() {
		linkLost = true;
	}

	public SPMCDUTag getSPMCDUTag() {
		return our_tag;
	}

    /**
     * Handle periodic age-out processing
     */
    private static void handleAgeOut(SessionFactory factory) {
        ArrayList<SPMCDUTagLink> tosslist = null;
        /* Loop through the exceptions */
        for (Map.Entry<SPMCDUTagLink, ExceptionState> rec : link_exceptions
            .entrySet()) {
            ExceptionState es = rec.getValue();
            es.ageout_cnt--; /* Drop age count */
            if (es.ageout_cnt <= 0) { /* Done aging? */
                /* Add to list to toss (don't mess with iterator now) */
                if (tosslist == null)
                    tosslist = new ArrayList<SPMCDUTagLink>();
                tosslist.add(rec.getKey());
            }
        }
        /* If we tossed anyone, do it now */
        if (tosslist != null) {
            for (SPMCDUTagLink tl : tosslist) {
                tl.our_tag.insertTagLink(tl.our_channel, null);
            }
        }
        /* If not empty now, re-enqueue timeout processing */
        if (link_exceptions.isEmpty() == false) {
            RangerProcessor.addDelayedAction(age_handler, TIMEOUT_PERIOD);
        }
    }

    /**
     * Start link age-out, if not already started
     * 
     * @param t_o -
     *            timeout in seconds
     */
    void startLinkAgeOut(int t_o) {
        ExceptionState es = link_exceptions.get(this);
        if (es == null) { /* If first time, initialize it */
            es = new ExceptionState();
            es.ageout_cnt = t_o;
            if (link_exceptions.isEmpty()) { /* We first exception? */
                /* Add delayed action for our age-out handler */
                RangerProcessor.addDelayedAction(age_handler, TIMEOUT_PERIOD);
            }
            link_exceptions.put(this, es);
        } else if (es.ageout_cnt > t_o) { /*
                                             * If shorter, shorten time
                                             * remaining
                                             */
            es.ageout_cnt = t_o;
        }
    }
    /**
     * Stop link age-out, if started
     */
    void stopLinkAgeOut() {
        link_exceptions.remove(this);
    }

}
