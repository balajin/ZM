package com.rfcode.spm;

import java.util.Map;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagType;

public class SPMCDUTagGroup extends TagGroup {
    private String groupcode; /* Normalized group code */
    /** Tag group attribute - group code */
    public static final String GROUPCODE_GROUPATTRIB = "groupcode";
    /** Common list of tag group attributes for mantis II tags */
    public static final String[] GROUPATTRIBLIST = {GROUPCODE_GROUPATTRIB};

	public SPMCDUTagGroup(TagType tt) {
		super(tt);
	}

    /**
     * Initialization method - must be called once all attributes have been
     * initialized
     *
     * @throws DuplicateEntityIDException
     *             if TagGroup is duplicate of existing entity
     * @throws if
     *             missing required attribute
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        if (groupcode == null) {
            throw new BadParameterException("Missing groupcode");
        }
        if (!testGroupCode()) {
            throw new BadParameterException("Groupcode must contain only 6 letters (A-Z)");
        }
        super.init();
    }

    private boolean testGroupCode() {
        int len = groupcode.length();
        if (len != 6) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            char c = groupcode.charAt(i);
            if (((c >= 'A') && (c <= 'Z')) || /* Uppercase */
                ((c >= 'a') && (c <= 'z'))) { /* Lowercase */
            }
            else {
                return false;
            }
        }
        return true;
    }

    /**
     * Set attributes required for TagGroup.
     * 
     * @param attrs -
     *            Map of attribute values, keyed by attribute ids
     */
    public void setTagGroupAttributes(Map<String, Object> attrs) {
        super.setTagGroupAttributes(attrs); /* Call parent setter */
        Object gc = attrs.get(GROUPCODE_GROUPATTRIB); /* Get value */
        if ((gc != null) && (gc instanceof String)) {
            groupcode = ((String) gc).toUpperCase(); /* Make upper case */
        }
    }
    /**
     * Get our group code (normalized for GUIDs)
     * 
     * @return group code
     */
    public String getGroupCode() {
        return groupcode;
    }
    /**
     * Get SPM tag type
     * 
     * @return tag type
     */
    public SPMCDUTagType getSPMCDUTagType() {
        return (SPMCDUTagType) getTagType();
    }
    /**
     * Add tag to the set of live tags in the group
     * 
     * @param tag -
     *            tag to add
     */
    void addSPMCDUTag(SPMCDUTag tag) {
        addTag(tag);
    }
    /**
     * Remove tag from the set of live tags in the group
     * 
     * @param tag -
     *            tag to remove
     */
    void removeSPMCDUTag(SPMCDUTag tag) {
        removeTag(tag);
    }

    /**
     * Call tag lifecycle listeners for creation of given tag.
     * 
     * @param tag -
     *            new tag
     * @param cause -
     *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
     */
    void reportTagLifecycleCreate(Tag tag, String cause) {
        notifyTagLifecycleCreate(tag, cause);
    }
    /**
     * Call tag lifecycle listeners for deletion of given tag.
     * 
     * @param tag -
     *            deleted tag
     */
    void reportTagLifecycleDelete(Tag tag) {
        notifyTagLifecycleDelete(tag);
    }
    /**
     * Call tag status listeners for attribute changed notification
     * 
     * @param tag -
     *            tag with change attributes
     * @param oldval -
     *            array of previous tag attribute values (may be longer than the
     *            number of atributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    void reportTagStatusAttributeChange(Tag tag, Object[] oldval) {
        notifyTagStatusAttributeChange(tag, oldval);
    }
    /**
     * Call tag status listeners for tag link added event.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    void reportTagStatusLinkAdded(Tag tag, TagLink taglink) {
        notifyTagStatusLinkAdded(tag, taglink);
    }
    /**
     * Call tag status listeners for tag link removed event.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    void reportTagStatusLinkRemoved(Tag tag, TagLink taglink) {
        notifyTagStatusLinkRemoved(tag, taglink);
    }
    /**
     * Call tag link status listeners for tag link attrib update event.
     * 
     * @param tag -
     *            tag with updated link
     * @param taglink -
     *            link updated to tag
     */
    void reportTagLinkUpdated(Tag tag, TagLink taglink) {
        notifyTagLinkUpdated(tag, taglink);
    }
    /**
     * Test if given tag ID matches group
     * @param tag_guid - tag GUID
     * @return true if matches, false if not
     */
    public boolean isMatchingTagID(String tag_guid) {
    	if ((tag_guid.length() == 14) && groupcode.equalsIgnoreCase(tag_guid.substring(0, 6))) {
    		return true;
    	}
    	return false;
    }
}
