package com.rfcode.spm;

import org.w3c.dom.Node;

public class CDUTower {
	// ID of the tower
	public final long id;
	// Name for tower
	public final String name;
	// Generated name for tower
	public final String absName;
	// User notes for tower
	public final String notes;
	// Serial number of tower
	public final String serial;
	// Model ID of tower
	public final String model;
	// Capabilities (0x8000 (failsafe) 0x4000 (fuseSense) 0x2000 (directCurrent) 0x1000 (threePhase) 0x0800 (fanSense) 0x0400 (tempSense))
	public final int capabilities;
	public static final int CAPABILITIES_FAILSAFE = 0x4000;
	public static final int CAPABILITIES_DIRECTCURRENT = 0x2000;
	public static final int CAPABILITIES_THREEPHASE = 0x1000;
	public static final int CAPABILITIES_FANSENSE = 0x0800;
	public static final int CAPABILITIES_TEMPSENSE = 0x0400;
	// The total apparent power capacity of the tower circuit. A non-negative value indicates the maximum total apparent power in Volt-Amps.
	public final long capacity;
	// If the CDU reports that it is 3 phase, this will be true. It cannot be assumed that the value of 'false' means that it is not 3 phase.
	public final boolean isThreePhase;
	// The used percentage of the tower circuit total apparent power capacity (towerApparentPower / towerVACapacity x 100). A non-negative value indicates the percentage of capacity used.
	public final double capacityUsed;
	// The total active power consumption of the tower circuit. A non-negative value indicates the total active power consumption in Watts.
	public final long activePower;
	// The total apparent power consumption of the tower circuit. A non-negative value indicates the total apparent power consumption in Volt-Amps.
	public final long apparentPower;
	// The overall power factor of the tower circuit. A non-negative value indicates the overall power factor.
	public final double powerFactor;
	// The total energy consumption of the tower circuit. A non-negative value indicates the total energy consumption in Kilowatt-Hours.
	public final long totalEnergy;
	// The frequency of the input feed line voltage. A non-negative value indicates the frequency in Hertz.
	public final int lineFrequency;
	
	public CDUTower(Node n) {
		// ID of the tower
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// Name for tower
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Generated name for tower
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User notes for tower
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// Serial number of tower
		this.serial = SPMInterface.getStringFromNode(n, "serial", "");
		// Model ID of tower
		this.model = SPMInterface.getStringFromNode(n, "model", "");
		// Capabilities (0x8000 (failsafe) 0x4000 (fuseSense) 0x2000 (directCurrent) 0x1000 (threePhase) 0x0800 (fanSense) 0x0400 (tempSense))
		this.capabilities = SPMInterface.getIntFromNode(n, "capabilities", -1);
		// The total apparent power capacity of the tower circuit. A non-negative value indicates the maximum total apparent power in Volt-Amps.
		this.capacity = SPMInterface.getLongFromNode(n, "capacity", -1);
		// If the CDU reports that it is 3 phase, this will be true. It cannot be assumed that the value of 'false' means that it is not 3 phase.
		this.isThreePhase = SPMInterface.getBooleanFromNode(n, "isThreePhase", false);
		// The used percentage of the tower circuit total apparent power capacity (towerApparentPower / towerVACapacity x 100). A non-negative value indicates the percentage of capacity used.
		this.capacityUsed = SPMInterface.getDoubleFromNode(n, "capacityUsed", -1);
		// The total active power consumption of the tower circuit. A non-negative value indicates the total active power consumption in Watts.
		this.activePower = SPMInterface.getLongFromNode(n, "activePower", -1);
		// The total apparent power consumption of the tower circuit. A non-negative value indicates the total apparent power consumption in Volt-Amps.
		this.apparentPower = SPMInterface.getLongFromNode(n, "apparentPower", -1);
		// The overall power factor of the tower circuit. A non-negative value indicates the overall power factor.
		this.powerFactor = SPMInterface.getDoubleFromNode(n, "powerFactor", -1);
		// The total energy consumption of the tower circuit. A non-negative value indicates the total energy consumption in Kilowatt-Hours.
		this.totalEnergy = SPMInterface.getLongFromNode(n, "totalEnergy", -1);
		// The frequency of the input feed line voltage. A non-negative value indicates the frequency in Hertz.
		this.lineFrequency = SPMInterface.getIntFromNode(n, "lineFrequency", -1);
	}
	public boolean failSafe() {
		return (capabilities & CAPABILITIES_FAILSAFE) != 0;
	}
	public boolean directCurrent() {
		return (capabilities & CAPABILITIES_DIRECTCURRENT) != 0;
	}
	public boolean threePhase() {
		return (capabilities & CAPABILITIES_THREEPHASE) != 0;
	}
	public boolean fanSense() {
		return (capabilities & CAPABILITIES_FANSENSE) != 0;
	}
	public boolean tempSense() {
		return (capabilities & CAPABILITIES_TEMPSENSE) != 0;
	}
}
