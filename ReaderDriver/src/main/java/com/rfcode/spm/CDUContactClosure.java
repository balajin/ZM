package com.rfcode.spm;

import org.w3c.dom.Node;

// Details for contact closure sensors
public class CDUContactClosure {
	// ID of the sensor
	public long id;
	// Name for sensor
	public String name;
	// Generated name for sensor
	public String absName;
	// User notes for sensor
	public String notes;
	// ID of environmental monitoring sensor it is attached to
	public long envMonId;
	// Contact closure sensor status (normal, alarm, noComm)
	public String status;
	
	public CDUContactClosure(Node n) {
		// ID of the sensor
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// Name for sensor
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Generated name for sensor
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User notes for sensor
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// ID of environmental monitoring sensor it is attached to
		this.envMonId = SPMInterface.getLongFromNode(n, "envMonId", -1);
		// Contact closure sensor status (normal, alarm, noComm)
		this.status = SPMInterface.getStringFromNode(n, "status", "");
	}
}
