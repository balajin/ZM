package com.rfcode.spm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagLinkLifecycleListener;

public class SPMReaderChannel implements ReaderChannel {
	private String id;
	private boolean valid;
	private SPMReader reader;
	private String rel_id;
	private String label;

    /* Our tag links, keyed by tag guid */
    private HashMap<String, TagLink> our_links;
    private Map<String, TagLink> our_links_ro;
    /* List of tag link listeners for this channel */
    private ArrayList<TagLinkLifecycleListener> listeners;
    private List<TagLinkLifecycleListener> listeners_ro;
    /* Empty list */
    private static List<TagLinkLifecycleListener> empty_list = Collections.emptyList();

	public SPMReaderChannel(String c_id, String c_label, SPMReader reader) {
        rel_id = c_id;
        label = c_label;
        id = reader.getID() + "_channel_" + rel_id;
		our_links = new HashMap<String, TagLink>();
        our_links_ro = Collections.unmodifiableMap(our_links);
        this.reader = reader;
        valid = true;
	}
	
	@Override
	public String getID() {
		return id;
	}

	@Override
	public void setID(String id) {
	}

	@Override
	public void init() throws DuplicateEntityIDException, BadParameterException {
		ReaderEntityDirectory.addEntity(this);
	}

	@Override
	public void cleanup() {
        ArrayList<SPMCDUTag> tags = new ArrayList<SPMCDUTag>();
        /* Get list of tags to be unlinked */
        for (TagLink tl : our_links.values()) {
            tags.add(((SPMCDUTagLink)tl).getSPMCDUTag());
        }
        /*
         * Then unhook em (need extra list because unlink calls back to alter
         * our_links during iteration)
         */
        for (SPMCDUTag tag : tags) {
            /* Unhook the link from the tag */
            tag.insertTagLink(this, null);
        }
        ReaderEntityDirectory.removeEntity(this);
        valid = false;
	}

	@Override
	public String getRelativeChannelID() {
		return rel_id;
	}

	@Override
	public String getChannelLabel() {
		return label;
	}

	@Override
	public void setChannelLabel(String lab) {
		this.label = lab;
	}

	@Override
	public Reader getReader() {
		return reader;
	}

	@Override
	public Map<String, TagLink> getTagLinks() {
        return our_links_ro;
    }

	@Override
	public int getTagLinkCount() {
		return our_links.size();
	}

	@Override
	public void addTagLinkListener(TagLinkLifecycleListener listen) {
        if (listeners == null) {
            listeners = new ArrayList<TagLinkLifecycleListener>();
            listeners_ro = Collections.unmodifiableList(listeners);
        }
        listeners.add(listen);
	}

	@Override
	public void removeTagLinkListener(TagLinkLifecycleListener listen) {
        if (listeners != null) {
            listeners.remove(listen);
            if (listeners.size() == 0) {
                listeners = null;
                listeners_ro = null;
            }
        }
	}

	@Override
	public List<TagLinkLifecycleListener> getTagLinkListeners() {
        if (listeners == null)
            return empty_list;
        return listeners_ro;
	}

	@Override
	public boolean isValid() {
        return valid;
    }

	@Override
	public int getChannelNoiseLevel() {
		return 0;
	}

	@Override
	public int getChannelEventsPerSec() {
		return 0;
	}

	public void addLink(SPMCDUTagLink tl) {
		our_links.put(tl.getTag().getTagGUID(), tl);
	}

}
