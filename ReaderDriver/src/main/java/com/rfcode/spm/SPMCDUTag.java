package com.rfcode.spm;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.mantis2.SubTag;

/**
 * Class representing a CDU from SPM
 * 
 * @author Mike Primm
 * 
 */
public class SPMCDUTag implements Tag {
	private final String guid;
	private SPMCDUTagGroup grp;
    private int lock_cnt;			/* Tag lock count */

    private Object[] attribs; 		/* Attribute values, indexed to match array of */
    private boolean[] updattribs;   /* Flags indicating which attributes are not default */
	private long loc_match_ts;  /* Timestamp for last location match change */
	
    private Map<String, TagLink> links; /* Our TagLinks */
    
    private static final int OFFSET_TAGID = 6;
    
    SPMCDUTagType.CDUState state;
    
	SPMCDUTag(SPMCDUTagGroup grp, String guid) {
		this.guid = guid;
		this.grp = grp;
		this.links = new HashMap<String, TagLink>();
		
        attribs = grp.getTagType().getTagAttributeDefaultValues(); 
        // Get default attributes
        updattribs = new boolean[attribs.length];
        // Make update-blocked attributes non-defaulted (since we will never set them)
        for(int i = 0;i < updattribs.length; i++) {
    		updattribs[i] = isBlockedUpdateTagAttrib(i);
        }
	}
	
	@Override
	public int readTagAttributes(Object[] val, int start, int count) {
        int i, j;
        boolean did_validate = false;
        for (i = start, j = 0; j < count; i++, j++) {
            // If in range, return value (just let negatives throw ArrayIndexOutOfBounds its what we'd do)
            if (i < attribs.length) {
                val[j] = attribs[i];
            } else { /* Past end */
                if (!did_validate) {
                    validateAttribs(); /* Update if needed once */
                    did_validate = true;
                }
                if (i < attribs.length) /* If in bounds now, get it */
                    val[j] = attribs[i];
                else
                    /* Else, undefined */
                    val[j] = null;
            }
        }
        return count; /* Return number of values copied */
	}

	@Override
	public Object readTagAttribute(int index) {
        if (index < attribs.length)
            return attribs[index];
        validateAttribs();
        if (index < attribs.length)
            return attribs[index];
        return null;
	}

	@Override
	public Map<String, Object> getTagAttributes() {
        validateAttribs(); /* Update if needed */

        HashMap<String, Object> map = new HashMap<String, Object>();
        String[] ids = getTagType().getTagAttributes();

        for (int i = 0; i < attribs.length; i++) { /* Add values to map */
        	if(attribs[i] instanceof CustomFieldValue) {
        		CustomFieldValue cfv = (CustomFieldValue) attribs[i];
        		map.put(cfv.getField(), cfv.getValue());
        	}
        	else
        		map.put(ids[i], attribs[i]);
        }
        return map;
	}

	@Override
	public boolean updateTagAttributes(int[] idx, Object[] val, int count) {
        if((attribs == null) || (idx == null) || (val == null))
            return false;
        validateAttribs();
        Object oldval[] = null;
        for (int i = 0; i < count; i++) {
            int ix = idx[i];
			if(isBlockedUpdateTagAttrib(ix))
				continue;
            /* If was null or is different */
            boolean diff = (attribs[ix] == null)?(val[i] != null):
                (!attribs[ix].equals(val[i]));
            if (diff || (!updattribs[ix])) {
                /* Back up old values, if not already done */
                if (oldval == null)
                    oldval = attribs.clone();
                attribs[ix] = val[i];
                updattribs[ix] = true;
            }
        }
        /* If changes made, report them */
        if (oldval != null) {
            grp.reportTagStatusAttributeChange(this, oldval);
        }
        return (oldval != null);
	}

	@Override
	public String getTagGUID() {
		return guid;
	}

	@Override
	public String getTagID() {
		return guid.substring(OFFSET_TAGID);
	}

	@Override
	public TagGroup getTagGroup() {
		return grp;
	}

	@Override
	public TagType getTagType() {
		return grp.getTagType();
	}
	
	SPMCDUTagType getSPMCDUTagType() {
		return grp.getSPMCDUTagType();
	}

	@Override
	public TagLink findTagLink(ReaderChannel channel) {
		return links.get(channel.getID());
	}

	@Override
	public TagLink findTagLink(String channelid) {
		return links.get(channelid);
	}

	@Override
	public Map<String, TagLink> getTagLinks() {
		return Collections.unmodifiableMap(links);
	}

	@Override
	public int getTagLinkCount() {
		return links.size();
	}

	@Override
	public void init() {
		grp.addSPMCDUTag(this);
	}

	@Override
	public void cleanup() {
        if (state != null) {
            state.cleanup();
            state = null;
        }
        if (links.isEmpty() != false) {
            for (TagLink link : links.values())
                link.cleanup();
            links.clear();
        }
        if (grp != null) {
            grp.reportTagLifecycleDelete(this);
            grp.removeSPMCDUTag(this);
            grp = null;
        }
        attribs = null;
        updattribs = null;
	}

	@Override
	public boolean isValid() {
		return (attribs != null);
	}

	@Override
	public int lockUnlockTag(boolean lock) {
        if(lock) {
            lock_cnt++;
        }
        else {
            lock_cnt--;
            if(lock_cnt <= 0) {
                if (links.isEmpty()) {
                    cleanup();
                }
            }
        }
        return lock_cnt;
	}

	@Override
	public Set<String> getDefaultedTagAttributes() {
        Set<String> rslt = null;
        String[] ids = null;
        for(int i = 0; i < updattribs.length; i++) {
            if(updattribs[i] == false) { /* If defaulted? */
				if(isBlockedUpdateTagAttrib(i)) {
					updattribs[i] = true;
				}
				else {
	                if(ids == null)
    	                ids = getTagType().getTagAttributes();
    	            if(rslt == null)
    	                rslt = new TreeSet<String>();
    	            rslt.add(ids[i]);
				}
            }
        }
        return rslt;
	}

	@Override
	public boolean isDefaultFlagged(int idx) {
        return !updattribs[idx];
    }
    /**
     * Mark attribute as updated from default
     */
    void clearDefaultFlag(int idx) {
        updattribs[idx] = true;
    }

	@Override
	public Set<Tag> getSubTags() {
		return SubTag.getSubTagsByTag(guid);
	}

	@Override
	public Tag getParentTag() {
		return null;
	}

	@Override
	public void setLocationMatchTimestamp(long ts) {
        loc_match_ts = ts;
	}

	@Override
	public long getLocationMatchTimestamp() {
		return loc_match_ts;
	}

	@Override
	public boolean isIRDetectingTag() {
		return false;
	}

	public void insertTagLink(SPMReaderChannel channel, TagLink taglink) {
        if(grp == null)
            return;
        if (taglink == null) {
            taglink = links.remove(channel.getID());
            if (taglink != null) { /* Was something removed? */
                grp.reportTagStatusLinkRemoved(this, taglink);
                taglink.cleanup(); /* Clean it up */
                /* If no more links and not locked, retire the tag */
                if (links.isEmpty()) {
                    if(lock_cnt <= 0) {
                        cleanup();
                    }
                    else {  /* Else, reset attributes */
                        grp.getSPMCDUTagType().setTagOffline(this);
                    }
                }
            }
        } else {
            links.put(channel.getID(), taglink);
            grp.reportTagStatusLinkAdded(this, taglink);
        }
	}
	static String buildTagGUID(SPMCDUTagGroup grp, int tagid) {
		return String.format("%s%08d", grp.getGroupCode(), tagid);
	}
	
    /**
     * Equals handler
     */
    public boolean equals(Object o) {
        if((o != null) && (o instanceof SPMCDUTag)) {
            return guid.equals(((SPMCDUTag)o).guid);
        }
        return false;
    }

    /**
     * Find tag, if exists, or create new tag
     * 
     * @param grp -
     *            our group
     * @param tagid -
     *            our tagid
     * @param no_create -
     *            if true, do not do create if missing
     * @return tag instance
     */
    static SPMCDUTag findOrCreateTag(SPMCDUTagGroup grp, int tagid, boolean no_create) {
        String guid = buildTagGUID(grp, tagid);
        SPMCDUTag tag = (SPMCDUTag) grp.findTag(guid); /* Try to find it */
        if ((tag == null) && (!no_create)) { /* Not found, make new one */
        	tag = grp.getSPMCDUTagType().makeTag(grp, guid);
            tag.init();
        }
        return tag;
    }

	/**
	 * Test if given attribute index is update blocked
	 */
	public boolean isBlockedUpdateTagAttrib(int idx) {
		return grp.isBlockedUpdateTagAttrib(idx);
	}

    /**
     * Validate our attribute set - make sure we fault in new values
     */
    private void validateAttribs() {
        int cnt = getTagType().getTagAttributeCount();
        if (attribs.length >= cnt) {
            return;
        }
        /* Get defaults and overlay our values */
        Object[] newattr = getTagType().getTagAttributeDefaultValues();
        System.arraycopy(attribs, 0, newattr, 0, attribs.length);
        attribs = newattr; /* And switch to new stuff */
        boolean[] newupd = new boolean[newattr.length];
        System.arraycopy(updattribs, 0, newupd, 0, updattribs.length);
        updattribs = newupd;
    }

	Object[] getRawAttribs() {
        return attribs;
	}

	SPMCDUTagGroup getSPMCDUTagGroup() {
		return grp;
	}
}
