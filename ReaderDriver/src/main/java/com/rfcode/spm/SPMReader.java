package com.rfcode.spm;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.drivers.readers.ReaderNetworkInterfaceStatusListener;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.mantis2.MantisIIReaderFactory;
import com.rfcode.drivers.readers.mantis2.MantisIITagLink;
import com.rfcode.drivers.readers.mantis2.X200ReaderFactory;
import com.rfcode.ranger.FirmwareLibrary;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.RangerServer;

public class SPMReader implements Reader {
	private SPMReaderFactory factory;
	private ReaderStatus status; // Current reader status
	private String readerid;
	private String label = "";
	private String hostname = "localhost";
	private int portnum = 80;
	private String userid = "admin";
	private String password = "admin";
	private int catalogPeriod = 600;	// Every 10 minutes, by default
	private int cduPeriod = 60;			// Every 1 minutes, by default
	private SPMInterface spm;
	private SPMReaderChannel[] channels;
	private SPMCDUTagGroup[] groups;
    private Map<String, Object> attribs_ro = Collections.emptyMap();	
    private ArrayList<ReaderStatusListener> status_listeners = new ArrayList<ReaderStatusListener>();
    private long startTime;
    private boolean enabled = false;
    private Thread ourThread;
    private boolean stop;
    private long lastCatalogTimestamp;
    private long[] cduIDs = new long[0];
    private int nextCDU;
    private long lastCDUTimestamp;
	
	private boolean inited = false;
	private boolean isMetric = false;
	
	public SPMReader(SPMReaderFactory spmfact) {
		factory = spmfact;
		groups = new SPMCDUTagGroup[0];
		status = ReaderStatus.DISABLED;
		try {
			spm = new SPMInterface();
		} catch (ParserConfigurationException e) {
		}
	}
	
	@Override
	public String getID() {
		return readerid;
	}

	@Override
	public void setID(String id) {
        readerid = id;
        if(label.length() == 0) {
            label = id;
        }
	}

	@Override
	public void init() throws DuplicateEntityIDException, BadParameterException {
        if (inited)
            return;
        if (hostname == null) {
            throw new BadParameterException("Missing hostname");
        }
        if ((portnum < 1) || (portnum > 65535)) {
            throw new BadParameterException("Missing port number");
        }
        if (readerid == null) {
            throw new BadParameterException("Missing reader ID");
        }
        /* Initialize for N channels */
        channels = new SPMReaderChannel[factory.getChannelCount()];
        for (int i = 0; i < channels.length; i++) {
            char ch = (char) ('A' + i);
            channels[i] = new SPMReaderChannel("" + ch, "Channel " + ch,
                this);
            channels[i].init();
        }
        /* Finally, add ourselves to the directory */
        ReaderEntityDirectory.addEntity(this);
        inited = true;
        /* If completed init(), call listeners */
        if (inited) {
            factory.callReaderLifecycleForCreate(this);
        }
	}

	@Override
	public void cleanup() {
		stop = true;
        /* If completed init(), call listeners */
        if (inited && (factory != null)) {
            factory.callReaderLifecycleForDelete(this);
            ReaderEntityDirectory.removeEntity(this);
            inited = false;
        }
        factory = null;
        if (status_listeners != null) {
            status_listeners.clear();
            status_listeners = null;
        }
        if (channels != null) {
            for (int i = 0; i < channels.length; i++) {
                channels[i].cleanup();
                channels[i] = null;
            }
            channels = null;
        }
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public void setLabel(String lab) {
        label = lab;
	}

	@Override
	public void setReaderAttributes(Map<String, Object> attribs)
			throws BadParameterException {
		Map<String, Object> newattribs = new HashMap<String, Object>(attribs_ro);
		String rname = (String) attribs.get(MantisIIReaderFactory.ATTRIB_READERNAME);
		String hostname = (String) attribs.get(MantisIIReaderFactory.ATTRIB_HOSTNAME);
		Integer port = (Integer) attribs.get(MantisIIReaderFactory.ATTRIB_PORTNUM);
		String userid = (String) attribs.get(MantisIIReaderFactory.ATTRIB_USERID);
		String passwd = (String) attribs.get(MantisIIReaderFactory.ATTRIB_PASSWORD);
		Integer updperiod = (Integer) attribs.get(SPMReaderFactory.ATTRIB_UPDATEPERIOD);
		Integer listupdperiod = (Integer) attribs.get(SPMReaderFactory.ATTRIB_LISTUPDATEPERIOD);
        Integer offlinelinkto = (Integer) attribs.get(MantisIIReaderFactory.ATTRIB_OFFLINELINKAGEOUT);
        if (offlinelinkto == null) {
            offlinelinkto = SPMReaderFactory.DEFAULT_OFFLINELINKAGEOUT;
        }

		if (hostname != null) {
			hostname = hostname.trim();
			if (hostname.length() == 0) {
				throw new BadParameterException(MantisIIReaderFactory.ATTRIB_HOSTNAME + " must be non-blank");
			}
			newattribs.put(MantisIIReaderFactory.ATTRIB_HOSTNAME, hostname);
		}
		if (port != null) {
			if ((port < 1) || (port > 65535)) {
				throw new BadParameterException(MantisIIReaderFactory.ATTRIB_PORTNUM + " value not valid: " + port);
			}
			newattribs.put(MantisIIReaderFactory.ATTRIB_PORTNUM, port);
		}
		if (rname != null) {
			newattribs.put(MantisIIReaderFactory.ATTRIB_READERNAME, rname.trim());
		}
		if (userid != null) {
			newattribs.put(MantisIIReaderFactory.ATTRIB_USERID, userid);
		}
		if (passwd != null) {
			newattribs.put(MantisIIReaderFactory.ATTRIB_PASSWORD, passwd);
		}
		int uperiod = SPMReaderFactory.ATTRIB_UPDATEPERIOD_DEFAULT;
		if (updperiod != null) {
			uperiod = updperiod;
			if (uperiod < 5) uperiod = 5;	// Clamp to 5 second minimum
		}
		int luperiod = SPMReaderFactory.ATTRIB_LISTUPDATEPERIOD_DEFAULT;
		if (listupdperiod != null) {
			luperiod = listupdperiod;
			if (luperiod < 5) luperiod = 5;	// Clamp to 5 second minimum
		}

        newattribs.put(SPMReaderFactory.ATTRIB_LISTUPDATEPERIOD, Integer.valueOf(luperiod));
        newattribs.put(SPMReaderFactory.ATTRIB_UPDATEPERIOD, Integer.valueOf(uperiod));
        newattribs.put(MantisIIReaderFactory.ATTRIB_OFFLINELINKAGEOUT, offlinelinkto);

		label = (String) newattribs.get(MantisIIReaderFactory.ATTRIB_READERNAME);
		hostname = (String) newattribs.get(MantisIIReaderFactory.ATTRIB_HOSTNAME);
		portnum = (Integer) newattribs.get(MantisIIReaderFactory.ATTRIB_PORTNUM);
		userid = (String) newattribs.get(MantisIIReaderFactory.ATTRIB_USERID);
		password = (String) newattribs.get(MantisIIReaderFactory.ATTRIB_PASSWORD);
		catalogPeriod = luperiod;
		cduPeriod = uperiod;
		attribs_ro = newattribs;
		// Update SPM object
		spm.setHostname(hostname + ":" + portnum);
		spm.setUserID(userid);
		spm.setPassword(password);
	}

	@Override
	public Map<String, Object> getReaderAttributes() {
		return attribs_ro;
	}

	@Override
	public ReaderFactory getReaderFactory() {
		return factory;
	}

	@Override
	public void setReaderEnabled(boolean enable) {
		if (enabled == enable) return;
		if (enable) {
			startTime = System.currentTimeMillis();
			stop = false;
			lastCatalogTimestamp = 0;
			ourThread = new Thread(new Runnable() {
				@Override
				public void run() {
					processSPM();
				}
			});
			updateReaderStatus(ReaderStatus.INITIALIZING, false);
			ourThread.start();
		}
		else {
			stop = true;
			if (ourThread != null) {
				ourThread.interrupt();
				ourThread = null;
			}
			updateReaderStatus(ReaderStatus.DISABLED, false);
		}
		enabled = enable;
	}

	@Override
	public boolean getReaderEnabled() {
		return enabled;
	}

	@Override
	public ReaderStatus getReaderStatus() {
		return status;
	}

	@Override
	public String getReaderStatusMsg() {
	    return getReaderStatus().toString();
	}

	@Override
	public String getReaderFirmwareVersion() {
		return "";
	}

	@Override
	public String getReaderFirmwareFamily() {
		return "";
	}

	@Override
	public void addReaderStatusListener(ReaderStatusListener listener) {
        synchronized (status_listeners) {
            status_listeners.add(listener);
        }
	}

	@Override
	public void removeReaderStatusListener(ReaderStatusListener listener) {
        synchronized (status_listeners) {
            status_listeners.remove(listener);
        }
	}

	@Override
	public TagGroup[] getReaderTagGroups() {
		return groups;
	}

	@Override
	public void setReaderTagGroups(TagGroup[] grps)
			throws BadParameterException {
		if (grps.length > 1) {
			throw new BadParameterException("Too many groups");
		}
		else if (grps.length == 1) {
			if ((grps[0] instanceof SPMCDUTagGroup) == false) {
				throw new BadParameterException("Invalid tag type");
			}
			groups = new SPMCDUTagGroup[1];
			groups[0] = (SPMCDUTagGroup) grps[0];
		}
		else {
			groups = new SPMCDUTagGroup[0];
		}
	}

	@Override
	public int getReaderMaxTagGroupCount() {
		return 1;
	}

	@Override
	public ReaderChannel[] getChannels() {
		return channels;
	}

	@Override
	public int getUsedTagCapacityPercent() {
		return 0;
	}

	@Override
	public boolean requestFirmwareUpgrade(boolean do_cancel) {
		return false;
	}

	@Override
	public int firmwareUpgradeProgress() {
		return 100;
	}

	@Override
	public boolean firmwareUpgradeAvailable() {
		return false;
	}

	@Override
	public void freshenDNS() {
		// TODO Auto-generated method stub

	}

	@Override
	public InetAddress getRemoteAddress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getTagTimeout() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GPS getGPS() {
		return null;
	}

	@Override
	public long getReaderStartupTime() {
		return startTime;
	}

	@Override
	public void requestReaderNetworkInterfaceStatus(
			ReaderNetworkInterfaceStatusListener callback) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean getReaderConnectionEncrypted() {
		return false;
	}

    /**
     * Update reader status (internal only)
     * 
     * @param s -
     *            new status
     * @param upd - force update
     */
    private void updateReaderStatus(ReaderStatus s, boolean upd) {
    	if (stop) return;
        if ((s != status) || upd) {
            ReaderStatus old_s = status;
            status = s;
            /* Call our listeners */
            for (ReaderStatusListener l : status_listeners) {
                l.readerStatusChanged(this, status, old_s);
            }
            /*
             * If not active anymore, kick timeout processing on all active
             * links
             */
            if (ReaderStatus.isOnline(old_s) && (!ReaderStatus.isOnline(status))) {
                kickTagLinkTimeoutProcessing();
            }
        }
    }
    /**
     * Kick timeout processing on all active links, in case we don't get back, or they
     * are gone before we do.
     */
    private void kickTagLinkTimeoutProcessing() {
        /* Get the timeout */
        int t_o;
       
        t_o = getAttribInteger(
            MantisIIReaderFactory.ATTRIB_OFFLINELINKAGEOUT,
            SPMReaderFactory.DEFAULT_OFFLINELINKAGEOUT);
        for (int i = 0; i < channels.length; i++) {
            for (Map.Entry<String,TagLink> tles : 
                channels[i].getTagLinks().entrySet()) {
                ((SPMCDUTagLink)tles.getValue()).startLinkAgeOut(t_o);
            }
        }
    }
    /**
     * Get attribute as integer, with default if invalid
     * 
     * @param id -
     *            attribute ID
     * @param def -
     *            default for bad values
     * @return value
     */
    private int getAttribInteger(String id, int def) {
        Object v = attribs_ro.get(id); /* Get value */
        if ((v != null) && (v instanceof Integer))
            return ((Integer) v).intValue();
        else
            return def;
    }
    
    private Object[] oldvals = new Object[1];
	private void doAddUpdateTag(long tagid, CDUDetails cdudet) {
    	if (groups.length < 1) return;
    	SPMCDUTag tag = SPMCDUTag.findOrCreateTag(groups[0], (int) tagid, false);
    	if (tag == null) return;
        boolean is_new = false;
        /* If no links yet, new tag */
        if (tag.getTagLinkCount() == 0) {
            is_new = true;
        }
        /* Now pass in the payload for parsing and updating attributes */
        if (!is_new) { /* Only need old values on updated tags */
            int cnt = tag.getTagType().getTagAttributes().length;
            /* Get values before update */
            if (cnt > oldvals.length) { /* Make sure enough room */
                oldvals = new Object[cnt];
            }
            tag.readTagAttributes(oldvals, 0, cnt); /* Read em */
            /* If different, report change */
            if (tag.getSPMCDUTagType().processUpdate(tag, cdudet, isMetric)) {
                tag.getSPMCDUTagGroup().reportTagStatusAttributeChange(tag, oldvals);
            }
        } else { /* Else, for new tag */
        	tag.getSPMCDUTagType().processUpdate(tag, cdudet, isMetric);	/* just process */
        	
        	tag.getSPMCDUTagGroup().reportTagLifecycleCreate(tag, null);
        }
        /* Next, handle our channels - add or remove as needed */
        for (int i = 0; i < channels.length; i++) {
            /* Find the link, if it exists */
            SPMCDUTagLink tl = (SPMCDUTagLink) tag.findTagLink(channels[i]);
            if (tl == null) { /* If link not defined, make it */
                tl = new SPMCDUTagLink(tag, channels[i]); /* Make it */
                tag.insertTagLink(channels[i], tl); /* And add it */
            }
        }
	}
	
    private void doDropTag(long tagid) {
    	if (groups.length < 1) return;
    	SPMCDUTag tag = SPMCDUTag.findOrCreateTag(groups[0], (int) tagid, true);
    	if (tag != null) {
    		for (int i = 0; i < channels.length; i++) {
    			tag.insertTagLink(channels[i], null); /* Drop all of em */
    		}
    	}
    }

    private void updateCatalog(long ts) throws InterruptedException {
		try {
			isMetric = spm.getSPMTempUnit();
			
			List<CDU> cdus = spm.getCDUCatalog();
			if (cdus != null) {
				long[] newcduids = new long[cdus.size()];
				for (int i = 0; i < newcduids.length; i++) {
					CDU cdu = cdus.get(i);
					if (cdu != null) {
						newcduids[i] = cdu.id;
						// New tag - nothing to do just yet
					}
				}
				Arrays.sort(newcduids);	// Sort new set
				// See if anything is dropped
				for (int i = 0; i < cduIDs.length; i++) {
					// See if new ID (not in old set)
					if (Arrays.binarySearch(newcduids, cduIDs[i]) < 0) {
						final long id = cduIDs[i];
						// Drop old tag
						RangerProcessor.addImmediateAction(new Runnable() {
							public void run() {
								doDropTag(id);
							}
						});
					}
				}
				cduIDs = newcduids;	// Replace old with new list
				
				if (nextCDU >= cduIDs.length){
					nextCDU = 0;
				}
			}
			lastCatalogTimestamp = ts;
			updateReaderStatus(ReaderStatus.ACTIVE, false);
		} catch (SPMInterface.LoginException lx) {
			updateReaderStatus(ReaderStatus.ACCESSDENIED, false);
			lastCatalogTimestamp = 0;	// Reset catalog result
			Thread.sleep(10000);		// Delay 10 seconds before retry
		} catch (IOException iox) {
			updateReaderStatus(ReaderStatus.CONNECTFAILURE, false);
			RangerServer.info("updateCatalog() - " + iox.getMessage());
			lastCatalogTimestamp = 0;	// Reset catalog result
			Thread.sleep(10000);		// Delay 10 seconds before retry
		} catch (SAXException sx) {
			updateReaderStatus(ReaderStatus.CONFIGFAILURE, false);
			RangerServer.info("updateCatalog() - " + sx.getMessage());
			lastCatalogTimestamp = ts;
			Thread.sleep(10000);		// Delay 10 seconds before retry
		}
    }
  
    private void updateNextCDU(long ts) throws InterruptedException {
    	if (cduIDs.length != 0) {
    		try {
    			final CDUDetails cdudet = spm.getCDUDetails(cduIDs[nextCDU]);
    			if (cdudet != null) {
    				final long id = cduIDs[nextCDU];
					// Add/update tag
					RangerProcessor.addImmediateAction(new Runnable() {
						public void run() {
							doAddUpdateTag(id, cdudet);
						}
					});
    			}
    			nextCDU++;
    			if (nextCDU >= cduIDs.length){
    				nextCDU = 0;
    			}
    		} catch (SPMInterface.LoginException lx) {
    			updateReaderStatus(ReaderStatus.ACCESSDENIED, false);
    			Thread.sleep(10000);		// Delay 10 seconds before retry
    			return;
    		} catch (IOException iox) {
    			updateReaderStatus(ReaderStatus.CONNECTFAILURE, false);
    			Thread.sleep(10000);		// Delay 10 seconds before retry
    			return;
    		} catch (SAXException sx) {
    			updateReaderStatus(ReaderStatus.CONFIGFAILURE, false);
    			Thread.sleep(10000);		// Delay 10 seconds before retry
    			return;
    		}
    	}
    	lastCDUTimestamp = ts;
    }
    
    private void processSPM() {
    	while (!stop) {
    		try {
				long ts = System.nanoTime();
				// If we need to update catalog, request it
				long catInt = 1000000000L * (long) catalogPeriod;
				if ((lastCatalogTimestamp == 0) || (ts > (lastCatalogTimestamp + catInt))) {
					updateCatalog(ts);
				}
				// Check if we're due for next CDU request
				long cduInt = (1000000000L * (long) cduPeriod) / ((cduIDs.length == 0) ? 1 : cduIDs.length);
				if ((lastCDUTimestamp == 0) || (ts > (lastCDUTimestamp + cduInt))) {
					updateNextCDU(ts);
				}
				ts = System.nanoTime();	// Get current time
				long delaycat = 0;
				if (ts < (lastCatalogTimestamp + catInt)) {
					delaycat = lastCatalogTimestamp + catInt - ts;
				}
				long delaycdu = 0;
				if (ts < (lastCDUTimestamp + cduInt)) {
					delaycdu = lastCDUTimestamp + cduInt - ts;
				}
				long delay = Math.min(delaycdu, delaycat);
				if (delay > 0) {
					Thread.sleep(delay / 1000000L, (int)(delay % 1000000L));
				}
    		} catch (InterruptedException ix) {
    		}
    	}
    	updateReaderStatus(ReaderStatus.DISABLED, false);
    }
    /**
     * Set license for reader
     */
    public boolean setReaderLicense(boolean actval) {
    	return ReaderEntityDirectory.activateSPM(actval);
    }
    /**
     * Add reader status attributes (non-configuration)
     */
    public void addReaderStatusAttributes(Map<String, Object> map) {
    }
}
