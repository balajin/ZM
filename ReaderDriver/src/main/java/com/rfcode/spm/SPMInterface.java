package com.rfcode.spm;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rfcode.ranger.RangerServer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SPMInterface {
	private String hostname;
	private String userid;
	private String passwordHash;
	// State variables
	private String sid;	// Session ID
	private DocumentBuilder db;
	
	public static class LoginException extends IOException {
		public LoginException(String msg) {
			super(msg);
		}
	}
	// Generic constructor
	public SPMInterface() throws ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		db = dbf.newDocumentBuilder();
	}
	// Set hostname
	public void setHostname(String hostname) {
		this.hostname = hostname;
		this.sid = null;
	}
	// Set user ID
	public boolean setUserID(String userid) {
		this.userid = userid;
		this.sid = null;
		return true;
	}
	// Set password hash
	public boolean setPassword(String pwd) {
		this.sid = null;
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(pwd.getBytes(), 0, pwd.length());
			passwordHash = new BigInteger(1, m.digest()).toString(16);
			return true;
		} catch (NoSuchAlgorithmException x) {
			return false;
		}
	}
	// Log in to get session ID
	public void loginServer() throws IOException, SAXException {
		URL url = new URL("http://" + hostname + "/api/user.php/login?userName=" + userid + "&passwd=" + passwordHash);
		
		HttpURLConnection con = null;
		InputStream in = null;
		try {
			con = (HttpURLConnection) url.openConnection();
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			con.connect();
			in = con.getInputStream();
			Document doc = db.parse(in);
			in.close();
			in = null;
			NodeList nl = doc.getElementsByTagName("sid");
			Node node = nl.item(0);
			if ((node == null) || (!node.hasChildNodes())) {
				throw new LoginException("Login error: " + url);
			}
			sid = node.getFirstChild().getNodeValue();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
		}
	}
	private Document makeAPICall(String relativeURL, String field1, String val1, String field2, String val2, String field3, String val3) throws IOException, SAXException {
		String uri = "http://" + hostname + relativeURL;
		String nextsep = "?";
		if (field1 != null) {
			uri += nextsep + field1 + "=" + val1;
			nextsep = "&";
			if (field2 != null) {
				uri += nextsep + field2 + "=" + val2;
				if (field3 != null) {
					uri += nextsep + field3 + "=" + val3;
				}
			}
		}
		if (sid == null) {
			loginServer();
		}
		int retry = 0;
		while (retry < 2) {
			URL url = new URL(uri + nextsep + "sid=" + sid);
			HttpURLConnection con = null;
			InputStream in = null;
			try {
				con = (HttpURLConnection) url.openConnection();
				con.setDoOutput(true);
				con.setRequestMethod("GET");
				con.connect();
				in = con.getInputStream();
				FileOutputStream fos = new FileOutputStream(val1 + ".txt");
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					fos.write(buf, 0, len);
				}
				fos.close();
				in = new FileInputStream(val1 + ".txt");
				Document doc = db.parse(in);
				in.close();
				in = null;
				
				NodeList nl = doc.getElementsByTagName("faultstring");
				if (nl.getLength() == 0) {	// No fault string?
					// Log SPM result, if enabled
					RangerServer.logSPMMessage(uri, doc);
					return doc;
				}

				// Else, try new session
				retry++;
				loginServer();
			} finally {
				if (in != null) {
					in.close();
					in = null;
				}
			}
		}
		throw new IOException("Error on request: " + uri);
	}
	
	public static Node findChildByName(Node n, String field) {
		Node cn = n.getFirstChild();
		while (cn != null) {
			if (field.equals(cn.getNodeName())) {
				//System.out.println("Found " + field);
				return cn;
			}
			cn = cn.getNextSibling();
		}
		System.out.println("Not found " + field);
		
		return null;
	}
	public static List<Node> findChildrenByName(Node n, String field) {
		ArrayList<Node> rslt = new ArrayList<Node>();
		Node cn = n.getFirstChild();
		while (cn != null) {
			if (field.equals(cn.getNodeName())) {
				rslt.add(cn);
			}
			cn = cn.getNextSibling();
		}
		return rslt;
	}

	public static String getStringFromNode(Node n, String field, String def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		return cn.getTextContent();
	}
	public static int getIntFromNode(Node n, String field, int def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		String v = cn.getTextContent();
		try {
			return Integer.parseInt(v);
		} catch (NumberFormatException nfx) {
			return def;
		}
	}
	public static long getLongFromNode(Node n, String field, long def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		String v = cn.getTextContent();
		try {
			return Long.parseLong(v);
		} catch (NumberFormatException nfx) {
			return def;
		}
	}
	public static double getDoubleFromNode(Node n, String field, double def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		String v = cn.getTextContent();
		try {
			return Double.parseDouble(v);
		} catch (NumberFormatException nfx) {
			return def;
		}
	}
	public static boolean getBooleanFromNode(Node n, String field, boolean def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		String v = cn.getTextContent();
		if (v.equals("true"))
			return true;
		else if (v.equals("false")) 
			return false;
		else 
			return def;
	}
	// Parser for ISO8601 - not perfect, so we'll fix input if needed
	private static SimpleDateFormat datefmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
	
	public static Date getDateFromNode(Node n, String field, Date def) {
		Node cn = findChildByName(n, field);
		if (cn == null) return def;
		String v = cn.getTextContent();
		int len = v.length();
		if (v.endsWith("Z")) {	// Zulu?
			v = v.substring(0, len - 1) + "GMT-00:00";
		}
		else if ((len >= 16) && ((v.charAt(len-6) == '+') || (v.charAt(len-6) == '-'))) {
			v = v.substring(0, len - 6) + "GMT" + v.substring(len-6);
		}
		try {
			return datefmt.parse(v);
		} catch (ParseException px) {
			return def;
		}
	}

	public List<CDU>	getCDUCatalog() throws IOException, SAXException {
		Document doc = makeAPICall("/api/cdu.php/catalog", null, null, null, null, null, null);
		Node cn = doc.getFirstChild();
		NodeList nl = cn.getChildNodes();
		ArrayList<CDU> cduList = new ArrayList<CDU>();
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			if (!n.getNodeName().equals("cdu")) {
				continue;
			}
			CDU cdu = new CDU(n);
			
			cduList.add(cdu);
		}
		return cduList;
	}
	public CDUDetails	getCDUDetails(long id) throws IOException, SAXException {
		Document doc = makeAPICall("/api/cdu.php/detail", "id", Long.toString(id), null, null, null, null);
		Node n = doc.getFirstChild();
		if (!n.getNodeName().equals("detail")) {
			return null;
		}
		CDUDetails cdu = new CDUDetails(n);
		
		return cdu;
	}
	// Return true if metric, false if english
	public boolean getSPMTempUnit() throws IOException, SAXException {
		Document doc = makeAPICall("/api/system.php/getSetting", "setting", "SystemTempUnit", null, null, null, null);
		Node n = doc.getFirstChild();
		if (!n.getNodeName().equals("getSetting")) {
			return false;
		}
		return ("0".equals(n.getTextContent()));
	}
	public static void main(String[] args) {
		
		try {
			SPMInterface spm = new SPMInterface();
			
			spm.setHostname("66.214.208.101");
			spm.setUserID("rfcode");
			spm.setPassword("RF$4d3v");
			
			spm.getSPMTempUnit();
			//Document doc = spm.makeAPICall("/api/user.php/catalog", null, null, null, null, null, null);
			//System.out.println("Response:");
			//spm.dumpDoc(doc, 0);
			
			List<CDU> cdus = spm.getCDUCatalog();
			
			for (CDU cdu : cdus) {
				CDUDetails det = spm.getCDUDetails(cdu.id);
				System.out.println(det.id + ": " + det.name);
			}
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (SAXException e) {
			System.out.println(e.getMessage());
		} catch (ParserConfigurationException e) {
			System.out.println(e.getMessage());
		}
	}
}
