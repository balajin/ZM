package com.rfcode.spm;

import java.util.Date;

import org.w3c.dom.Node;

// Details of CDU alarms (present or history)
public class CDUAlarm {
	// Severity (critical, warning, normal, unreachable, maintenance) of the alarm
	public final String severity;
	// Alarm descriptive text
	public final String message;
	// Start time of the alarm (only valid for alarm history)
	public final Date start;
	// End time of the alarm (only valid for alarm history)
	public final Date end;
	
	public CDUAlarm(Node n) {
		// Severity (critical, warning, normal, unreachable, maintenance) of the alarm
		this.severity = SPMInterface.getStringFromNode(n, "severity", "");
		// Alarm descriptive text
		this.message = SPMInterface.getStringFromNode(n, "message", "");
		// Start time of the alarm (only valid for alarm history)
		this.start = SPMInterface.getDateFromNode(n, "start", null);
		// End time of the alarm (only valid for alarm history)
		this.end = SPMInterface.getDateFromNode(n, "end", null);
	}
}
