package com.rfcode.spm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.mantis2.MantisIIReader;
import com.rfcode.drivers.readers.mantis2.MantisIIReaderFactory;

public class SPMReaderFactory implements ReaderFactory {
	public static final String FACTORYID = "SPM";
	public static final String FACTORYLABEL = "SPM Server API";
	public static final String FACTORYDESC = "Sentry Power Manager Server support, via HTTP/REST API.";
	
	public static final String ATTRIB_UPDATEPERIOD = "cduperiod";
	public static final String ATTRIB_LISTUPDATEPERIOD = "catalogperiod";
	public static final Integer ATTRIB_UPDATEPERIOD_DEFAULT = 60;	// Once per minute
	public static final Integer ATTRIB_LISTUPDATEPERIOD_DEFAULT = 600;	// Every 10 minutes; 
    /** Default offline reader link ageout */
    public static final Integer DEFAULT_OFFLINELINKAGEOUT = 300;
	
	private static final String[] TAGTYPES = { SPMCDUTagType.TAGTYPEID };
	private String factoryID;
    private ArrayList<ReaderLifecycleListener> lifecycle_listeners = new ArrayList<ReaderLifecycleListener>();
	private static final String[] ATTRIBS = {
		MantisIIReaderFactory.ATTRIB_READERNAME, MantisIIReaderFactory.ATTRIB_HOSTNAME, MantisIIReaderFactory.ATTRIB_PORTNUM,
		MantisIIReaderFactory.ATTRIB_USERID, MantisIIReaderFactory.ATTRIB_PASSWORD,
		ATTRIB_UPDATEPERIOD, ATTRIB_LISTUPDATEPERIOD,
        MantisIIReaderFactory.ATTRIB_OFFLINELINKAGEOUT
	};
    private static final Object[] ATTRIB_DEFAULTS = { "", "localhost", 80, 
        "admin", "admin", 
        ATTRIB_UPDATEPERIOD_DEFAULT, ATTRIB_LISTUPDATEPERIOD_DEFAULT,
        DEFAULT_OFFLINELINKAGEOUT
    };

    private HashMap<String, Object> attrib_def;
    private String[] attribs;

    public SPMReaderFactory() {
        setID(FACTORYID);
        attrib_def = new HashMap<String, Object>();
		for (int i = 0; i < ATTRIBS.length; i++) {
			attrib_def.put(ATTRIBS[i], ATTRIB_DEFAULTS[i]);
		}
		attribs = attrib_def.keySet().toArray(new String[0]);

    }
    
	@Override
	public String getID() {
		return this.factoryID;
	}

	@Override
	public void setID(String id) {
		this.factoryID = id;
	}

	@Override
	public void init() throws DuplicateEntityIDException, BadParameterException {
        ReaderEntityDirectory.addEntity(this);
	}

	@Override
	public void cleanup() {
        if (lifecycle_listeners != null) {
            lifecycle_listeners.clear();
            lifecycle_listeners = null;
        }
	}

	@Override
	public String[] getReaderAttributeIDs() {
		return attribs;
	}

	@Override
	public String getReaderFactoryLabel() {
		return FACTORYLABEL;
	}

	@Override
	public String getReaderFactoryDescription() {
		return FACTORYDESC;
	}

	@Override
	public Map<String, Object> getDefaultReaderAttributes() {
        return new HashMap<String,Object>(attrib_def);
	}

	@Override
	public Reader createReader() {
		return new SPMReader(this);
	}

	@Override
	public String[] getSupportedTagTypeIDs() {
		return TAGTYPES;
	}

	@Override
	public String[] getTagLinkAttributeIDs() {
		return new String[0];
	}

	@Override
	public void addReaderLifecycleListener(ReaderLifecycleListener listener) {
		lifecycle_listeners.add(listener);
	}

	@Override
	public void removeReaderLifecycleListener(ReaderLifecycleListener listener) {
        lifecycle_listeners.remove(listener);
	}

	@Override
	public int getVersion() {
		return 1;
	}

    /**
     * Call reader lifecycle listeners for reader create
     * 
     * @param reader -
     *            reader created
     */
    void callReaderLifecycleForCreate(Reader reader) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleCreate(reader);
        }
    }
    /**
     * Call reader lifecycle listeners for reader delete
     * 
     * @param reader -
     *            reader deleted
     */
    void callReaderLifecycleForDelete(Reader reader) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleDelete(reader);
        }
    }
    /**
     * Call reader lifecycle listeners for reader enable/disable
     * 
     * @param reader -
     *            reader enabled/disabled
     * @param enable -
     *            true if enabled, false if disabled
     */
    void callReaderLifecycleForEnable(Reader reader, boolean enable) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleEnable(reader, enable);
        }
    }

	public int getChannelCount() {
		return 1;
	}
}
