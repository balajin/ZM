package com.rfcode.spm;

import org.w3c.dom.Node;

public class CDUOutlet {
	// Identifier of the outlet
	public final long id;
	// User assigned name of the outlet
	public final String name;
	// Contrived name based on tower/infeed index (A,B,...) and outlet index (1,2,...) ie: AA1, AB2
	public final String absName;
	// User assigned notes for this outlet
	public final String notes;
	// Outlet status (on, off, ...)
	public final String status;
	// Outlet load status (normal, loadLow, ...)
	public final String loadStatus;
	// Outlet control state (idleOff, idleOn, ...)
	public final String controlState;
	// User assigned string describing the asset attached to the outlet
	public final String asset;
	// User assigned url of the asset device attached to the outlet
	public final String url;
	// User assigned device type of the asset device attached to the outlet: (Hub Switch Router Gateway System KVM Display Environmental Monitor Camera UPS Other
	public final String device;
	// User assigned height (in rack units) of the device attached to the outlet
	public final int height;
	// User assigned position (in rack units) of the device attached to the outlet
	public final int position;
	// Bitfield describing the outlet's capabilities: 0x8000 (onSense) 0x4000 (loadSense) 0x2000 (powerControl) 0x1000 (shutdown) 0x0800 (defaultOn) 0x0400 (ownInfeed) 0x0200 (fusedBranch) 0x0100 (voltageSense) 0x0080 (powerSense)
	public final int capabilities;
	public static final int CAPABILITIES_ONSENSE = 0x8000;
	public static final int CAPABILITIES_LOADSENSE = 0x4000;
	public static final int CAPABILITIES_POWERCONTROL = 0x2000;
	public static final int CAPABILITIES_SHUTDOWN = 0x1000;
	public static final int CAPABILITIES_DEFAULTON = 0x0800;
	public static final int CAPABILITIES_OWNINFEED = 0x0400;
	public static final int CAPABILITIES_FUSEDBRANCH = 0x0200;
	public static final int CAPABILITIES_VOLTAGESENSE = 0x0100;
	public static final int CAPABILITIES_POWERSENSE = 0x0080;
	// Identifier of the system this outlet is attached to
	public final long systemId;
	// Identifier of the infeed this outlet is attached to
	public final long infeedId;
	// Identifier of the tower this outlet is attached to
	public final long towerId;
	// Current outlet load (amps)
	public final double load;
	// User assigned low load threshold (amps)
	public final int loadLowThresh;
	// User assigned high load threshold (amps)
	public final int loadHighThresh;
	// Current outlet voltage (volts)
	public final double voltage;
	// Current outlet power consumption (watts)
	public final int power;
	// User assigned low power threshold (watts)
	public final int powerLowThresh;
	// User assigned high power threshold (watts)
	public final int powerHighThresh;
	// Outlet load capacity (amps)
	public final int capacity;
	// Current outlet apparent power consumption (volt-amps)
	public final int apparentPower;
	// Current outlet power factor
	public final double powerFactor;
	// Current outlet crest factor
	public final double crestFactor;
	// Current outlet watt hours
	public final long wattHours;
	
	public CDUOutlet(Node n) {
		// Identifier of the outlet
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// User assigned name of the outlet
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Contrived name based on tower/infeed index (A,B,...) and outlet index (1,2,...) ie: AA1, AB2
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User assigned notes for this outlet
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// Outlet status (on, off, ...)
		this.status = SPMInterface.getStringFromNode(n, "status", "");
		// Outlet load status (normal, loadLow, ...)
		this.loadStatus = SPMInterface.getStringFromNode(n, "loadStatus", "");
		// Outlet control state (idleOff, idleOn, ...)
		this.controlState = SPMInterface.getStringFromNode(n, "controlState", "");
		// User assigned string describing the asset attached to the outlet
		this.asset = SPMInterface.getStringFromNode(n, "asset", "");
		// User assigned url of the asset device attached to the outlet
		this.url = SPMInterface.getStringFromNode(n, "url", "");		
		// User assigned device type of the asset device attached to the outlet: (Hub Switch Router Gateway System KVM Display Environmental Monitor Camera UPS Other
		this.device = SPMInterface.getStringFromNode(n, "device", "");		
		// User assigned height (in rack units) of the device attached to the outlet
		this.height = SPMInterface.getIntFromNode(n, "height", -1);
		// User assigned position (in rack units) of the device attached to the outlet
		this.position = SPMInterface.getIntFromNode(n, "position", -1);		
		// Bitfield describing the outlet's capabilities: 0x8000 (onSense) 0x4000 (loadSense) 0x2000 (powerControl) 0x1000 (shutdown) 0x0800 (defaultOn) 0x0400 (ownInfeed) 0x0200 (fusedBranch) 0x0100 (voltageSense) 0x0080 (powerSense)
		this.capabilities = SPMInterface.getIntFromNode(n, "capabilities", -1);
		// Identifier of the system this outlet is attached to
		this.systemId = SPMInterface.getLongFromNode(n, "systemId", -1);
		// Identifier of the infeed this outlet is attached to
		this.infeedId = SPMInterface.getLongFromNode(n, "infeedId", -1);		
		// Identifier of the tower this outlet is attached to
		this.towerId = SPMInterface.getLongFromNode(n, "towerId", -1);
		// Current outlet load (amps)
		this.load = SPMInterface.getDoubleFromNode(n, "load", -1);
		// User assigned low load threshold (amps)
		this.loadLowThresh = SPMInterface.getIntFromNode(n, "loadLowThresh", -1);
		// User assigned high load threshold (amps)
		this.loadHighThresh = SPMInterface.getIntFromNode(n, "loadHighThresh", -1);		
		// Current outlet voltage (volts)
		this.voltage = SPMInterface.getDoubleFromNode(n, "voltage", -1);
		// Current outlet power consumption (watts)
		this.power = SPMInterface.getIntFromNode(n, "power", -1);
		// User assigned low power threshold (watts)
		this.powerLowThresh = SPMInterface.getIntFromNode(n, "powerLowThresh", -1);
		// User assigned high power threshold (watts)
		this.powerHighThresh = SPMInterface.getIntFromNode(n, "powerHighThresh", -1);
		// Outlet load capacity (amps)
		this.capacity = SPMInterface.getIntFromNode(n, "capacity", -1);
		// Current outlet apparent power consumption (volt-amps)
		this.apparentPower = SPMInterface.getIntFromNode(n, "apparentPower", -1);
		// Current outlet power factor
		this.powerFactor = SPMInterface.getDoubleFromNode(n, "powerFactor", -1);
		// Current outlet crest factor
		this.crestFactor = SPMInterface.getDoubleFromNode(n, "crestFactor", -1);
		// Current outlet watt hours
		this.wattHours = SPMInterface.getLongFromNode(n, "wattHours", -1);		
	}
	public boolean onSense() {
		return (capabilities & CAPABILITIES_ONSENSE) != 0;
	}
	public boolean loadSense() {
		return (capabilities & CAPABILITIES_LOADSENSE) != 0;
	}
	public boolean powerControl() {
		return (capabilities & CAPABILITIES_POWERCONTROL) != 0;
	}
	public boolean shutdown() {
		return (capabilities & CAPABILITIES_SHUTDOWN) != 0;
	}
	public boolean defaultOn() {
		return (capabilities & CAPABILITIES_DEFAULTON) != 0;
	}
	public boolean ownInfeed() {
		return (capabilities & CAPABILITIES_OWNINFEED) != 0;
	}
	public boolean fusedBranch() {
		return (capabilities & CAPABILITIES_FUSEDBRANCH) != 0;
	}
	public boolean voltageSense() {
		return (capabilities & CAPABILITIES_VOLTAGESENSE) != 0;
	}
	public boolean powerSense() {
		return (capabilities & CAPABILITIES_POWERSENSE) != 0;
	}
}
