package com.rfcode.spm;

import java.util.List;

import org.w3c.dom.Node;

// Details for specific CDU (returned via /api/cdu.php/detail)
public class CDUDetails extends CDU {
	// Community string for gets
	public final String getCommunity;
	// Community string for sets
	public final String setCommunity;
	// Environmental monitoring devices
	public final CDUEnvMon[] envMon;
	// Temperature sensors
	public final CDUSensor[] sensor;
	// Contact closure sensors
	public final CDUContactClosure[] contactClosure;
	// Towers for CDU
	public final CDUTower[] tower;
	// In feeds for CDU
	public final CDUInfeed[] infeed;
	// Outlets for CDU
	public final CDUOutlet[] outlet;
	// Alarms for CDU
	public final CDUAlarm[] alarm;
	// Alarm history (last 20) for CDU
	public final CDUAlarm[] alarmHistory;
	// User action history
	public final CDUAction[] action;
	// Calculated total power from phases
	public Double totalphasepower;
	public Double totalphaseapppower;
	
	public CDUDetails(Node n) {
		super(n);
		// Community string for gets
		this.getCommunity = SPMInterface.getStringFromNode(n, "getCommunity", "");
		// Community string for sets
		this.setCommunity = SPMInterface.getStringFromNode(n, "setCommunity", "");
		// Find envMon nodes
		List<Node> nl = SPMInterface.findChildrenByName(n, "envMon");
		envMon = new CDUEnvMon[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			envMon[i] = new CDUEnvMon(nl.get(i));
		}
		// Find sensor nodes
		nl = SPMInterface.findChildrenByName(n, "sensor");
		sensor = new CDUSensor[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			sensor[i] = new CDUSensor(nl.get(i));
		}
		// Find contactClosure nodes
		nl = SPMInterface.findChildrenByName(n, "contactClosure");
		contactClosure = new CDUContactClosure[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			contactClosure[i] = new CDUContactClosure(nl.get(i));
		}
		// Find tower nodes
		nl = SPMInterface.findChildrenByName(n, "tower");
		tower = new CDUTower[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			tower[i] = new CDUTower(nl.get(i));
		}
		// Find infeed nodes
		nl = SPMInterface.findChildrenByName(n, "infeed");
		infeed = new CDUInfeed[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			infeed[i] = new CDUInfeed(nl.get(i));
		}
		// Find outlet nodes
		nl = SPMInterface.findChildrenByName(n, "outlet");
		outlet = new CDUOutlet[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			outlet[i] = new CDUOutlet(nl.get(i));
		}
		// Find alarm nodes
		nl = SPMInterface.findChildrenByName(n, "alarm");
		alarm = new CDUAlarm[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			alarm[i] = new CDUAlarm(nl.get(i));
		}
		// Find alarmHistory nodes
		nl = SPMInterface.findChildrenByName(n, "alarmHistory");
		alarmHistory = new CDUAlarm[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			alarmHistory[i] = new CDUAlarm(nl.get(i));
		}
		// Find action nodes
		nl = SPMInterface.findChildrenByName(n, "action");
		action = new CDUAction[nl.size()];
		for (int i = 0; i < nl.size(); i++) {
			action[i] = new CDUAction(nl.get(i));
		}
	}
	
}
