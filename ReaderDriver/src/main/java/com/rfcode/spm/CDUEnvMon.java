package com.rfcode.spm;

import org.w3c.dom.Node;

// Details for CDU environmental sensor
public class CDUEnvMon {
	// ID of the environmental sensor
	public final long id;
	// Name for sensor
	public final String name;
	// Generated name for sensor
	public final String absName;
	// User notes for sensor
	public final String notes;
	// Sensor status (normal, noComm)
	public final String status;
	// Water sensor name
	public final String waterSensorName;
	// Water sensor status (normal, alarm, noComm)
	public final String waterSensorStatus;
	// A/D sensor name
	public final String adcName;
	// A/D sensor status (normal, alarm, noComm)
	public final String adcStatus;
	// A/D sensor count
	public final int adcCount;
	// A/D low value threshold setting
	public final int adcLowThresh;
	// A/D high value threshold setting
	public final int adcHighThresh;
	
	public CDUEnvMon(Node n) {
		// ID of the environmental sensor
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// Name for sensor
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// Generated name for sensor
		this.absName = SPMInterface.getStringFromNode(n, "absName", "");
		// User notes for sensor
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// Sensor status (normal, noComm)
		this.status = SPMInterface.getStringFromNode(n, "status", "");
		// Water sensor name
		this.waterSensorName = SPMInterface.getStringFromNode(n, "waterSensorName", "");
		// Water sensor status (normal, alarm, noComm)
		this.waterSensorStatus = SPMInterface.getStringFromNode(n, "waterSensorStatus", "");
		// A/D sensor name
		this.adcName = SPMInterface.getStringFromNode(n, "adcName", "");
		// A/D sensor status (normal, alarm, noComm)
		this.adcStatus = SPMInterface.getStringFromNode(n, "adcStatus", "");
		// A/D sensor count
		this.adcCount = SPMInterface.getIntFromNode(n, "adcCount", -1);
		// A/D low value threshold setting
		this.adcLowThresh = SPMInterface.getIntFromNode(n, "adcLowThresh", -1);
		// A/D high value threshold setting
		this.adcHighThresh = SPMInterface.getIntFromNode(n, "adcHighThresh", -1);
	}
}
