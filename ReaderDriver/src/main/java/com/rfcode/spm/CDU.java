package com.rfcode.spm;
import org.w3c.dom.Node;

public class CDU {
	// CPU ID
	public final long id;
	// Parent location ID
	public final long parentLoc;
	// Parent cabinet ID
	public final long parentCab;
	// CDU name
	public final String name;
	// CDU IP address
	public final String ipAddress;
	// CDU network address
	public final String network;
	// CDU web port
	public final int port;
	// SNMP enterprise ID
	public final int enterpriseID;
	// SNMP enterprise name
	public final String enterpriseName;
	// Notes about CDU
	public final String notes;
	// Height in U
	public final int height;
	// Position in U
	public final int position;
	// Status of CDU
	public final String status;
	// CDU serial number
	public final String serial;
	// CDU firmware versionn
	public final String version;
	// CDU user contact
	public final String contact;
	// CDU description
	public final String description;
	// Watts per unit area
	public final int wattsPerUnitArea;
	// CDU area (m^2)
	public final double area;
	// Area units    (0=m^2, 1=ft^2) 
	public final int areaUnit;
	// Maximum active infeed power (watts)
	public final int power;
	// Total power consumption of all feeds (watts)
	public final int totalPower;
	// Maximum infeed load (amps)
	public final double current;
	// User-defined capacity (watts)
	public final int powerCapacity;
	// Power factor used for power calculations
	public final double powerFactor;
	// Max temperature reported by CDU's sensors (C)
	public final double maxTemp;
	// Max humidity reported by CDU's sensors (% RH)
	public final int maxHumid;
	
	public CDU(Node n){
		// CDU ID
		this.id = SPMInterface.getLongFromNode(n, "id", -1);
		// Parent location ID
		this.parentLoc = SPMInterface.getLongFromNode(n, "parentLoc", -1);
		// Parent cabinet ID
		this.parentCab = SPMInterface.getLongFromNode(n, "parentCab", -1);
		// CDU name
		this.name = SPMInterface.getStringFromNode(n, "name", "");
		// CDU IP address
		this.ipAddress = SPMInterface.getStringFromNode(n, "ipaddress", "");
		// CDU network address
		this.network = SPMInterface.getStringFromNode(n, "network", "");
		// CDU web port
		this.port = SPMInterface.getIntFromNode(n, "port", -1);
		// SNMP enterprise ID
		this.enterpriseID = SPMInterface.getIntFromNode(n, "enterpriseId", -1);
		// SNMP enterprise name
		this.enterpriseName = SPMInterface.getStringFromNode(n, "enterpriseName", "");
		// Notes about CDU
		this.notes = SPMInterface.getStringFromNode(n, "notes", "");
		// Height in U
		this.height = SPMInterface.getIntFromNode(n, "height", -1);
		// Position in U
		this.position = SPMInterface.getIntFromNode(n, "position", -1);
		// Status of CDU
		this.status = SPMInterface.getStringFromNode(n, "status", "");
		// CDU serial number
		this.serial = SPMInterface.getStringFromNode(n, "serial", "");
		// CDU firmware versionn
		this.version = SPMInterface.getStringFromNode(n, "version", "");
		// CDU user contact
		this.contact = SPMInterface.getStringFromNode(n, "contact", "");
		// CDU description
		this.description = SPMInterface.getStringFromNode(n, "description", "");
		// Watts per unit area
		this.wattsPerUnitArea = SPMInterface.getIntFromNode(n, "wattsPerUnitArea", -1);
		// CDU area (m^2)
		this.area = SPMInterface.getDoubleFromNode(n, "area", -1.0);
		// Area units    (0=m^2, 1=ft^2) 
		this.areaUnit = SPMInterface.getIntFromNode(n, "areaUnit", -1);
		// Maximum active infeed power (watts)
		this.power = SPMInterface.getIntFromNode(n, "power", -1);
		// Total power consumption of all feeds (watts)
		this.totalPower = SPMInterface.getIntFromNode(n, "totalPower", -1);
		// Maximum infeed load (amps)
		this.current = SPMInterface.getDoubleFromNode(n, "current", -1.0);
		// User-defined capacity (watts)
		this.powerCapacity = SPMInterface.getIntFromNode(n, "powerCapacity", -1);
		// Power factor used for power calculations
		this.powerFactor = SPMInterface.getDoubleFromNode(n, "powerFactor", -1.0);
		// Max temperature reported by CDU's sensors (C)
		this.maxTemp = SPMInterface.getDoubleFromNode(n, "maxTemp", Double.MIN_VALUE);
		// Max humidity reported by CDU's sensors (% RH)
		this.maxHumid = SPMInterface.getIntFromNode(n, "maxHumid", -1);
	}
}	
