package com.rfcode.util.http;

import java.io.InputStream;
import java.io.IOException;
/**
 * Chunked input stream handler/translator - handles decode of HTTP
 * 1.1 chunked encoded streams.
 *
 * @author Mike Primm
 */
public class ChunkedInputStream extends InputStream {
    private static final int CR = 13;
    private static final int LF = 10;

    /* Raw input stream */
    private InputStream in;
    /* Number of bytes remaining in chunk */
    private int bytes_in_chunk;
    
    /**
     * Constructor - make wrapper for raw stream
     * @param rawin - encoded input stream
     */
    public ChunkedInputStream(InputStream rawin) {
        in = rawin;
        bytes_in_chunk = 0;
    }
    /**
     * Error - kill stream
     */
    private void killStream(String err) throws IOException {
        close();
        throw new IOException(err);
    }
    /**
     * Read next byte - returns next decoded byte from stream
     * @return data byte, or -1 if end or error
     */
    public int read() throws IOException {
        int c = -1; /* Assume bad */
        /* If bytes remaining in chunk is zero, need to get length of chunk */
        while((in != null) && (bytes_in_chunk == 0)) {
            int len = 0;
            while(bytes_in_chunk == 0) {
                int digit = in.read();      /* Get next byte */
                if((digit >= '0') && (digit <= '9')) {
                    len = (len * 16) + (digit - '0');
                }
                else if((digit >= 'A') && (digit <= 'F')) {
                    len = (len * 16) + (digit - 'A' + 10);                    
                }
                else if((digit >= 'a') && (digit <= 'f')) {
                    len = (len * 16) + (digit - 'a' + 10);                    
                }
                else if(c == CR) {  /* If CR. must have LF after */
                    c = in.read();
                    if(c == LF) {   /* We're good */
                        bytes_in_chunk = len;       
                        /* If zero length chunk, end of stream */
                        if(bytes_in_chunk == 0) {
                            close();
                        }            
                    }
                    else {
                        killStream("Missing LF after CR in chunk len");
                    }
                }
                else if(c == -1) {  /* EOF? */
                    close();
                }
                else {  /* Otherwise, bad length format */
                    killStream("Invalid character in chunk length");
                }
            }
        }
        /* If we have data to return, return it */
        if((in != null) && (bytes_in_chunk > 0)) {
            c = in.read();  /* Get next one */
            bytes_in_chunk--;   /* Drop count */
            if(bytes_in_chunk == 0) {   /* Last one?  Skip past CRLF */
                if(in.read() != CR) {
                    killStream("Missing trailing CR");
                }
                else if(in.read() != LF) {
                    killStream("Missing trailing LF");
                }
            }
        }
        return c;
    }
    /**
     * Read up to N bytes
     */
    public int read(byte[] b, int off, int len) throws IOException {
        int rlen = -1;
        if((in != null) && (len < bytes_in_chunk)) {  /* If less than rest of the chunk */
            rlen = in.read(b, off, len);    /* Do direct read - should be fast */
            if(rlen < 0) {  /* end of stream? */
                close();
            }
            else {
                bytes_in_chunk -= rlen; /* Reduce chunk space remaining */
            }
        }
        else {  /* Else, just let default handle it by looping on single reads */
            rlen = super.read(b, off, len);
        }
        return rlen;
    }
    /**
     * Return available to read
     */
    public int available() throws IOException {
        if(in == null)
            throw new IOException("Stream closed");
        if(bytes_in_chunk > 0) {
            int rawavail = in.available();
            /* Return how much we can read without blocking, up to rest of chunk */
            return ((bytes_in_chunk < rawavail)?bytes_in_chunk:rawavail);
        }
        return 0;
    }

    /**
     * Close the stream
     */
    public void close() throws IOException {
        in = null;
        bytes_in_chunk = -1;
    }
}
