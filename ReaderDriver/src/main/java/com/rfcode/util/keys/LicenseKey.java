//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2007-2010. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//Class Name:          LicenseKey
//
//Written By:          Mike Primm
//------------------------------ tabstop = 4 ----------------------------------
package com.rfcode.util.keys;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Date;
import java.util.Calendar;
/**
 * License key management class - used for generating and interpreting license keys
 *
 * Each key is encoded as a 16 character string, with each character representing 4
 * bits of data.  Each character position has a base offset used to determine what
 * 16 characters are appropriate for the position - this is used as a basic key
 * validation check.  The resulting 64-bit data word is a DES encrypted structure,
 * based around a fixed key, which decodes to the following structure:
 * 
 *  For format code 000:
 * 1) 1 byte key type: 3 bit format code (000), 1 bit flag for transient keys, 4 bit prod id
 * 2) 2 byte days-since-1/1/2000 for eval key expiration: 0000 if non-expiring (high->low)
 * 3) 2 byte license count (high->low) - 0000 if unlimited
 * 4) 3 byte license index (high->low)
 * 
 *  Other format codes currently reserved.
 * 
 * @author Mike Primm
 */
public class LicenseKey {
    /** Key format 0 */
    public static final int KEY_FORMAT_0 = 0;
    /** Product ID - ZoneManager */
    public static final int PROD_ZONEMGR = 0;
    /** Product ID - AssetServer */
    public static final int PROD_ASSETSERVER = 1;
    /** Product ID - AssetServer OEM */
    public static final int PROD_ASSETSERVER_OEM = 2;
    /** Product ID - AssetServer BIRT Support */
    public static final int PROD_ASSETSERVER_JMX = 3;
    /** Product ID - AssetServer BIRT Support */
    public static final int PROD_ASSETSERVER_BIRT = 4;
    /** Product ID - AssetServer BACnet Support */
    public static final int PROD_ASSETSERVER_BACNET = 5;
    /** Product ID - AssetServer Modbus Support */
    public static final int PROD_ASSETSERVER_MODBUS = 6;
    /** Product ID - ZoneMgr SPM Support */
    public static final int PROD_ZONEMGR_SPM = 7;
    /** Product ID - AssetServer SPM Support */
    public static final int PROD_ASSETSERVER_SPM = 8;
    /** Product ID - AssetServer Analytics Support */
    public static final int PROD_ASSETSERVER_ANALYTICS = 9;
    
    /** License count - unlimited */
    public static final int COUNT_UNLIMITED = 0;
    
    /** No expiration time */
    public static final int NON_EXPIRING = 0;
    
    /* Decoded key attributes */
    private int keyfmt; /* Format code */
    private boolean tempkey; /* Transient key */
    private int prodid; /* Product ID */
    private Date exp_date;  /* Expiration date, if any */
    private int license_cnt;    /* Licence count */
    private int license_idx;    /* License index */
    private boolean is_valid;   /* Valid license */
    private String key;     /* Key string for license */
    
    /* Private DES key used to encrypt and decrypt key bytes */
    private static final byte[] keybyte = {
        (byte)114, (byte)72, (byte)47, (byte)63, (byte)16, (byte)59, (byte)50, (byte)3 
    };
    /* Character table used for nibble to char mapping */
    private static final String charmap = "3456789ABCDEFGHJKLMNPQRSTUVWXY";
    /* Table of prime increments used for nibble to char mapping :
     * must be relatively prime to length of charmap (none of the 
     * prime factors of the charmap length can be here) */
    private static final int[] incmap = { 
        47, 31, 61, 37, 
        53, 13, 17, 43, 
        23, 41, 67, 7, 
        29, 19, 59, 11
    };
    /* Nibble to character mapping */
    private static char nibbleToChar(int nibble, int position) {
        return charmap.charAt((incmap[position] * (nibble+1)) % charmap.length());
    }
    /* Char to nibble mapping : returns -1 if bad char for position */
    private static int charToNibble(char c, int position) {
        if((position < 0) || (position >= incmap.length))
            return -1;
        c = Character.toUpperCase(c);   /* Make upper case */
        for(int i = 0; i < 16; i++) {
            if(c == charmap.charAt((incmap[position] * (i+1)) % charmap.length())) {
                return i;
            }
        }
        return -1;
    }
    /* Read key string, and return decoded bytes (or null if invalid) */
    private static byte[] keyToBytes(String k) {
        int ccnt = 0;
        byte[] msg = new byte[8];   /* Valid one is 8 bytes */
        for(int i = 0; i < k.length(); i++) {
            char c = k.charAt(i);
            int v = charToNibble(c, ccnt); /* Decode character */
            if(v >= 0) {    /* If valid, accumulate it */
                msg[ccnt/2] |= (v << (4 - (4 * (ccnt % 2))));
                ccnt++;
            }
            else if(c != '-') { /* Invalid char? */
                return null;
            }
        }
        if(ccnt != 16) { /* Must be 16 valid characters */
            msg = null;
        }
        return msg;
    }
    /* Take bytes and generate key string in standard format */
    private static String bytesToKey(byte[] k) {
        StringBuilder sb = new StringBuilder();
        if(k.length != 8)   /* Must be 8 bytes */
            return null;
        for(int i = 0; i < 16; i++) {   /* 16 chars */
            int nibble = 0x0F & (k[i/2] >> (4 - (4*(i%2))));
            sb.append(nibbleToChar(nibble, i));
            if(((i % 4) == 3) && (i < 15)) {
                sb.append("-");
            }
        }
        return sb.toString();
    }
    /* Decrypt key bytes */
    private static byte[] decrypt(byte[] bytes) throws GeneralSecurityException {
        SecretKeySpec key = new SecretKeySpec(keybyte, "DES");
        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] clrtext = new byte[8];
        cipher.doFinal(bytes, 0, bytes.length, clrtext, 0);
        return clrtext;
    }
    /* Encrypt key bytes */
    private static byte[] encrypt(byte[] bytes) throws GeneralSecurityException{
        SecretKeySpec key = new SecretKeySpec(keybyte, "DES");        
        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] ciphertext = new byte[8];
        cipher.doFinal(bytes, 0, bytes.length, ciphertext, 0);
        return ciphertext;
    }
    /**
     * Constructor - make key from string
     * @param keystr - key string
     */
    public LicenseKey(String keystr) {
        key = keystr;   /* Save string */
        byte[] decoded = keyToBytes(keystr);    /* Decode it */
        if(decoded == null) /* Quit if bad */
            return;
        key = bytesToKey(decoded);  /* Make key normalized */
        byte[] clear = null;
        try {
            clear = decrypt(decoded);    /* Unencrypt it */
        } catch (GeneralSecurityException gsx) {
            return;
        }
        /* Get fields */
        keyfmt = (clear[0] >> 5) & 0x7;
        tempkey = ((clear[0] & 0x10) != 0);
        prodid = (clear[0] & 0x0F);
        int expdays = ((0xFF & (int)clear[1])* 256) + (0xFF & (int)clear[2]);
        if(expdays != NON_EXPIRING) {
            Calendar c = Calendar.getInstance();
            c.set(2000, 0, 1);
            c.add(Calendar.DAY_OF_MONTH, expdays);
            exp_date = c.getTime();
        }
        license_cnt = ((0xFF & (int)clear[3])* 256) + (0xFF & (int)clear[4]);
        license_idx = ((0xFF & (int)clear[5])* 65536) + 
            ((0xFF & (int)clear[6])* 256) + (0xFF & (int)clear[7]);
        is_valid = true;
        key = bytesToKey(decoded);  /* Produce normalized key */
    }
    /**
     * Constructor - make key from key attributes
     * @param fmt - format code (0-7)
     * @param tmp - is temporary key?
     * @param prod_id - product ID (0-15)
     * @param expdays - days from now to expire (<=0 if doesn't expire)
     * @param cnt - license count
     * @param idx - license index
     */
    public LicenseKey(int fmt, boolean tmp, int prod_id, int expdays, int cnt,
        int idx) {
        keyfmt = fmt;   /* Save parameters */
        tempkey = tmp;
        prodid = prod_id;
        if(expdays > NON_EXPIRING) {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, expdays);
            exp_date = c.getTime(); 
        }
        license_cnt = cnt;
        license_idx = idx;
        /* Validate parameters */
        if(keyfmt != KEY_FORMAT_0) {   /* Only zero valid so far */
            return;
        }
        if((prodid < 0) || (prodid > 15)) { /* 0-15 product ID */
            return;
        }
        if((license_cnt < 0) || (license_cnt > 65535))
            return;
        if((license_idx < 0) || (license_idx >= (1 << 24)))
            return;
        /* Make encoded buffer */
        byte[] b = new byte[8];
        b[0] = (byte)((keyfmt << 5) | (tempkey?0x10:0) | prodid);
        if(exp_date != null) {
            Calendar c2000 = Calendar.getInstance();
            c2000.set(2000, 0, 1);
            int days = (int)((exp_date.getTime() - c2000.getTimeInMillis() + 12*3600*1000) /
                (24*3600*1000));
            b[1] = (byte)((days>>8) & 0xFF);
            b[2] = (byte)(days & 0xFF);
        }
        b[3] = (byte)((license_cnt >> 8) & 0xFF);
        b[4] = (byte)(license_cnt & 0xFF);
        b[5] = (byte)((license_idx >> 16) & 0xFF);
        b[6] = (byte)((license_idx >> 8) & 0xFF);
        b[7] = (byte)(license_idx & 0xFF);

        /* Now encrypt it and make key string */
        byte[] cipher = null;
        try {
            cipher = encrypt(b);    /* Encrypt it */
        } catch (GeneralSecurityException gsx) {
            return;
        }
        key = bytesToKey(cipher);  /* Produce normalized key */
        
        is_valid = true;            /* We're valid */
    }
    /**
     * Get key format
     * @return format code
     */
    public int getKeyFormat() { return keyfmt; }
    /**
     * Test if key is temporary or permanent key
     * @return true if temporary key (non-persistent)
     */
    public boolean isTransientKey() {
        return tempkey;
    }
    /**
     * Get product ID
     * @return product ID
     */
    public int getProductID() {
        return prodid;
    }
    /**
     * Get key expiration date, if defined
     * @return expiration date, or null if no expiration
     */
    public Date getExpirationDate() {
        return exp_date;
    }
    /**
     * Get license count for key 
     * @return license count - 0=unlimited
     */
    public int getLicenseCount() {
        return license_cnt;
    }
    /**
     * Get license index for key
     * @return license index
     */
    public int getLicenseIndex() {
        return license_idx;
    }
    /**
     * Test if key is valid (well-formed and correct, can be expired)
     * @return true if valid, false if bad format
     */
    public boolean isKeyValid() {
        return is_valid;
    }
    /**
     * Get normalized key string (xxxx-xxxx-xxxx-xxxx) for key, if valid.
     * @return key string, or null if invalid key
     */
    public String getKeyString() {
        return key;
    }
    /**
     * Test if key is expired (based on current date)
     * @return true if expired, false if not
     */
    public boolean isKeyExpired() {
        if(exp_date != null) {
            if(exp_date.getTime() < System.currentTimeMillis())
                return true;
        }
        return false;
    }
    /**
     * Get string representation of key state
     * @return string
     */
    public String toString() {
        return "" + key + " (fmt=" + keyfmt + ", prod=" + prodid + ", tmp=" + tempkey + 
            ", cnt=" + license_cnt + ", id=" + license_idx + ")";
    }
    
    public static void main(String[] args) {
        if(args.length < 3) {
            System.err.println("LicenseKey <prodid> <count> <id> <days-to-expire>");
            System.exit(1);
        }
        int exptime = 0;
        if(args.length > 3)
            exptime = Integer.parseInt(args[3]);
        LicenseKey lk1 = new LicenseKey(LicenseKey.KEY_FORMAT_0, false,
            Integer.parseInt(args[0]), exptime,
            Integer.parseInt(args[1]),
            Integer.parseInt(args[2]));
        System.out.println(lk1.getKeyString());
    }
}
