package com.rfcode.ranger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;

import java.io.InputStream;

import java.io.IOException;
import java.util.Map;

/**
 * This class implements a servlet providing an interface for upload of a replacement for the firmware.zip
 * file.  The file is delivered via a multi-part/form-data encoded POST, with the file delivered as the
 * FIRMWARELIB parameter.  Other parameters are ignored.  A GET on the servlet returns a trivial page for
 * manually installing the firmware.zip replacement.  Once uploaded, the replacement firmware file is only
 * used once any existing upgrades are not currently active.
 *
 * @author Mike Primm
 *
 */
public class FirmwareFileUploadServlet extends HttpServlet {
    static final long serialVersionUID = 0x6359185792357938L;
    /**
     * Init for servlet - gets things running
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /* ------------------------------------------------------------ */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html");
        ServletOutputStream out = response.getOutputStream();

        out.println("<html><body>");
        out.println("<FORM action=\"fwlibupdate\" method=\"post\" enctype=\"multipart/form-data\">");
        out.println("<P>New firmware.zip:<INPUT type=\"file\" name=\"FIRMWARELIB\"><BR>");
        out.println("<INPUT type=\"submit\" value=\"Send\">");
        out.println("</P>");
        out.println("</FORM></body></html>");
        out.flush();
    }

    /* ------------------------------------------------------------ */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        RangerServer.User usr = null;
        boolean is_admin = true;
        /* If we've got users, do our own basic authentication */
        Map<String,RangerServer.User> users = RangerServer.getUsers();
        if(users.size() > 0) {
            String username = (String) request.getAttribute(RangerLoginModule.USERNAME_ATTRIBUTE);
            usr = users.get(username); /* Find username */
            if (usr == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            is_admin = usr.getRole().equals(RangerServer.User.ROLE_ADMIN);
            if(!is_admin) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader ("Expires", 0);
        response.setContentType("text/plain");

        /* First, see if its multipart format */
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
            /* Create upload handler */
            ServletFileUpload upload = new ServletFileUpload();
            /* Parse request input stream */
            try {
                FileItemIterator iter = upload.getItemIterator(request);
                boolean found = false;
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    String name = item.getFieldName();
                    if(name.equals("FIRMWARELIB") == false)
                        continue;
                    InputStream stream = item.openStream();
			        ServletOutputStream out = response.getOutputStream();

                    String rsp = processNewLibrary(stream);
					if(rsp == null) {	/* Success? */
						out.println("OK: New firmware.zip received");
					}
					else {
						out.println("<error>: " + rsp);
					}
                    found = true;
                }
                if(!found)
					response.getOutputStream().println("<error>: POST must be multipart, and provide firmware.zip as FIRMWARELIB parameter");
            } catch (FileUploadException fux) {
                throw new ServletException(fux.getMessage());
            }
        }
        else {
			response.getOutputStream().println("<error>: POST must be multipart, and provide firmware.zip as FIRMWARELIB parameter");
        }
    }

    public String getServletInfo() {
        return RangerServer.PRODUCTNAME + ", " + RangerServer.getVersion();
    }

    /**
     * Process new library file
     *
     * @param inp
     */
    public static String processNewLibrary(InputStream inp) throws
        IOException, ServletException {
		try {
			FirmwareLibrary.upgradeLibrary(inp);
			return null;
		} catch (IOException iox) {
			return iox.getMessage();
		}
    }
}
