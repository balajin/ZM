package com.rfcode.ranger;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.Properties;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;

/**
 * Firmware library class - used to manage access to firmware.zip
 *
 * @author Mike Primm
 */
public class FirmwareLibrary {
	private static final String	FIRMWARELIB_NAME = "firmware.zip";
	private static final String	NEW_FIRMWARELIB_NAME = "firmware.zip.1";

	private static Object sync = new Object();
	private static int	open_cnt = 0;
	private static boolean	pending_new = false;
	private static ZipFile	open_lib = null;
	private static Properties	fwlib_prop = new Properties();

	public static class FirmwareStream {
		public InputStream stream;
		public long	length;
	};
	/**
     * Upgrade firmware library, using given stream
	 */
	public static void upgradeLibrary(InputStream ins) throws IOException {
		byte	buf[] = new byte[4096];
		int	len;
		File d = RangerServer.getDataDir();	/* Get data directory */
        /* Now write it to the file */
        File f = new File(d, NEW_FIRMWARELIB_NAME);
        FileOutputStream fw = null;
		try {
	        fw = new FileOutputStream(f);
			while((len = ins.read(buf)) >= 0) {
				if(len > 0)
					fw.write(buf, 0, len);
			}
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
		/* Now, sanity check the file, and see if we're ready to use it */
		if(checkLibrary(NEW_FIRMWARELIB_NAME)) {
			/* If good, make call to switch (either now, or ASAP) */
			switchToNewLibrary();
		}
		else {	/* Else, delete it and return error */
			f.delete();	
			throw new IOException("Invalid firmware.zip");
		}
        RangerServer.updateConfigHash();
	}
	/**
	 * Initialize firmware library
	 */
	public static void initialize() throws IOException {
		/* See if we had a pending valid update */
		if(checkLibrary(NEW_FIRMWARELIB_NAME)) {
			switchToNewLibrary();		/* If so, switch to it */
		}
		if(!checkLibrary(FIRMWARELIB_NAME)) {	/* Check current library */
			throw new IOException("Missing or corrupted firmware.zip");
		}
		else {
			loadLibraryInfo();
		}
	}
	/**
	 * Check contents of given firmware library file
	 */
	public static boolean checkLibrary(String fn) {
		boolean good = true;
		ZipFile	zf = null;
		try {
			zf = new ZipFile(new File(RangerServer.getDataDir(), fn));	/* Open the library */
			ZipEntry ze = zf.getEntry("properties");	/* Get base properties file */
			if(ze == null)
				throw new IOException("missing properties");
            Properties p = new Properties();
            p.load(zf.getInputStream(ze));
			String ver = p.getProperty("version");
			String desc = p.getProperty("description");
			if((ver == null) || (desc == null))
				throw new IOException("invalid properties");				
		} catch (IOException iox) {
			good = false;
		} finally {
			if(zf != null) {
				try {
					zf.close();
				} catch (IOException ix) {}
				zf = null;
			}
		}
		return good;		
	}
	/**
	 * Switch to new library, as soon as safe
	 */
	public static void switchToNewLibrary() {
		synchronized(sync) {
			if(open_cnt > 0) {	/* At least one active reader */
				pending_new = true;	/* Mark pending new */
			}
			else {				/* Else, delete old and rename new */
				File libf = new File(RangerServer.getDataDir(), FIRMWARELIB_NAME);
				File newlibf = new File(RangerServer.getDataDir(), NEW_FIRMWARELIB_NAME);
				if(newlibf.exists()) {
					if(libf.exists())
						libf.delete();		/* Delete old one */
					newlibf.renameTo(libf);	/* And rename new to old */

					loadLibraryInfo();		/* And load library information */
				}
				pending_new = false;
			}
		}
	}
	/**
	 * Find stream for given firmware family in library
	 */
	public static FirmwareStream openFirmware(String family_id) {
		boolean do_close = false;
		try {
			synchronized(sync) {
				if(open_cnt == 0) {	/* No opens yet? */
					open_lib = new ZipFile(new File(RangerServer.getDataDir(), FIRMWARELIB_NAME));
				}
				open_cnt++;
			}
			do_close = true;

			/* Fetch the firmware path */
			String fwname = fwlib_prop.getProperty("family." + family_id);
			if(fwname == null)
				throw new IOException("family not found");
			if(!fwname.endsWith("/")) fwname += "/";	/* Add "/" if needed */
			/* Find firmware (path + "/" + name-ending-in-".IMG") */
			Enumeration<?> fe = open_lib.entries();
			while(fe.hasMoreElements()) {
				String zen;
				ZipEntry ze = (ZipEntry)fe.nextElement();
				zen = ze.getName();
				/* Starts with fw path, ends with ".IMG", and no '/' after path */
				if(zen.startsWith(fwname) && zen.endsWith(".IMG") &&
					(zen.indexOf('/', fwname.length()) == -1)) {
					do_close = false;
					FirmwareStream fws = new FirmwareStream();
					fws.stream = open_lib.getInputStream(ze);	/* Return stream */
					fws.length = ze.getSize();
					return fws;
				}
			}
		} catch (IOException iox) {
			do_close = true;	/* Problem - close it */
		} finally {
			if(do_close) {
				doClose();
			}
		}
		return null;
	}
	/**
	 * Close opened firmware stream
	 */
	public static void closeFirmware(FirmwareStream ins) {
		if((ins != null) && (ins.stream != null)) {
			try { ins.stream.close(); } catch (IOException iox) {};
			ins.stream = null;
		}
		doClose();
	}

	/**
	 * Check for firmware family - return true if available
	 */
	public static boolean checkFirmware(String family_id) {
		synchronized(sync) {
			return (fwlib_prop.getProperty("family." + family_id) != null);
		}
	}
	/**
	 * Get method for firmware family (return null if not found)
	 */
	public static String getFirmwareMethod(String family_id) {
		synchronized(sync) {
			return fwlib_prop.getProperty("method." + family_id);
		}
	} 

	private static void doClose() {
		synchronized(sync) {
			if(open_cnt > 0) {	/* No opens yet? */
				open_cnt--;
				if(open_cnt == 0) {
					try { open_lib.close(); } catch (IOException io) {}
					open_lib = null;
					if(pending_new)
						switchToNewLibrary();
				}
			}
		}
	}

	/**
	 * Load firmware library version info
	 */
	private static void loadLibraryInfo() {
		boolean do_close = false;
		try {
			synchronized(sync) {
				if(open_cnt == 0) {	/* No opens yet? */
					open_lib = new ZipFile(new File(RangerServer.getDataDir(), FIRMWARELIB_NAME));
				}
				open_cnt++;
			}
			do_close = true;

			ZipEntry ze = open_lib.getEntry("properties");	/* Get base properties file */
			if(ze == null)
				throw new IOException("missing properties");
            Properties p = new Properties();
			InputStream in = open_lib.getInputStream(ze);
            p.load(in);
			in.close();
			synchronized(sync) {
				fwlib_prop = p;
			}
		} catch (IOException iox) {
			do_close = true;	/* Problem - close it */
		} finally {
			if(do_close) {
				doClose();
			}
		}
	}
	/**
	 * Get FW file version
	 */
	public static String getVersion() {
		synchronized(sync) {
			return fwlib_prop.getProperty("version", "");
		}
	}
	/**
	 * Get FW file description
	 */
	public static String getDescription() {
		synchronized(sync) {
			return fwlib_prop.getProperty("description", "");
		}
	}
	/**
	 * Get firmware version map
	 *  (returns map of versions, keyed by family)
	 */
	public static Map<String,String> getFirmwareVersions() {
		HashMap<String,String> vmap = new HashMap<String,String>();
		synchronized(sync) {
			Enumeration<?> pname = fwlib_prop.propertyNames();
			while(pname.hasMoreElements()) {
				String pn = (String)pname.nextElement();
				if(pn.startsWith("version.")) {	/* Version token? */
					vmap.put(pn.substring(8), fwlib_prop.getProperty(pn));
				}
			}
		}
		return vmap;				
	}
}
