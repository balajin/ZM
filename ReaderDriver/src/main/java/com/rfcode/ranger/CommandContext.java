package com.rfcode.ranger;

import org.apache.log4j.Logger;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.ranger.RangerServer.User;

/**
 * Interface defining context for processing of a command, whether received from
 * command line or from web (or other sources).
 * 
 * @author Mike Primm
 */
public interface CommandContext {
    public enum OutputSyntax {
        TEXT,
        XML,
        JSON
    };
    /**
     * Send output string. Lines end with "\n" in the text (will be translated
     * by context if needed)
     * 
     * @param s -
     *            message to send
     * @throws CommandException
     *             if error sending output (fatal to command)
     */
    public void sendString(String s) throws CommandException;
    /**
     * Make string version of given object value for output
     * 
     * @param v -
     *            value
     * @return String representation
     */
    public String makeString(Object v);
    /**
     * Report command error - do not call sendString() after this. Do not call
     * once sendString() has been used.
     * 
     * @param msg -
     *            error message
     */
    public void reportError(String msg);
    /**
     * Test if admin privilege
     * 
     * @return true if admin, false if not
     */
    public boolean isAdmin();
    /**
     * Test if config view privilege
     * 
     * @return true if view allowed, false if not
     */
    public boolean isConfigView();
    /**
     * Get our SessionFactory
     * 
     * @return factory
     */
    public SessionFactory getSessionFactory();
    /**
     * Get logger
     * 
     * @return logger
     */
    public Logger getLogger();
    /**
     * Get my user
     * 
     * @return user, or null if none
     */
    public User getUser();
    /**
     * Request session termination
     */
    public void requestSessionTermination();
    /**
     * Enable/disable broadcasts on session
     * 
     * @param enab -
     *            true=allow broadcasts
     */
    public void enableBroadcasts(boolean enab);
    /**
     * Set content-type for response - default is text/plain
     * 
     * @param ct -
     *            MIME content type
     */
    public void setContentType(String ct);
    /**
     * Use web syntax?
     * @return true if web-style, false if not
     */
    public boolean useWebSyntax();
    /**
     * Command type extension ( the "ext" in cmd.ext, if provided ).  Used
     * to signal formatting options, such as XML encoding.
     * @return ext value, or "" if not provided
     */
    public OutputSyntax getFormatExt();
    /**
     * Start object using requested encoding
     * @param sb - string builder to append to
     * @param type - object type
     * @param id - object id
     * @param is_first - true if first, false if not
     */
    public StringBuilder appendStartObject(StringBuilder sb, String type,
        String id, boolean is_first);
    /**
     * Add encoded attribute value pair using requested encoding (text, json, xml)
     * @param sb - string builder to append to
     * @param id - attribute ID
     * @param val - attribute value
     * @param is_first - true if first, false if second or later (for commas)
     */
    public StringBuilder appendAttrib(StringBuilder sb, String id, Object val,
        boolean is_first);
    /**
     * End object using requested encoding
     * @param sb - string builder to append to
     * @param type - object type
     * @param id - object id, or null if none
     */
    public StringBuilder appendEndObject(StringBuilder sb, String type, String id);
    /**
     * Report that command processing is incomplete - processing code will call
     * the commandCompleted() method once it is complete
     */
    public void commandStillInProgress();
    /**
     * Report that a command previously reported as commandStillInProgress() has
     * completed.  Report exception, if one occurred
     */
    public void commandCompleted(CommandException x);
}
