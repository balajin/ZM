package com.rfcode.ranger;

/**
 * Ranger extension interface - used for plugin initialization
 */
public interface RangerExtension {
    /**
     * Initialize ranger extension
     */
    public void extensionInit();
}
