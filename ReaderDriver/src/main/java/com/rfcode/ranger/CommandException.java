package com.rfcode.ranger;

/**
 * Command handling exception (fatal error for command)
 * 
 * @author Mike Primm
 * 
 */
public class CommandException extends RuntimeException {
    static final long serialVersionUID = 45618974457913248L;
    /**
     * Command exception, with given message
     * 
     * @param msg -
     *            error message
     */
    public CommandException(String msg) {
        super(msg);
    }
}
