package com.rfcode.ranger;
import com.rfcode.drivers.TCPIPServerSocket;
import com.rfcode.drivers.TCPIPServerHandler;
import com.rfcode.drivers.TCPIPSession;
import com.rfcode.drivers.Session;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.SessionHandler;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.SessionRead;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.drivers.readers.mantis2.MantisIITagLink;
import java.util.HashSet;
import java.util.Calendar;
import java.net.InetSocketAddress;

/**
 * Tavis Concentrator DirectStream handler
 */
public class TavisConcentratorDirectStreamHandler {
    private static TCPIPServerSocket concentrator_ss;
    private static HashSet<OurConcentratorSessionHandler> sessions = new HashSet<OurConcentratorSessionHandler>();
    private static long msgidx = 1;

    private static class OurConcentratorHandler implements TCPIPServerHandler {
        public OurConcentratorHandler() {
        }
        /**
         * Session accepted - used to notify when a client connection has been
         * established with the server
         *
         * @param server -
         *            our server
         * @param session -
         *            session with client
         */
        public void sessionAcceptCompleted(TCPIPServerSocket server,
            final TCPIPSession session) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            new OurConcentratorSessionHandler(session);
				}
			});
        }
        /**
         * Server socket bind completed
         *
         * @param server -
         *            our server
         * @param is_bound -
         *            true if successful, false if failed
         */
        public void serverBindCompleted(TCPIPServerSocket server,
            boolean is_bound) {
            if (is_bound == false) {
                RangerServer.getLogger().error(
                    "Error binding concentrator to port " + server.getIPAddress().getPort());
            }
        }
        /**
         * Server socket unbind completed
         *
         * @param server -
         *            our server
         */
        public void serverUnbindCompleted(TCPIPServerSocket server) {
        }
    }

    /* Handler for concentrator directstream sessions */
    private static class OurConcentratorSessionHandler implements SessionHandler {
        private Session sess;
        /**
         * Construct session for connection accepted
         */
        public OurConcentratorSessionHandler(Session s) {
            sess = s;
            sess.setSessionHandler(this);
            sessions.add(this);
        }
        /**
         * Clean up completed session
         */
        //public void cleanup() {
        //    if (sess != null) {
        //        sess.setSessionHandler(null);
        //        sess.cleanup();
        //        sess = null;
        //    }
        //    sessions.remove(this);
        //}
        /**
         * Not needed for inbound connection
         */
        public void sessionStartingConnect(Session s) {
        }
        /**
         * Connected  just save session (must be good if inbound)
         */
        public void sessionConnectCompleted(Session s, boolean is_connected) {
            sess = s;
        }
        /**
         * Session disconnected - used to notify when the session has become
         * disconnected from its target. Starting with this call, requests to write
         * data to the session can no longer be enqueued. sessionDataRead() and
         * sessionDataWriteComplete() callbacks will not happen once the session is
         * disconnected.
         *
         * @param s -
         *            session
         */
        public void sessionDisconnectCompleted(Session s) {
            sessions.remove(this);
        }
        /**
         * Data read from session - used to deliver raw data reads from session.
         *
         * @param sdr -
         *            read request result - only valid for duration of
         *            sessionDataRead callback
         */
        public void sessionDataRead(final SessionRead sdr) {
            /* No input accepted - just toss it */
		}
        /**
         * Send string to the reader's session
         *
         * @param st -
         *            string to send
         */
        //private void sendString(String st) {
        //    byte[] b = st.getBytes();
        //    try {
        //        sess.requestDataWrite(b, 0, b.length, null);
        //    } catch (SessionException sx) {
        //        RangerServer.getLogger().error("up-connect: " + sx);
        //    }
        //}
    }

    public static void configurePort(int port) throws SessionException {
        SessionFactory sf = RangerServer.getSessionFactory();
        concentrator_ss = sf.createTCPIPServerSocket();
        concentrator_ss.setIPAddress(new InetSocketAddress(port));
        concentrator_ss.setServerHandler(new OurConcentratorHandler());
        concentrator_ss.requestBind(); /* Activate it */
    }

    public static void cleanupPort() {
        if(concentrator_ss != null) {
            try {
                concentrator_ss.requestUnbind();
            } catch (SessionException sx) {
            }
            concentrator_ss = null;
        }
    }

    private static void broadcastString(String s) {
        byte[] bb = s.getBytes();

        for(OurConcentratorSessionHandler  hnd : sessions) {
            if(hnd.sess == null)
                continue;
            try {
                hnd.sess.requestDataWrite(bb, 0, bb.length, null);
            } catch (SessionException sx) {
                RangerServer.getLogger().error(sx);
            }
		}
    }

    public static void postMessage(Reader rdr, Tag tag, int[] ssi, int ssis, boolean is_lost, int payload, Object[] oldvals) {
        if((concentrator_ss == null) || (sessions.size() == 0))
            return;
        StringBuilder sb = new StringBuilder();
        /* Add message index */
        sb.append(msgidx);  msgidx++;   if(msgidx >= 0x80000000L) msgidx = 1;        
        sb.append(",").append(tag.getTagID());   /* Add tag ID */
        sb.append(",8,");   /* Bogus up range */
        sb.append(rdr.getID());    /* Add reader ID */
        Calendar c = Calendar.getInstance();    /* Get current time */
        sb.append(",").append(String.format("%02d/%02d/%02d %02d:%02d:%02d %s", c.get(Calendar.MONTH)+1,
            c.get(Calendar.DATE), c.get(Calendar.YEAR)%100, (c.get(Calendar.HOUR)==0?12:c.get(Calendar.HOUR)),
            c.get(Calendar.MINUTE), c.get(Calendar.SECOND), (c.get(Calendar.AM_PM)==Calendar.AM?"AM":"PM")));
        if(is_lost) {
            sb.append(",L");
        }
        else {  /* See if other known attributes */
            String code = "";
            int motidx = tag.getTagType().getTagAttributeIndex(MantisIITagType.MOTION_ATTRIB);
            if(motidx >= 0) {
                Boolean v = (Boolean)tag.readTagAttribute(motidx);
                if((v != null) && v.equals(Boolean.TRUE))
                    code += "M";
            }
            int tampidx = tag.getTagType().getTagAttributeIndex(MantisIITagType.TAMPER_ATTRIB);
            if(tampidx >= 0) {
                Boolean v = (Boolean)tag.readTagAttribute(tampidx);
                if((v != null) && v.equals(Boolean.TRUE))
                    code += "T";
            }
            int panidx = tag.getTagType().getTagAttributeIndex(MantisIITagType.PANIC_ATTRIB);
            if(panidx >= 0) {
                Boolean v = (Boolean)tag.readTagAttribute(panidx);
                if((v != null) && v.equals(Boolean.TRUE))
                    code += "P";
            }
            if(code.equals("")) {
                sb.append(",H");
            }
            else {
                sb.append(",").append(code);
            }
        }
        sb.append(",").append(tag.getTagGUID().substring(0,6));  /* Add group code */
        sb.append(",");
        boolean first = true;
        if(payload >= 0) {
            if(first) { first = false; } else { sb.append(";"); }
            sb.append("SEN=").append(String.format("%03o", payload));
        }
        if(ssis == MantisIITagLink.SSI_VALUE_NA) {
            if(ssi[0] != MantisIITagLink.SSI_VALUE_NA) {
                if(first) { first = false; } else { sb.append(";"); } 
                sb.append("SSI1=").append(-ssi[0]);
            }
            if(ssi[1] != MantisIITagLink.SSI_VALUE_NA) {
                if(first) { first = false; } else { sb.append(";"); }
                sb.append("SSI2=").append(-ssi[1]);
            }
        }
        else {
            if(first) { first = false; } else { sb.append(";"); };
            sb.append("SSI=").append(ssis);
        }
        sb.append(",,,\r");
        broadcastString(sb.toString());
    }
}

