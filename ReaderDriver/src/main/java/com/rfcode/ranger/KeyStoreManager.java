//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2011. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//Class Name:          KeyStoreManager
//
//Written By:          Richie Jefts
//------------------------------ tabstop = 4 ----------------------------------
package com.rfcode.ranger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;

import java.util.Enumeration;

/**
 * Manages our KeyStore.
 */
public class KeyStoreManager {
    // combined file which is recreated every server startup
    private static final String KEYSTORE_COMBINED_FILE = ".keystore-zonemgr";

    public static final String DEFAULT_PASSWORD = "rfcode";

    private final KeyStore _keystore;

    public KeyStoreManager(File directory, String password) throws Exception {
        char[] chpassword = (password != null ? password.toCharArray() : null);
        KeyStore keystore = cacerts(create(directory, chpassword));

        // We need to save the combined keystore into a file so we can set the
        // system keystore property. We need this in order to get LDAP
        // authentication working Spring LDAP 1.3.1 allows you to set the
        // SSLSocketFactory, however, it performs a SSL connection before
        // it uses our socket factory and therefore uses the keystore as
        // specified in the system property
        File combined = new File(directory, KEYSTORE_COMBINED_FILE);
        FileOutputStream output = new FileOutputStream(combined);
        try {
            if (password == null) {
                password = DEFAULT_PASSWORD;
                chpassword = password.toCharArray();
            }
            keystore.store(output, chpassword);
        }
        finally {
            if(output != null) {
                output.close();
                output = null;
            }
        }

        System.setProperty("javax.net.ssl.trustStore", combined.getAbsolutePath());
        System.setProperty("javax.net.ssl.trustStorePassword", password);

        _keystore = keystore;
    }

    private KeyStore create(File dir, char[] password) throws Exception {
        File file = new File(dir, "keystore");
        return load(file, password);
    }

    private KeyStore cacerts(KeyStore keystore) throws Exception {
        File dir = new File(System.getProperty("java.home"), "lib");
        File file = new File(new File(dir, "security"), "cacerts");

        KeyStore cacerts = load(file, "changeit".toCharArray());

        if (keystore == null) {
            keystore = cacerts;
        }
        else {
            Enumeration<String> aliases = cacerts.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                keystore.setCertificateEntry(alias, cacerts.getCertificate(alias));
            }
        }
        return keystore;
    }

    private KeyStore load(File file, char[] password) throws Exception {
        KeyStore keystore = null;
        if (file.exists()) {
            FileInputStream input = new FileInputStream(file);
            try {
                keystore = KeyStore.getInstance(KeyStore.getDefaultType());
                keystore.load(input, password);
            }
            finally {
                if(input != null) {
                    input.close();
                    input = null;
                }
            }
        }
        return keystore;
    }

    /**
     * Returns the keystore
     */
    public KeyStore getKeyStore() {
        return _keystore;
    }
}
