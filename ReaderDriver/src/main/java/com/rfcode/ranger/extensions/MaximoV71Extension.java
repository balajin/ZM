package com.rfcode.ranger.extensions;

import com.rfcode.ranger.RangerExtension;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.ConfigUpdateListener;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.TagLifecycleListener;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.commands.ReaderAddGroupHandler;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.w3c.dom.Document;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 

/**
 * IBM Maximo V7.1 event export extension
 *
 * This extension is used to publish tag and reader update events to
 * the HTTP POST interface on IBM Maximo.  It also accepts posts from
 * Maximo for defining locations, readers, tag groups, and rules.
 *
 * Configuration of the extension is via the 'configset' command.  The settings
 * include:
 *   maximo_hostname - hostname of maximo server (required - disabled if not set)
 *   maximo_port - port number of maximo server (default = 80)
 *   maximo_username - user name for authenticating posts to maximo (default = none)
 *   maximo_password - password for authenticating posts to maximo (default = none)
 *   maximo_extsysname - external system name to use for posts (default - RFCODE)
 *   maximo_tag_entservname - enterprise service name to use for tag posts (default - RFCTAGSInterface)
 *   maximo_reader_entservname - enterprise service name to use for reader posts (default - RFCREADERSTATUSSInterface)
 *   maximo_readersetting_entservname - enterprise service name to use for reader settings posts (default - RFCREADERSETTINGSInterfac)
 *   maximo_enabled - enable/disable (true=enabled, other=disabled) (default=enabled)
 *   maximo_defsiteid - default site ID (for locations without site__loc format)
 *   maximo_nodefval - if defined and set true, do not report defaulted tag values
 */
public class MaximoV71Extension implements RangerExtension,
    ConfigUpdateListener {
    public static final String MAXIMO_VERSION = "maximo_version";
    public static final String MAXIMO_VERSION_DEFAULT = "7";
    public static final String MAXIMO_HOSTNAME = "maximo_hostname";
    public static final String MAXIMO_HOSTNAME_DEFAULT = "";
    public static final String MAXIMO_PORT = "maximo_port";
    public static final String MAXIMO_PORT_DEFAULT = "80";
    public static final String MAXIMO_USERNAME = "maximo_username";
    public static final String MAXIMO_USERNAME_DEFAULT = "";
    public static final String MAXIMO_PASSWORD = "maximo_password";
    public static final String MAXIMO_PASSWORD_DEFAULT = "";
    public static final String MAXIMO_EXTSYSNAME = "maximo_extsysname";
    public static final String MAXIMO_EXTSYSNAME_DEFAULT = "RFCEXT1";
    public static final String MAXIMO_DEFSITEID = "maximo_defsiteid";
    public static final String MAXIMO_DEFSITEID_DEFAULT = "RFCODE";
    public static final String MAXIMO_TAG_ENTSERVNAME = "maximo_tag_entservname";
    public static final String MAXIMO_TAG_ENTSERVNAME_DEFAULT = "RFCTAGSInterface";
    public static final String MAXIMO_TAGTYPE_ENTSERVNAME = "maximo_tagtype_entservname";
    public static final String MAXIMO_TAGTYPE_ENTSERVNAME_DEFAULT = "RFCTAGTYPESInterface";
    public static final String MAXIMO_READER_ENTSERVNAME = "maximo_reader_entservname";
    public static final String MAXIMO_READER_ENTSERVNAME_DEFAULT = "RFCREADERSTATUSSInterface";
    public static final String MAXIMO_READERSETTING_ENTSERVNAME = "maximo_readersetting_entservname";
    public static final String MAXIMO_READERSETTING_ENTSERVNAME_DEFAULT = "RFCREADERSETTINGSInterfac";
    public static final String MAXIMO_READERTYPE_ENTSERVNAME = "maximo_readertype_entservname";
    public static final String MAXIMO_READERTYPE_ENTSERVNAME_DEFAULT = "RFCREADERTYPESInterface";
    public static final String MAXIMO_RULETYPE_ENTSERVNAME = "maximo_ruletype_entservname";
    public static final String MAXIMO_RULETYPE_ENTSERVNAME_DEFAULT = "RFCRULETYPESInterface";
    public static final String MAXIMO_RULESETTING_ENTSERVNAME = "maximo_rulesetting_entservname";
    public static final String MAXIMO_RULESETTING_ENTSERVNAME_DEFAULT = "RFCRULESETTINGSInterface";
    public static final String MAXIMO_ZONEMGR_ENTSERVNAME = "maximo_zonemgr_entservname";
    public static final String MAXIMO_ZONEMGR_ENTSERVNAME_DEFAULT = "RFCZONEMGRSInterface";
    public static final String MAXIMO_ENABLED = "maximo_enabled";
    public static final String MAXIMO_ENABLED_DEFAULT = "true";
    public static final String MAXIMO_PROTOCOL = "maximo_protocol";
    public static final String MAXIMO_PROTOCOL_HTTP = "HTTP";
    public static final String MAXIMO_PROTOCOL_HTTPS = "HTTPS";
    public static final String MAXIMO_PROTOCOL_HTTPS_VERIFY = "HTTPS_VERIFY";
    public static final String MAXIMO_PROTOCOL_DEFAULT = MAXIMO_PROTOCOL_HTTP;
	public static final String MAXIMO_NODEFVALS = "maximo_nodefval";
	public static final String MAXIMO_NODEFVALS_DEFAULT = "false";

    public static final int RETRY_DELAY = 5000; /* 5 sec */
    public static final int CONNECT_TIMEOUT = 30000; /* 30 seconds */
    public static final int READ_TIMEOUT = 120000; /* 2 minutes */
    public static final int MAX_AGGREGATE_TAGS = 100;

    public static final String EVENTCREATE = "Create";
    public static final String EVENTFOUND = "Found";
    public static final String EVENTUPDATE = "Update";
    public static final String EVENTDELETE = "Delete";
    public static final String EVENTLOST = "Lost";

    public static final int MAX_ERR_CNT = 5;
    private enum EventType {
        EVENT_TAG,
        EVENT_TAGTYPE,
        EVENT_RDR,
        EVENT_RDRTYPE,
        EVENT_RULETYPE,
        EVENT_RDRSETTING,
        EVENT_RULESETTING,
        EVENT_ZM,
        EVENT_ZM_INIT       /* Initialization ZM event */
    };
    /* Base URL : full URL is http://host:port/baseurl/extsysname/entservname */
    public static final String HTTPPOST_BASE_URL = "/meaweb/es/";
    public static final String HTTPPOST_BASE_URL_V6 = "/meaweb/measervlet/MAXIMO";

    private URL tagurl;
    private URL tagtypeurl;
    private URL rdrurl;
    private URL rdrsettingurl;
    private URL rdrtypeurl;
    private URL ruletypeurl;
    private URL rulesettingurl;
    private URL zmurl;
    private String extsysname;
    private String tag_entservname;
    private String tagtype_entservname;
    private String rdr_entservname;
    private String rdrsetting_entservname;
    private String rdrtype_entservname;
    private String ruletype_entservname;
    private String rulesetting_entservname;
    private String zm_entservname;
    private String uid;
    private String pwd;
    private String defsiteid;
    private boolean protocol_is_ssl = false;
    private boolean protocol_strict = false;
    private Listener lsnr;
    private boolean operational = false;
    private static final int MAXIMO_VERSION_7 = 7;
    private static final int MAXIMO_VERSION_6 = 6;
    private static final int MAXIMO_VERSION_711 = 711;
    private boolean maximo6 = false;
    private boolean maximo711 = false;
	private boolean nodefvals = false;

    private int max_version = MAXIMO_VERSION_7;
    /* Our processing thread */
    private ServiceThread our_thread;
    /* Our event queue */
    private LinkedBlockingQueue<MaximoEvent> evt_queue = new LinkedBlockingQueue<MaximoEvent>();
    private MaximoEvent last_tagevt = null;

    private static MaximoV71Extension ext = null;
    /**
     * Handler thread - does all the work
     */
    private class ServiceThread extends Thread {
        public ServiceThread() {
            super("MaximoExtension Service");
        }
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }

    /**
     * null verifier
     */
    private static class nullVerifier implements HostnameVerifier {
        public boolean verify(String hn, SSLSession sess) {
            return true;
        }
    }
    private static nullVerifier null_verifier = new nullVerifier();
    /**
     * Maximo event to be sent
     */
    public class MaximoEvent {
        EventType type;
        String xml;
        int cnt;
    }
    /**
     * Extension init
     */
    public void extensionInit() {
        RangerServer.info("Maximo extension init");
        ext = this; /* Save singleton */
        /* Register config update hook */
        RangerServer.addConfigUpdateListener(this);

        updateConfig();
    }
    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
    public void configUpdateNotification() {
        updateConfig();
    }
    /**
     * Update config, and return if active
     */
    private boolean updateConfig() {
        boolean is_enab = false;
        Properties cfg = RangerServer.getConfig();
        String enab = cfg.getProperty(MAXIMO_ENABLED, MAXIMO_ENABLED_DEFAULT);
        String host = cfg.getProperty(MAXIMO_HOSTNAME, MAXIMO_HOSTNAME_DEFAULT);
        String port = cfg.getProperty(MAXIMO_PORT, MAXIMO_PORT_DEFAULT);
        String newuid = cfg.getProperty(MAXIMO_USERNAME, MAXIMO_USERNAME_DEFAULT);
        String newpwd = cfg.getProperty(MAXIMO_PASSWORD, MAXIMO_PASSWORD_DEFAULT);
        String maxver = cfg.getProperty(MAXIMO_VERSION, MAXIMO_VERSION_DEFAULT);
        String proto = cfg.getProperty(MAXIMO_PROTOCOL, MAXIMO_PROTOCOL_DEFAULT);
		String nodefval = cfg.getProperty(MAXIMO_NODEFVALS, MAXIMO_NODEFVALS_DEFAULT);
        boolean strict_proto = false;
        boolean proto_is_ssl = false;
        URL newtagurl = null;
        URL newtagtypeurl = null;
        URL newrdrurl = null;
        URL newrdrsettingurl = null;
        URL newrdrtypeurl = null;
        URL newruletypeurl = null;
        URL newrulesettingurl = null;
        URL newzmurl = null;

        /* Check protocol */
        if(proto.equals(MAXIMO_PROTOCOL_HTTP)) {
            proto = "http";
        }
        else if(proto.equals(MAXIMO_PROTOCOL_HTTPS)) {
            proto = "https";
            proto_is_ssl = true;
        }
        else if(proto.equals(MAXIMO_PROTOCOL_HTTPS_VERIFY)) {
            proto = "https";
            strict_proto = true;
            proto_is_ssl = true;
        }
        else {
            RangerServer.error("Bad value for " + MAXIMO_PROTOCOL + "(" +
                proto + ") - using HTTP");
            proto = "http";
        }

        /* Update external system name */
        extsysname = cfg.getProperty(MAXIMO_EXTSYSNAME, MAXIMO_EXTSYSNAME_DEFAULT);
        /* Update enterprise service names for tag and reader objects */
        tag_entservname = cfg.getProperty(MAXIMO_TAG_ENTSERVNAME,
            MAXIMO_TAG_ENTSERVNAME_DEFAULT);
        tagtype_entservname = cfg.getProperty(MAXIMO_TAGTYPE_ENTSERVNAME,
            MAXIMO_TAGTYPE_ENTSERVNAME_DEFAULT);
        rdr_entservname = cfg.getProperty(MAXIMO_READER_ENTSERVNAME,
            MAXIMO_READER_ENTSERVNAME_DEFAULT);
        rdrsetting_entservname = cfg.getProperty(MAXIMO_READERSETTING_ENTSERVNAME,
            MAXIMO_READERSETTING_ENTSERVNAME_DEFAULT);
        rdrtype_entservname = cfg.getProperty(MAXIMO_READERTYPE_ENTSERVNAME,
            MAXIMO_READERTYPE_ENTSERVNAME_DEFAULT);
        ruletype_entservname = cfg.getProperty(MAXIMO_RULETYPE_ENTSERVNAME,
            MAXIMO_RULETYPE_ENTSERVNAME_DEFAULT);
        rulesetting_entservname = cfg.getProperty(MAXIMO_RULESETTING_ENTSERVNAME,
            MAXIMO_RULESETTING_ENTSERVNAME_DEFAULT);
        zm_entservname = cfg.getProperty(MAXIMO_ZONEMGR_ENTSERVNAME,
            MAXIMO_ZONEMGR_ENTSERVNAME_DEFAULT);
        defsiteid = cfg.getProperty(MAXIMO_DEFSITEID,
            MAXIMO_DEFSITEID_DEFAULT);
        if(enab.equals("true") && (host.length() > 0)) {
            int pnum = -1;
            try {
                pnum = Integer.parseInt(port);
            } catch (NumberFormatException nfx) {
                pnum = 80;
            }
			if(nodefval.equals("true"))
				nodefvals = true;
			else
				nodefvals = false;
            try {
                max_version = Integer.parseInt(maxver);
            } catch (NumberFormatException nfx) {
                max_version = MAXIMO_VERSION_7;
            }
            maximo711 = false;
            if(max_version <= MAXIMO_VERSION_6) {
                maximo6 = true;
                try {
                    newtagurl = newtagtypeurl = newrdrurl = newrdrsettingurl = 
                        newrdrtypeurl = newruletypeurl = newrulesettingurl = newzmurl = 
                        new URL(proto, host, pnum, HTTPPOST_BASE_URL_V6);             
                    is_enab = true;
                } catch (MalformedURLException mux) {
                    is_enab = false;
                }
            }
            else {
                maximo6 = false;
                if(max_version >= MAXIMO_VERSION_711) {
                    maximo711 = true;
                    max_version = MAXIMO_VERSION_7;
                }
                try {
                    newtagurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + tag_entservname);             
                    newtagtypeurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + tagtype_entservname);             
                    newrdrurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + rdr_entservname);             
                    newrdrsettingurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + rdrsetting_entservname);             
                    newrdrtypeurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + rdrtype_entservname);             
                    newruletypeurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + ruletype_entservname);             
                    newrulesettingurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + rulesetting_entservname);             
                    newzmurl = new URL(proto, host, pnum, 
                        HTTPPOST_BASE_URL + extsysname + "/" + zm_entservname);             
                    is_enab = true;
                } catch (MalformedURLException mux) {
                    is_enab = false;
                }
            }
        }
        if(is_enab == true) {   /* If active, start up */
            tagurl = newtagurl;
            tagtypeurl = newtagtypeurl;
            rdrurl = newrdrurl;
            rdrsettingurl = newrdrsettingurl;
            rdrtypeurl = newrdrtypeurl;
            ruletypeurl = newruletypeurl;
            rulesettingurl = newrulesettingurl;
            zmurl = newzmurl;
            protocol_is_ssl = proto_is_ssl;
            protocol_strict = strict_proto;
            uid = newuid;
            pwd = newpwd;
            if(our_thread == null) {    /* Not active? */
                /* Start it up */
                our_thread = new ServiceThread();
                our_thread.setDaemon(true);
                our_thread.start();
            }
            if(lsnr == null) {
                lsnr = new Listener();
                /* Start listening to tag updates */
                AbstractTagType.addGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.addReaderStatusListener(lsnr);
                RangerServer.addReaderLifecycleListener(lsnr);
            }
            synchronized(evt_queue) {
                last_tagevt = null;
                operational = false;    /* Not operational yet */
            }
            encodeZoneMgrEvent(true);   /* Enqueue event for zone mgr info */
        }
        else {  /* Else, if not active */
            if(lsnr != null) {
                /* Stop listening to tag updates */
                AbstractTagType.removeGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.removeReaderStatusListener(lsnr);
                RangerServer.removeReaderLifecycleListener(lsnr);
                lsnr = null;
            }
            if(our_thread != null) {
                our_thread.interrupt();
            }
            synchronized(evt_queue) {
                evt_queue.clear();  /* Toss all pending events */
                last_tagevt = null;
                operational = false;
            }
        }
        return is_enab;
    }

    /**
     * Processing method - called by our service thread
     */
    private void process() {
        MaximoEvent pushed_evt = null;
		SimpleDateFormat logdatefmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            int errcnt = 0;
            while(true) {
                MaximoEvent evt;
                if(pushed_evt != null) {
                    evt = pushed_evt; pushed_evt = null;
                }
                else {
                    evt = evt_queue.poll(15, TimeUnit.SECONDS);
                }
                if(evt == null) continue;
                synchronized(evt_queue) {
                    if(evt == last_tagevt) last_tagevt = null;
                }
                URL url;
                if(evt.type == EventType.EVENT_TAG)
                    url = tagurl;
                else if(evt.type == EventType.EVENT_ZM)
                    url = zmurl;
                else if(evt.type == EventType.EVENT_TAGTYPE)
                    url = tagtypeurl;
                else if(evt.type == EventType.EVENT_RDRTYPE)
                    url = rdrtypeurl;
                else if(evt.type == EventType.EVENT_RULETYPE)
                    url = ruletypeurl;
                else if(evt.type == EventType.EVENT_ZM_INIT)
                    url = zmurl;
                else if(evt.type == EventType.EVENT_RDRSETTING)
                    url = rdrsettingurl;
                else if(evt.type == EventType.EVENT_RULESETTING)
                    url = rulesettingurl;
                else
                    url = rdrurl;
                //RangerServer.info("posting to "+ url.toString());
                /* Build and issue request */
                try {
            		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    if(protocol_is_ssl && (!protocol_strict)) { /* If SSL but not strict */
                        ((HttpsURLConnection)connection).setHostnameVerifier(null_verifier);
                    }
	    	        connection.setRequestMethod("POST");
            		connection.setRequestProperty("Content-Type", "application/xml");
	    	        connection.setUseCaches(false);
	    	        connection.setDoInput(true);
            		connection.setDoOutput(true);
                    connection.setConnectTimeout(CONNECT_TIMEOUT);
                    connection.setReadTimeout(READ_TIMEOUT);
                    if((uid.length() > 0) || (pwd.length() > 0)) {
                        String userPassword = uid+":"+pwd;
                        String encoding = new sun.misc.BASE64Encoder().encode (userPassword.getBytes());
                        connection.setRequestProperty("Authorization", "Basic " + encoding);
                    }
	    	        connection.connect();
		
            		DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
                    /* If its a tag event, need to add header and try to batch with others in queue */
                    if(evt.type == EventType.EVENT_TAG) {
                        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                        if(maximo6) {
                            xml.append("<RFCTAGSInterface xmlns=\"http://www.mro.com/mx/integration\" ");
                            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
                            xml.append("<Header operation=\"Notify\" event=\"1\">\n");
                            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
                            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
                            xml.append("  <CreationDateTime>");
                            xml.append(doISO860(new Date()));
                            xml.append("</CreationDateTime>\n");
                            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
                            xml.append("</Header>\n");
                            xml.append("<Content>\n");
                        }
                        else {
                            xml.append("<SyncRFCTAGS xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
                            xml.append(" creationDateTime=\"");
                            xml.append(doISO860(new Date()));
                            xml.append("\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"");
                            xml.append(System.currentTimeMillis());
                            xml.append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
                            xml.append(" <RFCTAGSSet>\n");
                        }
                	    dos.writeBytes(xml.toString());
                	    dos.writeBytes(evt.xml);
                        if(maximo6) {
                            dos.writeBytes(" </Content>\n");
                            dos.writeBytes("</RFCTAGSInterface>\n");
                        }
                        else {        
                            dos.writeBytes(" </RFCTAGSSet>\n");
                            dos.writeBytes("</SyncRFCTAGS>\n");
                        }
                        //RangerServer.info("Sent " + evt.cnt + " tag events");
                    } else {
                	    dos.writeBytes(evt.xml);
                    }
            		dos.flush();
            		dos.close();
            		InputStream is = connection.getInputStream();
            		is.close();
                    /* Check response code */
                    if (connection.getResponseCode() == 200) {
                        /* Message sent */
                        //RangerServer.info("Posted " + evt.type + " to IBM Maximo Server");
                        errcnt = 0;
                        /* If we just posted initial event successfully, we're operational */
                        if(evt.type == EventType.EVENT_ZM_INIT) {
                            /* Flush existing event queue */
                            evt_queue.clear();
                            /* Send tag types */
                            encodeTagTypeEvent();
                            /* Send reader types */
                            encodeReaderTypeEvent();
                            /* Send rule types */
                            encodeRuleTypeEvent();
                            /* Do initial state queuing for all tags */
                            for(TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                                Map<String,Tag> tags = tg.getTags();
                                for(Tag t : tags.values()) {
                                    encodeTagEvent(t, EVENTFOUND);
                                }
                            }
                            operational = true;
                        }
                    }
                    else {  /* Else, push it back on the head of the queue */
                        errcnt++;
                        //if((connection.getResponseCode() != 500) && (errcnt < 4)) {
						if(errcnt < 4) {	/* Retry any error code 4 times before abandoning queue */
                            pushed_evt = evt;
                        }
                        else {
                            errcnt = 0;
                            synchronized(evt_queue) {
                                evt_queue.clear();  /* Toss all pending events */
                                last_tagevt = null;
                            }
                            encodeZoneMgrEvent(true);
                        }
                        RangerServer.error(logdatefmt.format(new Date()) + ": POST to Maximo failed: URL=" + url + ", response=" + 
                            connection.getResponseCode() + "(" + connection.getResponseMessage() + ")");
                        Thread.sleep(RETRY_DELAY);
                    }
                } catch (IOException iox) {
                    RangerServer.error(logdatefmt.format(new Date()) + ": POST to Maximo failed: URL=" + url + ", response=" +
                        iox.toString());
                    Thread.sleep(RETRY_DELAY);
                    errcnt++;
                    if(errcnt < 4) {
                        pushed_evt = evt;
                    }
                    else {
                        errcnt = 0;
                        synchronized(evt_queue) {
                            evt_queue.clear();  /* Toss all pending events */
                            last_tagevt = null;
                        }
                        encodeZoneMgrEvent(true);
                    }
                }
            }
        } catch (InterruptedException ix) {
        }
    }

    private static SimpleDateFormat ISO860Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    private static String doISO860(Date d) {
        String v = ISO860Local.format(d);
        /* Need to add ':' in Z portion of string (second from last) */
        return v.substring(0, v.length()-2) + ':' + v.substring(v.length()-2);
    }
    /**
     * Encode tag event
     */
    private void encodeTagEvent(Tag tag, String type) {
        String id = tag.getTagGUID();
        StringBuilder xml = new StringBuilder();
        Map<String,Object> attrs = tag.getTagAttributes();
		Set<String> defattr = null;
		if(nodefvals) {	/* If not OK to send defaults */
			defattr = tag.getDefaultedTagAttributes();	/* Get defaulted list */
		}
        if(maximo6) {
            xml.append("<RFCTAGS>\n");
            xml.append("  <RFCTAG action=\"AddChange\">\n");
        }
        else
            xml.append("  <RFCTAG>\n");
		/* If location is not defaulted */
		if((defattr == null) || 
			(!defattr.contains(LocationRuleFactory.ATTRIB_ZONELOCATION))) {
	        Object loc = attrs.get(LocationRuleFactory.ATTRIB_ZONELOCATION);
    	    if ((loc != null) && (loc instanceof String)) {
    	        String l = (String)loc;
    	        int split = l.indexOf("__");    /* Look for divider */
    	        String site = "";
    	        String locid = "";
    	        if(split > 0) { /* Proper location */
    	            site = l.substring(0, split);
    	            locid = l.substring(split+2);
    	        }
    	        else if(l.length() > 0) {  /* Else, assume default location for site */
    	            if(defsiteid.length() > 0)
    	                site = defsiteid;
    	            else
    	                site = l;
    	            locid = l;
    	        }
    	        site = site.toUpperCase();
    	        locid = locid.toUpperCase();
    	        if(site.length() > 8) site = site.substring(0,8);
    	        if(locid.length() > 12) locid = locid.substring(0,12);
    	        xml.append("   <CURRENTSITEID>").append(site).append("</CURRENTSITEID>\n");
    	        xml.append("   <CURRENTLOCATION>").append(locid).append("</CURRENTLOCATION>\n");
    	    }
    	    else {
    	        xml.append("   <CURRENTSITEID/>\n");
    	        xml.append("   <CURRENTLOCATION/>\n");
    	    }
		}
//        if(!maximo6)
//            xml.append("   <RFCTAGID>").append(hashTagID(id,null)).append("</RFCTAGID>\n");
        xml.append("   <TAGID>").append(id).append("</TAGID>\n");
        String online_tok;
        if(maximo711)
            online_tok = "TAGONLINE";
        else
            online_tok = "ONLINE";
        if(type.equals(EVENTLOST)) {
            xml.append("   <").append(online_tok).append(">0</").append(online_tok).append(">\n");
        }
        else {
            xml.append("   <").append(online_tok).append(">1</").append(online_tok).append(">\n");
        }
        xml.append("   <REPORTINGZM>").append(extsysname).append("</REPORTINGZM>\n");
        String now = doISO860(new Date());
        xml.append("   <LASTREPORT>").append(now).append("</LASTREPORT>\n");

        /* Roll through the tag attributes */
        for(Map.Entry<String,Object> me : attrs.entrySet()) {
			/* If defaulted, skip it for now */
			if((defattr != null) && defattr.contains(me.getKey()))
				continue;
            xml.append("   <RFCTAGATTR>\n");
            xml.append("     <ATTRIBID>").append(me.getKey()).append("</ATTRIBID>\n");
            if(me.getValue() == null) {
                xml.append("     <ATTRIBVAL/>\n");
                xml.append("     <ATTRIBNUMVAL/>\n");
                xml.append("     <ATTRIBBOOLVAL>0</ATTRIBBOOLVAL>\n");
            }
            else {
                xml.append("     <ATTRIBVAL>").append(me.getValue().toString()).append("</ATTRIBVAL>\n");
                if(me.getValue() instanceof Double) {
                    xml.append("     <ATTRIBNUMVAL>").append((Double)me.getValue()).append("</ATTRIBNUMVAL>\n");
                }
                else {
                    xml.append("     <ATTRIBNUMVAL/>\n");
                }
                if(me.getValue() instanceof Boolean) {
                    xml.append("     <ATTRIBBOOLVAL>").append(me.getValue().equals(Boolean.TRUE)?"1":"0").
                        append("</ATTRIBBOOLVAL>\n");
                }
                else {
                    xml.append("     <ATTRIBBOOLVAL>0</ATTRIBBOOLVAL>\n");
                }
            }
            xml.append("     <TAGID>").append(id).append("</TAGID>\n");
            xml.append("     <DESCRIPTION>").append( 
                tag.getTagType().getTagAttributeLabel(me.getKey())).append("</DESCRIPTION>\n");
            xml.append("   </RFCTAGATTR>\n");
        }
        xml.append("  </RFCTAG>\n");
        if(maximo6)
            xml.append("</RFCTAGS>\n");
        MaximoEvent e = null;
        synchronized(evt_queue) {
            /* If we have a pending tag event, add to it */
            if((last_tagevt != null) && (last_tagevt.cnt < MAX_AGGREGATE_TAGS)) {
                last_tagevt.xml = last_tagevt.xml + xml;
                last_tagevt.cnt++;
            }
            else {
                /* If update, see if pending update */
                e = new MaximoEvent();
                e.type = EventType.EVENT_TAG;
                e.xml = xml.toString();
                last_tagevt = e;
                e.cnt = 1;
            }
        }
        /* Add new event to end of queue, if needed */
        if(e != null)
            evt_queue.offer(e);
    }

    private void addEvent(StringBuilder xml, EventType type) {
        /* If update, see if pending update */
        MaximoEvent e = new MaximoEvent();
        e.type = type;
        e.xml = xml.toString();
        xml.setLength(0);
        /* Add new event to end of queue, if needed */
        synchronized(evt_queue) {
            last_tagevt = null; /* Maintain sequence */
        }
        evt_queue.offer(e);
    }
    private void encodeReaderEvent(Reader rdr, String type) {
        //RangerServer.info("encodeReaderEvent(" + rdr.getID() + "," + type + ")");
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        String rdrid = rdr.getID(); 
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCREADERSTATUSSInte xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"1\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
            xml.append("<RFCREADERSTATUSS>\n");
        }
        else {
            xml.append("<SyncRFCREADERSTATUSS xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCREADERSTATUSSSet>\n");
        }
        if(type.equals(EVENTDELETE))
            xml.append("  <RFCREADERSTATUS action=\"Delete\">\n");
        else if(maximo6)
            xml.append("  <RFCREADERSTATUS action=\"AddChange\">\n");
        else
            xml.append("  <RFCREADERSTATUS>\n");
        xml.append("   <READERID>").append(rdrid).append("</READERID>\n");
        xml.append("   <EXTSYSNAME>").append(extsysname).append("</EXTSYSNAME>\n");
        xml.append("   <STATUS>").append(rdr.getReaderStatus()).append("</STATUS>\n");
        xml.append("   <FWVERSION>").append(rdr.getReaderFirmwareVersion()).append("</FWVERSION>\n");
        ReaderChannel[] c = rdr.getChannels();
        for(int i = 0; i < c.length; i++) {
            if(type.equals(EVENTDELETE))
                xml.append("   <RFCREADERCHANNEL action=\"Delete\">\n");            
            else
                xml.append("   <RFCREADERCHANNEL>\n");            
            xml.append("    <READERID>").append(rdrid).append("</READERID>\n");
            xml.append("    <EXTSYSNAME>").append(extsysname).append("</EXTSYSNAME>\n");
            xml.append("    <CHANNELID>").append(c[i].getRelativeChannelID()).append("</CHANNELID>\n");
            xml.append("    <DESCRIPTION>").append(c[i].getChannelLabel()).append("</DESCRIPTION>\n");
            xml.append("    <NOISE>").append(c[i].getChannelNoiseLevel()).append("</NOISE>\n");
            xml.append("    <EVTPERSEC>").append(c[i].getChannelEventsPerSec()).append("</EVTPERSEC>\n");
            xml.append("   </RFCREADERCHANNEL>\n");
        }
        xml.append("  </RFCREADERSTATUS>\n");
        if(maximo6) {
            xml.append("</RFCREADERSTATUSS>\n");
            xml.append(" </Content>\n");
            xml.append("</RFCREADERSTATUSSInte>\n");
        }
        else {
            xml.append(" </RFCREADERSTATUSSSet>\n");
            xml.append("</SyncRFCREADERSTATUSS>\n");
        }

        /* If update, see if pending update */
        addEvent(xml, EventType.EVENT_RDR);
    }
    /**
     * Encode reader settings event
     */
    private void encodeReaderSettingsEvent(Reader rdr, String type, Set<String> skipattrids) {
        //RangerServer.info("encodeReaderSettingsEvent(" + rdr.getID() + "," + type + ")");
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        String rdrid = rdr.getID(); 
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCREADERSETTINGSInt xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"1\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCREADERSETTINGS xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCREADERSETTINGSSet>\n");
        }
        Map<String,Object> attr = rdr.getReaderAttributes();
        boolean did_one = false;
        for(Map.Entry<String,Object> me : attr.entrySet()) {
            if(skipattrids.contains(me.getKey())) {
                continue;
            }
            if(maximo6) {
                xml.append("<RFCREADERSETTINGS>\n");            
                xml.append("   <RFCREADERSETTING action=\"AddChange\">\n");
            }
            else
                xml.append("   <RFCREADERSETTING>\n");            
            xml.append("    <READERID>").append(rdrid).append("</READERID>\n");
            xml.append("    <EXTSYSNAME>").append(extsysname).append("</EXTSYSNAME>\n");
            xml.append("    <RFCREADERATTRIB>").append(me.getKey()).append("</RFCREADERATTRIB>\n");
            xml.append("    <ATTRIBVALUE>");
            if(me.getValue() instanceof Set) {
                Set<?> s = (Set<?>)me.getValue();
                boolean first = true;
                for(Object o : s) {
                    if(!first)
                        xml.append(",");
                    else
                        first = false;  
                    xml.append(o.toString());
                }
            }
            else {
                xml.append(me.getValue().toString());
            }
            xml.append("</ATTRIBVALUE>\n");
            xml.append("    <DESCRIPTION/>\n");
            xml.append("   </RFCREADERSETTING>\n");
            if(maximo6) {
                xml.append("  </RFCREADERSETTINGS>\n");            
            }
            did_one = true;
        }
        if(maximo6) {
            xml.append(" </Content>\n");
            xml.append("</RFCREADERSETTINGSInt>\n");
        }
        else {
            xml.append(" </RFCREADERSETTINGSSet>\n");
            xml.append("</SyncRFCREADERSETTINGS>\n");
        }
        /* If update, see if pending update */
        if(did_one) {
            addEvent(xml, EventType.EVENT_RDRSETTING);
        }
    }

    /**
     * Encode rule settings event
     */
    private void encodeRuleSettingsEvent(String ruleid, Map<String,Object> attr,
        String type, Set<String> skipattrids) {
        //RangerServer.info("encodeRuleSettingsEvent(" + ruleid + "," + type + ")");
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCRULESETTINGSInter xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCRULESETTINGS xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCRULESETTINGSSet>\n");
        }
        boolean did_one = false;
        for(Map.Entry<String,Object> me : attr.entrySet()) {
            if(skipattrids.contains(me.getKey())) {
                continue;
            }
            if(maximo6) {
                xml.append(" <RFCRULESETTINGS>\n");
                xml.append("   <RFCRULESETTING action=\"AddChange\">\n");            
            }
            else
                xml.append("   <RFCRULESETTING>\n");
            xml.append("    <RULEID>").append(ruleid).append("</RULEID>\n");
            xml.append("    <EXTSYSNAME>").append(extsysname).append("</EXTSYSNAME>\n");
            xml.append("    <RFCRULEATTRIB>").append(me.getKey()).append("</RFCRULEATTRIB>\n");
            xml.append("    <ATTRIBVALUE>");
            if(me.getValue() instanceof Set) {
                Set<?> s = (Set<?>)me.getValue();
                boolean first = true;
                for(Object o : s) {
                    if(!first)
                        xml.append(",");
                    else
                        first = false;  
                    xml.append(o.toString());
                }
            }
            else {
                xml.append(me.getValue().toString());
            }
            xml.append("</ATTRIBVALUE>\n");
            xml.append("    <DESCRIPTION/>\n");
            xml.append("   </RFCRULESETTING>\n");
            if(maximo6) {
                xml.append(" </RFCRULESETTINGS>\n");
            }
            did_one = true;
        }
        if(maximo6) {
            xml.append(" </Content>\n");
            xml.append("</RFCRULESETTINGSInter>\n");
        }
        else {
            xml.append(" </RFCRULESETTINGSSet>\n");
            xml.append("</SyncRFCRULESETTINGS>\n");
        }
        /* If update, see if pending update */
        if(did_one) {
            addEvent(xml, EventType.EVENT_RULESETTING);
        }
    }

    /**
     * Encode zone manager event
     */
    private void encodeZoneMgrEvent(boolean init) {
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCZONEMGRSInterface xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"1\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCZONEMGRS xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCZONEMGRSSet>\n");
        }
        if(maximo6) {
            xml.append(" <RFCZONEMGRS>\n");
            xml.append("  <RFCZONEMGR action=\"AddChange\">\n");
        }
        else
            xml.append("  <RFCZONEMGR>\n");
        xml.append("   <EXTSYSNAME>").append(extsysname).append("</EXTSYSNAME>\n");
        xml.append("   <LASTRESTART>").append(now).append("</LASTRESTART>\n");
        xml.append("  </RFCZONEMGR>\n");
        
        if(maximo6) {
            xml.append(" </RFCZONEMGRS>\n");
            xml.append(" </Content>\n");
            xml.append("</RFCZONEMGRSInterface>\n");
        }
        else {
            xml.append(" </RFCZONEMGRSSet>\n");
            xml.append("</SyncRFCZONEMGRS>\n");
        }
        /* If update, see if pending update */
        if(init)
            addEvent(xml, EventType.EVENT_ZM_INIT);
        else
            addEvent(xml, EventType.EVENT_ZM);
    }

    /**
     * Encode tag type event
     */
    private void encodeTagTypeEvent() {
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCTAGTYPESInterface xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"0\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCTAGTYPES xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCTAGTYPESSet>\n");
        }
        for(TagType tt : ReaderEntityDirectory.getTagTypes()) {
            if(maximo6) {
                xml.append(" <RFCTAGTYPES>\n");
                xml.append("  <RFCTAGTYPE action=\"AddChange\">\n");
            }
            else
                xml.append("  <RFCTAGTYPE>\n");
            xml.append("   <RFCTAGTYPE>").append(tt.getID()).append("</RFCTAGTYPE>\n");
            xml.append("   <DESCRIPTION>").append(tt.getLabel()).append("</DESCRIPTION>\n");
            xml.append("  </RFCTAGTYPE>\n");
            if(maximo6) {
                xml.append(" </RFCTAGTYPES>\n");
            }
        }
        if(maximo6) {
            xml.append(" </Content>\n");
            xml.append("</RFCTAGTYPESInterface>\n");
        }
        else {
            xml.append(" </RFCTAGTYPESSet>\n");
            xml.append("</SyncRFCTAGTYPES>\n");
        }

        addEvent(xml, EventType.EVENT_TAGTYPE);
    }

    /**
     * Encode reader type event
     */
    private void encodeReaderTypeEvent() {
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCREADERTYPESInterf xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"0\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(now);
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCREADERTYPES xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCREADERTYPESSet>\n");
        }
        for(ReaderFactory rt : ReaderEntityDirectory.getReaderFactories()) {
            if(maximo6) {
                xml.append(" <RFCREADERTYPES>\n");
                xml.append("  <RFCREADERTYPE action=\"AddChange\">\n");
            }
            else {
                xml.append("  <RFCREADERTYPE>\n");
            }
            xml.append("   <RFCREADERTYPE>").append(rt.getID()).append("</RFCREADERTYPE>\n");
            xml.append("   <DESCRIPTION>").append(rt.getReaderFactoryLabel()).append("</DESCRIPTION>\n");
            String[] aids = rt.getReaderAttributeIDs();
            for(String aid : aids) {
                xml.append("   <RFCREADERATTRIB>\n");
                xml.append("    <RFCREADERATTRIB>").append(aid).append("</RFCREADERATTRIB>\n");

                xml.append("    <RFCREADERTYPE>").append(rt.getID()).append("</RFCREADERTYPE>\n");
                xml.append("    <DESCRIPTION>").append(aid).append("</DESCRIPTION>\n");
                xml.append("   </RFCREADERATTRIB>\n");
            }
            xml.append("  </RFCREADERTYPE>\n");
            if(maximo6) {
                xml.append(" </RFCREADERTYPES>\n");
            }
        }
        if(maximo6) {
            xml.append(" </Content>\n");
            xml.append("</RFCREADERTYPESInterf>\n");
        }
        else {
            xml.append(" </RFCREADERTYPESSet>\n");
            xml.append("</SyncRFCREADERTYPES>\n");
        }
//        RangerServer.info(xml.toString());
        addEvent(xml, EventType.EVENT_RDRTYPE);
    }

    /**
     * Encode rule type event
     */
    private void encodeRuleTypeEvent() {
        StringBuilder xml = new StringBuilder();
        String now = doISO860(new Date());
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        if(maximo6) {
            xml.append("<RFCRULETYPESInterfac xmlns=\"http://www.mro.com/mx/integration\" ");
            xml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" language=\"EN\">\n");
            xml.append("<Header operation=\"Notify\" event=\"1\">\n");
            xml.append("  <SenderID type=\"MAXIMO\" majorversion=\"6\" minorversion=\"2\" build=\"156\" ");
            xml.append("dbbuild=\"V600-530\">" + extsysname + "</SenderID>\n");
            xml.append("  <CreationDateTime>");
            xml.append(doISO860(new Date()));
            xml.append("</CreationDateTime>\n");
            xml.append("<MessageID>").append(System.currentTimeMillis()).append("</MessageID>\n");
            xml.append("</Header>\n");
            xml.append("<Content>\n");
        }
        else {
            xml.append("<SyncRFCRULETYPES xmlns=\"http://www.ibm.com/maximo\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
            xml.append(" creationDateTime=\"" + now + "\" transLanguage=\"EN\" baseLanguage=\"EN\" messageID=\"").append(
                System.currentTimeMillis()).append("\" maximoVersion=\"7 1 137 V7110-890\">\n");
            xml.append(" <RFCRULETYPESSet>\n");
        }
        for(LocationRuleFactory rt : ReaderEntityDirectory.getLocationRuleFactories()) {
            if(maximo6) {
                xml.append(" <RFCRULETYPES>\n");
                xml.append("  <RFCRULETYPE action=\"AddChange\">\n");
            }
            else
                xml.append("  <RFCRULETYPE>\n");
            xml.append("   <RFCRULETYPE>").append(rt.getID()).append("</RFCRULETYPE>\n");
            xml.append("   <DESCRIPTION>").append(rt.getLocationRuleFactoryLabel()).append("</DESCRIPTION>\n");
            String[] aids = rt.getLocationRuleAttributeIDs();
            Map<String,Object> defs = rt.getDefaultLocationRuleAttributes();
            for(String aid : aids) {
                xml.append("   <RFCRULEATTRIB>\n");
                xml.append("    <RFCRULEATTRIB>").append(aid).append("</RFCRULEATTRIB>\n");
                xml.append("    <RFCRULETYPE>").append(rt.getID()).append("</RFCRULETYPE>\n");
                xml.append("    <DESCRIPTION>").append(aid).append("</DESCRIPTION>\n");
                Object dv = defs.get(aid);
                if(dv != null)
                    xml.append("    <ATTRIBDEFAULT>").append(dv.toString()).append("</ATTRIBDEFAULT>\n");
                xml.append("   </RFCRULEATTRIB>\n");
            }
            xml.append("  </RFCRULETYPE>\n");
            if(maximo6) {
                xml.append(" </RFCRULETYPES>\n");
            }
        }
        if(maximo6) {
            xml.append(" </Content>\n");
            xml.append("</RFCRULETYPESInterfac>\n");
        }
        else {
            xml.append(" </RFCRULETYPESSet>\n");
            xml.append("</SyncRFCRULETYPES>\n");
        }

        addEvent(xml, EventType.EVENT_RULETYPE);
    }

    private static class DoLocationAction implements Runnable {
        Set<String> add_upd_ids = new TreeSet<String>();
        Set<String> delete_ids = new TreeSet<String>();
        boolean isevt;
        String defsiteid;
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            Location loc;
            /* If not event, need to do delete on ones not being added */
            if(!isevt) {
                Set<String> ids = ReaderEntityDirectory.getLocationIDs();
                for(String oldid : ids) {
                    if(add_upd_ids.contains(oldid) == false) {
                        delete_ids.add(oldid);
                    }
                }
            }
            for(String id : add_upd_ids) {
                int split = id.indexOf("__");
                String siteid = defsiteid;
                Location siteloc;
                if(split >= 0)
                    siteid = id.substring(0,split);
                /* Find the site-specific location, and add if needed */
                siteloc = ReaderEntityDirectory.findLocation(siteid);
                if(siteloc == null) {
                    RangerServer.createLocation(siteid, null, new String[0]);
                    siteloc = ReaderEntityDirectory.findLocation(siteid);
                    if (siteloc != null)
                        RangerServer.saveLocation(siteloc);
                }
                delete_ids.remove(siteid);  /* Remove site from delete list, if needed */
                /* Find if it exists already */
                loc = ReaderEntityDirectory.findLocation(id);
                if(loc == null) {   /* If not, create it */
                    RangerServer.createLocation(id, siteloc, new String[0]);
                    loc = ReaderEntityDirectory.findLocation(id);
                    if (loc != null)
                        RangerServer.saveLocation(loc);
                }
                else {  /* Else, do update (nothing yet) */
                }
            }
            /* Delete locations first */
            for(String id : delete_ids) {
                loc = ReaderEntityDirectory.findLocation(id);
                if (loc != null) { /* If found, delete it */
                    RangerServer.deleteLocation(loc);
                    loc.cleanup(); /* And cleanup object */
                }
            }
            synchronized(this) {
                notify();
            }
        }
    }
    /**
     * Handle locations update from maximo
     */
    private void handleLocations(Document doc, boolean max6) throws ServletException {
        boolean isevt = false;
        String evt = null;
        Element docelem;
        if(max6) {
            NodeList hdr = doc.getElementsByTagName("Header");  /* Get header */
            if(hdr.getLength() > 0) {
                docelem = (Element)hdr.item(0);
                evt = docelem.getAttribute("event");
            }
        }
        else {
            docelem = doc.getDocumentElement();
            evt = docelem.getAttribute("event");
        }
        if((evt != null) && (evt.equals("1"))) {
            isevt = true;
        }
        NodeList locs = doc.getElementsByTagName("LOCATIONS");
        DoLocationAction dolocact = new DoLocationAction();

        dolocact.isevt = isevt;
        dolocact.defsiteid = defsiteid;
        for(int lid = 0; lid < locs.getLength(); lid++) {
            String locid = null;
            String siteid = null;
            Element lnode = (Element)locs.item(lid);
            /* See if its a delete */
            String del = lnode.getAttribute("action");
            boolean isdel = false;
            if((del != null) && (del.equals("Delete"))) {
                isdel = true;
            }
            NodeList loclist = lnode.getElementsByTagName("LOCATION");
            Element e;
            NodeList ec;
            if(loclist.getLength() > 0) {
                e = (Element)loclist.item(0);
                ec = e.getChildNodes();
                locid = ec.item(0).getNodeValue();
            }
            loclist = lnode.getElementsByTagName("SITEID");
            if(loclist.getLength() > 0) {
                e = (Element)loclist.item(0);
                ec = e.getChildNodes();
                siteid = ec.item(0).getNodeValue();
            }
            /* If valid site ID and location ID, process it */
            if((siteid != null) && (locid != null)) {
                String id;
                if(defsiteid.equals(siteid)) {  /* Default site? */
                    id = locid; /* Just use locid */
                }
                else {
                    id = siteid + "__" + locid;
                }
                if(!isdel)
                    dolocact.add_upd_ids.add(id);
                else
                    dolocact.delete_ids.add(id);
            }
        }
        /* Now run on processor thread */
		try {
	        RangerProcessor.runActionNow(dolocact);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
    }
    private static class TagGroupInfo {
        String grpid;
        String tagtype;
        String grpcode;
        String locupddelay;
    }
    private static class DoTagGroupAction implements Runnable {
        Map<String, TagGroupInfo> add_upd_ids = new HashMap<String, TagGroupInfo>();
        Set<String> delete_ids = new TreeSet<String>();
        boolean isevt;
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            TagGroup tg;
            boolean did_add = false;
            /* If not event, need to do delete on ones not being added */
            if(!isevt) {
                Set<String> ids = ReaderEntityDirectory.getTagGroupIDs();
                for(String oldid : ids) {
                    if(add_upd_ids.containsKey(oldid) == false) {
                        delete_ids.add(oldid);
                    }
                }
            }
            for(TagGroupInfo tgi : add_upd_ids.values()) {
                /* Find tag group, if it exists */
                tg = ReaderEntityDirectory.findTagGroup(tgi.grpid);
                /* If it doesn't exist, need to build it */
                if(tg == null) {
                    /* Build parameters for create */
                    String[] prms = new String[2];
                    prms[0] = "groupcode=" + tgi.grpcode;
                    prms[1] = "locupddelay=" + tgi.locupddelay;
                    String errmsg = RangerServer.createTagGroup(tgi.grpid,
                        tgi.tagtype, prms, null);
                    if(errmsg == null) {
                        tg = ReaderEntityDirectory.findTagGroup(tgi.grpid);
                        if (tg != null)
                            RangerServer.saveTagGroup(tg);
                        did_add = true;
                    }
                }
                else {  /* Update */
                    /* Make copy of current attributes */
                    Map<String, Object> attr = new HashMap<String,Object>(tg.getTagGroupAttributes());
                    try {
                        Integer ival = Integer.parseInt(tgi.locupddelay);
                        /* Update values */
                        attr.put("locupddelay", ival);
                        attr.put("groupcode", tgi.grpcode);
                        tg.setTagGroupAttributes(attr);
                        RangerServer.saveTagGroup(tg);
                    } catch (NumberFormatException nfx) {
                    }
                }
            }
            /* Delete tag groups */
            for(String id : delete_ids) {
                tg = ReaderEntityDirectory.findTagGroup(id);
                if (tg != null) { /* If found, delete it */
                    /* Loop through readers, remove from any using it */
                    for(Reader r : ReaderEntityDirectory.getReaders()) {
                        TagGroup[] grps = r.getReaderTagGroups();
                        for(int i = 0; i < grps.length; i++) {
                            if(grps[i] == tg) { /* If match, need to remove */
                                TagGroup[] newtg = new TagGroup[grps.length-1];
                                if(i > 0)
                                    System.arraycopy(grps, 0, newtg, 0, i);
                                if(i < (grps.length-1))
                                    System.arraycopy(grps, i+1, newtg, i, grps.length-i-1);
                                try {
                                    r.setReaderTagGroups(newtg);
                                    RangerServer.saveReader(r);
                                } catch (BadParameterException bpx) {
                                }
                                grps = newtg;
                            }
                        }
                    }
                    RangerServer.deleteTagGroup(tg);
                    tg.cleanup(); /* And cleanup object */
                }
            }
            /* Get command handler for readeraddgroup command */
            CommandHandler ch = RangerServer.getCommandHandlers().get(ReaderAddGroupHandler.READERADDGROUP_CMD);
            if(did_add && (ch != null)) {
                try {
                    HashMap<String,String> parms = new HashMap<String,String>();
                    parms.put(ReaderAddGroupHandler.READERID, "*");
                    parms.put(ReaderAddGroupHandler.GROUPID_PREFIX + "0", "*");
                    ch.invokeCommand(ReaderAddGroupHandler.READERADDGROUP_CMD, null, parms);
                } catch (CommandException cx) {
                }
            }
            synchronized(this) {
                notify();
            }
        }
    }

    /**
     * Handle tag groups update from maximo
     */
    private void handleTagGroups(Document doc, boolean max6) throws ServletException {
        boolean isevt = false;
        String evt = null;
        Element docelem;
        if(max6) {
            NodeList hdr = doc.getElementsByTagName("Header");  /* Get header */
            if(hdr.getLength() > 0) {
                docelem = (Element)hdr.item(0);
                evt = docelem.getAttribute("event");
            }
        }
        else {
            docelem = doc.getDocumentElement();
            evt = docelem.getAttribute("event");
        }
        if((evt != null) && (evt.equals("1"))) {
            isevt = true;
        }
        NodeList locs = doc.getElementsByTagName("RFCTAGGROUP");
        DoTagGroupAction dogrpact = new DoTagGroupAction();
        dogrpact.isevt = isevt;
        for(int lid = 0; lid < locs.getLength(); lid++) {
            String grpid = null;
            String tagtype = null;
            String grpcode = null;
            String locupddelay = null;
            Element lnode = (Element)locs.item(lid);
            /* See if its a delete */
            String del = lnode.getAttribute("action");
            boolean isdel = false;
            if((del != null) && (del.equals("Delete"))) {
                isdel = true;
            }
            NodeList grplist = lnode.getElementsByTagName("TAGGROUPID");
            Element e;
            NodeList ec;
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                grpid = ec.item(0).getNodeValue();
            }
            grplist = lnode.getElementsByTagName("RFCTAGTYPE");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                tagtype = ec.item(0).getNodeValue();
            }
            grplist = lnode.getElementsByTagName("GROUPCODE");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                grpcode = ec.item(0).getNodeValue();
            }
            grplist = lnode.getElementsByTagName("LOCUPDDELAY");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                locupddelay = ec.item(0).getNodeValue();
            }
            /* If valid site ID and location ID, process it */
            if((grpid != null) && (tagtype != null) &&
                (grpcode != null) && (locupddelay != null)) {
                TagGroupInfo tgi = new TagGroupInfo();
                tgi.grpid = grpid;
                tgi.grpcode = grpcode;
                tgi.tagtype = tagtype;
                tgi.locupddelay = locupddelay;
                if(!isdel)
                    dogrpact.add_upd_ids.put(grpid, tgi);
                else
                    dogrpact.delete_ids.add(grpid);
            }
        }
        /* Now run on session thread */
		try {
	        RangerProcessor.runActionNow(dogrpact);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
    }

    private static class ReaderInfo {
        String rdrid;
        String rdrtype;
        boolean enab;
        Map<String,String> settings;
    }        
     
    private static class DoReaderAction implements Runnable {
        Map<String, ReaderInfo> add_upd_ids = new HashMap<String, ReaderInfo>();
        Set<String> delete_ids = new TreeSet<String>();
        boolean isevt;
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            Reader rdr;
            /* If not event, need to do delete on ones not being added */
            if(!isevt) {
                Set<String> ids = ReaderEntityDirectory.getReaderIDs();
                for(String oldid : ids) {
                    if(add_upd_ids.containsKey(oldid) == false) {
                        delete_ids.add(oldid);
                    }
                }
            }
            //RangerServer.info("readeraction");
            for(ReaderInfo ri : add_upd_ids.values()) {
                /* Find reader, if it exists */
                rdr = ReaderEntityDirectory.findReader(ri.rdrid);
                /* If it doesn't exist, need to build it */
                if(rdr == null) {
                    /* Build parameters for create */
                    ArrayList<String> prms = new ArrayList<String>();
                    for(Map.Entry<String,String> me : ri.settings.entrySet()) {
                        prms.add(me.getKey() + "=" + me.getValue());
                    }
                    /* Build list of all tag groups to add */
                    Set<String> tgids = ReaderEntityDirectory.getTagGroupIDs();
                    ArrayList<String> tg = new ArrayList<String>();
                    for(String tgid : tgids)
                        tg.add(tgid);
                    String errmsg = RangerServer.createReader(ri.rdrid,
                        ri.rdrtype, prms, tg, null);
                    if(errmsg == null) {
                        rdr = ReaderEntityDirectory.findReader(ri.rdrid);
                        if (rdr != null) {
                            rdr.setReaderEnabled(ri.enab);
                            RangerServer.saveReader(rdr);
                        }
                        /* Enqueue create event, skipping provided attribs */
                        ext.encodeReaderSettingsEvent(rdr, EVENTCREATE,
                            ri.settings.keySet());
                    }
                    else {
                        RangerServer.error(errmsg);
                    }
                }
                else {  /* Update */
                    /* Make copy of current attributes */
                    Map<String, Object> attr = new HashMap<String,Object>(rdr.getReaderAttributes());
                    for(Map.Entry<String,String> me : ri.settings.entrySet()) {
                        String id = me.getKey();
                        String val = me.getValue();
                        Object oval = attr.get(id);     /* Get existing value */                    
                        if(oval == null) {  /* If null, bad attribute */
                        }
                        /* Else, if its an integer */
                        else if (oval instanceof Integer) {
                            try {
                                Integer ival = Integer.parseInt(val);
                                /* Update value */
                                attr.put(id, ival);
                            } catch (NumberFormatException nfx) {
                            }
                        }
                        /* Else, if its a boolean */
                        else if (oval instanceof Boolean) {
                            Boolean bval = Boolean.valueOf(val);
                           /* Update value */
                           attr.put(id, bval);
                        }
                        /* Else, if its a set of entity ids */
                        else if (oval instanceof Set) {
                            Set<ReaderEntity> nval = new HashSet<ReaderEntity>();
                            String[] l = val.split(",");
                            for (String s : l) {
                                ReaderEntity re = ReaderEntityDirectory.findEntity(s);
                                if (re != null)
                                    nval.add(re);
                            }
                            attr.put(id, nval);
                        } else { /* Else, put in a string */
                            attr.put(id, val);
                        }
                    }
                    try {
                        rdr.setReaderAttributes(attr);
                        rdr.setReaderEnabled(ri.enab);
                        RangerServer.saveReader(rdr);
                        /* Enqueue update event, skipping provided attribs */
                        ext.encodeReaderSettingsEvent(rdr, EVENTUPDATE,
                            ri.settings.keySet());
                    } catch (BadParameterException bpx) {
                        RangerServer.error("Bad parameter from Maximo on update for reader " + ri.rdrid);
                    }
                }
            }
            for(String delid : delete_ids) {
                /* Find the selected reader */
                rdr = ReaderEntityDirectory.findReader(delid);
                if (rdr == null) /* Doesn't exist */
                    continue;
                /* If enabled, need to release license */
                if(rdr.getReaderEnabled())
                	rdr.setReaderLicense(false);
                RangerServer.deleteReader(rdr); /* Delete the file */
                rdr.cleanup();
            }
            synchronized(this) {
                notify();
            }
        }
    }

    /**
     * Handle readers update from maximo
     */
    private void handleReaders(Document doc, boolean max6) throws ServletException {
        boolean isevt = false;
        String evt = null;
        Element docelem;
        if(max6) {
            NodeList hdr = doc.getElementsByTagName("Header");  /* Get header */
            if(hdr.getLength() > 0) {
                docelem = (Element)hdr.item(0);
                evt = docelem.getAttribute("event");
            }
        }
        else {
            docelem = doc.getDocumentElement();
            evt = docelem.getAttribute("event");
        }
        if((evt != null) && (evt.equals("1"))) {
            isevt = true;
        }
        NodeList rdrs = doc.getElementsByTagName("RFCREADER");
        DoReaderAction dordract = new DoReaderAction();
        dordract.isevt = isevt;
        for(int rid = 0; rid < rdrs.getLength(); rid++) {
            String extname = null;
            Element e;
            NodeList ec;
            Element lnode = (Element)rdrs.item(rid);
            /* Get external system name - ignore record if its not us */
            NodeList grplist = lnode.getElementsByTagName("EXTSYSNAME");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                extname = ec.item(0).getNodeValue();
            }
            if(extsysname.equals(extname) == false)
                continue;
            /* See if its a delete */
            String del = lnode.getAttribute("action");
            boolean isdel = false;
            if((del != null) && (del.equals("Delete"))) {
                isdel = true;
            }
            /* Get reader ID */
            String rdrid = null;
            grplist = lnode.getElementsByTagName("READERID");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                rdrid = ec.item(0).getNodeValue();
            }
            /* Get reader type */
            String rdrtype = null;
            grplist = lnode.getElementsByTagName("RFCREADERTYPE");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                rdrtype = ec.item(0).getNodeValue();
            }
            /* Get reader enabled */
            String enabled = null;
            grplist = lnode.getElementsByTagName("ENABLED");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                enabled = ec.item(0).getNodeValue();
            }
            boolean enab = "1".equals(enabled);
            /* Get settings */
            HashMap<String,String> settings = new HashMap<String,String>();
            grplist = lnode.getElementsByTagName("RFCREADERSETTING");
            for(int i = 0; i < grplist.getLength(); i++) {
                Element setting = (Element)grplist.item(i);
                NodeList vlist = setting.getElementsByTagName("RFCREADERATTRIB");
                String aid = null;
                if(vlist.getLength() > 0) {
                    e = (Element)vlist.item(0);
                    ec = e.getChildNodes();
                    if(ec.getLength() > 0)
                        aid = ec.item(0).getNodeValue();
                }
                vlist = setting.getElementsByTagName("ATTRIBVALUE");
                String aval = null;
                if(vlist.getLength() > 0) {
                    e = (Element)vlist.item(0);
                    ec = e.getChildNodes();
                    if(ec.getLength() > 0)
                        aval = ec.item(0).getNodeValue();
                    else
                        aval = "";
                }
                if((aid != null) && (aval != null))
                    settings.put(aid, aval);
            }
            /* If a delete, add to delete list */
            if(isdel) {
                dordract.delete_ids.add(rdrid);
            }
            else {      /* Else build add/update record */
                ReaderInfo rdrinfo = new ReaderInfo();
                rdrinfo.rdrid = rdrid;
                rdrinfo.rdrtype = rdrtype;
                rdrinfo.enab = enab;
                rdrinfo.settings = settings;
                dordract.add_upd_ids.put(rdrid, rdrinfo);   /* Add to table */
            }
        }
        /* Now run on ranger processor thread */
		try {
	        RangerProcessor.runActionNow(dordract);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
    }

    private static class RuleInfo {
        String ruleid;
        String ruletype;
        String location;
        boolean enab;
        Map<String,String> settings;
    }        
     
    private static class DoRuleAction implements Runnable {
        Map<String, RuleInfo> add_upd_ids = new HashMap<String, RuleInfo>();
        Set<String> delete_ids = new TreeSet<String>();
        boolean isevt;
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            LocationRule rule;
            /* If not event, need to do delete on ones not being added */
            if(!isevt) {
                Set<String> ids = ReaderEntityDirectory.getLocationRuleIDs();
                for(String oldid : ids) {
                    if(add_upd_ids.containsKey(oldid) == false) {
                        delete_ids.add(oldid);
                    }
                }
            }
            //RangerServer.info("ruleaction");
            for(RuleInfo ri : add_upd_ids.values()) {
                /* Find rule, if it exists */
                rule = ReaderEntityDirectory.findLocationRule(ri.ruleid);
                LocationRuleFactory rulefact = ReaderEntityDirectory.findLocationRuleFactory(ri.ruletype);
                if(rulefact == null) {
                    RangerServer.error("Error: bad rule type " + ri.ruletype + " for " + ri.ruleid);
                    continue;
                }
                /* If it doesn't exist, need to build it */
                if(rule == null) {
                    /* Build parameters for create */
                    ArrayList<String> prms = new ArrayList<String>();
                    for(Map.Entry<String,String> me : ri.settings.entrySet()) {
                        prms.add(me.getKey() + "=" + me.getValue());
                    }
                    String errmsg = RangerServer.createLocationRule(ri.ruleid, 
                        ri.ruletype, ri.location, prms.toArray(new String[prms.size()]));
                    if(errmsg == null) {
                        rule = ReaderEntityDirectory.findLocationRule(ri.ruleid);
                        if (rule != null) {
                            rule.setRuleEnabled(ri.enab);
                            RangerServer.saveLocationRule(rule);
                        }
                    }
                    else {
                        RangerServer.error(errmsg);
                    }
                    /* Enqueue create event, skipping provided attribs */
                    Map<String, Object> attr;
                    if(rule != null)
                        attr = rule.getLocationRuleAttributes();
                    else
                        attr = rulefact.getDefaultLocationRuleAttributes();
                    ext.encodeRuleSettingsEvent(ri.ruleid, 
                        attr, EVENTCREATE, ri.settings.keySet());
                }
                else {  /* Update */
                    /* Make copy of current attributes */
                    Map<String, Object> attr = new HashMap<String,Object>(rule.getLocationRuleAttributes());
                    for(Map.Entry<String,String> me : ri.settings.entrySet()) {
                        String id = me.getKey();
                        String val = me.getValue();
                        Object oval = attr.get(id);     /* Get existing value */                    
                        if(oval == null) {  /* If null, bad attribute */
                        }
                        /* Else, if its an integer */
                        else if (oval instanceof Integer) {
                            try {
                                Integer ival = Integer.parseInt(val);
                                /* Update value */
                                attr.put(id, ival);
                            } catch (NumberFormatException nfx) {
                            }
                        }
                        /* Else, if its a boolean */
                        else if (oval instanceof Boolean) {
                            Boolean bval = Boolean.valueOf(val);
                           /* Update value */
                           attr.put(id, bval);
                        }
                        /* Else, if its a set of entity ids */
                        else if (oval instanceof Set) {
                            Set<ReaderEntity> nval = new HashSet<ReaderEntity>();
                            String[] l = val.split(",");
                            for (String s : l) {
                                ReaderEntity re = ReaderEntityDirectory.findEntity(s);
                                if (re != null)
                                    nval.add(re);
                            }
                            attr.put(id, nval);
                        } else { /* Else, put in a string */
                            attr.put(id, val);
                        }
                    }
                    try {
                        rule.setLocationRuleAttributes(attr);
                        rule.setRuleEnabled(ri.enab);
                        RangerServer.saveLocationRule(rule);
                    } catch (BadParameterException bpx) {
                        RangerServer.error("Bad parameter from Maximo on update for rule " + ri.ruleid);
                    }
                    /* Enqueue update event, skipping provided attribs */
                    ext.encodeRuleSettingsEvent(ri.ruleid,
                        rule.getLocationRuleAttributes(), EVENTUPDATE,
                        ri.settings.keySet());
                }
            }
            for(String delid : delete_ids) {
                /* Find the selected rule */
                rule = ReaderEntityDirectory.findLocationRule(delid);
                if (rule == null) /* Doesn't exist */
                    continue;
                RangerServer.deleteLocationRule(rule); /* Delete the file */
                rule.cleanup();
            }
            synchronized(this) {
                notify();
            }
        }
    }

    /**
     * Handle rules update from maximo
     */
    private void handleRules(Document doc, boolean max6) throws ServletException {
        boolean isevt = false;
        String evt = null;
        Element docelem;
        if(max6) {
            NodeList hdr = doc.getElementsByTagName("Header");  /* Get header */
            if(hdr.getLength() > 0) {
                docelem = (Element)hdr.item(0);
                evt = docelem.getAttribute("event");
            }
        }
        else {
            docelem = doc.getDocumentElement();
            evt = docelem.getAttribute("event");
        }
        if((evt != null) && (evt.equals("1"))) {
            isevt = true;
        }
        NodeList rules = doc.getElementsByTagName("RFCRULE");
        DoRuleAction doruleact = new DoRuleAction();
        doruleact.isevt = isevt;
        for(int rid = 0; rid < rules.getLength(); rid++) {
            String extname = null;
            Element e;
            NodeList ec;
            Element lnode = (Element)rules.item(rid);
            /* Get external system name - ignore record if its not us */
            NodeList grplist = lnode.getElementsByTagName("EXTSYSNAME");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                extname = ec.item(0).getNodeValue();
            }
            if(extsysname.equals(extname) == false)
                continue;
            /* See if its a delete */
            String del = lnode.getAttribute("action");
            boolean isdel = false;
            if((del != null) && (del.equals("Delete"))) {
                isdel = true;
            }
            /* Get rule ID */
            String ruleid = null;
            grplist = lnode.getElementsByTagName("RULEID");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                ruleid = ec.item(0).getNodeValue();
            }
            /* Get rule type */
            String ruletype = null;
            grplist = lnode.getElementsByTagName("RFCRULETYPE");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                ruletype = ec.item(0).getNodeValue();
            }
            /* Get location */
            String location = null;
            grplist = lnode.getElementsByTagName("LOCATION");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                location = ec.item(0).getNodeValue();
            }
            /* Get site ID */
            String siteid = null;
            grplist = lnode.getElementsByTagName("SITEID");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                siteid = ec.item(0).getNodeValue();
            }
            if(!defsiteid.equals(siteid)) {  /* Not default site? */
                location = siteid + "__" + location;
            }
            /* Get rule enabled */
            String enabled = null;
            grplist = lnode.getElementsByTagName("ENABLED");
            if(grplist.getLength() > 0) {
                e = (Element)grplist.item(0);
                ec = e.getChildNodes();
                enabled = ec.item(0).getNodeValue();
            }
            boolean enab = "1".equals(enabled);
            /* Get settings */
            HashMap<String,String> settings = new HashMap<String,String>();
            grplist = lnode.getElementsByTagName("RFCRULESETTING");
            for(int i = 0; i < grplist.getLength(); i++) {
                Element setting = (Element)grplist.item(i);
                NodeList vlist = setting.getElementsByTagName("RFCRULEATTRIB");
                String aid = null;
                if(vlist.getLength() > 0) {
                    e = (Element)vlist.item(0);
                    ec = e.getChildNodes();
                    if(ec.getLength() > 0)
                        aid = ec.item(0).getNodeValue();
                }
                vlist = setting.getElementsByTagName("ATTRIBVALUE");
                String aval = null;
                if(vlist.getLength() > 0) {
                    e = (Element)vlist.item(0);
                    ec = e.getChildNodes();
                    if(ec.getLength() > 0)
                        aval = ec.item(0).getNodeValue();
                    else
                        aval = "";
                }
                if((aid != null) && (aval != null))
                    settings.put(aid, aval);
            }
            /* If a delete, add to delete list */
            if(isdel) {
                doruleact.delete_ids.add(ruleid);
            }
            else {      /* Else build add/update record */
                RuleInfo ruleinfo = new RuleInfo();
                ruleinfo.ruleid = ruleid;
                ruleinfo.ruletype = ruletype;
                ruleinfo.location = location;
                ruleinfo.enab = enab;
                ruleinfo.settings = settings;
                doruleact.add_upd_ids.put(ruleid, ruleinfo);   /* Add to table */
            }
        }
        /* Now run on ranger processor thread */
		try {
	        RangerProcessor.runActionNow(doruleact);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
    }
    /**
     * Handle requests from Maximo server
     */
    public static void handleRequest(HttpServletRequest request,
        HttpServletResponse response,
        CommandContext ctx) throws ServletException, IOException {
        RangerServer.info("Call from Maximo!!!");
        InputStream is = request.getInputStream();
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(is);
            // normalize text representation
            doc.getDocumentElement ().normalize ();
            //printElement(doc);
            /* Now, check the top level element and route document to correct handler */
            String top = doc.getDocumentElement().getNodeName();
            if(top.equals("PublishRFCLOCS")) {          /* Handle V7 */
                ext.handleLocations(doc, false);
            }
            else if(top.equals("RFCLOCSInterface")) {   /* Handle V6 */
                ext.handleLocations(doc, true);
            }
            else if(top.equals("PublishRFCTAGGROUPS")) {    /* Handle V7 */
                ext.handleTagGroups(doc, false);
            }
            else if(top.equals("RFCTAGGROUPSInterfac")) {  /* Handle V6 */
                ext.handleTagGroups(doc, true);
            }
            else if(top.equals("PublishRFCREADERS")) {
                ext.handleReaders(doc, false);              /* Handle V7 */
            }
            else if(top.equals("RFCREADERSInterface")) {    /* Handle V6 */
                ext.handleReaders(doc, true);
            }
            else if(top.equals("PublishRFCRULES")) {
                ext.handleRules(doc, false);            /* Hanslw V7 */
            }
            else if(top.equals("RFCRULESInterface")) {
                ext.handleRules(doc, true);             /* Handle V6 */
            }
            else {
                RangerServer.info ("Root element of the doc is " + 
                     doc.getDocumentElement().getNodeName());
            }
        } catch (SAXParseException err) {
            RangerServer.error ("** Parsing error" + ", line " 
                 + err.getLineNumber () + ", uri " + err.getSystemId ());
            RangerServer.error(" " + err.getMessage ());
        } catch (SAXException e) {
            RangerServer.getLogger().error(e);
        } catch (ParserConfigurationException pcx) {
            RangerServer.error("Parsing config error: " + pcx.getMessage ());
        }
    }
    /** Our listener object - internalized */
    private class Listener implements ReaderStatusListener, ReaderLifecycleListener,
            TagStatusListener, TagLifecycleListener {
        /**
         * Reader creation notification - called when new reader object creation
         * completed (at the end of its init() call).
         * 
         * @param reader -
         *            reader object created
         */
        public void readerLifecycleCreate(Reader reader) {
            if(operational)
                encodeReaderEvent(reader, EVENTCREATE);
        }
        /**
         * Reader deletion notification - called at start of reader's cleanup()
         * method, when reader is about to be deleted.
         * 
         * @param reader -
         *            reader object being deleted
         */
        public void readerLifecycleDelete(Reader reader) {
            if(operational)
                encodeReaderEvent(reader, EVENTDELETE);
        }
        /**
         * Reader enable change notification - called when reader has just changed
         * its enabled state (after the setReaderEnabled() method has been called).
         * 
         * @param reader -
         *            reader object being enabled or disabled
         * @param enable -
         *            true if now enabled, false if now disabled
         */
        public void readerLifecycleEnable(Reader reader, boolean enable) {
            if(operational)
                encodeReaderEvent(reader, EVENTUPDATE);
        }
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         * 
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            if(operational)
                encodeReaderEvent(reader, EVENTUPDATE);
        }
        /**
         * Tag created notification. Called after tag has been created, but before
         * its TagLinks have been added (i.e. after end of init()). Any initial
         * attributes from creating message will have been added by the time this is
         * called.
         * 
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            if(operational)
                encodeTagEvent(tag, EVENTFOUND);
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag is
         * about to be deleted (generally due to having no active TagLinks).
         * 
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
            if(operational)
                encodeTagEvent(tag, EVENTLOST);
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than the
         *            number of attributes for the given tag). Ordered to match
         *            attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            if(operational)
                encodeTagEvent(tag, EVENTUPDATE);
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        /**
         * TagLink added notificiation. Called after taglink has been added to tag.
         * 
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed from
         * tag.
         * 
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
        }
    }
}
