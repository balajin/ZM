package com.rfcode.ranger.extensions;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Enumeration;

import com.rfcode.ranger.RangerServer;

import org.apache.commons.httpclient.util.ParameterFormatter;
import org.apache.commons.httpclient.util.URIUtil;

import java.io.IOException;
import java.util.HashMap;

/**
 * Test servlet for zonemgr upconnect - provides two sets of URLs: one for up-connection
 * from zonemgr, and a second providing a mapping of standard "downconnect" URL
 * APIs to up-connected zonemgrs.
 *
 *   The up-connection is to /rfcode_zonemgr/upconnect/post/<zmid>, for zone manager with ID of <zmid>
 *
 *   The down-connection to call function <func> on zonemanager <zmid> is
 *     /rfcode_zonemgr/upconnect/redirect/<zmid>/<func>
 *
 * @author Mike Primm
 *
 */
public class UpConnectServlet extends HttpServlet {
    static final long serialVersionUID = 0x534abe2342dea234L;

    /**
     * Configured ZM IDs, as well as userids and passwords
     */
    private static class ConfiguredZMs {
        private String zmid;
        private String userid;
        private String password;
    }

    /** Table of valid and configured ZMIDs */
    private HashMap<String,ConfiguredZMs> zmlist = new HashMap<String,ConfiguredZMs>();
    /** Table of active ZMIDs - one for each active up-connection */
    private HashMap<String, ActiveZM> activezm = new HashMap<String, ActiveZM>();

    /**
     * Activation record for specific linked ZM
     */
    private static class ActiveZM {
        /** Queue of pending requests to given ZM */
        LinkedBlockingQueue<ZMRequest> req_queue = new LinkedBlockingQueue<ZMRequest>();
    };

    /**
     * Request to be handled by linked ZM
     */
    private static class ZMRequest {
        /* Base URL requested (part after /rfcode_zonemgr/zonemgr/api/), plus args */
        String request;
        /* Authentication requested */
        String userid;
        String password;
        /* Output - http return code */
        int httprc;
        /* Output - content type */
        String contenttype;
        /* Output - string containing body of response */
        String response;
    };

    /**
     * Base 64 string decode
     */
    public static String decode_b64(String s) {
        StringBuilder sb = new StringBuilder();
        int accum = 0;
        int accumcnt = 0;
        int len = s.length() & 0xFFFC;

        for (int i = 0; i < len; i++) {
            char c = s.charAt(i); /* Get character */
            int v = 0;
            if ((c >= 'A') && (c <= 'Z'))
                v = (int) c - (int) 'A';
            else if ((c >= 'a') && (c <= 'z'))
                v = (int) c - (int) 'a' + 26;
            else if ((c >= '0') && (c <= '9'))
                v = (int) c - (int) '0' + 52;
            else if (c == '+')
                v = 62;
            else if (c == '/')
                v = 63;
            else
                break;
            accum = (accum << 6) | v;
            accumcnt += 6;
            if (accumcnt >= 8) {
                sb.append((char) ((accum >> (accumcnt - 8)) & 0xFF));
                accumcnt -= 8;
            }
        }
        return sb.toString();
    }
    /**
     * Add known ZMID
     * @param zmid - ID of zone manager
     * @param uid - userid for authenticating it
     * @param pwd - password for authenticating it
     */
    public void addConfiguredZM(String zmid, String uid, String pwd) {
        ConfiguredZMs zm = new ConfiguredZMs();
        zm.zmid = zmid;
        zm.userid = uid;
        zm.password = pwd;    
        zmlist.put(zm.zmid, zm);
    }
    /**
     * Remove known ZMID
     * @param zmid - ID to remove
     */
    public void removeConfiguredZM(String zmid) {
        zmlist.remove(zmid);
    }
    /**
     * Init for servlet - gets things running
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        /* For test, just hard code a couple of ZMIDs */
        addConfiguredZM("zm1", "userid1", "password1");
        addConfiguredZM("zm2", "userid2", "password2");
        addConfiguredZM("zm3", "userid3", "password2");        
    }
    /**
     * Servlet unload method - do cleanup
     */
    public void destroy() {
        super.destroy();
    }

    /* ------------------------------------------------------------ */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String cmd = request.getPathInfo();
        if(cmd == null) cmd = "/";
        if(!cmd.startsWith(UPCONNECTREDIRECT_PATH)) {
			if(cmd.endsWith("/firmware.zip")) {		/* If firmware library request */
				response.setContentType("application/zip");				
				FileInputStream fis = new FileInputStream(new File(RangerServer.getDataDir(), "firmware.zip.update"));
				OutputStream out = response.getOutputStream();
				byte[] buf = new byte[4096];
				int len;
				while((len = fis.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				fis.close();
			}
			else {
	            PrintWriter writer = response.getWriter();
    	        writer.println("<html>");
    	        writer.println("<head><title>Zone Manager Up-Connect Servlet</title></head>");
    	        writer.println("<body><b>Zone Manager Up-Connect Servlet</b>");
    	        writer.println("<p>Up-Connect only available using POST method.</body>");
    	        writer.println("</html>");
	            writer.close();
			}
        }
        else    /* Redirect API OK to do GETs on */
            doPost(request, response);
    }

    private static final String UPCONNECT_PATH = "/post/";
    private static final String UPCONNECTREDIRECT_PATH = "/redirect/";
    /* ------------------------------------------------------------ */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String cmd;

        /* See what command is being requested */
        cmd = request.getPathInfo();

        if (cmd == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        if(cmd.startsWith(UPCONNECTREDIRECT_PATH)) {
            cmd = cmd.substring(UPCONNECTREDIRECT_PATH.length()); /* Remove base */
            int idx = cmd.indexOf('/'); /* Find split */
            if(idx > 0) {
                String zmid = cmd.substring(0, idx);    /* Get ZMID */
                cmd = cmd.substring(idx+1);             /* Command is rest */
                handleUpConnectRedirect(request, response, zmid, cmd);
                return;
            }
        }
        else if(cmd.startsWith(UPCONNECT_PATH)) { /* Upconnect request? */
            String zmid = cmd.substring(UPCONNECT_PATH.length());  /* Get ZMID */
            handleUpConnect(request, response, zmid);
            return;
        }
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    /**
     * Handle upconnect calls
     */
    private void handleUpConnect(HttpServletRequest request, 
        HttpServletResponse response, String zmid) 
        throws ServletException, IOException {
        /* Indicate that we're returning chunked encoding */
        response.addHeader("Transfer-Encoding", "chunked");

        /* Look up ZMID */
        ConfiguredZMs zm = zmlist.get(zmid);        
        /* If not found, return error */
        if(zm == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        /* If userid + password not blank, check auth */
        if((zm.userid.length() > 0) || (zm.password.length() > 0)) {
            String auth = request.getHeader("Authorization");            
            response.setHeader("WWW-Authenticate", "Basic realm=\"ZoneManagerUpconnect\"");
            if (auth == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
            boolean match = false;
            try {
                auth = auth.substring(auth.indexOf(" ")).trim();
                String decoded = decode_b64(auth);
                int i = decoded.indexOf(":");
                String username = decoded.substring(0, i);
                String password = decoded.substring(
                    i + 1, decoded.length());
                /* If either userid or password does not match, return error */
                if(username.equals(zm.userid) && password.equals(zm.password)) {
                    match = true;
                }
            } catch (Exception x) {
            }
            /* If no match, return unauthorized */
            if(!match) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        ActiveZM azm;
        ZMRequest req = null;
        /* We're good, so make active ZM handler for this one */
        azm = activezm.get(zmid);  /* Find existing, if any */
        if(azm != null) {   /* Already active?  Ignore the new one */
            response.sendError(HttpServletResponse.SC_CONFLICT);    /* Report as 409 */
            return;
        }
        try {
            azm = new ActiveZM();       /* Make new one */
            activezm.put(zmid, azm);    /* And put in table */

            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("text/plain");
            /* Now, need to read version info sent from sender */
            InputStream in = request.getInputStream();
            /* First, read version request from post-er */
            String v = readRequest(in);
            /* If bad, quit */
            if(!v.equals("_VERSION?version=1")) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            /* Send reply */
            OutputStream out = response.getOutputStream();
            writeCommand(out, "_VERSION?version=1");
    
            /* Initialize to unauthenticated */
            String cur_uid = "";
            String cur_pwd = "";
            /* Now, loop sending null commands every few seconds */
            while(true) {
                /* Poll the request queue */
                req = azm.req_queue.poll(15, TimeUnit.SECONDS);
                /* If no request, send ping to keep alive */
                if(req == null) {
                    writeCommand(out, "");
                    v = readResponse(in);   /* And get response */
                    continue;
                }
                /* Next, see if authentication requested is different */
                if(! (cur_uid.equals(req.userid) && cur_pwd.equals(req.password))) {
                    /* Set authorization information */
                    writeCommand(out, "_setauth?userid=" + req.userid +
                        "&password=" + req.password);
                    v = readResponse(in);
                    cur_uid = req.userid;
                    cur_pwd = req.password;
                }
                /* Next, send actual request */
                writeCommand(out, req.request);
                /* And fetch the response */
                v = readResponse(in);
                /* Parse the response in httrc, contenttype, and body */
                int idx = v.indexOf(',');
                req.httprc = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
                if(idx > 0) {
                    try {
                        req.httprc = Integer.parseInt(v.substring(0, idx));
                    } catch (NumberFormatException nfx) {}
                }
                int idx2 = v.indexOf(':');
                req.contenttype = "";
                req.response = "";
                if(idx2 > 0) {
                    req.contenttype = v.substring(idx+1, idx2);
                    req.response = URIUtil.decode(v.substring(idx2+1));
                }
                /* Signal that the request is completed */
                synchronized(req) {
                    req.notify();
                    req = null;
                }
            }
        } catch (InterruptedException ix) {
        } finally {     /* If we break out, need to clean up pending requests */
            /* Remove active ZM */
            activezm.remove(zmid);
            /* Signal error on current request */
            if(req != null) {
                req.httprc = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
                req.contenttype = req.response = "";
                synchronized(req) { req.notify(); }
            }
            /* Now, do the same for the rest of the requests queued */
            while((req = azm.req_queue.poll()) != null) {
                req.httprc = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
                req.contenttype = req.response = "";
                synchronized(req) { req.notify(); }
            }
        }
    }

    /**
     * Handle upconnect redirect calls
     */
    private void handleUpConnectRedirect(HttpServletRequest request, 
        HttpServletResponse response, String zmid, String cmd)
        throws ServletException, IOException {
        String userid = "";
        String pwd = "";
        /* Indicate that we're returning chunked encoding */
        response.addHeader("Transfer-Encoding", "chunked");
        /* Get authentication data to pass to request */
        String auth = request.getHeader("Authorization");            
        response.setHeader("WWW-Authenticate", "Basic realm=\"ZoneManagerUpconnectRedirect\"");
        if (auth != null) {
            try {
                auth = auth.substring(auth.indexOf(" ")).trim();
                String decoded = decode_b64(auth);
                int i = decoded.indexOf(":");
                userid = decoded.substring(0, i);
                pwd = decoded.substring(i + 1, decoded.length());
            } catch (Exception x) {
            }
        }
        /* Build request */
        ZMRequest req = new ZMRequest();
        req.request = cmd;
        Enumeration<?> pn = request.getParameterNames();
        while(pn.hasMoreElements()) {
            String parm = (String)pn.nextElement();
            String[] vals = request.getParameterValues(parm);
            for(String val : vals) {
                if(req.request == cmd)
                    req.request += "?";
                else
                    req.request += "&";
                req.request += URIUtil.encodeWithinQuery(parm) +
                    "=" + URIUtil.encodeWithinQuery(val);
            }
        }
        req.userid = userid;
        req.password = pwd;
        /* Now, see if ZM is linked */
        ActiveZM zm = activezm.get(zmid);
        /* If not active, return error */
        if(zm == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        synchronized(req) {
            zm.req_queue.offer(req);   /* Post it for processing */
            try {
                req.wait(); /* And wait for result */
            } catch (InterruptedException ix) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
        }
        if(req.httprc != 200) { /* Bad response? */
            response.sendError(req.httprc);
            return;
        }
        if(req.contenttype.equals("") == false) {   /* Set content type */
            response.setContentType(req.contenttype);
        }
        response.getWriter().print(req.response);   /* Send response */
	}
    /**
     * Read response string from stream
     */
    private String readResponse(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        while(true) {
            int c = in.read();  /* Get next character */
            if(c == -1) {   /* End read */
                return null;    /* End of stream */
            }
            if(c == '\n')   /* End of line? */
                break;
            sb.append((char)c);
        }
        return sb.toString();
    }
    /**
     * Read request from channel
     */
    public String readRequest(InputStream in) throws IOException {
        return readResponse(in);      /* Same format, for now */
    }
    /**
     * Write command string to stream and flush
     */
    public void writeCommand(OutputStream out, String s) throws IOException {
        s += "\n";  /* End with newline */
        out.write(s.getBytes());
        out.flush();
    }
    /**
     * Write reply to channel
     */
    public void sendReply(OutputStream out, int httprc, String contenttype, String rep) throws IOException {
        StringBuffer sb = new StringBuffer();
        ParameterFormatter.formatValue(sb, rep, false);
        writeCommand(out, Integer.toString(httprc) + "," + contenttype + ":" + sb.toString());
    }

    public String getServletInfo() {
        return "ZoneManager Upconnect sample servlet v0.01";
    }
}
