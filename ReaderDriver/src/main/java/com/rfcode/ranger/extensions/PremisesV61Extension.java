package com.rfcode.ranger.extensions;

import com.rfcode.ranger.RangerExtension;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.ConfigUpdateListener;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.TagLifecycleListener;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.ibm.sensorevent.model.IBMSensorEvent;
import com.ibm.sensorevent.model.ISensorEvent;
import com.ibm.sensorevent.model.converter.XMLConverter;
import com.ibm.sensorevent.model.generic.SensorEventException;
/**
 * IBM Premises V6.1 event export extension
 *
 * This extension, when active, publishes tag and reader status events to
 * the Premises V6.1 HTTP interface (http://hostname:port/ibmse/eventpublish).
 * Events are mapped into Premises events as follows:
 *    event-type: RFCode/Tag/Found - tag found/created event
 *    event-type: RFCode/Tag/Update - tag updated event
 *    event-type: RFCode/Tag/Lost - tag deleted/lost event
 *    event-type: RFCode/Reader/Create - reader added event
 *    event-type: RFCode/Reader/Update - reader updated event
 *    event-type: RFCode/Reader/Delete - reader deleted event
 * All events include all the object's attributes (versus just changes) mapped
 * as metadata for the Premises event.
 * In order to prevent filling memory during a connectivity loss/error, each
 * object (tag-id, reader-id) will only have one pending "update" event queued
 * at a time - if a second occurs, it will replace the previously queued event.
 *
 * Configuration of the extension is via the 'configset' command.  The settings
 * include:
 *   premises_host - hostname of premises server (required - disabled if not set)
 *   premises_port - port number of premises server (default = 9080)
 *   premises_source - source ID for premises events (default - RFCodeZoneMgr)
 *   premises_enabled - enable/disable (true=enabled, other=disabled) (default=enabled)
 */
public class PremisesV61Extension implements RangerExtension,
    ConfigUpdateListener {
    public static final String PREMISES_HOSTNAME = "premises_hostname";
    public static final String PREMISES_HOSTNAME_DEFAULT = "";
    public static final String PREMISES_PORT = "premises_port";
    public static final String PREMISES_PORT_DEFAULT = "9080";
    public static final String PREMISES_SOURCE = "premises_source";
    public static final String PREMISES_SOURCE_DEFAULT = "RFCodeZoneMgr";
    public static final String PREMISES_ENABLED = "premises_enabled";
    public static final String PREMISES_ENABLED_DEFAULT = "true";
	public static final String PREMISES_API_VERSION = "premises_version";
	public static final String PREMISES_API_VERSION_DEFAULT = "6";	

    public static final int RETRY_DELAY = 5000; /* 5 sec */
    public static final int CONNECT_TIMEOUT = 30000; /* 30 seconds */
    public static final int READ_TIMEOUT = 10000; /* 10 seconds */

    public static final String IBMSE_EVENT_URL = "/ibmse/eventpublish";

    public static final String TAGEVENTBASE = "RFCode/Tag/";
    public static final String RDREVENTBASE = "RFCode/Reader/";
    public static final String EVENTCREATE = "Create";
    public static final String EVENTFOUND = "Found";
    public static final String EVENTUPDATE = "Update";
    public static final String EVENTDELETE = "Delete";
    public static final String EVENTLOST = "Lost";
    private URL url;
    private String source;
    private Listener lsnr;
    /* Our processing thread */
    private ServiceThread our_thread;
    /* Our event queue */
    private LinkedBlockingQueue<PremisesEvent> evt_queue = new LinkedBlockingQueue<PremisesEvent>();
    /* Table of pending updates, keyed by object ID, containing message to send */
    private HashMap<String, String> upd_table = new HashMap<String, String>();
    /**
     * Handler thread - does all the work
     */
    private class ServiceThread extends Thread {
        public ServiceThread() {
            super("PremisesExtension Service");
        }
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }
    /**
     * Premises event to be sent
     */
    public class PremisesEvent {
        String type;
        String xml; /* XML encoded message to be sent (for non-update events, null for updates) */
        String id;  /* Object ID (non-null for updates) - update found in table */
    }
    /**
     * Extension init
     */
    public void extensionInit() {
        /* Register config update hook */
        RangerServer.addConfigUpdateListener(this);

        updateConfig();
    }
    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
    public void configUpdateNotification() {
        updateConfig();
    }
    /**
     * Update config, and return if active
     */
    private boolean updateConfig() {
        boolean is_enab = false;
        Properties cfg = RangerServer.getConfig();
        String enab = cfg.getProperty(PREMISES_ENABLED, PREMISES_ENABLED_DEFAULT);
        String host = cfg.getProperty(PREMISES_HOSTNAME, PREMISES_HOSTNAME_DEFAULT);
        String port = cfg.getProperty(PREMISES_PORT, PREMISES_PORT_DEFAULT);
		String ver = cfg.getProperty(PREMISES_API_VERSION, PREMISES_API_VERSION_DEFAULT);
        URL newurl = null;
        /* Update source ID */
        source = cfg.getProperty(PREMISES_SOURCE, PREMISES_SOURCE_DEFAULT);
		/* If enabled AND hostname defined AND set to 6.x version */
        if(enab.equals("true") && (host.length() > 0) && (ver.charAt(0) == '6')) {
            int pnum = -1;
            try {
                pnum = Integer.parseInt(port);
            } catch (NumberFormatException nfx) {
                pnum = 9080;
            }
            try {
                newurl = new URL("http", host, pnum, IBMSE_EVENT_URL);             
                is_enab = true;
            } catch (MalformedURLException mux) {
                is_enab = false;
            }
        }
        if(is_enab == true) {   /* If active, start up */
            url = newurl;
            if(our_thread == null) {    /* Not active? */
                /* Start it up */
                our_thread = new ServiceThread();
                our_thread.setDaemon(true);
                our_thread.start();
            }
            if(lsnr == null) {
                lsnr = new Listener();
                /* Start listening to tag updates */
                AbstractTagType.addGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.addReaderStatusListener(lsnr);
                RangerServer.addReaderLifecycleListener(lsnr);
            }
        }
        else {  /* Else, if not active */
            if(lsnr != null) {
                /* Stop listening to tag updates */
                AbstractTagType.removeGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.removeReaderStatusListener(lsnr);
                RangerServer.removeReaderLifecycleListener(lsnr);
                lsnr = null;
            }
            if(our_thread != null) {
                our_thread.interrupt();
            }
            synchronized(upd_table) {
                evt_queue.clear();  /* Toss all pending events */
                upd_table.clear();
            }
        }
        return is_enab;
    }

    /**
     * Processing method - called by our service thread
     */
    private void process() {
        PremisesEvent pushed_evt = null;
        try {
            while(true) {
                PremisesEvent evt;
                if(pushed_evt != null) {
                    evt = pushed_evt; pushed_evt = null;
                }
                else {
                    evt = evt_queue.poll(15, TimeUnit.SECONDS);
                }
                if(evt == null) continue;
                /* If ID defined, get message from update table */
                if(evt.id != null) {
                    synchronized(upd_table) {
                        evt.xml = upd_table.remove(evt.id);
                        evt.id = null;
                    }
                }
                /* Build and issue request */
                try {
            		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	    	        connection.setRequestMethod("POST");
            		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	    	        connection.setUseCaches(false);
	    	        connection.setDoInput(true);
            		connection.setDoOutput(true);
                    connection.setConnectTimeout(CONNECT_TIMEOUT);
                    connection.setReadTimeout(READ_TIMEOUT);
	    	        connection.connect();
		
            		StringBuffer data = new StringBuffer();		
                	data.append("eventxml=" + URLEncoder.encode(evt.xml, "UTF-8"));
            		DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
            	    dos.writeBytes(data.toString());
            		dos.flush();
            		dos.close();
            		InputStream is = connection.getInputStream();
            		is.close();
                    /* Check response code */
                    if (connection.getResponseCode() == 200) {
                        /* Message sent */
                        //RangerServer.info("Posted " + evt.type + " to IBM Premises Server");
                    }
                    else {  /* Else, push it back on the head of the queue */
                        pushed_evt = evt;
                        RangerServer.error("POST to Premises failed: URL=" + url + ", response=" + 
                            connection.getResponseCode() + "(" + connection.getResponseMessage() + ")");
                        Thread.sleep(RETRY_DELAY);
                    }
                } catch (IOException iox) {
                    pushed_evt = evt;
                    RangerServer.error("POST to Premises failed: URL=" + url + ", response=" +
                        iox.toString());
                    Thread.sleep(RETRY_DELAY);
                }
            }
        } catch (InterruptedException ix) {
        }
    }
    /**
     * Encode tag event
     */
    private void encodeTagEvent(Tag tag, String type) {
        Map<String,Object> attrs = new HashMap<String,Object>(tag.getTagAttributes());
        String id = tag.getTagGUID();
        String locpath = null;
        attrs.put("tagtype", tag.getTagType().getID());
        attrs.put("tagid", id);
        attrs.put("taggroup", tag.getTagGroup().getID());
        /* Look up our location, if defined */
        Object loc = attrs.get(LocationRuleFactory.ATTRIB_ZONELOCATION);
        if ((loc != null) && (loc instanceof String)) {
            Location our_loc = ReaderEntityDirectory.findLocation((String)loc);
            /* If found, add our inherited attributes too */
            if(our_loc != null) {
                Map<String, Object> vals = our_loc.getFullAttributes();
                if(vals != null) {
                    for(Map.Entry<String,Object> mape : vals.entrySet()) {
                        attrs.put("location." + mape.getKey(), mape.getValue());
                    }
                }
                locpath = "/" + our_loc.getPath();
            }
        }
        encodeEvent(TAGEVENTBASE+type, id, attrs, type.equals("Update"), locpath);
    }
    /**
     * Encode reader event
     */
    private void encodeReaderEvent(Reader rdr, String type) {
        Map<String,Object> attrs = new HashMap<String,Object>(rdr.getReaderAttributes());
        String id = rdr.getID();
        attrs.put("readertype", rdr.getReaderFactory().getID());
        attrs.put("readerid", id);
        attrs.put("enabled", rdr.getReaderEnabled());
        attrs.put("state", rdr.getReaderStatus());
        TagGroup[] grps = rdr.getReaderTagGroups();
        Set<String> g = new TreeSet<String>();
        for(TagGroup grp : grps) {
            g.add(grp.getID());
        }
        attrs.put("groups", g);
        attrs.remove("password");   /* Remove password attribute, if defined */

        encodeEvent(RDREVENTBASE+type, id, attrs, type.equals("Update"), null);
    }
    /**
     * Encode event
     */
    private void encodeEvent(String eventType, String id, Map<String,Object> attrs,
        boolean update, String locpath) {
        try {
            ISensorEvent event = IBMSensorEvent.getInstance();
            event.getHeader().setEventType(eventType);
            event.getHeader().setSourceId(source);
            if(locpath != null)
                event.getHeader().setGeoLocation(locpath);
            for(Map.Entry<String,Object> me : attrs.entrySet()) {
                String k = me.getKey();
                Object v = me.getValue();
                if(v instanceof Boolean) {
                    event.getPayloadMetaData().addBooleanAttribute(k,
                        ((Boolean)v).booleanValue());
                }
                else if(v instanceof Double) {
                    event.getPayloadMetaData().addDoubleAttribute(k,
                        ((Double)v).doubleValue());
                }
                else if(v instanceof Integer) {
                    event.getPayloadMetaData().addIntAttribute(k,
                        ((Integer)v).intValue());
                }
                else if(v instanceof Set) {
                    Set<?> sv = (Set<?>)v;
                    String[] vv = new String[sv.size()];
                    int i = 0;
                    for(Object o : sv) {
                        vv[i] = o.toString();
                        i++;
                    }
                    if(vv.length > 0)
                        event.getPayloadMetaData().addStringArrayAttribute(k,
                            vv);
                }
                else if(v instanceof Map) {
                    @SuppressWarnings("unchecked")
					Map<String,Object> mv = (Map<String,Object>)v;
                    String[] vv = new String[mv.size()];
                    int i = 0;
                    for(Map.Entry<String,Object> ent : mv.entrySet()) {
                        vv[i] = ent.getKey() + "=" + 
                            ent.getValue().toString();
                        i++;
                    }
                    if(vv.length > 0)
                        event.getPayloadMetaData().addStringArrayAttribute(k,
                            vv);
                }
                else if(v == null) {
                    // per IBM, don't add null attributes
                    //event.getPayloadMetaData().addStringAttribute(k,
                    //    "");
                }
                else {
                    event.getPayloadMetaData().addStringAttribute(k,
                        v.toString());
                }
            }
            // convert to XML
            XMLConverter converter = (XMLConverter) XMLConverter.getInstance();
            String xml = converter.toXMLString(event);
            /* If update, see if pending update */
            PremisesEvent e = new PremisesEvent();
            e.type = eventType;
            if(update) {
                e.id = id;  /* Save ID in event, XML in table */
                synchronized(upd_table) {
                    if(upd_table.containsKey(id)) { /* If pending */
                        e = null;           /* Don't add again */
                    }
                    upd_table.put(id, xml); /* Put message in table */
                }
            }
            else {  /* Else, message in event */
                e.xml = xml;
            }
            /* Add new event to end of queue, if needed */
            if(e != null) {
                evt_queue.offer(e);
            }
        } catch (SensorEventException sex) {
            RangerServer.error("Error encoding Premises event - " + sex.toString());
        }
    }

    /** Our listener object - internalized */
    private class Listener implements ReaderStatusListener, ReaderLifecycleListener,
            TagStatusListener, TagLifecycleListener {
        /**
         * Reader creation notification - called when new reader object creation
         * completed (at the end of its init() call).
         * 
         * @param reader -
         *            reader object created
         */
        public void readerLifecycleCreate(Reader reader) {
            encodeReaderEvent(reader, EVENTCREATE);
        }
        /**
         * Reader deletion notification - called at start of reader's cleanup()
         * method, when reader is about to be deleted.
         * 
         * @param reader -
         *            reader object being deleted
         */
        public void readerLifecycleDelete(Reader reader) {
            encodeReaderEvent(reader, EVENTDELETE);
        }
        /**
         * Reader enable change notification - called when reader has just changed
         * its enabled state (after the setReaderEnabled() method has been called).
         * 
         * @param reader -
         *            reader object being enabled or disabled
         * @param enable -
         *            true if now enabled, false if now disabled
         */
        public void readerLifecycleEnable(Reader reader, boolean enable) {
            encodeReaderEvent(reader, EVENTUPDATE);
        }
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         * 
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            encodeReaderEvent(reader, EVENTUPDATE);
        }
        /**
         * Tag created notification. Called after tag has been created, but before
         * its TagLinks have been added (i.e. after end of init()). Any initial
         * attributes from creating message will have been added by the time this is
         * called.
         * 
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            encodeTagEvent(tag, EVENTFOUND);
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag is
         * about to be deleted (generally due to having no active TagLinks).
         * 
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
            encodeTagEvent(tag, EVENTLOST);
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than the
         *            number of attributes for the given tag). Ordered to match
         *            attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            encodeTagEvent(tag, EVENTUPDATE);
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        /**
         * TagLink added notificiation. Called after taglink has been added to tag.
         * 
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed from
         * tag.
         * 
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
        }
    }
}
