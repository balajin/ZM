package com.rfcode.ranger.extensions;

import com.rfcode.ranger.RangerExtension;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.ConfigUpdateListener;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.TagLifecycleListener;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * IBM Premises V7.x event export extension
 *
 * This extension, when active, publishes tag and reader status events to
 * the Premises V7.x HTTP interface (http://hostname:port/ibmse/gateway/eventpublish).
 *
 * Events are mapped into Premises events as follows:
 *    event-type: report/asset/observation/tagfound - tag found/created event
 *    event-type: report/asset/observation/tagupdate - tag updated event
 *    event-type: report/asset/observation/taglost - tag deleted/lost event
 *    event-type: report/diagnostic/reader/create - reader added event
 *    event-type: report/diagnostic/reader/update - reader updated event
 *    event-type: report/diagnostic/reader/delete - reader deleted event
 *    event-type: report/asset/observation/location/zoneentry - tag entered zone
 *    event-type: report/asset/observation/location/zoneexit - tag exited zone
 * All events include all the object's attributes (versus just changes) mapped
 * as metadata for the Premises event.
 * In order to prevent filling memory during a connectivity loss/error, each
 * object (tag-id, reader-id) will only have one pending "update" event queued
 * at a time - if a second occurs, it will replace the previously queued event.
 *
 * Configuration of the extension is via the 'configset' command.  The settings
 * include:
 *   premises_host - hostname of premises server (required - disabled if not set)
 *   premises_port - port number of premises server (default = 9080)
 *   premises_source - source ID for premises events (default - RFCodeZoneMgr)
 *   premises_enabled - enable/disable (true=enabled, other=disabled) (default=enabled)
 *   wse_zone_for_source : use zone for source_id on sensor-event (if true), otherwise use ZM source_id (same as sensor-events)
 */
public class PremisesV7Extension implements RangerExtension,
    ConfigUpdateListener {
    public static final String PREMISES7_HOSTNAME = PremisesV61Extension.PREMISES_HOSTNAME;
    public static final String PREMISES7_HOSTNAME_DEFAULT = "";
    public static final String PREMISES7_PORT = PremisesV61Extension.PREMISES_PORT;
    public static final String PREMISES7_PORT_DEFAULT = PremisesV61Extension.PREMISES_PORT_DEFAULT;
    public static final String PREMISES7_SOURCE = PremisesV61Extension.PREMISES_SOURCE;
    public static final String PREMISES7_SOURCE_DEFAULT = PremisesV61Extension.PREMISES_SOURCE_DEFAULT;
    public static final String PREMISES7_ENABLED = PremisesV61Extension.PREMISES_ENABLED;
    public static final String PREMISES7_ENABLED_DEFAULT = PremisesV61Extension.PREMISES_ENABLED_DEFAULT;
	public static final String PREMISES7_API_VERSION = PremisesV61Extension.PREMISES_API_VERSION;
	public static final String PREMISES7_API_VERSION_DEFAULT = PremisesV61Extension.PREMISES_API_VERSION_DEFAULT;	
    public static final String PREMISES7_LOGFILE = "premises_logfile";
    public static final String WSE_ZONE_FOR_SOURCE = "wse_zone_for_source";
    public static final String WSE_TAG_NOTIFICATION = "wse_tag_notification";
    public static final String WSE_MAX_LOCATION_INTERVAL = "wse_max_loc_interval";

    public static final int MAX_RETRIES = 100;

    public static final int RETRY_DELAY = 5000; /* 5 sec */
    public static final int CONNECT_TIMEOUT = 30000; /* 30 seconds */
    public static final int READ_TIMEOUT = 10000; /* 10 seconds */

    public static final String IBMSE_EVENT_URL = "/ibmse/gateway/eventpublish";

    public static final String TAGEVENTBASE = "report/asset/observation/";
    public static final String TAGEVENTNOTICEBASE = "report/asset/observation/notification/";
    public static final String RDREVENTBASE = "report/diagnostic/reader/";
    public static final String RDREVENTCREATE = "create";
    public static final String TAGEVENTFOUND = "tagfound";
    public static final String TAGEVENTUPDATE = "tagupdate";
    public static final String RDREVENTUPDATE = "update";
    public static final String RDREVENTDELETE = "delete";
    public static final String TAGEVENTLOST = "taglost";
    public static final String TAGEVENTLOCATION = "location";
	public static final String ZONEENTEREVENT = "report/asset/observation/location/zoneentry";
	public static final String ZONEEXITEVENT = "report/asset/observation/location/zoneexit";
	public static final String LOCATIONEVENT = "report/asset/observation/location";
	public static final String DIAGALERTEVENT = "report/diagnostic/alert";
    private URL url;
    private String source;
    private Listener lsnr;
	private String logfile;
    private boolean zone_for_sourceid;
    private boolean tag_notice;
    private long    max_loc_interval;
    private LinkedHashMap<String, Long> locupdated = new LinkedHashMap<String, Long>();
    /* Our processing thread */
    private ServiceThread our_thread;
    /* Our event queue */
    private LinkedBlockingQueue<Premises7Event> evt_queue = new LinkedBlockingQueue<Premises7Event>();
    /* Table of pending updates, keyed by object ID, containing message to send */
    private HashMap<String, String> upd_table = new HashMap<String, String>();
    /**
     * Handler thread - does all the work
     */
    private class ServiceThread extends Thread {
        public ServiceThread() {
            super("Premises7Extension Service");
        }
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }

    private class CheckForLocationUpdates implements Runnable {
        public void run() {
            long now = System.currentTimeMillis();
            Set<String> todo = new HashSet<String>();
            for(Map.Entry<String, Long> v : locupdated.entrySet()) {
                if(v.getValue() > now) {    /* Not overdue? */
                    break;      /* Break out - done reporting */
                }
                todo.add(v.getKey());
            }
            for(String tid : todo) {
                Tag t = TagGroup.findTagInAnyGroup(tid);
                if(t != null) {
                    encodeTagEvent(t, TAGEVENTLOCATION, null);
                }
                else {
                    locupdated.remove(tid);
                }
            }
            if(max_loc_interval > 0)
                RangerProcessor.addDelayedAction(this, 5000L);
            else
                locupdjob = null;
        }
    }
    private CheckForLocationUpdates locupdjob;

    /**
     * Premises event to be sent
     */
    public class Premises7Event {
        String type;
        String xml; /* XML encoded message to be sent (for non-update events, null for updates) */
        String id;  /* Object ID (non-null for updates) - update found in table */
    }
    /**
     * Extension init
     */
    public void extensionInit() {
        /* Register config update hook */
        RangerServer.addConfigUpdateListener(this);

        updateConfig();
    }
    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
    public void configUpdateNotification() {
        updateConfig();
    }
    /**
     * Update config, and return if active
     */
    private boolean updateConfig() {
        boolean is_enab = false;
        Properties cfg = RangerServer.getConfig();
        String enab = cfg.getProperty(PREMISES7_ENABLED, PREMISES7_ENABLED_DEFAULT);
        String host = cfg.getProperty(PREMISES7_HOSTNAME, PREMISES7_HOSTNAME_DEFAULT);
        String port = cfg.getProperty(PREMISES7_PORT, PREMISES7_PORT_DEFAULT);
        URL newurl = null;
		String logf = cfg.getProperty(PREMISES7_LOGFILE, "");
		String ver = cfg.getProperty(PREMISES7_API_VERSION, PREMISES7_API_VERSION_DEFAULT);
        /* Update source ID */
        source = cfg.getProperty(PREMISES7_SOURCE, PREMISES7_SOURCE_DEFAULT);
        String use_srcid = cfg.getProperty(WSE_ZONE_FOR_SOURCE, "false");
        zone_for_sourceid = use_srcid.equals("true");
        String use_notice = cfg.getProperty(WSE_TAG_NOTIFICATION, "false");
        tag_notice = use_notice.equals("true");
        String loc_upd_int = cfg.getProperty(WSE_MAX_LOCATION_INTERVAL);
        max_loc_interval = 0;
        if(loc_upd_int != null) {
            try {
                max_loc_interval = Long.valueOf(loc_upd_int);
            } catch (NumberFormatException nfx) {
            }
        }
        /* If we're doing periodic location update, start periodic job to handle it */
        if(max_loc_interval > 0) {
            if(locupdjob == null) {
                locupdjob = new CheckForLocationUpdates();
                RangerProcessor.addDelayedAction(locupdjob, 5000L);
            }
        }

		/* If enabled AND host set AND version set to 7.x */
        if(enab.equals("true") && (host.length() > 0) && (ver.charAt(0) == '7')) {
            int pnum = -1;
            try {
                pnum = Integer.parseInt(port);
            } catch (NumberFormatException nfx) {
                pnum = 9080;
            }
            try {
                newurl = new URL("http", host, pnum, IBMSE_EVENT_URL);             
                is_enab = true;
            } catch (MalformedURLException mux) {
                is_enab = false;
            }
        }
		logfile = logf;
        if(is_enab == true) {   /* If active, start up */
            url = newurl;
            if(our_thread == null) {    /* Not active? */
                /* Start it up */
                our_thread = new ServiceThread();
                our_thread.setDaemon(true);
                our_thread.start();
            }
            if(lsnr == null) {
                lsnr = new Listener();
                /* Start listening to tag updates */
                AbstractTagType.addGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.addReaderStatusListener(lsnr);
                RangerServer.addReaderLifecycleListener(lsnr);
            }
        }
        else {  /* Else, if not active */
            if(lsnr != null) {
                /* Stop listening to tag updates */
                AbstractTagType.removeGlobalTagLifecycleListener(lsnr);
                /* And reader updates */
                RangerServer.removeReaderStatusListener(lsnr);
                RangerServer.removeReaderLifecycleListener(lsnr);
                lsnr = null;
            }
            if(our_thread != null) {
                our_thread.interrupt();
            }
            synchronized(upd_table) {
                evt_queue.clear();  /* Toss all pending events */
                upd_table.clear();
            }
        }

        return is_enab;
    }

    /**
     * Processing method - called by our service thread
     */
    private void process() {
        Premises7Event pushed_evt = null;
        int retry_cnt = 0;
        try {
            while(true) {
                Premises7Event evt;
                if(pushed_evt != null) {
                    evt = pushed_evt; pushed_evt = null;
                }
                else {
                    evt = evt_queue.poll(15, TimeUnit.SECONDS);
                }
                if(evt == null) continue;
                /* If ID defined, get message from update table */
                if(evt.id != null) {
                    synchronized(upd_table) {
                        evt.xml = upd_table.remove(evt.id);
                        evt.id = null;
                    }
                }
                /* Build and issue request */
                try {
            		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	    	        connection.setRequestMethod("POST");
            		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	    	        connection.setUseCaches(false);
	    	        connection.setDoInput(true);
            		connection.setDoOutput(true);
                    connection.setConnectTimeout(CONNECT_TIMEOUT);
                    connection.setReadTimeout(READ_TIMEOUT);
	    	        connection.connect();
		
            		StringBuffer data = new StringBuffer();		
                	data.append("eventxml=" + URLEncoder.encode(evt.xml, "UTF-8"));
            		DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
            	    dos.writeBytes(data.toString());
            		dos.flush();
            		dos.close();
            		InputStream is = connection.getInputStream();
            		is.close();
                    /* Check response code */
                    if (connection.getResponseCode() == 200) {
                        /* Message sent */
                        //RangerServer.info("Posted " + evt.type + " to IBM Premises Server");
                        retry_cnt = 0;
                    }
                    else {  /* Else, push it back on the head of the queue */
                        pushed_evt = evt;
                        RangerServer.error("POST to Premises7 failed: URL=" + url + ", response=" + 
                            connection.getResponseCode() + "(" + connection.getResponseMessage() + ")");
                        Thread.sleep(RETRY_DELAY);
                        retry_cnt++;
                    }
                } catch (IOException iox) {
                    pushed_evt = evt;
                    RangerServer.error("POST to Premises7 failed: URL=" + url + ", response=" +
                        iox.toString());
                    Thread.sleep(RETRY_DELAY);
                    retry_cnt++;
                }
                if(retry_cnt > MAX_RETRIES) {
                    evt_queue.clear();  /* Clean out queue - starting fresh (avoid out of memory on long term errors) */
                    pushed_evt = null;
                    RangerServer.error("Retry limit exceeded - event queue flushed");
                    retry_cnt = 0;
                }
            }
        } catch (InterruptedException ix) {
        }
    }
    /**
     * Encode tag event
     */
    private void encodeTagEvent(Tag tag, String type, Object[] oldval) {
        Map<String,Object> attrs = new HashMap<String,Object>(tag.getTagAttributes());
        String id = tag.getTagGUID();
        String locpath = null;
		String evt = (tag_notice?TAGEVENTNOTICEBASE:TAGEVENTBASE)+type;
        attrs.put("tagtype", tag.getTagType().getID());
        attrs.put("tagid", id);
        attrs.put("taggroup", tag.getTagGroup().getID());
        /* Look up our location, if defined */
        Object loc = attrs.get(LocationRuleFactory.ATTRIB_ZONELOCATION);
        if ((loc != null) && (loc instanceof String)) {
            Location our_loc = ReaderEntityDirectory.findLocation((String)loc);
			/* Build attribs for enter/exit events */
			Map<String,Object> oldattrs = new HashMap<String,Object>();
	        oldattrs.put("tagtype", tag.getTagType().getID());
			oldattrs.put("tagid", id);
	        oldattrs.put("taggroup", tag.getTagGroup().getID());

            /* If found, add our inherited attributes too */
            if(our_loc != null) {
                Map<String, Object> vals = our_loc.getFullAttributes();
                if(vals != null) {
                    for(Map.Entry<String,Object> mape : vals.entrySet()) {
                        attrs.put("location." + mape.getKey(), mape.getValue());
                    }
                }
                locpath = "/" + our_loc.getPath();
            }
			else {
				locpath = "/";
			}
            /* If period is non-zero, send location event */
            if(max_loc_interval > 0) {
				/* Send a location event */
				oldattrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, loc);
				encodeEvent(LOCATIONEVENT, id, oldattrs, false, locpath);
                /* Mark as one which has been updated */
                locupdated.remove(id);
                if(!type.equals(TAGEVENTLOST))
                    locupdated.put(id, System.currentTimeMillis() + max_loc_interval*1000L);
            }
			/* Find old location */
			else if(oldval != null) {
				int locidx = tag.getTagType().getTagAttributeIndex(LocationRuleFactory.ATTRIB_ZONELOCATION);
				Object oldloc = null;
				if(locidx >= 0) oldloc = oldval[locidx];
				/* If we had old location, and its different, we're going to do exit/entry events */
				if(loc != oldloc) {
					Location our_old_loc = null;
					String old_locpath = "/";
					if(oldval[locidx] != null) 
						our_old_loc = ReaderEntityDirectory.findLocation((String)oldval[locidx]);
					if(our_old_loc != null) {
						old_locpath = "/" + our_old_loc.getPath();
					}
					oldattrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, oldval[locidx]);
					encodeEvent(ZONEEXITEVENT, id, oldattrs, false, old_locpath);
					/* And send enter event */
					oldattrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, loc);
					encodeEvent(ZONEENTEREVENT, id, oldattrs, false, locpath);
				}
			}
			else {	/* Else, just enter event (since no old value */
				/* And send enter event */
				oldattrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, loc);
				encodeEvent(ZONEENTEREVENT, id, oldattrs, false, locpath);
			}
        }
        if(!type.equals(TAGEVENTLOCATION)) {
            encodeEvent(evt, id, attrs, type.equals(TAGEVENTUPDATE), locpath);
            Object lowbat = attrs.get(MantisIITagType.LOWBATT_ATTRIB);
            if(tag_notice && (lowbat != null) && ((Boolean)lowbat).booleanValue()) {
                encodeEvent(DIAGALERTEVENT, id, attrs, false, locpath);
            }
        }
    }
    /**
     * Encode reader event
     */
    private void encodeReaderEvent(Reader rdr, String type) {
        Map<String,Object> attrs = new HashMap<String,Object>(rdr.getReaderAttributes());
        String id = rdr.getID();
        attrs.put("readertype", rdr.getReaderFactory().getID());
        attrs.put("readerid", id);
        attrs.put("enabled", rdr.getReaderEnabled());
        attrs.put("state", rdr.getReaderStatus());
        TagGroup[] grps = rdr.getReaderTagGroups();
        Set<String> g = new TreeSet<String>();
        for(TagGroup grp : grps) {
            g.add(grp.getID());
        }
        attrs.put("groups", g);
        attrs.remove("password");   /* Remove password attribute, if defined */

        encodeEvent(RDREVENTBASE+type, id, attrs, type.equals(RDREVENTUPDATE), null);
    }

    private static SimpleDateFormat ISO860Local = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    private static String doISO860(Date d) {
        String v = ISO860Local.format(d);
        /* Need to add ':' in Z portion of string (second from last) */
        return v.substring(0, v.length()-2) + ':' + v.substring(v.length()-2);
    }

    /**
     * Encode event
     */
    private void encodeEvent(String eventType, String id, Map<String,Object> attrs,
        boolean update, String locpath) {
		StringBuilder sb = new StringBuilder();
		/* Add base header */
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		sb.append("<wse:sensor_events xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ")
			.append("xmlns:wse=\"http://www.ibm.com/xmlns/prod/websphere/sensorevents/wse70\" wse:version=\"7.0\" ")
			.append("xsi:schemaLocation=\"http://www.ibm.com/xmlns/prod/websphere/sensorevents/wse70 WSE_7_0.xsd\">\n");
		/* Add event unique ID */
		UUID evtid = UUID.randomUUID();
		sb.append(" <wse:event_id wse:type=\"String\">").append(evtid.toString()).append("</wse:event_id>\n");
		/* Add event timestamp */
		Date now = new Date();
		sb.append(" <wse:event_time wse:type=\"DateTime\">").append(doISO860(now)).append("</wse:event_time>\n");
		/* Add event type */
		sb.append(" <wse:event_type wse:type=\"String\">").append(eventType).append("</wse:event_type>\n");
		/* Add priority */
		sb.append(" <wse:priority wse:type=\"Integer\">1</wse:priority>\n");
		/* Add source ID */
		sb.append(" <wse:source_id wse:type=\"String\">").append(source).append("</wse:source_id>\n");
		/* Start sensor event */
		sb.append(" <wse:sensor_event>\n");
		/* Start condition section */
		sb.append("  <wse:condition>\n");
		/* Loop through attributes, handling special ones properly */
		for(Map.Entry<String,Object> mape : attrs.entrySet()) {
			String k = mape.getKey();
			Object v = mape.getValue();
			if(v == null)	/* Skip null values */
				continue;
			/* If lowbattery, handle with battery_status tag */
			if(k.equals("lowbattery")) {
				sb.append("   <wse:battery_status wse:type=\"String\">");
				if(((Boolean)v).booleanValue() == true)
					sb.append("0");
				else
					sb.append("5");
				sb.append("</wse:battery_status>\n");
			}
			/* If panic, handle with buttons_pressed */
			else if(k.equals("panic")) {
				if(((Boolean)v).booleanValue() == true) {
    				sb.append("   <wse:buttons_pressed wse:type=\"String\">Panic</wse:buttons_pressed>\n");
                }
			}
            /* Handle other generic buttons */
			else if(k.equals("button0")) {
				if(((Boolean)v).booleanValue() == true) {
    				sb.append("   <wse:buttons_pressed wse:type=\"String\">Button-0</wse:buttons_pressed>\n");
                }
			}
			else if(k.equals("button1")) {
				if(((Boolean)v).booleanValue() == true) {
    				sb.append("   <wse:buttons_pressed wse:type=\"String\">Button-1</wse:buttons_pressed>\n");
                }
			}
			else if(k.equals("button2")) {
				if(((Boolean)v).booleanValue() == true) {
    				sb.append("   <wse:buttons_pressed wse:type=\"String\">Button-2</wse:buttons_pressed>\n");
                }
			}
			else if(k.equals("tamper")) {
				if(((Boolean)v).booleanValue() == true) {
    				sb.append("   <wse:buttons_pressed wse:type=\"String\">Tamper</wse:buttons_pressed>\n");
                }
			}
			/* If temp, handle with temperature */
			else if(k.equals("temp")) {
				sb.append("   <wse:temperature wse:type=\"Float\">");
				sb.append(v.toString());
				sb.append("</wse:temperature>\n");
				sb.append("   <wse:temperature_units wse:type=\"String\">Celsius</wse:temperature_units>\n");
			}
			/* If humidity, handle with humidity */
			else if(k.equals("humidity")) {
				sb.append("   <wse:humidity wse:type=\"Float\">");
				sb.append(v.toString());
				sb.append("</wse:humidity>\n");
			}
			/* If boolean of some sort, pass in generic field */
			else if(v instanceof Boolean) {
				sb.append("   <wse:field wse:namespace=\"http://www.rfcode.com/xmlns/wse70\" wse:prefix=\"rfc\" wse:name=\"")
					.append(k).append("\" wse:type=\"Boolean\">").append(v.toString()).append("</wse:field>\n");
			}
			/* If integer of some sort, pass in generic field */
			else if(v instanceof Integer) {
				sb.append("   <wse:field wse:namespace=\"http://www.rfcode.com/xmlns/wse70\" wse:prefix=\"rfc\" wse:name=\"")
					.append(k).append("\" wse:type=\"Integer\">").append(v.toString()).append("</wse:field>\n");
			}
			/* If float of some sort, pass in generic field */
			else if(v instanceof Double) {
				sb.append("   <wse:field wse:namespace=\"http://www.rfcode.com/xmlns/wse70\" wse:prefix=\"rfc\" wse:name=\"")
					.append(k).append("\" wse:type=\"Real\">").append(v.toString()).append("</wse:field>\n");
			}
			/* If string of some sort, pass in generic field */
			else if(v instanceof String) {
				sb.append("   <wse:field wse:namespace=\"http://www.rfcode.com/xmlns/wse70\" wse:prefix=\"rfc\" wse:name=\"")
					.append(k).append("\" wse:type=\"String\">").append(v.toString()).append("</wse:field>\n");
			}
		}
		/* Close condition */
		sb.append("  </wse:condition>\n");		
		/* Add ID for specific event */
		UUID snrevtid = UUID.randomUUID();
		sb.append("  <wse:event_id wse:type=\"String\">").append(snrevtid.toString()).append("</wse:event_id>\n");
		/* Add event timestamp */
		sb.append("  <wse:event_time wse:type=\"DateTime\">").append(doISO860(now)).append("</wse:event_time>\n");
		/* Add type */
		sb.append("  <wse:event_type wse:type=\"String\">").append(eventType).append("</wse:event_type>\n");
		/* Add location if provided */
		if(locpath != null) {
			sb.append("  <wse:location>\n");
			sb.append("   <wse:zone wse:type=\"String\">").append(locpath).append("</wse:zone>\n");
            sb.append("   <wse:x wse:type=\"Real\">0.0</wse:x><wse:y wse:type=\"Real\">0.0</wse:y><wse:z wse:type=\"Real\">0.0</wse:z>\n");
			sb.append("   <wse:dimensionality wse:type=\"Integer\">3</wse:dimensionality>\n");
            if(locpath.equals("/"))
    			sb.append("   <wse:reliability wse:type=\"Integer\">0</wse:reliability>\n");
            else
    			sb.append("   <wse:reliability wse:type=\"Integer\">2</wse:reliability>\n");
			sb.append("  </wse:location>\n");

		}
		/* Add source ID */
        if(zone_for_sourceid)
    		sb.append("  <wse:source_id wse:type=\"String\">").append((locpath!=null)?locpath:"null").append("</wse:source_id>\n");
        else
    		sb.append("  <wse:source_id wse:type=\"String\">").append(source).append("</wse:source_id>\n");
		/* Add subject */
		sb.append("  <wse:subject>\n");
		if(attrs.get("taggroup") != null) {	/* If tag group defined, add as group ID */
			sb.append("   <wse:group wse:type=\"String\">").append(attrs.get("taggroup").toString()).append("</wse:group>\n");
		}
		sb.append("   <wse:id wse:type=\"String\">").append(id).append("</wse:id>\n");
		sb.append("  </wse:subject>\n");
		/* End sensor event */
		sb.append(" </wse:sensor_event>\n");
		/* End sensor events */
		sb.append("</wse:sensor_events>\n");

		/* Make string */
		String xml = sb.toString();

		if(logfile != null) {
			FileWriter fw = null;
			try {
				fw = new FileWriter(logfile, true);
				fw.write(xml);
			} catch (IOException iox) {
			} finally {
				if(fw != null) try { fw.close(); } catch (IOException iox2) {}
			}
		}

        /* If update, see if pending update */
        Premises7Event e = new Premises7Event();
        e.type = eventType;
        if(update) {
            e.id = id;  /* Save ID in event, XML in table */
            synchronized(upd_table) {
                if(upd_table.containsKey(id)) { /* If pending */
                    e = null;           /* Don't add again */
                }
                upd_table.put(id, xml); /* Put message in table */
            }
        }
        else {  /* Else, message in event */
            e.xml = xml;
        }
        /* Add new event to end of queue, if needed */
        if(e != null) {
	        evt_queue.offer(e);
        }
    }

    /** Our listener object - internalized */
    private class Listener implements ReaderStatusListener, ReaderLifecycleListener,
            TagStatusListener, TagLifecycleListener {
        /**
         * Reader creation notification - called when new reader object creation
         * completed (at the end of its init() call).
         * 
         * @param reader -
         *            reader object created
         */
        public void readerLifecycleCreate(Reader reader) {
            encodeReaderEvent(reader, RDREVENTCREATE);
        }
        /**
         * Reader deletion notification - called at start of reader's cleanup()
         * method, when reader is about to be deleted.
         * 
         * @param reader -
         *            reader object being deleted
         */
        public void readerLifecycleDelete(Reader reader) {
            encodeReaderEvent(reader, RDREVENTDELETE);
        }
        /**
         * Reader enable change notification - called when reader has just changed
         * its enabled state (after the setReaderEnabled() method has been called).
         * 
         * @param reader -
         *            reader object being enabled or disabled
         * @param enable -
         *            true if now enabled, false if now disabled
         */
        public void readerLifecycleEnable(Reader reader, boolean enable) {
            encodeReaderEvent(reader, RDREVENTUPDATE);
        }
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         * 
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            encodeReaderEvent(reader, RDREVENTUPDATE);
        }
        /**
         * Tag created notification. Called after tag has been created, but before
         * its TagLinks have been added (i.e. after end of init()). Any initial
         * attributes from creating message will have been added by the time this is
         * called.
         * 
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            encodeTagEvent(tag, TAGEVENTFOUND, null);
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag is
         * about to be deleted (generally due to having no active TagLinks).
         * 
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
            encodeTagEvent(tag, TAGEVENTLOST, null);
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than the
         *            number of attributes for the given tag). Ordered to match
         *            attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            encodeTagEvent(tag, TAGEVENTUPDATE, oldval);
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        /**
         * TagLink added notificiation. Called after taglink has been added to tag.
         * 
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed from
         * tag.
         * 
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
        }
    }
}
