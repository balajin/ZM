package com.rfcode.ranger.extensions;

import com.rfcode.ranger.RangerExtension;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.ConfigUpdateListener;
import com.rfcode.ranger.FirmwareFileUploadServlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Properties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.HttpConnection;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HeaderElement;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.ChunkedInputStream;
import org.apache.commons.httpclient.ChunkedOutputStream;
import org.apache.commons.httpclient.util.ParameterParser;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.httpclient.util.EncodingUtil;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.contrib.ssl.StrictSSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import javax.servlet.ServletException;
/**
 */
public class UpConnectExtension implements RangerExtension,
    ConfigUpdateListener {
    public static final String UPCONNECT_HOSTNAME = "upconnect_hostname";
    public static final String UPCONNECT_HOSTNAME_DEFAULT = "";
    public static final String UPCONNECT_PORT = "upconnect_port";
    public static final String UPCONNECT_PORT_DEFAULT = "6580";
    public static final String UPCONNECT_ZMID = "upconnect_zmid";
    public static final String UPCONNECT_ZMID_DEFAULT = "RFCodeZoneMgr";
    public static final String UPCONNECT_USERID = "upconnect_userid";
    public static final String UPCONNECT_USERID_DEFAULT = "";
    public static final String UPCONNECT_PASSWORD = "upconnect_password";
    public static final String UPCONNECT_PASSWORD_DEFAULT = "";
    public static final String UPCONNECT_ENABLED = "upconnect_enabled";
    public static final String UPCONNECT_ENABLED_DEFAULT = "true";
    public static final String UPCONNECT_URL_BASE = "upconnect_baseurl";
    public static final String UPCONNECT_URL_BASE_DEFAULT = "/upconnect";
    public static final String UPCONNECT_PROTOCOL = "upconnect_protocol";
    public static final String UPCONNECT_PROTOCOL_HTTP = "HTTP";
    public static final String UPCONNECT_PROTOCOL_HTTPS = "HTTPS";
    public static final String UPCONNECT_PROTOCOL_HTTPS_VERIFY = "HTTPS_VERIFY";
    public static final String UPCONNECT_PROTOCOL_DEFAULT = UPCONNECT_PROTOCOL_HTTP;

    public static final String LOCALHOST_PORT = "localhost_port";
    public static final String LOCALHOST_PORT_DEFAULT = "6580";

    public static final int RETRY_DELAY = 5000; /* 5 sec */
    public static final int CONNECT_TIMEOUT = 60000; /* 60 seconds */
    public static final int READ_TIMEOUT = 60000; /* 60 seconds */

    private static final String LOCAL_SERVERHOST = "localhost";

    private URL url;
    private String zmid;
    private String userid;
    private String passwd;
    private String hostname;
    private String protocol;
    private int portnum;
    private int localhost_portnum;
    private boolean changed;
    private boolean enabled;
    /* Our processing thread */
    private ServiceThread our_thread;
    /**
     * Handler thread - does all the work
     */
    private class ServiceThread extends Thread {
        public ServiceThread() {
            super("UpConnectExtension Service");
        }
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }
    /**
     * Extension init
     */
    public void extensionInit() {
        /* Register config update hook */
        RangerServer.addConfigUpdateListener(this);
        updateConfig();
    }
    
    /**
     * Pair "https" protocol with simple or strict (hostname verification)
     * ssl protocol socket factory per ssl setting by user; called by updateConfig()
     */
    public void registerHTTPS(String pt) {
    	ProtocolSocketFactory sf;
    	if (pt.equals(protocol)) {
    		//do nothing - protocol is not changed between configuration updates
    		return;
    	}
    	else {
    		if (pt.equals("stricthttps")) {
        		sf = new StrictSSLProtocolSocketFactory();
    	    }
    	    else { /* pt is "https" */
    	    	sf = new EasySSLProtocolSocketFactory();
    	    }
    		if (Protocol.getProtocol("https") != null ) {
    			Protocol.unregisterProtocol("https");
    		}
	        Protocol p = new Protocol("https", sf, 443);
	        Protocol.registerProtocol("https", p);
        }
    }

    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
    public void configUpdateNotification() {
        updateConfig();
    }
    /**
     * Update config, and return if active
     */
    private boolean updateConfig() {
        boolean is_enab = false;
        Properties cfg = RangerServer.getConfig();
        String enab = cfg.getProperty(UPCONNECT_ENABLED, UPCONNECT_ENABLED_DEFAULT);
        String host = cfg.getProperty(UPCONNECT_HOSTNAME, UPCONNECT_HOSTNAME_DEFAULT);
        String port = cfg.getProperty(UPCONNECT_PORT, UPCONNECT_PORT_DEFAULT);
        String loc_port = cfg.getProperty(LOCALHOST_PORT, LOCALHOST_PORT_DEFAULT);
        String base = cfg.getProperty(UPCONNECT_URL_BASE, UPCONNECT_URL_BASE_DEFAULT);
        String proto = cfg.getProperty(UPCONNECT_PROTOCOL, UPCONNECT_PROTOCOL_DEFAULT);
        URL newurl = null;
        String newuid = cfg.getProperty(UPCONNECT_USERID, UPCONNECT_USERID_DEFAULT);
        String newpwd = cfg.getProperty(UPCONNECT_PASSWORD, UPCONNECT_PASSWORD_DEFAULT);

        /* Update ZMID */
        zmid = cfg.getProperty(UPCONNECT_ZMID, UPCONNECT_ZMID_DEFAULT);

        int pnum = 6580;
        int loc_pnum = 6580;
        /* Check protocol */
        if(proto.equals(UPCONNECT_PROTOCOL_HTTP)) {
            proto = "http";
        }
        else if(proto.equals(UPCONNECT_PROTOCOL_HTTPS)) {
            proto = "https";
        }
        else if(proto.equals(UPCONNECT_PROTOCOL_HTTPS_VERIFY)) {
            proto = "stricthttps";
        }
        else {
            RangerServer.error("Bad value for " + UPCONNECT_PROTOCOL + "(" +
                proto + ") - using HTTP");
            proto = "http";
        }
        if(enab.equals("true") && (host.length() > 0)) {
            try {
                pnum = Integer.parseInt(port);
            } catch (NumberFormatException nfx) {
            }
            try {
                loc_pnum = Integer.parseInt(loc_port);
            } catch (NumberFormatException nfx) {
            }
            /* Build new URL : proto://host:port/base/zmid */
            //newurl = proto + "://" + host + ":" + pnum;
            String fname = "";
            if(base.charAt(0) != '/')
                fname += '/';
            fname += base;
            if(fname.charAt(fname.length()-1) != '/')
                fname += '/';
            fname += zmid;

            if (!proto.equals("http")) {
            	/* register https with proper ssl protocol socket */
            	registerHTTPS(proto);
            	try {
            		newurl = new URL("https", host, pnum, fname);
            	    is_enab = enabled = true;
            	} catch (MalformedURLException mux) {
            		RangerServer.error("Malformed upconnect URL : " + mux);
                    is_enab = enabled = false;
            	}

            } else {
                try {
                    newurl = new URL(proto, host, pnum, fname);
                    is_enab = enabled = true;
                } catch (MalformedURLException mux) {
                    RangerServer.error("Malformed upconnect URL : host=" + host + ", ID=" + zmid);
                    is_enab = enabled = false;            
                }
            }
        }
        else {
            enabled = false;
        }
        if(is_enab == true) {   /* If active, start up */
            if((!newurl.equals(url)) || (!newuid.equals(userid)) ||
                (!newpwd.equals(passwd)) || (!proto.equals(protocol))) {
                url = newurl;
                userid = newuid;
                passwd = newpwd;
                hostname = host;
                portnum = pnum;
                localhost_portnum = loc_pnum;
                protocol = proto;
                changed = true;
            }
            if(our_thread == null) {    /* Not active? */
                /* Start it up */
                our_thread = new ServiceThread();
                our_thread.setDaemon(true);
                our_thread.start();
            }
        }
        else {  /* Else, if not active */
            if(our_thread != null) {
                changed = true;
                our_thread.interrupt();
            }
        }
        return is_enab;
    }

    /** 
     * Dummy request entity
     */
    private static class DummyEntity implements RequestEntity {
        public String getContentType() { return null; }
        public long getContentLength() { return -1; }
        public boolean isRepeatable() { return false; }
        public void writeRequest(OutputStream out) throws IOException {
            throw new IOException("writeRequest not supported");
        }
    }

    /**
     * Wrapper for chunked output stream
     */
    private static class OurChunkedOutputStream extends ChunkedOutputStream {
        public OurChunkedOutputStream(OutputStream os) throws IOException {
            super(os);
        }
        public void flushChunk() throws IOException {
            flushCache();
            flush();
        }
    }   
    /**
     * Custom method for chunked bidirection stream
     */
    private static class CustomPostMethod extends EntityEnclosingMethod {
        private OurChunkedOutputStream out;
        private ChunkedInputStream in;
        /**
         * Custom POST method for bidirectional chunked stream
         */
        public CustomPostMethod(String url) {
            super(url);
            setRequestEntity(new DummyEntity());
        }
        /**
         * Get name of method (POST)
         */
        public String getName() {
            return "POST";
        }

        /**
         * Write command string to stream and flush
         */
        public void writeCommand(String s) throws IOException {
            s += "\n";  /* End with newline */
            out.write(EncodingUtil.getAsciiBytes(s));
            out.flushChunk();
        }
        /**
         * Read response string from stream
         */
        public String readResponse() throws IOException {
            StringBuilder sb = new StringBuilder();
            while(true) {
                int c = in.read();  /* Get next character */
                if(c == -1) {   /* End read */
                    return null;    /* End of stream */
                }
                if(c == '\n')   /* End of line? */
                    break;
                sb.append((char)c);
            }
            return sb.toString();
        }
        /**
         * Read request from channel
         */
        public String readRequest() throws IOException {
            return readResponse();      /* Same format, for now */
        }
        /**
         * Write reply to channel
         */
        public void sendReply(int httprc, String contenttype, String rep) throws IOException {
            writeCommand(Integer.toString(httprc) + "," + contenttype + ":" + 
                URIUtil.encodeWithinQuery(rep));
        }
        /**
         * Write reply stream to channel
         */
        public void sendReply(int httprc, String contenttype, InputStream rep) throws IOException {
            String s = Integer.toString(httprc) + "," + contenttype + ":";
            out.write(EncodingUtil.getAsciiBytes(s));    /* Write start */
            byte[] buf = new byte[512]; /* Allocate buffer */
            int len;
            do {
                len = rep.read(buf, 0, buf.length);     /* Read bytes */
                if(len > 0) {
                    String v = URIUtil.encodeWithinQuery(EncodingUtil.getAsciiString(buf, 0, len));
                    out.write(EncodingUtil.getAsciiBytes(v));
                }
            } while(len > 0);
            out.write('\n');
            out.flushChunk();
        }
        /**
         * Write method for request body - get output stream
         */
        protected boolean writeRequestBody(HttpState state, HttpConnection conn)
            throws IOException, HttpException {
            /* Make chunked output stream for our request */
            out = new OurChunkedOutputStream(conn.getRequestOutputStream());
            /* Send version ID command as handshake (currently version 1) */
            writeCommand("_VERSION?version=1");
            return true;
        }
        /**
         * Read method for response body - get the input stream
         */
        protected void readResponseBody(HttpState state, HttpConnection conn) 
            throws IOException, HttpException {
            /* Build input stream handler */
            in = new ChunkedInputStream(conn.getResponseInputStream());
            /* Start by reading response to our _VERSION command */
            String rsp = readResponse();
            /* TODO: parse result, save and/or log parameters */
            if(!rsp.equals("_VERSION?version=1")) { /* Not good? */
                throw new IOException("Bad Version: " + rsp);
            }
        }
    }

    /**
     * Processing method - called by our service thread
     */
    private void process() {
        HttpClient client = new HttpClient();
        HttpClient localclient = null;
        CustomPostMethod pm = null;
        try {
            /* Set connection parameters */
            client.getHttpConnectionManager().getParams().setConnectionTimeout(CONNECT_TIMEOUT);
            client.getHttpConnectionManager().getParams().setSoTimeout(READ_TIMEOUT);

            while(enabled) {
                try {
                    boolean auth = false;

                    changed = false;    /* Clear changed flag */

                    if((userid.length() > 0) || (passwd.length() > 0)) {
                        client.getState().setCredentials(
                            new AuthScope(url.getHost(), url.getPort(), null),
                            new UsernamePasswordCredentials(userid,passwd));
                        client.getParams().setAuthenticationPreemptive(true);
                        auth = true;
                    }
                    pm = new CustomPostMethod(url.toString());   /* Make POST method to URL */
                    if(auth)
                        pm.setDoAuthentication(true);
                    int httpCode = client.executeMethod(pm);
                    if (httpCode != 200) {
                        RangerServer.error("Upconnect POST failed: URL=" + url + ", httpresponse=" +
                            httpCode);
                        Thread.sleep(RETRY_DELAY);
                        continue;
                    }
                    /* Now, create clean local client for requests */
                    localclient = new HttpClient();
                    /* Main loop - read reqests from channel, write responses back */
                    do {
                        String req = pm.readRequest();      /* Read next request */
                        if(req == null) {                   /* End of stream? */
                            break;                          /* Quit, we're done */
                        }
                        if(req.length() == 0) {             /* Empty string?  just keep-alive */
                            pm.sendReply(200,"","");        /* Send trivial reply */
                        }
                        /* Else, if not handled, return bad reply */
                        else if(!handleRequest(localclient, pm, req)) {
                            pm.sendReply(500,"","");        /* Send bad reply */
                        }
                    } while (!changed);
                } catch (IOException e) {
                    RangerServer.error("Upconnect POS failed: URL=" + url + ", response=" +
                        e.toString());
                    Thread.sleep(RETRY_DELAY);
                } finally {
                    if(pm != null) {
                        pm.releaseConnection();
                        pm = null;
                    }
                    /* If we drop connection, reset local client */
                    if(localclient != null) {
                        localclient = null;
                    }
                }
            }
        } catch (InterruptedException ix) {

        } finally {
            client = null;
        }
    }
    private ParameterParser parmparse = new ParameterParser();

	/** Fetch firmware.zip from upconnect server, and update local firmware library */
	private boolean updateFirmwareLib(CustomPostMethod pm) throws IOException {
		/* Make client for our request */
		HttpClient client = new HttpClient();
	    /* Otherwise,build request to local server */
        GetMethod cmdmeth = new GetMethod(url + "/firmware.zip");
		/* Use up-connect credentials */
        if((userid.length() > 0) || (passwd.length() > 0)) {
            client.getState().setCredentials(
	            new AuthScope(hostname, portnum, null),
	            new UsernamePasswordCredentials(userid,passwd));
            client.getParams().setAuthenticationPreemptive(true);
        }
        if(client.getParams().isAuthenticationPreemptive())
            cmdmeth.setDoAuthentication(true);
        /* Issue command */
        int httpCode = client.executeMethod(cmdmeth);
        if (httpCode != 200) {	/* Not success? */
	        pm.sendReply(200,"","<error>: request for firmware.zip failed");
			return true;
		}
        /* Send reply */
        Header ct = cmdmeth.getResponseHeader("Content-Type");
        String content = "";
        if(ct != null) {
            HeaderElement[] he = ct.getElements();
            if((he != null) && (he.length > 0)) {
                content = he[0].getName();
            }
        }
		if(!content.equals("application/zip")) {	/* Not a zip response? */
	        pm.sendReply(200,"","<error>: Incorrect content type for firmware.zip");
			return true;
		}
		/* Get input stream for response */
        InputStream resp = cmdmeth.getResponseBodyAsStream();
 		/* Use stream to receive new firmware.zip */
		try {
			String rslt = FirmwareFileUploadServlet.processNewLibrary(resp);
			if(rslt != null) {	/* If error */
		        pm.sendReply(200,"", "<error>: " + rslt);
		        return true;	
			}
		} catch (ServletException sx) {
			return false;
		}
        pm.sendReply(200,"","");        /* Send success reply */
		
		return true;
	}
    /**
     * Handle command request
     */
    private boolean handleRequest(HttpClient client, 
        CustomPostMethod pm, String req) throws IOException {
        String cmd;
        int idx = req.indexOf('?'); /* Find questionmark */
        String args;
        if(idx >= 0) {
            cmd = req.substring(0, idx);
            args = req.substring(idx+1);
        }
        else {
            cmd = req;
            args = "";
        }
        List<?> arglist = parmparse.parse(args,'&');  /* Parse arguments */
        /* If command is "_setauth", update local client credentials */
        if(cmd.equals("_setauth")) {
            String uid = "";
            String pwd = "";
            for(Object o : arglist) {
                NameValuePair nvp = (NameValuePair)o;
                if(nvp.getName().equals("userid"))
                    uid = nvp.getValue();
                else if(nvp.getName().equals("password"))
                    pwd = nvp.getValue();
            }
            if((uid.length() + pwd.length()) > 0) { /* If userid/password */
                client.getState().setCredentials(
                    new AuthScope(LOCAL_SERVERHOST, localhost_portnum, null),
                    new UsernamePasswordCredentials(uid,pwd));
                client.getParams().setAuthenticationPreemptive(true);
            }
            else {
                client.getParams().setAuthenticationPreemptive(false);
                client.getState().clearCredentials();
            }
            pm.sendReply(200,"","");        /* Send success reply */
            return true;
        }  
		/* If command is "_updatefwlib", fetch firmware library from upconnect server */
		else if(cmd.equals("_updatefwlib")) {
			return updateFirmwareLib(pm);
		}
        /* Otherwise,build request to local server */
        PostMethod cmdmeth = new PostMethod("http://" + LOCAL_SERVERHOST + ":" +
            localhost_portnum + "/rfcode_zonemgr/zonemgr/api/" + cmd);
        if(client.getParams().isAuthenticationPreemptive())
            cmdmeth.setDoAuthentication(true);
        for(Object o : arglist) {
            NameValuePair nvp = (NameValuePair)o;
            cmdmeth.addParameter(nvp);
        }
        /* Issue command */
        int httpCode = client.executeMethod(cmdmeth);
        /* Send reply */
        Header ct = cmdmeth.getResponseHeader("Content-Type");
        String content = "";
        if(ct != null) {
            HeaderElement[] he = ct.getElements();
            if((he != null) && (he.length > 0)) {
                content = he[0].getName();
            }
        }
        InputStream resp = cmdmeth.getResponseBodyAsStream();
        if(resp != null) {
            pm.sendReply(httpCode, content, resp);
            return true;
        }
        return false;
    }
}
