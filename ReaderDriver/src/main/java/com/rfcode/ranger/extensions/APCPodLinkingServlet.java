package com.rfcode.ranger.extensions;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.TagLifecycleListener;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.RangerProcessor;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * APC/NetBotz Pod Linking Servlet
 *
 * Provice BotzWare compatible URL for supporting pod linking to various
 * tags and readers defined under Zone Manager.  Each supported tag or reader
 * is reported as a link-able "pod", with appropriate sensors for the tag's sensors
 * and status.  
 *
 * NOTICE:
 * The code in this module implements the confidential 'BotzWare XML Grammar Specification' and 
 * 'BotzWare HTTP Interface Specification', as provided by APC Corp, for interface with their
 * NetBotz appliances.  Do not release this source code to 3rd parties that lack rights to 
 * access to these specifications.
 * 
 * @author Mike Primm
 *
 */
public class APCPodLinkingServlet extends HttpServlet {
    static final long serialVersionUID = 0x198472923413123L;

    private static final String APCPODLINKING_SERIALNUM = "apclink_serialnum";

    /* List of tag attributes supported - we only map tags that have at least one of these */
    private static final String[] supported_tag_attrs = {
        MantisIITagType.DOOR_ATTRIB,
        MantisIITagType.TEMPERATURE_ATTRIB,
        MantisIITagType.HUMIDITY_ATTRIB,
        MantisIITagType.DIFFPRESS_ATTRIB,
        MantisIITagType.AVGDIFFPRESS_ATTRIB,
        MantisIITagType.DRY_ATTRIB,
        MantisIITagType.FLUID_ATTRIB,
        MantisIITagType.DEWPOINT_ATTRIB
    };

    private static String scrubID(String id) {
        int i;
        boolean good = true;
        for(i = 0; good && (i < id.length()); i++) {
            char c = id.charAt(i);
            if((c == '_') || ((c >= '0') && (c <= '9')) ||
                ((c >= 'a') && (c <= 'z')) ||
                ((c >= 'A') && (c <= 'Z'))) {
            }
            else {
                good = false;
            }
        }
        if(good)
            return id;

        StringBuilder sb = new StringBuilder();
        for(i = 0; i < id.length(); i++) {
            char c = id.charAt(i);
            if((c == '_') || ((c >= '0') && (c <= '9')) ||
                ((c >= 'a') && (c <= 'z')) ||
                ((c >= 'A') && (c <= 'Z')))
                sb.append(c);
            else
                sb.append('_');
        }
        return sb.toString();
    }
    /**
     * Base 64 string decode
     */
    public static String decode_b64(String s) {
        StringBuilder sb = new StringBuilder();
        int accum = 0;
        int accumcnt = 0;
        int len = s.length() & 0xFFFC;

        for (int i = 0; i < len; i++) {
            char c = s.charAt(i); /* Get character */
            int v = 0;
            if ((c >= 'A') && (c <= 'Z'))
                v = (int) c - (int) 'A';
            else if ((c >= 'a') && (c <= 'z'))
                v = (int) c - (int) 'a' + 26;
            else if ((c >= '0') && (c <= '9'))
                v = (int) c - (int) '0' + 52;
            else if (c == '+')
                v = 62;
            else if (c == '/')
                v = 63;
            else
                break;
            accum = (accum << 6) | v;
            accumcnt += 6;
            if (accumcnt >= 8) {
                sb.append((char) ((accum >> (accumcnt - 8)) & 0xFF));
                accumcnt -= 8;
            }
        }
        return sb.toString();
    }
    /**
     * Init for servlet - gets things running
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    /**
     * Servlet unload method - do cleanup
     */
    public void destroy() {
        super.destroy();
    }

    // 10 minute maximum session duration - do clean complete after each
    private static final long MAX_SESSION_TIME = (10 * 60 * 1000000000L);

    private static final String RFCTAG_PREFIX = "rfcTag_";
    private static final String RFCREADER_PREFIX = "rfcReader_";
    private static final String READERSTATUS = "readerstatus";
    private static final String READERTAGCAP = "readertagcap";
    /* ------------------------------------------------------------ */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        doPost(request, response);
    }
    /* ------------------------------------------------------------ */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setHeader("Transfer-Encoding", "chunked"); /* Switch to chunked */
        PrintWriter out = response.getWriter();        
        
        String enclist = request.getParameter("ENCLIST");
        Set<String> tagset = new HashSet<String>();
        Set<String> rdrset = new HashSet<String>();
        if(enclist != null) {   /* If list defined, tokenize it */
            String[] encs = enclist.split(",");
            for(int i = 0; i < encs.length; i++) {
                if(encs[i].startsWith(RFCTAG_PREFIX)) {
                    tagset.add(encs[i].substring(RFCTAG_PREFIX.length()));
                }
                else if(encs[i].startsWith(RFCREADER_PREFIX)) {
                    rdrset.add(encs[i].substring(RFCREADER_PREFIX.length()));
                }
            }
        }
        out.println("<variable-set-sequence>");     /* Start with sequence start */

        GetInitialData gid = new GetInitialData();
        gid.rdrset = rdrset;
        gid.tagset = tagset;
        /* Now run on session thread */
		try {
	        RangerProcessor.runActionNow(gid);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}

        try {
            long ts_init = System.nanoTime();
            while((ts_init + MAX_SESSION_TIME) > System.nanoTime()) {
                try {
                    String msg = gid.out_queue.poll(20, TimeUnit.SECONDS);
                    if(msg == null)
                        return;
                    out.print(msg);
                    out.flush();
                    if(out.checkError())
                        return;
                } catch (InterruptedException ix) {
                    return;
                }
            }
        } finally {
            gid.done = true;
            /* And tell it to unregister */
            RangerProcessor.addImmediateAction(gid);

            RangerServer.info("request from Botz done");
        }
    }

    public String getServletInfo() {
        return "ZoneManager APC Pod Linking Support v0.01";
    }

    /* Action for getting initial object states for all readers and supported tags */
    private static class GetInitialData implements Runnable,
        ReaderStatusListener, ReaderLifecycleListener, TagStatusListener, 
        TagLifecycleListener {
        PrintWriter out;
        StringWriter accum;
        Set<String> rdrset;
        Set<String> tagset;
        /* Set of sensors that have been fully published */
        Set<String> snrset = new HashSet<String>();
        /* Set of tags that have been fully published */
        Set<String> tagsent = new HashSet<String>();
        /* Set of readers that have been fully published */
        Set<String> rdrsent = new HashSet<String>();

        boolean initdone;
        boolean done;
        int pingcnt;

        LinkedBlockingQueue<String> out_queue = new LinkedBlockingQueue<String>();

        private void startOut() {
            accum = new StringWriter();
            out = new PrintWriter(accum);
        }
        private void endOut() {
            if(out != null) {
                String s = accum.toString();
                if((s != null) && (s.length() > 0)) {
                    out_queue.offer(s);
                }
                accum = null;
                out = null;
            }
        }
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            startOut();
            if(initdone) {  /* If not first time, just stuff ping and requeue */
                if(!done) {
                    pingcnt++;  /* Bump count */
                    if(pingcnt >= 6) {   /* 60 seconds elapsed */
                        pingcnt = 0;
                        out.println("<variable-set>");

                        /* Loop through readers, and enqueue adds */
                        for (Reader r : ReaderEntityDirectory.getReaders()) {
                            addReader(r);
                            String rid = scrubID(r.getID());
                            if(rdrset.contains(rid)) {
                                addSensor(rid, READERSTATUS, r.getReaderStatus());
                                if(r.getUsedTagCapacityPercent() >= 0) {
                                    addSensor(rid, READERTAGCAP, 
                                        r.getUsedTagCapacityPercent());
                                }
                            }
                        }

                        out.println("</variable-set>");
                    }
                    else {
                        out.println("<variable-set/>");
                    }

                    RangerProcessor.addDelayedAction(this, 10000);
                }
                else {  /* If we're done, clear out */
                    /* Stop listening to tag updates */
                    AbstractTagType.removeGlobalTagLifecycleListener(this);
                    /* And reader updates */
                    RangerServer.removeReaderStatusListener(this);
                    RangerServer.removeReaderLifecycleListener(this);
                    initdone = false;
                }
                endOut();
                return;
            }
            else if(done) { /* If we're done, quit */
                endOut();
                return;
            }
            initdone = true;
            try {
                out.println("<variable-set>");
                /* Loop through readers, and enqueue adds */
                for (Reader r : ReaderEntityDirectory.getReaders()) {
                    String rid = scrubID(r.getID());
                    addReader(r);
                    if(rdrset.contains(rid)) {
                        addSensor(rid, READERSTATUS, r.getReaderStatus());
                        if(r.getUsedTagCapacityPercent() >= 0) {
                            addSensor(rid, READERTAGCAP, 
                                r.getUsedTagCapacityPercent());
                        }
                    }
                }
                Set<String> missingtags = new HashSet<String>(tagset);
                List<Tag> sensorstosend = new LinkedList<Tag>();
                /* Loop through tags and enqueue adds */
                for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                    /* If tag group doesn't support any mapped attributes, skip it */
                    if(tagGroupSupported(tg) == false)
                        continue;
                    for (Map.Entry<String, Tag> te : tg.getTags().entrySet()) {
                        Tag tag = te.getValue();
                        if(tagSupported(tag)) {
                            addSensorTag(tag.getTagGUID(), tag, true);
                            if(tagset.contains(tag.getTagGUID())) { /* If one we're watching, add sensors */
                                sensorstosend.add(tag);
                            }
                            missingtags.remove(tag.getTagGUID());
                        }
                    }
                }
                /* Now, stuff any missing tags */
                for(String tagid : missingtags) {
                    addSensorTag(tagid, null, false);
                }
                /* Add base enclosure */
                addBaseEnclosure();
                /* Now, send sensors for tags */
                for(Tag t : sensorstosend) {
                    addSensors(t, false);
                }
                out.println("</variable-set>");
                /* Start listening to tag updates */
                AbstractTagType.addGlobalTagLifecycleListener(this);
                /* And reader updates */
                RangerServer.addReaderStatusListener(this);
                RangerServer.addReaderLifecycleListener(this);
                /* And queue up ping processing */
                RangerProcessor.addDelayedAction(this, 10000);
            } finally {
                endOut();
                synchronized (this) {
                    notifyAll();
                }
            }
        }
        /**
         * Reader creation notification - called when new reader object creation
         * completed (at the end of its init() call).
         * 
         * @param reader -
         *            reader object created
         */
        public void readerLifecycleCreate(Reader reader) {
            startOut();
            out.println("<variable-set>");
            addReader(reader);
            String rid = scrubID(reader.getID());
            if(rdrset.contains(rid)) {
                addSensor(rid, READERSTATUS, reader.getReaderStatus());
            }
            out.println("</variable-set>");
            endOut();
        }
        /**
         * Reader deletion notification - called at start of reader's cleanup()
         * method, when reader is about to be deleted.
         * 
         * @param reader -
         *            reader object being deleted
         */
        public void readerLifecycleDelete(Reader reader) {
            startOut();
            out.println("<variable-set>");
            dropReader(reader);
            String rid = scrubID(reader.getID());
            if(rdrset.contains(rid)) {
                dropSensor(rid, READERSTATUS);
                dropSensor(rid, READERTAGCAP);
            }
            out.println("</variable-set>");
            endOut();
        }
        /**
         * Reader enable change notification - called when reader has just changed
         * its enabled state (after the setReaderEnabled() method has been called).
         * 
         * @param reader -
         *            reader object being enabled or disabled
         * @param enable -
         *            true if now enabled, false if now disabled
         */
        public void readerLifecycleEnable(Reader reader, boolean enable) {
        }
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         * 
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            String rid = scrubID(reader.getID());
            if(rdrset.contains(rid)) {
                startOut();
                out.println("<variable-set>");
                addReader(reader);
                addSensor(rid, READERSTATUS, newstatus);
                if(reader.getUsedTagCapacityPercent() >= 0)
                    addSensor(rid, READERTAGCAP, reader.getUsedTagCapacityPercent());
                out.println("</variable-set>");
                endOut();
            }
        }
        /**
         * Tag created notification. Called after tag has been created, but before
         * its TagLinks have been added (i.e. after end of init()). Any initial
         * attributes from creating message will have been added by the time this is
         * called.
         * 
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            if(done) {  
                return;
            }
            /* If its a supported tag */
            if(tagSupported(tag) || tagset.contains(tag.getTagGUID())) {
                startOut();
                out.println("<variable-set>");
                addSensorTag(tag.getTagGUID(), tag, true);
                if(tagset.contains(tag.getTagGUID())) { /* If one we're watching, add sensors */
                    addSensors(tag, false);
                }
                out.println("</variable-set>");
                endOut();
            }
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag is
         * about to be deleted (generally due to having no active TagLinks).
         * 
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
            if(done) {  
                return;
            }
            /* Never sent - nothing to drop */
            if(!tagsent.contains(tag.getTagGUID())) {
                return;
            }
            startOut();
            out.println("<variable-set>");

            /* If its a tag of interest */
            if(tagset.contains(tag.getTagGUID())) {
                addSensorTag(tag.getTagGUID(), tag, false);
                addSensors(tag, true);
            }
            else {
                dropSensorTag(tag.getTagGUID());
                dropSensors(tag);
            }
            out.println("</variable-set>");
            endOut();
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than the
         *            number of attributes for the given tag). Ordered to match
         *            attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            boolean didone = false;
            if(done) {  
                return;
            }
            /* If its not a tag of interest */
            if(!tagset.contains(tag.getTagGUID())) {
                return;
            }

            startOut();

            String aids[] = tag.getTagType().getTagAttributes();
            Object[] vals = new Object[aids.length];

            tag.readTagAttributes(vals, 0, vals.length);

            for (int i = 0; i < aids.length; i++) {
                if (vals[i] != oldval[i]) {
                    boolean changed = false;
                    for(int j = 0; (!changed) && (j < supported_tag_attrs.length); j++) {
                        if(aids[i].equals(supported_tag_attrs[j])) {    /* Supported attrib? */
                            changed = true;
                        }
                    }
                    /* If low battery, add it also */
                    if(aids[i].equals(MantisIITagType.LOWBATT_ATTRIB))
                        changed = true;
                    if(changed) {
                        if(!didone) {
                            out.println("<variable-set>");  
                            didone = true;
                        }
                        addSensor(tag.getTagGUID(), aids[i], vals[i]);
                    }
                    /* If location change, do tag update */
                    if(aids[i].equals(LocationRuleFactory.ATTRIB_ZONELOCATION)) {
                        if(!didone) {
                            out.println("<variable-set>");  
                            didone = true;
                        }
                        addSensorTag(tag.getTagGUID(), tag, true);                        
                    }
                }
            }
            if(didone) {
                out.println("</variable-set>");  
            }
            endOut();
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        /**
         * TagLink added notificiation. Called after taglink has been added to tag.
         * 
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed from
         * tag.
         * 
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
        }

        private void dropSensor(String tagid, String aid) {
            String snrid = tagid + "_" + aid;
            if(snrset.contains(snrid)) {
                if(aid.equals(MantisIITagType.DOOR_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DOOR\" class=\"nbDoorSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor/nbDoorSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.TEMPERATURE_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_TEMP\" class=\"nbTempSensor\" classpath=\"/nbSensor/nbNumSensor/nbTempSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.HUMIDITY_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_HUMI\" class=\"nbHumidSensor\" classpath=\"/nbSensor/nbNumSensor/nbHumidSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.DIFFPRESS_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DIFFPRESS\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.AVGDIFFPRESS_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_AVGDIFFPRESS\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.DEWPOINT_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DEW\" class=\"nbDewPointSensor\" classpath=\"/nbSensor/nbNumSensor/nbDewPointSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.DRY_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DRY\" " + 
                        "class=\"nbDryContactSnr\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor/nbDryContactSnr\"/>");
                }
                else if(aid.equals(MantisIITagType.FLUID_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_FLUID\" class=\"nbBoolSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor\"/>");
                }
                else if(aid.equals(MantisIITagType.LOWBATT_ATTRIB)) {
                    out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_LOWBATT\" class=\"nbBoolSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor\"/>");
                }
                else if(aid.equals(READERSTATUS)) {
                    out.println("<variable varid=\"" + RFCREADER_PREFIX + tagid + "_STATUS\" class=\"nbStateSensor\" classpath=\"/nbSensor/nbStateSensor\"/>");
                }
                else if(aid.equals(READERTAGCAP)) {
                    out.println("<variable varid=\"" + RFCREADER_PREFIX + tagid + "_TAGCAP\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\"/>");
                }
                snrset.remove(snrid);
            }
        }

        private void addSensor(String tagid, String aid, Object val) {
            boolean full = false;
            boolean istag = true;
            String snrid = tagid + "_" + aid;
            if(!snrset.contains(snrid)) {    /* Already published? */
                full = true;    /* No, publish it */
                snrset.add(snrid);
            }
            if(aid.equals(MantisIITagType.DOOR_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DOOR\" class=\"nbDoorSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor/nbDoorSensor\">");
                if(val == null)
                    out.println("<u32-val isnull=\"yes\"/>");            
                else
                    out.println("<u32-val>" + (val.toString().equals("true")?0:1) + "</u32-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"yes\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Door Switch%}\">Door Switch</nls-string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.TEMPERATURE_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_TEMP\" class=\"nbTempSensor\" classpath=\"/nbSensor/nbNumSensor/nbTempSensor\">");
                if(val == null)
                    out.println("<double-val isnull=\"yes\"/>");            
                else
                    out.println("<double-val>" + val.toString() + "</double-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"yes\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Temperature%}\">Temperature</nls-string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.DEWPOINT_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DEW\" class=\"nbDewPointSensor\" classpath=\"/nbSensor/nbNumSensor/nbDewPointSensor\">");
                if(val == null)
                    out.println("<double-val isnull=\"yes\"/>");            
                else
                    out.println("<double-val>" + val.toString() + "</double-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"yes\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Dew Point%}\">Dew Point</nls-string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.HUMIDITY_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_HUMI\" class=\"nbHumidSensor\" classpath=\"/nbSensor/nbNumSensor/nbHumidSensor\">");
                if(val == null)
                    out.println("<double-val isnull=\"yes\"/>");            
                else
                    out.println("<double-val>" + val.toString() + "</double-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"yes\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Humidity%}\">Humidity</nls-string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.DIFFPRESS_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DIFFPRESS\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\">");
                if(val == null)
                    out.println("<double-val isnull=\"yes\"/>");            
                else {
                    double v = ((Double)val).doubleValue() * 0.00401463078662; /* Make inches WC */
                    out.println("<double-val>" + String.format("%.3f", v) + "</double-val>");
                }
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"no\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Differential Pressure%}\">Differential Pressure</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbUnitsID\" isclassdef=\"no\">");
                    out.println("<varid-val>nbUnits_inwc</varid-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbValInc\" isclassdef=\"no\">");
                    out.println("<double-val>0.001</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMinVal\" isclassdef=\"no\">");
                    out.println("<double-val>-2.0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMaxVal\" isclassdef=\"no\">");
                    out.println("<double-val>2.0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbPrintfFmt\" isclassdef=\"no\">");
                    out.println("<string-val>%.3f</string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.AVGDIFFPRESS_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_AVGDIFFPRESS\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\">");
                if(val == null)
                    out.println("<double-val isnull=\"yes\"/>");            
                else {
                    double v = ((Double)val).doubleValue() * 0.00401463078662; /* Make inches WC */
                    out.println("<double-val>" + String.format("%.3f", v) + "</double-val>");
                }
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\" isclassdef=\"no\">");
                    out.println("<nls-string-val raw=\"%{nbMsg|Average Differential Pressure%}\">Average Differential Pressure</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbUnitsID\" isclassdef=\"no\">");
                    out.println("<varid-val>nbUnits_inwc</varid-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbValInc\" isclassdef=\"no\">");
                    out.println("<double-val>0.001</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMinVal\" isclassdef=\"no\">");
                    out.println("<double-val>-2.0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMaxVal\" isclassdef=\"no\">");
                    out.println("<double-val>2.0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbPrintfFmt\" isclassdef=\"no\">");
                    out.println("<string-val>%.3f</string-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.DRY_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_DRY\" " + 
                    "class=\"nbDryContactSnr\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor/nbDryContactSnr\">");
                if(val == null)
                    out.println("<u32-val isnull=\"yes\"/>");            
                else
                    out.println("<u32-val>" + (val.toString().equals("true")?0:1) + "</u32-val>");
            }
            else if(aid.equals(MantisIITagType.FLUID_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_FLUID\" class=\"nbBoolSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor\">");
                if(val == null)
                    out.println("<u32-val isnull=\"yes\"/>");            
                else
                    out.println("<u32-val>" + (val.toString().equals("true")?1:0) + "</u32-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\">");
                    out.println("<nls-string-val raw=\"%{|Fluid Sensor%}\">Fluid Sensor</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbEnum\">");
                    out.println("<nls-string-list-val>");
                    out.println("<nls-string-val raw=\"%{|Normal%}\">Normal</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Fluid Detected%}\">Fluid Detected</nls-string-val>");
                    out.println("</nls-string-list-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbSimpleThreshDef\">");
                    out.println("<varid-list-val>");
                    out.println("<varid-val>nbSimpleThreshPlcyBlkDef_bool_err_1</varid-val>");
                    out.println("</varid-list-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(MantisIITagType.LOWBATT_ATTRIB)) {
                out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "_LOWBATT\" class=\"nbBoolSensor\" classpath=\"/nbSensor/nbStateSensor/nbBoolSensor\">");
                if(val == null)
                    out.println("<u32-val isnull=\"yes\"/>");            
                else
                    out.println("<u32-val>" + (val.toString().equals("true")?1:0) + "</u32-val>");
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\">");
                    out.println("<nls-string-val raw=\"%{|Battery Status%}\">Battery Status</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbEnum\">");
                    out.println("<nls-string-list-val>");
                    out.println("<nls-string-val raw=\"%{|Normal%}\">Normal</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Low%}\">Low</nls-string-val>");
                    out.println("</nls-string-list-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbSimpleThreshDef\">");
                    out.println("<varid-list-val>");
                    out.println("<varid-val>nbSimpleThreshPlcyBlkDef_bool_err_1</varid-val>");
                    out.println("</varid-list-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(READERSTATUS)) {
                istag = false;  /* Reader attribute */
                out.println("<variable varid=\"" + RFCREADER_PREFIX + tagid + "_STATUS\" class=\"nbStateSensor\" classpath=\"/nbSensor/nbStateSensor\">");
                if(val == null) {
                    out.println("<u32-val isnull=\"yes\"/>");            
                }
                else if(val instanceof ReaderStatus) {
                        out.println("<u32-val>" + ((ReaderStatus)val).getOrd() + "</u32-val>");
                }
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\">");
                    out.println("<nls-string-val raw=\"%{|Reader Status%}\">Reader Status</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbEnum\">");
                    out.println("<nls-string-list-val>");
                    out.println("<nls-string-val raw=\"%{|Disabled%}\">Disabled</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Connecting%}\">Connecting</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Initializing%}\">Initializing</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Initialized%}\">Initialized</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Active%}\">Active</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Disconnecting%}\">Disconnecting</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Disconnected%}\">Disconnected</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Reader Failure%}\">Reader Failure</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Config Failure%}\">Config Failure</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Connect Failure%}\">Connect Failure</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Noise Detected%}\">Noise Detected</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Unknown%}\">Unknown</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|Access Denied%}\">Access Denied</nls-string-val>");
                    out.println("<nls-string-val raw=\"%{|High Traffic%}\">High Traffic</nls-string-val>");
                    out.println("</nls-string-list-val>");
                    out.println("</metadata>");
                }
            }
            else if(aid.equals(READERTAGCAP)) {
                istag = false;  /* Reader attribute */
                out.println("<variable varid=\"" + RFCREADER_PREFIX + tagid + "_TAGCAP\" class=\"nbNumSensor\" classpath=\"/nbSensor/nbNumSensor\">");
                if(val == null) {
                    out.println("<double-val isnull=\"yes\"/>");            
                }
                else {
                        out.println("<double-val>" + val.toString() + "</double-val>");
                }
                if(full) {
                    out.println("<metadata slotid=\"nbLabel\">");
                    out.println("<nls-string-val raw=\"%{|Tag Capacity Used%}\">Tag Capacity Used</nls-string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbValInc\">");
                    out.println("<double-val>1.0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMinVal\">");
                    out.println("<double-val>0</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbMaxVal\">");
                    out.println("<double-val>100</double-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbPrintfFmt\">");
                    out.println("<string-val>%.0f</string-val>");
                    out.println("</metadata>");
                    out.println("<metadata slotid=\"nbUnitsID\">");
                    out.println("<varid-val>nbUnits_Pct</varid-val>");
                    out.println("</metadata>");
                }
            }
            else
                return;
            if(full) {
                out.println("<metadata slotid=\"nbEncID\">");
                if(istag)
                    out.println("<varid-val>" + RFCTAG_PREFIX + tagid + "</varid-val>");
                else
                    out.println("<varid-val>" + RFCREADER_PREFIX + tagid + "</varid-val>");
                out.println("</metadata>");
            }
            out.println("</variable>");            
        }

        private void dropSensors(Tag tag) {
            String tagid = tag.getTagGUID();
            for(int i = 0; i < supported_tag_attrs.length; i++) {
                dropSensor(tagid, supported_tag_attrs[i]);
            }
            dropSensor(tagid, MantisIITagType.LOWBATT_ATTRIB);
        }

        private void addSensors(Tag tag, boolean forcenull) {
            String tagid = tag.getTagGUID();
            Map<String,Object> map = tag.getTagAttributes();
            TagType tt = tag.getTagType();
            Object val;
            for(int i = 0; i < supported_tag_attrs.length; i++) {
                val = map.get(supported_tag_attrs[i]);
                if(forcenull)
                    val = null;
                if(val != null) {
                    addSensor(tagid, supported_tag_attrs[i], val);
                }
                else if(tt.getTagAttributeIndex(supported_tag_attrs[i]) >= 0) {
                    addSensor(tagid, supported_tag_attrs[i], null);
                }
            }
            /* Add low battery too */
            val = map.get(MantisIITagType.LOWBATT_ATTRIB);
            if(forcenull) val = null;
            addSensor(tagid, MantisIITagType.LOWBATT_ATTRIB, val);
        }

        private void dropSensorTag(String tagid) {
            if(tagsent.contains(tagid) == false) {  /* Not sent before? */
                return;
            }
            tagsent.remove(tagid);
            out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "\" class=\"nbEnclosure\" classpath=\"/nbEnclosure\"/>");
        }

        private void addSensorTag(String tagid, Tag tag, boolean online) {
            boolean full = false;
            if(tagsent.contains(tagid) == false) {  /* Not sent before? */
                full = true;
                tagsent.add(tagid);
            }
            out.println("<variable varid=\"" + RFCTAG_PREFIX + tagid + "\" class=\"nbEnclosure\" classpath=\"/nbEnclosure\">");
            if(online)
                out.println("<u32-val>2</u32-val>");
            else
                out.println("<u32-val>0</u32-val>");
            if(tag != null) {
                Object loc = tag.getTagAttributes().get(LocationRuleFactory.ATTRIB_ZONELOCATION);
                out.println("<metadata slotid=\"nbLocationData\"><struct-val>");
                if(loc != null) {
                    Location l = ReaderEntityDirectory.findLocation((String)loc);

                    out.print("<struct-element fieldid=\"LOCATION\"><string-val>");
                    if(l != null)
                        out.print("/" + l.getPath());
                    out.println("</string-val></struct-element>");
                }
                out.println("</struct-val></metadata>");
            }
            if(full) {
                out.println("<metadata slotid=\"nbLabel\">");
                out.println("<nls-string-val raw=\"%{|RF Code Tag %s|" + tagid + "%}\">RF Code Tag " +
                tagid + "</nls-string-val>");
                out.println("</metadata>");
                if(tag != null) {
                    TagType tt = tag.getTagType();

                    out.println("<metadata slotid=\"nbProductData\">");
                    out.println("<struct-val>");
                    out.println("<struct-element fieldid=\"vendor\"><string-val>RF Code, Inc.</string-val></struct-element>");
                    out.println("<struct-element fieldid=\"type\"><string-val>" + tt.getLabel() + "</string-val></struct-element>");
                    out.println("<struct-element fieldid=\"fullmodel\"><string-val>" + tt.getLabel() + "</string-val></struct-element>");
                    out.println("<struct-element fieldid=\"serial_num\"><string-val>" + tagid + "</string-val></struct-element>");
                    out.println("<struct-element fieldid=\"manufacturer\"><string-val>RF Code, Inc.</string-val></struct-element>");
                    out.println("</struct-val>");
                    out.println("</metadata>");
                }
                out.println("<metadata slotid=\"nbSerialNum\">");
                out.println("<string-val>" + tagid + "</string-val>");
                out.println("</metadata>");
            }
            if((tag != null) && (full || online)) {    /* If full, or online, send this (might need to fix bad defaults) */
                TagType tt = tag.getTagType();
                Map<String,Object> ta = tag.getTagAttributes();

                String baseid = "menu_link_sensor";
                String nbcid = "wireless_pod";

                if(tt.getTagAttributeIndex(MantisIITagType.HUMIDITY_ATTRIB) >= 0) { /* Humidity */
                    baseid = "menu_link_humi";
                }
                else if(tt.getTagAttributeIndex(MantisIITagType.TEMPERATURE_ATTRIB) >= 0) { /* Temp */
                    baseid = "menu_link_temp";
                }
                else if(ta.get(MantisIITagType.FLUID_ATTRIB) != null) { /* Fluid */
                    baseid = "menu_link_humi";
                }
                else if(ta.get(MantisIITagType.DOOR_ATTRIB) != null) { /* Door */
                    baseid = "menu_link_door";
                }
                else if(ta.get(MantisIITagType.DRY_ATTRIB) != null) { /* Dry */
                    baseid = "menu_link_dryc";
                }
                else if(tt.getTagAttributeIndex(MantisIITagType.FLUID_ATTRIB) >= 0) { /* Fluid */
                    baseid = "menu_link_humi";
                }
                else if(tt.getTagAttributeIndex(MantisIITagType.DOOR_ATTRIB) >= 0) { /* Door */
                    baseid = "menu_link_door";
                }
                else if(tt.getTagAttributeIndex(MantisIITagType.DRY_ATTRIB) >= 0) { /* Dry */
                    baseid = "menu_link_dryc";
                }
                out.println("<metadata slotid=\"nbEncMenuIcons\">");
                out.println("<string-list-val>");
                out.println("<string-val>" + baseid + "_unplug.gif</string-val>");
                out.println("<string-val>" + baseid + "_error.gif</string-val>");
                out.println("<string-val>" + baseid + ".gif</string-val>");
                out.println("</string-list-val>");
                out.println("</metadata>");
                out.println("<metadata slotid=\"nbEncNBCIcon\">");
                out.println("<string-val>" + nbcid + ".png</string-val>");
                out.println("</metadata>");
            }
            out.println("</variable>");            
        }
        private boolean tagSupported(Tag tag) {  
            Map<String,Object> map = tag.getTagAttributes();
            for(int i = 0; i < supported_tag_attrs.length; i++) {
                if(map.get(supported_tag_attrs[i]) != null)
                    return true;
            }
            return false;
        }
        private boolean tagGroupSupported(TagGroup tg) {  
            String[] aids = tg.getTagType().getTagAttributes();
            for(int i = 0; i < supported_tag_attrs.length; i++) {
                for(int j = 0; j < aids.length; j++) {
                    if(aids[j].equals(supported_tag_attrs[i]))
                        return true;
                }
            }
            return false;
        }

        private void dropReader(Reader rdr) {
            out.println("<variable varid=\"rfcReader_" + scrubID(rdr.getID()) + "\" class=\"nbEnclosure\" classpath=\"/nbEnclosure\">");
            out.println("</variable>");            
        }

        private void addReader(Reader rdr) {
            boolean full = false;
            String rdrid = RFCREADER_PREFIX + scrubID(rdr.getID());
            if(rdrsent.contains(rdrid) == false) {
                full = true;
                rdrsent.add(rdrid);
            }
            out.println("<variable varid=\"" + rdrid + "\" class=\"nbEnclosure\" classpath=\"/nbEnclosure\">");
            ReaderStatus rs = rdr.getReaderStatus();
            if(ReaderStatus.isOnline(rs))
                out.println("<u32-val>2</u32-val>");
            else
                out.println("<u32-val>0</u32-val>");
            if(full) {
                out.println("<metadata slotid=\"nbLabel\">");
                out.println("<nls-string-val raw=\"%{|RF Code Reader - %s|" + rdr.getID() + "%}\">RF Code Reader - " +
                    rdr.getID() + "</nls-string-val>");
                out.println("</metadata>");
                out.println("<metadata slotid=\"nbLocationData\" isclassdef=\"yes\"><struct-val/></metadata>");
                out.println("<metadata slotid=\"nbProductData\">");
                out.println("<struct-val>");
                out.println("<struct-element fieldid=\"vendor\"><string-val>RF Code, Inc.</string-val></struct-element>");
                out.println("<struct-element fieldid=\"type\"><string-val>" + 
                    rdr.getReaderFactory().getReaderFactoryLabel() + "</string-val></struct-element>");
                out.println("<struct-element fieldid=\"fullmodel\"><string-val>" + 
                    rdr.getReaderFactory().getReaderFactoryLabel() + "</string-val></struct-element>");
                out.println("<struct-element fieldid=\"manufacturer\"><string-val>RF Code, Inc.</string-val></struct-element>");
                out.println("<struct-element fieldid=\"appversion\"><string-val>" + 
                    rdr.getReaderFirmwareVersion() + "</string-val></struct-element>");
                out.println("<struct-element fieldid=\"serial_num\"><string-val>" + 
                    rdr.getID() + "</string-val></struct-element>");
                out.println("</struct-val>");
                out.println("</metadata>");
                out.println("<metadata slotid=\"nbEncMenuIcons\">");
                out.println("<string-list-val>");
                out.println("<string-val>menu_link_wireless_unplug.gif</string-val>");
                out.println("<string-val>menu_link_wireless_error.gif</string-val>");
                out.println("<string-val>menu_link_wireless.gif</string-val>");
                out.println("</string-list-val>");
                out.println("</metadata>");
                out.println("<metadata slotid=\"nbEncNBCIcon\">");
                out.println("<string-val>wireless_pod.png</string-val>");
                out.println("</metadata>");
                out.println("<metadata slotid=\"nbSerialNum\">");
                out.println("<string-val>reader_" + rdr.getID() + "</string-val>");
                out.println("</metadata>");            
            }
            out.println("</variable>");            
        }

        private void addBaseEnclosure() {
            out.println("<variable varid=\"nbBaseEnclosure\" class=\"nbEnclosure\" classpath=\"/nbEnclosure\">");
            out.println("<u32-val>2</u32-val>");
            out.println("<metadata slotid=\"nbLabel\">");
            out.println("<nls-string-val raw=\"%{|RF Code Zone Manager%}\">RF Code Zone Manager</nls-string-val>");
                out.println("</metadata>");
//            out.println("<metadata slotid=\"nbLocationData\" isclassdef=\"yes\"><struct-val/></metadata>");
            out.println("<metadata slotid=\"nbProductData\">");
            out.println("<struct-val>");
            out.println("<struct-element fieldid=\"vendor\"><string-val>RF Code, Inc.</string-val></struct-element>");
            out.println("<struct-element fieldid=\"type\"><string-val>Zone Manager</string-val></struct-element>");
            out.println("<struct-element fieldid=\"fullmodel\"><string-val>RF Code Zone Manager</string-val></struct-element>");
            out.println("<struct-element fieldid=\"manufacturer\"><string-val>RF Code, Inc.</string-val></struct-element>");
            out.println("<struct-element fieldid=\"appversion\"><string-val>" +
                RangerServer.getVersion() + "_" + 
                RangerServer.getBuild() + "</string-val></struct-element>");
            out.println("</struct-val>");
            out.println("</metadata>");
            out.println("<metadata slotid=\"nbEncMenuIcons\">");
            out.println("<string-list-val>");
            out.println("<string-val>menu_link_rack_unplug.gif</string-val>");
            out.println("<string-val>menu_link_rack_error.gif</string-val>");
            out.println("<string-val>menu_link_rack.gif</string-val>");
            out.println("</string-list-val>");
            out.println("</metadata>");
            out.println("<metadata slotid=\"nbEncNBCIcon\">");
            out.println("<string-val>rack400.png</string-val>");
            out.println("</metadata>");
            out.println("<metadata slotid=\"nbSerialNum\">");
            String snum = RangerServer.getConfig().getProperty(APCPODLINKING_SERIALNUM, "");
            if(snum.length() == 0) {    /* Not set yet, so make one */
                snum = "zonemgr_" + (new Random().nextInt(9)) + 
                    (new Random().nextInt(9)) + (new Random().nextInt(9))
                    + (new Random().nextInt(9)) + (new Random().nextInt(9))
                    + (new Random().nextInt(9)) + (new Random().nextInt(9))
                    + (new Random().nextInt(9));
                RangerServer.setConfigVariable(APCPODLINKING_SERIALNUM, snum);
                RangerServer.saveConfig();
            }
            out.println("<string-val>" + snum + "</string-val>");
            out.println("</metadata>");            
            out.println("</variable>");            
        }
    }
}

