package com.rfcode.ranger;

/**
 * Config update listener interface - used for notification of server configuration updates
 */
public interface ConfigUpdateListener {
    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
     public void configUpdateNotification();
}
