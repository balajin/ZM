package com.rfcode.ranger;

import java.io.File;
import java.io.FileFilter;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;
import java.util.Collections;

/**
 * Handler class for update event storage files
 *
 * Directory structure, under a provided base directory, is structured as follows:
 *   <base>--\
 *           |-<UTC-date : msec/msec-per-day>-\
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.log
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.init
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.log
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.init
 *           |                                .....(96 pairs per day)
 *           |-<UTC-date : msec/msec-per-day>-\
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.log
 *           |                                |-<UTC-15min : msec/msec-per-15minutes>.init
 *           |                                .....(96 pairs per day)
 *           ....
 *
 * @author Mike Primm
 */
public class UpdateEventStorage {
    private static final short REC_SIG = (short)0xBEEF;
    private File base_dir;
    private int max_age;
    private long last_fname = 0;
    /* One day per directory */
    private static final long TIME_PER_DIRECTORY = (1000L*3600L*24L);
    /* 15 Minutes per file (must be factor of TIME_PER_DIRECTORY) */
    private static final long TIME_PER_FILE = (1000L*60L*15L);
	private static final String LOGEXT = ".log";
	private static final String INITEXT = ".init";
	/**
     * Constructor for storage object
     * @param basedir - base directory for storage
     * @param maxage - maximum age, in hours, for data in storage
     */
    public UpdateEventStorage(File basedir, int maxage) {
        base_dir = basedir;
        max_age = maxage;
        if(max_age < 0) max_age = 0;
        last_fname = 0;
        /* Do age out, and check for time warps */
        doAgeOut(System.currentTimeMillis(), true);
    }
    private static class DirectoryFilter implements FileFilter {
        public boolean accept(File f) {
            if(f.isDirectory()) {
                try {
                    Long.parseLong(f.getName());
                    return true;
                } catch (NumberFormatException nfx) {
                }
            }
            return false;
        }
    }
    private static DirectoryFilter df = new DirectoryFilter();

    private static class LogFileFilter implements FileFilter {
        public boolean accept(File f) {
            if(f.isFile()) {
                try {
                    String n = f.getName();
                    int dot = n.indexOf('.');
                    if(dot > 0) {
                        Long.parseLong(n.substring(0,dot));
                        return true;
                    }
                } catch (NumberFormatException nfx) {
                }
            }
            return false;
        }
    }
    private static LogFileFilter lff = new LogFileFilter();
    /**
     * Do age out of old data files
     */
    public void doAgeOut(long now, boolean timewarp) {
        File[] dirs = base_dir.listFiles(df);   /* Get list of directories */
       
        if(dirs == null)
            return;
        long oldest = now - ((long)max_age * 3600L * 1000L);    /* Oldest allowed */
        long oldest_dir = oldest / TIME_PER_DIRECTORY;
        long newest_dir = now / TIME_PER_DIRECTORY;
        for(File f : dirs) {
            try {
                long date = Long.parseLong(f.getName());    /* Get UTC date */
                /* If older than oldest, or newer than newest, nuke it */
                if((date < oldest_dir) || (date > newest_dir) || (max_age == 0)) {
                    deleteDirectory(f);
                }
                else if(date == oldest_dir) {   /* If matches oldest date */
                    /* Toss all files before the oldest file index */
                    trimBefore(f, oldest);
                }
                else if(date == newest_dir) {   /* If matches newest date */
                    if(timewarp)
                        trimAfter(f, now);  /* Trim after present (time warp) */
                }
            } catch (NumberFormatException nfx) {
            }
        }
    }
    /**
     * Delete directory and all its contents (recursive)
     */
    public static void deleteDirectory(File f) {
        File[] contents = f.listFiles();
        if(contents == null)
            return;
        for(File ff : contents) {
            if(ff.isDirectory()) {
                String n = ff.getName();
                if(n.equals(".") || n.equals("..")) {   /* Skip . and .. */
                }
                else {
                    deleteDirectory(ff);
                }
            }
            else {  /* Anything else, just delete */
                ff.delete();
            }
        }
        f.delete();
    }
    /**
     * Trim all files from before given time
     */
    private static void trimBefore(File f, long oldest) {
        File[] contents = f.listFiles(lff); /* Get list of log files */
        if(contents == null)
            return;
        long last = oldest/TIME_PER_FILE;
        for(File ff : contents) {
            long d = -1;
            try {
                String n = ff.getName();
                int dot = n.indexOf('.');
                if(dot > 0) {
                    d = Long.parseLong(n.substring(0,dot));
                }
            } catch (NumberFormatException nfx) {
            }
            if(d < last) {  /* If older than last allowed */
                ff.delete();    /* Zap it */
            }
        }
    }
    /**
     * Trim all files after given time
     */
    private static void trimAfter(File f, long newest) {
        File[] contents = f.listFiles(lff); /* Get list of log files */
        if(contents == null)
            return;
        long last = (newest+TIME_PER_FILE-1)/TIME_PER_FILE;
        for(File ff : contents) {
            long d = -1;
            try {
                String n = ff.getName();
                int dot = n.indexOf('.');
                if(dot > 0) {
                    d = Long.parseLong(n.substring(0,dot));
                }
            } catch (NumberFormatException nfx) {
            }
            if(d > last) {  /* If newer than last allowed */
                ff.delete();    /* Zap it */
            }
            else if(d == last) {    /* If same as last allowed */
                scrubRecordsAfter(ff, newest);  /* Scrub too new records */                   
            }
        }
    }
    /**
     * Scrub records after given timestamp in log file
     */
    private static void scrubRecordsAfter(File f, long newest) {
        RandomAccessFile raf = null;
        long trimpoint = -1;
        try {
            raf = new RandomAccessFile(f,"r");
            do {
                trimpoint = raf.getFilePointer();   /* Save record start */
                short sig = raf.readShort();    /* Get record signature */
                short len = raf.readShort();    /* Get record length */
                long  ts = raf.readLong();      /* Get timestamp */
                if(sig != REC_SIG) {    /* If bad signature, quit (bad file) */
                    break;
                }
                if(ts >= newest) {  /* Too new?                         */
                    break;              /* Break - trim rest */
                }
                raf.seek(trimpoint + 4 + (long)len);    /* Seek to next record */
            } while (true);
        } catch (IOException iox) {
        } finally {
            if(raf != null) {
                try { raf.close(); } catch (IOException ix) {}
                raf = null;
            }
        }
        /* If need to trim it, do so */
        if((trimpoint > 0) && (trimpoint < f.length())) {
            try {
                raf = new RandomAccessFile(f,"rw");              
                raf.setLength(trimpoint);
            } catch (IOException iox) {
            } finally {
                if(raf != null) {
                    try { raf.close(); } catch (IOException ix) {}
                    raf = null;
                }
            }
        }
        else if(trimpoint == 0) {   /* If no records, zap the file */
            f.delete();
        }
    }
	/**
	 * Do initial state dump as hourly checkpoint
	 */
	private void doInitDump() throws IOException {
		/* Request dump of initial update events */
		LinkedList<UpdateEvent> lst = ServletCommandInterface.getInitUpdateEvents();
		if(lst != null) {
			writeEventsInt(lst, INITEXT);
		}
	}
    /**
     * Write events to log
     * @param evts - events to be written
     */
    public void writeEvents(List<UpdateEvent> evts) throws IOException {
		writeEventsInt(evts, LOGEXT);
	}
    /**
     * Write events to log
     * @param evts - events to be written
     */
    private void writeEventsInt(List<UpdateEvent> evts, String ext) throws IOException {
        if(max_age == 0) {  /* If diabled, just ignore */
            return;
        }
        long cur_date = -1;
        long cur_fname = -1;
        File dir = null;
        RandomAccessFile raf = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        long len = 0;
        boolean ageout = false;
		boolean doinitdump = false;
        try {
            /* Roll through the sequence */
            for(UpdateEvent evt : evts) {
                long date = evt.timestamp / TIME_PER_DIRECTORY;        
                long fname = evt.timestamp / TIME_PER_FILE;

                if(last_fname != fname) {   /* Writing to new filename? */
                    ageout = true;
                    last_fname = fname;
					doinitdump = true;
                }
                if(date != cur_date) {
                    dir = new File(base_dir, Long.toString(date));
                    if(dir.exists() == false) {
                        dir.mkdirs();
                    }
                    cur_date = date;
                }
                if(fname != cur_fname) {
                    if(raf != null) { raf.close(); raf = null; }
                    File f = new File(dir, Long.toString(fname) + ext);
					if(ext.equals(INITEXT)) {	/* If init dump, purge file first */
						if(f.exists()) 
							f.delete();
					}
                    raf = new RandomAccessFile(f, "rw");
                    len = raf.length(); /* Save initial length */
                    raf.seek(len);      /* See to end to append */
					cur_fname = fname;
                }
                evt.writeToStream(baos);    /* Encode event */
                byte[] rec = baos.toByteArray();
                baos.reset();
                raf.writeShort(REC_SIG);
                raf.writeShort(rec.length);
                raf.write(rec);
            }
        } catch (IOException iox) {
            if(raf != null) {
                raf.setLength(len); /* Rewind length on error */
            }
            throw iox;
        } finally {
            if(raf != null) { raf.close(); raf = null; }
			if(ext.equals(LOGEXT)) {
				if(ageout) {    /* If needed, do age out too */
					doAgeOut(System.currentTimeMillis(), false);
				}
				if(doinitdump) {
					doInitDump();
				}
			}
        }
    }
    /**
     * Write single event to log
     * @param evt - event to write
     */
    public void writeEvent(UpdateEvent evt) throws IOException {
        writeEvents(Collections.singletonList(evt));
    }
    /**
     * Update age out
     */
    public void updateAgeOut(int maxage) {
        boolean do_ageout = false;
        if(maxage < 0) maxage = 0;
        if(max_age > maxage) {  /* Only need to repeat age-out on shortening of max time */
            do_ageout = true;
        }
        max_age = maxage;
        if(do_ageout) {
            doAgeOut(System.currentTimeMillis(), false);
        }
    }
    /**
     * Get age out time, in hours
     */
    public int getAgeOut() {
        return max_age;
    }
    /**
     * Read range of events, based on provided time, and limited by given count
     */
    public LinkedList<UpdateEvent> readEvents(long start_time, long end_time, int maxcnt) {
        LinkedList<UpdateEvent> lst = new LinkedList<UpdateEvent>();
        long start_fname = start_time / TIME_PER_FILE;
        long end_fname = end_time / TIME_PER_FILE;
        long cur_dir = 0;
        File dir = null;
        int cnt = 0;
        long now = System.currentTimeMillis();
		boolean init_first = false;
		boolean might_init_first = false;
		boolean at_start = true;
        /* Sanity bound the range */
        long oldest_fname = (now - ((long)max_age * 3600L * 1000L)) / TIME_PER_FILE;
        if(start_fname < oldest_fname) {
			start_fname = oldest_fname;
			init_first = true;	/* We need to provide init first, since history exhausted */
		}
		else if(start_fname == oldest_fname) {	/* If during first file, might need to init */
			might_init_first = true;
		}
        long newest_fname = now / TIME_PER_FILE;
        if(end_fname > newest_fname) end_fname = newest_fname;

        for(long fname = start_fname; 
            (fname <= end_fname) && (cnt < maxcnt); fname++) {
            long dirname = (fname * TIME_PER_FILE)/TIME_PER_DIRECTORY;
            if(dirname != cur_dir) {
                dir = new File(base_dir, Long.toString(dirname));
                cur_dir = dirname;
            }
            if(dir.exists() == false) {	
				/* If at start, we need to do init */
				if (at_start) {
					init_first = true;
					might_init_first = false;
					at_start = false;
				}
                continue;
			}
            File f = new File(dir, Long.toString(fname)+".log");   /* Make file name */
            if(f.exists() == false) {     /* Not there, move on */
				if (at_start) {			/* File missing - if at start, do init */
					init_first = true;
					might_init_first = false;
					at_start = false;
				}
                continue;
			}
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(f, "r");
                long readpt = 0;
                long lastts = 0;
                boolean dup;
                do {
                    dup = false;
                    readpt = raf.getFilePointer();   /* Save record start */
                    short sig = raf.readShort();    /* Get record signature */
                    short len = raf.readShort();    /* Get record length */
                    long  ts = raf.readLong();      /* Get timestamp */
                    if(sig != REC_SIG) {    /* If bad signature, quit (bad file) */
                        break;
                    }
					/* If at start, and might need init, and first TS is after
					 * checkpoint time, do init */
					if(at_start && might_init_first && (ts > start_time)) {
						init_first = true;
						might_init_first = false;
					}
					/* If need init first, load list from init file */
					if(init_first) {
						start_time = loadInitFile(lst, dir, fname, start_time);
						init_first = false;
                        cnt = lst.size();
                        if(lst.size() > 0)
                            lastts = lst.peekLast().timestamp;
					}
					at_start = false;
                    if((ts > start_time) && (ts <= end_time)) { /* If in range? */
                        dup = (ts == lastts);       /* See if dup */

                        raf.seek(readpt + 4);   /* Back up  */
                        byte[] buf = new byte[len];
                        raf.read(buf);              /* Read record */
                        ByteArrayInputStream bais = new ByteArrayInputStream(buf);
                        UpdateEvent evt = new UpdateEvent();
                        evt.readFromStream(bais);   /* Load event */
                        lst.add(evt);           /* Add to list */
                        cnt++;                  /* Bump count */
                    }
                    else if(ts > end_time) {    /* Past end?  we're done */
                        break;
                    }
                    else {  /* Else, skip over it                           */
                        raf.seek(readpt + len + 4);
                    }
                    lastts = ts;            /* Save timestamp - need to help
                                            * keep dups together */
                } while ((cnt < maxcnt) || dup);
            } catch (IOException iox) {
            } finally {
                if(raf != null) { 
                    try { raf.close(); } catch (IOException iox) {};
                    raf = null;
                }
            }
        }
        return lst;
    }
	/**
	 * Load init file into provided list
	 * @param lst - list to append to
	 * @param dir - base directory for file
	 * @param fname - file number
	 * @param start_time - existing start time
	 * @return new start timestamp - correspond to init file time
	 */
	private long loadInitFile(LinkedList<UpdateEvent> lst, File dir, long fname,
		long start_time) {
		File f = new File(dir, Long.toString(fname) + ".init");   /* Make file name */
		if (f.exists() == false) {     /* Not there, move on */
			return start_time;
		}
		RandomAccessFile raf = null;
		try	{
			raf = new RandomAccessFile(f, "r");
			do {
				short sig = raf.readShort();    /* Get record signature */
				short len = raf.readShort();    /* Get record length */
				if (sig != REC_SIG)
				{    /* If bad signature, quit (bad file) */
					break;
				}
				byte[] buf = new byte[len];
				raf.read(buf);              /* Read record */
				ByteArrayInputStream bais = new ByteArrayInputStream(buf);
				UpdateEvent evt = new UpdateEvent();
				evt.readFromStream(bais);   /* Load event */
				lst.add(evt);           /* Add to list */
				start_time = evt.timestamp;	/* Update to time from init */
			} while (true);
		}
		catch (IOException iox)
		{
		}
		finally
		{
			if (raf != null)
			{
				try { raf.close(); }
				catch (IOException iox) { };
				raf = null;
			}
		}
		return start_time;
	}
}
