package com.rfcode.ranger.container;

import java.util.Collection;

/**
 * Generic container interface. Very similar to Collection interface but current one is designed to be used together
 * with {@link Containers} class as generic access interface to specific container.
 * <p>
 * Supports only to add, remove and view all actions against specific in-memory container implementation
 */
public interface Container<T> {
    /**
     * Add new element to container. In case duplicates behaviour depends on specific implementation
     *
     * @param v new element
     */
    void put(T v);

    /**
     * Remove element from container. If no element does nothing
     *
     * @param v existing element
     */
    void remove(T v);

    /**
     * Remove all elements from container
     */
    void removeAll();

    /**
     * Check if container contains element
     *
     * @param v random element
     * @return if container contains element
     */
    boolean contains(T v);

    /**
     * Return all elements from container
     *
     * @return all elements from container
     */
    Collection<T> elements();

    /**
     * Size of current container
     *
     * @return number of elements in container
     */
    int size();
}
