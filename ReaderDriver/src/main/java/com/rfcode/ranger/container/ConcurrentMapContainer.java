package com.rfcode.ranger.container;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Implementation of {@link Container} interface based on {@link ConcurrentHashMap}
 */
class ConcurrentMapContainer<V> implements Container<V> {
    private static final Object NOTHING = new Object();
    private ConcurrentMap<V, Object> elements = new ConcurrentHashMap();

    @Override
    public void put(V v) {
        elements.putIfAbsent(v, NOTHING);
    }

    @Override
    public void remove(V v) {
        elements.remove(v);
    }

    @Override
    public void removeAll() {
        elements.clear();
    }

    @Override
    public boolean contains(V v) {
        return elements.containsKey(v);
    }

    @Override
    public Collection<V> elements() {
        return elements.keySet();
    }

    @Override
    public int size() {
        return elements.keySet().size();
    }
}
