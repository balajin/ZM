package com.rfcode.ranger.container;

/**
 * Container holder class. Designed to separate creation of containers from containers implementation.
 * Instead of defining all containers as singletons all instances should be initiated and stored here.
 */
public final class Containers {
    private Containers() {
    }

    /**
     * Container for all tags with link to asset.
     * Based on this list we can easily filter valuable ZM updates to reduce traffic between CS and ZM.
     */
    private static final Container<String> tagsLinkedToAsset = new ConcurrentMapContainer<>();

    public static Container<String> assetTags() {
        return tagsLinkedToAsset;
    }
}
