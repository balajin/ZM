package com.rfcode.ranger;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;

import com.rfcode.ranger.container.Containers;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.InetAddress;

import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.locationrules.LocationRuleException;
import com.rfcode.drivers.locationrules.LocationRuleExceptionListener;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationLifecycleListener;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderGPSStatusListener;
import com.rfcode.drivers.readers.ReaderServiceStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagBeaconListener;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.drivers.readers.GPSStatusListener;
import com.rfcode.ranger.CommandContext.OutputSyntax;
import com.rfcode.ranger.RangerServer.User;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.extensions.MaximoV71Extension;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Arrays;

/**
 * This class implements a servlet that provides access to the command
 * interfaces implemented by the Ranger server. It also acts as the "main" entry
 * point when the server is run as a web application versus standalone.
 *
 * @author Mike Primm
 *
 */
public class ServletCommandInterface extends HttpServlet implements AsyncListener {
    static final long serialVersionUID = 0x3948392857354235L;
    private static final int LISTENER_TIMEOUT_MS = 30000; /* 30 second timeout */
    private static final int LISTENER_AGEOUT = 120; /* 120 second timeout */
    private static HashMap<String, TagListener> listeners = new HashMap<String, TagListener>();
    /* schema v2 specific args */
    public static final String TAGCREATETRIGGER = "$tagcreatetrigger";
    public static final String DEFAULTEDATTRIBS = "$defaultedattribs";
    private static HashSet<String> taggpsattribs;
    private static final String[] TAG_GPS_ATTRIBS = { "last_gpsfix", "last_latlon", "last_altitude", "last_speed", "last_course", "last_epe_horiz", "last_epe_vert",
       LocationRuleFactory.ATTRIB_GPSID, LocationRuleFactory.ATTRIB_LASTGPSID, LocationRuleFactory.ATTRIB_LASTGPSTS };
    
    private static final String TAG_EVENT_TYPE = "tag";
    /**
     * Base 64 string decode
     */
    public static String decode_b64(String s) {
        StringBuilder sb = new StringBuilder();
        int accum = 0;
        int accumcnt = 0;
        int len = s.length() & 0xFFFC;

        for (int i = 0; i < len; i++) {
            char c = s.charAt(i); /* Get character */
            int v = 0;
            if ((c >= 'A') && (c <= 'Z'))
                v = (int) c - (int) 'A';
            else if ((c >= 'a') && (c <= 'z'))
                v = (int) c - (int) 'a' + 26;
            else if ((c >= '0') && (c <= '9'))
                v = (int) c - (int) '0' + 52;
            else if (c == '+')
                v = 62;
            else if (c == '/')
                v = 63;
            else
                break;
            accum = (accum << 6) | v;
            accumcnt += 6;
            if (accumcnt >= 8) {
                sb.append((char) ((accum >> (accumcnt - 8)) & 0xFF));
                accumcnt -= 8;
            }
        }
        return sb.toString();
    }
    /**
     * Our tag lifecycle and status listener
     */
    private static class OurTagListener implements TagStatusListener {
        Object vals[] = new Object[16];
        /**
         * Tag created notification. Called after tag has been created, but
         * before its TagLinks have been added (i.e. the end of init()).
         *
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            Map<String, Object> attr = tag.getTagAttributes();
            attr.put("tagtype", tag.getTagType().getID());
            attr.put("taggroup", tag.getTagGroup().getID());
            if(cause != null)
                attr.put(TAGCREATETRIGGER, cause);
            Set<String> defs = tag.getDefaultedTagAttributes();
            if(defs != null) {
                attr.put(DEFAULTEDATTRIBS, defs);
            }
            TagListener.addEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.CREATE,
                attr, attr);
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag
         * is about to be deleted (generally due to having no active TagLinks).
         *
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
			String loc = null;
			/* If tag still has non-null location, pad in an update to clear it (helps AM logic) */
			int idx = tag.getTagType().getTagAttributeIndex(LocationRuleFactory.ATTRIB_ZONELOCATION);
			if(idx >= 0) {
				loc = (String)tag.readTagAttribute(idx);
			}
			if((loc != null) && (!loc.equals(LocationRuleFactory.ZONELOCATION_NOMATCH))) {
	            HashMap<String, Object> attrs = new HashMap<String, Object>();
				Map<String, Object> fattrs = tag.getTagAttributes();
				fattrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, LocationRuleFactory.ZONELOCATION_NOMATCH);
				attrs.put(LocationRuleFactory.ATTRIB_ZONELOCATION, LocationRuleFactory.ZONELOCATION_NOMATCH);
	            TagListener.addToUnkEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.UPDATE,
	                attrs, fattrs);
			}
            TagListener.addEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.DELETE,
                null, null);
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         *
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than
         *            the number of atributes for the given tag). Ordered to
         *            match attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            boolean show_lastgps = false;
            boolean offline_location_change = false;
            String aids[] = tag.getTagGroup().getTagType().getTagAttributes();
            if (aids.length > vals.length)
                vals = new Object[aids.length];

            tag.readTagAttributes(vals, 0, vals.length);

            boolean not_just_confidence = false;
            HashMap<String, Object> attrs = null;
            for (int i = 0; i < aids.length; i++) {
                if (vals[i] != oldval[i]) {
                    /* If not just a confidence change */
                    if (!aids[i]
                        .equals(LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE)) {
                        not_just_confidence = true;
                        /* Add to value update list */
                        if (attrs == null)
                            attrs = new HashMap<String, Object>();
                        if(vals[i] instanceof CustomFieldValue) {
                        	CustomFieldValue cfv = (CustomFieldValue) vals[i];
                        	CustomFieldValue oldcfv  = (CustomFieldValue) oldval[i];
                        	// If old value defined, and not same field ID, update to null
                        	if ((oldcfv !=  null) && (oldcfv.getField().equals(cfv.getField()) == false)) {
                        		attrs.put(oldcfv.getField(), null);
                        	}
                        	attrs.put(cfv.getField(), cfv.getValue());                        	
                        }
                        // If replacing old custom value with null, set old field to null
                        else if ((vals[i] == null) && (oldval[i] instanceof CustomFieldValue)) {
                        	CustomFieldValue oldcfv  = (CustomFieldValue) oldval[i];
                        	attrs.put(oldcfv.getField(), null);
                        }
                        else {
                        	attrs.put(aids[i], vals[i]);
                        }
                        /* If GPSID change, and changed to no-match, and we have lastGPSID */
                        if(aids[i].equals(LocationRuleFactory.ATTRIB_GPSID) &&
                            vals[i].equals(LocationRuleFactory.GPSID_NOMATCH)) {
                            show_lastgps = true;
                        }
                    }
                    if(aids[i].equals(LocationRuleFactory.ATTRIB_ZONELOCATION) && ((vals[i] == null) || vals[i].equals("")) && (tag.getTagLinkCount() == 0)) {
                        offline_location_change = true;
                    }
                }
            }
            /* Ignore just confidence changes */
            if (!not_just_confidence) {
                return;
            }
           
            GPSData gpsd = null;
            HashMap<String, Object> fattrs = new HashMap<String, Object>();
            for(int i = 0; i < aids.length; i++) {
                fattrs.put(aids[i], vals[i]);
                if(show_lastgps && aids[i].equals(LocationRuleFactory.ATTRIB_LASTGPSID) && (vals[i] instanceof String)) {
                    GPS gps = ReaderEntityDirectory.findGPS((String)vals[i]);
                    if(gps != null) {
                        gpsd = gps.getLastGPSDataForTag(tag.getTagGUID());
                    }
                }
            }
            if(gpsd != null) {
                if(gpsd.getGPSFix() != null) {
    				attrs.put("last_gpsfix", gpsd.getGPSFix().toString());
    				fattrs.put("last_gpsfix", gpsd.getGPSFix().toString());
                }
				ArrayList<Double> latlon = null;
				if((gpsd.getLatitude() != null) && (gpsd.getLongitude() != null)) {
					latlon = new ArrayList<Double>();
					latlon.add(gpsd.getLatitude());
					latlon.add(gpsd.getLongitude());
    	            attrs.put("last_latlon", latlon);
    	            fattrs.put("last_latlon", latlon);
				}
                if(gpsd.getAltitude() != null) {
    	            attrs.put("last_altitude", gpsd.getAltitude());
    	            fattrs.put("last_altitude", gpsd.getAltitude());
                }
                if(gpsd.getSpeed() != null) {
    	            attrs.put("last_speed", gpsd.getSpeed());
    	            fattrs.put("last_speed", gpsd.getSpeed());
                }
                if(gpsd.getCourse() != null) {
    	            attrs.put("last_course", gpsd.getCourse());
    	            fattrs.put("last_course", gpsd.getCourse());
                }
                if(gpsd.getEPEHorizontal() != null) {
    	            attrs.put("last_epe_horiz", gpsd.getEPEHorizontal());
    	            fattrs.put("last_epe_horiz", gpsd.getEPEHorizontal());
                }
                if(gpsd.getEPEVertical() != null) {
    	            attrs.put("last_epe_vert", gpsd.getEPEVertical());
    	            fattrs.put("last_epe_vert", gpsd.getEPEVertical());
                }
            }
            if(offline_location_change)
                TagListener.addToUnkEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.UPDATE,
                    attrs, fattrs);
            else
                TagListener.addEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.UPDATE,
                    attrs, fattrs);
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         *
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, Object[] attrvals) {
            HashMap<String, Object> attrs = new HashMap<String,Object>();

            attrs.put("triggerid", cmd);
            for (int i = 0; i < attrids.length; i++) {
                attrs.put(attrids[i], attrvals[i]);
            }
            TagListener.addEvent(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.TRIGGER,
                attrs, attrs);
        }

        /**
         * TagLink added notificiation. Called after taglink has been added to
         * tag.
         *
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {

        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed
         * from tag.
         *
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {

        }
    }
    /**
     * Our location lifecycle and status listener
     */
    private static class OurLocationListener implements
        LocationLifecycleListener {
        /**
         * Location created notification.
         *
         * @param loc -
         *            location being created
         */
        public void locationLifecycleCreate(Location loc) {
            TagListener.addEvent(loc.getID(), "location", EventType.CREATE, loc
                .getAttributes(), null);
        }
        /**
         * Location deleted notification.
         *
         * @param loc -
         *            location being deleted
         */
        public void locationLifecycleDelete(Location loc) {
            TagListener.addEvent(loc.getID(), "location", EventType.DELETE,
                null, null);
        }
        /**
         * Location attributes changed notification
         *
         * @param loc -
         *            location with change attributes
         * @param oldval -
         *            previous values
         */
        public void locationLifecycleAttributeChange(Location loc,
            Map<String, Object> oldval) {
            /* Get copy of current attributes */
            Map<String, Object> attr = new HashMap<String,Object>(loc.getAttributes());

            /* Loop through the old values, looking for changes */
            for (Map.Entry<String, Object> ent : oldval.entrySet()) {
                Object v = attr.get(ent.getKey());
                if (v == null) { /* If not defined, its been deleted */
                    attr.put(ent.getKey(), null);
                } else if (v.equals(ent.getValue())) { /* If same */
                    attr.remove(ent.getKey()); /* Drop it */
                }
            }
            TagListener.addEvent(loc.getID(), "location", EventType.UPDATE,
                attr, new HashMap<String,Object>(loc.getAttributes()));
        }
    }
	/**
     * Make attribute set for reader event (most are full status)
	 */
	private static HashMap<String, Object> getReaderAttribs(Reader reader) {
        HashMap<String, Object> attr = new HashMap<String, Object>();
        attr.put("enabled", reader.getReaderEnabled());
        attr.put("state", reader.getReaderStatus());
		InetAddress addr = reader.getRemoteAddress();
		attr.put("remoteaddr", (addr != null)?addr.getHostAddress():null);
        attr.put("fwversion", reader.getReaderFirmwareVersion());
        attr.put("fwfamily", reader.getReaderFirmwareFamily());
        for(ReaderChannel c : reader.getChannels()) {
            attr.put("noise" + c.getRelativeChannelID(), c.getChannelNoiseLevel());
            if(c.getChannelEventsPerSec() >= 0)
                attr.put("evtpersec" + c.getRelativeChannelID(), c.getChannelEventsPerSec());
        }
        if(reader.getUsedTagCapacityPercent() >= 0)
            attr.put("tagcapused", reader.getUsedTagCapacityPercent());
		GPS gps = reader.getGPS();
		if((gps != null) && (gps.getGPSData() != null)) {
			attr.put("gpsfix", gps.getGPSData().getGPSFix().toString());
		}
        else {
			attr.put("gpsfix", null);
        }
        /* Get reader startup time, if available */
        if(reader.getReaderStartupTime() > 0) {
            attr.put("startuptime", Long.valueOf(reader.getReaderStartupTime()));
        }
        attr.put("encrypted", Boolean.valueOf(reader.getReaderConnectionEncrypted()));
        attr.put("label", reader.getLabel());
        // Add other reader attributes
        reader.addReaderStatusAttributes(attr);

		return attr;
	}

    /* Handler for reader status changes */
    private static class OurReaderListener implements ReaderServiceStatusListener,
        ReaderLifecycleListener, ReaderGPSStatusListener {
        /**
         * Callback for reporting reader status change. Handler MUST process
         * immediately and without blocking.
         *
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            HashMap<String, Object> attr = getReaderAttribs(reader);
            TagListener.addEvent(reader.getID(), "reader", EventType.UPDATE,
                attr, attr);
        }
        /**
         * Report for service message
         * @param reader - reader reporting message
         * @param svcid - service ID
         * @param svcmsg - service message
         */
        public void reportServiceMessage(Reader reader, String svcid, String svcmsg) {
            HashMap<String, Object> attr = new HashMap<String, Object>();
            attr.put("svcid", svcid);
            attr.put("svcmsg", svcmsg);
            TagListener.addEvent(reader.getID(), "reader", EventType.SVC,
                attr, attr);
        }
        /**
         * Reader creation notification - called when new reader object creation
         * completed (at the end of its init() call).
         *
         * @param reader -
         *            reader object created
         */
        public void readerLifecycleCreate(Reader reader) {
            HashMap<String, Object> attr = getReaderAttribs(reader);
            TagListener.addEvent(reader.getID(), "reader", EventType.CREATE,
                attr, attr);
        }
        /**
         * Reader deletion notification - called at start of reader's cleanup()
         * method, when reader is about to be deleted.
         *
         * @param reader -
         *            reader object being deleted
         */
        public void readerLifecycleDelete(Reader reader) {
            TagListener.addEvent(reader.getID(), "reader", EventType.DELETE,
                null, null);
        }
        /**
         * Reader enable change notification - called when reader has just
         * changed its enabled state (after the setReaderEnabled() method has
         * been called).
         *
         * @param reader -
         *            reader object being enabled or disabled
         * @param enable -
         *            true if now enabled, false if now disabled
         */
        public void readerLifecycleEnable(Reader reader, boolean enable) {
            HashMap<String, Object> attr = getReaderAttribs(reader);
            TagListener.addEvent(reader.getID(), "reader", EventType.UPDATE,
                attr, attr);
        }
	    /**
	     * Callback for reporing reader GPS status change. Handler MUST process
	     * immediately and without blocking.
	     * 
	     * @param reader -
	     *            reader reporting change
	     * @param newstatus -
	     *            new GPS fix status for reader's GPS
	     * @param oldstatus -
	     *            previous GPS fix status for reader's GPS
	     */
	    public void readerGPSFixStatusChanged(Reader reader, GPSData.Fix newstatus,
	        GPSData.Fix oldstatus) {
            HashMap<String, Object> attr = getReaderAttribs(reader);
            TagListener.addEvent(reader.getID(), "reader", EventType.UPDATE,
                attr, attr);
		}
    }

    /* Handler for rule exception changes */
    private static class OurRuleExceptionListener implements LocationRuleExceptionListener {
    	public static HashMap<String,Object> getExceptionAttribs(LocationRuleException lrx) {
    		HashMap<String,Object> attr = new HashMap<String,Object>();
    		attr.put("type", lrx.getExceptionType());
    		attr.put("rule-id", lrx.getRuleWithException());
    		attr.put("start-time", lrx.getExceptionTimestamp());
    		attr.put("description", lrx.getDescription());
    		String[] assoc = lrx.getRulesRelatedToException();
    		if (assoc != null) {
    			List<String> assoclist = new ArrayList<String>();
    			for(String id : assoc) {
    				assoclist.add(id);
    			}
        		attr.put("associated-rule-ids", assoclist);
    		}
    		return attr;
    	}
    	
		@Override
		public void locationRuleExceptionCreated(LocationRuleException lrx) {
            HashMap<String, Object> attr = getExceptionAttribs(lrx);
            TagListener.addEvent(lrx.getID(), "rule-except", EventType.CREATE,
                attr, attr);
		}
		@Override
		public void locationRuleExceptionUpdated(LocationRuleException lrx) {
			HashMap<String, Object> attr = getExceptionAttribs(lrx);
            TagListener.addEvent(lrx.getID(), "rule-except", EventType.UPDATE,
                attr, attr);
		}
		@Override
		public void locationRuleExceptionResolved(LocationRuleException lrx) {
			HashMap<String, Object> attr = getExceptionAttribs(lrx);
            TagListener.addEvent(lrx.getID(), "rule-except", EventType.DELETE,
                attr, attr);
		}
    }

    /* Handler for GPS status changes */
    private static class OurGPSListener implements GPSStatusListener {
		static HashMap<String,Object> getGPSAttribs(GPS gps) {
            HashMap<String, Object> attr = new HashMap<String, Object>();
			GPSData gpsd = gps.getGPSData();
			if(gpsd != null) {
				attr.put("gpsfix", gpsd.getGPSFix().toString());
				ArrayList<Double> latlon = null;
				if((gpsd.getLatitude() != null) && (gpsd.getLongitude() != null)) {
					latlon = new ArrayList<Double>();
					latlon.add(gpsd.getLatitude());
					latlon.add(gpsd.getLongitude());
				}
	            attr.put("latlon", latlon);
	            attr.put("altitude", gpsd.getAltitude());
	            attr.put("speed", gpsd.getSpeed());
	            attr.put("course", gpsd.getCourse());
	            attr.put("epe_horiz", gpsd.getEPEHorizontal());
	            attr.put("epe_vert", gpsd.getEPEVertical());
			}
			if(gps.getReaderID() != null)
				attr.put("reader", gps.getReaderID());
			return attr;
		}
		/**
		 * GPS position data changed
	     * @param gps - GPS with new position data (access using gps.getGPSData())
		 * @param prev_data - previous GPS data for the GPS (value just replaced)
		 */
		public void gpsDataChanged(GPS gps, GPSData prev_data) {
            HashMap<String, Object> attr = getGPSAttribs(gps);

            TagListener.addEvent(gps.getID(), "gps", EventType.UPDATE,
                attr, attr);
		}
	    /**
    	 * GPS created notification. Called after GPS has been created. Any initial
	     * attributes will have been added by the time this is called.
	     * 
	     * @param gps -
	     *            GPS being created
	     */
    	public void gpsLifecycleCreate(GPS gps) {
            HashMap<String, Object> attr = getGPSAttribs(gps);

            TagListener.addEvent(gps.getID(), "gps", EventType.CREATE,
                attr, attr);
		}
	    /**
	     * GPS deleted notification. Called at start of GPS cleanup(), when GPS is
	     * about to be deleted.
	     * 
	     * @param gps -
	     *            GPS being deleted
	     */
    	public void gpsLifecycleDelete(GPS gps) {
            TagListener.addEvent(gps.getID(), "gps", EventType.DELETE,
                null, null);
		}
    }

    private class OurContext implements CommandContext, Runnable {
        String cmd;
        private static final int SBLEN_THRESH = 1024;
        StringBuilder sb = new StringBuilder(SBLEN_THRESH);
        ArrayList<String> slist = new ArrayList<String>();
        User user;
        boolean is_admin;
        boolean is_config_view;
        String contenttype;
        CommandHandler ch;
        Map<String, String> args;
        OutputSyntax ext;
        boolean cmd_pending;
        /**
         * Send output string. Lines end with "\n" in the text (will be
         * translated by context if needed)
         *
         * @param s -
         *            message to send
         * @throws CommandException
         *             if error sending output (fatal to command)
         */
        public void sendString(String s) throws CommandException {
            if(s.indexOf('\n') < 0) {   /* No CR?  Just append all */
                sb.append(s);
            }
            else {
                int len = s.length();
                boolean last_cr = false;
                for (int i = 0; i < len; i++) {
                    char c = s.charAt(i);
                    if ((c == '\n') && (!last_cr)) { /* \n not after \r */
                        sb.append('\r');
                    }
                    last_cr = (c == '\r');
                    sb.append(c);
                }
            }
            if(sb.length() > SBLEN_THRESH) {
                slist.add(sb.toString());
                sb.setLength(0);
            }
        }
        /**
         * Make string version of given object value for output
         *
         * @param o -
         *            value
         * @return String representation
         */
        public String makeString(Object o) {
            if (o == null)
                return RangerServer.NULL_VAL;
            if (o instanceof String) {
                return "\"" + o.toString() + "\"";
            } else if (o instanceof ReaderEntity) {
                return ((ReaderEntity) o).getID();
            } else if (o instanceof Set) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                for (Object oinst : (Set<?>) o) {
                    if (!first)
                        sb.append(',');
                    sb.append(makeString(oinst));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
            } else if (o instanceof Map) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                Map<?,?> omap = (Map<?,?>) o;
                for (Object e : omap.entrySet()) {
                    Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                    if (!first)
                        sb.append(',');
                    sb.append(ent.getKey().toString());
                    sb.append("=");
                    sb.append(makeString(ent.getValue()));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
			} else if (o instanceof Number) {
				return RangerServer.formatNumber(o);
            } else
                return o.toString();
        }
        /**
         * Report command error - do not call sendString() after this. Do not
         * call once sendString() has been used.
         *
         * @param msg -
         *            error message
         */
        public void reportError(String msg) {
            sb.append("<error>: ").append(msg).append("\n");
        }
        /**
         * Test if admin privilege
         *
         * @return true if admin, false if not
         */
        public boolean isAdmin() {
            return is_admin;
        }
        /**
         * Test if config view privilege
         *
         * @return true if view allowed, false if not
         */
        public boolean isConfigView() {
            return is_config_view || is_admin;
        }
        /**
         * Get our SessionFactory
         *
         * @return factory
         */
        public SessionFactory getSessionFactory() {
            return RangerServer.getSessionFactory();
        }
        /**
         * Get logger
         *
         * @return logger
         */
        public Logger getLogger() {
            return RangerServer.getSessionFactory().getLogger();
        }
        /**
         * Get my user
         *
         * @return user, or null if none
         */
        public User getUser() {
            return user;
        }
        /**
         * Request session termination
         */
        public void requestSessionTermination() {

        }
        /**
         * Enable/disable broadcasts on session
         *
         * @param enab -
         *            true=allow broadcasts
         */
        public void enableBroadcasts(boolean enab) {

        }
        /**
         * Set content-type for response - default is text/plain
         *
         * @param ct -
         *            MIME content type
         */
        public void setContentType(String ct) {
            contenttype = ct;
        }
        /**
         * Use web syntax?
         *
         * @return true if web-style, false if not
         */
        public boolean useWebSyntax() {
            return true;
        }
        /**
         * Start object using requested encoding
         *
         * @param sb -
         *            string builder to append to
         * @param type -
         *            object type
         * @param id -
         *            object id
         * @param is_first -
         *            true if first, false if not
         */
        public StringBuilder appendStartObject(StringBuilder sb, String type,
            String id, boolean is_first) {
            return RangerServer.appendStartObject(sb, type, id, is_first, this);
        }
        /**
         * Add encoded attribute value pair using requested encoding (text,
         * json, xml)
         *
         * @param sb -
         *            string builder to append to
         * @param id -
         *            attribute ID
         * @param val -
         *            attribute value
         * @param is_first -
         *            true if first, false otherwise
         */
        public StringBuilder appendAttrib(StringBuilder sb, String id,
            Object val, boolean is_first) {
            return RangerServer.appendAttrib(sb, id, val, is_first, this);
        }
        /**
         * End object using requested encoding
         *
         * @param sb -
         *            string builder to append to
         * @param type -
         *            object type
         * @param id -
         *            object id, or null if none
         */
        public StringBuilder appendEndObject(StringBuilder sb, String type,
            String id) {
            return RangerServer.appendEndObject(sb, type, id, this);
        }
        /**
         * Command type extension ( the "ext" in cmd.ext, if provided ). Used to
         * signal formatting options, such as XML encoding.
         *
         * @return ext value, or "" if not provided
         */
        public OutputSyntax getFormatExt() {
            return ext;
        }
        /**
         * Report that command processing is incomplete - processing code will call
         * the commandCompleted() method once it is complete
         */
        public void commandStillInProgress() {
            cmd_pending = true;
        }
        /**
         * Report that a command previously reported as commandStillInProgress() has
         * completed.
         */
        public void commandCompleted(CommandException cx) {
            try {
                if(cx == null) { /* Normal completion */
                    if (sb.length() == 0 && slist.isEmpty()) {
                        if (getFormatExt() == OutputSyntax.XML) {
                            sb.append("<done/>\n");
                        } else if (getFormatExt() == OutputSyntax.JSON) {
                            sb.append("{ \"__result\" : \"done\" }\n");
                        } else {
                            sb.append("<done>\n");
                        }
                    }
                }
                else {
                    sb.setLength(0);
                    if (getFormatExt() == OutputSyntax.XML) {
                        setContentType("text/xml");
                        sb.append("<error>").append(
                            RangerServer.toXMLString(cx.getMessage()));
                        sb.append("</error>\n");
                    } else if (getFormatExt() == OutputSyntax.JSON) {
                        setContentType("application/json");
                        sb.append("{ \"__result\" : \"error\" , \"__msg\" : ");
                        sb.append(RangerServer.toJSONString(cx.getMessage()));
                        sb.append(" }\n");
                    } else {
                        sb.append("<error>: " + cx.getMessage() + "\n");
                        if (cx instanceof CommandParameterException) {
                            sb.append("\nSyntax:\n");
                            /* Add syntax help */
                            String[] hlp = ch.getCommandWebSyntaxHelp(cmd);
                            for (String s : hlp) {
                                sb.append("  http://<baseurl>/").append(cmd);
                                if (s.length() > 0)
                                    sb.append("?");
                                sb.append(s);
                                sb.append("\n");
                            }
                        }
                    }
                } 
            } finally {
                synchronized (this) {
                    notifyAll();
                }
            }
        }
        /**
         * Method to be run when action is executed by ranger processor thread
         */
        public void run() {
            try {
                cmd_pending = false;
                ch.invokeCommand(cmd, this, args); /* Invoke command */
                if(!cmd_pending)
                    commandCompleted(null);
            } catch (CommandException cx) {
                commandCompleted(cx);
            }
        }
    }
    private static class TagComparator implements Comparator<Tag> {
        private String srtid;
        TagComparator(String sortid) {
            if(sortid == null)
                srtid = "id";
            else
                srtid = sortid;
        }

        public int compare(Tag t1, Tag t2) {
            /* Same tag - return equals */
            if(t1.getTagGUID().equals(t2.getTagGUID()))
                return 0;
            if(!srtid.equals("id")) {
                int idx1 = t1.getTagType().getTagAttributeIndex(srtid);
                int idx2 = t2.getTagType().getTagAttributeIndex(srtid);
                Object v1 = (idx1 == -1)?null:t1.readTagAttribute(idx1);
                Object v2 = (idx2 == -1)?null:t2.readTagAttribute(idx2);

                if(v1 == null) {    /* Not defined for T1 */
                    if(v2 != null) { /* Defined for T2 */
                        return 1;   /* null is after defined values */
                    }
                    /* Both null : equals */
                }
                else if(v2 == null) {   /* Not defined for T2 */
                    return -1;      /* null is after defined values */
                }
                else {              /* Both not null */
                    if(v1 instanceof Comparable) {
                        @SuppressWarnings({ "unchecked", "rawtypes" })
						int rc = ((Comparable)v1).compareTo(v2);
                        if(rc != 0)
                            return rc;
                    }
                }
            }
            return t1.getTagGUID().compareTo(t2.getTagGUID());
        }

        public boolean equals(Object o) {
            if((o != null) && (o instanceof TagComparator)) {
                TagComparator tc = (TagComparator)o;
                return tc.srtid.equals(srtid);
            }
            return false;
        }
    }
    /* Action for generating sorted, windowed GUID set, based on given restrictions */
    private static class GetGUIDSet implements Runnable {
        /* Results : ordered hash set of GUIDs */
        LinkedHashSet<String> guidset = new LinkedHashSet<String>();
        int tot_set = 0;    /* Total set size (before window via start_idx, row_cnt) */
        /* Sorting attribute (""=just GUID) */
        String sortattrib;
        /* Ascending? */
        boolean ascending;
        /* Location restriction? */
        String loc_req;
        /* Start index */
        int start_idx;
        /* Max row count */
        int row_cnt;
        /* attribute restriction? */
        String attrib;
        /* attribute value restriction? */
        String value;

        /* Matching tag group ID */
        String groupid;

        public void run() {
            try {
                HashSet<Tag> tags = new HashSet<Tag>();
                Set<String> locs = null;

                if(loc_req != null) {
                    Location loc = ReaderEntityDirectory.findLocation(loc_req);
                    if(loc != null) {
                        List<Location> cloc = loc.getChildrenRecursive();
                        locs = new HashSet<String>();
                        locs.add(loc_req);
                        for(Location l : cloc) {
                            locs.add(l.getID());
                        }
                    }
                    else if ("".equals(loc_req)) { /* tags without a location */
                        locs = new HashSet<String>();
                        locs.add("");
                    }
                    else {  /* Bad location - nothing matches */
                        return;
                    }
                }

                /* Loop through tag groups, and add all matching tags to set */
                for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                    if (groupid == null || groupid.equals(tg.getID())) {
                        int attrib_idx = -1;
                        if (attrib != null) {
                            attrib_idx = tg.getTagType().getTagAttributeIndex(attrib);
                        }
                        if(locs == null) {   /* No restriction, add all of em */
                            if (attrib == null) {
                                tags.addAll(tg.getTags().values());
                            }
                            else if (attrib_idx != -1) { /* Attribute restriction */
                                for (Map.Entry<String, Tag> te : tg.getTags().entrySet()) {
                                    Tag tag = te.getValue();
                                    Object av = tag.readTagAttribute(attrib_idx);
                                    if (av != null && av.toString().equals(value)) {
                                        tags.add(tag);
                                    }
                                }
                            }
                        }
                        else {
                            int locidx = tg.getTagType().getTagAttributeIndex(
                                LocationRuleFactory.ATTRIB_ZONELOCATION);
                            for (Map.Entry<String, Tag> te : tg.getTags().entrySet()) {
                                Tag tag = te.getValue();
                                String lv = (String)tag.readTagAttribute(locidx);
                                if((lv != null) && locs.contains(lv)) {
                                    if (attrib == null) {
                                    tags.add(tag);
                                }
                                    else if (attrib_idx != -1) { /* Attribute restriction */
                                        Object av = tag.readTagAttribute(attrib_idx);
                                        if (av != null && av.toString().equals(value)) {
                                            tags.add(tag);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                /* Make array of tags */
                Tag[] ta = new Tag[tags.size()];
                tags.toArray(ta);
                if(ta.length > 1) {
                    /* Build comparator */
                    TagComparator tc = new TagComparator(sortattrib);
                    /* Sort the array */
                    Arrays.sort(ta, tc);
                }
                tot_set = ta.length;    /* Remember total set size */
                if((row_cnt < 0) || (row_cnt > ta.length))
                    row_cnt = ta.length;
                if(start_idx < 0)
                    start_idx = 0;
                /* If ascending */
                if(ascending) {
                    for(int i = start_idx; (i < ta.length) && (i < (row_cnt + start_idx)); i++) {
                        guidset.add(ta[i].getTagGUID());
                    }
                }
                else {
                    for(int i = start_idx; (i < ta.length) && (i < (row_cnt + start_idx)); i++) {
                        guidset.add(ta[ta.length - 1 - i].getTagGUID());
                    }
                }
            } finally {
                synchronized (this) {
                    notifyAll();
                }
            }

        }

    }

    /* Action for enqueuing initial state for updates channel */
    private static class GetInitialUpdates implements Runnable {
        TagListener listener; /* Listener needing initial update set */
        /**
         * Method to be run when action is executed by ranger processor thread
         */
        public void run() {
            Map<String, Object> attr;
            try {
                /* Reset existing update queue, if any */
                if (listener.events != null) {
                    listener.events.clear();
                    listener.events = null;
                }
                listener.init_pending = false;
                /* Add init-start event */
                long now = System.currentTimeMillis();
                /* If total matching count, report it */
                attr = null;
                if(listener.getTotalMatching() != null) {   /* If we have a matching count */
                    attr = new HashMap<String,Object>();    /* Make attribute map for it */
                    attr.put("totalcnt", listener.getTotalMatching());
                }
                listener.addEventSingle(null,
                        EventType.INITSTART.getTypeStr(), EventType.INITSTART,
                        attr, null, now);
                /* Loop through readers, and enqueue adds */
                for (Reader r : ReaderEntityDirectory.getReaders()) {
					attr = new HashMap<String,Object>(r.getReaderAttributes());
					attr.putAll(getReaderAttribs(r));
                    listener.addEventSingle(r.getID(), "reader", EventType.CREATE,
                        attr, attr, now);
                }
                /* Loop through locations and enqueue adds */
                for (Location l : ReaderEntityDirectory.getLocations()) {
                    attr = l.getAttributes();
                    listener.addEventSingle(l.getID(), "location",
                        EventType.CREATE, attr, attr, now);
                }
                /* Loop through tags and enqueue adds */
                if(listener.getGUIDSet() != null) {
                    for(String tagid : listener.getGUIDSet()) {
                        Tag tag = TagGroup.findTagInAnyGroup(tagid);
                        if (tag != null) {
                            attr = new HashMap<String,Object>(tag.getTagAttributes());
                            attr.put("tagtype", tag.getTagType().getID());
                            attr.put("taggroup", tag.getTagGroup().getID());
                            Set<String> defs = tag.getDefaultedTagAttributes();
                            if(defs != null) {
                                attr.put(DEFAULTEDATTRIBS, defs);
                            }
                            listener.addEventSingle(tag.getTagGUID(), TAG_EVENT_TYPE,
                                EventType.CREATE, attr, attr, now);
                        }
                    }
                }
                else {
                    for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                        for (Map.Entry<String, Tag> te : tg.getTags().entrySet()) {
                            Tag tag = te.getValue();
                            attr = new HashMap<String,Object>(tag.getTagAttributes());
                            attr.put("tagtype", tag.getTagType().getID());
                            attr.put("taggroup", tg.getID());
                            Set<String> defs = tag.getDefaultedTagAttributes();
                            if(defs != null) {
                                attr.put(DEFAULTEDATTRIBS, defs);
                            }
                            listener.addEventSingle(tag.getTagGUID(), TAG_EVENT_TYPE,
                                EventType.CREATE, attr, attr, now);
                        }
                    }
                }
				/* Loop through GPS instances, and add create events */
				for(GPS gps : ReaderEntityDirectory.getGPSs()) {
					attr = OurGPSListener.getGPSAttribs(gps);
                    listener.addEventSingle(gps.getID(), "gps",
                        EventType.CREATE, attr, attr, now);
				}
				/* Loop through the rule exceptions */
				for (LocationRuleException exc : ReaderEntityDirectory.getLocationRuleExceptions()) {
					attr = OurRuleExceptionListener.getExceptionAttribs(exc);
                    listener.addEventSingle(exc.getID(), "rule-except",
                            EventType.CREATE, attr, attr, now);
				}
                /* Add init-end event */
                listener.addEventSingle(null,
                    EventType.INITEND.getTypeStr(), EventType.INITEND, null, null, now);
            } finally {
                synchronized (this) {
                    notifyAll();
                }
            }
        }
    }
    /* Action for enqueuing initial state for updates channel */
    private static class FreshenTags implements Runnable {
        TagListener listener; /* Listener needing freshened tags */
        String[] ids; /* IDs to be freshened */
        /**
         * Method to be run when action is executed by ranger processor thread
         */
        public void run() {
            Map<String, Object> attr;
            try {
                long now = System.currentTimeMillis();
                /* Loop through tags and enqueue adds */
                for(String tagid : ids) {
                    Tag tag = TagGroup.findTagInAnyGroup(tagid);
                    if (tag != null) {
                        attr = new HashMap<String,Object>(tag.getTagAttributes());
                        attr.put("tagtype", tag.getTagType().getID());
                        attr.put("taggroup", tag.getTagGroup().getID());
                        listener.addEventSingle(tag.getTagGUID(), TAG_EVENT_TYPE,
                            EventType.CREATE, attr, attr, now);
                    }
                    else {
                    	listener.addEventSingle(tagid, TAG_EVENT_TYPE, EventType.DELETE,
                                null, null, now);
                    }
                }
            } finally {
                synchronized (this) {
                    notifyAll();
                }
            }
        }
    }

    /**
     * Init for servlet - gets things running
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        /* Load version info */
        InputStream appinfo = config.getServletContext().getResourceAsStream(
            "/WEB-INF/VersionInfo");
        if (appinfo != null)
            RangerServer.loadVersion(appinfo);
        /* Initialize tag GPS attribute list */
        taggpsattribs = new HashSet<String>();
        for(String id : TAG_GPS_ATTRIBS) {
            taggpsattribs.add(id);
        }
        /* Initialize the ranger server */
        try {
            Properties cfg = RangerServer.getConfig();
            InputStream is = config.getServletContext().getResourceAsStream(
                "/WEB-INF/zonemgr.config");
            if (is == null)
                throw new ServletException("Error finding zonemgr.config");
            try {
                cfg.load(is);
            } catch (IOException iox) {
                throw new ServletException("Error loading zonemgr.config");
            }
            RangerServer.initRanger();
            /* Add our global listener */
            AbstractTagType.addGlobalTagLifecycleListener(new OurTagListener());
            /* Add our location listener */
            LocationRuleEngine
                .addLocationLifecycleListener(new OurLocationListener());
            /* Add our reader listener */
            OurReaderListener rdrlsn = new OurReaderListener();
            RangerServer.addReaderLifecycleListener(rdrlsn);
            RangerServer.addReaderStatusListener(rdrlsn);
            /* Add our location rule exception listener */
            OurRuleExceptionListener exclsn = new OurRuleExceptionListener();
            LocationRuleEngine.addLocationRuleExceptionListener(exclsn);
			/* Add our GPS listener */
			OurGPSListener gpslsn = new OurGPSListener();
			RangerServer.addGPSStatusListener(gpslsn);

        } catch (CommandException cx) {
            throw new ServletException(cx.getMessage());
        }
    }
    private void do_export(String cmd, String fname) {
        /* Now, make context handler for config dumps */
        OurContext ctx = new OurContext();
        ctx.cmd = cmd;
        ctx.ext = OutputSyntax.TEXT;
        ctx.contenttype = "text/plain";
        ctx.is_admin = true;
        ctx.is_config_view = true;
        ctx.args = new HashMap<String, String>();
        /* Do group config export */
        File outf;
        FileWriter fw;
        ctx.ch = RangerServer.getCommandHandlers().get(cmd);
        try {
            synchronized(ctx) {
                RangerProcessor.addImmediateAction(ctx);
                ctx.wait();
            }
            outf = new File(RangerServer.getDataDir(), fname);
            fw = new FileWriter(outf);
            for(String s : ctx.slist) {
                fw.append(s);
            }
            fw.append(ctx.sb);
            fw.close();
        } catch (IOException iox) {
            RangerServer.error("Error in " + cmd);
        } catch (InterruptedException ix) {
            RangerServer.error("Interrupted in " + cmd);
		}			
    }
    /**
     * Servlet unload method - do cleanup
     */
    public void destroy() {
        /* Do group config export */
        do_export("groupexport", "groups.csv");
        /* Do reader config export */
        do_export("readerexport", "readers.csv");
        /* Do location config export */
        do_export("locexport", "locations.csv");
        /* Do rule config export */
        do_export("ruleexport", "rules.csv");

        /* Clean up ranger */
        RangerServer.termRanger();

        super.destroy();
    }

    /* ------------------------------------------------------------ */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        doGet(request, response);
    }

    /* ------------------------------------------------------------ */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String cmd;
        String filename;
        RangerServer.User usr = null;
        boolean is_config_view = true;
        boolean is_admin = true;
        /* If we've got users, do our own basic authentication */
        Map<String, RangerServer.User> users = RangerServer.getUsers();
        if (users.size() > 0) {
            String username = (String) request.getAttribute(RangerLoginModule.USERNAME_ATTRIBUTE);
            usr = users.get(username); /* Find username */
            if (usr == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
            is_admin = usr.getRole().equals(User.ROLE_ADMIN);
            is_config_view = is_admin || usr.role.equals(User.ROLE_CONFIGVIEW);
        }
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("text/plain");
        /* See what command is being requested */
        cmd = request.getPathInfo();
        if (cmd == null)
            cmd = "/help"; /* If none, treat as help request */
        cmd = cmd.substring(1);
        filename = cmd;
        /* Strip off part after .ext, if there is one */
        int idx = cmd.indexOf('.');
        OutputSyntax ext = OutputSyntax.TEXT;
        String ct = "text/plain";
        if (idx >= 0) {
            String x = cmd.substring(idx + 1);
            if (x.equals("xml")) {
                ext = OutputSyntax.XML;
                ct = "text/xml";
            } else if (x.equals("json")) {
                ext = OutputSyntax.JSON;
                ct = "application/json";
            }
            cmd = cmd.substring(0, idx);
        }
        /* Add content-disposition for export calls so browser shows save as dialog */
        if (cmd.endsWith("export")) {
            // IE doesn't save file when using SSL unless you use no-store
            // http://support.microsoft.com/kb/815313
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-store");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        }
        /* Now, make context handler */
        OurContext ctx = new OurContext();
        ctx.cmd = cmd;
        ctx.ext = ext;
        ctx.user = usr;
        ctx.contenttype = ct;
        ctx.is_admin = is_admin;
        ctx.is_config_view = is_config_view;
        /* Build map of our arguments */
        ctx.args = new HashMap<String, String>();
        Map<?,?> pm = request.getParameterMap();
        for (Object key : pm.keySet()) {
            String[] v = (String[]) pm.get(key);
            if (v.length > 0)
                ctx.args.put((String) key, v[0]);
        }
        ServletOutputStream out = response.getOutputStream();
        /* If request for tag updates, call handler */
        if (cmd.equals("tagupdates")) {
            handleTagUpdates(request, response, ct, ext, ctx);
            response.setContentType(ctx.contenttype);
            for(String s : ctx.slist)
                out.print(s);
            out.print(ctx.sb.toString());
            out.flush();
            return;
        }
        /* If request from Maximo server, call handler */
        if (cmd.equals("maximo")) {
            MaximoV71Extension.handleRequest(request, response, ctx);
            response.setContentType(ctx.contenttype);
            for(String s : ctx.slist)
                out.print(s);
            out.print(ctx.sb.toString());
            out.flush();
            return;
        }
        /* Now, try to find the corresponding command handler */
        CommandHandler ch = RangerServer.getCommandHandlers().get(cmd);
        if (ch == null) { /* If not found, error */
            response.setContentType(ct);
            if (ext == OutputSyntax.XML) {
                out.println("<error>"
                    + RangerServer.toXMLString("Invalid command ID - " + cmd)
                    + "</error>\n");
                out.flush();
            } else if (ext == OutputSyntax.JSON) {
                out.println("{ \"__result\" : \"error\" , \"__msg\" : "
                    + RangerServer.toJSONString("Invalid command ID - " + cmd)
                    + "}\n");
                out.flush();
            } else {
                out.println("<error>: Invalid command ID - " + cmd);
                out.flush();
            }
            return;
        }
        /*
         * If admin command and not admin, or read-config and not config-viewer,
         * or read-config and not command session
         */
        if ((ch.isAdminRequired() && (!is_admin))
            || (ch.isConfigReadRequired() && (!is_config_view))) {
            response.setContentType(ct);
            if (ext == OutputSyntax.XML) {
                out.println("<error>Access not allowed</error>");
            } else if (ext == OutputSyntax.JSON) {
                out
                    .println("{ \"__result\" : \"error\" , \"__msg\" : \"Access not allowed\" } ");
            } else {
                out.println("<error>: Access not allowed");
            }
            out.flush();
            return;
        }
        ctx.ch = ch;
        /* Now run on session thread */
		try {
            synchronized(ctx) {
    	        RangerProcessor.addImmediateAction(ctx);	
                ctx.wait();
            }
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
        if (ctx.contenttype == null) {
            response.setContentType("text/plain");
        } else {
            response.setContentType(ctx.contenttype);
        }
        for(String s : ctx.slist)
            out.print(s);
        out.print(ctx.sb.toString());
        out.flush();
    }

    public String getServletInfo() {
        return RangerServer.PRODUCTNAME + ", " + RangerServer.getVersion();
    }

    private static class TagListener {
        private AsyncContext asyncContext;
        private LinkedList<UpdateEvent> events;
        private HttpSession session;
        private long last_req_ts;
        private String sid; /* Session ID + "sid" parm if defined (to handle Mozilla cookie
          duplication) - key for listeners map */
        private long histsince_pending;
        private boolean init_pending;
        private LinkedHashSet<String> guids = null;
        private Integer total_matching = null;  /* If filtered guid set, total number (vs windows set) */
        private String objtype; /* if non-null, listen for only these object type events */
		private boolean inc_gps;	/* If true, listener is interested in GPS events */
		private boolean inc_rule_exc;	 /* If true, listener is interested in rule exception events */
        private boolean inc_force_unknown_loc; /* If true, listener wants location->unknown before tag offline */
        private OurTagBeaconListener beaconlistener;	// Set if beacon listening is enabled

        private class OurTagBeaconListener implements TagBeaconListener {
    		@Override
    		public void tagBeacon(Tag tag, long timestamp, long payload,
    				Reader reader, int[] ssi) {
                Map<String, Object> attr = new HashMap<String, Object>();
                attr.put("reader", reader.getID());
                if (payload >= 0)
                	attr.put("payload", payload);
                else
                	attr.put("payload", null);
                ReaderChannel[] ch = reader.getChannels();
                for (int i = 0; i < ch.length; i++) {
                	if (ssi[i] != TagLink.SSI_VALUE_NA) {
                		attr.put("ssi" + ch[i].getRelativeChannelID(), ssi[i]);
                	}
            		else {
                		attr.put("ssi" + ch[i].getRelativeChannelID(), null);
            		}
                }
                addEventSingle(tag.getTagGUID(), TAG_EVENT_TYPE, EventType.BEACON,
                        attr, attr, timestamp);
    		}
        }

        public TagListener() {
            histsince_pending = 0;
            init_pending = false;
			inc_gps = false;	/* Default to uninterested (for compatability) */
			inc_rule_exc = false; // Default is not interested
            inc_force_unknown_loc = true; /* Defaul to true (for compatibility) */
        }
        public void setRawTagEvent(boolean enable) {
        	if (enable) {
        		if (beaconlistener == null) {
        			beaconlistener = new OurTagBeaconListener();
        			AbstractTagType.addTagBeaconListener(beaconlistener);
        		}
        	}
        	else {
            	if (beaconlistener != null) {
            		AbstractTagType.removeTagBeaconListener(beaconlistener);
            		beaconlistener = null;
            	}
        	}
        }
        public void cleanup() {
        	setRawTagEvent(false);
        	session = null;
        }
        public void setAsyncContext(AsyncContext ac) {
            /* If previous one, and not us, resume it */
            if ((ac != null) && (asyncContext != null) && (ac != asyncContext)) {
                //asyncContext.setObject(Boolean.TRUE);
                asyncContext.dispatch();
                asyncContext = null;
            }
            asyncContext = ac;
        }
        public void unsetAsyncContext() {
        	asyncContext = null;
        }
        /**
         * Set GUID set, restricting which events are reported */
        public void setGUIDSet(LinkedHashSet<String> guidset, int totalsize) {
            guids = guidset;
            if(guidset != null) {
                total_matching = totalsize;
            }
            else {
                total_matching = null;
            }
        }
        public LinkedHashSet<String> getGUIDSet() {
            return guids;
        }
        public Integer getTotalMatching() {
            return total_matching;
        }

        /**
         * Returns true if the event matches this listener's interested set.
         */
        public boolean matches(UpdateEvent ev) {
            if (ev.objid == null) {
                return true; // eg. InitStart
            }
            if (guids == null) {
                if (objtype != null) {
                    return objtype.equals(ev.objtype);
                }
				/* Filter out object GPS events if we aren't interested */
				if((!inc_gps) && ("gps".equals(ev.objtype)))
					return false;
				/* Filter out rule exception events if we aren't interested */
				if((!inc_rule_exc) && ("rule-except".equals(ev.objtype)))
					return false;

                return true;
            }
            return guids.contains(ev.objid);
        }

        /**
         * Add event to this listener
         *
         * @param id -
         *            object ID
         * @param type -
         *            object type
         * @param evttype -
         *            event type
         * @param attrs -
         *            attribute map, if create or update
         * @param full_attrs -
         *            full attribute map, if update
         * @param ts -
         *            timestamp
         */
        public void addEventSingle(String id, String type, EventType evttype,
            Map<String, Object> attrs, Map<String, Object> full_attrs, long ts) {
            doAddEvent(id, type, evttype, attrs, full_attrs, this, ts, false);
        }
        /**
         * Add event to all listeners
         *
         * @param id -
         *            object ID
         * @param type -
         *            object type
         * @param evttype -
         *            event type
         * @param attrs -
         *            attribute map, if create or update
         * @param full_attrs -
         *            full attribute map, if update
         */
        public static void addEvent(String id, String type, EventType evttype,
            Map<String, Object> attrs, Map<String, Object> full_attrs) {
            doAddEvent(id, type, evttype, attrs, full_attrs, null, 0, false);
        }
        /**
         * Add event to all listeners that want force location to unknown events
         *
         * @param id -
         *            object ID
         * @param type -

         *            object type
         * @param evttype -
         *            event type
         * @param attrs -
         *            attribute map, if create or update
         * @param full_attrs -
         *            full attribute map, if update
         */
        public static void addToUnkEvent(String id, String type, EventType evttype,
            Map<String, Object> attrs, Map<String, Object> full_attrs) {
            doAddEvent(id, type, evttype, attrs, full_attrs, null, 0, true);
        }
        /**
         * Add event to listeners, either all or a specific one
         *
         * @param id -
         *            object ID
         * @param type -
         *            object type
         * @param evttype -
         *            event type
         * @param attrs -
         *            attribute map, if create or update
         * @param full_attrs -
         *            full attribute map, if update
         * @param tgt -
         *            if non-null, single listener to add event to
         * @param ts -
         *            timestamp (0=now)
         * @param to_unk - 
        *             if true, only to 'force-to-unknown' listeners
         */
        private static void doAddEvent(String id, String type,
            EventType evttype, Map<String, Object> attrs,
            Map<String, Object> full_attrs, TagListener tgt, long ts, boolean to_unk) {

            if (TAG_EVENT_TYPE.equals(type) &&
                    RangerServer.isListenAssetTagsOnly() &&
                    !Containers.assetTags().contains(id)) {
                return;
            }

            LinkedList<TagListener> invalid = null;
            UpdateEvent te = new UpdateEvent();
            te.objid = id;
            te.objtype = type;
            te.evttype = evttype;
            if(ts != 0)
                te.timestamp = ts;
            else
                te.timestamp = System.currentTimeMillis();
            te.attribs = attrs;
            if(full_attrs != null)
                te.full_attribs = full_attrs;
            else
                te.full_attribs = attrs;
            /* Report to server */
            if(tgt == null)
                RangerServer.reportEvent(te);

            synchronized (listeners) {
                if (tgt != null) { /* Single target? */
                    try { /* See if session is still valid */
                        if(tgt.session != null)
                            tgt.session.getCreationTime();
                        /* If subsession is timed-out */
                        if(te.timestamp > (tgt.last_req_ts + (LISTENER_AGEOUT*1000L))) {
                            throw new IllegalStateException();
                        }
                        /* If force-to-unknown event, and target doesn't want them skip */
                        if(to_unk && (!tgt.inc_force_unknown_loc)) {
                        }
                        /* If no guid set, or matches the set, add the event */
                        else if(tgt.matches(te)) {
                            if (tgt.events == null) {
                                tgt.events = new LinkedList<UpdateEvent>();
                            }
                            tgt.events.add(te);
                            if (tgt.asyncContext != null) {
                                tgt.asyncContext.dispatch();
                                tgt.asyncContext = null;
                            }
                        }
                    } catch (IllegalStateException isx) {
                        listeners.remove(tgt.sid);
                      	tgt.cleanup();
                    }
                } else {
                    for (Map.Entry<String, TagListener> ent : listeners.entrySet()) {
                        TagListener tl = ent.getValue();
                        try { /* See if session is still valid */
                            if(tl.session != null)
                                tl.session.getCreationTime();
                            /* If subsession is timed-out */
                            if(te.timestamp > (tl.last_req_ts + (LISTENER_AGEOUT*1000L))) {
                                throw new IllegalStateException();
                            }
                        } catch (IllegalStateException isx) {
                            /* Session is invalid - delete at end of traversal */
                            if (invalid == null)
                                invalid = new LinkedList<TagListener>();
                            invalid.add(tl);
                        }
                        /* If force-to-unknown event, and target doesn't want them skip */
                        if(to_unk && (!tl.inc_force_unknown_loc)) {
                            continue;
                        }
                        /* If listener only doing specific GUIDs,and not match, skip */
                        if(!tl.matches(te))
                            continue;
                        /* If no init pending and no history pending */
                        if((tl.init_pending == false) &&
                            (tl.histsince_pending == 0)) {
                            if (tl.events == null) {
                                tl.events = new LinkedList<UpdateEvent>();
                            }
                            tl.events.add(te);
                            if (tl.asyncContext != null) {
                                
                                tl.asyncContext.dispatch();
                                tl.asyncContext = null;
                            }
                        }
                    }
                    /* If any obsolete sessions, clean em up */
                    if (invalid != null) {
                        for (TagListener tl : invalid) {
                            listeners.remove(tl.sid);
                            tl.cleanup();
                        }
                    }
                }
            }
        }
    }
    /**
     * Report reader upconnection error
     */
    public static void reportUpConnectionError(String errid,
        String rdrid, String mac, String curmac) {
        HashMap<String,Object> attrs = new HashMap<String,Object>();
        attrs.put("rdrid", rdrid);
        attrs.put("macaddr", mac);
        attrs.put("errorid", errid);
        if(curmac != null)
            attrs.put("curmacaddr", curmac);
        TagListener.addEvent(rdrid, "upconnect",
            EventType.UPCONNECTERROR, attrs, attrs);
    }
    /* ------------------------------------------------------------ */
    private void handleTagUpdates(HttpServletRequest request,
        HttpServletResponse response, String ct, OutputSyntax ext,
        CommandContext ctx) throws ServletException, IOException {
        int max_updates = 0;
        TagListener tl;
        String sid;
        int timeout = LISTENER_TIMEOUT_MS;
        boolean do_fullupdate = false;
        int schemaver = 1;
        /* Get our session */
        HttpSession sess = request.getSession(true);
        /* Set inactive to ageout time */
        sess.setMaxInactiveInterval(LISTENER_AGEOUT);
        /* Check for session ID parameter */
        String sid_prm = request.getParameter("_sid");
        if(sid_prm != null)
            sid = sess.getId() + sid_prm;
        else
            sid = sess.getId();
        /* Get tag listener */
        synchronized (listeners) {
            tl = listeners.get(sid);
            if (tl == null) {
                tl = new TagListener();
                tl.session = sess;
                tl.sid = sid;

                /* check for type */
                String objtype = request.getParameter("_objtype");
                if (objtype != null && objtype.length() > 0) {
                    tl.objtype = objtype;
                }
                listeners.put(sid, tl);
            }
            tl.last_req_ts = System.currentTimeMillis();
        }
        /* Check for max updates parameter */
        String m_u = request.getParameter("_maxcnt");
        if (m_u != null) {
            try {
                max_updates = Integer.parseInt(m_u);
            } catch (NumberFormatException nfx) {
                max_updates = 0;
            }
        }
		/* Check for incgps=true */
		String incgps = request.getParameter("_incgps");
		if(incgps != null) {
			if(incgps.equals("true")) {
				tl.inc_gps = true;
			}
			else {
				tl.inc_gps = false;
			}
		}
        /* Check for raw beacons */
        String incbeacon = request.getParameter("_inctagbeacons");
        if (incbeacon != null) {
        	tl.setRawTagEvent(incbeacon.equals("true"));
        }

		/* Check for incruleexc=true */
		String incexc = request.getParameter("_incruleexc");
		if(incexc != null) {
			if(incexc.equals("true")) {
				tl.inc_rule_exc = true;
			}
			else {
				tl.inc_rule_exc = false;
			}
		}
		/* Check for forcetounknown=true */
		String ftu = request.getParameter("_forcetounknown");
		if(ftu != null) {
			if(ftu.equals("true")) {
				tl.inc_force_unknown_loc = true;
			}
			else {
				tl.inc_force_unknown_loc = false;
			}
		}
        /* Check for stretch transaction parameter */
        String do_stretch = request.getParameter("_stretch");
		boolean stretch = false;
        if ((do_stretch != null) && (do_stretch.equals("true"))) {
			stretch = true;
		}
        /* Check for history-since parameter */
        String hist_prm = request.getParameter("_histsince");
        long histsince = 0;
        if (hist_prm != null) {
            try {
                histsince = Long.parseLong(hist_prm);   /* Parse it */
            } catch (NumberFormatException nfx) {}
        }
        /* If history requested, set start period and clear event queue */
        if(histsince != 0) {
            tl.events = null;
            tl.histsince_pending = histsince;
        }
        /* Check for initial parameter */
        String init_prm = request.getParameter("_init");
        String sortattr = null;
        LinkedHashSet<String> guidset = null;
        if ((init_prm != null) && (init_prm.equals("true"))) {
            int tot_set = 0;
            tl.init_pending = true;
            tl.events = null;
            /* Check if we were given GUID set */
            String guids = request.getParameter("_guids");
            if(guids != null) {   /* If so, build set */
                guidset = new LinkedHashSet<String>();
                String[] ids = guids.split(",");
                for(String id : ids) {
                    if(id.length() > 0)
                        guidset.add(id);
                }
            }
            /* Else, if we got sorting attribute */
            else if((sortattr = request.getParameter("_sortattrib")) != null) {
                GetGUIDSet ggs = new GetGUIDSet();
                ggs.sortattrib = sortattr;
                ggs.ascending = true;
                ggs.start_idx = 0;
                ggs.row_cnt = -1;
                /* If _sortorder=dec, descending sort */
                if("dec".equals(request.getParameter("_sortorder"))) {
                    ggs.ascending = false;
                }
                String startrow = request.getParameter("_sortstart");
                try {
                    if(startrow != null)
                        ggs.start_idx = Integer.parseInt(startrow);
                } catch (NumberFormatException nfx) {}
                String rowcnt = request.getParameter("_sortrowcnt");
                try {
                    if(rowcnt != null)
                        ggs.row_cnt = Integer.parseInt(rowcnt);
                } catch (NumberFormatException nfx) {}
                /* Get location constraint, if defined */
                ggs.loc_req = request.getParameter("_sortlocation");
                /* Get taggroup constraint, if defined */
                ggs.groupid = request.getParameter("_groupid");
                /* Get attribute constraint, if defined */
                ggs.attrib = request.getParameter("_attribute");
                /* Get attribute value constraint, if defined */
                ggs.value = request.getParameter("_value");
                /* Now run on session thread */
				try {
	                RangerProcessor.runActionNow(ggs);
				} catch (InterruptedException ix) {
					throw new ServletException("Interrupted!");
				}
                guidset = ggs.guidset;  /* Get the resulting set */
                tot_set = ggs.tot_set;
            }
            tl.setGUIDSet(guidset, tot_set);
        }
        /* If history requested, or still reading, we answer this first */
        if(tl.histsince_pending != 0) {
            boolean first = false;
            if(histsince != 0) {
                first = true;
            }
            long now = System.currentTimeMillis();
            /* Request list */
            int max = (max_updates > 0)?max_updates:1000;
            LinkedList<UpdateEvent> lst = RangerServer.readEvents(
                tl.histsince_pending, now,
                max);
            int cnt = lst.size();   /* Check number returned */
            if(first) { /* If first request, prepend hist-start */
                UpdateEvent hstart = new UpdateEvent();
                hstart.timestamp = tl.histsince_pending;
                hstart.evttype = EventType.HISTSTART;
                lst.addFirst(hstart);  /* Put in front */
            }
            if(cnt < max) { /* If less than max, we've found end */
                /* If init is pending, do it here - prevent race on history vs init */
                if(tl.init_pending) {
                    GetInitialUpdates giu = new GetInitialUpdates();
                    giu.listener = tl;
                    /* Now run on session thread */
					try {
		                RangerProcessor.runActionNow(giu);
					} catch (InterruptedException ix) {
						throw new ServletException("Interrupted!");
					}
                    long endtime = System.currentTimeMillis();
                    /* If we got initial events, get time */
                    if((tl.events != null) && (tl.events.size() > 0)) {
                        endtime = tl.events.peekFirst().timestamp;
                    }
                    /* If any extra snuck in between list and init */
                    LinkedList<UpdateEvent> xtra = RangerServer.readEvents(
                        now, endtime, max);
                    lst.addAll(xtra);   /* Put them at the end */
                    UpdateEvent hend = new UpdateEvent();
                    hend.timestamp = endtime;
                    hend.evttype = EventType.HISTEND;
                    lst.add(hend);      /* Then the end mark */
                    if(tl.events != null) {
                        lst.addAll(tl.events);  /* Then the init stuff */
                        tl.events = null;
                    }
                }
                else {  /* Else no init, so just add history */
                    UpdateEvent hend = new UpdateEvent();
                    hend.timestamp = now;
                    hend.evttype = EventType.HISTEND;
                    lst.add(hend);      /* Then the end mark */
               }
               tl.histsince_pending = 0;   /* No longer pending */
            }
            else {  /* Else, more may be pending */
                tl.histsince_pending = lst.peekLast().timestamp;    /* Get last time */
            }
            if (tl.objtype == null) {
                if(tl.events != null)
                    tl.events.addAll(lst);
                else
                    tl.events = lst;    /* This is our event list */
            }
            else {
                if (tl.events == null) {
                    tl.events = new LinkedList<UpdateEvent>();
                }
                /* filter the events */
                for (UpdateEvent ev : lst) {
                    if (tl.matches(ev)) {
                        tl.events.add(ev);
                    }
                }
            }
        }
        /* Else, if init pending (and no pending history) */
        else if(tl.init_pending) {
            GetInitialUpdates giu = new GetInitialUpdates();
            giu.listener = tl;
            /* Now run on session thread */
			try {			
	            RangerProcessor.runActionNow(giu);
			} catch (InterruptedException ix) {
				throw new ServletException("Interrupted!");
			}
        }
        /* If specific tag request list */
        String tagids = request.getParameter("_tagrefresh");
        if(tagids != null) {
        	FreshenTags ft = new FreshenTags();
        	ft.ids = tagids.split(",");
        	ft.listener = tl;
            /* Now run on session thread */
			try {			
	            RangerProcessor.runActionNow(ft);
			} catch (InterruptedException ix) {
				throw new ServletException("Interrupted!");
			}
        }
        /* Check for fields parameter : indicates CSV format */
        String flds = request.getParameter("_fields");
        String[] fldids = null;
        if(flds != null) {
            fldids = flds.split(",");
        }
        /* If header parm provided, check value */
        boolean hdr = true;
        String hdrparm = request.getParameter("_hdr");
        if((hdrparm != null) &&
            (hdrparm.equals("false") || (hdrparm.equals("off")))) {
            hdr = false;
        }
        /* Check for timeout parameter */
        String to = request.getParameter("_timeout");
        if(to != null) {
            try {
                timeout = Integer.parseInt(to);
            } catch (NumberFormatException nfx) {
                timeout = LISTENER_TIMEOUT_MS;
            }
        }
        /* Check for _fullupd parameter */
        String fullupd_prm = request.getParameter("_fullupd");
        if ((fullupd_prm != null) && (fullupd_prm.equals("true"))) {
            do_fullupdate = true;
        }
        /* Check for _schemaver parameter */
        String schemaver_prm = request.getParameter("_schemaver");
        if(schemaver_prm != null) {
            try {
                schemaver = Integer.parseInt(schemaver_prm);
            } catch (NumberFormatException nfx) {
                schemaver = 1;
            }
        }
        LinkedList<UpdateEvent> events = null;
        synchronized (listeners) {
            /* Get tag listener */
            synchronized (listeners) {
                tl = listeners.get(sid);
                if (tl == null) {
                    tl = new TagListener();
                    tl.session = sess;
                    tl.sid = sid;
                    listeners.put(sid, tl);
                }
                tl.last_req_ts = System.currentTimeMillis();
            }

            boolean cancelled = false;
            if(timeout > 0) {
                if (tl.events == null) { /* If no pending events */
                    if(request.getDispatcherType() == DispatcherType.REQUEST) {
                    	final AsyncContext asyncContext = request.startAsync(request, response);
                    	tl.setAsyncContext(asyncContext);
                    	asyncContext.setTimeout(LISTENER_TIMEOUT_MS);
                    	asyncContext.addListener(this); /*
                                                       * Remember our request for
                                                       * restart */
                    	return;
                    }
                }
            }
            tl.unsetAsyncContext();
            /* If no max, no events, or less than max, grab em all */
            if(!cancelled) {
            	if ((max_updates <= 0) || (tl.events == null) || (max_updates >= tl.events.size())) {
            		events = tl.events;
            		tl.events = null;
            	} else {
            		events = new LinkedList<UpdateEvent>();
            		/* Pull first events from queue - maintain order */
            		for (int i = 0; i < max_updates; i++) {
            			events.add(tl.events.poll());
            		}
            	}
            	/* If we're stretching, get last event we added and keep going until new timestamp */
            	if(stretch && events != null && !events.isEmpty()) {
            		long tslast = events.getLast().timestamp;
            		UpdateEvent ue = null;
            		boolean done = true;
            		do {
            			/* Add additional events until end or new timestamp value */
            			if (tl.events != null) {
            				for(ue = tl.events.peek(); 
            						(ue != null) && (ue.timestamp == tslast); 
            						ue = tl.events.peek()) {
            					events.add(tl.events.poll());
            				}
            			}
            			/* If we emptied queue, wait a bit and make sure no other events with same timestamp are being added */
            			if(ue == null) {
            				if(done) {
            					try { listeners.wait(100); } catch (InterruptedException ix) {}
            					done = false;   /* Delay and repeat once */
            				}
            				else
            					done = true;
            			}
            			else {
            				/* we looped once and additional events do not have same timestamp */
            				done = true;
            			}
            		} while (!done);
            	}
            }
        }
        /* And send any events we do have */
        try {
            if (fldids != null) {   /* If CSV request */
                sendEventsAsCSV(response, events, ctx, fldids, hdr, do_fullupdate, schemaver);
            }
            else if (ext == OutputSyntax.TEXT)
                sendEventsAsText(response, events, ctx, do_fullupdate, schemaver, tl.inc_gps);
            else
                sendEvents(response, events, ctx, do_fullupdate, schemaver, tl.inc_gps);
        } catch (CommandException cx) {
            throw new ServletException(cx.getMessage());
        }
    }
    /* Send response as CVS */
    private static void sendEventsAsCSV(HttpServletResponse response,
        LinkedList<UpdateEvent> events, CommandContext ctx, String[] fldids,
        boolean hdr, boolean do_fullupdate, int schemaver) throws IOException,
        CommandException {
        StringBuilder sb = new StringBuilder();
        ctx.setContentType("text/plain");
        /* Now output header line - always have timestamp, event type, object ID and type */
        if(hdr) {   /* Include header line if requested */
            sb.append("timestamp,eventtype,id,type");
            for(int i = 0; i < fldids.length; i++) {
                sb.append(",").append(fldids[i]);
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        /* No events?  we're done */
        if (events == null) {
            return;
        }
        for (UpdateEvent te : events) {
            sb.append(RangerServer.encodeForCSV(te.timestamp));
            sb.append(",");
            sb.append(RangerServer.encodeForCSV(te.evttype.getTextTypeStr()));
            sb.append(",");
            sb.append(RangerServer.encodeForCSV(te.objid));
            sb.append(",");
            sb.append(RangerServer.encodeForCSV(te.objtype));
            Map<String,Object> av = (do_fullupdate?te.full_attribs:te.attribs);
            for(int i = 0; i < fldids.length; i++) {
                Object v = ((av != null)?av.get(fldids[i]):null);
                sb.append(",").append(RangerServer.encodeForCSV(v));
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
    }
    /* Send response as text */
    private static void sendEventsAsText(HttpServletResponse response,
        LinkedList<UpdateEvent> events, CommandContext ctx, boolean do_fullupdate, int schemaver, boolean inc_gps)
        throws IOException, CommandException {
        StringBuilder sb = new StringBuilder();
        ctx.setContentType("text/plain");
        if (events == null) {
            ctx.sendString("<done>\n");
            return;
        }
        for (UpdateEvent te : events) {
            sb.append(te.timestamp);
            sb.append(": ");
            sb.append(te.evttype.getTextTypeStr());
            sb.append(":");
            if (te.objid != null) {
                sb.append(" id=");
                sb.append(te.objid);
                sb.append(" type=");
                sb.append(te.objtype);
                Map<String,Object> av = (do_fullupdate?te.full_attribs:te.attribs);
                if (av != null) {
                    for (Map.Entry<String, Object> ent : av.entrySet()) {
                        /* If not at lease v2 schema, skip tag create trigger and defaultedattribs */
                        if((schemaver < 2) && (ent.getKey().equals(TAGCREATETRIGGER) ||
                            ent.getKey().equals(DEFAULTEDATTRIBS)))
                            continue;
                        /* If no gps, see if excluded attribute */
                        if((!inc_gps) && taggpsattribs.contains(ent.getKey()))
                            continue;
                        ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), false);
                    }
                }
            }
            else {  /* Handle any attributes */
                boolean first = true;
                Map<String,Object> av = (do_fullupdate?te.full_attribs:te.attribs);
                if (av != null) {
                    for (Map.Entry<String, Object> ent : av.entrySet()) {
                        /* If not at lease v2 schema, skip tag create trigger and defaultedattribs */
                        if((schemaver < 2) && (ent.getKey().equals(TAGCREATETRIGGER) ||
                            ent.getKey().equals(DEFAULTEDATTRIBS)))
                            continue;
                        /* If no gps, see if excluded attribute */
                        if((!inc_gps) && taggpsattribs.contains(ent.getKey()))
                            continue;
                        ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), first);
                        first = false;
                    }
                }
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
    }
    /* Send response */
    private static void sendEvents(HttpServletResponse response,
        LinkedList<UpdateEvent> events, CommandContext ctx, boolean do_fullupdate, int schemaver, boolean inc_gps)
        throws IOException, CommandException {
        StringBuilder sb = new StringBuilder();
        boolean is_json = (ctx.getFormatExt() == OutputSyntax.JSON);
        if (is_json)
            sb.append("[ ");
        else
            ctx.appendStartObject(sb, "update-events", null, true);
        if (events == null) {
            if (is_json)
                sb.append("]\n");
            else
                ctx.appendEndObject(sb, "update-events", null);
            ctx.sendString(sb.toString());
            return;
        }
        boolean first = true;
        for (UpdateEvent te : events) {
            ctx.appendStartObject(sb, "update-event", null, first);
            first = false;
            ctx.appendAttrib(sb, "timestamp", te.timestamp, true);
            ctx.appendAttrib(sb, "eventtype", te.evttype.getTypeStr(), false);
            if (te.objid != null) {
                ctx.appendAttrib(sb, "id", te.objid, false);
                ctx.appendAttrib(sb, "type", te.objtype, false);
                if (is_json) {
                    sb.append(",\n \"object\" : { \"id\" : \"").append(te.objid)
                            .append("\", \"attributes\" : { \n");
                } else {
                    ctx.appendStartObject(sb, te.objtype, te.objid, false);
                }
                first = false;
                boolean afirst = true;
                Map<String,Object> av = (do_fullupdate?te.full_attribs:te.attribs);
                if (av != null) {
                    for (Map.Entry<String, Object> ent : av.entrySet()) {
                        /* If not at lease v2 schema, skip tag create trigger and defaultedattribs */
                        if((schemaver < 2) && (ent.getKey().equals(TAGCREATETRIGGER) ||
                            ent.getKey().equals(DEFAULTEDATTRIBS)))
                            continue;
                        /* If no gps, see if excluded attribute */
                        if((!inc_gps) && taggpsattribs.contains(ent.getKey()))
                            continue;
                        ctx.appendAttrib(sb, ent.getKey(), ent.getValue(),
                            afirst);
                        afirst = false;
                    }
                }
                ctx.appendEndObject(sb, te.objtype, te.objid);
            }
            else {  /* Handle any attributes */
                Map<String,Object> av = (do_fullupdate?te.full_attribs:te.attribs);
                if (av != null) {
                    for (Map.Entry<String, Object> ent : av.entrySet()) {
                        /* If not at lease v2 schema, skip tag create trigger and defaultedattribs */
                        if((schemaver < 2) && (ent.getKey().equals(TAGCREATETRIGGER) ||
                            ent.getKey().equals(DEFAULTEDATTRIBS)))
                            continue;
                        /* If no gps, see if excluded attribute */
                        if((!inc_gps) && taggpsattribs.contains(ent.getKey()))
                            continue;
                        ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), false);
                    }
                }
            }
            ctx.appendEndObject(sb, "update-event", null);
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        if (is_json)
            sb.append("]\n");
        else
            ctx.appendEndObject(sb, "update-events", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Get initial state event dump
     */
    public static LinkedList<UpdateEvent> getInitUpdateEvents() {
        TagListener tl = new TagListener();
        tl.session = null;
        tl.sid = null;
        tl.last_req_ts = System.currentTimeMillis();
        /* Create request to run on session thread */
        GetInitialUpdates giu = new GetInitialUpdates();
        giu.listener = tl;
        /* Now run on session thread */
		try {
	        RangerProcessor.runActionNow(giu);
		} catch (InterruptedException ix) {
			RangerServer.error("Interrupted in getInitUpdateEvents()");
			tl.events = null;
		}
        return tl.events;
    }
	@Override
	public void onComplete(AsyncEvent evt) throws IOException {
	}
	@Override
	public void onError(AsyncEvent evt) throws IOException {
	}
	@Override
	public void onStartAsync(AsyncEvent evt) throws IOException {
	}
	@Override
	public void onTimeout(AsyncEvent evt) throws IOException {
	}
}
