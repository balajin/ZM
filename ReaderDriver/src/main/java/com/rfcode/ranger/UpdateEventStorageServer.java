package com.rfcode.ranger;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Properties;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class UpdateEventStorageServer implements ConfigUpdateListener {
    /* Property name for maximum event storage time, in hours (0=disabled) */
    public static final String EVENTSTORAGE_MAXTIME = "maxhisttime";
    public static final String EVENTSTORAGE_MAXTIME_DEFAULT = "0";
    /* Our command queue */
    private LinkedBlockingQueue<UpdateEventServerCommand> cmd_queue = 
        new LinkedBlockingQueue<UpdateEventServerCommand>();

    private ServiceThread our_thread;
    private UpdateEventStorage store;

    /**
     * Interface for server commands
     */
    public interface UpdateEventServerCommand {

    }

    /**
     * Update maxtime command
     */
    private static class UpdateMaxTimeCmd implements UpdateEventServerCommand {
        int new_maxtime;
    }
    /**
     * Update event logging command
     */
    private static class UpdateEventCmd implements UpdateEventServerCommand {
        UpdateEvent evt;
    }
    /**
     * Query history command
     */
    private static class UpdateEventHistoryCmd implements UpdateEventServerCommand {
        long start_time;    /* Events after (>) this time */
        long end_time;      /* Events at or before (<=) this time */
        int maxcnt;         /* Maximum number of events to return (suggested - may exceed if
                             * needed to keep events with identical time together */
        LinkedList<UpdateEvent>   results;    /* Output - returned results */
    }
    /**
     * Handler thread - does all the work
     */
    private class ServiceThread extends Thread {
        public ServiceThread() {
            super("UpdateEventStorage Service");
        }
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }

    /**
     * Constructor - set up initial state
     */
    public UpdateEventStorageServer(File basedir) {
        Properties cfg = RangerServer.getConfig();
        String max = cfg.getProperty(EVENTSTORAGE_MAXTIME, EVENTSTORAGE_MAXTIME_DEFAULT);
        int t = 0;
        try {
            t = Integer.parseInt(max);
        } catch (NumberFormatException nfx) {
        }
        store = new UpdateEventStorage(basedir, t);
        if(t > 0) {   /* If now active, start it */
            our_thread = new ServiceThread();
            our_thread.setDaemon(true);
            our_thread.start();
            /* Write our restart event */
            UpdateEvent evt = new UpdateEvent();
            evt.timestamp = System.currentTimeMillis();
            evt.evttype = EventType.RESTART;

            reportEvent(evt);
        }
        /* Register config update hook */
        RangerServer.addConfigUpdateListener(this);
    }

    /**
     * Main storage processing thread - all file I/O done here
     */
    private void process() {
        UpdateEventServerCommand cmd;

        try {
            while(true) {
                cmd = cmd_queue.poll(15, TimeUnit.SECONDS);
                if(cmd == null) continue;
                if(cmd instanceof UpdateMaxTimeCmd) {   /* Update max command? */
                    UpdateMaxTimeCmd c = (UpdateMaxTimeCmd)cmd;
                    store.updateAgeOut(c.new_maxtime);    /* Do update      */
                }
                else if(cmd instanceof UpdateEventCmd) {    /* New update event */
                    LinkedList<UpdateEvent> pending = new LinkedList<UpdateEvent>();
                    UpdateEventCmd c = (UpdateEventCmd)cmd;
                    pending.add(c.evt);   /* Make list with event                */
                    while(cmd_queue.peek() instanceof UpdateEventCmd) {
                        synchronized(cmd) { /* Signal done on previous cmd */
                            cmd.notify();
                        }
                        cmd = cmd_queue.poll();
                        c = (UpdateEventCmd)cmd;
                        pending.add(c.evt);
                    }
                    try {
                        store.writeEvents(pending);
                        pending.clear();
                    } catch (IOException iox) {
                        RangerServer.error("Error logging events to event store - " +
                            iox);
                    }
                }
                else if(cmd instanceof UpdateEventHistoryCmd) {
                    UpdateEventHistoryCmd c = (UpdateEventHistoryCmd)cmd;
                    c.results = store.readEvents(
                        c.start_time, c.end_time, c.maxcnt);
                }
                /* Signal done */
                synchronized(cmd) {
                    cmd.notify();
                }
            }
        } catch (InterruptedException ix) {
        }
    }

    /**
     * Configuration update notification - called once each time a set of
     * server configuration updates are saved/committed
     */
    public void configUpdateNotification() {
        Properties cfg = RangerServer.getConfig();
        String max = cfg.getProperty(EVENTSTORAGE_MAXTIME, EVENTSTORAGE_MAXTIME_DEFAULT);
        int t = 0;
        try {
            t = Integer.parseInt(max);
        } catch (NumberFormatException nfx) {
        }
        UpdateMaxTimeCmd cmd = new UpdateMaxTimeCmd();
        cmd.new_maxtime = t;
        cmd_queue.offer(cmd);   /* Post to queue */
        if((t > 0) && (our_thread == null)) {   /* If now active, but no thread, start it */
            our_thread = new ServiceThread();
            our_thread.setDaemon(true);
            our_thread.start();
        }
    }
    /**
     * Report event
     */
    public void reportEvent(UpdateEvent evt) {
        if(store.getAgeOut() > 0) {
            UpdateEventCmd c = new UpdateEventCmd();
            c.evt = evt;
            cmd_queue.offer(c);
        }
    }
    /**
     * Read history for given range
     * @param start_time - start time (events after this time)
     * @param end_time - end time (events before or equal to this time)
     * @param maxcnt - recommended max events to return (may exceed to batch events with
     *      identical times)
     */
    public LinkedList<UpdateEvent>    readEvents(long start_time, long end_time, int maxcnt) {
        UpdateEventHistoryCmd cmd = new UpdateEventHistoryCmd();
        cmd.start_time = start_time;
        cmd.end_time = end_time;
        cmd.maxcnt = maxcnt;
        if(store.getAgeOut() > 0) {
            synchronized(cmd) {
                cmd_queue.offer(cmd);
                try {
                    cmd.wait();     /* Wait for results */
                } catch (InterruptedException ix) {
                }
            }
        }
        else
            cmd.results = new LinkedList<UpdateEvent>();
        return cmd.results;
    }
}
