package com.rfcode.ranger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.Reader;
import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.locationrules.impl.AverageSSILocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.mantis2.MantisIITagType04A;
import com.rfcode.ranger.RangerServer.User;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.commands.ReaderGetHandler;
import com.rfcode.ranger.commands.ReaderCreateHandler;
import com.rfcode.ranger.commands.ReaderSetHandler;
import com.rfcode.ranger.commands.ReaderAddGroupHandler;
import com.rfcode.ranger.commands.ReaderActivateHandler;
import com.rfcode.ranger.commands.GroupCreateHandler;
import com.rfcode.ranger.commands.LocationGetHandler;
import com.rfcode.ranger.commands.LocationCreateHandler;
import com.rfcode.ranger.commands.LocationSetHandler;
import com.rfcode.ranger.commands.RuleGetHandler;
import com.rfcode.ranger.commands.RuleCreateHandler;
import com.rfcode.ranger.commands.RuleSetHandler;
import com.rfcode.ranger.commands.RuleActivateHandler;

import java.io.IOException;
import java.util.Map;

/**
 * This class implements a servlet providing an interface for bulk import of reader, location
 * and rule definitions formatted in CVS format.
 * The import is received via a multi-part/formdata encoded file.  The column names in the
 * first line of the CVS are used to control the requested operations, and must be formatted
 * as follows:
 *   reader.id - defines the ID of the reader object.  Will be created if not defined
 *   reader.type - defines the type of reader object (only used if created).  Default is "R200".
 *   reader.enabled - defines activation state to set for reader (default is true (active))
 *   reader.groups - defines list of group IDs (comma separated) to be set for the reader.  If undefined,
 *       all defines groups are added if (and only if) the reader is being newly created.
 *   reader.<attrib> - provides a value for the field <attrib> of the indicated reader:
 *       reader.id must be defined in order for this to work
 *   location.<N>.id - defines the ID of the Nth object in the location path for the given
 *       position.  N=1 is location corresponding to reader, N=2 is that location's parent, etc.
 *   location.<N>.<attrib> - provides a value for field <attrib> in the location identified
 *       by location.<N>.id (which must be defined).
 *   rule.id - defines the ID of a location rule relating the reader's channels to the
 *       location.1.id location (by default, given reader.id and location.1.id are defined).
 *       If not provided, but reader.id and location.1.id are defined, rule.id is asssumed to
 *       be "<reader.id>_<location.1.id>_rule".
 *   rule.type - defines the rule type.  If undefined, "AverageSSIRule" is assumed.
 *   rule.enabled - defines the rule activation state.  Default is "true" (active).
 *   rule.<attrib> - defines the value of the field <attrib> in the rule
 * If the value of a given field is blank for a given row, the field is ignored for that
 * row (versus being used to set an attribute to blank, for example).  To delete an
 * attribute, the value must be set to the string "<null>".  To set it to a blank string,
 * use the value "<blank>".
 *   group.id - defines the ID of a tag group to be created (if it does not exist)
 *   group.type - defines the tag type of a tag group to be created.  If not defined,
 *      mantis04A is assumed.
 *   group.<attrib> - defines the settings for a tag group.  For Mantis tags, the only
 *      defined attribute is 'groupcode'.
 *
 *  The general flow of execution for each line is as follows:
 *  1) If group.id is provided, and a tag group with that ID is not defined, the other
 *  group.* values are used to create the tag group.
 *  2) If reader.id is provided, a reader with that ID is created, if needed, using the
 *  reader.type value (or "R200" otherwise).
 *  3) If any reader.<attrib> values are defined, they are set for the reader.
 *  4) If the reader was newly created, it is activated (unless reader.enabled is defined
 *  and set to false).  If reader.enabled is defined, the reader's activation state is
 *  updated accordingly.
 *  5) For each location.<N>.id defined, starting at the highest value of <N>, a location
 *  with the given ID is created, if needed, and set to have the location.<N+1>.id as its
 *  parent (if defined).  Any location.<N>.<attrib> values are set for each location.
 *  6) If reader.id is defined AND location.1.id is defined, a location rule is created
 *  (if needed).  If rule.id is defined, the value is used as the ID.  If not, the ID
 *  is assumed to be "<reader.id>_<location.1.id>_rule".  If created, the rule type
 *  is "AverageSSIRule", unless rule.type was provided.  The channellist for the rule
 *  is assumed to be "<reader.id>_channel_A,<reader.id>_channel_B", unless
 *  rule.channellist is set.  Other rule.<attrib> values are used to set rule settings.
 *  If created, the rule is activated (unless rule.enabled is defined and false).  If
 *  rule.enabled is set, the rule activation is updated appropriately.
 *
 * @author Mike Primm
 *
 */
public class RackConfigImportServlet extends HttpServlet {
    static final long serialVersionUID = 0x6892739658234234L;
    /**
     * Init for servlet - gets things running
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /* ------------------------------------------------------------ */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html");
        ServletOutputStream out = response.getOutputStream();

        out.println("<html><body>");
        out.println("<FORM action=\"cfgimport\" method=\"post\" enctype=\"multipart/form-data\">");
        out.println("<P>CSV File for config:<INPUT type=\"file\" name=\"csvfile\"><BR>");
        out.println("<INPUT type=\"submit\" value=\"Send\">");
        out.println("</P>");
        out.println("</FORM></body></html>");
        out.flush();
    }

    /* ------------------------------------------------------------ */
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        RangerServer.User usr = null;
        boolean is_admin = true;
        /* If we've got users, do our own basic authentication */
        Map<String,RangerServer.User> users = RangerServer.getUsers();
        if(users.size() > 0) {
            String username = (String) request.getAttribute(RangerLoginModule.USERNAME_ATTRIBUTE);
            usr = users.get(username); /* Find username */
            if (usr == null) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            is_admin = usr.getRole().equals(User.ROLE_ADMIN);
            if(!is_admin) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        response.setHeader("Cache-Control","no-cache");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader ("Expires", 0);
        response.setContentType("text/plain");

        /* First, see if its multipart format */
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
            /* Create upload handler */
            ServletFileUpload upload = new ServletFileUpload();
            /* Parse request input stream */
            try {
                FileItemIterator iter = upload.getItemIterator(request);
                boolean found = false;
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    String name = item.getFieldName();
                    if(name.equals("csvfile") == false)
                        continue;
                    InputStream stream = item.openStream();
                    processCSVFile(new InputStreamReader(stream), response,
                        usr);
                    found = true;
                }
                if(!found)
                    RangerServer.error("no multipart csvfile");
            } catch (FileUploadException fux) {
                throw new ServletException(fux.getMessage());
            }
        }
        else {
            String prm = request.getParameter("csvfile");
            if(prm != null) {
                processCSVFile(new StringReader(prm), response, usr);
            } else {
                RangerServer.error("no csvfile");
            }
        }
    }

    public String getServletInfo() {
        return RangerServer.PRODUCTNAME + ", " + RangerServer.getVersion();
    }

    private class OurContext implements CommandContext, Runnable {
        String cmd;
        User user;
        //String contenttype;
        CommandHandler ch;
        Map<String,String> args;
        OutputSyntax ext;
        String err;
        boolean in_progress;
        /**
         * Send output string. Lines end with "\n" in the text (will be
         * translated by context if needed)
         *
         * @param s -
         *            message to send
         * @throws CommandException
         *             if error sending output (fatal to command)
         */
        public void sendString(String s) throws CommandException {
            return;
        }
        /**
         * Make string version of given object value for output
         *
         * @param o -
         *            value
         * @return String representation
         */
        public String makeString(Object o) {
            if (o == null)
                return RangerServer.NULL_VAL;
            if (o instanceof String) {
                return "\"" + o.toString() + "\"";
            } else if (o instanceof ReaderEntity) {
                return ((ReaderEntity) o).getID();
            } else if (o instanceof Set) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                for (Object oinst : (Set<?>) o) {
                    if (!first)
                        sb.append(',');
                    sb.append(makeString(oinst));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
            } else if (o instanceof Map) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                Map<?,?> omap = (Map<?,?>) o;
                for (Object e : omap.entrySet()) {
                    Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                    if (!first)
                        sb.append(',');
                    sb.append(ent.getKey().toString());
                    sb.append("=");
                    sb.append(makeString(ent.getValue()));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
            } else
                return o.toString();
        }
        /**
         * Report command error - do not call sendString() after this. Do not
         * call once sendString() has been used.
         *
         * @param msg -
         *            error message
         */
        public void reportError(String msg) {
            err = msg;
        }
        /**
         * Test if admin privilege
         *
         * @return true if admin, false if not
         */
        public boolean isAdmin() {
            return true;
        }
        /**
         * Test if config view privilege
         *
         * @return true if view allowed, false if not
         */
        public boolean isConfigView() {
            return true;
        }
        /**
         * Get our SessionFactory
         *
         * @return factory
         */
        public SessionFactory getSessionFactory() {
            return RangerServer.getSessionFactory();
        }
        /**
         * Get logger
         *
         * @return logger
         */
        public Logger getLogger() {
            return RangerServer.getSessionFactory().getLogger();
        }
        /**
         * Get my user
         *
         * @return user, or null if none
         */
        public User getUser() {
            return user;
        }
        /**
         * Request session termination
         */
        public void requestSessionTermination() {

        }
        /**
         * Enable/disable broadcasts on session
         *
         * @param enab -
         *            true=allow broadcasts
         */
        public void enableBroadcasts(boolean enab) {

        }
        /**
         * Set content-type for response - default is text/plain
         *
         * @param ct -
         *            MIME content type
         */
        public void setContentType(String ct) {
            //contenttype = ct;
        }
        /**
         * Use web syntax?
         * @return true if web-style, false if not
         */
        public boolean useWebSyntax() {
            return true;
        }
        /**
         * Start object using requested encoding
         * @param sb - string builder to append to
         * @param type - object type
         * @param id - object id
         * @param is_first - true if first, false if not
         */
        public StringBuilder appendStartObject(StringBuilder sb, String type,
            String id, boolean is_first) {
            return sb;
        }
        /**
         * Add encoded attribute value pair using requested encoding (text, json, xml)
         * @param sb - string builder to append to
         * @param id - attribute ID
         * @param val - attribute value
         * @param is_first - true if first, false otherwise
         */
        public StringBuilder appendAttrib(StringBuilder sb, String id, Object val,
            boolean is_first) {
            return sb;
        }
        /**
         * End object using requested encoding
         * @param sb - string builder to append to
         * @param type - object type
         * @param id - object id, or null if none
         */
        public StringBuilder appendEndObject(StringBuilder sb, String type, String id) {
            return sb;
        }
        /**
         * Command type extension ( the "ext" in cmd.ext, if provided ).  Used
         * to signal formatting options, such as XML encoding.
         * @return ext value, or "" if not provided
         */
        public OutputSyntax getFormatExt() {
            return ext;
        }
        /**
         * Report that command processing is incomplete - processing code will call
         * the commandCompleted() method once it is complete
         */
        public void commandStillInProgress() {
            in_progress = true;
        }
        /**
         * Report that a command previously reported as commandStillInProgress() has
         * completed.  Report exception, if one occurred
         */
        public void commandCompleted(CommandException cx) {
            try {
                if(cx == null) {
                }
                else {
                    err = cx.getMessage();
                }
            } finally {
                synchronized(this) {
                    notifyAll();
                }
            }
        }
        /**
         * Method to be run when action is executed by factory thread
         */
        public void run() {
            try {
                in_progress = false;
                ch.invokeCommand(cmd, this, args);  /* Invoke command */
                if(!in_progress)
                    commandCompleted(null);
            } catch (CommandException cx) {
                commandCompleted(cx);
            }
        }
    }

    private static final String ID_SUFFIX = "id";
    private static final String LOCATION_PREFIX = "location.";
    private static final String GROUP_PREFIX = "group.";
    private static final String READER_PREFIX = "reader.";
    private static final String RULE_PREFIX = "rule.";
    private static final String READERID_PARM = READER_PREFIX + ID_SUFFIX;
    private static final String GROUPID_PARM = GROUP_PREFIX + ID_SUFFIX;
    private static final String LOCATION1ID_PARM = LOCATION_PREFIX + "1." + ID_SUFFIX;
    private static final String RULEID_PARM = RULE_PREFIX + ID_SUFFIX;
    private static final String RULETYPE_PARM = RULE_PREFIX + "type";
    private static final String RULETYPE_DEF = AverageSSILocationRuleFactory.FACTORY_ID;
    private static final String RULECHANLIST_PARM = RULE_PREFIX + "channellist";
    private static final String READERTYPE_PARM = READER_PREFIX + "type";
    private static final String READERTYPE_DEF = "R200";
    private static final String READERENABLED_PARM = READER_PREFIX + "enabled";
    private static final String RULEENABLED_PARM = RULE_PREFIX + "enabled";
    private static final String READERGROUPS_PARM = READER_PREFIX + "groups";
    private static final String READERGROUPS_DEF = "*";
    private static final String GROUPTYPE_PARM = GROUP_PREFIX + "type";
    private static final String GROUPTYPE_DEF = MantisIITagType04A.TAGTYPEID;
    private static final String BLANK_VAL = "<blank>";
    private static final String NULL_VAL = "<null>";
    private static final String TRUE_VAL = "true";
    private static final String FALSE_VAL = "false";

    private enum qstate { UNQUOTED, IN_QUOTE, AT_END_OR_INNER, AFTER_END };
    /**
     * Tokenize CSV-formatted line
     */
    public static List<String> tokenizeCSVLine(String line) {
        ArrayList<String> out = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        qstate state = qstate.UNQUOTED;
        int i, len = line.length();
        for(i = 0; i < len; i++) {
            char c = line.charAt(i);
            if(state == qstate.IN_QUOTE) {  /* If after open quote */
                if(c == '\"') { /* Quote found?     */
                    state = qstate.AT_END_OR_INNER;    /* We're at quote - next
                        will tell us if end or embedded */
                }
                else {  /* Anything else we add to string */
                    sb.append(c);
                }
            }
            else if(state == qstate.AT_END_OR_INNER) {  /* Just past quote */
                if(c == '\"') { /* Another quote?  Its a quote character */
                    sb.append(c);   /* Append it */
                    state = qstate.IN_QUOTE;    /* Still in quoted string */
                }
                else {  /* Else, push completed string and continue */
                    out.add(sb.toString());
                    sb.setLength(0);
                    if(c == ',')    /* If comma found */
                        state = qstate.UNQUOTED;    /* Ready for next one */
                    else
                        state = qstate.AFTER_END;   /* Looking for comma */
                }
            }
            else if(state == qstate.AFTER_END) {    /* After end of string */
                if(c == ',')
                    state = qstate.UNQUOTED;        /* Found comma - start */
            }
            else { /* Unquoted - look for quotes or accum chars */
                if(c == '\"') { /* Start of quote? */
                    sb.setLength(0);
                    state = qstate.IN_QUOTE;
                }
                else if(c == ',') { /* Found comma */
                    out.add(sb.toString()); /* Flush string */
                    sb.setLength(0);
                }
                else {          /* Else, accum characters */
                    sb.append(c);
                }
            }
        }
        /* Always finish the last string (unless already past end) */
        if(state != qstate.AFTER_END) {
            out.add(sb.toString());
        }
        return out;
    }
    /**
     * Execute command
     */
    private String executeCommand(String cmd, Map<String,String> args,
        RangerServer.User usr) throws IOException,
        ServletException {
        /* Now, make context handler  */
        OurContext ctx = new OurContext();
        ctx.cmd = cmd;
        ctx.ext = null;
        ctx.user = usr;
        //ctx.contenttype = "text/plain";
        /* Build map of our arguments */
        ctx.args = args;
        /* Now, try to find the corresponding command handler */
        CommandHandler ch = RangerServer.getCommandHandlers().get(cmd);
        if(ch == null) {    /* If not found, error */
            return "Invalid command ID - " + cmd;
        }
        ctx.ch = ch;
        /* Now run on session thread */
		try {
	        RangerProcessor.runActionNow(ctx);
		} catch (InterruptedException ix) {
			throw new ServletException("Interrupted!");
		}
        return ctx.err;
    }
    /**
     * Process CSV-formatted configuration data stream
     *
     * @param inp
     */
    private void processCSVFile(Reader inp, HttpServletResponse response,
        RangerServer.User usr) throws
        IOException, ServletException {
        BufferedReader br = new BufferedReader(inp);
        ServletOutputStream out = response.getOutputStream();
        String line;
        List<String> fields;
        /* Read first line - these are our field IDs */
        line = br.readLine();
        if(line == null)
            throw new IOException("Bad CSV file format");
        fields = tokenizeCSVLine(line);
        /* Do basic validation on field IDs */
        int i = 0;
        for(String fld : fields) {
            /* If first letter is upper-case, make it lower case (some spreadsheets
             * force fields to be capitalized)
             */
            if(fld.length() > 0 && Character.isUpperCase(fld.charAt(0))) {
                fld = fld.substring(0,1).toLowerCase() + fld.substring(1);
                fields.set(i, fld);
            }
            if(fld.startsWith(READER_PREFIX) ||
                fld.startsWith(RULE_PREFIX) ||
                fld.startsWith(GROUP_PREFIX) ||
                fld.startsWith(LOCATION_PREFIX)) {
            }
            else {
                out.println("<warning>: Column name " + fld + " is invalid, column ignored");
            }
            i++;
        }
        /* Now, loop through the other lines */
        while((line = br.readLine()) != null) {
            String rdrid = null;
            String loc1id = null;
            String ruleid = null;
            String grpid = null;
            /* Tokenize the line */
            List<String> tokens = tokenizeCSVLine(line);
            /* If we have a group ID, process group operations */
            grpid = getField(fields, tokens, GROUPID_PARM);
            if(grpid != null) {
                processGroupOperations(fields, tokens, grpid, usr, out);
            }
            /* If we have a reader ID, process reader operations */
            rdrid = getField(fields, tokens, READERID_PARM);
            if(rdrid != null) {
                processReaderOperations(fields, tokens, rdrid, usr, out);
            }
            /* If we have a location.1 ID, process location ops */
            loc1id = getField(fields, tokens, LOCATION1ID_PARM);
            if(loc1id != null) {
                processLocationOperations(fields, tokens, 1,
                    loc1id, usr, out);
            }
            /* If we have location.1 AND (reader ID OR rule ID), handle rule ops */
            ruleid = getField(fields, tokens, RULEID_PARM);
            if(((rdrid != null) || (ruleid != null)) && (loc1id != null)) {
                if(ruleid == null)
                    ruleid = rdrid + "_" + loc1id + "_rule";
                processRuleOperations(fields, tokens, ruleid, rdrid, loc1id, usr, out);
            }
        }
        out.flush();
    }
    /**
     * Process reader operations
     */
    private void processReaderOperations(List<String> fields,
        List<String> tokens, String rdrid,
        RangerServer.User usr, ServletOutputStream out) throws IOException,
        ServletException {
        boolean do_enable = false;
        boolean do_disable = false;
        boolean is_new = false;
        HashMap<String,String> args = new HashMap<String,String>();
        /* First, do get on reader to see if it exists already */
        args.clear();
        args.put(ReaderGetHandler.READERID, rdrid);
        String err = executeCommand(ReaderGetHandler.READERGET_CMD, args, usr);
        if(err != null) {   /* If not, need to create it */
            String rdrtype = getField(fields, tokens, READERTYPE_PARM,
                READERTYPE_DEF);
            /* Issue create command */
            args.clear();
            args.put(ReaderCreateHandler.READERID, rdrid);
            args.put(ReaderCreateHandler.READERTYPE, rdrtype);
            err = executeCommand(ReaderCreateHandler.READERCREATE_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to create reader " + rdrid + ": " + err);
                return;
            }
            else {
                out.println("<done>: Created reader " + rdrid);
                do_enable = true;
                is_new = true;
            }
        }
        else {
            out.println("<info>: Reader " + rdrid + " already exists");
        }
        /* Build readerset command to set reader settings */
        int fldidx = -1;
        for(String fldid : fields) {
            fldidx++;
            if(!fldid.startsWith(READER_PREFIX))
                continue;
            /* If ID or type or enable, not an extra parm */
            if(fldid.equals(READERID_PARM) || fldid.equals(READERTYPE_PARM) ||
                fldid.equals(READERENABLED_PARM) || fldid.equals(READERGROUPS_PARM))
                continue;
            if(fldidx >= tokens.size())
                continue;
            String pval = tokens.get(fldidx);
            if(pval == null)
                continue;
            if(pval.length() == 0)
                continue;
            if(pval.equals(BLANK_VAL))
                pval = "";
            if(pval.equals(NULL_VAL))
                pval = "null";
            args.clear();
            String prmid = fldid.substring(READER_PREFIX.length());
            args.put(prmid, pval);
            args.put(ReaderSetHandler.READERID, rdrid);
            err = executeCommand(ReaderSetHandler.READERSET_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to set " +
                    prmid + " on reader " + rdrid + ": " + err);
            }
            else {
                out.println("<done>: Set " +
                    prmid + "=" + pval + " on reader " + rdrid);
            }
        }
        /* Handle group add operations */
        String grpids = getField(fields, tokens, READERGROUPS_PARM);
        if((grpids == null) && (is_new)) {
            grpids = READERGROUPS_DEF;
        }
        if(grpids != null) {
            args.clear();
            args.put(ReaderAddGroupHandler.READERID, rdrid);
            String[] gids = grpids.split(",");
            for(int i = 0; i < gids.length; i++) {
                args.put(ReaderAddGroupHandler.GROUPID_PREFIX + i, gids[i].trim());
            }
            err = executeCommand(ReaderAddGroupHandler.READERADDGROUP_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to add groups " +
                    grpids + " on reader " + rdrid + ": " + err);
            }
            else {
                out.println("<done>: Added tag groups " +
                    grpids + " on reader " + rdrid);
            }
        }
        /* See if we need to do activate/deactivate */
        String enab = getField(fields, tokens, READERENABLED_PARM);
        if(enab != null) {
            enab = enab.toLowerCase();
            if(enab.equals(TRUE_VAL)) {
                do_enable = true;
            }
            else if(enab.equals(FALSE_VAL)) {
                do_disable = true;
            }
            else {
                out.println("<error>: Bad value for reader.enabled (" +
                    enab +"): must be true or false");
            }
        }
        /* If disable or enable requested, issue command */
        if(do_disable || do_enable) {
            args.clear();
            args.put(ReaderActivateHandler.READERID, rdrid);
            err = executeCommand(
                do_disable?ReaderActivateHandler.READERDEACTIVATE_CMD:
                    ReaderActivateHandler.READERACTIVATE_CMD,
                args, usr);
            if(do_disable) {
                if(err != null) {
                    out.println("<error>: Failed to deactivate reader " + rdrid + ": " + err);
                }
                else {
                    out.println("<done>: Deactivated reader " + rdrid);
                }
            } else {
                if(err != null) {
                    out.println("<error>: Failed to activate reader " + rdrid + ": " + err);
                }
                else {
                    out.println("<done>: Activated reader " + rdrid);
                }
            }
        }
    }
    /**
     * Process group operations
     */
    private void processGroupOperations(List<String> fields,
        List<String> tokens, String grpid,
        RangerServer.User usr, ServletOutputStream out) throws IOException,
        ServletException {
        HashMap<String,String> args = new HashMap<String,String>();
        /* Build create command, and handle duplicate ID error */
        args.clear();
        args.put(GroupCreateHandler.GROUPID, grpid);
        String tagtype = getField(fields, tokens,
            GROUPTYPE_PARM, GROUPTYPE_DEF);
        args.put(GroupCreateHandler.TAGTYPE, tagtype);
        int fldidx = -1;
        for(String fldid : fields) {
            fldidx++;
            if(!fldid.startsWith(GROUP_PREFIX))
                continue;
            /* If ID or type or enable, not an extra parm */
            if(fldid.equals(GROUPID_PARM) || fldid.equals(GROUPTYPE_PARM))
                continue;
            if(fldidx >= tokens.size())
                continue;
            String pval = tokens.get(fldidx);
            if(pval == null)
                continue;
            if(pval.length() == 0)
                continue;
            if(pval.equals(BLANK_VAL))
                pval = "";
            if(pval.equals(NULL_VAL))
                pval = "null";
            String prmid = fldid.substring(GROUP_PREFIX.length());
            args.put(prmid, pval);
        }
        String err = executeCommand(GroupCreateHandler.GROUPCREATE_CMD, args, usr);
        if(err != null) {   /* Error */
            if(err.indexOf("Duplicate") >= 0) {
                out.println("<info>: Group " + grpid + " already exists");
            }
            else {
                out.println("<error>: Failed to create group " + grpid + ": " + err);
            }
        }
        else {
            out.println("<done>: Group " + grpid + " created");
        }
    }
    /**
     * Process location operations
     */
    private void processLocationOperations(List<String> fields,
        List<String> tokens, int loclvl, String locid,
        RangerServer.User usr, ServletOutputStream out) throws IOException,
        ServletException {
        /* First, see if we have parent ID - we process depth-first */
        String parid = getField(fields, tokens,
            LOCATION_PREFIX + Integer.toString(loclvl+1)+ "." + ID_SUFFIX);
        if(parid != null) {
            /* Recurse to cover parent first */
            processLocationOperations(fields, tokens, loclvl+1,
                parid, usr, out);
        }
        /* Now, handle our location */
        HashMap<String,String> args = new HashMap<String,String>();
        /* First, do get on location to see if it exists already */
        args.clear();
        args.put(LocationGetHandler.LOCATIONID, locid);
        String err = executeCommand(LocationGetHandler.LOCATIONGET_CMD, args, usr);
        if(err != null) {   /* If not, need to create it */
            /* Issue create command */
            args.clear();
            args.put(LocationCreateHandler.LOCID, locid);
            if(parid != null)
                args.put(LocationCreateHandler.PARENTID, parid);
            err = executeCommand(LocationCreateHandler.LOCCREATE_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to create location " + locid + ": " + err);
                return;
            }
            else {
                out.println("<done>: Created location " + locid);
            }
        }
        else {
            out.println("<info>: Location " + locid + " already exists");
            if(parid != null) {
                args.clear();
                args.put(LocationCreateHandler.PARENTID, parid);
                args.put(LocationSetHandler.LOCID, locid);
                err = executeCommand(LocationSetHandler.LOCSET_CMD,
                    args, usr);
                if(err != null) {   /* Error here is fatal */
                    out.println("<error>: Failed to set parent on location " + locid + ": " + err);
                }
                else {
                    out.println("<done>: Set parent=" + parid + " on location " + locid);
                }
            }
        }
        /* Build locset command to set location settings */
        int fldidx = -1;
        String our_prefix = LOCATION_PREFIX + Integer.toString(loclvl) + ".";
        for(String fldid : fields) {
            fldidx++;
            if(!fldid.startsWith(our_prefix))
                continue;
            String fid = fldid.substring(our_prefix.length());
            /* If ID, not an extra parm */
            if(fid.equals(ID_SUFFIX))
                continue;
            if(fldidx >= tokens.size())
                continue;
            String pval = tokens.get(fldidx);
            if(pval == null)
                continue;
            if(pval.length() == 0)
                continue;
            if(pval.equals(BLANK_VAL))
                pval = "";
            if(pval.equals(NULL_VAL))
                pval = "null";
            args.clear();
            args.put(fid, pval);
            args.put(LocationSetHandler.LOCID, locid);
            err = executeCommand(LocationSetHandler.LOCSET_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to set attribute " +
                    fid + " on location " + locid + ": " + err);
            }
            else {
                out.println("<done>: Set " +
                    fid + "=" + pval + " on location " + locid);
            }
        }
    }
    /**
     * Process rule operations
     */
    private void processRuleOperations(List<String> fields,
        List<String> tokens, String ruleid,
        String rdrid, String loc1id,
        RangerServer.User usr, ServletOutputStream out) throws IOException,
        ServletException {
        HashMap<String,String> args = new HashMap<String,String>();
        HashMap<String,String> set_args = new HashMap<String,String>();
        boolean do_enable = false;
        boolean do_disable = false;

        /* Parse out the settable args, since we might use these in the
         * create if the rule doesn't exist */
        /* Build ruleset command to set rule settings */
        int fldidx = -1;
        for(String fldid : fields) {
            fldidx++;
            if(!fldid.startsWith(RULE_PREFIX))
                continue;
            /* If ID or type, skip */
            if(fldid.equals(RULETYPE_PARM) || fldid.equals(RULEID_PARM) ||
                fldid.equals(RULEENABLED_PARM))
                continue;
            String fid = fldid.substring(RULE_PREFIX.length());
            if(fldidx >= tokens.size())
                continue;
            String pval = tokens.get(fldidx);
            if(pval == null)
                continue;
            if(pval.length() == 0)
                continue;
            if(pval.equals(BLANK_VAL))
                pval = "";
            if(pval.equals(NULL_VAL))
                pval = "null";
            set_args.put(fid, pval);
        }
        /* Now, do get on rule to see if it exists already */
        args.clear();
        args.put(RuleGetHandler.RULEID, ruleid);
        String err = executeCommand(RuleGetHandler.RULEGET_CMD, args, usr);
        if(err != null) {   /* If not, need to create it */
            String ruletype = getField(fields, tokens, RULETYPE_PARM, RULETYPE_DEF);
            /* Get channel list */
            String chanlist = getField(fields, tokens, RULECHANLIST_PARM,
                (rdrid != null)?(rdrid + "_channel_A," + rdrid + "_channel_B"):"");
            /* Issue create command */
            args.clear();
            args.putAll(set_args);  /* Put in the settings first */
            set_args.clear();       /* Erase em, since we're doing them here */
            args.put(RuleCreateHandler.RULEID, ruleid);
            args.put(RuleCreateHandler.RULETYPE, ruletype);
            args.put(LocationRuleFactory.ATTRIB_CHANNELLIST, chanlist);
            args.put(RuleCreateHandler.LOCATIONID, loc1id);
            err = executeCommand(RuleCreateHandler.RULECREATE_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to create rule " + ruleid + ": " + err);
                return;
            }
            else {
                out.println("<done>: Created rule " + ruleid);
                do_enable = true;
            }
        }
        else {
            out.println("<info>: Rule " + ruleid + " already exists");
        }
        /* Build ruleset command to set rule settings */
        for(Map.Entry<String,String> se : set_args.entrySet()) {
            args.clear();
            args.put(se.getKey(), se.getValue());
            args.put(RuleSetHandler.RULEID, ruleid);
            err = executeCommand(RuleSetHandler.RULESET_CMD,
                args, usr);
            if(err != null) {   /* Error here is fatal */
                out.println("<error>: Failed to set " +
                    se.getKey() + " on rule " + ruleid + ": " + err);
            }
            else {
                out.println("<done>: Set " +
                    se.getKey() + "=" + se.getValue() + " on rule " + ruleid);
            }
        }
        /* See if we need to do activate/deactivate */
        String enab = getField(fields, tokens, RULEENABLED_PARM);
        if(enab != null) {
            enab = enab.toLowerCase();
            if(enab.equals(TRUE_VAL)) {
                do_enable = true;
            }
            else if(enab.equals(FALSE_VAL)) {
                do_disable = true;
            }
            else {
                out.println("<error>: Bad value for rule.enabled (" +
                    enab +"): must be true or false");
            }
        }
        /* If disable or enable requested, issue command */
        if(do_disable || do_enable) {
            args.clear();
            args.put(RuleActivateHandler.RULEID, ruleid);
            err = executeCommand(
                do_disable?RuleActivateHandler.RULEDEACTIVATE_CMD:
                    RuleActivateHandler.RULEACTIVATE_CMD,
                args, usr);
            if(do_disable) {
                if(err != null) {
                    out.println("<error>: Failed to deactivate rule " + ruleid + ": " + err);
                }
                else {
                    out.println("<done>: Deactivated rule " + ruleid);
                }
            } else {
                if(err != null) {
                    out.println("<error>: Failed to activate rule " + ruleid + ": " + err);
                }
                else {
                    out.println("<done>: Activated rule " + ruleid);
                }
            }
        }

    }
    /* Get trimmed field from token list, if defined */
    private static String getField(List<String> fields, List<String> tokens,
        String fldid, String def) {
        String val = null;
        int idx = fields.indexOf(fldid);
        if(idx >= 0) {
            if(idx < tokens.size())
                val = tokens.get(idx);
            if(val != null) {
                val = val.trim();
                if(val.length() == 0)
                    val = null;
            }
        }
        if(val == null) val = def;
        return val;
    }
    private static String getField(List<String> fields, List<String> tokens,
        String fldid) {
        return getField(fields, tokens, fldid, null);
    }
}
