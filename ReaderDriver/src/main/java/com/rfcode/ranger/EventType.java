package com.rfcode.ranger;

import java.util.Map;
import java.util.HashMap;

/**
 * Enum for event types
 * 
 * @author Mike Primm
 */
public enum EventType {
    CREATE("create", "Create"), UPDATE("update", "Update"), DELETE("delete", "Delete"),
    INITSTART("init-start", "InitStart"), INITEND( "init-end", "InitEnd"), 
    SVC("service", "Service"), TRIGGER("trigger", "Trigger"),
    UPCONNECTERROR("upconnect-error", "UpConnectError"),
    RESTART("restart", "Restart"), HISTSTART("hist-start", "HistStart"),
    HISTEND("hist-end", "HistEnd"), BEACON("beacon", "Beacon");

	private static Map<String,EventType> type_to_val = new HashMap<String,EventType>();

    String typestr;
    String txtstr;
	
    EventType(String tstr, String txstr) {
        typestr = tstr;
        txtstr = txstr;
    }
    String getTypeStr() {
        return typestr;
    }
    String getTextTypeStr() {
        return txtstr;
    }
	static EventType getTypeByString(String tstr) {
		return type_to_val.get(tstr);
	}
    static {
        for(EventType e : EventType.values()) {
            type_to_val.put(e.typestr, e);
        }
    }
}
