package com.rfcode.ranger;

import java.util.ArrayDeque;
import java.util.IdentityHashMap;

import java.util.PriorityQueue;

/**
 * Main processor thread for ranger state information: work queue, both immediate and delayed, used to
 * serialize access and manipulation of large, shared, interconnected reader and tag state without excessive synchronization.
 * All processing is done on single, internally-maintained thread, so all provided work must respect this (no blocking or
 * other long-lived operations). 
 *
 * @author Mike Primm
 */
public class RangerProcessor {
    /* Time delay queue - used for time delayed actions like timeouts */
    private static final PriorityQueue<DelayedAction> delayqueue = new PriorityQueue<DelayedAction>();
    private static final IdentityHashMap<Runnable, DelayedAction> sact_to_dact = new IdentityHashMap<Runnable, DelayedAction>();
    private static long delayqueue_basetime = System.currentTimeMillis(); /* Reference time (create time) for delayqueue */
    private static long delayqueue_lasttime = delayqueue_basetime; /* Reference time (last time) for delayqueue */
    /* Our action queue */
    private static final ArrayDeque<Runnable> act_queue = new ArrayDeque<Runnable>();
    /*
     * Maximum time between check for the delay queue - used for time-warp
     * detection
     */
    private static final int MAX_ELAPSED_TIME = 10000;
    /* Our processing thread */
    private static ServiceThread our_thread;
	/* Lock object */
	private static Object lock = new Object();
	private static Object start_end = new Object();

    /**
     * Private queue element for delayed actions
     */
    private static class DelayedAction implements Comparable<DelayedAction> {
        private long delaytime; /*
                                 * Time to delay - relative to queue create
                                 * time, to handle clock resets and the like
                                 */
        private Runnable action; /* Action to be run */
        /**
         * Comparison function - used to order items in delay queue
         */
        public int compareTo(DelayedAction act) {
            return (int) (delaytime - act.delaytime);
        }
    }
    /**
     * Handler thread for our sessions - does all the work
     */
    private static class ServiceThread extends Thread {
		ServiceThread() {
			super("RangerProcessor");
		}
        /**
         * Main routine for our service thread
         */
        public void run() {
            try {
                process(); /* Call private method in parent class */
            } finally {
                our_thread = null; /* We're dead, so unhook */
            }
        }
    }
    /**
     * Init method - used to activate the configured factory
     */
    public static void init() {
        if (our_thread != null)
            return;
        our_thread = new ServiceThread(); /* Create our service thread */
        our_thread.setPriority((Thread.NORM_PRIORITY+Thread.MAX_PRIORITY)/2);
        synchronized (start_end) {
            our_thread.start(); /* And set it running */
            try {
                start_end.wait();
            } catch (InterruptedException ix) {
            }
        }
    }
    /**
     * Cleanup method - used to deactivate and clean up the factory
     */
    public static void cleanup() {
        if (our_thread != null) {
            /* Wait for thread to finish up */
            synchronized (start_end) {
                our_thread.interrupt();
                try {
                    start_end.wait();
                } catch (InterruptedException ix) {
                }
            }
        }
        act_queue.clear();
        delayqueue.clear();
        sact_to_dact.clear();
    }
    /**
     * Handle all processing
     */
    private static void process() {
    	/* Signal that thread is ready for business */
    	synchronized (start_end) {
        	start_end.notifyAll();
    	}

        int run_action_count = 0;

    	while (!our_thread.isInterrupted()) {
       		Runnable r = null;

			/* Fetch next runnable event */
			synchronized(lock) {
                long sleep = -1;

                if (run_action_count < 1000) {
                    r = act_queue.pollFirst();  /* Start with immediate stuff */
                }
				if(r == null) {		/* If none, see if time for delayed action */
	                long now = System.currentTimeMillis();
    	            checkDelayQueueClock(now); /* Check and adjust base */
    	            delayqueue_lasttime = now;
    	            DelayedAction act = delayqueue.peek(); /* Peek next in queue */
	                /* If action in queue */
    	            if (act != null) {
    	                /* If not due to run, enter wait for long enough (additions will wake us too) */
    	                if (act.delaytime > (now - delayqueue_basetime)) {
							sleep = act.delaytime - (now - delayqueue_basetime);	/* Remember amount to go */
						}
						else { /* Else, time to run it */
							act = delayqueue.poll(); /* Remove from queue */
                        	sact_to_dact.remove(act.action);
							r = act.action;		/* Save action to be run */
                    	}
                    }
                }
                if (r == null) {
                    r = act_queue.pollFirst();
                    run_action_count = 0;
                }

				if(r == null) {	/* If no work, wait */
					try {
						if(sleep >= 0)
							lock.wait(sleep);
						else
							lock.wait();
					} catch (InterruptedException ix) {
					}
				}
            }

            if(r != null) { /* If we have work, run it */
   	     		try {
                	r.run();
            	} catch (Exception x) {
                    RangerServer.getLogger().error("Error processing RangerProcessor task", x);
            	}
        	}
            run_action_count++;
        }
        /* Signal that thread is cleaned up */
        synchronized (start_end) {
            our_thread = null;
            start_end.notifyAll();
        }
    }

    /**
     * Sanity check reference time - used to handle "time warps" due to system
     * clock changes.
     */
    private static void checkDelayQueueClock(long now) {
        if (now < delayqueue_lasttime) { /* Time warped backwards? */
            /* Assume no/little elased time - adjust base time */
            delayqueue_basetime -= delayqueue_lasttime - now;
        }
        /* Time warp forward - too much time since last call to be real */
        else if (now > (delayqueue_lasttime + MAX_ELAPSED_TIME)) {
            delayqueue_basetime += now - delayqueue_lasttime - MAX_ELAPSED_TIME;
        }
    }
	/**
	 * Add an immediate action
	 * @param act -
     *            Runnable action
     */
    public static void addImmediateAction(Runnable act) {
		synchronized(lock) {
	        act_queue.addLast(act);
			if(our_thread == null)
				init();
			lock.notify(); /* Wake up selector thread to handle it */
		}
	}
	private static class RunNow implements Runnable {
		Runnable act;

		public void run() {
			try {
				act.run();
			} finally {
				synchronized(this) {
					this.notify();
				}
			}
		}
	}
	/**
	 * Run action immediately (block until done)
	 * @param act - 
     *       Runnable action
     */
	public static void runActionNow(Runnable act) throws InterruptedException {
		RunNow r = new RunNow();
		r.act = act;
		synchronized(r) {
			addImmediateAction(r);
			r.wait();
		}
	}
    /**
     * Add a delayed action, with a given time delay in milliseoncds
     * 
     * @param act -
     *            Runnable action
     * @param delay -
     *            Time delay in milliseconds
     */
    public static void addDelayedAction(Runnable act, long delay) {
        if (delay == 0) {
			addImmediateAction(act);
			return;
        }
        synchronized (lock) {
            long now = System.currentTimeMillis(); /* Current time */
            checkDelayQueueClock(now); /* Check and adjust base */
            DelayedAction action = new DelayedAction();
            action.action = act; /* Save action */

            /* Compute delay time as end-time relative to queue's base time */
            /* (milliseconds since the queue was created) */
            action.delaytime = now - delayqueue_basetime + delay;
            delayqueue_lasttime = now; /* Remember last time */
            delayqueue.add(action); /* Add to queue */
            sact_to_dact.put(act, action);  /* Add reverse lookup */
            if (delayqueue.peek() == action) { /* If we're first, have */
				if(our_thread == null)
					init();
                /* selector thread wake and check */
                lock.notify(); /* Wake up selector thread to check it */
            }
        }
    }
    /**
     * Cancel delayed action - removed from queue, if not already removed
     */
    public static void removeDelayedAction(Runnable act) {
        synchronized (lock) {
            DelayedAction dact = sact_to_dact.remove(act);  /* Find/remove reverse */
            if(dact != null) {  /* If found, pull from delay queue */
                delayqueue.remove(dact); /* Remove the action */
            }
        }
    }
}
