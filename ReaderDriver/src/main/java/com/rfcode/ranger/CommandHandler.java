package com.rfcode.ranger;

import java.util.Map;
import java.util.List;

/**
 * Interface used to provide a handler for command line and/or web commands in
 * Ranger.
 * 
 * @author Mike Primm
 */
public interface CommandHandler {
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException;
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException;
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs();
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd);
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd);
    /**
     * Get web syntax help for command (only text after <command>?)
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd);
    /**
     * Requires admin privilege?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired();
    /**
     * Requires config read priv? If isAdminRequired=true, this must be true/
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired();
}
