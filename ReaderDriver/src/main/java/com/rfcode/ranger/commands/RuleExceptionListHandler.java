package com.rfcode.ranger.commands;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.locationrules.LocationRuleException;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "ruleexceptlist" commands
 * 
 * @author Mike Primm
 */
public class RuleExceptionListHandler implements CommandHandler {
    public static final String RULEEXCEPTLIST_CMD = "ruleexceptlist";
    /* Command ID */
    public static final String[] CMD_IDS = {RULEEXCEPTLIST_CMD};
    /* Command help */
    private static final String[] RULEEXCEPTLIST_CMD_HELP = {"List all location rule exceptions."};
    /* Command syntax help */
    private static final String[] RULEEXCEPTLIST_CMD_SYNTAX_HELP = {RULEEXCEPTLIST_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        LocationRuleException lrx;
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getLocationRuleExceptionIDs());
        StringBuilder sb = new StringBuilder();
        
        /* Start rule-except-list */
        ctx.appendStartObject(sb, "rule-except-list", null, true);
        boolean first = true;
        for (String key : keys) {
            lrx = ReaderEntityDirectory.findLocationRuleException(key);
            /* Start rule exception */
            ctx.appendStartObject(sb, "rule-except", lrx.getID(), first);
            ctx.appendAttrib(sb, "type", lrx.getExceptionType(), true);
            ctx.appendAttrib(sb, "rule-id", lrx.getRuleWithException(), false);
            ctx.appendAttrib(sb, "start-time", lrx.getExceptionTimestamp(), false);
            ctx.appendAttrib(sb, "description", lrx.getDescription(), false);
            String[] k = lrx.getRulesRelatedToException();
            if (k != null) {
            	List<String> lst = new ArrayList<String>();
            	for(String kk : k) {
            		lst.add(kk);
            	}
                ctx.appendAttrib(sb, "associated-rule-ids", lst, false);
            }
            ctx.appendEndObject(sb, "rule-except", lrx.getID());
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            first = false;
        }
        ctx.appendEndObject(sb, "rule-except-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return RULEEXCEPTLIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
    	return RULEEXCEPTLIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
