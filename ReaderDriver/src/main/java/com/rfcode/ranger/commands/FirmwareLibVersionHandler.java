package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.FirmwareLibrary;

/**
 * Handler for "fwlibversion" command
 * 
 * @author Mike Primm
 */
public class FirmwareLibVersionHandler implements CommandHandler {
    public static final String FWLIBVERSION_CMD = "fwlibversion";
    /* Command ID */
    public static final String[] CMD_IDS = {FWLIBVERSION_CMD};
    /* Command help */
    private static final String[] FWLIBVERSION_CMD_HELP = {"Get server firmware library version and information."};
    /* Command syntax help */
    private static final String[] FWLIBVERSION_CMD_SYNTAX_HELP = {
        FWLIBVERSION_CMD
        };
    private static final String[] CMD_WEB_SYNTAX_HELP = { "" };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        ctx.appendStartObject(sb, "fwlibversion", null, true);
        ctx.appendAttrib(sb, "version", FirmwareLibrary.getVersion(), true);
        ctx.appendAttrib(sb, "desc", FirmwareLibrary.getDescription(), false);
		ctx.appendAttrib(sb, "fwfamversions", FirmwareLibrary.getFirmwareVersions(), false);
        ctx.appendEndObject(sb, "fwlibversion", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return FWLIBVERSION_CMD_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return FWLIBVERSION_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
