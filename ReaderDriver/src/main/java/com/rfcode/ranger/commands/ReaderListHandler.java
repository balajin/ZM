package com.rfcode.ranger.commands;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.net.InetAddress;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
/**
 * Handler for "readerlist" and "readerstatus" commands
 *
 * @author Mike Primm
 */
public class ReaderListHandler implements CommandHandler {
    /* Parameter for reader ID */
    public static final String READERID = "id";

    public static final String READERLIST_CMD = "readerlist";
    public static final String READERSTATUS_CMD = "readerstatus";
    public static final String READEREXPORT_CMD = "readerexport";
    /* Command ID */
    public static final String[] CMD_IDS = {READERLIST_CMD, READERSTATUS_CMD, READEREXPORT_CMD};
    /* Command help */
    private static final String[] READERLIST_CMD_HELP = {"List all readers and their settings and status."};
    private static final String[] READERSTATUS_CMD_HELP = {"List all readers and their status."};
    private static final String[] READEREXPORT_CMD_HELP = {"Export all readers, in importable CSV format."};
    /* Command syntax help */
    private static final String[] READERLIST_CMD_SYNTAX_HELP = {READERLIST_CMD};
    private static final String[] READERSTATUS_CMD_SYNTAX_HELP = {READERSTATUS_CMD};
    private static final String[] READEREXPORT_CMD_SYNTAX_HELP = {READEREXPORT_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Set<String> keys;

        if(parms.get(READERID) == null) {
            keys = new TreeSet<String>(ReaderEntityDirectory.getReaderIDs());
        }
        else {
            keys = new TreeSet<String>();
            keys.add(parms.get(READERID));
        }
        StringBuilder sb = new StringBuilder();
        boolean just_status = false;
        boolean do_exp = false;
        Set<String> colids = null;

        if(cmd.equals(READEREXPORT_CMD)) {
            do_exp = true;
            //ctx.setContentType("text/x-excel-csv");
            /* Build column set of distinct attribtues */
            colids = new LinkedHashSet<String>();
            for(String key : keys) {
                Reader rdr = ReaderEntityDirectory.findReader(key);
                colids.addAll(rdr.getReaderAttributes().keySet());
            }
            /* Output first line for CVS */
            sb.append("reader.id,reader.type,reader.enabled,reader.groups,reader.label");
            for(String col : colids) {
                sb.append(",reader.");
                sb.append(col);
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }

        /* Start reader-list */
        if(!do_exp)
            ctx.appendStartObject(sb, "reader-list", null, true);

        /* Check if status command */
        if (cmd.equals(READERSTATUS_CMD))
            just_status = true;

        boolean first = true;
        for (String key : keys) {
            Reader rdr = ReaderEntityDirectory.findReader(key);
            if (rdr == null) {
                continue;
            }
            if(!do_exp) {
                ctx.appendStartObject(sb, "reader", rdr.getID(), first);
                first = false;
                boolean firstattrib = true;

                if (!just_status) {
                    Map<String, Object> a = rdr.getReaderAttributes();
                    String[] k = rdr.getReaderFactory().getReaderAttributeIDs();
                    for (int j = 0; j < k.length; j++) {
                        ctx.appendAttrib(sb, k[j], a.get(k[j]), firstattrib);
                        firstattrib = false;
                    }
                    ctx.appendAttrib(sb, "enabled", rdr.getReaderEnabled(), firstattrib);
                    firstattrib = false;
                }
                ctx.appendAttrib(sb, "state", rdr.getReaderStatus(), firstattrib);
                firstattrib = false;
                ctx.appendAttrib(sb, "fwversion", rdr.getReaderFirmwareVersion(), firstattrib);
                ctx.appendAttrib(sb, "fwfamily", rdr.getReaderFirmwareFamily(), firstattrib);
				if(rdr.firmwareUpgradeProgress() >= 0) {
					ctx.appendAttrib(sb, "fwupgradeprogress", rdr.firmwareUpgradeProgress(), firstattrib);
				}
				ctx.appendAttrib(sb, "fwupgradeavail", rdr.firmwareUpgradeAvailable(), firstattrib);
                for(ReaderChannel rdrch : rdr.getChannels()) {
                    ctx.appendAttrib(sb, "noise" + rdrch.getRelativeChannelID(),
                        rdrch.getChannelNoiseLevel(), false);
                    if(rdrch.getChannelEventsPerSec() >= 0)
                        ctx.appendAttrib(sb, "evtpersec" + rdrch.getRelativeChannelID(),
                            rdrch.getChannelEventsPerSec(), false);
                }
                if(rdr.getUsedTagCapacityPercent() >= 0)
                    ctx.appendAttrib(sb, "tagcapused", rdr.getUsedTagCapacityPercent(), false);
                if (!just_status) {
                    TreeSet<String> grpids = new TreeSet<String>();
                    /* List the groups */
                    TagGroup[] grps = rdr.getReaderTagGroups();
                    for (int i = 0; i < grps.length; i++) {
                        grpids.add(grps[i].getID());
                    }
                    ctx.appendAttrib(sb, "groups", grpids, false);
                    /* List the channels */
                    grpids.clear();
                    for(ReaderChannel chan : rdr.getChannels()) {
                        grpids.add(chan.getID());
                    }
                    ctx.appendAttrib(sb, "channels", grpids, false);
                    /* Add the reader type */
                    ctx.appendAttrib(sb, "type", rdr.getReaderFactory().getID(), false);
                }
				/* Get connected address for reader, if connected */
				InetAddress rmtaddr = rdr.getRemoteAddress();
				if(rmtaddr != null) {
                    ctx.appendAttrib(sb, "remoteaddr", rmtaddr.getHostAddress(), false);
                    // Not safe for customers with broken reverse DNS lookup (e.g. CC)
					//ctx.appendAttrib(sb, "remoteaddr", rmtaddr.getCanonicalHostName(), false);
				}
				GPS gps = rdr.getGPS();
				if((gps != null) && (gps.getGPSData() != null)) {
					ctx.appendAttrib(sb, "gpsfix", gps.getGPSData().getGPSFix().toString(), false);
				}
                /* Get reader startup time, if available */
                if(rdr.getReaderStartupTime() > 0) {
                    ctx.appendAttrib(sb, "startuptime", Long.valueOf(rdr.getReaderStartupTime()), false);
                }
                ctx.appendAttrib(sb, "encrypted", rdr.getReaderConnectionEncrypted(), false);
                ctx.appendAttrib(sb, "label", rdr.getLabel(), false);

                ctx.appendEndObject(sb, "reader", rdr.getID());
            }
            else {
                sb.append(RangerServer.encodeForCSV(key)).append(",").
                    append(RangerServer.encodeForCSV(rdr.getReaderFactory().getID()));
                sb.append(",").append(rdr.getReaderEnabled()?"true":"false");
                /* List the groups */
                StringBuilder glist = new StringBuilder();
                TagGroup[] grps = rdr.getReaderTagGroups();
                for (int i = 0; i < grps.length; i++) {
                    if(i != 0) glist.append(",");
                    glist.append(grps[i].getID());
                }
                sb.append(",").append(RangerServer.encodeForCSV(glist.toString()));
                sb.append(",").append(RangerServer.encodeForCSV(rdr.getLabel()));
                /* List attributes */
                Map<String, Object> v = rdr.getReaderAttributes();
                for(String col : colids) {
                    Object val = v.get(col);    /* Find attrib, if any */
                    sb.append(",");
                    if(val != null) {
                        sb.append(RangerServer.encodeForCSV(val));
                    }
                }
                sb.append("\n");
            }
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        if(!do_exp) {
            ctx.appendEndObject(sb, "reader-list", null);
            ctx.sendString(sb.toString());
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        Map<String,String> parms = new HashMap<String, String>();

        /* Add the first one, if defined */
        if(tokenlist.size() > 0)
            parms.put(READERID, tokenlist.get(0));

        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if (cmd.equals(READERLIST_CMD))
            return READERLIST_CMD_HELP.clone();
        else if(cmd.equals(READEREXPORT_CMD))
            return READEREXPORT_CMD_HELP.clone();
        else
            return READERSTATUS_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(READERLIST_CMD))
            return READERLIST_CMD_SYNTAX_HELP.clone();
        else if(cmd.equals(READEREXPORT_CMD))
            return READEREXPORT_CMD_SYNTAX_HELP.clone();
        else
            return READERSTATUS_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
