package com.rfcode.ranger.commands;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.container.Containers;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Handles detected tags list command
 */
public class DetectedTagsHandler implements CommandHandler {
    static final String DETECTED_TAGS_LIST = "detectedtagslist";
    static final String NO_HELP = "This command is not supported";

    private static final String DETECTED_TAGS_LIST_HELP = "Returns list of tags which are detected but not assigned to any assets";
    private static final String DETECTED_TAGS_LIST_SYNTAX_HELP = "This command doesn't support any parameters";

    @Override
    public void invokeCommand(String cmd, CommandContext ctx, Map<String, String> parms) throws CommandException {

        if (!DETECTED_TAGS_LIST.equals(cmd)) {
            ctx.sendString(NO_HELP);
            return;
        }

        Set<String> allTags = new HashSet<>();

        for (String tagGroupId : ReaderEntityDirectory.getTagGroupIDs()) {
            TagGroup tg = ReaderEntityDirectory.findTagGroup(tagGroupId);
            allTags.addAll(tg.getTags().keySet());
        }

        allTags.removeAll(Containers.assetTags().elements());

        StringBuilder sb = new StringBuilder();
        ctx.appendStartObject(sb, "detectedtags", null, true);
        ctx.appendAttrib(sb, "tags", allTags, true);
        ctx.appendEndObject(sb, "detectedtags", null);
        ctx.sendString(sb.toString());
    }

    @Override
    public Map<String, String> translateCommandLine(String cmd, List<String> tokenlist) throws CommandParameterException {
        return Collections.emptyMap();
    }

    @Override
    public String[] getCommandIDs() {
        return new String[]{DETECTED_TAGS_LIST};
    }

    @Override
    public String[] getCommandHelp(String cmd) {
        switch (cmd) {
            case DETECTED_TAGS_LIST:
                return new String[]{DETECTED_TAGS_LIST_HELP};
            default:
                return new String[]{NO_HELP};
        }

    }

    @Override
    public String[] getCommandSyntaxHelp(String cmd) {
        switch (cmd) {
            case DETECTED_TAGS_LIST:
                return new String[]{DETECTED_TAGS_LIST_SYNTAX_HELP};
            default:
                return new String[]{NO_HELP};
        }
    }

    @Override
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return getCommandSyntaxHelp(cmd);
    }

    @Override
    public boolean isAdminRequired() {
        return false;
    }

    @Override
    public boolean isConfigReadRequired() {
        return false;
    }
}
