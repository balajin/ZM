package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Properties;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "configlist" command
 * 
 * @author Mike Primm
 */
public class ConfigListHandler implements CommandHandler {
    public static final String CFGLIST_CMD = "configlist";
    /* Command ID */
    public static final String[] CMD_IDS = {CFGLIST_CMD};
    /* Command help */
    private static final String[] CFGLIST_CMD_HELP = {"List all server settings."};
    /* Command syntax help */
    private static final String[] CFGLIST_CMD_SYNTAX_HELP = {CFGLIST_CMD};
    private static final String[] CFGLIST_CMD_WEB_SYNTAX_HELP = {""};
    
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Properties p = RangerServer.getCustomConfig();
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        
        /* Start config object list*/
        ctx.appendStartObject(sb, "config-list", null, true);
        
        Set<String> keys = new TreeSet<String>(p.stringPropertyNames());
        for (String id : keys) {
            String val = p.getProperty(id);
            ctx.appendStartObject(sb, "config", id, first);        
            ctx.appendAttrib(sb, "value", val, true);
            ctx.appendEndObject(sb, "config", id);
            first = false;
        }
        ctx.appendEndObject(sb, "config-list", null);
        sb.append("\n");
        /* Output completed line */
        ctx.sendString(sb.toString());
        sb.setLength(0);
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CFGLIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CFGLIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CFGLIST_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
