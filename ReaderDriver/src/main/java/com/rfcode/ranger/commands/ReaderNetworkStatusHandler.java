package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderNetworkInterfaceStatusListener;
import com.rfcode.drivers.readers.ReaderNetworkInterface;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "readernetstat" command
 *
 * @author Mike Primm
 */
public class ReaderNetworkStatusHandler implements CommandHandler {
    /* Parameter for reader ID */
    public static final String READERID = "id";
    /* Command ID */
    public static final String READERNETSTAT_CMD = "readernetstat";
    public static final String[] CMD_IDS = {READERNETSTAT_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Get reader network interface status, if supported."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {READERNETSTAT_CMD+" <reader-id>"};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        READERID+"=<reader-id>"};

    private class ResponseHandler implements ReaderNetworkInterfaceStatusListener {
        CommandContext ctx;
        String id;
        public void readerNetworkInterfaceResponse(Reader reader, ReaderNetworkInterface[] interfaces) {
            StringBuilder sb = new StringBuilder();
            if(interfaces == null) {    /* If failed */
                ctx.commandCompleted(new CommandException("Request to reader failed"));
                return;
            }
            ctx.appendStartObject(sb, "netinterface-list", id, true);
            boolean isfirst = true;
            for(ReaderNetworkInterface ni : interfaces) {
                if(ni == null) continue;
                ctx.appendStartObject(sb, "netinterface", ni.getName(), isfirst);
                isfirst = false;
                ctx.appendAttrib(sb, "rxbytes", Long.valueOf(ni.getTotalRXBytes()), true);
                ctx.appendAttrib(sb, "txbytes", Long.valueOf(ni.getTotalTXBytes()), false);
                if(ni.getIPv4Addresses() != null) {
                    ArrayList<String> lst = new ArrayList<String>();
                    for(String s : ni.getIPv4Addresses()) lst.add(s);
                    ctx.appendAttrib(sb, "ipv4address", lst, false);
                }
                if(ni.getIPv6Addresses() != null) {
                    ArrayList<String> lst = new ArrayList<String>();
                    for(String s : ni.getIPv6Addresses()) lst.add(s);
                    ctx.appendAttrib(sb, "ipv6address", lst, false);
                }
                if(ni.getHWAddress() != null) {
                    ctx.appendAttrib(sb, "hwaddress", ni.getHWAddress(), false);
                }
                ctx.appendAttrib(sb, "up", Boolean.valueOf(ni.isUp()), false);
                if(ni.getIPv4Gateway() != null) {
                    ctx.appendAttrib(sb, "ipv4gateway", ni.getIPv4Gateway(), false);
                }
                if(ni.getIPv6Gateway() != null) {
                    ctx.appendAttrib(sb, "ipv6gateway", ni.getIPv6Gateway(), false);
                }
                ctx.appendEndObject(sb, "netinterface", ni.getName());
            }
            ctx.appendEndObject(sb, "netinterface-list", id);
            CommandException cx = null;
            try {
                ctx.sendString(sb.toString());
            } catch (CommandException c_x) {
                cx = c_x;
            }
            ctx.commandCompleted(cx);
        }
    }
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.remove(READERID); /* Fetch our ID parm */
        if (id == null)
            throw new CommandParameterException("Missing reader ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* See if ID exists */
        Reader rdr = ReaderEntityDirectory.findReader(id);
        if (rdr == null)
            throw new CommandParameterException("Invalid reader ID");
        if (ReaderStatus.isOnline(rdr.getReaderStatus()) == false)
            throw new CommandParameterException("Reader is not online");
        ResponseHandler rh = new ResponseHandler();
        rh.ctx = ctx;
        rh.id = id;
        ctx.commandStillInProgress();
        rdr.requestReaderNetworkInterfaceStatus(rh);
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing reader ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(READERID, tokenlist.get(0));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
