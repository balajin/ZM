package com.rfcode.ranger.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "ruleactivate" and "ruledeactivate" commands
 *
 * @author Mike Primm
 */
public class RuleActivateHandler implements CommandHandler {
    public static final String RULEACTIVATE_CMD = "ruleactivate";
    public static final String RULEDEACTIVATE_CMD = "ruledeactivate";
    /* Parameter for rule ID */
    public static final String RULEID = "id";
    /* Command ID */
    public static final String[] CMD_IDS = {RULEACTIVATE_CMD,
        RULEDEACTIVATE_CMD};
    /* Command help */
    private static final String[] ACTIVATE_CMD_HELP = {"Activates a location rule instance."};
    private static final String[] DEACTIVATE_CMD_HELP = {"Deactivates a location rule instance."};
    /* Command syntax help */
    private static final String[] ACTIVATE_CMD_SYNTAX_HELP = { RULEACTIVATE_CMD + " <rule-id>"};
    private static final String[] DEACTIVATE_CMD_SYNTAX_HELP = { RULEDEACTIVATE_CMD + " <rule-id>"};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        RULEID+"=<rule-id>"};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {

        String id = parms.get(RULEID);
        if (id == null)
            throw new CommandParameterException("Missing rule ID");

        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = ReaderEntityDirectory.getLocationRuleIDs();
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String ruleid : ids) {
                try {
                    parms.put(RULEID, ruleid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        boolean activate = false;
        if (cmd.equals(RULEACTIVATE_CMD))
            activate = true;

        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* See if ID valid */
        LocationRule rule = ReaderEntityDirectory.findLocationRule(id);
        if (rule == null)
            throw new CommandParameterException("Invalid rule ID");
        if (rule.getLocationRuleFactory() == null)
            throw new CommandParameterException("Internal rule ID");
        /* If not target state */
        if (rule.getRuleEnabled() == (!activate)) {
            rule.setRuleEnabled(activate);
            RangerServer.saveLocationRule(rule);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing rule ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first */
        parms.put(RULEID, tokenlist.get(0));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if (cmd.equals(RULEACTIVATE_CMD))
            return ACTIVATE_CMD_HELP.clone();
        else
            return DEACTIVATE_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(RULEACTIVATE_CMD))
            return ACTIVATE_CMD_SYNTAX_HELP.clone();
        else
            return DEACTIVATE_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
