package com.rfcode.ranger.commands;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.container.Containers;

import java.util.*;

/**
 * {@link CommandHandler} implementation.
 * Designed to manage {@link Containers#tagsLinkedToAsset} container.
 * <p>
 * Supports next commands:
 * <ul>
 * <li>assettagssetadd - adds new tag id to container</li>
 * <li>assettagssetremove - removes tag id from container, command supports '*' which means all tags</li>
 * <li>assettagssetlist - list all elements in container</li>
 * </ul>
 */
public class AssetTagsSetHandler implements CommandHandler {
    static final String ASSET_TAGS_SET_ADD = "assettagssetadd";
    static final String ASSET_TAGS_SET_REMOVE = "assettagssetremove";
    static final String ASSET_TAGS_SET_LIST = "assettagssetlist";
    private static final String[] ASSET_TAGS_CMDS = new String[]{ASSET_TAGS_SET_ADD, ASSET_TAGS_SET_REMOVE, ASSET_TAGS_SET_LIST};

    private static final String[] CMD_ADD_REMOVE_SYNTAX_HELP = new String[]{"<TAG_ID>,<TAG_ID>"};
    private static final String[] NO_SYNTAX_HELP = new String[]{"Doesn't support command line arguments"};
    private static final String[] EMPTY_RESPONSE = new String[]{};

    private static final String TAGS_MAP_DEFAULT_VALUE = "";
    private static final String ALL = "*";

    @Override
    public void invokeCommand(String cmdStr, CommandContext ctx, Map<String, String> parms) throws CommandException {
        Optional.ofNullable(cmdStr).ifPresent(cmd -> {
            switch (cmd.toLowerCase()) {
                case ASSET_TAGS_SET_ADD:
                    parms.keySet().forEach(Containers.assetTags()::put);
                    RangerServer.saveAssetTagsSet();
                    break;
                case ASSET_TAGS_SET_REMOVE:
                    if (parms.size() == 1 && parms.keySet().contains(ALL)) {
                        Containers.assetTags().removeAll();
                    } else {
                        parms.keySet().forEach(Containers.assetTags()::remove);
                    }
                    RangerServer.saveAssetTagsSet();
                    break;
                case ASSET_TAGS_SET_LIST:
                    StringBuilder sb = new StringBuilder();
                    ctx.appendStartObject(sb, "assettagsset", null, true);
                    ctx.appendAttrib(sb, "tags", Containers.assetTags().elements(), true);
                    ctx.appendEndObject(sb, "assettagsset", null);
                    ctx.sendString(sb.toString());
                    break;
            }
        });
    }

    @Override
    public Map<String, String> translateCommandLine(String cmdStr, List<String> tokenlist) throws CommandParameterException {
        return Optional.ofNullable(cmdStr).map(cmd -> {
            switch (cmd.toLowerCase()) {
                case ASSET_TAGS_SET_ADD:
                case ASSET_TAGS_SET_REMOVE:
                    return Optional.ofNullable(tokenlist)
                            .map(l -> l.isEmpty() ? null : l.get(0))
                            .map(tagsListStr -> {
                                Map<String, String> tags = new HashMap<>();
                                for (String s1 : tagsListStr.split(",")) {
                                    tags.put(s1.trim(), TAGS_MAP_DEFAULT_VALUE);
                                }
                                return tags;
                            }).orElse(Collections.emptyMap());
                default:
                    return Collections.<String, String>emptyMap();
            }
        }).orElse(Collections.emptyMap());
    }

    @Override
    public String[] getCommandIDs() {
        return ASSET_TAGS_CMDS.clone();
    }

    @Override
    public String[] getCommandHelp(String cmd) {
        return Optional.ofNullable(cmd).map(s -> {
            switch (s.toLowerCase()) {
                case ASSET_TAGS_SET_ADD:
                    return toArray("Add new asset tag");
                case ASSET_TAGS_SET_REMOVE:
                    return toArray("Remove asset tag");
                case ASSET_TAGS_SET_LIST:
                    return toArray("List all asset tags");
                default:
                    return EMPTY_RESPONSE;
            }
        }).orElse(EMPTY_RESPONSE);
    }

    @Override
    public String[] getCommandSyntaxHelp(String cmd) {
        return Optional.ofNullable(cmd).map(s -> {
            switch (s.toLowerCase()) {
                case ASSET_TAGS_SET_ADD:
                case ASSET_TAGS_SET_REMOVE:
                    return CMD_ADD_REMOVE_SYNTAX_HELP;

                case ASSET_TAGS_SET_LIST:
                    return NO_SYNTAX_HELP;

                default:
                    return EMPTY_RESPONSE;
            }
        }).orElse(EMPTY_RESPONSE);
    }

    @Override
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return getCommandSyntaxHelp(cmd);
    }

    @Override
    public boolean isAdminRequired() {
        return false;
    }

    @Override
    public boolean isConfigReadRequired() {
        return false;
    }

    private <T> T[] toArray(T... vals) {
        return vals;
    }
}
