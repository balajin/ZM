package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "sensordefset" command
 *
 * @author Mike Primm
 */
public class SensorDefSetHandler implements CommandHandler {
    /* Parameter for sensor definition ID */
    public static final String SENSORDEFID = "id";
    /* Command ID */
    public static final String SENSORDEFSET_CMD = "sensordefset";
    public static final String[] CMD_IDS = {SENSORDEFSET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Update one or more settings of an existing sensor definition."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        SENSORDEFSET_CMD+" <def-id> <attrib-id>=<new-value> ...",
        SENSORDEFSET_CMD+" <def-id>,<def-id>,... <attrib-id>=<new-value> ...",
        SENSORDEFSET_CMD+" * <attrib-id>=<new-value> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        SENSORDEFID+"=<def-id>&<attrib-id>=<new-value>&...",
        SENSORDEFID+"=<def-id>,<def-id>,...&<attrib-id>=<new-value>&...",
        SENSORDEFID+"=*&<attrib-id>=<new-value>&...",
        };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;
        String id = parms.remove(SENSORDEFID);
        if (id == null)
            throw new CommandParameterException("Missing sensor defintion ID");

        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = new HashSet<String>(ReaderEntityDirectory.getSensorDefinitionIDs());
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String grpid : ids) {
                try {
                    parms.put(SENSORDEFID, grpid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Find the selected sensor definition */
        SensorDefinition def = ReaderEntityDirectory.findSensorDefinition(id);
        if (def == null) /* Doesn't exist */
            throw new CommandParameterException("Invalid sensor definition ID");
        if (parms.size() == 0) /* No more parameters? */
            throw new CommandParameterException("Missing attribute id=value");
        /* Get copy of attributes */
        Map<String, Object> attr = new HashMap<String,Object>(def.getAttributes());
        String lbl = null;
        String dest = null;
        /* Loop through the parameters */
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            String attrid = ent.getKey();
            String aval = ent.getValue();
            /* If label, handle specially */
            if(attrid.equals("label")) {
                lbl = aval;
                continue;
            }
            /* If destination, handle specially */
            if(attrid.equals("dest")) {
                dest = aval;
                continue;
            }
            /* Get current attribute value */
            Object v = attr.get(attrid);
            /* If not defined, invalid attrib */
            if (v == null)
                throw new CommandParameterException("Bad attribute ID - "
                    + attrid);
            /* Else, if its an integer */
            else if (v instanceof Integer) {
                try {
                    Integer ival = Integer.parseInt(aval);
                    /* Update value */
                    attr.put(attrid, ival);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad integer attribute value - " + aval);
                }
			}
            /* Else, if its an double */
            else if (v instanceof Double) {
                try {
                    Double dval = Double.parseDouble(aval);
                    /* Update value */
                    attr.put(attrid, dval);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad double attribute value - " + aval);
                }
            }
            else { /* Else, put in a string */
                attr.put(attrid, aval);
                update = true;
            }
        }
        /* Set the tag group's attributes */
        if (update) {
            try {
                def.setAttributes(attr);
            } catch (BadParameterException e) {
                throw new CommandParameterException(e.getMessage());
            }
            RangerServer.saveSensorDefinition(def);
        }
        /* Apply label change */
        if(lbl != null) {
            def.setLabel(lbl);
            RangerServer.saveSensorDefinition(def);
        }
        /* Apply destination change */
        if(dest != null) {
            def.setDestinationAttrib(dest);
            RangerServer.saveSensorDefinition(def);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing sensor definition ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(SENSORDEFID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
