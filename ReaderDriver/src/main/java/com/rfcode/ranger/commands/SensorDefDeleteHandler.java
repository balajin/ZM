package com.rfcode.ranger.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "sensordefdelete" command
 *
 * @author Mike Primm
 */
public class SensorDefDeleteHandler implements CommandHandler {
    /* Parameter for sensor definition ID */
    public static final String SENSORDEFID = "id";
    /* Command ID */
    public static final String SENSORDEFDELETE_CMD = "sensordefdelete";
    public static final String[] CMD_IDS = {SENSORDEFDELETE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Delete existing sensor definition."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        SENSORDEFDELETE_CMD+" <def-id>",
        SENSORDEFDELETE_CMD+" *"};
    /* Command web syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        SENSORDEFID + "=<def-id>",
        SENSORDEFID + "=*"
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.get(SENSORDEFID);
        if (id == null)
            throw new CommandParameterException("Missing sensor definition ID");
        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = new HashSet<String>(ReaderEntityDirectory.getSensorDefinitionIDs());
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String grpid : ids) {
                try {
                    parms.put(SENSORDEFID, grpid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Find the selected sensor definition */
        SensorDefinition def = ReaderEntityDirectory.findSensorDefinition(id);
        if (def == null) /* Doesn't exist */
            throw new CommandParameterException("Invalid sensor definition ID");
        RangerServer.deleteSensorDefintion(def); // Delete the file
        def.cleanup();
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing sensor definition ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(SENSORDEFID, tokenlist.get(0));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
