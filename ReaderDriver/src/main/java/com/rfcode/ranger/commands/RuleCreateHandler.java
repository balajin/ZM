package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "rulecreate" command
 *
 * @author Mike Primm
 */
public class RuleCreateHandler implements CommandHandler {
    public static final String RULECREATE_CMD = "rulecreate";
    /* Parameter for rule ID */
    public static final String RULEID = "id";
    /* Parameter ID for rule type */
    public static final String RULETYPE = "type";
    /* Parameter ID for rule target location */
    public static final String LOCATIONID = "locid";
    /* Command ID */
    public static final String[] CMD_IDS = { RULECREATE_CMD };
    /* Command help */
    private static final String[] CMD_HELP = {"Create new location rule instance of given type with given target location.  Rule is initially disabled."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        RULECREATE_CMD + " <rule-id> <rule-type> <location-id> <attribid>=<attribval> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        RULEID + "=<rule-id>&"+RULETYPE+"=<rule-type>&"+
        LOCATIONID+"=<location-id>&<attribid>=<attribval>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.remove(RULEID);
        if (id == null || id.length() == 0)
            throw new CommandParameterException("Missing rule ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String type = parms.remove(RULETYPE);
        if (type == null)
            throw new CommandParameterException("Missing rule type");
        if (!RangerServer.testIfValidID(type))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String locid = parms.remove(LOCATIONID);
        if (locid == null)
            throw new CommandParameterException("Missing location ID");
        if (!RangerServer.testIfValidID(locid))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Pass to create method */
        String[] prms = new String[parms.size()];
        int i = 0;
        for(Map.Entry<String,String> ent : parms.entrySet()) {
            prms[i] = ent.getKey() + "=" + ent.getValue();
            i++;
        }
        String errmsg = RangerServer.createLocationRule(id, type, locid, prms);
        if (errmsg != null) {
            throw new CommandException(errmsg);
        } else { /* If successful, save it to file */
            LocationRule lr = ReaderEntityDirectory.findLocationRule(id);
            if (lr != null)
                RangerServer.saveLocationRule(lr);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing rule ID");
        } else if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing rule type");
        } else if (tokenlist.size() < 3) {
            throw new CommandParameterException("Missing location ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first three */
        parms.put(RULEID, tokenlist.get(0));
        parms.put(RULETYPE, tokenlist.get(1));
        parms.put(LOCATIONID, tokenlist.get(2));
        /* Add others as listed attributes */
        for (int i = 3; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
