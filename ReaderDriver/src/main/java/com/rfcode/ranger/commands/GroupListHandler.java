package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.LinkedHashSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.CommandParameterException;
/**
 * Handler for "grouplist" command
 * 
 * @author Mike Primm
 */
public class GroupListHandler implements CommandHandler {
    public static final String GROUPLIST_CMD = "grouplist";
    public static final String GROUPLIST2_CMD = "grouplist2";
    public static final String GROUPEXPORT_CMD = "groupexport";
    /* Command ID */
    public static final String[] CMD_IDS = {GROUPLIST_CMD, GROUPLIST2_CMD, GROUPEXPORT_CMD};
    /* Command help */
    private static final String[] GROUPLIST_CMD_HELP = {"List all groups and their settings."};
    private static final String[] GROUPLIST2_CMD_HELP = {"List all groups and their settings, including subgroups."};
    private static final String[] GROUPEXPORT_CMD_HELP = {"Export all groups, in importable CVS format."};
    /* Command syntax help */
    private static final String[] GROUPLIST_CMD_SYNTAX_HELP = {GROUPLIST_CMD};
    private static final String[] GROUPLIST2_CMD_SYNTAX_HELP = {GROUPLIST2_CMD};
    private static final String[] GROUPEXPORT_CMD_SYNTAX_HELP = {GROUPEXPORT_CMD};
    /* Command web syntax help */
    private static final String[] GROUPLIST_CMD_WEB_SYNTAX_HELP = { "" };
    private static final String[] GROUPLIST2_CMD_WEB_SYNTAX_HELP = { "" };
    private static final String[] GROUPEXPORT_CMD_WEB_SYNTAX_HELP = { "" };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        TagGroup tg;
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getTagGroupIDs());
        StringBuilder sb = new StringBuilder();
        boolean do_exp = false;
        Set<String> colids = null;
		boolean inc_subgrp = cmd.equals(GROUPLIST2_CMD);
        
        if(cmd.equals(GROUPEXPORT_CMD)) {
            do_exp = true;
            //ctx.setContentType("text/x-excel-csv");
            /* Build column set of distinct attribtues */
            colids = new LinkedHashSet<String>();
            for(String key : keys) {
                tg = ReaderEntityDirectory.findTagGroup(key);
				if(!tg.isSubGroup())
	                colids.addAll(tg.getTagGroupAttributes().keySet());
            }
            /* Output first line for CVS */
            sb.append("group.id,group.type,group.label");
            for(String col : colids) {
                sb.append(",group.");
                sb.append(col);
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        /* Start group list */
        if(!do_exp)
            ctx.appendStartObject(sb, "group-list", null, true);
        boolean first = true;
        for (String key : keys) {
            tg = ReaderEntityDirectory.findTagGroup(key);
			if((!inc_subgrp) && tg.isSubGroup())
				continue;
            Map<String, Object> v = tg.getTagGroupAttributes();
            if(!do_exp) {
                Object val;
                ctx.appendStartObject(sb, "group", key, first);
                ctx.appendAttrib(sb, "type", tg.getTagType(), true);
                ctx.appendAttrib(sb, "label", tg.getLabel(), false);
                String[] k = tg.getTagType().getTagGroupAttributes();
                for (int i = 0; i < k.length; i++) {
                    val = v.get(k[i]); /* Get attribute value */
                    ctx.appendAttrib(sb, k[i], val, false);
                }
                val = v.get(TagGroup.ATTRIB_LOC_UPD_DELAY); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_LOC_UPD_DELAY, val, false);
                val = v.get(TagGroup.ATTRIB_BLK_ATTR_UPD_LIST); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_BLK_ATTR_UPD_LIST, val, false);
                val = v.get(TagGroup.ATTRIB_ENH_PAYLOAD_VERIFY); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_ENH_PAYLOAD_VERIFY, val, false);
                val = v.get(TagGroup.ATTRIB_GROUP_TIMEOUT); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_GROUP_TIMEOUT, val, false);
                val = v.get(TagGroup.ATTRIB_LOC_MATCH_BOOST); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_LOC_MATCH_BOOST, val, false);
                val = v.get(TagGroup.ATTRIB_LOC_MATCH_TIME); /* Get attribute value */
				if(val != null)
	                ctx.appendAttrib(sb, TagGroup.ATTRIB_LOC_MATCH_TIME, val, false);
				if(tg.getParentGroup() != null)
					ctx.appendAttrib(sb, TagGroup.ATTRIB_PAR_GROUP, 
						tg.getParentGroup().getID(), false);
                ctx.appendEndObject(sb, "group", key);
            }
            else {
                sb.append(RangerServer.encodeForCSV(key)).append(",").
                    append(RangerServer.encodeForCSV(tg.getTagType().getID())).append(",")
                    .append(RangerServer.encodeForCSV(tg.getLabel()));
                for(String col : colids) {
                    Object val = v.get(col);    /* Find attrib, if any */
                    sb.append(",");
                    if(val != null) {
                        sb.append(RangerServer.encodeForCSV(val));
                    }
                }
                sb.append("\n");
            }
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            first = false;
        }
        if(!do_exp) {
            ctx.appendEndObject(sb, "group-list", null);
            ctx.sendString(sb.toString());
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(GROUPLIST_CMD))
            return GROUPLIST_CMD_HELP.clone();
        else if(cmd.equals(GROUPLIST2_CMD))
            return GROUPLIST2_CMD_HELP.clone();
        else
            return GROUPEXPORT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(GROUPLIST_CMD))
            return GROUPLIST_CMD_SYNTAX_HELP.clone();
        else if(cmd.equals(GROUPLIST2_CMD))
            return GROUPLIST2_CMD_SYNTAX_HELP.clone();
        else
            return GROUPEXPORT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if(cmd.equals(GROUPLIST_CMD))
            return GROUPLIST_CMD_WEB_SYNTAX_HELP.clone();
        else if(cmd.equals(GROUPLIST2_CMD))
            return GROUPLIST2_CMD_WEB_SYNTAX_HELP.clone();
        else
            return GROUPEXPORT_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
