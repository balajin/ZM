package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "sensordefcreate" command
 *
 * @author Mike Primm
 */
public class SensorDefCreateHandler implements CommandHandler {
    /* Parameter for definition ID */
    public static final String SENSORDEFID = "id";
    /* Parameter ID for definition type */
    public static final String DEFTYPE = "type";
    /* Parameter ID for destination attribute */
    public static final String DEST = "dest";
    /* Parameter ID for label */
    public static final String LABEL = "label";
    /* Command ID */
    public static final String SENSORDEFCREATE_CMD = "sensordefcreate";
    public static final String[] CMD_IDS = {SENSORDEFCREATE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Create new sensor definition based on a given definition type."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        SENSORDEFCREATE_CMD + " <def-id> <def-type> <dest-attrib> <defarg>=<argval> ..."
    };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        SENSORDEFID + "=<defid>&" + DEFTYPE + "=<deftype>&" + DEST + "=<dest-attrib>&<defarg>=<argval>&..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.remove(SENSORDEFID);
        if (id == null || id.length() == 0)
            throw new CommandParameterException("Missing sensor definition ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String type = parms.remove(DEFTYPE);
        if (type == null)
            throw new CommandException("Missing sensor definition type");
		if (!SensorDefinition.isValidDefinitionType(type)) {
            throw new CommandParameterException("Invalid sensor definition type type");
		}
		String dest = parms.remove(DEST);
		if (dest == null)
            throw new CommandException("Missing destination attribute");
        if (!RangerServer.testIfValidID(dest))
            throw new CommandParameterException(CommandConstants.DEST_INVALID_CHARACTERS);
        String lbl = parms.remove(LABEL);
        /* Pass to create method */
        String[] prms = new String[parms.size()];
        int i = 0;
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            prms[i++] = ent.getKey() + "=" + ent.getValue();
        }
        String errmsg = RangerServer.createSensorDef(id, type, prms, lbl, dest);
        if (errmsg != null) {
            throw new CommandException(errmsg);
        } 
        else { /* If successful, save it to file */
            SensorDefinition sd = ReaderEntityDirectory.findSensorDefinition(id);
            if (sd != null) {
                RangerServer.saveSensorDefinition(sd);
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing sensor definition ID");
        } else if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing sensor definition type");
        } else if (tokenlist.size() < 3) {
            throw new CommandParameterException("Missing destination attribute");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(SENSORDEFID, tokenlist.get(0));
        parms.put(DEFTYPE, tokenlist.get(1));
        parms.put(DEST, tokenlist.get(2));
        /* Add others as listed attributes */
        for (int i = 3; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
