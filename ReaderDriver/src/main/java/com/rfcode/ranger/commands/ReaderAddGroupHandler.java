package com.rfcode.ranger.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "readeraddgroup" and "readerremovegroup" commands
 *
 * @author Mike Primm
 */
public class ReaderAddGroupHandler implements CommandHandler {
    public static final String READERADDGROUP_CMD = "readeraddgroup";
    public static final String READERREMOVEGROUP_CMD = "readerremovegroup";
    /* Parameter for reader ID */
    public static final String READERID = "id";
    /* Parameter for group ID prefix (followed by 0-based index) */
    public static final String GROUPID_PREFIX = "groupid";
    /* Command ID */
    public static final String[] CMD_IDS = {READERADDGROUP_CMD,
        READERREMOVEGROUP_CMD};
    /* Command help */
    private static final String[] ADDGROUP_CMD_HELP = {"Configure a reader to monitor tags defined by a given tag group."};
    private static final String[] REMOVEGROUP_CMD_HELP = {"Configure a reader to stop monitoring tags defined by a given tag group."};
    /* Command syntax help */
    private static final String[] ADDGROUP_CMD_SYNTAX_HELP = {
        READERADDGROUP_CMD + " <reader-id> <group-id>",
        READERADDGROUP_CMD + " * <group-id>",
        READERADDGROUP_CMD + " <reader-id>,<reader-id>,... <group-id>"};
    private static final String[] REMOVEGROUP_CMD_SYNTAX_HELP = {
        READERREMOVEGROUP_CMD + " <reader-id> <group-id>",
        READERREMOVEGROUP_CMD + " * <group-id>",
        READERREMOVEGROUP_CMD + " <reader-id>,<reader-id>,... <group-id>"};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        READERID+"=<reader-id>&"+GROUPID_PREFIX+"0=<group-id>&" +
        GROUPID_PREFIX+"1=<group-id>&...",
        READERID+"=*&"+GROUPID_PREFIX+"0=<group-id>&" +
        GROUPID_PREFIX+"1=<group-id>&...",
        READERID+"=<reader-id>,<reader-id>,...&"+GROUPID_PREFIX+"0=<group-id>&" +
        GROUPID_PREFIX+"1=<group-id>&..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean do_add = false;
        if (cmd.equals(READERADDGROUP_CMD))
            do_add = true;
        String id = parms.remove(READERID);

        /* If first group ID is wildcard, substitute in all group IDs */
        String gid0 = parms.get(GROUPID_PREFIX + "0");
        if((gid0 != null) && (gid0.equals("*"))) {
            int i = 0;
            parms.remove(GROUPID_PREFIX + "0");
            for(String gid : ReaderEntityDirectory.getTagGroupIDs()) {
	            TagGroup tg = ReaderEntityDirectory.findTagGroup(gid);
				if((tg != null) && (tg.isSubGroup() == false)) {
	                parms.put(GROUPID_PREFIX + i, gid);
	                i++;
				}
            }
        }

        if (id == null)
            throw new CommandParameterException("Missing reader ID");

        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = ReaderEntityDirectory.getReaderIDs();
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String rdrid : ids) {
                try {
                    parms.put(READERID, rdrid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }

        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Find reader */
        Reader rdr = ReaderEntityDirectory.findReader(id);
        if (rdr == null)
            throw new CommandParameterException("Invalid reader ID");
        /* Get existing group list for reader */
        TagGroup[] oldset = rdr.getReaderTagGroups();
        TagGroup[] newset = oldset;
        /* Loop through the groups */
        String tgid;
        boolean changed = false;
        for (int i = 0; (tgid = parms.get(GROUPID_PREFIX + i)) != null; i++) {
            if (!RangerServer.testIfValidID(tgid))
                throw new CommandParameterException(
                    "Tag group ID can only contain alphanumeric or '$', '-', and '_' characters");
            /* Find tag group */
            TagGroup tg = ReaderEntityDirectory.findTagGroup(tgid);
            if (tg == null)
                throw new CommandParameterException("Invalid tag group ID");
			if(tg.isSubGroup()) {	/* Can't add subgroups to readers */
                throw new CommandParameterException("Cannot add subgroup");
  			}				
            if (do_add) {
                /* Make sure we're not already in the list */
                boolean is_dup = false;
                for (int j = 0; j < newset.length; j++) {
                    /* Already in set? we're done */
                    if (newset[j].getID().equals(tgid)) {
                        is_dup = true;
                    }
                }
                if (is_dup)
                    continue;
                /* Make sure we're not too many */
                if (newset.length >= rdr.getReaderMaxTagGroupCount())
                    throw new CommandParameterException(
                        "Max tag groups for reader");
                oldset = newset;
                newset = new TagGroup[oldset.length + 1];
                System.arraycopy(oldset, 0, newset, 0, oldset.length);
                newset[oldset.length] = tg; /* Append new group */
                changed = true;
            } else { /* Else, remove group */
                int idx = -1;
                int j, k;
                /* Find us in the list */
                for (j = 0; j < newset.length; j++) {
                    if (newset[j].getID().equals(tgid)) {
                        idx = j;
                        break;
                    }
                }
                /* If not found, quit (already gone) */
                if (idx < 0) {
                    continue;
                }
                /* Make smaller set */
                oldset = newset;
                newset = new TagGroup[oldset.length - 1];
                for (j = 0, k = 0; j < oldset.length; j++) {
                    if (j == idx)
                        continue; /* Skip */
                    newset[k] = oldset[j];
                    k++;
                }
                changed = true;
            }
        }
        /* And set it */
        if(changed) {
            try {
                rdr.setReaderTagGroups(newset);
                RangerServer.saveReader(rdr);
            } catch (BadParameterException bpx) {
                throw new CommandException(bpx.getMessage());
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing reader ID");
        }
        if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing tag group ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(READERID, tokenlist.get(0));
        for (int i = 1; i < tokenlist.size(); i++)
            parms.put(GROUPID_PREFIX + (i - 1), tokenlist.get(i));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if (cmd.equals(READERADDGROUP_CMD))
            return ADDGROUP_CMD_HELP.clone();
        else
            return REMOVEGROUP_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(READERADDGROUP_CMD))
            return ADDGROUP_CMD_SYNTAX_HELP.clone();
        else
            return REMOVEGROUP_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
