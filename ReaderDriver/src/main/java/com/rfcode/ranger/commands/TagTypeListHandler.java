package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "tagtypelist" command
 * 
 * @author Mike Primm
 */
public class TagTypeListHandler implements CommandHandler {
    public static final String TAGTYPELIST_CMD = "tagtypelist";
    public static final String TAGTYPELIST2_CMD = "tagtypelist2";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGTYPELIST_CMD,TAGTYPELIST2_CMD};
    /* Command help */
    private static final String[] TAGTYPELIST_CMD_HELP = {"List all tag types and their attributes."};
    private static final String[] TAGTYPELIST2_CMD_HELP = {"List all tag types and their attributes, including subtypes."};
    /* Command syntax help */
    private static final String[] TAGTYPELIST_CMD_SYNTAX_HELP = {TAGTYPELIST_CMD};
    private static final String[] TAGTYPELIST2_CMD_SYNTAX_HELP = {TAGTYPELIST2_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getTagTypeIDs());
        StringBuilder sb = new StringBuilder();
		boolean inc_subtype = cmd.equals(TAGTYPELIST2_CMD);
        
        /* Start tag type list */
        ctx.appendStartObject(sb, "tag-type-list", null, true);
        boolean first = true;
        for (String key : keys) {
            TagType tt = ReaderEntityDirectory.findTagType(key);
			if((inc_subtype == false) && tt.isSubType())
				continue;
            ctx.appendStartObject(sb, "tag-type", tt.getID(), first);
            ctx.appendAttrib(sb, "label", tt.getLabel(), true);
            ctx.appendAttrib(sb, "version", tt.getVersion(), false);
            ctx.appendAttrib(sb, "defgrpattribs", 
                tt.getTagGroupDefaultAttributes(), false);
            TreeSet<String> ta = new TreeSet<String>();
            for(String s: tt.getTagAttributes()) { ta.add(s); }
            ctx.appendAttrib(sb, "tagattribs", ta, false);
			if(inc_subtype) {
	            ctx.appendAttrib(sb, "issubtype", Boolean.valueOf(tt.isSubType()), false);			
				if(tt.getSubTypes() != null) {
					ta = new TreeSet<String>();
		            for(String s: tt.getSubTypes()) { ta.add(s); }
		            ctx.appendAttrib(sb, "subtypes", ta, false);
				}
			}
            ctx.appendEndObject(sb, "tag-type", tt.getID());
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            first = false;
        }
        ctx.appendEndObject(sb, "tag-type-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
		if(cmd.equals(TAGTYPELIST_CMD))
	        return TAGTYPELIST_CMD_HELP.clone();
		else
	        return TAGTYPELIST2_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
		if(cmd.equals(TAGTYPELIST_CMD))
	        return TAGTYPELIST_CMD_SYNTAX_HELP.clone();
		else
	        return TAGTYPELIST2_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
