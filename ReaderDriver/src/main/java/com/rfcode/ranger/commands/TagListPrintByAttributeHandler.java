package com.rfcode.ranger.commands;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "taglistbyattrib", "tagprintbyattrib" commands
 * 
 * @author Mike Primm
 */
public class TagListPrintByAttributeHandler implements CommandHandler {
    public static final String TAGLISTBYATTRIB_CMD = "taglistbyattrib";
    public static final String TAGPRINTBYATTRIB_CMD = "tagprintbyattrib";
    public static final String TAGCOUNTBYATTRIB_CMD = "tagcountbyattrib";
    /* Attribute ID for attribute IDs (prefix - followed by zero-based number) */
    public static final String ATTRIBID_PREFIX = "attribid";
    /* Attribute ID for attribute values (prefix - followed by zero-based number) */
    public static final String ATTRIBVAL_PREFIX = "attribval";
    /* Attrib ID for start index and limit count (result windowing) */
    public static final String  FIRSTROW_ID = "_firstrow";
    public static final String  ROWCOUNT_ID = "_rowcnt";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGLISTBYATTRIB_CMD,
        TAGPRINTBYATTRIB_CMD,TAGCOUNTBYATTRIB_CMD};
    /* Command help */
    private static final String[] TAGLISTBYATTRIB_CMD_HELP = {"List all tags matching a set of specific attribute values."};
    private static final String[] TAGPRINTBYATTRIB_CMD_HELP = {"Print details of all tags matching a set of specific attribute values."};
    private static final String[] TAGCOUNTBYATTRIB_CMD_HELP = {"Count number of tags matching a set of specific attribute values."};
    /* Command syntax help */
    private static final String[] TAGLISTBYATTRIB_CMD_SYNTAX_HELP = {
        TAGLISTBYATTRIB_CMD + " <attrid-id>=<attrib-val> ..."
        };
    private static final String[] TAGPRINTBYATTRIB_CMD_SYNTAX_HELP = {
        TAGPRINTBYATTRIB_CMD + " <attrid-id>=<attrib-val> ..."
        };
    private static final String[] TAGCOUNTBYATTRIB_CMD_SYNTAX_HELP = {
        TAGCOUNTBYATTRIB_CMD + " <attrid-id>=<attrib-val> ..."
        };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        ATTRIBID_PREFIX + "0=<attrid-id>&" + ATTRIBVAL_PREFIX +
        "0=<attrib-val>&..."
        };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Set<String> grps = ReaderEntityDirectory.getTagGroupIDs();
        /* Get our IDs and values, if defined */
        List<String> aids = new ArrayList<String>();
        List<String> avals = new ArrayList<String>();
        String aid;
        int i;
        boolean do_print = false;
        int firstrow = 0;
        int rowcnt = Integer.MAX_VALUE;
        int rowidx = 0;
        boolean do_count = false;
        
        if(cmd.equals(TAGCOUNTBYATTRIB_CMD)) {
            do_count = true;
        }
        /* Get row limits, if any */
        String p = parms.get(FIRSTROW_ID);
        if(p != null) {
            try {
                firstrow = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row index parameter");
            }
        }
        p = parms.get(ROWCOUNT_ID);
        if(p != null) {
            try {
                rowcnt = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row count parameter");
            }
        }
        
        /* Set tag list */
        if(do_count)
            ctx.appendStartObject(sb, "tag-count", null, true);
        else
            ctx.appendStartObject(sb, "tag-list", null, true);
        
        if(cmd.equals(TAGPRINTBYATTRIB_CMD)) {
            do_print = true;
        }
        for(i = 0; (aid = parms.get(ATTRIBID_PREFIX + i)) != null; i++) {
            aids.add(aid);  /* Add to list */
            avals.add(parms.get(ATTRIBVAL_PREFIX + i));
        }
        if (aids.size() < 1)
            throw new CommandParameterException("Missing attribute ID = value constraint");
        int attridx[] = new int[aids.size()];
        boolean first = true;
        for (String key : grps) {
            TagGroup tg = ReaderEntityDirectory.findTagGroup(key);
            if (tg == null)
                throw new CommandException("Invalid tag group ID");
            Map<String, Tag> tags = tg.getTags(); /* Get our tags */
            /* Get attribute indices */
            boolean missed = false;
            for(i = 0; (i < aids.size()) && (!missed); i++) {
                attridx[i] = tg.getTagType().getTagAttributeIndex(
                    aids.get(i));
                if(attridx[i] < 0) {
                    missed = true; /* Undefined for this type */
                }
            }
            if (missed) /* If any undefined, skip it */
                continue;
            TreeSet<String> keys = new TreeSet<String>(tags.keySet());
            for (String tagid : keys) {
                Tag tag = tags.get(tagid);
                missed = false;
                for(i = 0; i < attridx.length; i++) {
                    Object v = tag.readTagAttribute(attridx[i]);
                    String val = avals.get(i);
                    if(val == null) {   /* Just testing existence */
                        if(v == null)   
                            missed = true;
                    }
                    /* Else if null value or mismatch, missed too */
                    else if((v == null) || (!v.toString().equals(val))) {
                        missed = true; /* We missed */
                    }
                }
                if(!missed) {
                    /* If not in limits, skip it */
                    if((rowidx < firstrow) || ((rowidx-firstrow) >= rowcnt)) {
                    }
                    else if (do_count) {
                        /* Skip if we're just counting */
                    }
                    else if (do_print) {
                        RangerServer.printTag(ctx, sb, tag, first);
                        first = false;
                    } else {
                        ctx.appendStartObject(sb, "tag", tag.getTagGUID(), first);
                        ctx.appendAttrib(sb, "taggroupid", tag.getTagGroup(), true);
                        ctx.appendEndObject(sb, "tag", tag.getTagGUID());
                        first = false;                        
                    }
                    /* Output completed line */
                    ctx.sendString(sb.toString());
                    sb.setLength(0);
                    rowidx++;
                }
            }
        }
        if(do_count) {
            ctx.appendAttrib(sb, "count", rowidx, true);
            ctx.appendEndObject(sb, "tag-count", null);
        }
        else
            ctx.appendEndObject(sb, "tag-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        /* Others, encode the channel IDs */
        for (int i = 0; i < tokenlist.size(); i++) {
            String[] v = tokenlist.get(i).split("=");
            vals.put(ATTRIBID_PREFIX + i, v[0]);
            if(v.length > 1)
                vals.put(ATTRIBVAL_PREFIX + i, v[1]);
            else
                vals.put(ATTRIBVAL_PREFIX + i, null);
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(TAGLISTBYATTRIB_CMD))
            return TAGLISTBYATTRIB_CMD_HELP.clone();
        else if(cmd.equals(TAGCOUNTBYATTRIB_CMD))
            return TAGCOUNTBYATTRIB_CMD_HELP.clone();
        else
            return TAGPRINTBYATTRIB_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLISTBYATTRIB_CMD))
            return TAGLISTBYATTRIB_CMD_SYNTAX_HELP.clone();
        else if(cmd.equals(TAGCOUNTBYATTRIB_CMD))
            return TAGCOUNTBYATTRIB_CMD_SYNTAX_HELP.clone();
        else
            return TAGPRINTBYATTRIB_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
