package com.rfcode.ranger.commands;

public class CommandConstants {
	public static final String ID_INVALID_CHARACTERS = "ID can only contain alphanumeric or '$', '-', and '_' characters";
    public static final String DEST_INVALID_CHARACTERS = "Destination attribute ID can only contain alphanumeric or '$', '-', and '_' characters";
}
