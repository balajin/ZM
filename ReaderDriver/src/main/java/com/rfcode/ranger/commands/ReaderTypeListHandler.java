package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "readertypelist" command
 * 
 * @author Mike Primm
 */
public class ReaderTypeListHandler implements CommandHandler {
    public static final String READERTYPELIST_CMD = "readertypelist";
    /* Command ID */
    public static final String[] CMD_IDS = {READERTYPELIST_CMD};
    /* Command help */
    private static final String[] READERTYPELIST_CMD_HELP = {"List all reader types and their attributes."};
    /* Command syntax help */
    private static final String[] READERTYPELIST_CMD_SYNTAX_HELP = {READERTYPELIST_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getReaderFactoryIDs());
        
        /* Start reader type list */
        ctx.appendStartObject(sb, "reader-type-list", null, true);
        
        boolean first = true;
        for (String key : keys) {
            ReaderFactory rf = ReaderEntityDirectory.findReaderFactory(key);
            ctx.appendStartObject(sb, "reader", rf.getID(), first);
            first = false;
            ctx.appendAttrib(sb, "label", rf.getReaderFactoryLabel(), true);
            /* Build tag type list */
            TreeSet<String> ttids = new TreeSet<String>();
            String[] tt = rf.getSupportedTagTypeIDs();
            for (int i = 0; i < tt.length; i++) {
                ttids.add(tt[i]);
            }
            ctx.appendAttrib(sb, "tagtypes", ttids, false);
            ctx.appendAttrib(sb, "version", rf.getVersion(), false);
            /* Add attribute IDs and default values */
            ttids.clear();
            for(String s: rf.getReaderAttributeIDs()) ttids.add(s);
            ctx.appendAttrib(sb, "attribids", ttids, false);
            ctx.appendAttrib(sb, "defattribvals", rf.getDefaultReaderAttributes(), false);
            /* Add tag link attributes */
            ttids.clear();
            for(String s: rf.getTagLinkAttributeIDs()) ttids.add(s);
            ctx.appendAttrib(sb, "linkattribids", ttids, false);
            ctx.appendEndObject(sb, "reader", rf.getID());
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        ctx.appendEndObject(sb, "reader-type-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return READERTYPELIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return READERTYPELIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
