package com.rfcode.ranger.commands;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Date;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.util.keys.LicenseKey;
/**
 * Handler for "licenselist" command
 * 
 * @author Mike Primm
 */
public class LicenseListHandler implements CommandHandler {
    public static final String LICENSELIST_CMD = "licenselist";
    /* Command ID */
    public static final String[] CMD_IDS = {LICENSELIST_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"List all installed license keys, and license status."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {LICENSELIST_CMD};
    /* Command web syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = { "" };
    /* Date formatter */
    private static SimpleDateFormat datefmt = new SimpleDateFormat(
    "yyyy-MM-dd");
    
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Set<String> keyids = new TreeSet<String>();
        Map<String,String> expdates = new HashMap<String,String>();
        /* Make key ID list */
        for(LicenseKey k : ReaderEntityDirectory.getLicenseKeys()) {
            keyids.add(k.getKeyString());
            Date d = k.getExpirationDate();
            if(d != null) {
                expdates.put(k.getKeyString(), datefmt.format(d));
            }
        }
        
        /* Start keys list */
        ctx.appendStartObject(sb, "licenseinfo", null, true);
        int cnt = ReaderEntityDirectory.getActiveReaderLimit();
        if(cnt == Integer.MAX_VALUE)
            ctx.appendAttrib(sb, "limit",
                "unlimited", true);        
        else
            ctx.appendAttrib(sb, "limit",
                ReaderEntityDirectory.getActiveReaderLimit(), true);        
        ctx.appendAttrib(sb, "used",
            ReaderEntityDirectory.getActiveReaderCount(), false);        
        ctx.appendAttrib(sb, "keys", keyids, false);        
        ctx.appendAttrib(sb, "expdates", expdates, false);
        ctx.appendEndObject(sb, "licenseinfo", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
