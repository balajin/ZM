package com.rfcode.ranger.commands;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "groupdelete" command
 *
 * @author Mike Primm
 */
public class GroupDeleteHandler implements CommandHandler {
    /* Parameter for group ID */
    public static final String GROUPID = "id";
    /* Command ID */
    public static final String GROUPDELETE_CMD = "groupdelete";
    public static final String[] CMD_IDS = {GROUPDELETE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Delete existing tag group."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        GROUPDELETE_CMD+" <group-id>",
        GROUPDELETE_CMD+" *"};
    /* Command web syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        GROUPID + "=<group-id>",
        GROUPID + "=*"
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.get(GROUPID);
        if (id == null)
            throw new CommandParameterException("Missing group ID");
        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = new HashSet<String>(ReaderEntityDirectory.getTagGroupIDs());
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String grpid : ids) {
                try {
                    parms.put(GROUPID, grpid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Find the selected reader */
        TagGroup grp = ReaderEntityDirectory.findTagGroup(id);
        if (grp == null) /* Doesn't exist */
            throw new CommandParameterException("Invalid group ID");
		else if(grp.isSubGroup())
            throw new CommandParameterException("Cannot delete subgroup");
        /* Loop through readers, remove from any using it */
        for(Reader r : ReaderEntityDirectory.getReaders()) {
            TagGroup[] tg = r.getReaderTagGroups();
            for(int i = 0; i < tg.length; i++) {
                if(tg[i] == grp) { /* If match, need to remove */
                    TagGroup[] newtg = new TagGroup[tg.length-1];
                    if(i > 0)
                        System.arraycopy(tg, 0, newtg, 0, i);
                    if(i < (tg.length-1))
                        System.arraycopy(tg, i+1, newtg, i, tg.length-i-1);
                    try {
                        r.setReaderTagGroups(newtg);
                        RangerServer.saveReader(r);
                    } catch (BadParameterException bpx) {
                    }
                    tg = newtg;
                }
            }
        }
        RangerServer.deleteTagGroup(grp); /* Delete the file */
        grp.cleanup();
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing group ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(GROUPID, tokenlist.get(0));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
