package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "groupcreate" command
 *
 * @author Mike Primm
 */
public class GroupCreateHandler implements CommandHandler {
    /* Parameter for group ID */
    public static final String GROUPID = "id";
    /* Parameter ID for tag type */
    public static final String TAGTYPE = "type";
    /* Parameter ID for label */
    public static final String LABEL = "label";
    /* Command ID */
    public static final String GROUPCREATE_CMD = "groupcreate";
    public static final String[] CMD_IDS = {GROUPCREATE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Create new tag group based on a given tag type."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        GROUPCREATE_CMD + " <group-id> <tag-type> <grouparg>=<argval> ..."
    };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        GROUPID + "=<groupid>&" + TAGTYPE + "=<tagtype>&<grouparg>=<argval>&..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.remove(GROUPID);
        if (id == null || id.length() == 0)
            throw new CommandParameterException("Missing group ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String type = parms.remove(TAGTYPE);
        if (type == null)
            throw new CommandException("Missing tag type");
		TagType tt = ReaderEntityDirectory.findTagType(type);
        if (tt == null)
            throw new CommandParameterException("Invalid tag type");
		else if(tt.isSubType())
			throw new CommandParameterException("Cannot create group from subtype");
        String lbl = parms.remove(LABEL);
        /* Pass to create method */
        String[] prms = new String[parms.size()];
        int i = 0;
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            prms[i++] = ent.getKey() + "=" + ent.getValue();
        }
        String errmsg = RangerServer.createTagGroup(id, type, prms, lbl);
        if (errmsg != null) {
            throw new CommandException(errmsg);
        } else { /* If successful, save it to file */
            TagGroup tg = ReaderEntityDirectory.findTagGroup(id);
            if (tg != null)
                RangerServer.saveTagGroup(tg);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing group ID");
        } else if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing tag type");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(GROUPID, tokenlist.get(0));
        parms.put(TAGTYPE, tokenlist.get(1));
        /* Add others as listed attributes */
        for (int i = 2; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
