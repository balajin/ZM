package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.RangerServer.User;

/**
 * Handler for "usercreate" command
 *
 * @author Mike Primm
 */
public class UserCreateHandler implements CommandHandler {
    /* Command ID */
    public static final String USERCREATE_CMD = "usercreate";
    /* Parameter for user ID */
    public static final String USERID = "id";
    /* Parameter for password */
    public static final String PASSWORD = "password";
    /* Parameter ID for role */
    public static final String ROLE = "role";
    /* Command ID */
    public static final String[] CMD_IDS = {USERCREATE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Create new user account."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = { USERCREATE_CMD + " <user-id> <password> <role>"};
    /* Command syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        USERID+ "=<user-id>&" + PASSWORD + "=<password>&"+
        ROLE+"=<role>"};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.get(USERID);
        if (id == null || id.length() == 0)
            throw new CommandParameterException("Missing user ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String password = parms.get(PASSWORD);
        if (password == null)
            throw new CommandParameterException("Missing password");
        String role = parms.get(ROLE);
        if (role == null)
            throw new CommandParameterException("Missing role");
        if (!RangerServer.testIfValidID(role))
            throw new CommandParameterException("Role must be alphanumeric");
        /* If first account, must be admin */
        Map<String,User> users = RangerServer.getUsers();
        if ((users.size() == 0) && (!role.equals(User.ROLE_ADMIN)))
            throw new CommandException("Role for first account must be 'admin'");
        String errmsg = RangerServer.createUser(id, password, null, role);
        if (errmsg != null) {
            throw new CommandException(errmsg);
        } else { /* If successful, save it to file */
            User usr = users.get(id);
            if (usr != null)
                RangerServer.saveUser(usr);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing user ID");
        } else if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing password");
        } else if (tokenlist.size() < 3) {
            throw new CommandParameterException("Missing role");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first three */
        parms.put(USERID, tokenlist.get(0));
        parms.put(PASSWORD, tokenlist.get(1));
        parms.put(ROLE, tokenlist.get(2));
        /* Add others as listed attributes */
        for (int i = 3; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
