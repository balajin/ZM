package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "locset" command
 *
 * @author Mike Primm
 */
public class LocationSetHandler implements CommandHandler {
    /* Command ID */
    public static final String LOCSET_CMD = "locset";
    /* Parameter for location ID */
    public static final String LOCID = "id";
    /* Parameter for parent location ID */
    public static final String PARENTLOCID = "parent";
    /* Command ID */
    public static final String[] CMD_IDS = {LOCSET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Update one or more settings of an existing location instance."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        LOCSET_CMD + " <loc-id> <attrib-id>=<new-value> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        LOCID+"=<loc-id>&<attrib-id>=<new-value>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;

        String id = parms.remove(LOCID); /* Get it, and remove it */
        if (id == null)
            throw new CommandParameterException("Missing location ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        if (parms.size() == 0) /* No more parameters? */
            throw new CommandParameterException("Missing attribute id=value");
        /* See if ID exists */
        Location loc = ReaderEntityDirectory.findLocation(id);
        if (loc == null)
            throw new CommandParameterException("Invalid location ID");
        /* Get copy of attributes */
        Map<String, Object> attr = new HashMap<String,Object>(loc.getAttributes());
        /* Loop through the parameters */
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            String attrid = ent.getKey();
            String aval = ent.getValue();
            if (attrid.equals(PARENTLOCID)) {
                Location par = null;
                if ((aval.length() > 0)
                    && (!aval.equals(RangerServer.NULL_VAL))) {
                    par = ReaderEntityDirectory.findLocation(aval);
                    if (par == null)
                        throw new CommandParameterException("Parent does not exist");
                }
                try {
                    loc.setParent(par);
                } catch (BadParameterException bpx) {
                    throw new CommandParameterException(bpx.getMessage());
                }
            } else if (aval.equals(RangerServer.NULL_VAL)) {
                attr.remove(attrid);
            } else if (attrid.equals(Location.LOCATTRIB_DEFINESIRDOMAIN)) {
                attr.put(attrid, aval.equals("true"));
            } else {
                attr.put(attrid, aval);
            }
            update = true;
        }
        /* Set the reader's attributes */
        if (update) {
            loc.setAttibutes(attr);
            RangerServer.saveLocation(loc);
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing rule ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(LOCID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }

    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
