package com.rfcode.ranger.commands;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.drivers.readers.ListListDouble;

/**
 * Handler for "ruleset" command
 *
 * @author Mike Primm
 */
public class RuleSetHandler implements CommandHandler {
    /* Command ID */
    public static final String RULESET_CMD = "ruleset";
    /* Parameter for reader ID */
    public static final String RULEID = "id";
    /* Command ID */
    public static final String[] CMD_IDS = {RULESET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Update one or more settings of an existing location rule."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        RULESET_CMD + " <rule-id> <attrib-id>=<new-value> ...",
        RULESET_CMD + " <rule-id>,<rule-id>,... <attrib-id>=<new-value> ...",
        RULESET_CMD + " * <attrib-id>=<new-value> ..." };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        RULEID + "=<rule-id>&<attrib-id>=<new-value>&...",
        RULEID + "=<rule-id>,<rule-id>,...&<attrib-id>=<new-value>&...",
        RULEID + "=*&<attrib-id>=<new-value>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;
        boolean do_act = false;
        boolean actval = false;

        String id = parms.remove(RULEID); /* Get it, and remove it */
        if (id == null)
            throw new CommandParameterException("Missing rule ID");
        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = ReaderEntityDirectory.getLocationRuleIDs();
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String ruleid : ids) {
                try {
                    parms.put(RULEID, ruleid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        if (parms.size() == 0) /* No more parameters? */
            throw new CommandParameterException("Missing attribute id=value");

        /* See if ID exists */
        LocationRule rule = ReaderEntityDirectory.findLocationRule(id);
        if (rule == null)
            throw new CommandParameterException("Invalid rule ID");
        if (rule.getLocationRuleFactory() == null)
            throw new CommandParameterException("Internal rule ID");
        /* Get copy of attributes */
        Map<String, Object> attr = new HashMap<String, Object>(rule.getLocationRuleAttributes());
        /* Loop through the parameters */
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            String attrid = ent.getKey();
            String aval = ent.getValue();
            /* If "enabled", changing activations state - do last */
            if(attrid.equals("enabled")) {
                actval = aval.equalsIgnoreCase("true");
                do_act = true;
                continue;
            }
            /* Get current attribute value */
            Object v = attr.get(attrid);
            /* If not defined, invalid attrib */
            if (v == null) {
                throw new CommandParameterException("Bad attribute ID - "
                    + attrid);
            }
            /* Else, if its an integer */
            else if (v instanceof Integer) {
                try {
                    Integer ival = Integer.parseInt(aval);
                    /* Update value */
                    attr.put(attrid, ival);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad integer attribute value - " + aval);
                }
            } else if (v instanceof Double) {
                try {
                    Double dval = Double.parseDouble(aval);
                    /* Update value */
                    attr.put(attrid, dval);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad double attribute value - " + aval);
                }
            } else if (v instanceof Boolean) {
                try {
                    Boolean bval = Boolean.parseBoolean(aval); /* Parse value */
                    /* Update value */
                    attr.put(attrid, bval);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad attribute value - " + aval);
                }
            } else if (v instanceof Double) {
                try {
                    Double dval = Double.parseDouble(aval);
                    /* Update value */
                    attr.put(attrid, dval);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad double attribute value - " + aval);
                }
            } else if (v instanceof Set) {
                Set<ReaderEntity> nval = new HashSet<ReaderEntity>();
                String[] l = aval.split(",");
                for (String s : l) {
                    ReaderEntity re = ReaderEntityDirectory.findEntity(s);
                    if (re != null)
                        nval.add(re);
                }
                attr.put(attrid, nval);
                update = true;
			} else if (v instanceof ListListDouble) {	/* Special list for list of list of doubles */
				ListListDouble nval = new ListListDouble();
				List<Double> sval = null;
				String[] l = aval.split(",");	/* Split at commas */
				for(String s : l) {
					if(s.length() == 0)
						continue;
					int sindx = s.indexOf('[');	/* See if open? */
					if(sindx >= 0) {	/* Has open? */
						if(sval == null)
							sval = new ArrayList<Double>();
						else
                            throw new CommandParameterException("Bad attribute value - " + aval);
						s = s.substring(sindx+1);	/* Keep stuff after open */
					}
					sindx = s.indexOf(']');	/* See if close? */
					boolean endit = false;
					if(sindx >= 0) {	/* If so, close */
						s = s.substring(0, sindx);	/* Strip off after close */
						endit = true;
					}
                    if(sval == null)
                        throw new CommandParameterException("Bad attribute value - " + aval);
					/* Add to list */
					try {
						sval.add(Double.valueOf(s));	/* Parse it */
					} catch(NumberFormatException nfx) {
                        throw new CommandParameterException("Bad attribute value - " + aval);
					}
					if(endit) {	/* If close of sublist, end it */
						nval.add(sval);
						sval = null;
					}
				}
                attr.put(attrid, nval);
                update = true;
            } else if (v instanceof List) {
                List<String> nval = new ArrayList<String>();
                String[] l = aval.split(",");
                for (String s : l) {
                    nval.add(s);
                }
                attr.put(attrid, nval);
                update = true;
            } else { /* Else, put in a string */
                attr.put(attrid, aval);
                update = true;
            }
        }
        /* If activation set and disabling, do it first */
        if(do_act && (!actval)) {
            /* If not target state */
            if (rule.getRuleEnabled() == (!actval)) {
                rule.setRuleEnabled(actval);
                RangerServer.saveLocationRule(rule);
            }
        }
        /* Set the reader's attributes */
        if (update) {
            try {
                rule.setLocationRuleAttributes(attr);
                RangerServer.saveLocationRule(rule);
            } catch (BadParameterException bpx) {
                throw new CommandParameterException(bpx.getMessage());
            }
        }
        /* If activation set and enabling, do it last */
        if(do_act && actval) {
            /* If not target state */
            if (rule.getRuleEnabled() == (!actval)) {
                rule.setRuleEnabled(actval);
                RangerServer.saveLocationRule(rule);
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing rule ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(RULEID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
