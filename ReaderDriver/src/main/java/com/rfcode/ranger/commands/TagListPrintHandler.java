package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "taglist", "taglistbygroup", "tagprintbygroup" commands
 *
 * @author Mike Primm
 */
public class TagListPrintHandler implements CommandHandler {
    public static final String TAGLIST_CMD = "taglist";
    public static final String TAGLISTBYGROUP_CMD = "taglistbygroup";
    public static final String TAGPRINTBYGROUP_CMD = "tagprintbygroup";
    public static final String TAGCOUNTBYGROUP_CMD = "tagcountbygroup";
    /* Attribute ID for group IDs (prefix - followed by zero-based number) */
    public static final String GROUPID_PREFIX = "groupid";
    /* Attrib ID for start index and limit count (result windowing) */
    public static final String  FIRSTROW_ID = "_firstrow";
    public static final String  ROWCOUNT_ID = "_rowcnt";

    /* Attrib ID for value lookup */
    public static final String VALUE_ID = "_value";

    /* Command ID */
    public static final String[] CMD_IDS = {TAGLIST_CMD, TAGLISTBYGROUP_CMD,
        TAGPRINTBYGROUP_CMD,TAGCOUNTBYGROUP_CMD};
    /* Command help */
    private static final String[] TAGLIST_CMD_HELP = {"List all tags."};
    private static final String[] TAGLISTBYGROUP_CMD_HELP = {"List all tags in one or more specific tag groups."};
    private static final String[] TAGPRINTBYGROUP_CMD_HELP = {"List all tags in one or more specific tag groups, and all their status."};
    private static final String[] TAGCOUNTBYGROUP_CMD_HELP = {"Count tags in one or more specific tag groups."};
    /* Command syntax help */
    private static final String[] TAGLIST_CMD_SYNTAX_HELP = {TAGLIST_CMD};
    private static final String[] TAGLISTBYGROUP_CMD_SYNTAX_HELP = {TAGLISTBYGROUP_CMD
        + " <group-id> ..."};
    private static final String[] TAGPRINTBYGROUP_CMD_SYNTAX_HELP = {TAGPRINTBYGROUP_CMD
        + " <group-id> ..."};
    private static final String[] TAGCOUNTBYGROUP_CMD_SYNTAX_HELP = {TAGCOUNTBYGROUP_CMD
        + " <group-id> ..."};
    private static final String[] TAGLIST_CMD_WEB_SYNTAX_HELP = {""};
    private static final String[] TAGLISTBYGROUP_CMD_WEB_SYNTAX_HELP = {
        GROUPID_PREFIX + "0=<group-id>&"+
        GROUPID_PREFIX + "1=<group-id>&..."};
    private static final String[] TAGPRINTBYGROUP_CMD_WEB_SYNTAX_HELP = {
        GROUPID_PREFIX + "0=<group-id>&"+
        GROUPID_PREFIX + "1=<group-id>&..."};
    private static final String[] TAGCOUNTBYGROUP_CMD_WEB_SYNTAX_HELP = {
        GROUPID_PREFIX + "0=<group-id>&"+
        GROUPID_PREFIX + "1=<group-id>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Set<String> grps;
        String value;
        boolean do_print = false;
        int firstrow = 0;
        int rowcnt = Integer.MAX_VALUE;
        int rowidx = 0;
        boolean do_count = false;

        if(cmd.equals(TAGCOUNTBYGROUP_CMD))
            do_count = true;
        /* Get row limits, if any */
        String p = parms.get(FIRSTROW_ID);
        if(p != null) {
            try {
                firstrow = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row index parameter");
            }
        }
        p = parms.get(ROWCOUNT_ID);
        if(p != null) {
            try {
                rowcnt = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row count parameter");
            }
        }

        if (cmd.equals(TAGPRINTBYGROUP_CMD))
            do_print = true;
        /* Get the group parameters */
        grps = new TreeSet<String>();
        String id;
        for (int i = 0; (id = parms.get(GROUPID_PREFIX + i)) != null; i++) {
            grps.add(id);
        }
        /* If none we provided, do all */
        if(grps.size() == 0)
            grps = ReaderEntityDirectory.getTagGroupIDs();

        /* Get the operator - only support contains case insensitive for now */
        value = parms.get(VALUE_ID);
        if (value != null) {
            value = value.toLowerCase();
        }

        /* Start tag list */
        if(do_count)
            ctx.appendStartObject(sb, "tag-count", null, true);
        else
            ctx.appendStartObject(sb, "tag-list", null, true);
        /* Loop through the groups */
        boolean first = true;
        for (String key : grps) {
            TagGroup tg = ReaderEntityDirectory.findTagGroup(key);
            if (tg == null)
                throw new CommandParameterException("Invalid tag group ID");
            Map<String, Tag> tags = tg.getTags(); /* Get our tags */
            TreeSet<String> keys = new TreeSet<String>(tags.keySet());
            for (String tagid : keys) {
                if (value != null && !tagid.toLowerCase().contains(value)) {
                    continue;
                }
                Tag tag = tags.get(tagid);
                if((rowidx < firstrow) || ((rowidx-firstrow) >= rowcnt)) {

                }
                else if (do_count) {
                    /* Skip if counting */
                }
                else if (do_print) {
                    RangerServer.printTag(ctx, sb, tag, first);
                    first = false;
                } else {
                    ctx.appendStartObject(sb, "tag", tag.getTagGUID(), first);
                    ctx.appendAttrib(sb, "taggroupid", tag.getTagGroup(), true);
                    ctx.appendEndObject(sb, "tag", tag.getTagGUID());
                    first = false;
                }
                /* Output completed line */
                ctx.sendString(sb.toString());
                sb.setLength(0);
                rowidx++;
            }
        }
        if(do_count) {
            ctx.appendAttrib(sb, "count", rowidx, true);
            ctx.appendEndObject(sb, "tag-count", null);
        }
        else {
            ctx.appendEndObject(sb, "tag-list", null);
        }
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        /* No parameters for taglist */
        if (cmd.equals(TAGLIST_CMD))
            return vals;
        /* Others, encode the group IDs */
        for (int i = 0; i < tokenlist.size(); i++) {
            vals.put(GROUPID_PREFIX + i, tokenlist.get(i));
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if (cmd.equals(TAGLIST_CMD))
            return TAGLIST_CMD_HELP.clone();
        else if (cmd.equals(TAGLISTBYGROUP_CMD))
            return TAGLISTBYGROUP_CMD_HELP.clone();
        else if (cmd.equals(TAGCOUNTBYGROUP_CMD))
            return TAGCOUNTBYGROUP_CMD_HELP.clone();
        else
            return TAGPRINTBYGROUP_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLIST_CMD))
            return TAGLIST_CMD_SYNTAX_HELP.clone();
        else if (cmd.equals(TAGLISTBYGROUP_CMD))
            return TAGLISTBYGROUP_CMD_SYNTAX_HELP.clone();
        else if (cmd.equals(TAGCOUNTBYGROUP_CMD))
            return TAGCOUNTBYGROUP_CMD_SYNTAX_HELP.clone();
        else
            return TAGPRINTBYGROUP_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLIST_CMD))
            return TAGLIST_CMD_WEB_SYNTAX_HELP.clone();
        else if (cmd.equals(TAGLISTBYGROUP_CMD))
            return TAGLISTBYGROUP_CMD_WEB_SYNTAX_HELP.clone();
        else if (cmd.equals(TAGCOUNTBYGROUP_CMD))
            return TAGCOUNTBYGROUP_CMD_WEB_SYNTAX_HELP.clone();
        else
            return TAGPRINTBYGROUP_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
