package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "help" command
 * 
 * @author Mike Primm
 */
public class HelpHandler implements CommandHandler {
    public static final String HELP_CMD = "help";
    /* Parameter ID for command */
    public static final String COMMANDID = "command";
    /* Command ID */
    public static final String[] CMD_IDS = {HELP_CMD};
    /* Command help */
    private static final String[] HELP_CMD_HELP = {"List all available commands, or details on a specific command."};
    /* Command syntax help */
    private static final String[] HELP_CMD_SYNTAX_HELP = {
        HELP_CMD,
        HELP_CMD + " <command-id>"
        };
    private static final String[] HELP_CMD_WEB_SYNTAX_HELP = {
        "",
        COMMANDID + "=<command-id>"
        };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        /* Check for command ID */
        CommandHandler ch = null;
        String cid = parms.get(COMMANDID);
        if(cid != null) {   /* Find requested command */
            ch = RangerServer.getCommandHandlers().get(cid);
            if(ch == null)
                sb.append("Unknown command - " + cid + "\n\n");
        }
        /* If none, do list of all commands */
        if(ch == null) {
            sb.append("\nAvailable commands:\n");
            Set<String> cmds = new TreeSet<String>(RangerServer.getCommandHandlers().keySet());
            for(String c : cmds) {
                sb.append(' ');
                sb.append(c);
                sb.append('\n');
            }
            sb.append("\nFor help with a specific command, enter: help <command>\n");
        }
        else {  /* Else, help for specific command */
            sb.append("\n");
            /* Output syntax help */
            String[] hlp;
            if(ctx.useWebSyntax()) {
                hlp = ch.getCommandWebSyntaxHelp(cid);
                for(String s : hlp) {
                    sb.append("http://<baseurl>/").append(cid);
                    if(s.length()>0)
                        sb.append("?");
                    sb.append(s);
                    sb.append("\n");
                }
            }
            else {
                hlp = ch.getCommandSyntaxHelp(cid);
                for(String s : hlp) {
                    sb.append(s);
                    sb.append("\n");
                }
            }
            /* Output text help too */
            hlp = ch.getCommandHelp(cid);
            for(String s : hlp) {
                sb.append("  ");
                sb.append(s);
                sb.append("\n");
            }
        }
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        Map<String, String> prms = new HashMap<String, String>();
        if(tokenlist.size() > 0) {
            prms.put(COMMANDID, tokenlist.get(0));
        }
        return prms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return HELP_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return HELP_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return HELP_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
