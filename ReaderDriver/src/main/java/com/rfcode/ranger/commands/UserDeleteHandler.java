package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.RangerServer.User;

/**
 * Handler for "userdelete" command
 *
 * @author Mike Primm
 */
public class UserDeleteHandler implements CommandHandler {
    /* Command ID */
    public static final String USERDELETE_CMD = "userdelete";
    /* Parameter for user ID */
    public static final String USERID = "id";
    /* Command ID */
    public static final String[] CMD_IDS = {USERDELETE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Delete user account.  Last 'admin' role account cannot be deleted until all other accounts are deleted."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = { USERDELETE_CMD + " <user-id>"};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        USERID+"=<user-id>"};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.get(USERID);
        if (id == null)
            throw new CommandParameterException("Missing user ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Now find rule */
        Map<String,User> users = RangerServer.getUsers();
        User usr = users.get(id);
        if (usr != null) { /* If found, delete it */
            if (usr.getRole().equals(User.ROLE_ADMIN)) {
                int nonadmin = 0;
                int admin = 0;
                for (User u : users.values()) {
                    if (u.getRole().equals(User.ROLE_ADMIN))
                        admin++;
                    else
                        nonadmin++;
                }
                /* If only one admin, and any non-admin, error */
                if ((admin == 1) && (nonadmin > 0))
                    throw new CommandException("Cannot delete last admin while non-admin accounts exist.");
            }
            RangerServer.deleteUser(usr); /* And delete file */
        } else {
            throw new CommandParameterException("User does not exist");
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing user ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first three */
        parms.put(USERID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
