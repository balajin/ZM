package com.rfcode.ranger.commands;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "locdelete" command
 *
 * @author Mike Primm
 */
public class LocationDeleteHandler implements CommandHandler {
    /* Command ID */
    public static final String LOCDELETE_CMD = "locdelete";
    public static final String LOCTREEDELETE_CMD = "loctreedelete";
    /* Parameter for location ID */
    public static final String LOCID = "id";
    /* Command ID */
    public static final String[] CMD_IDS = {LOCDELETE_CMD, LOCTREEDELETE_CMD};
    /* Command help */
    private static final String[] LOCDELETE_CMD_HELP = {"Delete existing location instance."};
    private static final String[] LOCTREEDELETE_CMD_HELP = {"Delete existing location instance, and all its children."};
    /* Command syntax help */
    private static final String[] LOCDELETE_CMD_SYNTAX_HELP = {
        LOCDELETE_CMD + " <loc-id>",
        LOCDELETE_CMD + " *"};
    private static final String[] LOCTREEDELETE_CMD_SYNTAX_HELP = {
        LOCTREEDELETE_CMD + " <loc-id>"};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        LOCID + "=<loc-id>",
        LOCID + "=*"};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Location loc;
        boolean recurse = false;
        String id = parms.remove(LOCID);
        if (id == null)
            throw new CommandParameterException("Missing location ID");

        if(cmd.equals(LOCTREEDELETE_CMD)) {
            recurse = true;
        }
        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = new HashSet<String>(ReaderEntityDirectory.getLocationIDs());
                cmd = LOCDELETE_CMD;    /* If doing all, no need to recurse */
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String locid : ids) {
                try {
                    parms.put(LOCID, locid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        /* Now find rule */
        loc = ReaderEntityDirectory.findLocation(id);
        if (loc != null) { /* If found, delete it */
            List<Location> clist;
            if(!recurse) {
                clist = loc.getChildren();   /* Save list of children */
            }
            else {
                clist = loc.getChildrenRecursive(); /* Get child tree */
                recurse = true;
            }
            RangerServer.deleteLocation(loc); /* And delete file */
            loc.cleanup(); /* And cleanup object */
            /* Repeat across children */
            for(Location child : clist) {
                /* If deleting just location, save updated children */
                if(!recurse) {
                    /* And save updated children (new parent) */
                    RangerServer.saveLocation(child);
                }
                else {  /* Else, delete them too                    */
                    RangerServer.deleteLocation(child);
                    child.cleanup();
                }
            }
        } else {
            throw new CommandParameterException("Invalid location ID");
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing location ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(LOCID, tokenlist.get(0));
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(LOCDELETE_CMD))
            return LOCDELETE_CMD_HELP.clone();
        else
            return LOCTREEDELETE_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(LOCDELETE_CMD))
            return LOCDELETE_CMD_SYNTAX_HELP.clone();
        else
            return LOCTREEDELETE_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
