package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.ListDouble;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "readerset" command
 *
 * @author Mike Primm
 */
public class ReaderSetHandler implements CommandHandler {
    /* Parameter for reader ID */
    public static final String READERID = "id";
    /* Command ID */
    public static final String READERSET_CMD = "readerset";
    public static final String[] CMD_IDS = {READERSET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Update one or more settings of an existing reader."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        READERSET_CMD+" <reader-id> <attrib-id>=<new-value> ...",
        READERSET_CMD+" <reader-id>,<reader-id>,... <attrib-id>=<new-value> ...",
        READERSET_CMD+" * <attrib-id>=<new-value> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        READERID+"=<reader-id>&<attrib-id>=<new-value>&...",
        READERID+"=<reader-id>,<reader-id>,...&<attrib-id>=<new-value>&...",
        READERID+"=*&<attrib-id>=<new-value>&...",
        };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;
        boolean do_act = false;
        boolean actval = false;
        boolean do_grps = false;
        boolean do_lbl = false;
        String lbl = null;
        TagGroup[] grpval = null;

        String id = parms.remove(READERID); /* Get it, and remove it */
        if (id == null)
            throw new CommandParameterException("Missing reader ID");
        /* If wildcard or list, we're going to process it recursively */
        if(id.equals("*") || (id.indexOf(',') >= 0)) {
            Set<String> ids = null;

            if(id.equals("*")) {    /* If *, get all IDs */
                ids = ReaderEntityDirectory.getReaderIDs();
            }
            else {
                ids = new HashSet<String>();
                String[] idlist = id.split(",");
                for(String anid : idlist) {
                    ids.add(anid);
                }
            }
            /* Recurse over list */
            boolean is_good = false;
            CommandException last_cx = null;
            for(String rdrid : ids) {
                try {
                    parms.put(READERID, rdrid);
                    invokeCommand(cmd, ctx, parms);
                    is_good = true;
                } catch (CommandException cx) {
                    /* We swallow these - we're good if one succeeds */
                    last_cx = cx;
                }
            }
            /* If none worked, throw last reported exception */
            if(!is_good && ids.size() > 0)
                throw last_cx;
            return;
        }
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        if (parms.size() == 0) /* No more parameters? */
            throw new CommandParameterException("Missing attribute id=value");
        /* See if ID exists */
        Reader rdr = ReaderEntityDirectory.findReader(id);
        if (rdr == null)
            throw new CommandParameterException("Invalid reader ID");
        /* Get copy of attributes */
        Map<String, Object> attr = new HashMap<String,Object>(rdr.getReaderAttributes());
        /* Loop through the parameters */
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            String attrid = ent.getKey();
            String aval = ent.getValue();
            /* If "enable", activate/deactivate reader */
            if(attrid.equals("enabled")) {
                do_act = true;
                actval = aval.equalsIgnoreCase("true");
                continue;
            }
            /* If "groups", get group list */
            if(attrid.equals("groups")) {
                do_grps = true;
                if(aval.length() > 0) {
                    String[] vset = aval.split(",");
                    grpval = new TagGroup[vset.length];
                    int i = 0;
                    for(String v : vset) {
                        grpval[i] = ReaderEntityDirectory.findTagGroup(v);
                        if(grpval[i] != null)
                            i++;
                        else
                            throw new CommandParameterException("Invalid group ID - " + v);
                    }
                }
                else {
                    grpval = new TagGroup[0];
                }
                continue;
            }
            /* If "label", set label */
            if(attrid.equals("label")) {
                lbl = aval;
                do_lbl = true;
                continue;
            }                                
            /* Get current attribute value */
            Object v = attr.get(attrid);
            /* If not defined, invalid attrib */
            if (v == null)
                throw new CommandParameterException("Bad attribute ID - "
                    + attrid);
            /* Else, if its an integer */
            else if (v instanceof Integer) {
                try {
                    Integer ival = Integer.parseInt(aval);
                    /* Update value */
                    attr.put(attrid, ival);
                    update = true;
                } catch (NumberFormatException nfx) {
                    throw new CommandParameterException(
                        "Bad integer attribute value - " + aval);
                }
            }
            /* Else, if its a boolean */
            else if (v instanceof Boolean) {
                Boolean bval = Boolean.valueOf(aval);
                /* Update value */
                attr.put(attrid, bval);
                update = true;
            }
            /* Else, if its a double list */
            else if (v instanceof ListDouble) {
                int idx = aval.indexOf('[');
                if(idx >= 0) aval = aval.substring(idx+1).trim();
                idx = aval.lastIndexOf(']');
                if(idx >= 0) aval = aval.substring(0, idx).trim();
                ListDouble nval = new ListDouble();
                String[] l = aval.split(",");
                for(String s : l) {
                    try {
                        nval.add(Double.parseDouble(s));
                    } catch (NumberFormatException nfx) {
                        throw new CommandParameterException(
                            "Bad format for double list");
                    }
                }
                attr.put(attrid, nval);
                update = true;
            } else { /* Else, put in a string */
                attr.put(attrid, aval);
                update = true;
            }
        }
        /* Set the reader's attributes */
        if (update) {
            try {
                rdr.setReaderAttributes(attr);
                RangerServer.saveReader(rdr);
            } catch (BadParameterException bpx) {
                throw new CommandParameterException(bpx.getMessage());
            }
        }
        /* If update label */
        if(do_lbl) {
            rdr.setLabel(lbl);
            RangerServer.saveReader(rdr);
        }
        /* Change groups, do next */
        if(do_grps) {
            try {
                rdr.setReaderTagGroups(grpval);
                RangerServer.saveReader(rdr);
            } catch (BadParameterException bpx) {
                throw new CommandParameterException(bpx.getMessage());
            }
        }
        /* Change activation state last, if needed */
        if(do_act) {
            /* If not already in state */
            if (rdr.getReaderEnabled() != actval) {
                if(rdr.setReaderLicense(actval)) {
                    rdr.setReaderEnabled(actval);
                    RangerServer.saveReader(rdr);
                }
                else {
                    throw new CommandParameterException("Reader cannot activate - license count exceeded");
                }
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing reader ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(READERID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
