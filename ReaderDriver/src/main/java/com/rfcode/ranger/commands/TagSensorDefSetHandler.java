package com.rfcode.ranger.commands;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "tagsensordefset", "tagsensordeflist" command
 *
 * @author Mike Primm
 */
public class TagSensorDefSetHandler implements CommandHandler {
    /* Command ID */
    public static final String TAGSENSORDEFSET_CMD = "tagsensordefset";
    public static final String TAGSENSORDEFLIST_CMD = "tagsensordeflist";
    
    public static final String DEF_ID_PARAM = "id";
    public static final String TAG_ID_PARAM = "tag";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGSENSORDEFSET_CMD,
        TAGSENSORDEFLIST_CMD};
    /* Command help */
    private static final String[] CMD_HELP_SET = {
        "Set the sensor definition for one or more tags." };
    private static final String[] CMD_HELP_LIST = {
        "List of tags using a given sensor definition." };
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP_SET = {
        TAGSENSORDEFSET_CMD + " <def-id> <tag-id> <tag-id> ..." };
    private static final String[] CMD_SYNTAX_HELP_LIST = {
        TAGSENSORDEFLIST_CMD,
        TAGSENSORDEFLIST_CMD + " <def-id> <def-id> ..."
    };
    private static final String[] CMD_WEB_SYNTAX_HELP_SET = {
        DEF_ID_PARAM + "=<def-id>&" + TAG_ID_PARAM + "=<tag-id>,<tag-id>,..." };
    private static final String[] CMD_WEB_SYNTAX_HELP_LIST = {
        "",
        DEF_ID_PARAM + "=<def-id>,<def-id>,..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {

        if(cmd.equals(TAGSENSORDEFLIST_CMD)) {    /* List command? */
        	Collection<SensorDefinition> defs;
        	
            if(parms.size() == 0) { /* If no parameters, do all */
            	defs = ReaderEntityDirectory.getSensorDefinitions();
            }
            else {
            	String ids = parms.get(DEF_ID_PARAM);
            	String idlist[] = ids.split(",");
            	defs = new ArrayList<SensorDefinition>();
            	for (String p : idlist) {
            		SensorDefinition sd = ReaderEntityDirectory.findSensorDefinition(p);
            		if (sd == null) {
            			throw new CommandParameterException("Invalid sensor definition ID: " + p);
            		}
            		defs.add(sd);
            	}
            }
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            /* Start object list*/
            ctx.appendStartObject(sb, "tagsensordef-list", null, true);
            for(SensorDefinition sd : defs) {
                ctx.appendStartObject(sb, "tagsensordef", sd.getID(), first);
                ctx.appendAttrib(sb, "tags", sd.getTags(), true);
                ctx.appendEndObject(sb, "tagsensordef", sd.getID());
                first = false;
            }
            ctx.appendEndObject(sb, "tagsensordef-list", null);
            sb.append("\n");
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        else {
        	String id = parms.remove(DEF_ID_PARAM);
        	if (id == null) {
        		throw new CommandParameterException("Missing sensor definition ID");
        	}
        	SensorDefinition def = null;
        	if (id.equals("null") == false) {
    			def = ReaderEntityDirectory.findSensorDefinition(id);
        		if (def == null) {
        			throw new CommandParameterException("Invalid sensor definition ID: " + id);
        		}
        	}
        	String taglist = parms.remove(TAG_ID_PARAM);
        	if (taglist == null) {
                throw new CommandParameterException("Missing tag IDs");
        	}
            String[] tags = taglist.split(",");   /* Split into list */
            HashSet<String> tagset = new HashSet<String>();
            for (String t : tags) {
            	tagset.add(t);
            }
            /* Do remove first */
            for (SensorDefinition sd : ReaderEntityDirectory.getSensorDefinitions()) {
            	if (sd != def) {	// Target?
            		if (sd.removeAllTags(tagset)) {
            			RangerServer.saveSensorDefinition(sd);
            		}
            	}
            }
            if (def != null) {
        		if (def.addAllTags(tagset)) {
        			RangerServer.saveSensorDefinition(def);
        		}
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> parms = new HashMap<String, String>();
        int start = 0;
        String listparm = DEF_ID_PARAM;
        
        if (cmd.equals(TAGSENSORDEFSET_CMD)) {
        	if (tokenlist.size() < 1) {
        		throw new CommandParameterException("Missing sensor definition ID");
        	}
        	parms.put(DEF_ID_PARAM, tokenlist.get(0));
        	start = 1;
        	listparm = TAG_ID_PARAM;
        }
        /* Add others as listed attributes */
        String tags = "";
        if (tokenlist.size() > start) {
        	for (int i = start; i < tokenlist.size(); i++) {
        		String s = tokenlist.get(i);
            	if (i != start)
            		tags += ",";
            	tags += s;
        	}
        	parms.put(listparm, tags);
        }
        
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(TAGSENSORDEFLIST_CMD)) {    /* List command? */
            return CMD_HELP_LIST.clone();
        }
        else {
            return CMD_HELP_SET.clone();
        }
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(TAGSENSORDEFLIST_CMD)) {    /* List command? */
            return CMD_SYNTAX_HELP_LIST.clone();
        }
        else {
            return CMD_SYNTAX_HELP_SET.clone();
        }
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if(cmd.equals(TAGSENSORDEFLIST_CMD)) {    /* List command? */
            return CMD_WEB_SYNTAX_HELP_LIST.clone();
        }
        else {
            return CMD_WEB_SYNTAX_HELP_SET.clone();
        }
    }

    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
