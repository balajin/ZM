package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.GPSDefaults;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "gpsdefset" command
 *
 * @author Mike Primm
 */
public class GPSDefSetHandler implements CommandHandler {
    /* Command ID */
    public static final String GPSDEFSET_CMD = "gpsdefset";
    /* Command ID */
    public static final String[] CMD_IDS = {GPSDEFSET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Update one or more global GPS default attributes."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        GPSDEFSET_CMD + " <attrib-id>=<new-value> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        "<attrib-id>=<new-value>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;
		GPSDefaults orig = GPSDefaults.getDefault();	/* Get current values */
		double min_horiz = orig.getGPSMinHoriz();
		double min_vert = orig.getGPSMinVert();
		String data_set = orig.getGPSDataSet();
		int	min_period = orig.getGPSMinPeriod();

        if (parms.size() == 0) /* No more parameters? */
            throw new CommandParameterException("Missing attribute id=value");
        /* Loop through the parameters */
        for (Map.Entry<String, String> ent : parms.entrySet()) {
            String attrid = ent.getKey();
            String aval = ent.getValue();
			/* Handle attributes */
			if(attrid.equals(GPSDefaults.GPS_DATASET)) {
				data_set = aval;
			}
			else if(attrid.equals(GPSDefaults.GPS_MIN_PERIOD)) {
				try {
					min_period = Integer.parseInt(aval);
				} catch (NumberFormatException nfx) {
		            throw new CommandParameterException("Invalid attribute value for " + attrid);					
				}
			}
			else if(attrid.equals(GPSDefaults.GPS_MIN_HORIZ)) {
				try {
					min_horiz = Double.parseDouble(aval);
				} catch (NumberFormatException nfx) {
		            throw new CommandParameterException("Invalid attribute value for " + attrid);					
				}
			}
			else if(attrid.equals(GPSDefaults.GPS_MIN_VERT)) {
				try {
					min_vert = Double.parseDouble(aval);
				} catch (NumberFormatException nfx) {
		            throw new CommandParameterException("Invalid attribute value for " + attrid);					
				}
			}
			else {
	            throw new CommandParameterException("Invalid attribute ID - " + attrid);					
			}
            update = true;
        }
        /* Set the reader's attributes */
        if (update) {
			if(GPSDefaults.replaceDefault(data_set, min_period, min_horiz, min_vert)) {
	            RangerServer.saveGPSDefaults();
			}
			else {
	            throw new CommandParameterException("Invalid parameters");					
			}
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add others as listed attributes */
        for (int i = 0; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }

    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
