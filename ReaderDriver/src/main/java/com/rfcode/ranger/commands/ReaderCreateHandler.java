package com.rfcode.ranger.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "readercreate" command
 *
 * @author Mike Primm
 */
public class ReaderCreateHandler implements CommandHandler {
    /* Parameter for reader ID */
    public static final String READERID = "id";
    /* Parameter ID for reader type */
    public static final String READERTYPE = "type";
    /* Parameter ID for groups */
    public static final String READERGROUPS = "groups";
    /* Parameter ID for label */
    public static final String LABEL = "label";
    /* Command ID */
    public static final String READERCREATE_CMD = "readercreate";
    public static final String[] CMD_IDS = {READERCREATE_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Create new reader instance of given type.  Initially disabled"};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {
        READERCREATE_CMD+" <reader-id> <reader-type>"
        };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        READERID+"=<reader-id>&"+READERTYPE+"=<reader-type>"};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean is_xml = false;
        boolean is_json = false;
        StringBuilder sb = new StringBuilder();
        if(ctx.getFormatExt() == CommandContext.OutputSyntax.XML) {
            is_xml = true;
            ctx.setContentType("text/xml");
            sb.append("<done>");
        }
        else if(ctx.getFormatExt() == CommandContext.OutputSyntax.JSON) {
            is_json = true;
            ctx.setContentType("application/json");
            sb.append("{ \"__result\" : \"done\", \"__msg\" : ");
        }
        String id = parms.remove(READERID);
        if (id == null || id.length() == 0)
            throw new CommandParameterException("Missing reader ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        String type = parms.remove(READERTYPE);
        if (type == null)
            throw new CommandParameterException("Missing reader type");
        String label = parms.remove(LABEL);
        /* Check if groups list provided */
        ArrayList<String> grplist = new ArrayList<String>();
        String grps = parms.remove(READERGROUPS);
        if(grps != null) {
            String[] l = grps.split(",");
            for (String s : l) {
                grplist.add(s);
            }
        }
        /* Make parm list */
        ArrayList<String> pms = new ArrayList<String>();
        for(Map.Entry<String,String> ent : parms.entrySet()) {
            pms.add(ent.getKey() + "=" + ent.getValue());
        }
        /* Create the reader */
        String err = RangerServer.createReader(id, type,
            pms, grplist, label);
        if (err == null) {
            if(is_xml)
                sb.append("Reader created");
            else if(is_json)
                sb.append(RangerServer.toJSONString("Reader created"));
            else
                sb.append(id + ": <created>\n");
            Reader rdr = ReaderEntityDirectory.findReader(id);
            if (rdr != null)
                RangerServer.saveReader(rdr);
        } else {
            throw new CommandException(err);
        }
        if(is_xml)
            sb.append("</done>\n");
        else if(is_json)
            sb.append(" }\n");
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing reader ID");
        } else if (tokenlist.size() < 2) {
            throw new CommandParameterException("Missing reader type");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first two */
        parms.put(READERID, tokenlist.get(0));
        parms.put(READERTYPE, tokenlist.get(1));
        /* Add others as listed attributes */
        for (int i = 2; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
