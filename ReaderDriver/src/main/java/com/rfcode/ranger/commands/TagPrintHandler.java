package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "tagprint" command
 * 
 * @author Mike Primm
 */
public class TagPrintHandler implements CommandHandler {
    public static final String TAGPRINT_CMD = "tagprint";
    /* Attribute ID for tag IDs (prefix - followed by zero-based number) */
    public static final String TAGID_PREFIX = "tagid";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGPRINT_CMD};
    /* Command help */
    private static final String[] TAGPRINT_CMD_HELP = {"Print details of one or more specific tags."};
    /* Command syntax help */
    private static final String[] TAGPRINT_CMD_SYNTAX_HELP = {
        TAGPRINT_CMD + " <tag-id> ..."};
    /* Command syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        TAGID_PREFIX + "0=<tag-id>&"+
        TAGID_PREFIX + "1=<tag-id>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        if(parms.get(TAGID_PREFIX + 0) == null)
            throw new CommandParameterException("No Tag GUIDs");
        String id;
        /* Start tag list */
        ctx.appendStartObject(sb, "tag-list", null, true);
        boolean first = true;
        for (int i = 0; (id = parms.get(TAGID_PREFIX + i)) != null; i++) {
            Tag t = TagGroup.findTagInAnyGroup(id);
            if (t == null)
                throw new CommandParameterException("Invalid tag GUID - " + id);
            RangerServer.printTag(ctx, sb, t, first);
            first = false;
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        ctx.appendEndObject(sb, "tag-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        for (int i = 0; i < tokenlist.size(); i++) {
            vals.put(TAGID_PREFIX + i, tokenlist.get(i));
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return TAGPRINT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return TAGPRINT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
