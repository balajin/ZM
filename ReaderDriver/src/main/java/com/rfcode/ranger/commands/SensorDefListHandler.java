package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
/**
 * Handler for "sensordeflist" command
 * 
 * @author Mike Primm
 */
public class SensorDefListHandler implements CommandHandler {
    public static final String SENSORDEFLIST_CMD = "sensordeflist";
    /* Command ID */
    public static final String[] CMD_IDS = {SENSORDEFLIST_CMD};
    /* Command help */
    private static final String[] SENSORDEFLIST_CMD_HELP = {"List all sensor definitions and their settings."};
    /* Command syntax help */
    private static final String[] SENSORDEFLIST_CMD_SYNTAX_HELP = {SENSORDEFLIST_CMD};
    /* Command web syntax help */
    private static final String[] SENSORDEFLIST_CMD_WEB_SYNTAX_HELP = { "" };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        SensorDefinition sd;
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getSensorDefinitionIDs());
        StringBuilder sb = new StringBuilder();
        /* Start sensordef list */
        ctx.appendStartObject(sb, "sensor-def-list", null, true);
        boolean first = true;
        for (String key : keys) {
            sd = ReaderEntityDirectory.findSensorDefinition(key);
            Map<String, Object> v = sd.getAttributes();
            Object val;
            ctx.appendStartObject(sb, "sensor-def", key, first);
            ctx.appendAttrib(sb, "type", sd.getSensorDefinitionType(), true);
            ctx.appendAttrib(sb, "label", sd.getLabel(), false);
            ctx.appendAttrib(sb, "dest", sd.getDestinationAttrib(), false);
            for (String k : v.keySet()) {
            	if (k.equals(SensorDefinition.TAGS)) {
            		continue;
            	}
                val = v.get(k); /* Get attribute value */
                ctx.appendAttrib(sb, k, val, false);
            }
            ctx.appendEndObject(sb, "sensor-def", key);
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            first = false;
        }
        ctx.appendEndObject(sb, "sensor-def-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return SENSORDEFLIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return SENSORDEFLIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return SENSORDEFLIST_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
