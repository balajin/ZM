package com.rfcode.ranger.commands;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "tagexplocset", "tagexploclist" and "tagexplocreset" command
 *
 * @author Mike Primm
 */
public class TagExpectedLocationSetHandler implements CommandHandler {
    /* Command ID */
    public static final String TAGEXPLOCSET_CMD = "tagexplocset";
    public static final String TAGEXPLOCRESET_CMD = "tagexplocreset";
    public static final String TAGEXPLOCLIST_CMD = "tagexploclist";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGEXPLOCSET_CMD,
        TAGEXPLOCRESET_CMD, TAGEXPLOCLIST_CMD};
    /* Command help */
    private static final String[] CMD_HELP_SET = {
        "Set list of expected locations for one or more tags." };
    private static final String[] CMD_HELP_RESET = {
        "Clear list of expected locations for all tags." };
    private static final String[] CMD_HELP_LIST = {
        "List of expected locations for specified tags." };
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP_SET = {
        TAGEXPLOCSET_CMD + " <tag-id>=<exp-loc-1>,<exp-loc-2>,..." };
    private static final String[] CMD_SYNTAX_HELP_RESET = {
        TAGEXPLOCRESET_CMD };
    private static final String[] CMD_SYNTAX_HELP_LIST = {
        TAGEXPLOCLIST_CMD,
        TAGEXPLOCLIST_CMD + " <tag-id> ..."
    };
    private static final String[] CMD_WEB_SYNTAX_HELP_SET = {
        "<tag-id>=<exp-loc-1>,<exp-loc-2>&..." };
    private static final String[] CMD_WEB_SYNTAX_HELP_RESET = {
        "" };
    private static final String[] CMD_WEB_SYNTAX_HELP_LIST = {
        "",
        "<tag-id>&..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean update = false;
        LocationRuleEngine lre = LocationRuleEngine.getLocationRuleEngine();

        if(cmd.equals(TAGEXPLOCRESET_CMD)) {    /* Reset command? */
            lre.resetTagExpectedLocations();    /* Do reset */
            RangerServer.saveTagExpectedLocations();    /* And save it */
        }
        else if(cmd.equals(TAGEXPLOCLIST_CMD)) {    /* List command? */
            Set<String> tagids;
            if(parms.size() == 0) { /* If no parameters, do all */
                tagids = lre.getTagsWithExpectedLocations();
            }
            else {
                tagids = parms.keySet();
            }
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            /* Start config object list*/
            ctx.appendStartObject(sb, "tagexploc-list", null, true);
            for(String tid : tagids) {
                ctx.appendStartObject(sb, "tagexploc", tid, first);
                ctx.appendAttrib(sb, "value", lre.getTagExpectedLocations(tid), true);
                ctx.appendEndObject(sb, "tagexploc", tid);
                first = false;
            }
            ctx.appendEndObject(sb, "tagexploc-list", null);
            sb.append("\n");
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        else {
            if (parms.size() == 0) /* No more parameters? */
                throw new CommandParameterException("Missing tag id=exp-loc-list");
            /* Loop through the parameters */
            for (Map.Entry<String, String> ent : parms.entrySet()) {
                String tid = ent.getKey();
                String aval = ent.getValue();
                String[] locs = aval.split(",");   /* Split into list */
                ArrayList<String> loclist = new ArrayList<String>();
                for(String s : locs) {
                    s = s.trim();
                    if(s.length() > 0)
                        loclist.add(s);
                }
                if(lre.setTagExpectedLocations(tid, loclist))
                    update = true;
            }
            /* If changed, save it */
            if (update) {
                RangerServer.saveTagExpectedLocations();
            }
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add others as listed attributes */
        for (int i = 0; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            int idx = s.indexOf("=");
            if (idx < 0) {
                parms.put(s, ""); /* Add ID, no value */
            } else {
                parms.put(s.substring(0, idx), s.substring(idx + 1));
            }
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(TAGEXPLOCRESET_CMD)) {    /* Reset command? */
            return CMD_HELP_RESET.clone();
        }
        else if(cmd.equals(TAGEXPLOCLIST_CMD)) {    /* List command? */
            return CMD_HELP_LIST.clone();
        }
        else {
            return CMD_HELP_SET.clone();
        }
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(TAGEXPLOCRESET_CMD)) {    /* Reset command? */
            return CMD_SYNTAX_HELP_RESET.clone();
        }
        else if(cmd.equals(TAGEXPLOCLIST_CMD)) {    /* List command? */
            return CMD_SYNTAX_HELP_LIST.clone();
        }
        else {
            return CMD_SYNTAX_HELP_SET.clone();
        }
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if(cmd.equals(TAGEXPLOCRESET_CMD)) {    /* Reset command? */
            return CMD_WEB_SYNTAX_HELP_RESET.clone();
        }
        else if(cmd.equals(TAGEXPLOCLIST_CMD)) {    /* List command? */
            return CMD_WEB_SYNTAX_HELP_LIST.clone();
        }
        else {
            return CMD_WEB_SYNTAX_HELP_SET.clone();
        }
    }

    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
