package com.rfcode.ranger.commands;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "loclist" commands
 * 
 * @author Mike Primm
 */
public class LocationListHandler implements CommandHandler {
    public static final String LOCLIST_CMD = "loclist";
    public static final String LOCEXPORT_CMD = "locexport";
    /* Command ID */
    public static final String[] CMD_IDS = {LOCLIST_CMD, LOCEXPORT_CMD};
    /* Command help */
    private static final String[] LOCLIST_CMD_HELP = {"List all locations and their settings."};
    private static final String[] LOCEXPORT_CMD_HELP = {"Export all locations, in importable CVS format."};
    /* Command syntax help */
    private static final String[] LOCLIST_CMD_SYNTAX_HELP = {LOCLIST_CMD};
    private static final String[] LOCEXPORT_CMD_SYNTAX_HELP = {LOCEXPORT_CMD};
    private static final String[] LOCLIST_CMD_WEB_SYNTAX_HELP = {""};
    private static final String[] LOCEXPORT_CMD_WEB_SYNTAX_HELP = {""};
    
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Location lr;
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getLocationIDs());
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        boolean do_exp = false;
        Set<String> colids = null;
        
        if(cmd.equals(LOCEXPORT_CMD)) {
            do_exp = true;
            //ctx.setContentType("text/x-excel-csv");
            /* Build column set of distinct attribtues */
            colids = new LinkedHashSet<String>();
            for(String key : keys) {
                lr = ReaderEntityDirectory.findLocation(key);
                colids.addAll(lr.getAttributes().keySet());
            }
            /* Output first line for CVS */
            sb.append("location.1.id,location.2.id");
            for(String col : colids) {
                sb.append(",location.1.");
                sb.append(col);
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        /* Make ordering of keys show parents before their children */
        TreeMap<String,Location> keymap = new TreeMap<String,Location>();
        for (String key : keys) {
            lr = ReaderEntityDirectory.findLocation(key);
            keymap.put(lr.getPath(), lr);
        }
        /* Start location list */
        if(!do_exp)
            ctx.appendStartObject(sb, "location-list", null, true);
        
        for (Map.Entry<String, Location> le : keymap.entrySet()) {
            lr = le.getValue();
            Map<String, Object> v = lr.getAttributes();
            if(!do_exp) {
                ctx.appendStartObject(sb, "location", lr.getID(), first);
                ctx.appendAttrib(sb, "parent", lr.getParent(), true);
                for (Map.Entry<String, Object> ent : v.entrySet()) {
                    ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), false);
                }
                ctx.appendAttrib(sb, RangerServer.LOCATIONPATH_ID,
                    lr.getPath(), false);
                ctx.appendEndObject(sb, "location", lr.getID());
            }
            else {
                sb.append(RangerServer.encodeForCSV(lr.getID())).append(",");
                if(lr.getParent() != null) {
                    sb.append(RangerServer.encodeForCSV(lr.getParent().getID()));
                }
                for(String col : colids) {
                    Object val = v.get(col);    /* Find attrib, if any */
                    sb.append(",");
                    if(val != null) {
                        sb.append(RangerServer.encodeForCSV(val));
                    }
                }
                sb.append("\n");
            }
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            
            first = false;
        }
        if(!do_exp) {
            ctx.appendEndObject(sb, "location-list", null);
            ctx.sendString(sb.toString());
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(LOCLIST_CMD))
            return LOCLIST_CMD_HELP.clone();
        else
            return LOCEXPORT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(LOCLIST_CMD))
            return LOCLIST_CMD_SYNTAX_HELP.clone();
        else
            return LOCEXPORT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if(cmd.equals(LOCLIST_CMD))
            return LOCLIST_CMD_WEB_SYNTAX_HELP.clone();
        else
            return LOCEXPORT_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
