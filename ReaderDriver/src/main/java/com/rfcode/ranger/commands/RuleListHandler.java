package com.rfcode.ranger.commands;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "rulelist" commands
 * 
 * @author Mike Primm
 */
public class RuleListHandler implements CommandHandler {
    public static final String RULELIST_CMD = "rulelist";
    public static final String RULEEXPORT_CMD = "ruleexport";
    /* Command ID */
    public static final String[] CMD_IDS = {RULELIST_CMD, RULEEXPORT_CMD};
    /* Command help */
    private static final String[] RULELIST_CMD_HELP = {"List all location rules and their settings."};
    private static final String[] RULEEXPORT_CMD_HELP = {"Export all location rules, in importable CSV format."};
    /* Command syntax help */
    private static final String[] RULELIST_CMD_SYNTAX_HELP = {RULELIST_CMD};
    private static final String[] RULEEXPORT_CMD_SYNTAX_HELP = {RULEEXPORT_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        LocationRule lr;
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getLocationRuleIDs());
        StringBuilder sb = new StringBuilder();
        boolean do_exp = false;
        Set<String> colids = null;
        
        if(cmd.equals(RULEEXPORT_CMD)) {
            do_exp = true;
            //ctx.setContentType("text/x-excel-csv");
            /* Build column set of distinct attribtues */
            colids = new LinkedHashSet<String>();
            for(String key : keys) {
                lr = ReaderEntityDirectory.findLocationRule(key);
                colids.addAll(lr.getLocationRuleAttributes().keySet());
            }
            /* Output first line for CVS */
            sb.append("rule.id,rule.type,rule.enabled,location.1.id");
            for(String col : colids) {
                sb.append(",rule.");
                sb.append(col);
            }
            sb.append("\n");
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }        
        /* Start rule-list */
        if(!do_exp)
            ctx.appendStartObject(sb, "rule-list", null, true);
        boolean first = true;
        for (String key : keys) {
            lr = ReaderEntityDirectory.findLocationRule(key);
			/* If no factory, its internal, so don't report it */
			if(lr.getLocationRuleFactory() == null)
				continue;
            Map<String, Object> v = lr.getLocationRuleAttributes();
            if(!do_exp) {
                /* Start rule */
                ctx.appendStartObject(sb, "rule", lr.getID(), first);
                ctx.appendAttrib(sb, "type", lr.getLocationRuleFactory(), true);
                ctx.appendAttrib(sb, "target_location", lr.getRuleTargetLocation(), false);
            
                String[] k = lr.getLocationRuleFactory()
                    .getLocationRuleAttributeIDs();
                for (int i = 0; i < k.length; i++) {
                    Object val = v.get(k[i]); /* Get attribute value */
                    ctx.appendAttrib(sb, k[i], val, false);
                }
                ctx.appendAttrib(sb, "enabled", lr.getRuleEnabled(), false);
                ctx.appendEndObject(sb, "rule", lr.getID());
            }
            else {
                sb.append(RangerServer.encodeForCSV(key)).append(",").
                    append(RangerServer.encodeForCSV(lr.getLocationRuleFactory().getID()));
                sb.append(",").append(lr.getRuleEnabled()?"true":"false");
                sb.append(",").append(RangerServer.encodeForCSV(
                    lr.getRuleTargetLocation().getID()));
                for(String col : colids) {
                    Object val = v.get(col);    /* Find attrib, if any */
                    sb.append(",");
                    if(val != null) {
                        sb.append(RangerServer.encodeForCSV(val));
                    }
                }
                sb.append("\n");
            }
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            first = false;
        }
        if(!do_exp) {
            ctx.appendEndObject(sb, "rule-list", null);
            ctx.sendString(sb.toString());
        }
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(RULELIST_CMD))
            return RULELIST_CMD_HELP.clone();
        else
            return RULEEXPORT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(RULELIST_CMD))
            return RULELIST_CMD_SYNTAX_HELP.clone();
        else
            return RULEEXPORT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
