package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer.User;

/**
 * Handler for "whoami" command
 * 
 * @author Mike Primm
 */
public class UserCurrentHandler implements CommandHandler {
    public static final String USERCURRENT_CMD = "whoami";
    /* Command ID */
    public static final String[] CMD_IDS = {USERCURRENT_CMD};
    /* Command help */
    private static final String[] USERCURRENT_CMD_HELP = {"Show info on current user."};
    /* Command syntax help */
    private static final String[] USERCURRENT_CMD_SYNTAX_HELP = {USERCURRENT_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        User user = ctx.getUser();

        StringBuilder sb = new StringBuilder();

        ctx.appendStartObject(sb, "user-list", null, true);
        /* If there is a current user */
        if(user != null) {        
            ctx.appendStartObject(sb, "user", user.getID(), true);
            ctx.appendAttrib(sb, "role", user.getRole(), true);
            ctx.appendEndObject(sb, "user", user.getID());
        }
        ctx.appendEndObject(sb, "user-list", null);
        /* Output completed line */
        ctx.sendString(sb.toString());
        sb.setLength(0);
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return USERCURRENT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return USERCURRENT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
