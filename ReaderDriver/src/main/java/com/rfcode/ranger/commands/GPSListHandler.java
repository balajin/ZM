package com.rfcode.ranger.commands;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "gpslist" command
 * 
 * @author Mike Primm
 */
public class GPSListHandler implements CommandHandler {
    public static final String GPSLIST_CMD = "gpslist";
    /* Command ID */
    public static final String[] CMD_IDS = {GPSLIST_CMD};
    /* Command help */
    private static final String[] GPSLIST_CMD_HELP = {"List all position information sources and their current readings."};
    /* Command syntax help */
    private static final String[] GPSLIST_CMD_SYNTAX_HELP = {GPSLIST_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getGPSIDs());
        
        /* Start gps list */
        ctx.appendStartObject(sb, "gps-list", null, true);
        
        boolean first = true;
        for (String key : keys) {
            GPS gps = ReaderEntityDirectory.findGPS(key);
            ctx.appendStartObject(sb, "gps", gps.getID(), first);
            ctx.appendAttrib(sb, "srctype", gps.getType(), true);
            first = false;
			/* Get reader ID, and add it */
			Reader rdr = gps.getReader();
			if(rdr != null) {
	            ctx.appendAttrib(sb, "readerid", rdr.getID(), false);
			}
			/* Get GPS data */
			GPSData gpsd = gps.getGPSData();
			if(gpsd != null) {
	            ctx.appendAttrib(sb, "gpsfix", gpsd.getGPSFix().toString(), false);
				if((gpsd.getLatitude() != null) && (gpsd.getLongitude() != null)) {
					ArrayList<Double> latlon = new ArrayList<Double>();
					latlon.add(gpsd.getLatitude());
					latlon.add(gpsd.getLongitude());
		            ctx.appendAttrib(sb, "latlon", latlon, false);
				}
				if(gpsd.getAltitude() != null)
		            ctx.appendAttrib(sb, "altitude", gpsd.getAltitude(), false);
				if(gpsd.getSpeed() != null)
		            ctx.appendAttrib(sb, "speed", gpsd.getSpeed(), false);
				if(gpsd.getCourse() != null)
		            ctx.appendAttrib(sb, "course", gpsd.getCourse(), false);
				if(gpsd.getEPEHorizontal() != null)
		            ctx.appendAttrib(sb, "epe_horiz", gpsd.getEPEHorizontal(), false);
				if(gpsd.getEPEVertical() != null)
		            ctx.appendAttrib(sb, "epe_vert", gpsd.getEPEVertical(), false);
			}				
            ctx.appendEndObject(sb, "gps", gps.getID());
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        ctx.appendEndObject(sb, "gps-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return GPSLIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return GPSLIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
