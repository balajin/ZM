package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
/**
 * Handler for "channellist" command
 * 
 * @author Mike Primm
 */
public class ReaderChannelListHandler implements CommandHandler {
    public static final String CHANNELLIST_CMD = "channellist";
    /* Command ID */
    public static final String[] CMD_IDS = {CHANNELLIST_CMD};
    /* Command help */
    private static final String[] CHANNELLIST_CMD_HELP = {"List all reader channels and their attributes."};
    /* Command syntax help */
    private static final String[] CHANNELLIST_CMD_SYNTAX_HELP = {CHANNELLIST_CMD};
    private static final String[] CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Set<String> keys = new TreeSet<String>(ReaderEntityDirectory
            .getReaderChannelIDs());
        
        StringBuilder sb = new StringBuilder();
       
        /* Start reader-channel-list */
        ctx.appendStartObject(sb, "reader-channel-list", null, true);
        
        boolean first = true;
        for (String key : keys) {
            ReaderChannel chan = ReaderEntityDirectory.findReaderChannel(key);
            if(chan == null) continue;
            ctx.appendStartObject(sb, "reader-channel", chan.getID(), first);
            first = false;
            ctx.appendAttrib(sb, "reader", chan.getReader().getID(), true);
            ctx.appendAttrib(sb, "rel-id", chan.getRelativeChannelID(), false);
            ctx.appendAttrib(sb, "label", chan.getChannelLabel(), false);
            ctx.appendEndObject(sb, "reader-channel", chan.getID());
            ctx.sendString(sb.toString());
            sb.setLength(0);
        }
        ctx.appendEndObject(sb, "reader-channel-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CHANNELLIST_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CHANNELLIST_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
