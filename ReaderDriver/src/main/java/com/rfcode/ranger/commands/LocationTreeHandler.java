package com.rfcode.ranger.commands;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;

import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "loctree" commands
 * 
 * @author Mike Primm
 */
public class LocationTreeHandler implements CommandHandler {
    public static final String LOCTREE_CMD = "loctree";
    /* Command ID */
    public static final String[] CMD_IDS = {LOCTREE_CMD};
    /* Command help */
    private static final String[] LOCTREE_CMD_HELP = {"Show tree of all locations and their settings, including those inherited from their parents."};
    /* Command syntax help */
    private static final String[] LOCTREE_CMD_SYNTAX_HELP = {LOCTREE_CMD};
    private static final String[] LOCTREE_CMD_WEB_SYNTAX_HELP = {""};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        recurseTreePrint(ctx, null, 0);
    }
    /**
     * Recursive print of location tree.
     * 
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parent - 
     *            parent node of subtree to print
     * @param depth - 
     *            depth in tree
     * @throws CommandException
     *             if command error
     */
    private void recurseTreePrint(CommandContext ctx, Location parent,
        int depth) throws CommandException {
        TreeMap<String, Location> locmap = new TreeMap<String, Location>();
        Collection<Location> locs;
        boolean is_text = (ctx.getFormatExt() == CommandContext.OutputSyntax.TEXT);
        boolean is_json = (ctx.getFormatExt() == CommandContext.OutputSyntax.JSON);
        if (parent == null) { /* Top level */
            locs = ReaderEntityDirectory.getLocations();
        } else {
            locs = parent.getChildren();
        }
        /* Get em and sort em */
        for (Location loc : locs) {
            if ((parent != null) || (loc.getParent() == null)) {
                locmap.put(loc.getID(), loc);
            }
        }
        /* Build message for each child at this level */
        StringBuilder sb = new StringBuilder();
        
        /* Start location list */
        if(!is_text)
            ctx.appendStartObject(sb, "location-list", null, true);
        boolean first = true;
        for (Location lr : locmap.values()) {
            if(is_text)
                for (int i = 0; i < depth; i++)
                    sb.append("  ");
            /* Start location */
            ctx.appendStartObject(sb, "location", lr.getID(), first);
            Map<String, Object> v = lr.getFullAttributes();
            boolean firstattrib = true;
            for (Map.Entry<String, Object> ent : v.entrySet()) {
                ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), firstattrib);
                firstattrib = false;
            }
            if(is_json) {
                /* End attribute list and start children array */
                sb.append(" },\n \"children\" : ");
            }
            if(is_text)
                ctx.appendEndObject(sb, "location", lr.getID());
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            /* Now, recurse to handle children of this node */
            recurseTreePrint(ctx, lr, depth + 1);
            if(!is_text) {
                if(is_json) {
                    sb.append("\n}");
                } else {
                    ctx.appendEndObject(sb, "location", lr.getID());
                }
            }
            first = false;
        }
        if(!is_text)        
            ctx.appendEndObject(sb, "location-list", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        return new HashMap<String, String>();
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return LOCTREE_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return LOCTREE_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return LOCTREE_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
