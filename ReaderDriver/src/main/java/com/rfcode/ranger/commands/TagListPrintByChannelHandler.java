package com.rfcode.ranger.commands;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "taglistbychannel", "tagprintbychannel" commands
 * 
 * @author Mike Primm
 */
public class TagListPrintByChannelHandler implements CommandHandler {
    public static final String TAGLISTBYCHANNEL_CMD = "taglistbychannel";
    public static final String TAGPRINTBYCHANNEL_CMD = "tagprintbychannel";
    public static final String TAGCOUNTBYCHANNEL_CMD = "tagcountbychannel";
    /* Attribute ID for channel IDs (prefix - followed by zero-based number) */
    public static final String CHANNELID_PREFIX = "channelid";
    /* Attrib ID for start index and limit count (result windowing) */
    public static final String  FIRSTROW_ID = "_firstrow";
    public static final String  ROWCOUNT_ID = "_rowcnt";

    /* Command ID */
    public static final String[] CMD_IDS = {TAGLISTBYCHANNEL_CMD,
        TAGPRINTBYCHANNEL_CMD,TAGCOUNTBYCHANNEL_CMD};
    /* Command help */
    private static final String[] TAGLISTBYCHANNEL_CMD_HELP = {"List all tags visible to a specific reader or set of channels."};
    private static final String[] TAGPRINTBYCHANNEL_CMD_HELP = {"Print details of all tags visible to a specific reader or set of channels."};
    private static final String[] TAGCOUNTBYCHANNEL_CMD_HELP = {"Count number of tags visible to a specific reader or set of channels."};
    /* Command syntax help */
    private static final String[] TAGLISTBYCHANNEL_CMD_SYNTAX_HELP = {
        TAGLISTBYCHANNEL_CMD + " <reader-id> ...",
        TAGLISTBYCHANNEL_CMD + " <channel-id> ..."
        };
    private static final String[] TAGPRINTBYCHANNEL_CMD_SYNTAX_HELP = {
        TAGPRINTBYCHANNEL_CMD + " <reader-id> ...",
        TAGPRINTBYCHANNEL_CMD + " <channel-id> ..."
        };
    private static final String[] TAGCOUNTBYCHANNEL_CMD_SYNTAX_HELP = {
        TAGCOUNTBYCHANNEL_CMD + " <reader-id> ...",
        TAGCOUNTBYCHANNEL_CMD + " <channel-id> ..."
        };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        CHANNELID_PREFIX+"0=<reader-id>&"+CHANNELID_PREFIX+"1=<reader-id>...",
        CHANNELID_PREFIX+"0=<channel-id>&"+CHANNELID_PREFIX+"1=<channel-id>...",
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        boolean do_print = false;
        int firstrow = 0;
        int rowcnt = Integer.MAX_VALUE;
        int rowidx = 0;
        boolean do_count = false;
        
        if(cmd.equals(TAGCOUNTBYCHANNEL_CMD))
            do_count = true;
        
        /* Get row limits, if any */
        String p = parms.get(FIRSTROW_ID);
        if(p != null) {
            try {
                firstrow = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row index parameter");
            }
        }
        p = parms.get(ROWCOUNT_ID);
        if(p != null) {
            try {
                rowcnt = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row count parameter");
            }
        }
        /* Start tag list */
        if(do_count)
            ctx.appendStartObject(sb, "tag-count", null, true);
        else
            ctx.appendStartObject(sb, "tag-list", null, true);
        if(cmd.equals(TAGPRINTBYCHANNEL_CMD))
            do_print = true;
        if(parms.get(CHANNELID_PREFIX + 0) == null)
            throw new CommandParameterException("Missing reader ID or reader.channel ID or reader_channel_ID");
        /* Build set of distinct tags */
        Map<String, Tag> tags = new TreeMap<String, Tag>();
        /* Args are readerid.channelid or reader IDs */
        String id;
        for (int i = 0; (id = parms.get(CHANNELID_PREFIX + i)) != null; i++) {
            String[] parts = id.split("\\x2E");
            /* Try to find the reader */
            Reader r = ReaderEntityDirectory.findReader(parts[0]);
            ReaderChannel[] rc;
            if (r == null) { /* Not a reader - see if channel */
                ReaderChannel r_c = ReaderEntityDirectory
                    .findReaderChannel(parts[0]);
                if (r_c == null)
                    throw new CommandParameterException("Invalid reader or channel ID - " + parts[0]);
                rc = new ReaderChannel[1];
                rc[0] = r_c;
            } else {
                rc = r.getChannels();
            }
            boolean found = false;
            for (int j = 0; j < rc.length; j++) {
                /* If all channels for reader, or matches the channel */
                if ((parts.length < 2)
                    || (rc[j].getRelativeChannelID().equals(parts[1]))) {
                    found = true;
                    /* Now, get its taglinks, and add tag IDs to set */
                    Map<String, TagLink> tls = rc[j].getTagLinks();
                    for (TagLink tl : tls.values()) {
                        tags.put(tl.getTag().getTagGUID(), tl.getTag());
                    }
                }
            }
            if (!found)
                throw new CommandParameterException("Invalid channel ID - " + id);
        }
        TreeSet<String> keys = new TreeSet<String>(tags.keySet());
        /* Loop through the tags */
        boolean first = true;
        for (String tagid : keys) {
            Tag tag = tags.get(tagid);
            /* If outside limits, skip */
            if((rowidx < firstrow) || ((rowidx-firstrow) >= rowcnt)) {

            }
            else if (do_count) {
                /* Skip if just counting */
            }
            else if (do_print) {
                RangerServer.printTag(ctx, sb, tag, first);
                first = false;
            } else {
                ctx.appendStartObject(sb, "tag", tag.getTagGUID(), first);
                ctx.appendAttrib(sb, "taggroupid", tag.getTagGroup(), true);
                ctx.appendEndObject(sb, "tag", tag.getTagGUID());
                first = false;
            }
            /* Output completed line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            rowidx++;
        }
        if(do_count) {
            ctx.appendAttrib(sb, "count", rowidx, true);
            ctx.appendEndObject(sb, "tag-count", null);
        }
        else {
            ctx.appendEndObject(sb, "tag-list", null);
        }
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        /* Others, encode the channel IDs */
        for (int i = 0; i < tokenlist.size(); i++) {
            vals.put(CHANNELID_PREFIX + i, tokenlist.get(i));
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(TAGLISTBYCHANNEL_CMD))
            return TAGLISTBYCHANNEL_CMD_HELP.clone();
        else if(cmd.equals(TAGCOUNTBYCHANNEL_CMD))
            return TAGCOUNTBYCHANNEL_CMD_HELP.clone();
        else
            return TAGPRINTBYCHANNEL_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLISTBYCHANNEL_CMD))
            return TAGLISTBYCHANNEL_CMD_SYNTAX_HELP.clone();
        else if (cmd.equals(TAGCOUNTBYCHANNEL_CMD))
            return TAGCOUNTBYCHANNEL_CMD_SYNTAX_HELP.clone();
        else
            return TAGPRINTBYCHANNEL_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
