package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "readerget" command
 *
 * @author Mike Primm
 */
public class ReaderGetHandler implements CommandHandler {
    /* Parameter for reader ID */
    public static final String READERID = "id";
    /* Parameter for attribute ID (prefix to base-0 number) */
    public static final String READERPARMID_PREFIX = "attribid";
    /* Command ID */
    public static final String READERGET_CMD = "readerget";
    public static final String[] CMD_IDS = {READERGET_CMD};
    /* Command help */
    private static final String[] CMD_HELP = {"Get reader setting value or values."};
    /* Command syntax help */
    private static final String[] CMD_SYNTAX_HELP = {READERGET_CMD+" <reader-id>",
        READERGET_CMD + " <reader-id> <attrib-id> ..."};
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        READERID+"=<reader-id>",
        READERID+"=<reader-id>&"+READERPARMID_PREFIX+"0=<attrib-id>&"
        +READERPARMID_PREFIX+"1=<attrib-id>&..."};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     *
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        String id = parms.remove(READERID); /* Fetch our ID parm */
        if (id == null)
            throw new CommandParameterException("Missing reader ID");
        if (!RangerServer.testIfValidID(id))
            throw new CommandParameterException(CommandConstants.ID_INVALID_CHARACTERS);
        StringBuilder sb = new StringBuilder();
        /* See if ID exists */
        Reader rdr = ReaderEntityDirectory.findReader(id);
        if (rdr == null)
            throw new CommandParameterException("Invalid reader ID");
        ctx.appendStartObject(sb, "reader", id, true);
        /* Get attributes */
        Map<String, Object> attr = rdr.getReaderAttributes();
        /* If we're getting all, dump all of em */
        String pid = parms.get(READERPARMID_PREFIX + "0");
        boolean first = true;
        if (pid != null) {
            /* Else, do specific ones in order */
            for (int i = 0; (pid != null); i++) {
                if (pid.length() == 0)
                    continue;
                /* Get current attribute value */
                Object v;
                if(pid.equals("type"))
                    v = rdr.getReaderFactory().getID();
                else if(pid.equals("label"))
                    v = rdr.getLabel();
                else
                    v = attr.get(pid);
                ctx.appendAttrib(sb, pid, v, first);
                /* Get next one */
                pid = parms.get(READERPARMID_PREFIX + (i + 1));
                first = false;
            }
        } else { /* Else, dump all */
            String[] k = rdr.getReaderFactory().getReaderAttributeIDs();
            for (int j = 0; j < k.length; j++) {
                ctx.appendAttrib(sb, k[j], attr.get(k[j]), first);
                first = false;
            }
            /* Add the reader type */
            ctx.appendAttrib(sb, "type", rdr.getReaderFactory().getID(), first);
            first = false;
            ctx.appendAttrib(sb, "label", rdr.getLabel(), first);
        }
        ctx.appendEndObject(sb, "reader", id);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     *
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing reader ID");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add the first one */
        parms.put(READERID, tokenlist.get(0));
        /* Add others as listed attributes */
        for (int i = 1; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            parms.put(READERPARMID_PREFIX + (i - 1), s);
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     *
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     *
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     *
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     *
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
