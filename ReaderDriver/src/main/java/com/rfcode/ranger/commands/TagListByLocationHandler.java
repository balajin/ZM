package com.rfcode.ranger.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeSet;

import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Handler for "taglistbylocation", "tagcountbylocation" commands
 * 
 * @author Mike Primm
 */
public class TagListByLocationHandler implements CommandHandler {
    public static final String TAGLISTBYLOC_CMD = "taglistbylocation";
    public static final String TAGCOUNTBYLOC_CMD = "tagcountbylocation";
    /* Attribute ID for field IDs (prefix - followed by zero-based number) */
    public static final String FIELDID_PREFIX = "fieldid";
    /* Attribute ID for location IDs (prefix - followed by zero-based number) */
    public static final String LOCID_PREFIX = "locid";
    /* Attrib ID for start index and limit count (result windowing) */
    public static final String FIRSTROW_ID = "_firstrow";
    public static final String ROWCOUNT_ID = "_rowcnt";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGLISTBYLOC_CMD, TAGCOUNTBYLOC_CMD};
    /* Command help */
    private static final String[] TAGLISTBYLOC_CMD_HELP = {"List tags in given location, optionally as comma-seperated report."};
    private static final String[] TAGCOUNTBYLOC_CMD_HELP = {"Report number of tags in a given location."};
    /* Command syntax help */
    private static final String[] TAGLISTBYLOC_CMD_SYNTAX_HELP = {
        TAGLISTBYLOC_CMD + " <loc-id> ...",
        TAGLISTBYLOC_CMD + " fields=<fieldid>,<fieldid>,... <loc-id> ..."};
    private static final String[] TAGCOUNTBYLOC_CMD_SYNTAX_HELP = {TAGCOUNTBYLOC_CMD
        + " <loc-id> ..."};
    private static final String[] TAGLISTBYLOC_CMD_WEB_SYNTAX_HELP = {
        LOCID_PREFIX + "0=<loc-id>&" + LOCID_PREFIX + "1=<loc-id>&...",
        FIELDID_PREFIX + "0=<fieldid>&" + FIELDID_PREFIX + "1=<fieldid>&...&"
            + LOCID_PREFIX + "0=<loc-id>&" + LOCID_PREFIX + "1=<loc-id>&..."};
    private static final String[] TAGCOUNTBYLOC_CMD_WEB_SYNTAX_HELP = {LOCID_PREFIX
        + "0=<loc-id>&" + LOCID_PREFIX + "1=<loc-id>&...",};
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        StringBuilder sb = new StringBuilder();
        Collection<TagGroup> grps = ReaderEntityDirectory.getTagGroups();
        HashMap<String, Location> locids = new HashMap<String, Location>();
        boolean listall = false;
        boolean is_report = false;
        ArrayList<String> fieldids = null;
        TreeSet<String> maxattrids = null;
        int count = 0;
        boolean do_count = false;
        boolean inc_null = false;
        boolean is_text = ctx.getFormatExt().equals(
            CommandContext.OutputSyntax.TEXT);
        int firstrow = 0;
        int rowcnt = Integer.MAX_VALUE;
        int rowidx = 0;
        /* Get row limits, if any */
        String p = parms.get(FIRSTROW_ID);
        if (p != null) {
            try {
                firstrow = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row index parameter");
            }
        }
        p = parms.get(ROWCOUNT_ID);
        if (p != null) {
            try {
                rowcnt = Integer.parseInt(p);
            } catch (NumberFormatException nfx) {
                throw new CommandParameterException("Bad row count parameter");
            }
        }

        if (cmd.equals(TAGCOUNTBYLOC_CMD)) {
            do_count = true;
            maxattrids = new TreeSet<String>(); /* Collect attrib ids */
        }
        /* If any fields, we're doing report */
        if ((!do_count) && (parms.get(FIELDID_PREFIX + 0) != null)) {
            /* Build and output first line of report */
            if (is_text) {
                sb.append("tagid");
            }
            String fldid;
            fieldids = new ArrayList<String>(); /* Build array */
            for (int i = 0; (fldid = parms.get(FIELDID_PREFIX + i)) != null; i++) {
                fieldids.add(fldid);
                if (is_text)
                    sb.append(",").append(fldid);
            }
            if (is_text)
                sb.append("\n");
            /* Output the header line */
            ctx.sendString(sb.toString());
            sb.setLength(0);
            //if (is_text)
            //    ctx.setContentType("text/x-excel-csv");
            is_report = true;
        }
        /* Start tag list */
        if (do_count)
            ctx.appendStartObject(sb, "tag-count", null, true);
        else
            ctx.appendStartObject(sb, "tag-list", null, true);
        /* Build location list */
        listall = true;
        String locid;
        for (int i = 0; (locid = parms.get(LOCID_PREFIX + i)) != null; i++) {
            listall = false;
            if(locid.equals("null")) {
                inc_null = true;
                continue;
            }
            Location loc = ReaderEntityDirectory.findLocation(locid);
            if (loc == null)
                throw new CommandParameterException("Invalid location ID");
            locids.put(locid, loc);
            /* Add sublocations too */
            List<Location> sublocs = loc.getChildrenRecursive();
            for (Location subloc : sublocs) {
                locids.put(subloc.getID(), subloc);
            }
        }
        /* If none, we're getting all */
        if (listall) {
            Collection<Location> locs = ReaderEntityDirectory.getLocations();
            for (Location loc : locs) {
                locids.put(loc.getID(), loc);
            }
        }
        ctx.sendString(sb.toString());
        sb.setLength(0);
        boolean first = true;
        /* Loop through all tag groups */
        for (TagGroup tg : grps) {
            Map<String, Tag> tags = tg.getTags(); /* Get our tags */
            TreeSet<String> tids = new TreeSet<String>(tags.keySet());
            for (String id : tids) {
                Tag tag = tg.findTag(id);
                /* See if it matches our location constraints */
                Map<String, Object> tattr = tag.getTagAttributes();
                Object loc = tattr
                    .get(LocationRuleFactory.ATTRIB_ZONELOCATION);
                if ((loc != null) && (loc instanceof String)
                    && (locids.containsKey((String) loc) || listall ||
                        (inc_null && loc.equals("")))) {
                    /* If in range of our limits */
                    if ((rowidx < firstrow) || ((rowidx - firstrow) >= rowcnt)) {
                        /* If not, skip it */
                    } else if (is_report) { /* If report, generate output */
                        RangerServer.printTagReport(ctx, sb, tag, fieldids, first);
                        first = false;
                    } else {    /* Else, generate list element or maintain count */
                        Location ourloc = locids.get((String) loc);
                        if (do_count) {
                            count++;
                        } else {
                            ctx.appendStartObject(sb, "tag", tag.getTagGUID(),
                                first);
                        }
                        boolean afirst = true;
                        /* And add on tag attributes too */
                        for (Map.Entry<String, Object> ent : tattr.entrySet()) {
                            if (do_count)
                                maxattrids.add(ent.getKey());
                            else
                                ctx.appendAttrib(sb, ent.getKey(), ent
                                    .getValue(), afirst);
                            afirst = false;
                        }
                        if (ourloc != null) {
                            Map<String, Object> vals = ourloc
                                .getFullAttributes();
                            TreeSet<String> keys = new TreeSet<String>(vals
                                .keySet());
                            for (String k : keys) {
                                if (do_count)
                                    maxattrids.add(k);
                                else
                                    ctx.appendAttrib(sb, k, vals.get(k), afirst);
                                afirst = false;
                            }
                        }
                        /* Finish up the line */
                        if (!do_count) {
	                        ctx.appendAttrib(sb, "taggroupid", tag.getTagGroup(), afirst);
							afirst = false;
                            ctx.appendEndObject(sb, "tag", tag.getTagGUID());
                        }
                        first = false;
                    }
                    /* Output completed line */
                    if(sb.length() > 0) {
                        ctx.sendString(sb.toString());
                        sb.setLength(0);                        
                    }
                    rowidx++;   /* Bump index, since we matched */
                }
            }
        }
        if (do_count) {
            ctx.appendAttrib(sb, "count", count, true);
            ctx.appendAttrib(sb, "attribids", maxattrids, false);
            ctx.appendEndObject(sb, "tag-count", null);
        } else {
            ctx.appendEndObject(sb, "tag-list", null);
        }
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        /* Loop through parameters */
        for (int i = 0, locid = 0; i < tokenlist.size(); i++) {
            String tok = tokenlist.get(i);
            if (tok.startsWith("fields=")) { /* If fields list */
                /* Get list of field IDs */
                String[] flds = tok.substring("fields=".length()).split(",");
                for (int j = 0; j < flds.length; j++) {
                    vals.put(FIELDID_PREFIX + j, flds[j]);
                }
            } else { /* ELse, its a location ID */
                vals.put(LOCID_PREFIX + locid, tok);
                locid++;
            }
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if (cmd.equals(TAGLISTBYLOC_CMD))
            return TAGLISTBYLOC_CMD_HELP.clone();
        else
            return TAGCOUNTBYLOC_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLISTBYLOC_CMD))
            return TAGLISTBYLOC_CMD_SYNTAX_HELP.clone();
        else
            return TAGCOUNTBYLOC_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        if (cmd.equals(TAGLISTBYLOC_CMD))
            return TAGLISTBYLOC_CMD_WEB_SYNTAX_HELP.clone();
        else
            return TAGCOUNTBYLOC_CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
