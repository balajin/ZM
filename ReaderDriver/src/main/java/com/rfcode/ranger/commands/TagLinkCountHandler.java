package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;

/**
 * Handler for "taglinkcount" command
 * 
 * @author Mike Primm
 */
public class TagLinkCountHandler implements CommandHandler {
    public static final String TAGLINKCOUNT_CMD = "taglinkcount";
    /* Attribute ID for group IDs (prefix - followed by zero-based number) */
    public static final String GROUPID_PREFIX = "groupid";
    /* Command ID */
    public static final String[] CMD_IDS = {TAGLINKCOUNT_CMD};
    /* Command help */
    private static final String[] TAGLINKCOUNT_CMD_HELP = {"Get count of all tag links, or tag links in one or more specific tag groups."};
    /* Command syntax help */
    private static final String[] TAGLINKCOUNT_CMD_SYNTAX_HELP = { 
        TAGLINKCOUNT_CMD, TAGLINKCOUNT_CMD + " <group-id> ..."
    };
    /* Command syntax help */
    private static final String[] CMD_WEB_SYNTAX_HELP = { 
        "",
        GROUPID_PREFIX + "0=<group-id>&"+GROUPID_PREFIX+"1=<group-id>..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        Set<String> grps;
        int count = 0;
        StringBuilder sb = new StringBuilder();
        /* Start tag link list */
        ctx.appendStartObject(sb, "tag-link-count", null, true);
        grps = new TreeSet<String>();
        String grp;
        for (int i = 0; (grp = parms.get(GROUPID_PREFIX + i)) != null; i++) {
            grps.add(grp);
        }
        if(grps.size() == 0) {  /* If none provided, do all */
            grps = ReaderEntityDirectory.getTagGroupIDs();
        }
        for (String key : grps) {
            TagGroup tg = ReaderEntityDirectory.findTagGroup(key);
            if (tg == null) {
                throw new CommandParameterException("Invalid tag group ID");
            }
            Map<String, Tag> tags = tg.getTags(); /* Get our tags */
            for (Tag tag : tags.values()) {
                count += tag.getTagLinkCount();
            }
        }
        ctx.appendAttrib(sb, "count", count, true);
        ctx.appendEndObject(sb, "tag-link-count", null);
        ctx.sendString(sb.toString());
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        HashMap<String, String> vals = new HashMap<String, String>();
        /* Others, encode the group IDs */
        for (int i = 0; i < tokenlist.size(); i++) {
            vals.put(GROUPID_PREFIX + i, tokenlist.get(i));
        }
        return vals;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        return TAGLINKCOUNT_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        return TAGLINKCOUNT_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return false;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return false;
    }
}
