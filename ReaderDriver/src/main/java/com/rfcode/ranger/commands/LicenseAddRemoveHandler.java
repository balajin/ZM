package com.rfcode.ranger.commands;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.ranger.CommandContext;
import com.rfcode.ranger.CommandException;
import com.rfcode.ranger.CommandHandler;
import com.rfcode.ranger.CommandParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.util.keys.LicenseKey;

/**
 * Handler for "licenseadd" and "licenseremove" commands
 * 
 * @author Mike Primm
 */
public class LicenseAddRemoveHandler implements CommandHandler {
    /* Parameter for license key */
    public static final String LICENSEKEY_PREFIX = "key";
    /* Command ID */
    public static final String LICENSEADD_CMD = "licenseadd";
    public static final String LICENSEREMOVE_CMD = "licenseremove";
    public static final String[] CMD_IDS = {
        LICENSEADD_CMD, LICENSEREMOVE_CMD
        };
    /* Command help */
    private static final String[] LICENSEADD_CMD_HELP = {
            "Install license key to enable activation of additional readers."};
    private static final String[] LICENSEREMOVE_CMD_HELP = {
            "Remove license key."};
    /* Command syntax help */
    private static final String[] LICENSEADD_CMD_SYNTAX_HELP = {
        LICENSEADD_CMD + " <key> <key> ..."
    };
    private static final String[] LICENSEREMOVE_CMD_SYNTAX_HELP = {
        LICENSEREMOVE_CMD + " <key> <key> ..."
    };
    private static final String[] CMD_WEB_SYNTAX_HELP = {
        LICENSEKEY_PREFIX + "0=<key>&" + 
        LICENSEKEY_PREFIX + "1=<key>&..."
    };
    /**
     * Command invocation - called from within context of SessionFactory
     * processing thread, so must run quickly.
     * 
     * @param cmd -
     *            actual command ID (needed if more than one supported)
     * @param ctx -
     *            context for request (user, I/O channel, etc).
     * @param parms -
     *            map of parameter values, keyed by ID
     * @throws CommandException
     *             if command error
     */
    public void invokeCommand(String cmd, CommandContext ctx,
        Map<String, String> parms) throws CommandException {
        boolean do_add = false;
        if(cmd.equals(LICENSEADD_CMD)) {
            do_add = true;
        }
        /* Loop through the keys */
        String keystr;
        for (int i = 0; (keystr = parms.get(LICENSEKEY_PREFIX + i)) != null; i++) {
            LicenseKey lk = new LicenseKey(keystr);
            if(!do_add) {
                ReaderEntityDirectory.removeLicenseKey(keystr);
            }
            else if((lk.isKeyValid() == false)) {
                throw new CommandParameterException("Invalid license key - " + keystr);
            }
            else if(lk.isKeyExpired()) {
                throw new CommandParameterException("Expired license key - " + keystr);
            }
            else if(lk.getProductID() != LicenseKey.PROD_ZONEMGR && lk.getProductID() != LicenseKey.PROD_ZONEMGR_SPM) {
                throw new CommandParameterException("Key for wrong product - " + keystr);
            }
            else { 
                keystr = lk.getKeyString();
                ReaderEntityDirectory.addLicenseKey(keystr);
            }
        }
        /* Update saved licenses */
        RangerServer.updateLicenses();
    }
    /**
     * Translate ordered list of parameter tokens (from text command line) into
     * parameter map.
     * 
     * @param cmd -
     *            command to be parsed
     * @param tokenlist -
     *            List of token strings
     * @return map parameters, keyed by parameter ID
     * @throws CommandParameterException
     *             if error in parameters
     */
    public Map<String, String> translateCommandLine(String cmd,
        List<String> tokenlist) throws CommandParameterException {
        if (tokenlist.size() < 1) {
            throw new CommandParameterException("Missing key");
        }
        HashMap<String, String> parms = new HashMap<String, String>();
        /* Add others as listed attributes */
        for (int i = 0; i < tokenlist.size(); i++) {
            String s = tokenlist.get(i);
            parms.put(LICENSEKEY_PREFIX + i, s);
        }
        return parms;
    }
    /**
     * Command ID list - what are the command codes supported by this command?
     * 
     * @return list of command IDs
     */
    public String[] getCommandIDs() {
        return CMD_IDS.clone();
    }
    /**
     * Get help text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandHelp(String cmd) {
        if(cmd.equals(LICENSEADD_CMD))
            return LICENSEADD_CMD_HELP.clone();
        else
            return LICENSEREMOVE_CMD_HELP.clone();
    }
    /**
     * Get syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandSyntaxHelp(String cmd) {
        if(cmd.equals(LICENSEADD_CMD))
            return LICENSEADD_CMD_SYNTAX_HELP.clone();
        else
            return LICENSEREMOVE_CMD_SYNTAX_HELP.clone();
    }
    /**
     * Get web syntax text for command
     * 
     * @param cmd -
     *            command requesting help for
     */
    public String[] getCommandWebSyntaxHelp(String cmd) {
        return CMD_WEB_SYNTAX_HELP.clone();
    }
    /**
     * Requires admin priv?
     * 
     * @return true if admin required
     */
    public boolean isAdminRequired() {
        return true;
    }
    /**
     * Requires config read priv?
     * 
     * @return true if config read required
     */
    public boolean isConfigReadRequired() {
        return true;
    }
}
