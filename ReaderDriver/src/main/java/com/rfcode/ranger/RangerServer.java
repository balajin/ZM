package com.rfcode.ranger;


import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.Random;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Enumeration;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.LinkedList;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.security.KeyStore;
import java.util.concurrent.LinkedBlockingQueue;
import javax.net.ssl.SSLContext;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;
import com.rfcode.drivers.*;
import com.rfcode.drivers.impl.SessionFactoryImpl;
import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.drivers.readers.SessionUsingReaderFactory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderServiceStatusListener;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.GPSLifecycleListener;
import com.rfcode.drivers.readers.GPSStatusListener;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.drivers.readers.GPSDefaults;
import com.rfcode.drivers.readers.mantis2.*;
import com.rfcode.ranger.container.Containers;
import com.rfcode.spm.SPMReaderFactory;
import com.rfcode.drivers.readers.ListListDouble;
import com.rfcode.drivers.readers.ListDouble;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import org.apache.log4j.Logger;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.util.keys.LicenseKey;

import org.w3c.dom.Document;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Ranger server main application
 *
 * @author Mike Primm
 */
public class RangerServer {

    public static String COMPANYNAME = "RF Code";
    public static String COMPANYURL = "http://www.rfcode.com";
    public static String PRODUCTNAME = "RF Code Zone Manager";
    private static String VERSION = "2.3.0";
    private static String BUILD = "private(mike)";
    private static int  READERLIMIT = -1;
    private static final String PROMPT = "# ";
    private static final String LOGINPROMPT = "Login: ";
    private static final String PWDPROMPT = "Password: ";
    private static final String ERRORPREFIX = "<error>: ";
    private static final int LOGINLIMIT = 3;
    private static final int LICENSE_TIMEOUT = 24;  /* Hours to timeout temp license grant */
    private static SessionFactory sf;
    private static Properties base_config = new Properties();
    private static Properties cust_config = new Properties();
    private static Properties our_config = new Properties();    /* Combined config */
    private static Properties system_config = new Properties();  /* For http/https port and auth info for keystore */
    private static File data_dir;
    private static File system_config_dir;
    private static ReaderStatusHandler statuslistener = new ReaderStatusHandler();
    private static SimpleDateFormat datefmt = new SimpleDateFormat(
        "(yyyy-MM-dd HH:mm:ssZ) ");
    private static HashMap<String, User> users = new HashMap<String, User>();
    /* Our command handlers */
    private static HashMap<String, CommandHandler> cmds = new HashMap<String, CommandHandler>();
    /* List of reader status listeners */
    private static ArrayList<ReaderStatusListener> rdrlisteners =
        new ArrayList<ReaderStatusListener>();
    /* List of reader lifecycle listeners */
    private static ArrayList<ReaderLifecycleListener> rdrlifelisteners =
        new ArrayList<ReaderLifecycleListener>();
	/* List of GPS status listeners */
	private static ArrayList<GPSStatusListener> gpslisteners =
		new ArrayList<GPSStatusListener>();
	/* List of GPS lifecycle listeners */
	private static ArrayList<GPSLifecycleListener> gpslifelisteners =
		new ArrayList<GPSLifecycleListener>();

    private static boolean isListenAssetTagsOnly = false;

    private static final String RANGER_DATADIR_PROPERTY = "zonemgr.datadir";
    private static final String ATTRIB_PROPERTY_PREFIX = "attrib.";

    /** Server settings property files - loaded from datadir, overlays builtin props */
    private static final String SERVER_FILENAME = "server.properties";

	/** GPS defaults property file */
    private static final String GPSDEF_FILENAME = "gpsdef.properties";
    
    /** System settings property file - loaded from system_config_dir; */
    private static final String SYSTEM_FILENAME = "system.properties";

    /** Property IDs for .taggroup property files */
    private static final String TAGGROUP_FILENAME_SUFFIX = ".taggroup";
    private static final String TAGGROUP_TAGGROUPID_PROPERTY = "taggroupid";
    private static final String TAGGROUP_TAGTYPE_PROPERTY = "tagtype";
    private static final String TAGGROUP_LABEL_PROPERTY = "label";
    /** Property IDs for .reader property files */
    private static final String READER_FILENAME_SUFFIX = ".reader";
    private static final String READER_FACTORY_PROPERTY = "factory";
    private static final String READER_READERID_PROPERTY = "readerid";
    private static final String READER_TAGGROUP_PREFIX = "taggroup.";
    private static final String READER_ENABLED_PROPERTY = "enabled";
    private static final String READER_LABEL_PROPERTY = "label";
    private static final String ENABLED_TRUE = "true";
    private static final String ENABLED_FALSE = "false";
    /** Property IDs for .locationrule property files */
    private static final String LOCATIONRULE_FILENAME_SUFFIX = ".locationrule";
    private static final String LOCATIONRULE_RULEID_PROPERTY = "ruleid";
    private static final String LOCATIONRULE_ZONEID_PROPERTY = "zoneid";
    private static final String LOCATIONRULE_RULETYPE_PROPERTY = "ruletype";
    private static final String LOCATIONRULE_ENABLED_PROPERTY = "enabled";
    /** Property IDs for .location property files */
    private static final String LOCATION_FILENAME_SUFFIX = ".location";
    private static final String LOCATION_ZONEID_PROPERTY = "zoneid";
    private static final String LOCATION_PARENTID_PROPERTY = "parentid";
    private static final String LOCATION_PARENT_ATTRID = "parent";

    /** Property IDs for .user property files */
    private static final String USER_FILENAME_SUFFIX = ".user";
    private static final String USER_USERID_PROPERTY = "userid";
    private static final String USER_PWDHASH_PROPERTY = "pwdhash";
    private static final String USER_ROLE_PROPERTY = "role";

    /** Property IDs for license.key file */
    private static final String LICENSE_KEY_FILENAME = "license.key";
    private static final String KEY_PREFIX = "key.";

    /** Property IDs for .sensordef property files */
    private static final String SENSORDEF_FILENAME_SUFFIX = ".sensordef";
    private static final String SENSORDEF_DEFID_PROPERTY = "defid";
    private static final String SENSORDEF_SENSORTYPE_PROPERTY = "sensortype";
    private static final String SENSORDEF_LABEL_PROPERTY = "label";
    private static final String SENSORDEF_DEST_PROPERTY = "dest";

    /** File name for tag expected locations file */
    private static final String TAGEXPLOC_FILENAME = "tagexploc";
    private static final String ASSET_TAGS_SET_FILENAME = "assetTagsSet";

    private static final String TAG_EXP_CONF_BUMP = "tagexplocbump";

    public static final String NULL_VAL = "null";
    public static final String LOCATIONPATH_ID = "locationpath";
    public static final String CONFIG_HASH_FILENAME = "confighash";
    public static final String CONFIG_HASH_PROPERTY = "hash";
    public static final String LISTEN_ASSET_TAGS_ONLY_PROPERTY = "listen_asset_tags_only";

    private static TCPIPServerSocket cmd_ss;
    private static TCPIPServerSocket evt_ss;
    private static TCPIPServerSocket upconn_ss;
    private static String   config_hash;
    private static Random   rnd = new Random();

    /* Update event storage manager */
    private static UpdateEventStorageServer evtstore = null;

    /* Config update subscribers */
    private static ArrayList<ConfigUpdateListener> cfgupd = new ArrayList<ConfigUpdateListener>();

	/* Decimal number formatter : prevent very small fraction rounding issues */
	private static DecimalFormat decfmt;

    /* Key store manager */
    private static KeyStoreManager keymgr;

    /*
     * Simple user account object, used for login access control for server
     * ports
     */
    public static class User {
        String userid;
        String pwd_hash; /* String representation of MD5 hash of password */
        String role; /* Access role */
        public static final String ROLE_ADMIN = "admin"; /* Full access */
        public static final String ROLE_CONFIGVIEW = "configview"; /*
                                                                     * Read-only
                                                                     * control
                                                                     * port
                                                                     */
        public static final String ROLE_EVENTVIEW = "eventview"; /*
                                                                     * event
                                                                     * port view
                                                                     * only
                                                                     */

        User(String uid, String pwd, String role) throws BadParameterException {
            userid = uid;
            pwd_hash = toHashString(pwd);
            if (role.equals(ROLE_ADMIN) || role.equals(ROLE_CONFIGVIEW)
                || role.equals(ROLE_EVENTVIEW))
                this.role = role;
            else
                throw new BadParameterException("Invalid role");
        }
        public static String toHashString(String pwd) {
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] hash = md.digest(pwd.getBytes());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < hash.length; i++) {
                    if (i > 0)
                        sb.append(":");
                    sb.append((int) hash[i]);
                }
                return sb.toString();
            } catch (NoSuchAlgorithmException nsax) {
                return pwd;
            }
        }
        public String getID() {
            return userid;
        }
        public String getRole() {
            return role;
        }
        public void setPasswdHash(String hash) {
            pwd_hash = hash;
        }
    }

    /* Handler for reader status changes */
    private static class ReaderStatusHandler implements ReaderServiceStatusListener {
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         *
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            if(newstatus != oldstatus) {
                InboundSessionHandler.broadcastString("ReaderStatus: readerid="
                    + reader.getID() + ", status=\"" + newstatus.toString()
                    + "\"\n");
            }
            ArrayList<ReaderStatusListener> lst = rdrlisteners;
            int sz = lst.size();
            for(int i = 0; i < sz; i++) {
                lst.get(i).readerStatusChanged(reader, newstatus, oldstatus);
            }
        }
        /**
         * Report for service message
         * @param reader - reader reporting message
         * @param svcid - service ID
         * @param svcmsg - service message
         */
        public void reportServiceMessage(Reader reader, String svcid, String svcmsg) {
            InboundSessionHandler.broadcastString("ReaderServiceMsg: readerid="
                + reader.getID() + ", service=\"" + svcid + "\", message=\"" +
                svcmsg + "\"\n");
            ArrayList<ReaderStatusListener> lst = rdrlisteners;
            int sz = lst.size();
            for(int i = 0; i < sz; i++) {
                ReaderStatusListener rsl = lst.get(i);
                if(rsl instanceof ReaderServiceStatusListener) {
                    ((ReaderServiceStatusListener)rsl).reportServiceMessage(reader,
                        svcid, svcmsg);
                }
            }
        }
    }

    /* Handler for inbound (command) sessions */
    private static class InboundSessionHandler implements SessionHandler,
        MantisIIReaderCommandResponseListener, CommandContext {
        private StringBuilder sb = new StringBuilder();
        private String sessid;
        private Session sess;
        private boolean cmdsess;
        private int state = STATE_LOGIN;
        private int logincnt = 0;
        private User user; /* User active on port */
        private boolean is_admin;
        private boolean is_config_view;
        private boolean hide_broadcasts;
        private OutputSyntax ext;
        private boolean cmd_pending;
        private CommandHandler pending_cmdhand;
        private String pending_cc;
        private Object wobj = new Object();
        private static final int STATE_LOGIN = 0;
        private static final int STATE_PASSWORD = 1;
        private static final int STATE_AUTHENTICATED = 2;

        private static List<InboundSessionHandler> active_sessions = new ArrayList<InboundSessionHandler>();
		private static Object lock = new Object();

        /**
         * Constructor for inbound sessions
         *
         * @param s -
         *            communications session
         * @param sessionid -
         *            session ID string
         * @param cmd_sess -
         *            if true, its a command session. Otherwise, its a tag event
         *            session
         */
        public InboundSessionHandler(Session s, String sessionid,
            boolean cmd_sess) {
            sessid = sessionid;
            sess = s;
            cmdsess = cmd_sess;
			synchronized(lock) {
				List<InboundSessionHandler> newlist = new ArrayList<InboundSessionHandler>(active_sessions);
	            newlist.add(this);	
				active_sessions = newlist;
			}
            sess.setSessionHandler(this);
            hide_broadcasts = false;
            ext = OutputSyntax.TEXT;
        }
        public void cleanup() {
			synchronized(lock) {
				List<InboundSessionHandler> newlist = new ArrayList<InboundSessionHandler>(active_sessions);
	            newlist.remove(this);	
				active_sessions = newlist;
			}
            if (sess != null) {
                sess.setSessionHandler(null);
                sess.cleanup();
                sess = null;
            }
            user = null;
        }
        public void sessionStartingConnect(Session s) {
        }

        public void sessionConnectCompleted(final Session s, final boolean is_connected) {
            sess = s;
  		    if (cmdsess) { /* Send logo and prompt for cmdsess */
                sendString(PRODUCTNAME + " - Command Server\n");
  		    } else
   		        sendString(PRODUCTNAME + " - Tag Event Server\n");
            if (users.size() == 0) { /* No users? */
   		        state = STATE_AUTHENTICATED; /* We're good */
   		        is_admin = true;
   		        is_config_view = true;
   		        sendString(PROMPT);
			} else
        		sendString(LOGINPROMPT);
        }
        /**
         * Send string to all active command sessions, tag message sessions, or
         * both.
         *
         * @param s -
         *            string to be sent
         * @param cmd_sess -
         *            if true, send to command sessions.
         * @param tag_sess -
         *            if tru, send to tag event sessions.
         */
        public static void broadcastString(String s, boolean cmd_sess,
            boolean tag_sess) {
            String date = null;
            byte[] bb = null;
			List<InboundSessionHandler> list;

			synchronized(lock) {
				list = active_sessions;	/* Grab iteration safe reference, since we replace list on update */
			}
            for (InboundSessionHandler sess : list) {
                if (sess.sess == null)
                    continue;
                if (sess.state != STATE_AUTHENTICATED)
                    continue;
                if (sess.hide_broadcasts)
                    continue;
                if ((sess.cmdsess && cmd_sess) || ((!sess.cmdsess) && tag_sess)) {
                    if (date == null) {
                        date = datefmt.format(new Date());
                        s = date + s;
                        bb = getBytesForString(s);
	                }
                    try {
                        sess.sess.requestDataWrite(bb, 0, bb.length, null);
                    } catch (SessionException sx) {
                        error(sess.sessid + ": " + sx);
                    }
	            }
			}
        }
        /**
         * Send string to all active sessions
         *
         * @param s -
         *            string to be sent
         */
        public static void broadcastString(String s) {
            broadcastString(s, true, true);
        }
        /**
         * Test if any sessions to broadcast to
         *
         * @param cmd_sess -
         *            if true, send to command sessions.
         * @param tag_sess -
         *            if tru, send to tag event sessions.
         */
        public static boolean broadcastTest(boolean cmd_sess, boolean tag_sess) {
			List<InboundSessionHandler> list;

			synchronized(lock) {
				list = active_sessions;	/* Grab iteration safe reference, since we replace list on update */
			}
            for (InboundSessionHandler sess : list) {
	            if (sess.sess == null)
	                continue;
	            if (sess.state != STATE_AUTHENTICATED)
	                continue;
	            if (sess.hide_broadcasts)
	                continue;
	            if ((sess.cmdsess && cmd_sess) || ((!sess.cmdsess) && tag_sess)) {
	                return true;
	            }
	        }
            return false;
        }
        /**
         * Session disconnected - used to notify when the session has become
         * disconnected from its target. Starting with this call, requests to
         * write data to the session can no longer be enqueued.
         * sessionDataRead() and sessionDataWriteComplete() callbacks will not
         * happen once the session is disconnected.
         *
         * @param s -
         *            session
         */
        public void sessionDisconnectCompleted(Session s) {
            cleanup();
        }
        /**
         * Handle a session passthru command
         */
        private void handleSessionPassthru(StringBuilder sb) {
            if (!isAdmin())
                return;
            int end = sb.indexOf(":"); /* Find end */
            if (end >= 0) {
                String id = sb.substring(1, end);
                /* Find the selected session */
                Reader sess = ReaderEntityDirectory.findReader(id);
                if ((sess != null) && (sess instanceof MantisIIReader)) { /*
                                                                             * If
                                                                             * found
                                                                             */
                    end++;
                    while ((end < sb.length()) && (sb.charAt(end) == ' '))
                        end++;
                    /* Create new command request */
                    ((MantisIIReader) sess).getReaderSession().enqueueCommand(
                        sb.substring(end), this);
                } else {
                    sendString(ERRORPREFIX + "Invalid target - " + id + "\n");
                }
            }
        }
        /**
         * Report that command processing is incomplete - processing code will call
         * the commandCompleted() method once it is complete
         */
        public void commandStillInProgress() {
            cmd_pending = true;
        }
        /**
         * Report that a command previously reported as commandStillInProgress() has
         * completed.
         */
        public void commandCompleted(CommandException cx) {
            try {
                if(cx == null) {
    
                }
                else if(cx instanceof CommandParameterException) {
                    if (ext == OutputSyntax.XML) {
                        sendString("<error>"
                            + RangerServer.toXMLString(cx.getMessage())
                            + "</error>\n");
                    } else if (ext == OutputSyntax.JSON) {
                        sendString("{ \"__result\" : \"error\" , \"__msg\" : "
                            + RangerServer.toJSONString(cx.getMessage())
                            + " } \n");
                    } else {
                        sendString(ERRORPREFIX + cx.getMessage() + "\n");
                        String[] syntax = pending_cmdhand.getCommandSyntaxHelp(pending_cc);
                        for (String s : syntax) {
                            sendString("  " + s + "\n");
                        }
                    }
                }
                else {
                    if (ext == OutputSyntax.XML) {
                        sendString("<error>"
                            + RangerServer.toXMLString(cx.getMessage())
                            + "</error>\n");
                    } else if (ext == OutputSyntax.JSON) {
                        sendString("{ \"__result\" : \"error\" , \"__msg\" : "
                            + RangerServer.toJSONString(cx.getMessage())
                            + " } \n");
                    } else {
                        sendString(ERRORPREFIX + cx.getMessage() + "\n");
                    }
                }
            } finally {
                synchronized(wobj) {
                    wobj.notifyAll();
                }
            }
        }
        /**
         * Process command with tokenized parameters
         */
        private void handleCommand(ArrayList<String> tokens, Session sess) {
            String[] cmd = new String[tokens.size()];
            tokens.toArray(cmd);
            if (cmd.length == 0) { /* No args? */
                commandCompleted(null);
                return;
            }
            String cc = cmd[0];
            /* Check for extension */
            int idx = cc.indexOf('.');
            ext = OutputSyntax.TEXT;
            if (idx >= 0) {
                String x = cc.substring(idx + 1);
                if (x.equals("xml"))
                    ext = OutputSyntax.XML;
                else if (x.equals("json"))
                    ext = OutputSyntax.JSON;
                cc = cc.substring(0, idx);
            }
            CommandHandler cmdhand = cmds.get(cc); /* Find command handler */

            /* If handler, process using it */
            if (cmdhand != null) {
                /*
                 * If admin command and not admin, or read-config and not
                 * config-viewer, or read-config and not command session
                 */
                if ((cmdhand.isAdminRequired() && (!is_admin))
                    || (cmdhand.isConfigReadRequired() && (!is_config_view))
                    || (cmdhand.isConfigReadRequired() && (!cmdsess))) {
                    if (ext == OutputSyntax.XML) {
                        sendString("<error>Access not allowed</error>\n");
                    } else if (ext == OutputSyntax.JSON) {
                        sendString("{ \"__result\" : \"error\" , \"__msg\" : \"Access not allowed\" } \n");
                    } else {
                        sendString(ERRORPREFIX + "Access not allowed\n");
                    }
                    commandCompleted(null);
                } else {
                    try {
                        tokens.remove(0); /* Remove first token(cmd) */
                        Map<String, String> parms = cmdhand
                            .translateCommandLine(cc, tokens);
                        cmd_pending = false;
                        pending_cmdhand = cmdhand;
                        pending_cc = cc;
                        cmdhand.invokeCommand(cc, this, parms);
                        if(!cmd_pending)
                            commandCompleted(null);
                    } catch (CommandException cx) {
                        commandCompleted(cx);
                    }
                }
            } else {
                if (ext == OutputSyntax.XML) {
                    sendString("<error>Unknown command</error>\n");
                } else if (ext == OutputSyntax.JSON) {
                    sendString("{ \"__result\" : \"error\" , \"__msg\" : \"Unknown command\" } \n");
                } else {
                    sendString(ERRORPREFIX + "Unknown command\n");
                }
                commandCompleted(null);
            }
        }
        /**
         * Data read from session - used to deliver raw data reads from session.
         *
         * @param sdr -
         *            read request result - only valid for duration of
         *            sessionDataRead callback
         */
        public void sessionDataRead(final SessionRead sdr) {
			final Session sess = sdr.getSession();
			byte[] b = sdr.readBytes();
            for (int i = 0; i < b.length; i++) { /* Loop through them */
                char c = (char) b[i]; /* We're ASCII, so we can do this */
                if (c == '\r') { /* End of line? */
                    if (sb.length() > 0) {
                        /* Waiting for userid? */
                        if (state == STATE_LOGIN) {
                            /* Find the user - don't report fail here */
                            user = users.get(sb.toString());
                            state = STATE_PASSWORD;
                        } else if (state == STATE_PASSWORD) {
                            if ((user != null)
                                && (User.toHashString(sb.toString())
                                    .equals(user.pwd_hash))) {
                                /*
                                 * If event-view role and command session, kick
                                 * em
                                 */
                                if (user.role.equals(User.ROLE_EVENTVIEW)
                                    && (cmdsess)) {
                                    try {
                                        sess.requestSessionDisconnect();
                                    } catch (SessionException sx) {

                                    }
                                    state = STATE_LOGIN;
                                    return;
                                } else
                                    state = STATE_AUTHENTICATED;
                                is_admin = user.role.equals(User.ROLE_ADMIN);
                                is_config_view = is_admin
                                    || user.role.equals(User.ROLE_CONFIGVIEW);

                            } else { /* Else, login error */
                                sendString(ERRORPREFIX
                                    + "Bad user-id or password\n");
                                state = STATE_LOGIN;
                                logincnt++;
                                if (logincnt >= LOGINLIMIT) {
                                    try {
                                        sess.requestSessionDisconnect();
                                    } catch (SessionException sx) {

                                    }
                                    return;
                                }
                                user = null;
                            }
                        }
                        /* First, see if its a session targetted cmd */
                        else if ((!cmdsess) && (sb.charAt(0) == '@')) {
							try {
								RangerProcessor.runActionNow(new Runnable() {
									public void run() {
			                            handleSessionPassthru(sb);
									}
								});
							} catch (InterruptedException ix) {
                                try {
	                                sess.requestSessionDisconnect();
                                } catch (SessionException sx) {}
							}
                        } else { /* Else, tokenize the command */
                            final ArrayList<String> tokens = splitLine(sb);
							try {
                                synchronized(wobj) {
    								RangerProcessor.addImmediateAction(new Runnable() {
        								public void run() {
    										handleCommand(tokens, sess);
                                        }
    								});
                                    wobj.wait();
                                }
							} catch (InterruptedException ix) {
                                try {
	                                sess.requestSessionDisconnect();
                                } catch (SessionException sx) {}
							}
                        }
                        sb.setLength(0); /* Clear it */
                    }
                    switch (state) {
                        case STATE_AUTHENTICATED:
                            sendString(PROMPT); /* And send prompt */
                            break;
                        case STATE_LOGIN:
                            sendString("\n" + LOGINPROMPT);
                            break;
                        case STATE_PASSWORD:
                            sendString(PWDPROMPT);
                            break;
                    }
                } else if (c == '\n') { /* Ignore newline */
                } else if (c == '\u0008') { /* backspace */
                    if (sb.length() > 0) {
                        sb.setLength(sb.length() - 1);
                    }
                } else {
                    sb.append(c);
                }
            }
        }
        /**
         * Callback method delivering command response strings
         *
         * @param sess -
         *            Reader session completing command
         * @param rsp -
         *            list of response strings for command
         * @param completed -
         *            true if response completed, false if timeout or failure to
         *            get response
         */
        public void commandResponseCallback(MantisIIReaderSession sess,
            List<String> rsp, boolean completed) {
            for (String s : rsp) {
                sendString("@" + sess.getReaderID() + ": " + s + "\n");
            }
            if (completed)
                sendString("@" + sess.getReaderID() + ": Completed\n");
            else
                sendString("@" + sess.getReaderID() + ": Error\r\n");
        }
        /**
         * Get byte for given string
         */
        private static byte[] getBytesForString(String s) {
            StringBuilder sb = new StringBuilder();
            int len = s.length();
            boolean last_cr = false;
            for (int i = 0; i < len; i++) {
                char c = s.charAt(i);
                if ((c == '\n') && (!last_cr)) { /* \n not after \r */
                    sb.append('\r');
                }
                last_cr = (c == '\r');
                sb.append(c);
            }
            return sb.toString().getBytes();
        }
        /**
         * Send output string. Lines end with "\n" in the text (will be
         * translated by context if needed)
         *
         * @param s -
         *            message to send
         * @throws CommandException
         *             if error sending output (fatal to command)
         */
        public void sendString(String s) {
            if (sess == null)
                return;
            byte[] b = getBytesForString(s);
            try {
                sess.requestDataWrite(b, 0, b.length, null);
            } catch (SessionException sx) {
                error(sessid + ": " + sx);
            }
        }

        /**
         * Make string version of given object value for output
         *
         * @param o -
         *            value
         * @return String representation
         */
        public String makeString(Object o) {
            if (o == null)
                return RangerServer.NULL_VAL;
            if (o instanceof String) {
                return "\"" + o.toString() + "\"";
            } else if (o instanceof ReaderEntity) {
                return ((ReaderEntity) o).getID();
            } else if (o instanceof Set) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                for (Object oinst : (Set<?>) o) {
                    if (!first)
                        sb.append(',');
                    sb.append(makeString(oinst));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
            } else if (o instanceof List) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                for (Object oinst : (List<?>) o) {
                    if (!first)
                        sb.append(',');
                    sb.append(makeString(oinst));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
            } else if (o instanceof Map) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                boolean first = true;
                Map<?,?> omap = (Map<?,?>) o;
                for (Object e : omap.entrySet()) {
                    Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                    if (!first)
                        sb.append(',');
                    sb.append(ent.getKey().toString());
                    sb.append("=");
                    sb.append(makeString(ent.getValue()));
                    first = false;
                }
                sb.append("}");
                return sb.toString();
			} else if (o instanceof Number) {
				String s = RangerServer.formatNumber(o);
                if(s == null) s = RangerServer.NULL_VAL;
                return s;
            } else
                return o.toString();
        }
        /**
         * Report command error - do not call sendString() after this. Do not
         * call once sendString() has been used.
         *
         * @param msg -
         *            error message
         */
        public void reportError(String msg) {
            sendString(ERRORPREFIX + msg + "\n");
        }
        /**
         * Test for admin, and return error if not
         *
         * @return true if admin, false if not
         */
        public boolean isAdmin() {
            return is_admin;
        }
        /**
         * Test if config view privilege
         *
         * @return true if view allowed, false if not
         */
        public boolean isConfigView() {
            return is_config_view;
        }
        /**
         * Get our SessionFactory
         *
         * @return factory
         */
        public SessionFactory getSessionFactory() {
            return sf;
        }
        /**
         * Get logger
         *
         * @return logger
         */
        public Logger getLogger() {
            return sf.getLogger();
        }
        /**
         * Get my user
         *
         * @return user, or null if none
         */
        public User getUser() {
            return user;
        }
        /**
         * Request session termination
         */
        public void requestSessionTermination() {
            try {
                if (sess != null)
                    sess.requestSessionDisconnect();
            } catch (SessionException sx) {
            }
        }
        /**
         * Enable/disable broadcasts on session
         *
         * @param enab -
         *            true=allow broadcasts
         */
        public void enableBroadcasts(boolean enab) {
            hide_broadcasts = !enab;
        }
        /**
         * Set content-type for response - default is text/plain
         *
         * @param ct -
         *            MIME content type
         */
        public void setContentType(String ct) {
        }
        /**
         * Use web syntax?
         *
         * @return true if web-style, false if not
         */
        public boolean useWebSyntax() {
            return false;
        }
        /**
         * Command type extension ( the "ext" in cmd.ext, if provided ). Used to
         * signal formatting options, such as XML encoding.
         *
         * @return ext value, or "" if not provided
         */
        public OutputSyntax getFormatExt() {
            return ext;
        }
        /**
         * Start object using requested encoding
         *
         * @param sb -
         *            string builder to append to
         * @param type -
         *            object type
         * @param id -
         *            object id
         * @param is_first -
         *            true if first, false if not
         */
        public StringBuilder appendStartObject(StringBuilder sb, String type,
            String id, boolean is_first) {
            return RangerServer.appendStartObject(sb, type, id, is_first, this);
        }
        /**
         * Add encoded attribute value pair using requested encoding (text,
         * json, xml)
         *
         * @param sb -
         *            string builder to append to
         * @param id -
         *            attribute ID
         * @param val -
         *            attribute value
         * @param is_first -
         *            true if first, false otherwise
         */
        public StringBuilder appendAttrib(StringBuilder sb, String id,
            Object val, boolean is_first) {
            return RangerServer.appendAttrib(sb, id, val, is_first, this);
        }
        /**
         * End object using requested encoding
         *
         * @param sb -
         *            string builder to append to
         * @param type -
         *            object type
         * @param id -
         *            object ID, or null if none
         */
        public StringBuilder appendEndObject(StringBuilder sb, String type,
            String id) {
            return RangerServer.appendEndObject(sb, type, id, this);
        }
    }

    private static class OurTCPIPServerHandler implements TCPIPServerHandler {
        private boolean cmdsess;

        public OurTCPIPServerHandler(boolean commandsessions) {
            cmdsess = commandsessions;
        }
        /**
         * Session accepted - used to notify when a client connection has been
         * established with the server
         *
         * @param server -
         *            our server
         * @param session -
         *            session with client
         */
        public void sessionAcceptCompleted(final TCPIPServerSocket server,
            TCPIPSession session) {
            new InboundSessionHandler(session, session.getIPAddress().toString(), cmdsess);
        }
        /**
         * Server socket bind completed
         *
         * @param server -
         *            our server
         * @param is_bound -
         *            true if successful, false if failed
         */
        public void serverBindCompleted(TCPIPServerSocket server,
            boolean is_bound) {
            // log("Bind completed: " + is_bound);
            if (is_bound == false) {
                error("Error binding to port " + server.getIPAddress().getPort());
            }
        }
        /**
         * Server socket unbind completed
         *
         * @param server -
         *            our server
         */
        public void serverUnbindCompleted(TCPIPServerSocket server) {
        }
    }

    /* Handler for up connection sessions */
    private static class OurUpConnectionSessionHandler implements SessionHandler {
        private Session sess;
        private StringBuilder sb = new StringBuilder();
        private int sesskey;
        private boolean got_req;
        private MantisIIReader rdr;
        private String ourmac;
        private String ourrdrid;
        /**
         * Constructor for up connection sessions
         *
         * @param s -
         *            communications session
         * @param sessionid -
         *            session ID string
         */
        public OurUpConnectionSessionHandler(Session s) {
            sess = s;
            sess.setSessionHandler(this);
        }
//        public void cleanup() {
//            if (sess != null) {
//                sess.setSessionHandler(null);
//                sess.cleanup();
//                sess = null;
//            }
//        }
        /**
         * Session about to attempt connect - used to notify when the session is
         * about to attempt a connection.
         *
         * @param s -
         *            session
         */
        public void sessionStartingConnect(Session s) {
        }
        /**
         * Session connected - used to notify when the session has completed an
         * attempt to connect to its target. If is_connected = true, the session has
         * connected successfully. If connected successfully, requests to write data
         * to the session can be enqueued and sessionDataRead() callbacks can begin
         * after this callback completes.
         *
         * @param s -
         *            session
         * @param is_connected -
         *            if true, session was connected successfully. if false, connect
         *            failed.
         */
        public void sessionConnectCompleted(Session s, boolean is_connected) {
            sess = s;
        }
        /**
         * Session disconnected - used to notify when the session has become
         * disconnected from its target. Starting with this call, requests to write
         * data to the session can no longer be enqueued. sessionDataRead() and
         * sessionDataWriteComplete() callbacks will not happen once the session is
         * disconnected.
         *
         * @param s -
         *            session
         */
        public void sessionDisconnectCompleted(Session s) {
        }
        /**
         * Handle request message
         */
        private void handleRequest(String line) {
            if(line.startsWith("$RFCODEREADER,")) {
                String[] tok = line.split(",");
                rdr = null;
                if(tok.length > 2) {
                    ourmac = tok[1];
                    ourrdrid = tok[2];
                    if(ourrdrid.length() == 0)
                        ourrdrid = ourmac;
                    rdr = MantisIIReader.findReaderByUpconnectionID(ourrdrid);
                }
                if((rdr != null) &&     /* Found match */
                    rdr.getReaderEnabled() && /* And its active reader */
                    rdr.getUpConnectionEnabled()) {
                    String mac = rdr.getLastUpConnectionMAC();  /* Check mac of current session, if any */
                    if((mac == null) || (ourmac.equals(mac))) { /* If none, or matches new connection */
                        got_req = true;
                        sesskey = rnd.nextInt() & 0x7FFFFFFF;    /* Get random session key (positive) */
                        sendString("$OK," + Integer.toHexString(sesskey) + "\r");    /* Send decimal representation of random */
                    }
                    else {  /* Else, rdr ID matches another reader - report error */
                        sendString("$DUPRDRID\r");
                        try {
                            sess.requestSessionDisconnect();
                        } catch (SessionException sx) {
                        }
                        reportUpConnectionError("dup-rdrid", ourrdrid, ourmac,
                            mac);
                    }
                }
                else {
                    sendString("$UNKRDRID\r");
                    try {
                        sess.requestSessionDisconnect();
                    } catch (SessionException sx) {
                    }
                    reportUpConnectionError("undef-rdrid", tok[2], tok[1], null);
                }
            }
            else {  /* Else, bad message */
                sendString("$ERROR\r");
                try {
                    sess.requestSessionDisconnect();
                } catch (SessionException sx) {
                }
            }
        }
        /**
         * Handle auth message
         */
        private void handleAuth(String line) {
            if(line.startsWith("$RFCODEAUTH,")) {
                String[] tok = line.split(",");
                if(rdr.checkUpConnectionAuth(sesskey, tok[1])) {    /* See if auth good */
                    if(rdr.getReaderEnabled() && /* And its active reader */
                        rdr.getUpConnectionEnabled()) {
                        String mac = rdr.getLastUpConnectionMAC();  /* Check mac of current session, if any */
                        if((mac == null) || (ourmac.equals(mac))) { /* If none, or matches new connection */
                            rdr.upConnectionEstablished(sess, ourmac);
                        }
                        else {  /* Else, rdr ID matches another reader - report error */
                            sendString("$DUPRDRID\r");
                            try {
                                sess.requestSessionDisconnect();
                            } catch (SessionException sx) {
                            }
                            reportUpConnectionError("dup-rdrid", ourrdrid, ourmac,
                                mac);
                        }
                    }
                    else {
                        sendString("$UNKRDRID\r");
                        try {
                            sess.requestSessionDisconnect();
                        } catch (SessionException sx) {
                        }
                        reportUpConnectionError("undef-rdrid", ourrdrid, ourmac, null);
                    }
                }
                else {  /* Else, bad login */
                    sendString("$UNKRDRID\r");
                    try {
                        sess.requestSessionDisconnect();
                    } catch (SessionException sx) {
                    }
                    reportUpConnectionError("badauth-rdrid", ourrdrid, ourmac, null);
                }
            }
            else {  /* Else, bad message */
                sendString("$ERROR\r");
                try {
                    sess.requestSessionDisconnect();
                } catch (SessionException sx) {
                }
            }
        }
        /**
         * Data read from session - used to deliver raw data reads from session.
         *
         * @param sdr -
         *            read request result - only valid for duration of
         *            sessionDataRead callback
         */
        public void sessionDataRead(final SessionRead sdr) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
					doSessionDataRead(sdr.getSession(), sdr.readBytes());
				}
			});
		}
		private void doSessionDataRead(Session sess, byte[] b) {
            for (int i = 0; i < b.length; i++) { /* Loop through them */
                char c = (char) b[i]; /* We're ASCII, so we can do this */
                if (c == '\r') { /* End of line? */
                    String line = sb.toString();
                    sb.setLength(0);
                    if(!got_req) {  /* If no request yet, handle it */
                        handleRequest(line);
                    }
                    else {
                        handleAuth(line);
                    }
                    return;
                } else if (c == '\n') { /* Ignore newline */
                } else if (c == '\u0008') { /* backspace */
                    if (sb.length() > 0) {
                        sb.setLength(sb.length() - 1);
                    }
                } else {
                    sb.append(c);
                }
            }
        }
        /**
         * Send string to the reader's session
         *
         * @param st -
         *            string to send
         */
        private void sendString(String st) {
            byte[] b = st.getBytes();
            try {
                sess.requestDataWrite(b, 0, b.length, null);
            } catch (SessionException sx) {
                error("up-connect: " + sx);
            }
        }
        /**
         * Report upconnect error
         */
        public void reportUpConnectionError(String errid, String rdrid,
            String mac, String curmac) {
            ServletCommandInterface.reportUpConnectionError(errid,
                rdrid, mac, curmac);
            StringBuilder sb = new StringBuilder();
            sb.append("UpConnectionError: errorid=").append(errid).append(", readerid=\"").
                append(rdrid).append("\", mac=").append(mac);
            if(curmac != null)
                sb.append(", currentmac=" + curmac);
            sb.append("\n");
            InboundSessionHandler.broadcastString(sb.toString());
        }
    }

    private static class OurUpConnectionHandler implements TCPIPServerHandler {
        public OurUpConnectionHandler() {
        }
        /**
         * Session accepted - used to notify when a client connection has been
         * established with the server
         *
         * @param server -
         *            our server
         * @param session -
         *            session with client
         */
        public void sessionAcceptCompleted(TCPIPServerSocket server,
            final TCPIPSession session) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            new OurUpConnectionSessionHandler(session);
				}
			});
        }
        /**
         * Server socket bind completed
         *
         * @param server -
         *            our server
         * @param is_bound -
         *            true if successful, false if failed
         */
        public void serverBindCompleted(TCPIPServerSocket server,
            boolean is_bound) {
            if (is_bound == false) {
                error("Error binding to port " + server.getIPAddress().getPort());
            }
        }
        /**
         * Server socket unbind completed
         *
         * @param server -
         *            our server
         */
        public void serverUnbindCompleted(TCPIPServerSocket server) {
        }
    }

    /**
     * Load configuration file, and initialize
     */
    private static void loadConfig() {
        File config;
        /* Try to find our config file */
        String s = System.getProperty("zonemgr.config");
        if (s == null) { /* If not defined, set defaul */
            config = new File(System.getProperty("user.dir"), "zonemgr.config");
        } else { /* Else, use the provided filename */
            config = new File(s);
        }
        /* If it doesn't exist, try loading resource */
        if (config.exists() == false) {
            error("Error loading configuration from zonemgr.config resource");
            return;
        } else {
            /* Now, load configuration file into properties object */
            FileInputStream r = null;
            try {
                r = new FileInputStream(config);
                base_config.load(r); /* Do load */
            } catch (IOException iox) {
                error("Error loading configuration from " + config.getName());
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
        }
    }
    
    /**
     * Set system configuration directory and load configuration file
     */
    private static void loadSystemConfig() {
    	/* find/set system configuration directory */
    	String conf = System.getProperty("rfcode.conf");
    
    	if (conf == null) {
    		/* in development mode */
    		conf = System.getProperty("user.home") + File.separator + ".rfcode";
    	}
    	
    	/* set file directory for system.properties*/
    	system_config_dir = new File(conf);
    	File config = new File(system_config_dir, SYSTEM_FILENAME);
    	
    	if (config.exists() == false) {
    		error("Error loading configuration from" + system_config_dir);
    		return;
    	} else {
    		FileInputStream r = null;
    		try {
    			r = new FileInputStream(config);
    			system_config.load(r); /* Do load */
    		} catch (IOException iox) {
    			error("Error loading configuration from" + config.getName());
    		} finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                        r = null;
                    }
                }
    		}
    	}
    }

    /**
     * Load customized configuration file
     */
    public static void loadCustomConfig() {
        File config;
        /* If data directory defined, load custom too */
        if(data_dir != null) {
            /* Now load the dynamic server config */
            config = new File(data_dir, SERVER_FILENAME);
            if (config.exists()) {
                /* Now, load configuration file into properties object */
                FileInputStream r = null;
                try {
                    r = new FileInputStream(config);
                    cust_config.load(r); /* Do load */
                } catch (IOException iox) {
                    error("Error loading configuration from " + config.getName());
                } finally {
                    if (r != null) {
                        try {
                            r.close();
                        } catch (IOException iox) {
                        }
                        r = null;
                    }
                }
            }
        }
        /* Build combined config */
        our_config.putAll(cust_config);

        checkLogging();
    }
    /**
     * Save server configuration
     */
    public static void saveConfig() {
        /* Now write it to the file */
        File f = new File(data_dir, SERVER_FILENAME);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            cust_config.store(fw, "Server Configuration");
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
        for(ConfigUpdateListener cul : cfgupd) {
            cul.configUpdateNotification();
        }
        checkLogging();
    }
    /**
     * Set server configuration setting
     */
    public static void setConfigVariable(String id, String value) {
        String bval = base_config.getProperty(id);
        if((bval != null) && (bval.equals(value))) { /* Default? */
            cust_config.remove(id);
        }
        else {
            cust_config.setProperty(id, value);
        }
        our_config.setProperty(id, value);

        if (LISTEN_ASSET_TAGS_ONLY_PROPERTY.equals(id)) {
            setIsListenAssetTagsOnly(Optional
                    .ofNullable(value)
                    .map(Boolean::valueOf)
                    .orElse(Boolean.FALSE));
        }
    }
    /**
     * Load our reader factories
     */
    private static void loadReaderFactories() {
        String v;

        /* Now, loop through properties for reader factories */
        for (Enumeration<?> keys = our_config.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();
            /* Only do readerfactory.* attributes this pass */
            if (key.startsWith("readerfactory.") == false) {
                continue;
            }
            v = our_config.getProperty(key);
            try {
                Class<?> c = Class.forName(v); /* Load the class */
                Object factory = c.newInstance();
                if (factory instanceof ReaderFactory) {
                    ReaderFactory rf = (ReaderFactory) factory;
                    /* Set session factory for our reader factory */
                    if (rf instanceof SessionUsingReaderFactory) {
                        ((SessionUsingReaderFactory) rf).setSessionFactory(sf);
                    }
                    rf.init(); /* Initialize it */
                    /* Add to our factory set */
                    info("Loaded reader factory " + rf.getID());
                }
            } catch (ClassNotFoundException cnfx) {
                error("Class for factory not found - " + v);
            } catch (IllegalAccessException iax) {
                error("Access error for factory - " + v);
            } catch (InstantiationException ix) {
                error("Error creating factory - " + v);
            } catch (DuplicateEntityIDException deix) {
                error("Error creating factory - duplicate ID - "
                    + v);
            } catch (BadParameterException bpx) {
                error("Error: " + bpx.getMessage() + " - " + v);
            }
        }
    }
    /**
     * Load tag types
     */
    private static void loadTagTypes() {
        String v;

        /* Now, loop through properties for tag types */
        for (Enumeration<?> keys = our_config.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();

            /* Only do tagtype.* attributes this pass */
            if (key.startsWith("tagtype.") == false) {
                continue;
            }
            v = our_config.getProperty(key);
            try {
                Class<?> c = Class.forName(v); /* Load the class */
                Object tagt = c.newInstance();
                if (tagt instanceof TagType) {
                    TagType tt = (TagType) tagt;
                    tt.init(); /* Initialize it */
                    info("Loaded tag type " + tt.getID());
                }
            } catch (ClassNotFoundException cnfx) {
                error("Class for tag type not found - " + v);
            } catch (IllegalAccessException iax) {
                error("Access error for tag type - " + v);
            } catch (InstantiationException ix) {
                error("Error creating tag type - " + v);
            } catch (DuplicateEntityIDException dttx) {
                error("Duplicate tag type error - " + v);
            } catch (BadParameterException bpx) {
                error("Error: " + bpx.getMessage() + " - " + v);
            }
        }
    }
    /**
     * Load location rule types
     */
    private static void loadLocationRuleTypes() {
        String v;

        /* Now, loop through properties for tag types */
        for (Enumeration<?> keys = our_config.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();

            /* Only do locrule.* attributes this pass */
            if (key.startsWith("locrule.") == false) {
                continue;
            }
            v = our_config.getProperty(key);
            try {
                Class<?> c = Class.forName(v); /* Load the class */
                Object lrt = c.newInstance();
                if (lrt instanceof LocationRuleFactory) {
                    LocationRuleFactory lrf = (LocationRuleFactory) lrt;
                    lrf.setLocationRuleEngine(LocationRuleEngine
                        .getLocationRuleEngine());
                    lrf.init(); /* Initialize it */
                    info("Loaded rule type " + lrf.getID());
                }
            } catch (ClassNotFoundException cnfx) {
                error("Class for location rule type not found - "
                    + v);
            } catch (IllegalAccessException iax) {
                error("Access error for location rule type - " + v);
            } catch (InstantiationException ix) {
                error("Error creating location rule type - " + v);
            } catch (DuplicateEntityIDException deix) {
                error("Error duplicate ID for location rule type - " + v);
            } catch (BadParameterException bpx) {
                error("Error: " + bpx.getMessage() + " - " + v);
            }
        }
    }
    /**
     * Load command handlers
     */
    private static void loadCommandHandlers() {
        String v;

        /* Now, loop through properties for tag types */
        for (Enumeration<?> keys = our_config.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();

            /* Only do command.* attributes this pass */
            if (key.startsWith("command.") == false) {
                continue;
            }
            v = our_config.getProperty(key);
            try {
                Class<?> c = Class.forName(v); /* Load the class */
                Object cmdh = c.newInstance();
                if (cmdh instanceof CommandHandler) {
                    CommandHandler cmd = (CommandHandler) cmdh;
                    String[] cmdids = cmd.getCommandIDs();
                    for (String cmdid : cmdids) {
                        if (cmds.put(cmdid, cmd) != null) {
                            error("Duplicate handler for " + cmdid
                                + " = " + v);
                        }
                        info("Loaded command handler " + cmdid);
                    }
                }
            } catch (ClassNotFoundException cnfx) {
                error("Class for command handler not found - " + v);
            } catch (IllegalAccessException iax) {
                error("Access error for command handler - " + v);
            } catch (InstantiationException ix) {
                error("Error creating command handler - " + v);
            }
        }
    }
    /**
     * Load add ons handlers
     */
    private static void loadExtensionHandlers() {
        String v;

        /* Now, loop through properties for tag types */
        for (Enumeration<?> keys = our_config.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();

            /* Only do extension.* attributes this pass */
            if (key.startsWith("extension.") == false) {
                continue;
            }
            v = our_config.getProperty(key);
            try {
                info("Loading " + v);
                Class<?> c = Class.forName(v); /* Load the class */
                info("Loaded " + v);
                Object ext = c.newInstance();
                info("Instanciated " + v);
                if (ext instanceof RangerExtension) {
                    RangerExtension rx = (RangerExtension) ext;
                    rx.extensionInit();
                    info("Inited " + v);
                }
            } catch (ClassNotFoundException cnfx) {
                error("Class for extension not found - " + v);
            } catch (IllegalAccessException iax) {
                error("Access error for extension - " + v);
            } catch (InstantiationException ix) {
                error("Error creating extension - " + v);
            } catch (NoClassDefFoundError ncdfe) {
                error("Error loading extension - " + v + " - missing required class or library");
            }
        }
    }
    private static class LicenseCheck implements Runnable {
        public void run() {
            ReaderEntityDirectory.updateKeyCount();
            /* Enqueue for another check in 24 hours */
            RangerProcessor.addDelayedAction(this, 1000L*3600L*LICENSE_TIMEOUT);
        }
    }
    private static class TagTimeoutCheck implements Runnable {
        public void run() {
            AbstractTagType.processTagTypeAgeOut();
            /* Enqueue for another check in 1 second */
            RangerProcessor.addDelayedAction(this, 1000L);
        }
    }
    private static class ReaderDNSCheck implements Runnable {
		List<Reader> rdrs;
        public void run() {
			rdrs = new ArrayList<Reader>(ReaderEntityDirectory.getReaders());
			Thread t = new Thread(new Runner(), "ReaderDNSCheck");
			t.setDaemon(true);
			t.start();
        }
		private class Runner implements Runnable {
			public void run() {
				List<Reader> r = rdrs;	
				for(Reader rdr : r) {
					rdr.freshenDNS();
				}
    	        /* Enqueue for another check in 60 seconds (after completed) */
    	        RangerProcessor.addDelayedAction(ReaderDNSCheck.this, 60*1000L);
			}
		}
    }

    /**
     * Initialize the data directory
     */
    private static void initDataDirectory() {
        /* Get from property first */
        String datadir = System.getProperty(RANGER_DATADIR_PROPERTY);
        if (datadir == null) {
            /* Get our data directory from config */
            datadir = our_config.getProperty("datadir");
        }
        if (datadir == null) { /* Fallback to under current directory */
            datadir = "zonemgr.datadir";
        }
        data_dir = new File(datadir); /* Make file object for directory */
        /* If exists, but isn't a directory, error */
        if (data_dir.exists() && (data_dir.isDirectory() == false)) {
            error("Data directory invalid - " + datadir);
        } else if (data_dir.exists() == false) { /* Does not exist yet? */
            data_dir.mkdirs(); /* Make it, if needed */
        }
        loadCustomConfig();       /* Reload config, since we can find custom now */
    }
    /**
     * Filename filter for files with given suffix
     */
    private static class SuffixFilenameFilter implements FilenameFilter {
        String suffix;
        SuffixFilenameFilter(String s) {
            suffix = s;
        }
        public boolean accept(File dir, String name) {
            return name.endsWith(suffix);
        }
    }
    /**
     * Load saved tag groups
     */
    private static void loadTagGroups() {
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            TAGGROUP_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get the tag type */
            String tt = p.getProperty(TAGGROUP_TAGTYPE_PROPERTY);
            /* Get group ID */
            String tgid = p.getProperty(TAGGROUP_TAGGROUPID_PROPERTY);
            /* Get label */
            String tglabel = p.getProperty(TAGGROUP_LABEL_PROPERTY);
            /* Get attribute strings */
            List<String> args = getEncodedAttributes(p);
            /* And create the tag group */
            String err = createTagGroup(tgid, tt, args.toArray(new String[args
                .size()]), tglabel);
            if (err != null) {
                error(err);
            } else {
                info("Loaded tag group " + tgid);
            }
        }
    }
    /**
     * Load saved location rules
     */
    private static void loadLocationRules() {
        ArrayList<LocationRule> to_activate = new ArrayList<LocationRule>();
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            LOCATIONRULE_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get the rule type */
            String lrt = p.getProperty(LOCATIONRULE_RULETYPE_PROPERTY);
            /* Get rule ID */
            String lrid = p.getProperty(LOCATIONRULE_RULEID_PROPERTY);
            /* Get zone ID */
            String zone = p.getProperty(LOCATIONRULE_ZONEID_PROPERTY);
            /* Get attribute strings */
            List<String> args = getEncodedAttributes(p);
            /* And create the location rule */
            String err = createLocationRule(lrid, lrt, zone, args
                .toArray(new String[args.size()]));
            if (err != null) {
                error(err);
            } else {
                String enabled = p.getProperty(LOCATIONRULE_ENABLED_PROPERTY,
                    "false");
                if (enabled.equalsIgnoreCase(ENABLED_TRUE)) {
                    to_activate.add(ReaderEntityDirectory
                        .findLocationRule(lrid));
                }
                info("Loaded location rule " + lrid);
            }
        }
        /* Now, activate anything we need to activate */
        for (LocationRule rl : to_activate) {
            rl.setRuleEnabled(true);
            info("Enabled location rule " + rl.getID());
        }
    }
    /**
     * Load saved locations
     */
    private static void loadLocations() {
        HashMap<String, String> parentlist = new HashMap<String, String>();
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            LOCATION_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get zone ID */
            String zone = p.getProperty(LOCATION_ZONEID_PROPERTY);
            /* Get parent ID */
            String pid = p.getProperty(LOCATION_PARENTID_PROPERTY);
            if ((pid != null) && (pid.length() > 0)) {
                parentlist.put(zone, pid); /* Save for end */
            }
            /* Get attribute strings */
            List<String> args = getEncodedAttributes(p);
            /* And create the location rule */
            String err = createLocation(zone, null, args
                .toArray(new String[args.size()]));
            if (err != null) {
                error(err);
            } else {
                info("Loaded location " + zone);
            }
        }
        /* Now, link up any parents */
        for (Map.Entry<String, String> ent : parentlist.entrySet()) {
            Location loc = ReaderEntityDirectory.findLocation(ent.getKey());
            Location par = ReaderEntityDirectory.findLocation(ent.getValue());
            if (par != null) {
                try {
                    loc.setParent(par);
                } catch (BadParameterException bpx) {
                    error("Error: " + bpx.getMessage());
                }
            }
        }
    }
    /**
     * Load licenses
     */
    private static void loadLicenses() {
        File hc = new File(data_dir, LICENSE_KEY_FILENAME);
        Properties p = new Properties();
        FileInputStream r = null;
        try {
            r = new FileInputStream(hc);
            p.load(r);
        } catch (IOException iox) {
            error("Error loading "
                + hc.getAbsolutePath());
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (IOException iox) {
                }
                r = null;
            }
        }
        /* Iterate over entries - process all key.N attributes */
        for(Map.Entry<Object,Object> ke : p.entrySet()) {
            String key = (String)ke.getKey();
            if(key.startsWith(KEY_PREFIX)) {
                ReaderEntityDirectory.addLicenseKey((String)ke.getValue());
            }
        }
    }
    /**
     * Update license keys
     */
    public static void updateLicenses() {
        File hc = new File(data_dir, LICENSE_KEY_FILENAME);
        Properties p = new Properties();
        /* Validate license keys */
        ReaderEntityDirectory.updateKeyCount();
        /* Loop through our license keys */
        int i = 0;
        for(LicenseKey k : ReaderEntityDirectory.getLicenseKeys()) {
            /* If valid, non-expired, and non-transient, add it */
            if(k.isKeyValid() && (!k.isKeyExpired()) &&
                (!k.isTransientKey())) {
                p.setProperty(KEY_PREFIX + i, k.getKeyString());
                i++;
            }
        }
        /* Now write it to the file */
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(hc);
            p.store(fw, "");
        } catch (IOException iox) {
            error("Error updating config hashcode");
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
    }
    /**
     * Load saved users
     */
    private static void loadUsers() {
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            USER_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get pwdhash */
            String pwdhash = p.getProperty(USER_PWDHASH_PROPERTY);
            /* Get role */
            String role = p.getProperty(USER_ROLE_PROPERTY);
            /* Get userid */
            String userid = p.getProperty(USER_USERID_PROPERTY);
            /* And create the location rule */
            String err = createUser(userid, null, pwdhash, role);
            if (err != null) {
                error(err);
            } else {
                info("Loaded user " + userid);
            }
        }
    }
    /**
     * Load configuration hashcode
     */
    private static void loadConfigHash() {
        File hc = new File(data_dir, CONFIG_HASH_FILENAME);
        Properties p = new Properties();
        FileInputStream r = null;
        try {
            r = new FileInputStream(hc);
            p.load(r);
        } catch (IOException iox) {
            error("Error loading "
                + hc.getAbsolutePath());
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (IOException iox) {
                }
                r = null;
            }
        }
        config_hash = p.getProperty(CONFIG_HASH_PROPERTY);
        if(config_hash == null) {
            updateConfigHash();
        }
    }
    /**
     * Update config hashcode
     */
    static void updateConfigHash() {
        File hc = new File(data_dir, CONFIG_HASH_FILENAME);
        Properties p = new Properties();
        long newhash = rnd.nextLong();
        config_hash = Long.toHexString(newhash);
        p.setProperty(CONFIG_HASH_PROPERTY, config_hash);
        /* Now write it to the file */
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(hc);
            p.store(fw, "");
        } catch (IOException iox) {
            error("Error updating config hashcode");
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }

    }
    
    /**
     * Load sensor definitions
     */
    private static void loadSensorDefinitions() {
        ArrayList<LocationRule> to_activate = new ArrayList<LocationRule>();
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            SENSORDEF_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get def ID */
            String defid = p.getProperty(SENSORDEF_DEFID_PROPERTY);
            /* Get the sensor type */
            String deftype = p.getProperty(SENSORDEF_SENSORTYPE_PROPERTY);
            /* Get label */
            String tglabel = p.getProperty(SENSORDEF_LABEL_PROPERTY);
            /* Get dest */
            String dest = p.getProperty(SENSORDEF_DEST_PROPERTY);
            /* Get attribute strings */
            List<String> args = getEncodedAttributes(p);
            /* And create the sensor definition */
            String err = createSensorDef(defid, deftype, args
                .toArray(new String[args.size()]), tglabel, dest);
            if (err != null) {
                error(err);
            } else {
                info("Loaded sensor definition " + defid);
            }
        }
        /* Now, activate anything we need to activate */
        for (LocationRule rl : to_activate) {
            rl.setRuleEnabled(true);
            info("Enabled location rule " + rl.getID());
        }
    }

    /**
     * Get encoded attribute values : assumes properties are named "attrib."
     * attributeid
     */
    private static List<String> getEncodedAttributes(Properties p) {
        ArrayList<String> rslt = new ArrayList<String>();
        for (Enumeration<?> keys = p.propertyNames(); keys.hasMoreElements();) {
            String key = (String)keys.nextElement();

            if (key.startsWith(ATTRIB_PROPERTY_PREFIX)) {
                rslt.add(key.substring(ATTRIB_PROPERTY_PREFIX.length()) + "="
                    + p.getProperty(key));
            }
        }
        return rslt;
    }
    /**
     * Get encoded attribute values : assumes properties are named "attrib."
     * attributeid
     */
    private static void addEncodedAttributes(Properties p,
        Map<String, Object> vals) {
        for (Map.Entry<String, Object> ent : vals.entrySet()) {
            Object v = ent.getValue();
            if (v instanceof Set) {
                StringBuilder sb = new StringBuilder();
                for (Object r : ((Set<?>) v)) {
                    if (r instanceof ReaderEntity) {
                        if (sb.length() > 0)
                            sb.append(',');
                        sb.append(((ReaderEntity) r).getID());
                    }
                }
                p.setProperty(ATTRIB_PROPERTY_PREFIX + ent.getKey(), sb
                    .toString());
            }
            else if (v instanceof List) {
                StringBuilder sb = new StringBuilder();
                for (Object r : ((List<?>) v)) {
                    if (sb.length() > 0)
                        sb.append(',');
                    sb.append(r.toString());
                }
                p.setProperty(ATTRIB_PROPERTY_PREFIX + ent.getKey(), sb
                    .toString());
            }
            else {
                p.setProperty(ATTRIB_PROPERTY_PREFIX + ent.getKey(), v
                    .toString());
            }
        }
    }

    /**
     * Create tag group
     *
     * @return Error string, if failed
     */
    public static String createTagGroup(String id, String tt, String[] args, String lbl) {
        TagGroup tg;
		int i;
        /* First, be sure it doesn't already exist */
		try {
			ReaderEntityDirectory.testID(id);
		} catch (DuplicateEntityIDException deidx) {
            return "Duplicate ID error";
        }
        /* Next, find the tag type */
        TagType ttype = ReaderEntityDirectory.findTagType(tt);
        if (ttype == null) { /* Not valid? */
            return "Invalid tag type";
        }
		if(ttype.isSubType()) {	/* Can't create group for subtype */
			return "Cannot create group from subtype";
		}
        /* Now, get default attributes needed for tag group of type */
        Map<String, Object> gaids = ttype.getTagGroupDefaultAttributes();
        gaids.put(TagGroup.ATTRIB_LOC_UPD_DELAY, Integer.valueOf(0));
        gaids.put(TagGroup.ATTRIB_BLK_ATTR_UPD_LIST, new ArrayList<String>());
        gaids.put(TagGroup.ATTRIB_GROUP_TIMEOUT, Integer.valueOf(TagGroup.DEFAULT_GROUP_TIMEOUT));
		if(ttype.isEnhPayloadVerifySupported()) {
			gaids.put(TagGroup.ATTRIB_ENH_PAYLOAD_VERIFY, TagGroup.ENH_PAYLOAD_VERIFY_IFSUPPORTED);
        }
        gaids.put(TagGroup.ATTRIB_LOC_MATCH_BOOST, Double.valueOf(TagGroup.DEFAULT_LOC_MATCH_BOOST));
        gaids.put(TagGroup.ATTRIB_LOC_MATCH_TIME, Integer.valueOf(TagGroup.DEFAULT_LOC_MATCH_TIME));
        /* Loop through other arguments for values */
        for (i = 0; i < args.length; i++) {
            int eq = args[i].indexOf('='); /* Find equals */
            if (eq < 0) {
                return "Attribute id=value format error";
            }
            String aid = args[i].substring(0, eq); /* Get id */
            String aval = args[i].substring(eq + 1); /* Get value */
            /* Find default */
            Object oldval = gaids.get(aid);
            if (oldval == null) {
                return "Bad attribute id - " + aid;
            }
            if (oldval instanceof Integer) { /* Is integer? */
                try {
                    oldval = Integer.parseInt(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value - " + aval;
                }
			}
			else if(oldval instanceof List) {	/* List=list of strings */
                List<String> nval = new ArrayList<String>();
                String[] l = aval.split(",");
                for (String s : l) {
					if(s.length() > 0)
	                    nval.add(s);
                }
                oldval = nval;
            }
            else if (oldval instanceof Double) { /* Is double? */
                try {
                    oldval = Double.parseDouble(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value - " + aval;
                }
            }
            else if (oldval instanceof Boolean) { /* Is boolean? */
                oldval = aval.equals("true");
            } else {
                oldval = aval;
            }
            gaids.put(aid, oldval); /* Update value */
        }
        /* Now create tag group */
        tg = ttype.createTagGroup();
        tg.setID(id); /* Set ID */
        if(lbl != null) /* Set label */
            tg.setLabel(lbl);
        tg.setTagGroupAttributes(gaids); /* Set group attributes */
        try {
            tg.init(); /* And initialize it */
        } catch (BadParameterException bpx) {
            tg.cleanup();
            return bpx.getMessage();
        } catch (DuplicateEntityIDException dtgx) {
            tg.cleanup();
            return "Tag group exists";
        }
        return null;
    }
    /**
     * Create location rule
     *
     * @return Error string, if failed
     */
    public static String createLocationRule(String id, String rt, String zone,
        String[] args) {
        boolean do_act = false;
        boolean actval = false;
        /* First, be sure it doesn't already exist */
		try {
			ReaderEntityDirectory.testID(id);
		} catch (DuplicateEntityIDException deidx) {
            return "Duplicate ID error";
        }
        /* Next, find the rule type */
        LocationRuleFactory lrtype = ReaderEntityDirectory
            .findLocationRuleFactory(rt);
        if (lrtype == null) { /* Not valid? */
            return "Invalid rule type";
        }
        /* Now, get default attributes needed for tag group of type */
        Map<String, Object> gaids = lrtype.getDefaultLocationRuleAttributes();
        /* Loop through other arguments for values */
        for (int i = 0; i < args.length; i++) {
            int eq = args[i].indexOf('='); /* Find equals */
            if (eq < 0) {
                return "Attribute id=value format error";
            }
            String aid = args[i].substring(0, eq); /* Get id */
            String aval = args[i].substring(eq + 1); /* Get value */
            /* If "enabled", do activate/deactivate later */
            if(aid.equals("enabled")) {
                do_act = true;
                actval = aval.equalsIgnoreCase("true");
                continue;
            }
            /* Find default */
            Object oldval = gaids.get(aid);
            if (oldval == null) {
                /* Ignore bogu channellist, but complain about others */
                if(aid.equals(LocationRuleFactory.ATTRIB_CHANNELLIST))
                    continue;
                else
                    return "Bad attribute id - " + aid;
            }
            if (oldval instanceof Integer) { /* Is integer? */
                try {
                    oldval = Integer.parseInt(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value - " + aval;
                }
            } else if (oldval instanceof Double) {
                try {
                    oldval = Double.parseDouble(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value - " + aval;
                }
            } else if (oldval instanceof Boolean) {
                try {
                    oldval = Boolean.parseBoolean(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value - " + aval;
                }
            } else if (oldval instanceof String) {
                oldval = aval;
            } else if (oldval instanceof Set) { /* Set=set of ReaderEntity */
                Set<ReaderEntity> nval = new HashSet<ReaderEntity>();
                String[] l = aval.split(",");
                for (String s : l) {
                    ReaderEntity re = ReaderEntityDirectory.findEntity(s);
                    if (re != null)
                        nval.add(re);
                }
                oldval = nval;
			} else if (oldval instanceof ListListDouble) {	/* Special list for list of list of doubles */
				ListListDouble nval = new ListListDouble();
				List<Double> sval = null;
				String[] l = aval.split(",");	/* Split at commas */
				for(String s : l) {
					if(s.length() == 0)
						continue;
					int sindx = s.indexOf('[');	/* See if open? */
					if(sindx >= 0) {	/* Has open? */
						if(sval == null)
							sval = new ArrayList<Double>();
						else
		                    return "Bad attribute value - " + aval;
						s = s.substring(sindx+1);	/* Keep stuff after open */
					}
					sindx = s.indexOf(']');	/* See if close? */
					boolean endit = false;
					if(sindx >= 0) {	/* If so, close */
						s = s.substring(0, sindx);	/* Strip off after close */
						endit = true;
					}
                    if(sval == null)
                        return "Bad attribute value - " + aval;
					/* Add to list */
					try {
						sval.add(Double.valueOf(s));	/* Parse it */
					} catch(NumberFormatException nfx) {
	                    return "Bad attribute value - " + aval;
					}
					if(endit) {	/* If close of sublist, end it */
						nval.add(sval);
						sval = null;
					}
				}
                oldval = nval;
            } else if (oldval instanceof List) {    /* List=list of strings */
                List<String> nval = new ArrayList<String>();
                String[] l = aval.split(",");
                for (String s : l) {
                    nval.add(s);
                }
                oldval = nval;
            } else {
                error("Unknown type - "
                    + oldval.getClass().toString());
            }
            gaids.put(aid, oldval); /* Update value */
        }
        /* Now create rule */
        LocationRule lr = lrtype.createLocationRule();
        lr.setID(id); /* Set ID */
        try {
            lr.setLocationRuleAttributes(gaids); /* Set attributes */
        } catch (BadParameterException bpx) {
            lr.cleanup();
            return bpx.getMessage();
        }
        /* Find location */
        Location loc = ReaderEntityDirectory.findLocation(zone);
        if (loc == null) { /* If not found, lazy create one */
            loc = new Location();
            if (zone.equals(id)) { /* Duplicate of ID? */
                zone += "_loc"; /* Make it different */
            }
            loc.setID(zone);
            try {
                loc.init();
                saveLocation(loc);
            } catch (DuplicateEntityIDException deix) {
                loc.cleanup();
                lr.cleanup();
                return "Duplicate ID error";
            } catch (BadParameterException bpx) {
                loc.cleanup();
                lr.cleanup();
                return bpx.getMessage();
            }
        }
        lr.setRuleTargetLocation(loc);
        try {
            lr.init(); /* And initialize it */
            if(do_act) {    /* And activate if requested */
                lr.setRuleEnabled(actval);
            }
        } catch (BadParameterException bpx) {
            lr.cleanup();
            return bpx.getMessage();
        } catch (DuplicateEntityIDException deix) {
            lr.cleanup();
            return "Duplicate ID error";
        }
        return null;
    }
    /**
     * Create location
     *
     * @return Error string, if failed
     */
    public static String createLocation(String zone, Location parent,
        String[] args) {
        Map<String, Object> attr = new HashMap<String, Object>();

        /* First, be sure it doesn't already exist */
		try {
			ReaderEntityDirectory.testID(zone);
		} catch (DuplicateEntityIDException deidx) {
            return "Duplicate ID error";
        }
        /* Loop through other arguments for values */
        for (int i = 0; i < args.length; i++) {
            int eq = args[i].indexOf('='); /* Find equals */
            if (eq < 0) {
                return "Attribute id=value format error";
            }
            String aid = args[i].substring(0, eq); /* Get id */
            String aval = args[i].substring(eq + 1); /* Get value */
            if (aid.equals(LOCATION_PARENT_ATTRID)) {
                parent = ReaderEntityDirectory.findLocation(aval);
            } else if (aid.equals(Location.LOCATTRIB_DEFINESIRDOMAIN)) {
                attr.put(aid, aval.equals("true"));
            } else {
                attr.put(aid, aval);
            }
        }
        /* Now create rule */
        Location lr = new Location();
        lr.setID(zone); /* Set ID */
        lr.setAttibutes(attr); /* Set attributes */
        try {
            lr.setParent(parent);
            lr.init(); /* And initialize it */
        } catch (BadParameterException bpx) {
            lr.cleanup();
            return bpx.getMessage();
        } catch (DuplicateEntityIDException deix) {
            lr.cleanup();
            return "Duplicate ID error";
        }
        return null;
    }
    /**
     * Create user
     *
     * @return Error string, if failed
     */
    public static String createUser(String userid, String pwd, String pwdhash,
        String role) {
        /* Make sure it doesn't exist */
        if (users.get(userid) != null) {
            return "Duplicate ID error";
        }
        /* Create user object */
        try {
            User usr = new User(userid, ((pwd != null) ? pwd : ""), role);
            if (pwdhash != null)
                usr.pwd_hash = pwdhash;
            users.put(userid, usr);
            /* We're done */
            return null;
        } catch (BadParameterException bpx) {
            return bpx.getMessage();
        }
    }

    /**
     * Create sensor definition
     *
     * @return Error string, if failed
     */
    public static String createSensorDef(String id, String type,
        String[] args, String lbl, String dest) {
        /* First, be sure it doesn't already exist */
		try {
			ReaderEntityDirectory.testID(id);
		} catch (DuplicateEntityIDException deidx) {
            return "Duplicate ID error";
        }
		// Create sensor definition by type
		SensorDefinition def = SensorDefinition.createDefinition(type);
		if (def == null) {
			return "Invalid sensor type - " + type;
		}
		// Set ID
		def.setID(id);
		if (lbl != null)
			def.setLabel(lbl);
		// Get default attributes
		Map<String, Object> map = def.getAttributes();
        /* Loop through other arguments for values */
        for (int i = 0; i < args.length; i++) {
            int eq = args[i].indexOf('='); /* Find equals */
            if (eq < 0) {
            	def.cleanup();
                return "Attribute id=value format error";
            }
            String aid = args[i].substring(0, eq); /* Get id */
            String aval = args[i].substring(eq + 1); /* Get value */
            /* Find default */
            Object oldval = map.get(aid);
            if (oldval == null) {
            	def.cleanup();
            	return "Bad attribute id - " + aid;
            }
            if (oldval instanceof Integer) { /* Is integer? */
                try {
                    oldval = Integer.parseInt(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                	def.cleanup();
                    return "Bad attribute value - " + aval;
                }
            } 
            else if (oldval instanceof Double) { /* Is double? */
            	try {
                    oldval = Double.parseDouble(aval); /* Parse value */
                } catch (NumberFormatException nfx) {
                	def.cleanup();
                    return "Bad attribute value - " + aval;
                }
            } else if (oldval instanceof List) { /* List=set of tag IDs */
                List<String> nval = new ArrayList<String>();
                aval = aval.trim();
                if (aval.length() > 0) {
                	String[] l = aval.split(",");
                	for (String s : l) {
                		nval.add(s);
                	}
                }
                oldval = nval;
            } else if (oldval instanceof String) {
                oldval = aval;
            } else {
                error("Unknown type - " + oldval.getClass().toString());
            }
            map.put(aid, oldval); /* Update value */
        }
        // Set attributes
        try {
        	def.setDestinationAttrib(dest);
        	
            def.setAttributes(map);
            
            def.init();
        } catch (BadParameterException bpx) {
        	def.cleanup();
            return bpx.getMessage();
        } catch (DuplicateEntityIDException e) {
        	def.cleanup();
        	return e.getMessage();
		}
        return null;
    }

    private static class ActivateReaders implements Runnable {
		ArrayList<Reader> to_activate;

        public void run() {
	        /* Once we're done, activate the enabled ones */
	        for (Reader rdr : to_activate) {
	            if(rdr.setReaderLicense(true)) {
	                rdr.setReaderEnabled(true);
	                info("Activated reader " + rdr.getID());
	            }
	            else {
	                error("Reader " + rdr.getID() + " not activated : license count exceeded");
	            }
	        }
        }
    }

    /**
     * Load saved readers
     */
    private static ActivateReaders loadReaders() {
        ArrayList<Reader> to_activate = new ArrayList<Reader>();
        int spmcnt = 0;
        /* Now, list the files in the directory */
        File[] gfiles = data_dir.listFiles(new SuffixFilenameFilter(
            READER_FILENAME_SUFFIX));
        /* Loop through them, and load */
        for (int i = 0; i < gfiles.length; i++) {
            Properties p = new Properties();
            FileInputStream r = null;
            try {
                r = new FileInputStream(gfiles[i]);
                p.load(r);
            } catch (IOException iox) {
                error("Error loading "
                    + gfiles[i].getAbsolutePath());
                continue;
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
            /* Get the reader factory */
            String factid = p.getProperty(READER_FACTORY_PROPERTY);
            /* Get reader ID */
            String readerid = p.getProperty(READER_READERID_PROPERTY);
            /* Get reader label */
            String readerlabel = p.getProperty(READER_LABEL_PROPERTY, readerid);
            /* Get attribute strings */
            List<String> args = getEncodedAttributes(p);
            /* Get list of tag groups for the reader */
            List<String> tg = new ArrayList<String>();
            for (Enumeration<?> keys = p.propertyNames(); keys.hasMoreElements();) {
                String key = (String)keys.nextElement();

                if (key.startsWith(READER_TAGGROUP_PREFIX)) {
                    tg.add(p.getProperty(key));
                }
            }
            /* And create the readerfs */
            String err = createReader(readerid, factid, args, tg, readerlabel);
            if (err != null) {
                error(err);
            } else { /* Else, see if this one is active */
                String enabled = p
                    .getProperty(READER_ENABLED_PROPERTY, "false");
                if (enabled.equalsIgnoreCase(ENABLED_TRUE)) {
                    to_activate.add(ReaderEntityDirectory.findReader(readerid));
                    if (factid.equals(SPMReaderFactory.FACTORYID)) {
                    	spmcnt++;
                    }
                }
                info("Loaded reader " + readerid);
            }
        }
        /* For initial load, we temporarily grant enough licenses to activate
         * for 24 hours (assumes that we are Daytona and just need to be resent
         * licenses, or we mistakenly removed key) */
        int rdrcnt = to_activate.size() - spmcnt; 
        if (rdrcnt > ReaderEntityDirectory.getActiveReaderLimit()) {
            error("Insufficient licenses - some readers will be deactivated after " +
                LICENSE_TIMEOUT + " hours if no license is installed, and no additional readers can be activated.");
            ReaderEntityDirectory.setActiveReaderLimit(rdrcnt);
        }
        if (spmcnt > ReaderEntityDirectory.getActiveSPMLimit()) {
            error("Insufficient SPM licenses - some SPM readers will be deactivated after " +
                    LICENSE_TIMEOUT + " hours if no license is installed, and no additional SPM readers can be activated.");
                ReaderEntityDirectory.setActiveSPMLimit(spmcnt);
        }
		/* Do activate async - keep from stalling startup */
		ActivateReaders ar = new ActivateReaders();
		ar.to_activate = to_activate;

        return ar;
    }
    /**
     * Create a reader instance, using the given reader id, factory id,
     * attributes, and tag group associations
     */
    public static String createReader(String readerid, String factid,
        List<String> attribs, List<String> taggroups, String label) {
        int i;
        boolean do_act = false;
        boolean actval = false;
        /* Make sure the factory exists */
        ReaderFactory readerfactory = ReaderEntityDirectory
            .findReaderFactory(factid);
        if (readerfactory == null) {
            return "Invalid reader type";
        }
        /* See if ID already used */
		try {
			ReaderEntityDirectory.testID(readerid);
		} catch (DuplicateEntityIDException deidx) {
            return "Duplicate ID error";
        }
        /* Initialize the reader */
        Reader rdr = readerfactory.createReader();
        /* Get defaults */
        Map<String, Object> attr = readerfactory.getDefaultReaderAttributes();
        /* Set the reader's ID */
        rdr.setID(readerid);
        /* Set the reader's label */
        if(label != null)
            rdr.setLabel(label);
        /* Loop through the parameters */
        for (String v : attribs) {
            int eq = v.indexOf('=');
            if (eq < 0) {
                return "Missing equals in id=value";
            }
            String attrid = v.substring(0, eq);
            String aval = v.substring(eq + 1);
            /* If enabled, do activate/deactivate later */
            if(attrid.equals("enabled")) {
                do_act = true;
                actval = aval.equalsIgnoreCase("true");
            }
            /* Get current attribute value */
            Object curv = attr.get(attrid);
            /* If not defined, invalid attrib */
            if (curv == null) {
                continue;
            }
            /* Else, if its an integer */
            else if (curv instanceof Integer) {
                try {
                    Integer ival = Integer.parseInt(aval);
                    /* Update value */
                    attr.put(attrid, ival);
                } catch (NumberFormatException nfx) {
                    return "Bad attribute value";
                }
            }
            /* Else, if its a boolean */
            else if (curv instanceof Boolean) {
                Boolean bval = Boolean.valueOf(aval);
               /* Update value */
               attr.put(attrid, bval);
            }
            /* Else, if its a double list */
            else if (curv instanceof ListDouble) {
                int idx = aval.indexOf('[');
                if(idx >= 0) aval = aval.substring(idx+1).trim();
                idx = aval.lastIndexOf(']');
                if(idx >= 0) aval = aval.substring(0, idx).trim();
                ListDouble nval = new ListDouble();
                String[] l = aval.split(",");
                for(String s : l) {
                    try {
                        nval.add(Double.parseDouble(s));
                    } catch (NumberFormatException nfx) {
                        return "Bad format for double list";
                    }
                }
                attr.put(attrid, nval);
            }
            /* Else, if its a set of entity ids */
            else if (curv instanceof Set) {
                Set<ReaderEntity> nval = new HashSet<ReaderEntity>();
                String[] l = aval.split(",");
                for (String s : l) {
                    ReaderEntity re = ReaderEntityDirectory.findEntity(s);
                    if (re != null)
                        nval.add(re);
                }
                attr.put(attrid, nval);
            } else { /* Else, put in a string */
                attr.put(attrid, aval);
            }
        }
        try {
            /* Set the reader's attributes */
            rdr.setReaderAttributes(attr);
            rdr.init();
        } catch (DuplicateEntityIDException deix) {
            rdr.cleanup();
            return "Target exists";
        } catch (BadParameterException bpx) {
            rdr.cleanup();
            return bpx.getMessage();
        }

        /* Now build tag group list */
        if (taggroups != null) {
            TagGroup[] grps = new TagGroup[taggroups.size()];
            i = 0;
            for (String tgid : taggroups) {
                /* Find tag group */
                grps[i] = ReaderEntityDirectory.findTagGroup(tgid);
                i++;
            }
            try {
                /* Set the tag groups */
                rdr.setReaderTagGroups(grps);
            } catch (BadParameterException bpx) {
                rdr.cleanup();
                return bpx.getMessage();
            }
        }
        /* If enable/disable, do that now */
        if(do_act) {
            if(rdr.getReaderEnabled() != actval) {
                if(rdr.setReaderLicense(actval)) {
                    rdr.setReaderEnabled(actval);
                }
                else {
                    rdr.cleanup();
                    return "Reader cannot activate - license count exceeded";
                }
            }
        }
        /* Call lifecycle listeners */
        ArrayList<ReaderLifecycleListener> lst = rdrlifelisteners;
        int sz = lst.size();
        for(i = 0; i < sz; i++) {
            lst.get(i).readerLifecycleCreate(rdr);
        }
        /* Set session handler */
        rdr.addReaderStatusListener(statuslistener);

        return null;
    }
    /**
     * Save tag group definition to data directory
     */
    public static void saveTagGroup(TagGroup tg) {
		if(tg.isSubGroup())	/* Cannot persist subgroups */
			return;
        /* Build properties for object */
        Properties p = new Properties();
        /* Add tag type */
        p.setProperty(TAGGROUP_TAGTYPE_PROPERTY, tg.getTagType().getID());
        /* Add tag group ID */
        p.setProperty(TAGGROUP_TAGGROUPID_PROPERTY, tg.getID());
        /* Add label */
        p.setProperty(TAGGROUP_LABEL_PROPERTY, tg.getLabel());
        /* Add attributes to property set */
        addEncodedAttributes(p, tg.getTagGroupAttributes());
        /* Now write it to the file */
        File f = new File(data_dir, tg.getID() + TAGGROUP_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Tag group " + tg.getID());
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete tag group file
     */
    public static void deleteTagGroup(TagGroup tg) {
		if(tg.isSubGroup())	/* Cannot persist subgroups */
			return;
        File f = new File(data_dir, tg.getID() + TAGGROUP_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
        }
        updateConfigHash();
    }
    /**
     * Save location rule definition to data directory
     */
    public static void saveLocationRule(LocationRule lr) {
        /* Build properties for object */
        Properties p = new Properties();
        /* Add rule type */
        p.setProperty(LOCATIONRULE_RULETYPE_PROPERTY, lr
            .getLocationRuleFactory().getID());
        /* Add rule ID */
        p.setProperty(LOCATIONRULE_RULEID_PROPERTY, lr.getID());
        /* Add zone ID */
        p.setProperty(LOCATIONRULE_ZONEID_PROPERTY, lr.getRuleTargetLocation()
            .getID());
        /* Add enabled property */
        p.setProperty(LOCATIONRULE_ENABLED_PROPERTY, (lr.getRuleEnabled()
            ? ENABLED_TRUE
            : ENABLED_FALSE));
        /* Add attributes to property set */
        addEncodedAttributes(p, lr.getLocationRuleAttributes());
        /* Now write it to the file */
        File f = new File(data_dir, lr.getID() + LOCATIONRULE_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Location rule " + lr.getID());
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete location rule file
     */
    public static void deleteLocationRule(LocationRule lr) {
        File f = new File(data_dir, lr.getID() + LOCATIONRULE_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
        }
        updateConfigHash();
    }

    /**
     * Save sensor definition to data directory
     */
    public static void saveSensorDefinition(SensorDefinition sd) {
        /* Build properties for object */
        Properties p = new Properties();
        /* Add sensor definition type */
        p.setProperty(SENSORDEF_SENSORTYPE_PROPERTY, sd.getSensorDefinitionType());
        /* Add sensor definition ID */
        p.setProperty(SENSORDEF_DEFID_PROPERTY, sd.getID());
        /* Add label */
        p.setProperty(SENSORDEF_LABEL_PROPERTY, sd.getLabel());
        /* Add destination */
        p.setProperty(SENSORDEF_DEST_PROPERTY, sd.getDestinationAttrib());
        /* Add attributes to property set */
        addEncodedAttributes(p, sd.getAttributes());
        /* Now write it to the file */
        File f = new File(data_dir, sd.getID() + SENSORDEF_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Sensor Definition " + sd.getID());
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete sensor definition
     */
    public static void deleteSensorDefintion(SensorDefinition sd) {
        File f = new File(data_dir, sd.getID() + SENSORDEF_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
        }
        updateConfigHash();
    }

    /**
     * Save location definition to data directory
     */
    public static void saveLocation(Location lr) {
        /* Build properties for object */
        Properties p = new Properties();
        /* Add zone ID */
        p.setProperty(LOCATION_ZONEID_PROPERTY, lr.getID());
        /* Add parent ID */
        if (lr.getParent() != null)
            p.setProperty(LOCATION_PARENTID_PROPERTY, lr.getParent().getID());
        else
            p.setProperty(LOCATION_PARENTID_PROPERTY, "");
        /* Add attributes to property set */
        addEncodedAttributes(p, lr.getAttributes());
        /* Now write it to the file */
        File f = new File(data_dir, lr.getID() + LOCATION_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Location " + lr.getID());
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete location file
     */
    public static void deleteLocation(Location lr) {
        File f = new File(data_dir, lr.getID() + LOCATION_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
        }
        updateConfigHash();
    }

    /**
     * Save user definition to data directory
     */
    public static void saveUser(User usr) {
        /* Build properties for object */
        Properties p = new Properties();
        /* Add user ID */
        p.setProperty(USER_USERID_PROPERTY, usr.userid);
        /* Add pwd hash */
        p.setProperty(USER_PWDHASH_PROPERTY, usr.pwd_hash);
        /* Add role */
        p.setProperty(USER_ROLE_PROPERTY, usr.role);
        /* Now write it to the file */
        File f = new File(data_dir, usr.userid + USER_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "User " + usr.userid);
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete user file
     */
    public static void deleteUser(User usr) {
        File f = new File(data_dir, usr.userid + USER_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
            users.remove(usr.userid);
        }
        updateConfigHash();
    }

    /**
     * Save reader definition to data directory
     */
    public static void saveReader(Reader rdr) {
        /* Build properties for object */
        Properties p = new Properties();
        /* Set the reader factory */
        p.setProperty(READER_FACTORY_PROPERTY, rdr.getReaderFactory().getID());
        /* Set reader ID */
        p.setProperty(READER_READERID_PROPERTY, rdr.getID());
        /* Set reader label */
        p.setProperty(READER_LABEL_PROPERTY, rdr.getLabel());
        /* Add attributes to property set */
        addEncodedAttributes(p, rdr.getReaderAttributes());
        /* Add tag groups for reader */
        TagGroup[] tg = rdr.getReaderTagGroups();
        for (int i = 0; i < tg.length; i++) {
            p.setProperty(READER_TAGGROUP_PREFIX + i, tg[i].getID());
        }
        /* Add enabled setting */
        p.setProperty(READER_ENABLED_PROPERTY, rdr.getReaderEnabled()
            ? ENABLED_TRUE
            : ENABLED_FALSE);
        /* Now write it to the file */
        File f = new File(data_dir, rdr.getID() + READER_FILENAME_SUFFIX);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Reader " + rdr.getID());
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }
    /**
     * Delete reader file
     */
    public static void deleteReader(Reader rdr) {
        /* Call lifecycle listeners */
        ArrayList<ReaderLifecycleListener> lst = rdrlifelisteners;
        for(int i = 0; i < lst.size(); i++) {
            lst.get(i).readerLifecycleDelete(rdr);
        }
        File f = new File(data_dir, rdr.getID() + READER_FILENAME_SUFFIX);
        if (f.exists()) {
            f.delete();
        }
        updateConfigHash();
    }
    /**
     * Our tag lifecycle and status listener
     */
    private static class OurTagListener implements TagStatusListener {
        Object vals[] = new Object[16];
        StringBuilder sb = new StringBuilder();
        /**
         * Tag created notification. Called after tag has been created, but
         * before its TagLinks have been added (i.e. the end of init()).
         *
         * @param tag -
         *            tag being created
         * @param cause -
         *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
         */
        public void tagLifecycleCreate(Tag tag, String cause) {
            if (!InboundSessionHandler.broadcastTest(false, true))
                return;
            sb.setLength(0);
            sb.append("TagFound: guid=").append(tag.getTagGUID());
            String aids[] = tag.getTagGroup().getTagType().getTagAttributes();
            if (aids.length > vals.length)
                vals = new Object[aids.length];
            tag.readTagAttributes(vals, 0, vals.length);
            for (int i = 0; i < aids.length; i++) {
            	if (vals[i] instanceof CustomFieldValue) {
            		CustomFieldValue cfv = (CustomFieldValue) vals[i];
            		sb.append(",").append(cfv.getField()).append("=").append(cfv.getValue());
            	}
            	else {
            		sb.append(",").append(aids[i]).append("=").append(vals[i]);
            	}
            }
            sb.append("\n");
            InboundSessionHandler.broadcastString(sb.toString(), false, true);
        }
        /**
         * Tag deleted notification. Called at start of tag cleanup(), when tag
         * is about to be deleted (generally due to having no active TagLinks).
         *
         * @param tag -
         *            tag being deleted
         */
        public void tagLifecycleDelete(Tag tag) {
            if (!InboundSessionHandler.broadcastTest(false, true))
                return;
            sb.setLength(0);
            sb.append("TagLost: guid=").append(tag.getTagGUID()).append("\n");
            InboundSessionHandler.broadcastString(sb.toString(), false, true);
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         *
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than
         *            the number of atributes for the given tag). Ordered to
         *            match attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            if (!InboundSessionHandler.broadcastTest(false, true))
                return;
            sb.setLength(0);
            sb.append("TagChange: guid=").append(tag.getTagGUID());

            String aids[] = tag.getTagGroup().getTagType().getTagAttributes();
            if (aids.length > vals.length)
                vals = new Object[aids.length];

            tag.readTagAttributes(vals, 0, vals.length);

            boolean not_just_confidence = false;
            for (int i = 0; i < aids.length; i++) {
                if (vals[i] != oldval[i]) {
                    /* If not just a confidence change */
                    if (!aids[i]
                        .equals(LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE)) {
                        not_just_confidence = true;
                    	if (vals[i] instanceof CustomFieldValue) {
                    		CustomFieldValue cfv = (CustomFieldValue) vals[i];
                    		sb.append(",").append(cfv.getField()).append("=").append(
                            		cfv.getValue());
                    	}
                    	else {
                    		sb.append(",").append(aids[i]).append("=").append(
                        		vals[i]);
                    	}
                    }
                }
            }
            sb.append("\n");
            if (not_just_confidence)
                InboundSessionHandler.broadcastString(sb.toString(), false,
                    true);
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         *
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, Object[] attrvals) {
            if (!InboundSessionHandler.broadcastTest(false, true))
                return;
            sb.setLength(0);
            sb.append("TagTriggered: guid=").append(tag.getTagGUID());

            sb.append(", triggerid=" + cmd);
            for (int i = 0; i < attrids.length; i++) {
                sb.append(",").append(attrids[i]).append("=").append(
                            attrvals[i]);
            }
            sb.append("\n");
            InboundSessionHandler.broadcastString(sb.toString(), false, true);
        }

        /**
         * TagLink added notificiation. Called after taglink has been added to
         * tag.
         *
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {

        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed
         * from tag.
         *
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {

        }
    }
    /**
     * Parse args form input - splits based on non-whitespace runs, while
     * allowing for quoted strings containing whitespace and/or quotes
     *
     * @param s -
     *            input string
     * @return array of tokens
     */
    public static ArrayList<String> splitLine(StringBuilder s) {
        ArrayList<String> rslt = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        boolean in_quote = false;
        boolean got_char = false;
        boolean got_backslash = false;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if ((!in_quote) && Character.isWhitespace(c)) { /* If whitespace */
                if (got_char) { /* Accumulated string? */
                    rslt.add(sb.toString()); /* Add it */
                    sb.setLength(0); /* And clear accum */
                    got_char = false;
                }
            } else {
                if (c == '"') { /* Quote? */
                    if(got_backslash)   /* Escaped quote */
                        sb.append(c);
                    else
                        in_quote = !in_quote;
                    got_backslash = false;
                } else {
                    if(got_backslash) {
                        sb.append('\\');
                        got_backslash = false;
                    }
                    if(c == '\\')
                        got_backslash = true;
                    else
                        sb.append(c);
                }
                got_char = true;
            }
        }
        if (got_char) { /* Anything left? */
            rslt.add(sb.toString());
        }
        return rslt;
    }
    /**
     * Test if valid ID string
     *
     * @param s -
     *            ID string
     * @return true if valid, false if not
     */
    public static boolean testIfValidID(String s) {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if (((c >= 'A') && (c <= 'Z')) || /* Uppercase */
            ((c >= 'a') && (c <= 'z')) || /* Lowercase */
            ((c >= '0') && (c <= '9')) || /* Number */
            (c == '_') || /* Underscore */
            (c == '-') || /* Dash */
            (c == '$')) { /* Dollarsign */
            } else
                return false;
        }
        if (s.equals(RangerServer.NULL_VAL)) /* "null" is reserved */
            return false;
        return true;
    }
    /**
     * Print tag data
     */
    public static void printTag(CommandContext ctx, StringBuilder sb, Tag tag,
        boolean is_first) {
        TagType tt = tag.getTagGroup().getTagType();
        boolean is_text = (ctx.getFormatExt() == CommandContext.OutputSyntax.TEXT);
        boolean is_json = (ctx.getFormatExt() == CommandContext.OutputSyntax.JSON);

        ctx.appendStartObject(sb, "tag", tag.getTagGUID(), is_first);
        ctx.appendAttrib(sb, "tagid", tag.getTagID(), true);
        ctx.appendAttrib(sb, "taggroupid", tag.getTagGroup(), false);
        ctx.appendAttrib(sb, "tagtype", tt, false);
        Map<String, Object> attribs = tag.getTagAttributes();
        for (String id : attribs.keySet()) {
        	Object val = attribs.get(id);
        	if (val instanceof CustomFieldValue) {
        		CustomFieldValue cfv = (CustomFieldValue) val;
        		ctx.appendAttrib(sb, cfv.getField(), cfv.getValue(), false);
        	}
        	else {
        		ctx.appendAttrib(sb, id, val, false);
        	}
        }
        /* Loop through our links */
        Map<String, TagLink> links = tag.getTagLinks();
        TreeSet<String> chans = new TreeSet<String>(links.keySet());
        if (is_json) {
            sb.append("\n },\n \"taglinks\" : [\n ");
        } else if (is_text) {
            sb.append("\n");
        } else {
            ctx.appendStartObject(sb, "tag-link-list", null, false);
        }
        boolean firstlink = true;
        for (String tlid : chans) {
            TagLink tl = links.get(tlid);
            if (!is_text) {
                ctx.appendStartObject(sb, "tag-link", null, firstlink);
                ctx.appendAttrib(sb, "tagid", tl.getTag().getTagGUID(), true);
                ctx.appendAttrib(sb, "channelid", tl.getChannel().getID(),
                    false);
            } else {
                sb.append("(").append(ctx.makeString(tl.getChannel()));
                sb.append(": reader=");
                sb.append(ctx.makeString(tl.getChannel().getReader()));
                sb.append(", channel=");
                sb.append(ctx
                    .makeString(tl.getChannel().getRelativeChannelID()));
            }
            attribs = tl.getTagLinkAttributes();
            for (Map.Entry<String, Object> ent : attribs.entrySet()) {
            	Object val = ent.getValue();
            	if (val instanceof CustomFieldValue) {
            		CustomFieldValue cfv = (CustomFieldValue) val;
            		ctx.appendAttrib(sb, cfv.getField(), cfv.getValue(), false);
            	}
            	else {
            		ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), false);
            	}
            }
            if (!is_text) {
                ctx.appendEndObject(sb, "tag-link", null);
            } else {
                sb.append(")\n");
            }
            firstlink = false;
        }
        if (is_json) {
            sb.append("] ");
        }
        else if (!is_text) {
            ctx.appendEndObject(sb, "tag-link-list", null);
        }
        if (is_json)
            sb.append("}\n");
        else
            ctx.appendEndObject(sb, "tag", tag.getTagGUID());
    }
    /**
     * Print tag link data
     */
    public static void printTagLink(CommandContext ctx, StringBuilder sb,
        TagLink tl, boolean is_first) {
        boolean is_text = ctx.getFormatExt().equals(
            CommandContext.OutputSyntax.TEXT);
        String id = tl.getTag().getTagGUID() + "_" + tl.getChannel().getID();
        if (!is_text) {
            ctx.appendStartObject(sb, "tag-link", id, is_first);
            ctx.appendAttrib(sb, "tagid", tl.getTag().getTagGUID(), true);
            ctx.appendAttrib(sb, "channelid", tl.getChannel().getID(), false);
        } else {
            sb.append("Tag ");
            sb.append(tl.getTag().getTagGUID());
            sb.append(": reader=");
            sb.append(ctx.makeString(tl.getChannel().getReader()));
            sb.append(", channel=");
            sb.append(ctx.makeString(tl.getChannel().getRelativeChannelID()));
        }
        Map<String, Object> attribs = tl.getTagLinkAttributes();
        for (Map.Entry<String, Object> ent : attribs.entrySet()) {
            ctx.appendAttrib(sb, ent.getKey(), ent.getValue(), false);
        }
        ctx.appendEndObject(sb, "tag-link", id);
    }
    /**
     * Encode string for CVS output, if needed
     * @param o - input object
     * @returns same string (if no change needed), or encoded string
     */
    public static String encodeForCSV(Object o) {
        String s;
        if (o == null) {
            s = "<null>";
        } else if (o instanceof String) {
            s = o.toString();
        } else if (o instanceof ReaderEntity) {
            s = ((ReaderEntity) o).getID();
        } else if (o instanceof Set) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (Object oinst : (Set<?>) o) {
                if(!first) sb.append(",");
                sb.append(oinst.toString());
                first = false;
            }
            s = sb.toString();
        } else if (o instanceof List) {
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (Object oinst : (List<?>) o) {
                if(!first) sb.append(",");
                sb.append(oinst.toString());
                first = false;
            }
            s = sb.toString();
        } else if (o instanceof Map) {
            StringBuilder sb = new StringBuilder();
            Map<?,?> omap = (Map<?,?>) o;
            boolean first = true;
            for (Object e : omap.entrySet()) {
                if(!first) sb.append(",");
                Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                sb.append(ent.getKey().toString()).append("=").append(
                    ent.getValue().toString());
                first = false;
            }
            s = sb.toString();
        } else
            s = o.toString();
        if(s.length() == 0)
            s = "<blank>";
        /* If quote or embedded space or comma, needs to be fixed */
       if((s.indexOf('\"') >= 0) || (s.indexOf(' ') >= 0) ||
           (s.indexOf(',') >= 0)) {
           StringBuilder sb = new StringBuilder();
           sb.append("\"");
           for(int i = 0; i < s.length(); i++) {
               char c = s.charAt(i);
               if(c == '\"') {  /* If quote, double it */
                   sb.append("\"\"");
               }
               else {
                   sb.append(c);
               }
           }
           sb.append('\"');
           s = sb.toString();
       }
       return s;
    }
    /**
     * Print tag report, with given attribute IDs
     */
    public static void printTagReport(CommandContext ctx, StringBuilder sb,
        Tag tag, List<String> attribs, boolean is_first) {
        Object loc = tag.getTagAttributes().get(
            LocationRuleFactory.ATTRIB_ZONELOCATION);
        Location ourloc = null;
        Map<String, Object> locvals = null;
        if ((loc != null) && (loc instanceof String)) {
            ourloc = ReaderEntityDirectory.findLocation((String) loc);
            if (ourloc != null)
                locvals = ourloc.getFullAttributes();
        }
        Map<String, Object> tagvals = tag.getTagAttributes();
        /* Always start with tag GUID */
        sb.append(encodeForCSV(tag.getTagGUID()));
        for (String aid : attribs) {
            Object val = tagvals.get(aid); /* Try to get attribute from tag */
            if (val == null) { /* If not there, check location */
                if (locvals != null)
                    val = locvals.get(aid);
                if ((val == null) && (ourloc != null)
                    && aid.equals(RangerServer.LOCATIONPATH_ID)) {
                    val = ourloc.getPath();
                }
            }
            sb.append(",");
            if(val != null)
                sb.append(encodeForCSV(val.toString()));
        }
        sb.append("\n");
    }
    /**
     * Encode string with escapes for XML
     *
     * @param s -
     *            string
     * @return encoded string
     */
    public static String encXML(String s) {
        StringBuilder sb = new StringBuilder();
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '>':
                    sb.append("&gt;");
                    break;
                case '<':
                    sb.append("&lt;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }
    /**
     * Encode string with escapes for JSON
     *
     * @param s -
     *            string
     * @return encoded string
     */
    public static String encJSON(String s) {
        StringBuilder sb = new StringBuilder();
        int len = s.length();
        sb.append('\"');
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '\"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        sb.append('\"');
        return sb.toString();
    }
    /**
     * Method for encoding value as XML-CDATA string
     *
     * @param o -
     *            object value
     */
    public static String toXMLString(Object o) {
        String v;
        if (o == null) {
            v = "<null/>";
        } else if (o instanceof String) {
            v = encXML(o.toString());
        } else if (o instanceof ReaderEntity) {
            v = encXML(((ReaderEntity) o).getID());
        } else if (o instanceof Set) {
            StringBuilder sb = new StringBuilder();
            sb.append("<set>");
            for (Object oinst : (Set<?>) o) {
                sb.append("<element>");
                sb.append(toXMLString(oinst));
                sb.append("</element>");
            }
            sb.append("</set>");
            v = sb.toString();
        } else if (o instanceof List) {
            StringBuilder sb = new StringBuilder();
            sb.append("<list>");
            for (Object oinst : (List<?>) o) {
                sb.append("<element>");
                sb.append(toXMLString(oinst));
                sb.append("</element>");
            }
            sb.append("</list>");
            v = sb.toString();
        } else if (o instanceof Map) {
            StringBuilder sb = new StringBuilder();
            sb.append("<map>");
            Map<?,?> omap = (Map<?,?>) o;
            for (Object e : omap.entrySet()) {
                Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                sb.append("<entry id=\"").append(
                    encXML(ent.getKey().toString())).append("\">").append(
                    toXMLString(ent.getValue())).append("</entry>");
            }
            sb.append("</map>");
            v = sb.toString();
		} else if (o instanceof Number) {
            v = RangerServer.formatNumber(o);
            if(v == null) 
                v = "<null/>";
            else
    			v = encXML(v);
        } else
            v = encXML(o.toString());
        return v;
    }
    /**
     * Start object using requested encoding
     *
     * @param sb -
     *            string builder to append to
     * @param type -
     *            object type
     * @param id -
     *            object id
     * @param is_first -
     *            true if first, false if not
     */
    static StringBuilder appendStartObject(StringBuilder sb, String type,
        String id, boolean is_first, CommandContext ctx) {
        CommandContext.OutputSyntax enc = ctx.getFormatExt();
        if (enc == CommandContext.OutputSyntax.XML) {
            sb.append("<").append(type);
            if (id != null)
                sb.append(" id=\"").append(id).append("\"");
            sb.append(">");
        } else if (enc == CommandContext.OutputSyntax.JSON) {
            if (!is_first)
                sb.append(",\n");
            if (id == null) {
                sb.append("{ ");
            } else {
                sb.append("\"").append(id).append("\" : { \"id\" : \"").append(
                    id).append("\",\n \"attributes\" : { \n ");
            }
        } else {
            if (id != null) {
                sb.append(id).append(": ");
            }
        }
        return sb;
    }
    /**
     * Add attribute id = value, based on encoding
     *
     * @param sb -
     *            string builder to append to
     * @param id -
     *            attribute ID
     * @param val -
     *            attribute value
     * @param is_first -
     *            if first in list
     * @param ctx -
     *            output context
     */
    static StringBuilder appendAttrib(StringBuilder sb, String id, Object val,
        boolean is_first, CommandContext ctx) {
        CommandContext.OutputSyntax enc = ctx.getFormatExt();
        if (enc == CommandContext.OutputSyntax.XML)
            return appendXMLAttrib(sb, id, val);
        else if (enc == CommandContext.OutputSyntax.JSON) {
            if (!is_first)
                sb.append(",\n ");
            return appendJSONAttrib(sb, id, val);
        } else { /* Else, assume plain text */
            if (!is_first)
                sb.append(", ");
            sb.append(id).append("=").append(ctx.makeString(val));
            return sb;
        }
    }
    /**
     * End object using requested encoding
     *
     * @param sb -
     *            string builder to append to
     * @param type -
     *            object type
     * @param ctx -
     *            output context
     */
    static StringBuilder appendEndObject(StringBuilder sb, String type,
        String id, CommandContext ctx) {
        CommandContext.OutputSyntax enc = ctx.getFormatExt();
        if (enc == CommandContext.OutputSyntax.XML)
            sb.append("</").append(type).append(">\n");
        else if (enc == CommandContext.OutputSyntax.JSON) {
            if (id == null)
                sb.append(" }\n");
            else
                sb.append(" } }\n");
        } else { /* Else, assume plain text */
            sb.append("\n");
        }
        return sb;
    }
    /**
     * Add XML encoded attribute tag
     *
     * @param sb -
     *            string builder to append to
     * @param id -
     *            attribute ID
     * @param val -
     *            attribute value
     */
    private static StringBuilder appendXMLAttrib(StringBuilder sb, String id,
        Object val) {
        sb.append("<attrib id=\"").append(toXMLString(id)).append("\">");
        sb.append(toXMLString(val)).append("</attrib>");
        return sb;
    }
    /**
     * Method for encoding value as JSON value
     *
     * @param o -
     *            object value
     */
    public static String toJSONString(Object o) {
        String v;
        if (o == null) {
            v = "null";
        } else if (o instanceof String) {
            v = encJSON(o.toString());
        } else if (o instanceof ReaderEntity) {
            v = encJSON(((ReaderEntity) o).getID());
        } else if (o instanceof Set) {
            StringBuilder sb = new StringBuilder();
            sb.append(" [ ");
            boolean first = true;
            for (Object oinst : (Set<?>) o) {
                if (!first)
                    sb.append(" ,\n");
                sb.append(toJSONString(oinst));
                first = false;
            }
            sb.append(" ]\n");
            v = sb.toString();
        } else if (o instanceof List) {
            StringBuilder sb = new StringBuilder();
            sb.append(" [ ");
            boolean first = true;
            for (Object oinst : (List<?>) o) {
                if (!first)
                    sb.append(" ,\n");
                sb.append(toJSONString(oinst));
                first = false;
            }
            sb.append(" ]\n");
            v = sb.toString();
        } else if (o instanceof Map) {
            StringBuilder sb = new StringBuilder();
            sb.append("{ ");
            Map<?,?> omap = (Map<?,?>) o;
            boolean first = true;
            for (Object e : omap.entrySet()) {
                Map.Entry<?,?> ent = (Map.Entry<?,?>) e;
                if (!first)
                    sb.append(" ,\n");
                sb.append(toJSONString(ent.getKey().toString()));
                sb.append(" : ");
                sb.append(toJSONString(ent.getValue()));
                first = false;
            }
            sb.append(" }\n");
            v = sb.toString();
        } else if ((o instanceof Integer) || (o instanceof Double)
            || (o instanceof Long)) {
            v = RangerServer.formatNumber(o);
            if(v == null) v = "null";
        } else
            v = encJSON(o.toString());
        return v;
    }
    /**
     * Add JSON encoded attribute value pair
     *
     * @param sb -
     *            string builder to append to
     * @param id -
     *            attribute ID
     * @param val -
     *            attribute value
     */
    static StringBuilder appendJSONAttrib(StringBuilder sb, String id,
        Object val) {
        sb.append(toJSONString(id)).append(" : ").append(toJSONString(val));
        return sb;
    }
    /**
     * Get user accounts
     */
    public static Map<String, User> getUsers() {
        return users;
    }
    /**
     * Get command handlers
     */
    public static Map<String, CommandHandler> getCommandHandlers() {
        return cmds;
    }
    /**
     * Get version
     */
    public static String getVersion() {
        return VERSION;
    }
    /**
     * Get product name
     */
    public static String getProductName() {
        return PRODUCTNAME;
    }
    /**
     * Get company name
     */
    public static String getCompanyName() {
        return COMPANYNAME;
    }
    /**
     * Get company url
     */
    public static String getCompanyURL() {
        return COMPANYURL;
    }
    /**
     * Get build
     */
    public static String getBuild() {
        return BUILD;
    }
    /**
     * Main entry point for server
     */
    public static void initRanger() throws CommandException {
        LocationRuleEngine lre;

        /* This SUCKS, but welcome to Java.... internal permanent DNS caching
         * when every operating system already handles its own DNS caching...
         * permanent DNS caching == broken DHCP */
        java.security.Security.setProperty("networkaddress.cache.ttl" , "30");

        /* Initialize the data directory */
        initDataDirectory();

        try {
            /* Get our session factory */
            sf = new SessionFactoryImpl();
            sf.init();
        } catch (SessionException sx) {
            throw new CommandException(sx.getMessage());
        }
        /* Add 24-hour periodic license check job */
        RangerProcessor.addDelayedAction(
            new LicenseCheck(),
            1000L*3600L*LICENSE_TIMEOUT);
        /* Add 1 second periodic tag timeout processing job */
        RangerProcessor.addDelayedAction(
            new TagTimeoutCheck(),
            1000L);
        /* Add 60 second periodic reader DNS processing job */
        RangerProcessor.addDelayedAction(
            new ReaderDNSCheck(),
            60*1000L);
        /* Initialize the update event storage manager */
        initUpdateEventStorage();

        /* Add our global listener */
        AbstractTagType.addGlobalTagLifecycleListener(new OurTagListener());

        /* Load reader factories */
        loadReaderFactories();
        /* Load tag types */
        loadTagTypes();
        /* Load command handlers */
        loadCommandHandlers();
        /* Initialize key manager */
        initializeKeyManager();
        /* Get our location rule engine */
        lre = LocationRuleEngine.getLocationRuleEngine();
        lre.setSessionFactory(sf);
        try {
            lre.init();
        } catch (BadParameterException bpx) {
            throw new CommandException(bpx.getMessage());
        }
        /* Load tag expected locations (do early to prevent any invalidates) */
        loadTagExpectedLocations();
        /* Load location rule types */
        loadLocationRuleTypes();
        /* Load extensions */
        loadExtensionHandlers();
		/* Load GPS Defaults */
		loadGPSDefaults();
        /* Load licenses */
        loadLicenses();
        /* Load users */
        loadUsers();
        /* Load sensor definitions */
        loadSensorDefinitions();
        /* Load tag groups */
        loadTagGroups();
        /* Load readers */
        ActivateReaders ar = loadReaders();
        /* Load configuration hashcode */
        loadConfigHash();
        /* Load locations */
        loadLocations();
        /* Load location rules */
        loadLocationRules();
        /* Load asset tags set */
        loadAssetTagsSet();
		/* Initialize firmware update */
		try { 
			FirmwareLibrary.initialize();
		} catch (IOException iox) {
			error("Error initializing firmware upgrade: " + iox.getMessage());
		}			

        /* Start logger thread */
        Thread t = new Thread(new RangerLogger(), "RangerLogger");
		t.setDaemon(true);
		t.start();

        try {
            /* Create server socket for command sessions */
            int port = Integer.parseInt(our_config.getProperty("commandport",
                "6501"));
            cmd_ss = sf.createTCPIPServerSocket();
            cmd_ss.setIPAddress(new InetSocketAddress(port));
            cmd_ss.setServerHandler(new OurTCPIPServerHandler(true));
            cmd_ss.requestBind(); /* Activate it */
            info("Command port active on " + port);
            /* Create server socket for tag event sessions */
            port = Integer
                .parseInt(our_config.getProperty("eventport", "6502"));
            evt_ss = sf.createTCPIPServerSocket();
            evt_ss.setIPAddress(new InetSocketAddress(port));
            evt_ss.setServerHandler(new OurTCPIPServerHandler(false));
            evt_ss.requestBind(); /* Activate it */
            info("Event port active on " + port);
            /* Create server socket for up connection sessions */
            port = Integer
                .parseInt(our_config.getProperty("upconnect", "6503"));
            upconn_ss = sf.createTCPIPServerSocket();
            upconn_ss.setIPAddress(new InetSocketAddress(port));
            upconn_ss.setServerHandler(new OurUpConnectionHandler());
            upconn_ss.requestBind(); /* Activate it */
            info("Up connection port active on " + port);
            /* Check if Tavis Concentrator DirectStream is configured, and set it up if needed */
            port = Integer.parseInt(our_config.getProperty("directstream", "0"));
            if(port > 0) {
                TavisConcentratorDirectStreamHandler.configurePort(port);
            }

        } catch (SessionException sx) {
            throw new CommandException(sx.getMessage());
        }
        /* Fire off reader activation */
        if(ar != null) {
            RangerProcessor.addDelayedAction(ar, 5000L);
        }
    }
    /**
     * Terminate ranger server
     */
    public static void termRanger() {
        if (cmd_ss != null) {
            try {
                cmd_ss.requestUnbind();
            } catch (SessionException sx) {
            }
            cmd_ss = null;
        }
        if (evt_ss != null) {
            try {
                evt_ss.requestUnbind();
            } catch (SessionException sx) {
            }
            evt_ss = null;
        }
        if (upconn_ss != null) {
            try {
                upconn_ss.requestUnbind();
            } catch (SessionException sx) {
            }
            upconn_ss = null;
        }
        TavisConcentratorDirectStreamHandler.cleanupPort();
        if (sf != null) {
            sf.cleanup();
            sf = null;
        }
    }
    /* Load our version properties */
    public static void loadVersion(InputStream is) {
        try {
            our_config.load(is);
            String pn = our_config.getProperty("productname");
            if (pn != null)
                PRODUCTNAME = pn;
            pn = our_config.getProperty("companyname");
            if (pn != null)
                COMPANYNAME = pn;
            pn = our_config.getProperty("companyurl");
            if (pn != null)
                COMPANYURL = pn;
            pn = our_config.getProperty("version");
            if (pn != null)
                VERSION = pn;
            pn = our_config.getProperty("build");
            if (pn != null)
                BUILD = pn;
            pn = our_config.getProperty("readercount");
            if(pn != null) {
                READERLIMIT = Integer.parseInt(pn);
                PRODUCTNAME += " (evaluation - " + READERLIMIT + " readers)";
            }
            ReaderEntityDirectory.setActiveReaderLimit(READERLIMIT);
        } catch (IOException iox) {
            error("Error loading VersionInfo");
        }
    }
    /**
     * Main entry point for test app
     *
     * @param args
     */
    public static void main(String[] args) {
        loadConfig();
        loadCustomConfig();
        try {
            initRanger();
        } catch (CommandException cx) {
            termRanger();
        }
    }
    /**
     * Get session factory
     */
    public static SessionFactory getSessionFactory() {
        return sf;
    }
    /**
     * Get our config object
     */
    public static Properties getConfig() {
        return our_config;
    }
    /**
     * Get custom config object
     */
    public static Properties getCustomConfig() {
        return cust_config;
    }
    
    /**
     * Get system config object
     */
    public static Properties getSystemConfig() {
    	return system_config;
    }

    /* Add listener for reader status */
    public static void addReaderStatusListener(ReaderStatusListener rdr) {
    	ArrayList<ReaderStatusListener> newlst = new ArrayList<ReaderStatusListener>(rdrlisteners);
		newlst.add(rdr);
		rdrlisteners = newlst;
    }
    /* Remove listener for reader status */
    public static void removeReaderStatusListener(ReaderStatusListener rdr) {
    	ArrayList<ReaderStatusListener> newlst = new ArrayList<ReaderStatusListener>(rdrlisteners);
		newlst.remove(rdr);
		rdrlisteners = newlst;
    }
    /* Add listener for reader lifecycle */
    public static void addReaderLifecycleListener(ReaderLifecycleListener rdr) {
		ArrayList<ReaderLifecycleListener> newlst = new ArrayList<ReaderLifecycleListener>(rdrlifelisteners);
		newlst.add(rdr);
		rdrlifelisteners = newlst;
    }
    /* Remove listener for reader lifecycle */
    public static void removeReaderLifecycleListener(ReaderLifecycleListener rdr) {
		ArrayList<ReaderLifecycleListener> newlst = new ArrayList<ReaderLifecycleListener>(rdrlifelisteners);
		newlst.remove(rdr);
		rdrlifelisteners = newlst;
    }

    /* Add listener for gps status */
    public static void addGPSStatusListener(GPSStatusListener gps) {
    	ArrayList<GPSStatusListener> newlst = new ArrayList<GPSStatusListener>(gpslisteners);
		newlst.add(gps);
		gpslisteners = newlst;
    }
    /* Remove listener for GPS status */
    public static void removeGPSStatusListener(GPSStatusListener gps) {
    	ArrayList<GPSStatusListener> newlst = new ArrayList<GPSStatusListener>(gpslisteners);
		newlst.remove(gps);
		gpslisteners = newlst;
    }
    /* Add listener for GPS lifecycle */
    public static void addGPSLifecycleListener(GPSLifecycleListener gps) {
		ArrayList<GPSLifecycleListener> newlst = new ArrayList<GPSLifecycleListener>(gpslifelisteners);
		newlst.add(gps);
		gpslifelisteners = newlst;
    }
    /* Remove listener for GPS lifecycle */
    public static void removeGPSLifecycleListener(GPSLifecycleListener gps) {
		ArrayList<GPSLifecycleListener> newlst = new ArrayList<GPSLifecycleListener>(gpslifelisteners);
		newlst.remove(gps);
		gpslifelisteners = newlst;
    }
	/** Send GPS create event to listeners
	 * @param gps - new GPS
	 */
	public static void notifyGPSCreate(GPS gps) {
		/* Send to all lifecycle listeners */
        ArrayList<GPSLifecycleListener> lst = gpslifelisteners;
		for(int i = 0; i < lst.size(); i++) {
			lst.get(i).gpsLifecycleCreate(gps);
		}
		/* And to all status listeners (since they are extension of lifecycle) */
        ArrayList<GPSStatusListener> lst2 = gpslisteners;
		for(int i = 0; i < lst2.size(); i++) {
			lst2.get(i).gpsLifecycleCreate(gps);
		}
	}
	/** Send GPS delete event to listeners
	 * @param gps - new GPS
	 */
	public static void notifyGPSDelete(GPS gps) {
		/* Send to all lifecycle listeners */
        ArrayList<GPSLifecycleListener> lst = gpslifelisteners;
		for(int i = 0; i < lst.size(); i++) {
			lst.get(i).gpsLifecycleDelete(gps);
		}
		/* And to all status listeners (since they are extension of lifecycle) */
        ArrayList<GPSStatusListener> lst2 = gpslisteners;
		for(int i = 0; i < lst2.size(); i++) {
			lst2.get(i).gpsLifecycleDelete(gps);
		}
	}
	/** Send GPS data update event to listeners
	 * @param gps - updated GPS
	 * @param prev - previous GPSData
	 */
	public static void notifyGPSDataUpdate(GPS gps, GPSData prev) {
        ArrayList<GPSStatusListener> lst = gpslisteners;
		for(int i = 0; i < lst.size(); i++) {
			lst.get(i).gpsDataChanged(gps, prev);
		}
	}

    /*
     * Get configuration hashcode
     */
    public static String getConfigHash() {
        return config_hash;
    }
    /* Get data directory */
    public static File getDataDir() {
        return data_dir;
    }

    /* Get system configuration directory */
    public static File getSystemConfigDir() {
    	return system_config_dir;
    }
    
    /* Add listener for config updates */
    public static void addConfigUpdateListener(ConfigUpdateListener cul) {
        cfgupd.add(cul);
    }
    /* Remove listener for config updates */
    public static void removeConfigUpdateListener(ConfigUpdateListener cul) {
        cfgupd.remove(cul);
    }

    private static boolean dotaglog = false;
    private static PrintWriter   taglog = null;
    private static int          lastlogday = -1;
    private static int          lastlogsecflush = -1;
    private static SimpleDateFormat datetimefmt = new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss.SSS");
    private static SimpleDateFormat ymdfmt = new SimpleDateFormat(
        "yyyy-MM-dd");
    private static boolean dopdulog = false;
    private static PrintWriter   pdulog = null;
    private static int          lastpdulogday = -1;
    private static int          lastpdulogsecflush = -1;

    private static boolean dospmlog = false;
    private static PrintWriter spmlog = null;
    private static int          lastspmlogday = -1;
    private static int          lastspmlogsecflush = -1;
    
    private static boolean doreaderinitlog = false;
    private static boolean doverbosetagmode = false;

    private interface LogEvent {
    };

    private static class TagLogEvent implements LogEvent {
        String rdrid;
        String tagevt;
        long    ts;
    };
    private static class PDULogEvent implements LogEvent {
        String tagid;
        int[] msg;
        int alarmid;
        boolean alarmstate;
        String mtype;
        long    ts;
    };
    private static class SPMLogEvent implements LogEvent {
        String url;
        String doc;
        long    ts;
    };

    private static LinkedBlockingQueue<LogEvent> logqueue = new LinkedBlockingQueue<LogEvent>();

    private static class RangerLogger implements Runnable {
        public void run() {
            LogEvent le;
            Calendar cal = Calendar.getInstance();  /* Get timestamp */
            try {
                while((le = logqueue.take()) != null) {
                    if(le instanceof TagLogEvent) {
                        TagLogEvent tle = (TagLogEvent)le;
                        cal.setTimeInMillis(tle.ts);
                        doLogTagEvent(cal, tle.rdrid, tle.tagevt);
                    }
                    else if(le instanceof PDULogEvent) {
                        PDULogEvent ple = (PDULogEvent)le;
                        cal.setTimeInMillis(ple.ts);
                        doLogPDUTagMessage(cal, ple.tagid, ple.msg, ple.alarmid, ple.alarmstate, ple.mtype);
                    }
                    else if (le instanceof SPMLogEvent) {
                    	SPMLogEvent sle = (SPMLogEvent)le;
                    	cal.setTimeInMillis(sle.ts);
                    	doLogSPMMessage(cal, sle.url, sle.doc);
                    }
                }
			} catch (InterruptedException ix) {}
		}
    }

    public static void logTagEvent(String rdrid, StringBuilder tagevt) {
        if(!dotaglog) return;
        TagLogEvent tle = new TagLogEvent();
        tle.rdrid = rdrid;
        tle.tagevt = tagevt.toString();
        tle.ts = System.currentTimeMillis();
        logqueue.offer(tle);
    }        
    
    private static void doLogTagEvent(Calendar cal, String rdrid, String tagevt) {
        try {
            int day = cal.get(Calendar.DATE);
            int second = cal.get(Calendar.SECOND);
            if(day != lastlogday) {
                if(taglog != null) {
                    taglog.close();
                    taglog = null;
                }
            }
            if(taglog == null) {
                File f = new File(data_dir, "tagevents-" + ymdfmt.format(cal.getTime()) + ".log");
                taglog = new PrintWriter(new FileOutputStream(f, true));
                lastlogday = day;
            }
            taglog.println(datetimefmt.format(cal.getTime())+","+rdrid+","+tagevt);
            if (second != lastlogsecflush) {
                lastlogsecflush = second;
                taglog.flush();
            }
        } catch (IOException iox) {
            taglog = null;
        }
    }

	public static void logPDUTagMessage(String tagid, int[] msg, int alarmid, boolean alarmstate) {
		logPDUTagMessage(tagid, msg, alarmid, alarmstate, null);
	}
	public static void logPDUTagMessage(String tagid, int[] msg, int alarmid, boolean alarmstate, String mtype) {
		if(!dopdulog) return;
        PDULogEvent ple = new PDULogEvent();
        ple.tagid = tagid;
        ple.msg = msg.clone();
        ple.alarmid = alarmid;
        ple.alarmstate = alarmstate;
        ple.mtype = mtype;
        ple.ts = System.currentTimeMillis();
        logqueue.offer(ple);
    }

	public static void logSPMMessage(String url, Document doc) {
		if(!dospmlog) return;
        SPMLogEvent sle = new SPMLogEvent();
        sle.url = url;
        sle.ts = System.currentTimeMillis();
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource src = new DOMSource(doc);
			StreamResult rslt = new StreamResult(new StringWriter());
			t.transform(src, rslt);
			sle.doc = rslt.getWriter().toString();
		} catch (TransformerException tx) {
			sle.doc = "Error transforming XML:" + tx.getMessage();
		}
        logqueue.offer(sle);
    }

	private static void doLogPDUTagMessage(Calendar cal, String tagid, int[] msg, int alarmid, boolean alarmstate, String mtype) {
        try {
            int day = cal.get(Calendar.DATE);
            int sec = cal.get(Calendar.SECOND);
            if(day != lastpdulogday) {
                if(pdulog != null) {
                    pdulog.close();
                    pdulog = null;
                }
            }
            if(pdulog == null) {
                File f = new File(data_dir, "pdumsgs-" + ymdfmt.format(cal.getTime()) + ".log");
                pdulog = new PrintWriter(new FileOutputStream(f, true));
                lastpdulogday = day;
            }
			pdulog.print(datetimefmt.format(cal.getTime()) + ": tag=" + tagid);
			if(msg != null) {
				pdulog.print(", msg=\"");
				for(int i = 0; i < msg.length; i++) {
					pdulog.print(String.format("%02x", msg[i]));
				}
				pdulog.print("\"");
			}
			if(alarmid >= 0) {
				pdulog.print(", alarm=" + alarmid + ", state=" + alarmstate);
			}
			if(mtype != null) {
				pdulog.print(", mtype=\"" + mtype + "\"");
			}
            pdulog.println();
            if (lastpdulogsecflush != sec) {
                lastpdulogsecflush = sec;
                pdulog.flush();
            }
        } catch (IOException iox) {
            pdulog = null;
        }
	
	}

	private static void doLogSPMMessage(Calendar cal, String url, String doc) {
        try {
            int day = cal.get(Calendar.DATE);
            int sec = cal.get(Calendar.SECOND);
            if(day != lastspmlogday) {
                if(spmlog != null) {
                    spmlog.close();
                    spmlog = null;
                }
            }
            if(spmlog == null) {
                File f = new File(data_dir, "spmmsgs-" + ymdfmt.format(cal.getTime()) + ".log");
                spmlog = new PrintWriter(new FileOutputStream(f, true));
                lastspmlogday = day;
            }
			spmlog.println(datetimefmt.format(cal.getTime()) + ": url=" + url);
			if (doc != null) {
				spmlog.println(doc);
			}
            if (lastspmlogsecflush != sec) {
                lastspmlogsecflush = sec;
                spmlog.flush();
            }
        } catch (IOException iox) {
            spmlog = null;
        }
	
	}

    private static void checkLogging() {
        String do_log = cust_config.getProperty("tageventlog");
        if((do_log != null) && (do_log.equals("true"))) {
            dotaglog = true;
        }
        else {
            dotaglog = false;
            if(taglog != null) {
                taglog.close();
                taglog = null;
            }
        }
		do_log = cust_config.getProperty("pdumsglog");
        if((do_log != null) && (do_log.equals("true"))) {
            dopdulog = true;
        }
        else {
            dopdulog = false;
            if(pdulog != null) {
                pdulog.close();
                pdulog = null;
            }
        }
		do_log = cust_config.getProperty("readerinitlog");
        if((do_log != null) && (do_log.equals("true"))) {
            doreaderinitlog = true;
        }
        else {
            doreaderinitlog = false;
        }
        do_log = cust_config.getProperty("tagverbosemode");
        if((do_log != null) && (do_log.equals("true"))) {
            doverbosetagmode = true;
        }
        else {
            doverbosetagmode = false;
        }
		do_log = cust_config.getProperty("spmmsglog");
        if((do_log != null) && (do_log.equals("true"))) {
            dospmlog = true;
        }
        else {
            dospmlog = false;
            if(spmlog != null) {
                spmlog.close();
                spmlog = null;
            }
        }
    }
    /* Initialize the update event storage manager */
    private static void initUpdateEventStorage() {
        /* Create storage manager */
        evtstore = new UpdateEventStorageServer(new File(data_dir, "history"));
    }
    /**
     * Report event - used for storage
     */
    public static void reportEvent(UpdateEvent te) {
        evtstore.reportEvent(te);
    }

    /**
     * Read history for given range
     * @param start_time - start time (events after this time)
     * @param end_time - end time (events before or equal to this time)
     * @param maxcnt - recommended max events to return (may exceed to batch events with
     *      identical times)
     */
    public static LinkedList<UpdateEvent>    readEvents(long start_time, long end_time, int maxcnt) {
        return evtstore.readEvents(start_time, end_time, maxcnt);
    }

    private static class TagExpLocConfigListener implements ConfigUpdateListener {
        public void configUpdateNotification() {
            updateTagExpLocBump();
        }
    }
    /**
     * Update tag expected location bump
     */
    private static void updateTagExpLocBump() {
        LocationRuleEngine lre = LocationRuleEngine.getLocationRuleEngine();
        Properties cfg = RangerServer.getConfig();
        String tecb = cfg.getProperty(TAG_EXP_CONF_BUMP);
        if(tecb != null) {
            try {
                lre.setTagExpectedLocationBump(Double.parseDouble(tecb.trim()));
            } catch (NumberFormatException nfx) {
                error("Invalid parameter value for " +
                    TAG_EXP_CONF_BUMP + ": " + tecb);
            }
        }
    }
    /**
     * Load expected tag locations
     */
    private static void loadTagExpectedLocations() {
        LocationRuleEngine lre = LocationRuleEngine.getLocationRuleEngine();
        /* Initialize exp location bump */
        updateTagExpLocBump();
        /* Add listener for config changes */
        addConfigUpdateListener(new TagExpLocConfigListener());

        /* Now, list the files in the directory */
        File  file = new File(data_dir, TAGEXPLOC_FILENAME);
        Properties p = new Properties();
        FileInputStream r = null;
        try {
            r = new FileInputStream(file);
            p.load(r);
        } catch (IOException iox) {
            error("Error loading "
                + file.getAbsolutePath());
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (IOException iox) {
                }
                r = null;
            }
        }
        /* Enumerate the properties - format is TagGUID=loc1,loc2,... */
        for(String tid : p.stringPropertyNames()) {
            String v = p.getProperty(tid);  /* Get value */
            String[] locs = v.split(",");   /* Split into list */
            ArrayList<String> loclist = new ArrayList<String>();
            for(String s : locs) {
                s = s.trim();
                if(s.length() > 0)
                    loclist.add(s);
            }
            /* Set it */
            lre.setTagExpectedLocations(tid, loclist);
        }
    }
    /**
     * Save tag expected locations
     */
    public static void saveTagExpectedLocations() {
        LocationRuleEngine lre = LocationRuleEngine.getLocationRuleEngine();

        /* Build properties for object */
        Properties p = new Properties();
        /* Get tag IDs with expected location data */
        Set<String> exptags = lre.getTagsWithExpectedLocations();
        /* Loop through them */
        for(String tid : exptags) {
            StringBuilder sb = new StringBuilder();
            List<String> explist = lre.getTagExpectedLocations(tid);
            for(String s : explist) {   /* Build comma seperated list */
                if(sb.length() > 0)
                    sb.append(',');
                sb.append(s);
            }
            p.setProperty(tid, sb.toString());
        }
        /* Now write it to the file */
        File f = new File(data_dir, TAGEXPLOC_FILENAME);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "Tag expected locations");
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
    }

    /**
     * Save asset tags
     */
    public static void saveAssetTagsSet() {
        StringBuilder sb = new StringBuilder();
        for (String tid : Containers.assetTags().elements()) {
            sb.append(tid).append(',');
        }

        try (PrintWriter printWriter = new PrintWriter(new File(data_dir, ASSET_TAGS_SET_FILENAME))) {
            printWriter.write(sb.toString());
        } catch (FileNotFoundException e) {
            error(e.getMessage());
        }

        updateConfigHash();
    }

    /**
     * Load asset tags
     */
    public static void loadAssetTagsSet() {
        try (Scanner sc = new Scanner(new File(data_dir, ASSET_TAGS_SET_FILENAME))) {
            if (!sc.hasNext()) return;

            String tagsStr = sc.next();
            for (String tagId : tagsStr.split(",")) {
                Optional.ofNullable(tagId).ifPresent(Containers.assetTags()::put);
            }
        } catch (FileNotFoundException e) {
            error(e.getMessage());
        }
    }

    /**
     * Reset tag expected locations
     */
    public static void resetTagExpectedLocations() {
        LocationRuleEngine lre = LocationRuleEngine.getLocationRuleEngine();
        lre.resetTagExpectedLocations();
    }
	/**
	 * Format number object
	 */
	public static String formatNumber(Object o) {
		if(decfmt == null) {
            
			decfmt = new DecimalFormat();
			decfmt.setMinimumFractionDigits(0);
            decfmt.setMaximumFractionDigits(6);
			decfmt.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
			decfmt.setGroupingUsed(false);
		}
		if(o instanceof Double) {
            Double d = (Double)o;
            if(d.isInfinite() || d.isNaN())
                return null;
			return decfmt.format(((Double)o).doubleValue());
		}
		else if(o instanceof Float) {
            Float f = (Float)o;
            if(f.isInfinite() || f.isNaN())
                return null;
			return decfmt.format(((Float)o).floatValue());
		}
        else if(o instanceof Long) {
            if(((Long)o).longValue() == Long.MIN_VALUE) /* Null value indicator */
                return null;
            else
                return o.toString();
        }
		else {
			return o.toString();
		}
	}

	/**
	 * Save GPS Defaults
	 */
	public static void saveGPSDefaults() {
		/* Build properties */
		Properties p = new Properties();
		GPSDefaults gps = GPSDefaults.getDefault();	/* Get data */
		p.setProperty(GPSDefaults.GPS_DATASET, gps.getGPSDataSet());
		p.setProperty(GPSDefaults.GPS_MIN_PERIOD, Integer.toString(gps.getGPSMinPeriod()));
		p.setProperty(GPSDefaults.GPS_MIN_HORIZ, Double.toString(gps.getGPSMinHoriz()));
		p.setProperty(GPSDefaults.GPS_MIN_VERT, Double.toString(gps.getGPSMinVert()));

        /* Now write it to the file */
        File f = new File(data_dir, GPSDEF_FILENAME);
        FileOutputStream fw = null;
        try {
            fw = new FileOutputStream(f);
            p.store(fw, "GPS Default Configuration");
        } catch (IOException iox) {
            error("Error saving to " + f);
            return;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException iox) {
                }
                fw = null;
            }
        }
        updateConfigHash();
	}
	/**
	 * Load GPS defaults
	 */
	private static void loadGPSDefaults() {
        File config;

        if(data_dir != null) {
            /* Now load the gps default config */
            config = new File(data_dir, GPSDEF_FILENAME);
            if (config.exists()) {
				Properties p = new Properties();
                /* Now, load configuration file into properties object */
                FileInputStream r = null;
                try {
                    r = new FileInputStream(config);
                    p.load(r); /* Do load */
                } catch (IOException iox) {
                    error("Error loading configuration from "
                        + config.getName());
                } finally {
                    if (r != null) {
                        try {
                            r.close();
                        } catch (IOException iox) {
                        }
                        r = null;
                    }
                }
				/* Get default values */
				GPSDefaults orig = GPSDefaults.getDefault();
				double min_horiz = orig.getGPSMinHoriz();
				double min_vert = orig.getGPSMinVert();
				String data_set = orig.getGPSDataSet();
				int	min_period = orig.getGPSMinPeriod();
				/* Read settings */
				data_set = p.getProperty(GPSDefaults.GPS_DATASET, data_set);
				try {
					min_period = Integer.parseInt(p.getProperty(GPSDefaults.GPS_MIN_PERIOD, 
						Integer.toString(min_period)));
					min_horiz = Double.parseDouble(p.getProperty(GPSDefaults.GPS_MIN_HORIZ, 
						Double.toString(min_horiz)));
					min_vert = Double.parseDouble(p.getProperty(GPSDefaults.GPS_MIN_VERT, 
						Double.toString(min_vert)));
				} catch (NumberFormatException nfx) {
                    error("Invalid GPS defaults");
					return;
				}
				/* Apply changes */
				if(!GPSDefaults.replaceDefault(data_set, min_period, min_horiz, min_vert)) {
                    error("Invalid GPS defaults");
				}
            }
        }
	}
    /* Initialize key manager */
    private static void initializeKeyManager() {
    	/* load system configuration file to get keystore password configured by user */
    	loadSystemConfig();
    	
        try {
            keymgr = new KeyStoreManager(getSystemConfigDir(), getSystemConfig().getProperty("https.certPassword", 
                    KeyStoreManager.DEFAULT_PASSWORD));
        } catch (Exception x) {
            error("Error initializing key store manager - " + x);
            return;
        }
    }
    /* Get key store from key store manager */
    public static KeyStore getKeyStore() {
        if(keymgr != null)
            return keymgr.getKeyStore();
        return null;
    }

    private static final X509TrustManager X509 = new X509TrustManager() {
        public void checkClientTrusted(X509Certificate[] c, String s) throws CertificateException {
            //info("checkClientTrusted("  + s + ")");
        }
        public void checkServerTrusted(X509Certificate[] c, String s) throws CertificateException {
            //info("checkServerTrusted("  + s + ")");
        }
        public X509Certificate[] getAcceptedIssuers() {
            //info("getAcceptedIssuers()");
            return new X509Certificate[0];
        }
    };

    private static final X509TrustManager[] X509_MANAGERS = { X509 };
    /* Get SSL context */
    public static SSLContext getSSLContext() {
        try {
            SSLContext ctx = SSLContext.getInstance("SSLv3");
            ctx.init(null, X509_MANAGERS, null);
            return ctx;

        } catch (Exception x) {
            error("Error creating new SSL context - " + x);
        }
        return null;
    }
    /**
     * Test if reader initialization logging is active
     */
    public static boolean isReaderInitLoggingActive() {
        return doreaderinitlog;
    }
    /**
     * Test if verbose tag mode (disable exception mode on readers)
     */
    public static boolean isTagVerboseModeActive() {
        return doverbosetagmode;
    }
    private static Logger log = Logger.getLogger("ZoneManager");    

    public static Logger getLogger() { return log; }

    public static void info(String msg) { log.info(msg); }

    public static void error(String msg) { log.error(msg); }

    public static void setIsListenAssetTagsOnly(boolean isListenAssetTagsOnly) {
        RangerServer.isListenAssetTagsOnly = isListenAssetTagsOnly;
    }

    public static boolean isListenAssetTagsOnly() {
        return isListenAssetTagsOnly;
    }
}
