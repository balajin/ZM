package com.rfcode.ranger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.ReaderEntity;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Container class for update events
 * 
 * @author Mike Primm
 */
class UpdateEvent {
    String objid;
    String objtype;
    EventType evttype;
    Map<String, Object> attribs;
    Map<String, Object> full_attribs;
    long timestamp;

    private static final byte TYPE_INT = 0;
	private static final byte TYPE_LONG = 1;
	private static final byte TYPE_STRING = 2;
	private static final byte TYPE_DOUBLE = 3;
	private static final byte TYPE_BOOLEAN = 4;
	private static final byte TYPE_SET = 5;
	private static final byte TYPE_LIST = 6;
	private static final byte TYPE_MAP = 7;
	private static final byte TYPE_NULL = 8;
	private static final byte TYPE_RDRENTITY = 9;

	/* Serialize update event for storage in file */
	public void writeToStream(OutputStream out) throws IOException {
		DataOutputStream dout = new DataOutputStream(out);
		/* First, write the timestamp */
		dout.writeLong(timestamp);
		dout.writeUTF(evttype.getTypeStr());	/* Then event type string */
		dout.writeUTF((objtype != null)?objtype:"");	/* Object type */
		dout.writeUTF((objid != null)?objid:"");	/* Object ID */
		/* Now add attrib map */
		if(attribs != null) {
			dout.writeShort((short)attribs.size());	/* Write number of attributes */
			for(Map.Entry<String,Object> ent : attribs.entrySet()) {
				String k = ent.getKey();
				Object v = ent.getValue();
				if (v instanceof CustomFieldValue) {
					CustomFieldValue cfv = (CustomFieldValue) v;
					k = cfv.getField();
					v = cfv.getValue();
				}
				dout.writeUTF(k);	/* Write key */
				encodeValue(dout, v);
			}
		}
		else {	/* No attributes - save -1 for count */
			dout.writeShort(-1);
		}
		/* If full_attrib is dup of attribs, write length of -1 */
		if(full_attribs == attribs) {
			dout.writeShort(-1);
		}
		else {
			dout.writeShort((short)full_attribs.size());	/* Write number of attributes */
			for(Map.Entry<String,Object> fe : full_attribs.entrySet()) {
				String k = fe.getKey();
				Object v = fe.getValue();
				if (v instanceof CustomFieldValue) {
					CustomFieldValue cfv = (CustomFieldValue) v;
					k = cfv.getField();
					v = cfv.getValue();
				}
				dout.writeUTF(k);	/* Write key */
				encodeValue(dout, v);
			}
		}
		dout.flush();
		out.flush();
	}
	public void readFromStream(InputStream in) throws IOException {
		DataInputStream din = new DataInputStream(in);
		timestamp = din.readLong();	/* Read timestamp */
		evttype = EventType.getTypeByString(din.readUTF());
		objtype = din.readUTF(); if(objtype.equals("")) objtype = null;
		objid = din.readUTF(); if(objid.equals("")) objid = null;
		short cnt = din.readShort();	/* Get count for attribs */
		short i;
		if(cnt >= 0) {
			attribs = new HashMap<String,Object>();
			for(i = 0; i < cnt; i++) {
				String k = din.readUTF();		/* Get Key */
				attribs.put(k, decodeValue(din));
			}
		}
		else {
			attribs = null;
		}
		/* Read count for full attribs */
		cnt = din.readShort();
		if(cnt == -1) {	/* If -1, its same as attribs */
			full_attribs = attribs;
		}
		else {
			full_attribs = new HashMap<String,Object>();
			for(i = 0; i < cnt; i++) {
				String k = din.readUTF();		/* Get Key */
				full_attribs.put(k, decodeValue(din));
			}
		}
	}
	@SuppressWarnings("rawtypes")
	private void encodeValue(DataOutputStream dout, Object v) throws
		IOException {
		if(v == null) {
			dout.writeByte(TYPE_NULL);
		}
		else if(v instanceof String) {
			dout.writeByte(TYPE_STRING);
			dout.writeUTF((String)v);
		}
		else if(v instanceof Integer) {
			dout.writeByte(TYPE_INT);
			dout.writeInt((Integer)v);
		}
		else if(v instanceof Long) {
			dout.writeByte(TYPE_LONG);
			dout.writeLong((Long)v);
		}
		else if(v instanceof Double) {
			dout.writeByte(TYPE_DOUBLE);
			dout.writeDouble((Double)v);
		}
		else if(v instanceof ReaderEntity) {
			dout.writeByte(TYPE_RDRENTITY);
			dout.writeUTF(((ReaderEntity)v).getID());
		}
		else if(v instanceof Boolean) {
			dout.writeByte(TYPE_BOOLEAN);
			dout.writeBoolean((Boolean)v);
		}
		else if(v instanceof Set) {
			dout.writeByte(TYPE_SET);
			dout.writeShort((short)((Set)v).size());
			for(Object o : (Set)v) {
				encodeValue(dout, o);
			}
		}
		else if(v instanceof List) {
			dout.writeByte(TYPE_LIST);
			dout.writeShort((short)((List)v).size());
			for(Object o : (List)v) {
				encodeValue(dout, o);
			}
		}
		else if(v instanceof Map) {
			dout.writeByte(TYPE_MAP);
			Map m = (Map)v;
			dout.writeShort((short)m.size());
			for(Object k : m.keySet()) {
				dout.writeUTF(k.toString());
				encodeValue(dout, m.get(k));
			}
		}
		else {
			dout.writeByte(TYPE_STRING);
			dout.writeUTF(v.toString());
		}
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object decodeValue(DataInputStream din) throws IOException {
		short i, cnt;
		byte t = din.readByte();
		Object o;
		switch(t) {
			case TYPE_NULL:
        		return null;
   			case TYPE_STRING:
				return din.readUTF();
			case TYPE_INT:
				return new Integer(din.readInt());
			case TYPE_LONG:
				return new Long(din.readLong());
			case TYPE_DOUBLE:
				return new Double(din.readDouble());
			case TYPE_RDRENTITY:
				return din.readUTF();
			case TYPE_BOOLEAN:
				return Boolean.valueOf(din.readBoolean());
			case TYPE_SET:
				{
					Set s = new TreeSet();
					cnt = din.readShort();
					for(i = 0; i < cnt; i++) {
						o = decodeValue(din);
						s.add(o);
					}
					return s;
				}
			case TYPE_LIST:
				{
					List lst = new ArrayList();
					cnt = din.readShort();
					for(i = 0; i < cnt; i++) {
						o = decodeValue(din);
						lst.add(o);
					}
					return lst;
				}
			case TYPE_MAP:
				{
					Map m = new HashMap<String,Object>();
					cnt = din.readShort();
					for(i = 0; i < cnt; i++) {
						String k = din.readUTF();
						o = decodeValue(din);
						m.put(k, o);
					}
					return m;
				}
		}
		return null;
	}
}
