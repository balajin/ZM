package com.rfcode.ranger;

import java.io.*;
import java.security.*;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.codec.binary.Hex;

/**
 * Basic authentication filter for Zone Manager URLs, including static content
 *
 * @author Mike Primm
 */
public class RangerLoginModule implements Filter {

    /*
     * The following applies only to requests from the UI:
     * Authentication cookie value based on:
     * http://www.jasondavies.com/blog/2009/05/27/secure-cookie-authentication-couchdb/
     */

    // request attribute passed to servlets if user authenticates properly
    public static final String USERNAME_ATTRIBUTE = "com.rfcode.username";

    private static final String COOKIE_AUTH_SESSION = "AuthSession";

    private static final long COOKIE_TIMEOUT = 30 * 60 * 1000;

    // UI sends this header on all API requests
    private static final String HEADER_X_REQUESTED_WITH = "X-Requested-With";

    // value of the X-Requested-With header for UI requests
    private static final String XML_HTTP_REQUEST = "XMLHttpRequest";

    // The format for our authentication cookie - user:ts:hmac(user:ts)
    private static final Pattern COOKIE_PATTERN = Pattern.compile("^([A-Za-z0-9-_$]+):(\\d+):([a-f0-9]+)$");

    // Unauthenticated API requests (eg. from curl)
    private static final int STATE_UNAUTHENTICATED = 0;

    // All API requests
    private static final int STATE_AUTHENTICATED = 1;

    // API requests from the browser
    private static final int STATE_LOGIN_SUCCESS = 2;
    private static final int STATE_LOGIN_FAILURE = 3;
    private static final int STATE_LOGOUT = 4;

    // Mac used to generate the authentication cookie
    private SecretKey secret;

    public void init(FilterConfig filterConfig) throws ServletException {
        try {
            KeyGenerator kg = KeyGenerator.getInstance("HmacSHA1");
            secret = kg.generateKey();
        }
        catch (NoSuchAlgorithmException e) {
            throw new ServletException("Unable to find HmacSHA1 algorithm", e);
        }
    }

    public void doFilter ( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String uri = req.getRequestURI();
        String context = req.getContextPath();
        int state = STATE_UNAUTHENTICATED;

        /* If we've got users, do our own basic authentication */
        Map<String, RangerServer.User> users = RangerServer.getUsers();
        if (users.size() > 0) {
            String auth = req.getHeader("Authorization");

            if (uri.endsWith("/logout")) {
                state = STATE_LOGOUT;
            }
            else if (auth != null) {
                state = doBasicAuth(req, auth);
            }
            else if (uri.endsWith("/login")) {
                state = login(req, resp);
            }
            else {
                Cookie cookie = getCookie(req);
                if (cookie != null) {
                    state = handleCookie(req, resp, cookie);
                }
                else if (uri.endsWith("/index.jsp") ||
                         uri.equals(context + '/') ||
                         isAjax(req) ||
                         "true".equals(req.getParameter("__ui"))) {
                    state = STATE_LOGOUT;
                }
            }
        }
        else {
            // no users defined
            state = STATE_AUTHENTICATED;
            req.setAttribute(USERNAME_ATTRIBUTE, "");
        }

        switch (state) {
        case STATE_AUTHENTICATED:
            chain.doFilter(request, response);
            break;
        case STATE_UNAUTHENTICATED:
            resp.setHeader("WWW-Authenticate", "Basic realm=\""
                           + RangerServer.PRODUCTNAME + "\"");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            break;
        case STATE_LOGIN_SUCCESS:
            resp.sendRedirect(context + "/index.jsp");
            break;
        case STATE_LOGIN_FAILURE:
            logout(req, resp);
            resp.sendRedirect(context + "/login.jsp?login_error=1");
            break;
        case STATE_LOGOUT:
            logout(req, resp);
            if (isAjax(req)) {
                // UI will redirect to login.jsp
                resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
            else {
                resp.sendRedirect(req.getContextPath() + "/login.jsp");
            }
            break;
        }
    }

    private boolean isAjax(HttpServletRequest req) {
        return XML_HTTP_REQUEST.equals(req.getHeader(HEADER_X_REQUESTED_WITH));
    }

    private int doBasicAuth(HttpServletRequest req, String auth) throws IOException {
        int state = STATE_UNAUTHENTICATED;
        try {
            auth = auth.substring(auth.indexOf(" ")).trim();
            String decoded = ServletCommandInterface.decode_b64(auth);
            int i = decoded.indexOf(":");
            String username = decoded.substring(0, i);
            String password = decoded.substring(i + 1, decoded.length());

            if (isAuthenticated(username, password)) {
                state = STATE_AUTHENTICATED;
                req.setAttribute(USERNAME_ATTRIBUTE, username);
            }
        }
        catch (Exception x) {
        }
        return state;
    }

    /**
     * Handler for form-based browser login
     */
    private int login(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (isAuthenticated(username, password)) {
            setCookie(req, resp, username);
            return STATE_LOGIN_SUCCESS;
        }
        return STATE_LOGIN_FAILURE;
    }

    /**
     * Handler for a browser request containing the authentication cookie
     */
    private int handleCookie(HttpServletRequest req, HttpServletResponse resp, Cookie cookie)
        throws IOException, ServletException {
        String value = cookie.getValue();

        // cookie does not exist
        if (value == null) {
            return STATE_LOGOUT;
        }

        // check format
        Matcher matcher = COOKIE_PATTERN.matcher(cookie.getValue());
        if (!matcher.find()) {
            return STATE_LOGOUT;
        }

        // check HMAC
        String username = matcher.group(1);
        long ts = Long.parseLong(matcher.group(2));
        long elapsed = System.currentTimeMillis() - ts;
        String hmac = hmac(username, ts);

        if (elapsed > COOKIE_TIMEOUT || !hmac.equals(matcher.group(3))) {
            if (!req.getRequestURI().contains("/api/") || isAjax(req)) {
                return STATE_LOGOUT;
            }
            return STATE_UNAUTHENTICATED;
        }

        if (elapsed > COOKIE_TIMEOUT/2) {
            setCookie(req, resp, username);
        }
        req.setAttribute(USERNAME_ATTRIBUTE, username);
        return STATE_AUTHENTICATED;
    }

    private void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // cancel the cookie
        Cookie cookie = new Cookie(COOKIE_AUTH_SESSION, null);
        cookie.setMaxAge(0);
        cookie.setPath(req.getContextPath());
        cookie.setValue("");
        resp.addCookie(cookie);
    }

    private String hmac(String username, long ts) throws ServletException {
        try {
            String data = username + ':' + ts;
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secret);
            return new String(Hex.encodeHex(mac.doFinal(data.getBytes())));
        }
        catch (InvalidKeyException e) {
            throw new ServletException("Invalid key", e);
        }
        catch (NoSuchAlgorithmException e) {
            throw new ServletException("Unable to find HmacSHA1 algorithm", e);
        }
    }

    private boolean isAuthenticated(String username, String password) {
        if (username == null || password == null) {
            return false;
        }
        RangerServer.User usr = RangerServer.getUsers().get(username); /* Find username */
        return usr != null && usr.pwd_hash.equals(RangerServer.User.toHashString(password));
    }

    /**
     * Return the authentication cookie.
     */
    private Cookie getCookie(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (COOKIE_AUTH_SESSION.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * Set the authentication cookie.
     */
    private void setCookie(HttpServletRequest req, HttpServletResponse resp,
                           String username) throws IOException, ServletException {
        long ts = System.currentTimeMillis();
        String cvalue = username + ':' + ts + ':' + hmac(username, ts);
        Cookie cookie = getCookie(req);

        if (cookie == null) {
            cookie = new Cookie(COOKIE_AUTH_SESSION, cvalue);
        }
        else {
            cookie.setValue(cvalue);
        }
        cookie.setPath(req.getContextPath());
        resp.addCookie(cookie);
    }

    public void destroy() {
    }
}