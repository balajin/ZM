package com.rfcode.ranger;

/**
 * Command parameter error
 * 
 * @author Mike Primm
 */
public class CommandParameterException extends CommandException {
    static final long serialVersionUID = 98613917213965090L;
    /**
     * Command parameter exception
     * 
     * @param msg -
     *            error message
     */
    public CommandParameterException(String msg) {
        super(msg);
    }
}
