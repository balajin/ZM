package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Basic rule factory for average SSI threshold based zone determination.
 * 
 * @author Mike Primm
 */
public class AverageSSILocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "AverageSSIRule";
    /** Attribute ID - SSI minimum (integer) */
    public static final String ATTRIB_SSIMINIMUM = "ssiminimum";
    /** Attribute ID - SSI high-confidence (integer) */
    public static final String ATTRIB_SSIHIGHCONF = "ssihighconf";
    /** Attribute ID - SSI per-channel minimum (integer) */
    public static final String ATTRIB_SSICHANMINIMUM = "ssichanminimum";
    /** Default SSI minimum */
    public static final int DEFAULT_SSIMINIMUM = -100;
    /** Default high-confidence SSI */
    public static final int DEFAULT_SSIHIGHCONF = -60;
    /** Default SSI channel minimum */
    public static final int DEFAULT_SSICHANMINIMUM = -120;
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_SSIMINIMUM,
        ATTRIB_SSIHIGHCONF, ATTRIB_SSICHANMINIMUM, ATTRIB_CHANNELLIST};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {new Integer(DEFAULT_SSIMINIMUM),
        new Integer(DEFAULT_SSIHIGHCONF), new Integer(DEFAULT_SSICHANMINIMUM),
        new HashSet<ReaderChannel>()};
    /** Our label */
    public static final String LABEL = "Match by average SSI";
    /** Our description */
    public static final String DESC = "Matches tag with location based on threshold for average of SSI values for a set of one or more radios.";
    /* Map of our defaults */
    private static Map<String,Object> defmap = null;
    
    /**
     * Constructor
     */
    public AverageSSILocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new AverageSSILocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
