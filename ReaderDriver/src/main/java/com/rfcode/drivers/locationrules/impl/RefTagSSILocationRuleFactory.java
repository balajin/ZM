package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Reference tag based rule factory for SSI threshold based zone determination.
 * 
 * @author Mike Primm
 */
public class RefTagSSILocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "RefTagSSIRule";
    /** Attribute ID - Reference tag list (list of tag IDs) */
    public static final String ATTRIB_REFTAGS = "reftags";
    /** Attribute ID - Reference tag SSI minimum (integer) */
    public static final String ATTRIB_REFTAGSSIMINIMUM = "reftagssiminimum";
    /** Attribute ID - Reference tag matching time (integer) */
    public static final String ATTRIB_REFTAGMATCHTIME = "reftagmatchtime";
    /** Attribute ID - SSI minimum (integer) */
    public static final String ATTRIB_SSIMINIMUM = "ssiminimum";
    /** Attribute ID - SSI high-confidence (integer) */
    public static final String ATTRIB_SSIHIGHCONF = "ssihighconf";
    /** Default reference tag SSI minimum */
    public static final int DEFAULT_REFTAGSSIMINIMUM = -60;
    /** Default reference tag match time */
    public static final int DEFAULT_REFTAGMATCHTIME = 0;
    /** Default SSI minimum */
    public static final int DEFAULT_SSIMINIMUM = -90;
    /** Default high-confidence SSI */
    public static final int DEFAULT_SSIHIGHCONF = -60;
    /** Our attribute list */
    public static final String[] ATTRIBS = {
        ATTRIB_REFTAGS,
        ATTRIB_REFTAGSSIMINIMUM,
        ATTRIB_REFTAGMATCHTIME,
        ATTRIB_SSIMINIMUM,
        ATTRIB_SSIHIGHCONF};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {
        new ArrayList<String>(),
        Integer.valueOf(DEFAULT_REFTAGSSIMINIMUM),
        Integer.valueOf(DEFAULT_REFTAGMATCHTIME),                
        Integer.valueOf(DEFAULT_SSIMINIMUM),
        Integer.valueOf(DEFAULT_SSIHIGHCONF)};
    /** Our label */
    public static final String LABEL = "Match by SSI when near to reference tag";
    /** Our description */
    public static final String DESC = "Matches tag with location based on SSI threshold while reader is near reference tag for location.";
    /* Default value map */
    private static Map<String,Object> defmap = null;
    /**
     * Constructor
     */
    public RefTagSSILocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new RefTagSSILocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);    
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
