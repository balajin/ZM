package com.rfcode.drivers.locationrules;

import java.util.HashMap;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.locationrules.impl.LocationRuleExceptionImpl;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.ranger.RangerServer;

public class IRCodeManager {
	public enum ExceptionType {
		WC_TO_WC_CONFLICT("IRRuleWCtoWCConflict", "One or more rules matching the same IR Code as this rule exist."),
		WC_TO_CHAN_CONFLICT("IRRuleWCtoChanConflict", "One or more rules matching the same IR Code as this rule, but with reader channel lists defined, exist."),
		CHAN_TO_WC_CONFLICT("IRRuleChanToWCConflict", "One or more rules matching the same IR Code as this rule, but with no reader channel lists, exist."),
		CHAN_TO_CHAN_CONFLICT("IRRuleChanToChanConflict", "One or more rules matching the same IR Code, and having one or more of the same channels in their reader channel lists, exist.");
	
		public final String id; 
		public final String desc;
		ExceptionType(String id, String desc) {
			this.id = id;
			this.desc = desc;
		}
	}
	
	private static IRCodeManager inst = new IRCodeManager();
	
	public static IRCodeManager instance() {
		return inst;
	}

    private static class IRCodeContainer {
        String ircode;
        String[] wildcard_rules;    // List of rules matching all channels
        String[] chan_rules;	// List of rules matching specific channels
        HashMap<String, String[]> channel_rules;    // Lists of rules matching specific channels, keyed by channel
        static HashMap<String, LocationRuleExceptionImpl> exceptions = new HashMap<String, LocationRuleExceptionImpl>(); // Active exception, by rule+exception type
        
        public String getExceptionKey(String ruleid, ExceptionType excp) {
        	return ruleid + "_" + excp.id + "_" + ircode;
        }
        private void updateException(String ruleid, ExceptionType excepttyp, String[] associated, boolean iserr) {
        	String exceptid = getExceptionKey(ruleid, excepttyp);
        	// Get existing exception, if any
        	LocationRuleExceptionImpl lre = exceptions.get(exceptid);
        	if (iserr) {	// There should be an error
        		if (lre == null) {
        			lre = new LocationRuleExceptionImpl(ruleid, excepttyp.id, excepttyp.desc);
        			lre.setID(exceptid);
        			lre.setRulesRelatedToException(associated);
        			try {
						lre.init();
						exceptions.put(exceptid, lre);
						LocationRuleEngine.triggerRuleExceptionCreate(lre);
					} catch (DuplicateEntityIDException x) {
						RangerServer.info("Error creating exception " + exceptid + " - duplicate ID");
					} catch (BadParameterException x) {
						RangerServer.info("Error creating exception " + exceptid + " - bad parameter");
					}
        		}
        		else {	// Else, update existing
        			lre.setRulesRelatedToException(associated); // Only thing that can change
        			LocationRuleEngine.triggerRuleExceptionUpdate(lre);
        		}
        	}
        	else {
        		if (lre != null) {	// If exists, delete it
        			exceptions.remove(exceptid);
        			LocationRuleEngine.triggerRuleExceptionResolved(lre);
        			lre.cleanup();
        		}
        	}
        }
    }

    /* Map of used IR codes */
    private HashMap<String, IRCodeContainer> ircodes = new HashMap<String, IRCodeContainer>();        

    /* Map of IR containers by rule ID */
    private HashMap<String, IRCodeContainer> rules = new HashMap<String, IRCodeContainer>();

    private int findID(String[] list, String id) {
        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                if (list[i].equals(id)) {
                    return i;
                }
            }
        }
        return -1;
    }
    private String[] removeID(String[] list, String id) {
        int idx = findID(list, id);
        if (idx < 0) return list;

        if (list.length == 1) return null;
        String[] newlist = new String[list.length-1];
        for (int i = 0, j = 0; i < list.length; i++) {
            if (i != idx) {
                newlist[j] = list[i];
                j++;
            }
        } 
        return newlist;
    }
    private String[] addID(String[] list, String id) {
        int idx = findID(list, id);
        if (idx >= 0) return list;  // Already in list
        if (list == null) {
            return new String[] { id };
        }
        String[] newlist = new String[list.length + 1];
        System.arraycopy(list, 0, newlist, 0, list.length);
        newlist[list.length] = id;
        return newlist;
    }

    public void registerIRRule(String ruleid, String ircode, String[] channels) {
        ircode = ircode.trim();
        /* See if rule has existing container */
        IRCodeContainer icc = rules.get(ruleid);
        if (icc != null) {
        	if (!ircode.equals(icc.ircode)) { // If we're not same code as before
        		unregisterIRRule(ruleid); // Drop it
        		icc = null; // Start as if new
        	}
        	// Changed from wildcard to channel list
        	else if ((channels != null) && (findID(icc.wildcard_rules, ruleid) >= 0)) {
        		unregisterIRRule(ruleid); // Drop it
        		icc = null; // Start as if new
        	}
        	// Changed from channel list to wildcard
        	else if ((channels == null) && (findID(icc.wildcard_rules, ruleid) < 0)) {
        		unregisterIRRule(ruleid); // Drop it
        		icc = null; // Start as if new
        	}
        }
        if (icc == null) { // If no record, see if one for ir code
            icc = ircodes.get(ircode);
            if (icc == null) {  // Not found, make record
                icc = new IRCodeContainer();
                icc.ircode = ircode;
                ircodes.put(ircode, icc);
            }
            // And set for this rule
            rules.put(ruleid, icc);
        }
        // If wildcard, add to its list
        if (channels == null) {
            icc.wildcard_rules = addID(icc.wildcard_rules, ruleid);
            icc.chan_rules = removeID(icc.chan_rules, ruleid);
            if (icc.channel_rules != null) {
            	// Remove any specific channels
            	for (String id : icc.channel_rules.keySet()) {
            		String[] v = icc.channel_rules.get(id);
            		icc.channel_rules.put(id, removeID(v, ruleid));            
            	}
            }
        }
        else { // Else, channel specific
        	// Add to channel rules list
        	icc.chan_rules = addID(icc.chan_rules, ruleid);
            // Remove from wildcard list
            icc.wildcard_rules = removeID(icc.wildcard_rules, ruleid);
            // Add or remove from appropriate channel lists
            if (icc.channel_rules != null) {
            	for (String id : icc.channel_rules.keySet()) {
            		String[] v = icc.channel_rules.get(id);
            		if (findID(channels, id) >= 0) { // In channel list?
            			icc.channel_rules.put(id, addID(v, ruleid));            
            		}
            		else {
            			icc.channel_rules.put(id, removeID(v, ruleid));            
            		}
            	}
            }
            // Look for missing channels
            for (String ch : channels) {
            	String[] v = null;
            	if (icc.channel_rules == null)
            		icc.channel_rules = new HashMap<String, String[]>();
    			else
    				v = icc.channel_rules.get(ch);
            	if (v == null) {	// If no existing list for channel, add it
            		icc.channel_rules.put(ch, new String[] { ruleid });
            	}
            }
        }
        updateExceptions(icc);
    }
    public void unregisterIRRule(String ruleid) {
        /* See if rule has existing container */
        IRCodeContainer icc = rules.get(ruleid);
        if (icc == null) return;
        /* Now, see if any exceptions for this rule */
    	for (ExceptionType et : ExceptionType.values()) {
    		LocationRuleException exc = IRCodeContainer.exceptions.remove(icc.getExceptionKey(ruleid, et));
    		if (exc != null) {
    			LocationRuleEngine.triggerRuleExceptionResolved(exc);
    			exc.cleanup();
    		}
    	}
        /* Remove from all lists */
        icc.wildcard_rules = removeID(icc.wildcard_rules, ruleid);
        icc.chan_rules = removeID(icc.chan_rules, ruleid);
        if (icc.channel_rules != null) {
        	for (String id : icc.channel_rules.keySet()) {
        		String[] v = icc.channel_rules.get(id);
        		icc.channel_rules.put(id, removeID(v, ruleid));            
        	}
        }
        updateExceptions(icc);
    }

    
    private void updateExceptions(IRCodeContainer icc) {
    	boolean wc2wcerr = false;
    	boolean wc2chanerr = false;
    	// Go through wildcard rules
    	if (icc.wildcard_rules != null) {
    		wc2wcerr = (icc.wildcard_rules.length > 1);
    		for (String id : icc.wildcard_rules) {
    			icc.updateException(id, ExceptionType.WC_TO_WC_CONFLICT, icc.wildcard_rules, wc2wcerr);
    		}
    		// Process wc2chan errors
    		wc2chanerr = (icc.wildcard_rules.length > 0) && (icc.chan_rules != null);
    		for (String id : icc.wildcard_rules) {
    			icc.updateException(id, ExceptionType.WC_TO_CHAN_CONFLICT, icc.chan_rules, wc2chanerr);
    		}
    	}
    	// Go through channel rules
    	if (icc.chan_rules != null) {
    		// If we have wc2chan errors, we have chan2wc errors for everyone
    		for (String id : icc.chan_rules) {
    			icc.updateException(id, ExceptionType.CHAN_TO_WC_CONFLICT, icc.wildcard_rules, wc2chanerr);
    		}
    		// Finally, we have chan2chan for any channels with more than 1 rule
    		if (icc.channel_rules != null) {
    			HashMap<String, String[]> errrules = null;
    			// Figure out which rules have errors, and accumulate their associated IDs
    			for (String[] clist : icc.channel_rules.values()) {
					if (clist == null) continue;
					boolean iserr = (clist.length > 1);
					if (iserr) {
						if (errrules == null) {
							errrules = new HashMap<String, String[]>();
						}
						for (String id : clist) {
							String[] v = errrules.get(id); // Get existing associated, if any
							for (String id2 : clist) {
								v = addID(v, id2);
							}
							errrules.put(id, v);
						}
					}
    			}
    			for (String id : icc.chan_rules) {
    				String[] assoc = null;
    				if (errrules != null) {
    					assoc = errrules.get(id);
    				}
					icc.updateException(id, ExceptionType.CHAN_TO_CHAN_CONFLICT, assoc, assoc != null);
    			}
    		}
    	}
    }
}

