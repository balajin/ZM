package com.rfcode.drivers.locationrules.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;

import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.locationrules.LocationRuleWithResultFeedback;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;

/**
 * Basic rule factory for portal-based location detection, based on having one
 * or more "inside the door" channels, and one or more "outside the door"
 * channels. If an inside channel matches better than an outside channel, the
 * tag is reported to match (with similar confidence to SimpleSSIRule). If
 * outside matches better, rule does not match. If neither matches, and the
 * previous report was a match ("inside"), and low confidence match is reported.
 * 
 * @author Mike Primm
 */
public class PortalSSILocationRule extends AbstractLocationRule implements
    LocationRuleWithResultFeedback {
    private ArrayList<RuleTrigger> in_channel_triggers;
    private ArrayList<RuleTrigger> out_channel_triggers;
    private int ssi_minimum;
    private int ssi_highconf;
    private double inside_unreported_conf;
    private Set<ReaderChannel> in_chanlist;
    private Set<ReaderChannel> out_chanlist;
    /* Confidence at high confidence SSI */
    private static final double CONF_FOR_HIGH = 0.9;
    /* Confidence at max SSI (assume 40dBm above high level) */
    private static final double CONF_FOR_MAX = 0.95;
    private static final int DBM_MAX_ABOVE_HIGH = 40;
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    PortalSSILocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set channel triggers, based on our settings
     */
    private void setChannelTriggers() {
        LocationRuleEngine eng = getLocationRuleFactory()
            .getLocationRuleEngine();
        /* If we're sensitive to which radios, add listeners for those radios */
        if ((in_chanlist != null) && (in_chanlist.size() > 0)) {
            in_channel_triggers = new ArrayList<RuleTrigger>();
            for (ReaderChannel chan : in_chanlist) {
                RuleTrigger rt = eng.registerRuleTriggerByChannel(this, chan);
                in_channel_triggers.add(rt);
            }
        }
        if ((out_chanlist != null) && (out_chanlist.size() > 0)) {
            out_channel_triggers = new ArrayList<RuleTrigger>();
            for (ReaderChannel chan : out_chanlist) {
                RuleTrigger rt = eng.registerRuleTriggerByChannel(this, chan);
                out_channel_triggers.add(rt);
            }
        }
    }
    /**
     * Clean up existing channel triggers
     */
    private void cleanupChannelTriggers() {
        /* If channel triggers defined, clean up */
        if (in_channel_triggers != null) {
            for (RuleTrigger rt : in_channel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            in_channel_triggers.clear();
            in_channel_triggers = null;
        }
        if (out_channel_triggers != null) {
            for (RuleTrigger rt : out_channel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            out_channel_triggers.clear();
            out_channel_triggers = null;
        }
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        int newmin;
        int newhigh;
        double newconf;
        /* Get SSI minimum value */
        Object v = attr.get(PortalSSILocationRuleFactory.ATTRIB_SSIMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI minimum not set");
        }
        newmin = ((Integer) v).intValue();
        /* Get SSI high confidence value */
        v = attr.get(PortalSSILocationRuleFactory.ATTRIB_SSIHIGHCONF);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI high confidence not set");
        }
        newhigh = ((Integer) v).intValue();
        /* If high confidence is lower that minimum, bad */
        if (newhigh < newmin) {
            throw new BadParameterException(
                "SSI high confidence is below SSI miniumum");
        }
        /* Get inside-but-unreported confidence value */
        v = attr.get(PortalSSILocationRuleFactory.ATTRIB_INSIDEUNREPORTEDCONF);
        if ((v == null) || (!(v instanceof Double))) {
            throw new BadParameterException(
                "Unreported last matching confidence not set");
        }
        newconf = ((Double) v).doubleValue();
        /* If out of range, report error */
        if ((newconf < 0.0) || (newconf > 0.5)) {
            throw new BadParameterException(
                "Invalid value for unreported last matching confidence");
        }
        /* Check for channel list */
        v = attr.get(PortalSSILocationRuleFactory.ATTRIB_INSIDECHANNELLIST);
        Set<ReaderChannel> new_in_chanlist = null;
        if (v != null) {
            if (!(v instanceof Set)) {
                throw new BadParameterException("Inside channel set wrong type");
            }
            new_in_chanlist = new HashSet<ReaderChannel>();
            for (Object o : ((Set<?>) v)) {
                if (o instanceof ReaderChannel)
                    new_in_chanlist.add((ReaderChannel) o);
                else
                    throw new BadParameterException("Channel wrong type in set");
            }
        }
        if (in_chanlist != null) { /* Don't assert on default set */
            if ((v == null) || (((Set<?>) v).size() == 0))
                throw new BadParameterException(
                    "Inside channel list must have at least one channel");
        }
        /* Check for channel list */
        v = attr.get(PortalSSILocationRuleFactory.ATTRIB_OUTSIDECHANNELLIST);
        Set<ReaderChannel> new_out_chanlist = null;
        if (v != null) {
            if (!(v instanceof Set)) {
                throw new BadParameterException(
                    "Outside channel set wrong type");
            }
            new_out_chanlist = new HashSet<ReaderChannel>();
            for (Object o : ((Set<?>) v)) {
                if (o instanceof ReaderChannel)
                    new_out_chanlist.add((ReaderChannel) o);
                else
                    throw new BadParameterException("Channel wrong type in set");
            }
        }
        if (out_chanlist != null) { /* Don't assert on default set */
            if ((v == null) || (((Set<?>) v).size() == 0))
                throw new BadParameterException(
                    "Outside channel list must have at least one channel");
        }
        in_chanlist = new_in_chanlist;
        out_chanlist = new_out_chanlist;
        ssi_minimum = newmin;
        ssi_highconf = newhigh;
        inside_unreported_conf = newconf;
        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        if (enable) { /* If we're enabled, set triggers */
            /*
             * If we're sensitive to which radios, add listeners for those
             * radios
             */
            setChannelTriggers();
        } else { /* Else, disabled - clean up triggers */
            cleanupChannelTriggers();
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        /*
         * Get the channels for the tag, and see find the max of those that are
         * in our list
         */
        int best_ssi = Integer.MIN_VALUE;
        boolean best_outside = true;
        Map<String, TagLink> taglinks = tag.getTagLinks();
        /* Check inside channels first */
        for (ReaderChannel c : in_chanlist) {
            TagLink t = taglinks.get(c.getID()); /* See if in active link set */
            if (t != null) {
                Object v = t.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                if ((v != null) && (v instanceof Integer)) {
                    int ssi = ((Integer) v).intValue(); /* Get value */
                    if (ssi > best_ssi) {
                        best_ssi = ssi; /* See if new best */
                        best_outside = false; /* Match inside */
                    }
                }
            }
        }
        /* Check outside channels second */
        for (ReaderChannel c : out_chanlist) {
            TagLink t = taglinks.get(c.getID()); /* See if in active link set */
            if (t != null) {
                Object v = t.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                if ((v != null) && (v instanceof Integer)) {
                    int ssi = ((Integer) v).intValue(); /* Get value */
                    if (ssi > best_ssi) {
                        best_ssi = ssi; /* See if new best */
                        best_outside = true; /* Match outside */
                    }
                }
            }
        }
        double conf = LocationRule.NOMATCH;
        /* If best is outside, and we're above minimum, we're outside */
        if (best_outside) {
            /* If not a match on the outside channel AND we were matching before */
            if ((best_ssi <= ssi_minimum) && (old_conf > 0.0)) {
                conf = inside_unreported_conf; /* Low confidence we still are */
            }
        } else { /* Else, inside is matching candidate */
            /* Now, compute confidence based on best SSI */
            if (best_ssi <= ssi_minimum) { /* If below minimum */
                conf = LocationRule.NOMATCH;
            } else if (best_ssi >= (ssi_highconf + DBM_MAX_ABOVE_HIGH)) { /* Max */
                conf = CONF_FOR_MAX;
            } else if (best_ssi >= ssi_highconf) { /* If above high-conf */
                conf = CONF_FOR_HIGH
                    + ((CONF_FOR_MAX - CONF_FOR_HIGH)
                        * (double) (best_ssi - ssi_highconf) / (double) DBM_MAX_ABOVE_HIGH);
            } else { /* Between min and high */
                conf = CONF_FOR_HIGH * (double) (best_ssi - ssi_minimum)
                    / (double) (ssi_highconf - ssi_minimum);
            }
            /* If we were matching before, inside conf is lower bound */
            if (old_conf > 0.0) {
                if (conf < inside_unreported_conf)
                    conf = inside_unreported_conf;
            }
        }
        return conf;
    }
    /**
     * Report location result change for given tag
     * 
     * @param tag -
     *            tag
     * @param loc -
     *            new matching location
     * @param old_conf -
     *            old confidence for our rule
     * @return new confidence for our rule
     */
    public double reportLocationResult(Tag tag, Location loc, double old_conf) {
        double conf = old_conf; /* Assume no change */

        /* If above our assumption level, no revision */
        if (conf <= (inside_unreported_conf+0.000000001)) {
            Location our_loc = this.getRuleTargetLocation();
            if (loc != our_loc) { /* Not our location? */
                /* See if match is not a child of our location, need to revise */
                while (loc != null) {
                    loc = loc.getParent();
                    if (loc == our_loc)
                        break;
                }
                if (loc != our_loc) { /* Not a child */
                    conf = LocationRule.NOMATCH;
                }
            }
        }
        return conf;
    }
}
