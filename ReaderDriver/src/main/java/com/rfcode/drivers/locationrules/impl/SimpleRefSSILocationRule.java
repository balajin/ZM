package com.rfcode.drivers.locationrules.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;

/**
 * Basic SSI threshold based location rule, with thresholds based on one or more
 * reference tags. This rule monitors one or more
 * radios, and takes the strongest SSI value among them to produce a confidence
 * versus a cutoff threshold (0.0 match) and a high confidence threshold (0.9
 * match).  The cutoff and high confidence thresholds are based on the maximum of
 * a configured base value and the highest SSI reported by one or more reference
 * tags.
 * 
 * @author Mike Primm
 */
public class SimpleRefSSILocationRule extends AbstractLocationRule {
    private ArrayList<RuleTrigger> our_channel_triggers;
    private int ssi_minimum;
    private int ssi_highconf;
    private int ssi_base_val;   /* Base value - minimum reference SSI */
    private String[] reftags;   /* IDs of reference tags */
    private int[] reftagssi;    /* Current SSI for reference tags */
    private int cur_base_val;   /* Current base - max of ref SSIs and ssi_base_val */
    private Set<ReaderChannel> chanlist;
    private Set<Tag> assoc_tags = new HashSet<Tag>();    /* Associated tags (matching or almost matching) */
    /* Confidence at high confidence SSI */
    private static final double CONF_FOR_HIGH = 0.9;
    /* Confidence at max SSI (assume 40dBm above high level) */
    private static final double CONF_FOR_MAX = 0.95;
    private static final int DBM_MAX_ABOVE_HIGH = 40;
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    SimpleRefSSILocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set channel triggers, based on our settings
     */
    private void setChannelTriggers() {
        LocationRuleEngine eng = getLocationRuleFactory()
            .getLocationRuleEngine();
        /* If we're sensitive to which radios, add listeners for those radios */
        if ((chanlist != null) && (chanlist.size() > 0)) {
            our_channel_triggers = new ArrayList<RuleTrigger>();
            for (ReaderChannel chan : chanlist) {
                RuleTrigger rt = eng.registerRuleTriggerByChannel(this, chan);
                our_channel_triggers.add(rt);
            }
        }
    }
    /**
     * Clean up existing channel triggers
     */
    private void cleanupChannelTriggers() {
        /* If channel triggers defined, clean up */
        if (our_channel_triggers != null) {
            for (RuleTrigger rt : our_channel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            our_channel_triggers.clear();
            our_channel_triggers = null;
        }
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        int newmin;
        int newhigh;
        int newbase;
        /* Get SSI minimum value */
        Object v = attr.get(SimpleRefSSILocationRuleFactory.ATTRIB_OFFSSIMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("Relative SSI minimum not set");
        }
        newmin = ((Integer) v).intValue();
        /* Get SSI high confidence value */
        v = attr.get(SimpleRefSSILocationRuleFactory.ATTRIB_OFFSSIHIGHCONF);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("Relative SSI high confidence not set");
        }
        newhigh = ((Integer) v).intValue();
        /* Get SSI base value */
        v = attr.get(SimpleRefSSILocationRuleFactory.ATTRIB_REFMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("Minimum reference SSI not set");
        }
        newbase = ((Integer) v).intValue();
        /* If high confidence is lower that minimum, bad */
        if (newhigh < newmin) {
            throw new BadParameterException(
                "Relative SSI high confidence is below relative SSI miniumum");
        }
        /* Check for reference tag list */
        v = attr.get(SimpleRefSSILocationRuleFactory.ATTRIB_REFTAGS);
        Set<String> new_reftaglist = null;
        if (v != null) {
            if (!(v instanceof List)) {
                throw new BadParameterException("Reference tag set wrong type");
            }
            new_reftaglist = new HashSet<String>();
            for(Object o : ((List<?>)v)) {
                if(o instanceof String)
                    new_reftaglist.add((String)o);
                else
                    throw new BadParameterException("Reference tag ID - wrong type in set");
            }
        }
        if (reftags != null) { /* Don't assert on default set */
            if ((v == null) || (((List<?>) v).size() == 0))
                throw new BadParameterException(
                    "Reference tag list must have at least one tag");
        }
        /* Check for channel list */
        v = attr.get(SimpleRefSSILocationRuleFactory.ATTRIB_CHANNELLIST);
        Set<ReaderChannel> new_chanlist = null;
        if (v != null) {
            if (!(v instanceof Set)) {
                throw new BadParameterException("Channel set wrong type");
            }
            new_chanlist = new HashSet<ReaderChannel>();
            for(Object o : ((Set<?>)v)) {
                if(o instanceof ReaderChannel)
                    new_chanlist.add((ReaderChannel)o);
                else
                    throw new BadParameterException("Channel wrong type in set");
            }
        }
        if (chanlist != null) { /* Don't assert on default set */
            if ((v == null) || (((Set<?>) v).size() == 0))
                throw new BadParameterException(
                    "Channel list must have at least one channel");
        }
        chanlist = new_chanlist;
        reftags = new String[new_reftaglist.size()];
        new_reftaglist.toArray(reftags);
        reftagssi = new int[reftags.length];        /* Reset reftag SSI values */
        for(int i = 0; i < reftagssi.length; i++)
            reftagssi[i] = Integer.MIN_VALUE;
        ssi_minimum = newmin;
        ssi_highconf = newhigh;
        ssi_base_val = newbase;
        cur_base_val = newbase; /* Default to min base until we get ref tag SSIs */
        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        if (enable) { /* If we're enabled, set triggers */
            /*
             * If we're sensitive to which radios, add listeners for those
             * radios
             */
            setChannelTriggers();
        } else { /* Else, disabled - clean up triggers */
            cleanupChannelTriggers();
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        /*
         * Get the channels for the tag, and see find the max of those that are
         * in our list
         */
        int best_ssi = Integer.MIN_VALUE;
        Map<String, TagLink> taglinks = tag.getTagLinks();
        for (ReaderChannel c : chanlist) {
            TagLink t = taglinks.get(c.getID()); /* See if in active link set */
            if (t != null) {
                Object v = t.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                if ((v != null) && (v instanceof Integer)) {
                    int ssi = ((Integer) v).intValue(); /* Get value */
                    if (ssi > best_ssi)
                        best_ssi = ssi; /* See if new best */
                }
            }
        }
        /* Next, see if its a reference tag */
        String id = tag.getTagGUID();
        int idx;
        boolean is_ref = false;
        for(idx = 0; idx < reftags.length; idx++) {
            if(id.equals(reftags[idx])) {
                is_ref = true;
                break;
            }
        }
        /* If its a reference tag, need to see if we need to adjust base */
        if(is_ref) {
            /* If we changed AND were best, or we went up AND beat best,
             * base SSI will be recomputed */
            if(((best_ssi != reftagssi[idx]) && (reftagssi[idx] == cur_base_val)) ||
                ((best_ssi > reftagssi[idx]) && (best_ssi > cur_base_val))) {
                int newmax = ssi_base_val;  /* Start with base (this is min) */
                for(int i = 0; i < reftagssi.length; i++) { /* Find new max */
                    if(reftagssi[i] > newmax) newmax = reftagssi[i];
                }
                if(newmax != cur_base_val) {    /* If changed, update and evaluate */
                    LocationRuleEngine eng = getLocationRuleFactory()
                        .getLocationRuleEngine();
                    cur_base_val = newmax;      /* Save new base */
                    /* Kick all our matching tags to be re-evaluated */
                    for(Tag t : assoc_tags) {
                        if(t.isValid())
                            eng.triggerRule(t, this);
                    }
                }
            }
            reftagssi[idx] = best_ssi;      /* Update our value, in any case */

            /* Also, ref tag is match, by definition */
            return CONF_FOR_MAX;
        }
        /* If we match or almost match, make sure we remember tag (in case ref moves) */
        if(best_ssi < (ssi_minimum+ssi_base_val)) { /* Too low to be interesting? */
            assoc_tags.remove(tag);
        }
        else {      /* Else, its of interest (could match or start matching if ref changes) */
            assoc_tags.add(tag);
        }
        /* Now, compute confidence based on best SSI offset from base */
        best_ssi = best_ssi - cur_base_val;
        if (best_ssi <= ssi_minimum) { /* If below minimum */
            return LocationRule.NOMATCH;
        } else if (best_ssi >= (ssi_highconf + DBM_MAX_ABOVE_HIGH)) { /* Max */
            return CONF_FOR_MAX;
        } else if (best_ssi >= ssi_highconf) { /* If above high-conf */
            return CONF_FOR_HIGH
                + ((CONF_FOR_MAX - CONF_FOR_HIGH)
                    * (double) (best_ssi - ssi_highconf) / (double) DBM_MAX_ABOVE_HIGH);
        } else { /* Between min and high */
            return CONF_FOR_HIGH * (double) (best_ssi - ssi_minimum)
                / (double) (ssi_highconf - ssi_minimum);
        }
    }
}
