package com.rfcode.drivers.locationrules;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.BadParameterException;


/**
 * Abstract base class for location rules - provides mechanisms for maintaining
 * lookup-up-by-ID features and other common features.
 * 
 * @author Mike Primm
 */
public abstract class AbstractLocationRule implements LocationRule {
    /* Rule ID */
    private String ruleid;
    /* Target location */
    private Location tgt_location;
    /* Enabled? */
    private boolean enabled;
    /* Init done? */
    private boolean did_init;
    /* Location rule factory */
    private LocationRuleFactory factory;
    /* Attribute values */
    private Map<String, Object> attribs;
    /**
     * Constructor
     * 
     * @param f -
     *            rule factory
     */
    protected AbstractLocationRule(LocationRuleFactory f) {
        factory = f;
        enabled = false;
        did_init = false;
        try {
			if(f != null)
	            setLocationRuleAttributes(f.getDefaultLocationRuleAttributes());
        } catch (BadParameterException bpx) {
        }
    }
    /**
     * Set location rule ID (unique)
     * 
     * @param id -
     *            location rule ID
     */
    public void setID(String id) {
        ruleid = id;
    }
    /**
     * Get rule's unique ID
     * 
     * @return id
     */
    public String getID() {
        return ruleid;
    }
    /**
     * Initialization method - must be called once attributes set
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        if (ruleid == null) {
            throw new BadParameterException("Location Rule ID not set");
        }
        /* Need valid target location */
        if ((tgt_location == null) && !(this instanceof GPSRule)) {
            throw new BadParameterException("Target location not set");
        }
        ReaderEntityDirectory.addEntity(this);
        did_init = true; /* Init completed */
        /* If enabled, set the rule triggers */
        if (enabled) {
            setRuleEnabled(false); /* Force re-enable with init ready */
            setRuleEnabled(true);
        }
    }
    /**
     * Cleanup method
     */
    public void cleanup() {
        setRuleEnabled(false); /* Disable the rule */
        if (ruleid != null) {
            ReaderEntityDirectory.removeEntity(this);
        }
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        /* Make copy for ourselves */
        HashMap<String, Object> newattribs = new HashMap<String, Object>(attr);
        /* Save new values */
        attribs = newattribs;
        /* If we're active, need to reset listeners */
        if (isInitDone() && getRuleEnabled()) {
            setRuleEnabled(false); /* Disable rule to clear triggers */
            setRuleEnabled(true); /* And re-enable to reset them */
        }
    }
    /**
     * Get current value of attributes for the location rule. The set returned
     * will include values for all attribute IDs indicated by the
     * getLocationRuleAttributeIDs() method of the rule's factory.
     * 
     * @return map of attributes, keyed by attribute ID
     */
    public Map<String, Object> getLocationRuleAttributes() {
        return Collections.unmodifiableMap(attribs);
    }
    /**
     * Get the location rule's factory
     * 
     * @return factory
     */
    public LocationRuleFactory getLocationRuleFactory() {
        return factory;
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (enabled == enable)
            return;
        enabled = enable;
        if(!enabled)    /* If set to disabled, trigger rules to scrub contributions */
            LocationRuleEngine.getLocationRuleEngine().triggerAllTagsByRule(this);
    }
    /**
     * Get rule enabled/disabled
     * 
     * @return enable setting
     */
    public boolean getRuleEnabled() {
        return enabled;
    }
    /**
     * Get the rule's target location
     * 
     * @return target location
     */
    public Location getRuleTargetLocation() {
        return tgt_location;
    }
    /**
     * Set the rule's target location
     * 
     * @param tgt -
     *            target location
     */
    public void setRuleTargetLocation(Location tgt) {
        tgt_location = tgt;
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public abstract double evaluateRuleForTag(Tag tag, double old_conf);
    /**
     * Init done?
     */
    protected boolean isInitDone() {
        return did_init;
    }
	/**
	 * To string - use rule ID
	 */
	public String toString() { 
		return ruleid;
	}
	/**
	 * Two rules with the same ruleid are equal
	 */
	public boolean equals(Object o) {
		if((o != null) && (o instanceof LocationRule) && (ruleid != null))
			return ruleid.equals( ((LocationRule)o).getID() );
		else
			return false;
	}
	/*
	 * Use ID as hashcode
	 */
	public int hashCode() {
		if(ruleid != null)
			return ruleid.hashCode();
		else
			return -1;
	}
    /**
     * Test if rule is IR domain determining rule (usually just IR rules do this)
     */
    public boolean isIRDomainDetermining() {
        return false;
    }
    /**
     * Test if rule overrides location update delay
     */
    public boolean overrideLocationUpdateDelay() {
    	return false;
    }

}
