package com.rfcode.drivers.locationrules;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.BadParameterException;

/**
 * Abstract base class for location rule factories. Primarily provides default
 * methods and simple lookup-by-id machanisms.
 * 
 * @author Mike Primm
 * 
 */
public abstract class AbstractLocationRuleFactory implements
    LocationRuleFactory {
    private LocationRuleEngine engine;
    /**
     * Constructor
     */
    protected AbstractLocationRuleFactory() {
    }
    /**
     * Initialize the location rule factory - registers new attributes with
     * TagTypes and the like.
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        if (engine == null)
            throw new BadParameterException("LocationRuleEngine not set");
        ReaderEntityDirectory.addEntity(this);
    }
    /**
     * Cleanup location rule factory
     */
    public void cleanup() {
        ReaderEntityDirectory.removeEntity(this);
    }
    /**
     * Get factory ID
     * 
     * @return factory ID
     */
    public abstract String getID();
    /**
     * Set factory ID
     * 
     * @param id -
     *            factory ID
     */
    public void setID(String id) {
    }
    /**
     * Set engine
     * 
     * @param eng -
     *            our LocationRuleEngine
     */
    public void setLocationRuleEngine(LocationRuleEngine eng) {
        engine = eng;
    }
    /**
     * Get engine
     * 
     * @return our LocationRuleEngine
     */
    public LocationRuleEngine getLocationRuleEngine() {
        return engine;
    }
    
}
