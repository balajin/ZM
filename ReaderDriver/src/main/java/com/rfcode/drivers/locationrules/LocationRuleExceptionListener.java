package com.rfcode.drivers.locationrules;

public interface LocationRuleExceptionListener {
	/**
	 * New exception has occurred
	 */
	public void locationRuleExceptionCreated(LocationRuleException lrx);
	/**
	 * Exist exception has been updated
	 */
	public void locationRuleExceptionUpdated(LocationRuleException lrx);
	/**
	 * Exception has been resolved
	 */
	public void locationRuleExceptionResolved(LocationRuleException lrx);
}
