package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Basic rule factory for simple SSI threshold based zone determination.  Uses
 * values relative to one or more reference tags.
 * 
 * @author Mike Primm
 */
public class SimpleRefSSILocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "SimpleRefSSIRule";
    /** Attribute ID - SSI minimum (integer) offset from ref */
    public static final String ATTRIB_OFFSSIMINIMUM = "offssiminimum";
    /** Attribute ID - SSI high-confidence (integer) offset from ref */
    public static final String ATTRIB_OFFSSIHIGHCONF = "offssihighconf";
    /** Attribute ID - SSI reference minimum (integer) */
    public static final String ATTRIB_REFMINIMUM = "refminimum";    
    /** Attribute ID - Reference tag list (set of tag IDs) */
    public static final String ATTRIB_REFTAGS = "reftags";
    /** Default SSI minimum offset */
    public static final int DEFAULT_OFFSSIMINIMUM = -20;
    /** Default high-confidence SSI offset */
    public static final int DEFAULT_OFFSSIHIGHCONF = 10;
    /** Default SSI reference minimum (integer) */
    public static final int DEFAULT_REFMINIMUM = -70;
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_OFFSSIMINIMUM,
        ATTRIB_OFFSSIHIGHCONF, ATTRIB_REFMINIMUM, 
        ATTRIB_REFTAGS,
        ATTRIB_CHANNELLIST};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {
        Integer.valueOf(DEFAULT_OFFSSIMINIMUM),
        Integer.valueOf(DEFAULT_OFFSSIHIGHCONF),
        Integer.valueOf(DEFAULT_REFMINIMUM),  
        new ArrayList<String>(),
        new HashSet<ReaderChannel>()};
    /** Our label */
    public static final String LABEL = "Match by strongest SSI, relative to reference tags";
    /** Our description */
    public static final String DESC = "Matches tag with location based on SSI threshold for a set of one or more radios, using thresholds relative to reference tags.";
    /* Default value map */
    private static Map<String,Object> defmap = null;
    /**
     * Constructor
     */
    public SimpleRefSSILocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new SimpleRefSSILocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);    
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
