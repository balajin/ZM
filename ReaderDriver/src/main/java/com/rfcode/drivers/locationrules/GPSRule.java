package com.rfcode.drivers.locationrules;

import com.rfcode.drivers.readers.GPS;

/**
 * Interface representing an instance of a rule for GPS correlation (special location rule)
 * 
 * @author Mike Primm
 */
public interface GPSRule extends LocationRule {
    /**
     * Get the rule's target GPS
     * 
     * @return GPS indicated when rule matches
     */
    public GPS getRuleTargetGPS();
    /**
     * Set the rule's target GPS
     * 
     * @param gps - GPS
     */
    public void setRuleTargetGPS(GPS gps);
}
