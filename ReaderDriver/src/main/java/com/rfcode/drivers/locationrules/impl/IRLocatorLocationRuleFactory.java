package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Basic rule factory for IR locator-based zone determination.
 * 
 * @author Mike Primm
 */
public class IRLocatorLocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "IRLocatorRule";
    /** Attribute ID - locator ID code (integer) */
    public static final String ATTRIB_IRLOCATORCODE = "irlocatorcode";
    /** Attribute ID - IR code not found timeout (integer) */
    public static final String ATTRIB_IRNOTFOUNDTIMEOUT = "irnotfoundtimeout";
    /** Attribute ID - locator reference tag (string) */
    public static final String ATTRIB_LOCATORREFTAGID = "locatorreftagid";
    /** Attribute ID - ignore location update delay (boolean) */
    public static final String ATTRIB_IGNORELOCUPDATEDELAY = "ignorelocupddelay";
    
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_IRLOCATORCODE,
        ATTRIB_CHANNELLIST, ATTRIB_IRNOTFOUNDTIMEOUT, ATTRIB_LOCATORREFTAGID, ATTRIB_IGNORELOCUPDATEDELAY};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {"001",
        new HashSet<ReaderChannel>(), Integer.valueOf(30), "", Boolean.FALSE};
    /** Our label */
    public static final String LABEL = "Match by IR Locator";
    /** Our description */
    public static final String DESC = "Matches tag reporting IR Locator codes with specific location.";
    /* Default value map */
    private static Map<String,Object> defmap = null;


    /**
     * Constructor
     */
    public IRLocatorLocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new IRLocatorLocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get copy of default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
