package com.rfcode.drivers.locationrules.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;

import com.rfcode.drivers.locationrules.IRCodeManager;
import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;

/**
 * Basic IR locator-based location rule. This rule monitors the 'irlocator'
 * attribute of tags supporting it, and maps recognized values to zone codes.
 * 
 * @author Mike Primm
 */
public class IRLocatorLocationRule extends AbstractLocationRule {
    private RuleTrigger our_attrib_trigger;
    private RuleTrigger[] our_channel_triggers;
    private RuleTrigger our_reftag_trigger;
    private RuleTrigger[] our_reftagchannel_triggers;
    private String locator_code;
    private int irnotfoundtimeout;
    private double conf_timeout_thresh;
    private ReaderChannel[] chanlist;  // Static channels, if any
    private ReaderChannel[] refchanlist;  // Reference tag channels, if any
    private String[] chanids;	// Combined channel ID list (static and reference
    private String reftag;
    private boolean ignoreLocUpdDelay;

    private static final double CONF_INC = 0.000001;
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    IRLocatorLocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set channel triggers, based on our settings
     */
    private void setChannelTriggers() {
        LocationRuleEngine eng = getLocationRuleFactory()
            .getLocationRuleEngine();
        /* If we're sensitive to which radios, add listeners for those radios */
        if (chanlist != null) {
        	ArrayList<RuleTrigger> lst = new ArrayList<RuleTrigger>();
            for (ReaderChannel chan : chanlist) {
                RuleTrigger rt = eng.registerRuleTriggerByChannel(this, chan);
                lst.add(rt);
            }
            our_channel_triggers = lst.toArray(new RuleTrigger[0]);
        }
        // If we have a reference tag, set the trigger
        if (reftag != null) {
        	refchanlist = new ReaderChannel[0];	// Default to empty list
        	our_reftagchannel_triggers = new RuleTrigger[0];
        	our_reftag_trigger = eng.registerRuleTriggerByTagIDs(this, Collections.singleton(reftag));
        }
        else {
        	refchanlist = null;
        	our_reftagchannel_triggers = null;
        }
    }
    /**
     * Clean up existing channel triggers
     */
    private void cleanupChannelTriggers() {
        if(our_reftag_trigger != null) {
        	our_reftag_trigger.unregisterRuleTrigger(this);
        	our_reftag_trigger = null;
        }
        /* If reftag channel triggers defined, clean up */
        if (our_reftagchannel_triggers != null) {
            for (RuleTrigger rt : our_reftagchannel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            our_reftagchannel_triggers = null;
        }
        /* If channel triggers defined, clean up */
        if (our_channel_triggers != null) {
            for (RuleTrigger rt : our_channel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            our_channel_triggers = null;
        }
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        String newcode;
        int newtimeout;
        boolean new_ignorelocupd = false;
        /* Get locator value */
        Object v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_IRLOCATORCODE);
        if ((v == null) || (!(v instanceof String))) {
            throw new BadParameterException("IR locator code not set");
        }
        newcode = (String) v;
        while(newcode.length() < 3)
            newcode = "0" + newcode;
        /* Get IR not found timeout */
        v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_IRNOTFOUNDTIMEOUT);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("IR not found timeout not set");
        }
        newtimeout = ((Integer) v).intValue();
        /* Get ignore loc upd delay */
        v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_IGNORELOCUPDATEDELAY);
        if ((v != null) && (v instanceof Boolean)) {
        	new_ignorelocupd = ((Boolean) v).booleanValue();    
        }
        /* Check for channel list */
        v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_CHANNELLIST);
        ReaderChannel[] new_chanlist = null;
        if (v != null) {
            if (!(v instanceof Set)) {
                throw new BadParameterException("Channel set wrong type");
            }
            HashSet<ReaderChannel> new_chans = null;
            for(Object o : ((Set<?>)v)) {
                if(o instanceof ReaderChannel) {
                	if (new_chans == null) {
                		new_chans = new HashSet<ReaderChannel>();
                	}
                    new_chans.add((ReaderChannel)o);
                }
                else
                    throw new BadParameterException("Channel wrong type in set");
            }
            if (new_chans != null) {
            	new_chanlist = new_chans.toArray(new ReaderChannel[0]);
            }
        }
        /* Check for reference tag */
        v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_LOCATORREFTAGID);
        String new_reftag = null;
    	if (v != null) {
    		if (!(v instanceof String)) {
                throw new BadParameterException("Reference tag ID wrong type");
    		}
    		new_reftag = ((String)v).trim();
    		if (new_reftag.length() == 0) {
    			new_reftag = null;
    		}
    	}
    	// If enabled, disable before changing settings
    	boolean enab = this.getRuleEnabled();
    	if (enab) {
    		this.setRuleEnabled(false);
    	}
        chanlist = new_chanlist;
        reftag = new_reftag;
        ignoreLocUpdDelay = new_ignorelocupd;
        // Rebuild the channel ID list
        buildChanIDList();
        
        locator_code = newcode; /* Save locator code */
        irnotfoundtimeout = newtimeout;
        /* Compute confidence threshold */
        conf_timeout_thresh = MAXMATCH
            - ((CONF_INC * 1000 * irnotfoundtimeout) / LocationRuleEngine.TRIGGER_DELAY);
        attr.put(IRLocatorLocationRuleFactory.ATTRIB_IRLOCATORCODE, locator_code);        
        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
        // Re-enable, if needed
        if (enab) {
        	this.setRuleEnabled(true);
        }
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        LocationRuleEngine eng = getLocationRuleFactory()
            .getLocationRuleEngine();
        if (enable) { /* If we're enabled, set triggers */
            /* Register trigger for the IRLOCATOR attribute */
            our_attrib_trigger = eng.registerRuleTriggerByTagAttribute(this,
                MantisIITagType.IRLOCATOR_ATTRIB, locator_code);
            /*
             * If we're sensitive to which radios, add listeners for those
             * radios
             */
            setChannelTriggers();
            buildChanIDList();            
            // Register with IR code manager (for conflict detection
            IRCodeManager.instance().registerIRRule(this.getID(), this.locator_code, this.chanids);
        } else { /* Else, disabled - clean up triggers */
            if (our_attrib_trigger != null) {
                our_attrib_trigger.unregisterRuleTrigger(this);
            }
            cleanupChannelTriggers();
            buildChanIDList();
            // Unregister with IR code manager
            IRCodeManager.instance().unregisterIRRule(this.getID());
            
        }
    }
    /**
     * Process reference tag update
     */
    private void processRefTag(Tag tag) {
    	// Get current links : need to keep channel list in sync with this
    	Map<String, TagLink> links = tag.getTagLinks();
    	ArrayList<ReaderChannel> toadd = null;
    	ArrayList<Integer> todrop = null;
    	// Go through existing list: see what is missing
    	for (int i = 0; i < refchanlist.length; i++) {
    		String id = refchanlist[i].getID();
    		if (!links.containsKey(id)) {	 // Now missing?
    			if (todrop == null) {
    				todrop = new ArrayList<Integer>();
    			}
    			todrop.add(i);
    		}
    	}
    	// Go through current list: see what to add
    	for (String id : links.keySet()) {
    		boolean match = false;
    		for (int j = 0; j < refchanlist.length; j++) {
    			if (refchanlist[j].getID().equals(id)) {
    				match = true;
    				break;
    			}
    		}
    		if (!match) {
    			if (toadd == null) {
    				toadd = new ArrayList<ReaderChannel>();
    			}
    			toadd.add(links.get(id).getChannel());
    		}
    	}
    	// If any changes
    	if ((todrop != null) || (toadd != null)) {
    		ArrayList<ReaderChannel> new_refchanlist = new ArrayList<ReaderChannel>();
    		ArrayList<RuleTrigger> new_reftagchannel_triggers = new ArrayList<RuleTrigger>();
    		// If any to drop, drop them
    		if (todrop != null) {
    			for (Integer idx : todrop) {
    				// Unregister trigger
    				our_reftagchannel_triggers[idx].unregisterRuleTrigger(this);
    				refchanlist[idx] = null;
	            }
    		}
			// Copy rest to new lists
			for (int i = 0; i < refchanlist.length; i++) {
				if (refchanlist[i] == null) continue;
				new_refchanlist.add(refchanlist[i]);
				new_reftagchannel_triggers.add(our_reftagchannel_triggers[i]);
			}
			// And add any new ones
			if (toadd != null) {
		        LocationRuleEngine eng = getLocationRuleFactory().getLocationRuleEngine();
				for (ReaderChannel rc : toadd) {
					new_refchanlist.add(rc);
					new_reftagchannel_triggers.add(eng.registerRuleTriggerByChannel(this, rc));
				}
			}
			// Replace existing lists with new ones
			refchanlist = new_refchanlist.toArray(new ReaderChannel[0]);
			our_reftagchannel_triggers = new_reftagchannel_triggers.toArray(new RuleTrigger[0]);
			// And rebuild channel ID list
			buildChanIDList();
		}
    }
    private void buildChanIDList() {
    	String[] old_chanids = chanids;
    	boolean changed = false;
    	// If no channel specified, null list
    	if ((chanlist == null) && (refchanlist == null)) {
    		chanids = null;
    		if (old_chanids != null) {
    			changed = true;
    		}
    	}
    	else {	// Else, need list (even if empty due to no reference tag matches)
    		HashSet<String> new_idlist = new HashSet<String>();
    		if (chanlist != null) {
    			for (ReaderChannel rc : chanlist) {
    				new_idlist.add(rc.getID());
    			}
    		}
    		if (refchanlist != null) {
    			for (ReaderChannel rc : refchanlist) {
    				new_idlist.add(rc.getID());
    			}
    		}
    		chanids = new_idlist.toArray(new String[0]);
    		if ((old_chanids == null) || (old_chanids.length != chanids.length)) {
    			changed = true;
    		}
    		else {
    			for (int i = 0; i < chanids.length; i++) {
    				if (!chanids[i].equals(old_chanids[i])) {
    					changed = true;
    					break;
    				}
    			}
    		}
    	}
    	// If we're enabled, refresh register with new rule IDs
    	if (this.getRuleEnabled() && changed) {
    		IRCodeManager.instance().registerIRRule(this.getID(), this.locator_code, this.chanids);
    	}
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        double conf = LocationRule.MAXMATCH;
        /* See if reference tag (which may not be an IR tag) */
        if ((reftag != null) && tag.getTagGUID().equals(reftag)) {
        	processRefTag(tag);
        }
        int index = tag.getTagType().getTagAttributeIndex(
            MantisIITagType.IRLOCATOR_ATTRIB);
        if(index < 0)
            return LocationRule.NOMATCH; 
        Object v = tag.readTagAttribute(index); /* Read attribute for IR */
        if ((v == null) || (!(v instanceof String))) {
            return LocationRule.NOMATCH;
        }
        String code = (String) v;
        while(code.length() < 3)
            code = "0" + code;
        /* If wrong locator code, we're done */
        if (!code.equals(locator_code)) {
            /* If zero, and we matched before, handle IR not found timeout */
            if (code.equals("000") &&
                (old_conf > LocationRule.NOMATCH)) {
                conf = old_conf - CONF_INC; /* Subtract incremental confidence */
                /* Below threshold for timeout? */
                if (conf < conf_timeout_thresh) {
                    return LocationRule.NOMATCH;
                }
                /* Request another trigger for this rule, to do timeout stepping */
                LocationRuleEngine.getLocationRuleEngine().triggerRule(tag,
                    this);
            } else
                return LocationRule.NOMATCH;
        }
        /* If we've got a channel list (static or reference tag), make sure we match one of em */
        if (chanids != null) {
            boolean match = false;
        	for (String cid : chanids) {
        		if (tag.findTagLink(cid) != null) {
        			match = true;
        			break;
        		}
        	}
            if (!match) { /* If no match, we're done */
                conf =  LocationRule.NOMATCH;
            }
        }
        else {  /* Else, make sure tag is visible (no match if
                 * tag isn't able to be seen) */
            if(tag.getTagLinks().isEmpty())
                conf = LocationRule.NOMATCH;
        }
        /* If we're good, max match (only IR should do this) */
        return conf;
    }
    /**
     * Test if rule is IR domain determining rule (usually just IR rules do this)
     */
    public boolean isIRDomainDetermining() {
        return true;
    }
    /**
     * Test if rule overrides location update delay
     */
    public boolean overrideLocationUpdateDelay() {
    	return ignoreLocUpdDelay;
    }
}
