package com.rfcode.drivers.locationrules;

import java.util.Map;

import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.BadParameterException;

/**
 * Interface representing an instance of a location rule.
 * 
 * @author Mike Primm
 */
public interface LocationRule extends ReaderEntity {
    /** Confidence level - no match */
    public static final Double NOMATCH = 0.0;
    /** Confidence level - max match */
    public static final Double MAXMATCH = 1.0;
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attribs -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attribs)
        throws BadParameterException;
    /**
     * Get current value of attributes for the location rule. The set returned
     * will include values for all attribute IDs indicated by the
     * getLocationRuleAttributeIDs() method of the rule's factory.
     * 
     * @return map of attributes, keyed by attribute ID
     */
    public Map<String, Object> getLocationRuleAttributes();
    /**
     * Get the location rule's factory
     * 
     * @return factory
     */
    public LocationRuleFactory getLocationRuleFactory();
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable);
    /**
     * Get rule enabled/disabled
     * 
     * @return enable setting
     */
    public boolean getRuleEnabled();
    /**
     * Get the rule's target location
     * 
     * @return location indicated when rule matches
     */
    public Location getRuleTargetLocation();
    /**
     * Set the rule's target location
     * 
     * @param loc -
     *            location
     */
    public void setRuleTargetLocation(Location loc);
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf);
    /**
     * Test if rule is IR domain determining rule (usually just IR rules do this)
     */
    public boolean isIRDomainDetermining();
    /**
     * Test if rule overrides location update delay
     */
    public boolean overrideLocationUpdateDelay();
}
