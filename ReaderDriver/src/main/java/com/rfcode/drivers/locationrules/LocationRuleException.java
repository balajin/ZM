package com.rfcode.drivers.locationrules;

import com.rfcode.drivers.readers.ReaderEntity;

/**
 * Interface representing an instance of a location rule exception
 * 
 * @author Mike Primm
 */
public interface LocationRuleException extends ReaderEntity {
    /**
     * Get location rule associated with exception
     * @returns location rule with given error
     */
    public String getRuleWithException();
    /**
     * Get location rules related to exception (e.g. conflicting rules)
     * @returns location rules related to given error
     */
    public String[] getRulesRelatedToException();
    /**
     * Get description message for exception
     * @returns message
     */
    public String getDescription();
    /**
     * Get exception type (language neutral)
     * @returns exception type (can be used for translatable message selection)
     */
    public String getExceptionType();
    /**
     * Get exception timestamp (when first detected)
     * @returns UTC milliseconds
     */
    public long getExceptionTimestamp();
}


