package com.rfcode.drivers.locationrules;

import com.rfcode.drivers.readers.Tag;

/**
 * Interface representing an instance of a location rule that is
 * interested in location result reporting.  This is used for rules
 * that need to revise their results based on matches with other rules.
 * 
 * @author Mike Primm
 */
public interface LocationRuleWithResultFeedback extends LocationRule {
    /**
     * Report location result change for given tag
     * @param tag - tag
     * @param loc - new matching location
     * @param old_conf - old confidence for our rule
     * @return new confidence for our rule
     */
    public double reportLocationResult(Tag tag, Location loc, double old_conf);
}
