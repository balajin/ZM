package com.rfcode.drivers.locationrules.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.List;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.ranger.RangerProcessor;

/**
 * Reference tag SSI threshold based location rule. This rule monitors 
 * any radios that can see one or more specific reference tags above a given SSI level,
 * indicating proximity to the target location, and takes the strongest SSI value among
 * them to produce a confidence versus a cutoff threshold (0.0 match) and a 
 * high confidence threshold (0.9 match) for other tags seen.
 * 
 * @author Mike Primm
 */
public class RefTagSSILocationRule extends AbstractLocationRule {
    private int ssi_minimum;
    private int ssi_highconf;
    private int reftag_ssi_minimum;
    private Set<String> reftaglist;
    private RuleTrigger tag_trigger;
    private long matchtime; /* Match delay in msec */

    /* Record for tracking specific channel */
    private static class ChannelRec {
        Set<String> matching_reftags;   /* Matching reference tags */
        RuleTrigger trig;   /* Registered trigger */
        ReaderChannel rc;
    };
    /* Set of currently matching channels, and their triggers */
    private HashMap<String, ChannelRec> chanlist = 
        new HashMap<String, ChannelRec>();
    private HashMap<String, Set<String>> chan_by_reftag = 
        new HashMap<String, Set<String>>();

    /* Confidence at high confidence SSI */
    private static final double CONF_FOR_HIGH = 0.9;
    /* Confidence at max SSI (assume 40dBm above high level) */
    private static final double CONF_FOR_MAX = 0.95;
    private static final int DBM_MAX_ABOVE_HIGH = 40;

    /* Our session action - handles periodic callbacks from session */
    private class SessionAct implements Runnable {
        ChannelRec cr;
        public SessionAct(ChannelRec ourcr, SessionFactory sf) {
            cr = ourcr;
            /* Add delayed action to ranger processor thread */
            RangerProcessor.addDelayedAction(this, matchtime);
        }
        public void run() {
            if (cr.matching_reftags.size() == 0) { /* No longer active? */
                return; /* Quit */
            }
            /* Add the channel trigger */
            if(cr.trig == null) {
                LocationRuleEngine eng = getLocationRuleFactory()
                    .getLocationRuleEngine();
                cr.trig = eng.registerRuleTriggerByChannel(
                    RefTagSSILocationRule.this, 
                    cr.rc);
            }
        }
    }
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    RefTagSSILocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set tag triggers, based on our settings
     */
    private void setTagTriggers() {
        /* Add empty lists for all the reference tags */
        for(String rtid : reftaglist) {
            chan_by_reftag.put(rtid, new HashSet<String>());
        }
        LocationRuleEngine eng = getLocationRuleFactory()
            .getLocationRuleEngine();
        if(reftaglist != null) {
            tag_trigger = eng.registerRuleTriggerByTagIDs(this, reftaglist);
        }
    }
    /**
     * Clean up existing tag triggers
     */
    private void cleanupTagTriggers() {
        if(tag_trigger != null) {
            tag_trigger.unregisterRuleTrigger(this);
            tag_trigger = null;
        }
        /* Drop all the channel triggers */
        for(ChannelRec cr : chanlist.values()) {
            if(cr.trig != null) {
                cr.trig.unregisterRuleTrigger(this);
                cr.trig = null;
            }
        }
        /* Empty channel list and tag ref list*/
        chanlist.clear();
        chan_by_reftag.clear();
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        int newmin;
        int newhigh;
        int newrefmin;
        int newmatchtime;
        /* Get SSI minimum value */
        Object v = attr.get(RefTagSSILocationRuleFactory.ATTRIB_SSIMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI minimum not set");
        }
        newmin = ((Integer) v).intValue();
        /* Get SSI high confidence value */
        v = attr.get(RefTagSSILocationRuleFactory.ATTRIB_SSIHIGHCONF);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI high confidence not set");
        }
        newhigh = ((Integer) v).intValue();
        /* If high confidence is lower that minimum, bad */
        if (newhigh < newmin) {
            throw new BadParameterException(
                "SSI high confidence is below SSI miniumum");
        }
        /* Get ref tag SSI minimum value */
        v = attr.get(RefTagSSILocationRuleFactory.ATTRIB_REFTAGSSIMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("Ref Tag SSI minimum not set");
        }
        newrefmin = ((Integer) v).intValue();
        /* Get ref tag match time */
        v = attr.get(RefTagSSILocationRuleFactory.ATTRIB_REFTAGMATCHTIME);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("Ref Tag Match Time not set");
        }
        newmatchtime = ((Integer) v).intValue();
        if(newmatchtime < 0) newmatchtime = 0;

        /* Check for ref tag list */
        v = attr.get(RefTagSSILocationRuleFactory.ATTRIB_REFTAGS);
        Set<String> new_reftaglist = null;
        if (v != null) {
            if (!(v instanceof List)) {
                throw new BadParameterException("Reference tag set wrong type");
            }
            new_reftaglist = new HashSet<String>();
            for(Object o : ((List<?>)v)) {
                if(o instanceof String)
                    new_reftaglist.add((String)o);
                else
                    throw new BadParameterException("Reference tag wrong type in set");
            }
        }
        if (reftaglist != null) { /* Don't assert on default set */
            if ((v == null) || (((List<?>) v).size() == 0))
                throw new BadParameterException(
                    "Reference tag list must have at least one tag");
        }
        reftaglist = new_reftaglist;
        reftag_ssi_minimum = newrefmin;
        ssi_minimum = newmin;
        ssi_highconf = newhigh;
        matchtime = newmatchtime*1000L;
        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        if (enable) { /* If we're enabled, set triggers */
            setTagTriggers();
        } else { /* Else, disabled - clean up triggers */
            cleanupTagTriggers();
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        String tid = tag.getTagGUID();
        Map<String, TagLink> taglinks = tag.getTagLinks();
        TagLink tl;
        /* First, see if its a reference tag */
        if(reftaglist.contains(tid)) {
            Set<String> rcids = chan_by_reftag.get(tid);   /* Look up current matching channels */
            Set<String> newrcids = new HashSet<String>();
            /* For each current link, see if its new match */
            for(String cur_rcid : taglinks.keySet()) {
                if(rcids.contains(cur_rcid)) {  /* If existing match, nothing to do (we'll validate later) */
                    continue;
                }
                tl = taglinks.get(cur_rcid);    /* Get link */
                Object v = tl.readTagLinkAttribute(TagLink.ATTRIB_SSI);            
                /* If defined and big enough to match */
                if ((v != null) && (v instanceof Integer) && 
                    (((Integer)v).intValue() >= reftag_ssi_minimum)) {
                    /* Add channel, if not already added */
                    ChannelRec cr = chanlist.get(cur_rcid);
                    if(cr == null) {    /* Not defined yet, so new match */
                        LocationRuleEngine eng = getLocationRuleFactory()
                            .getLocationRuleEngine();
                        cr = new ChannelRec();
                        cr.rc = tl.getChannel();
                        cr.matching_reftags = new HashSet<String>();
                        cr.matching_reftags.add(tid);   /* Match tag */
                        chanlist.put(cur_rcid, cr);
                        if(matchtime > 0) { /* Add session delay */
                            new SessionAct(cr, eng.getSessionFactory());
                        }
                        else {  /* Else, add trigger now */
                            cr.trig = eng.registerRuleTriggerByChannel(this, 
                                tl.getChannel());
                        }
                    }
                    else {
                        cr.matching_reftags.add(tid);   /* Match tag */
                    }
                    newrcids.add(cur_rcid); /* Update match list for reftag */
                }
            }
            /* For each existing, see if still present and strong enough */
            for(String rcid : rcids) {
                boolean match = false;
                tl = taglinks.get(rcid);    /* See if link defined */
                if(tl != null) {
                    Object v = tl.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                    /* If defined and big enough to match */
                    if ((v != null) && (v instanceof Integer) && 
                        (((Integer)v).intValue() >= reftag_ssi_minimum)) {
                        newrcids.add(rcid); /* Keep it in the set */
                        match = true;
                    }
                }
                if(!match) {    /* Not matching, so drop it */
                    ChannelRec cr = chanlist.get(rcid); /* Get record */
                    if(cr != null) {        /* Found it, remove from list */
                        cr.matching_reftags.remove(tid);
                        if(cr.matching_reftags.size() == 0) {   /* Empty? */
                            /* Unregister trigger */
                            if(cr.trig != null) {
                                cr.trig.unregisterRuleTrigger(this);
                                cr.trig = null;
                            }
                            chanlist.remove(rcid);  /* Drop from active channels */
                        }
                    }
                }
            }
            chan_by_reftag.put(tid, newrcids);  /* Update match list */
        }        
        /*
         * Get the channels for the tag, and see find the max of those that are
         * in our list
         */
        int best_ssi = Integer.MIN_VALUE;
        for (String cid : chanlist.keySet()) {
            TagLink t = taglinks.get(cid); /* See if in active link set */
            if (t != null) {
                Object v = t.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                if ((v != null) && (v instanceof Integer)) {
                    int ssi = ((Integer) v).intValue(); /* Get value */
                    if (ssi > best_ssi)
                        best_ssi = ssi; /* See if new best */
                }
            }
        }
        /* Now, compute confidence based on best SSI */
        if (best_ssi <= ssi_minimum) { /* If below minimum */
            return LocationRule.NOMATCH;
        } else if (best_ssi >= (ssi_highconf + DBM_MAX_ABOVE_HIGH)) { /* Max */
            return CONF_FOR_MAX;
        } else if (best_ssi >= ssi_highconf) { /* If above high-conf */
            return CONF_FOR_HIGH
                + ((CONF_FOR_MAX - CONF_FOR_HIGH)
                    * (double) (best_ssi - ssi_highconf) / (double) DBM_MAX_ABOVE_HIGH);
        } else { /* Between min and high */
            return CONF_FOR_HIGH * (double) (best_ssi - ssi_minimum)
                / (double) (ssi_highconf - ssi_minimum);
        }
    }
}
