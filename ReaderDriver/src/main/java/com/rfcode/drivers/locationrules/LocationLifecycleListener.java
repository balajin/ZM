package com.rfcode.drivers.locationrules;

import java.util.Map;
/**
 * Location lifecycle listener - used for reporting location creation and deletion
 * events.
 * 
 * @author Mike Primm
 */
public interface LocationLifecycleListener {
    /**
     * Location created notification. Called after location has been created
     * (i.e. after end of init()).
     * 
     * @param loc -
     *            location being created
     */
    public void locationLifecycleCreate(Location loc);
    /**
     * Location deleted notification. Called at start of location cleanup(), when
     * location is about to be deleted.
     * 
     * @param loc -
     *            location being deleted
     */
    public void locationLifecycleDelete(Location loc);
    /**
     * Location attributes changed notification - called when any of the location's
     * attributes have been changed from their previous values.  This does NOT include
     * inherited attributes.
     * 
     * @param loc -
     *            location with change attributes
     * @param oldval -
     *            Map of previous location attribute values (may be longer than the
     *            number of attributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    public void locationLifecycleAttributeChange(Location loc, Map<String,Object> oldval);
}
