package com.rfcode.drivers.locationrules.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;

/**
 * Basic SSI threshold based location rule. This rule monitors one or more
 * radios, and takes the strongest SSI value among them to produce a confidence
 * versus a cutoff threshold (0.0 match) and a high confidence threshold (0.9
 * match).
 * 
 * @author Mike Primm
 */
public class SimpleSSILocationRule extends AbstractLocationRule {
    private ArrayList<RuleTrigger> our_channel_triggers;
    private int ssi_minimum;
    private int ssi_highconf;
    private Set<ReaderChannel> chanlist;
    /* Confidence at high confidence SSI */
    private static final double CONF_FOR_HIGH = 0.9;
    /* Confidence at max SSI (assume 40dBm above high level) */
    private static final double CONF_FOR_MAX = 0.95;
    private static final int DBM_MAX_ABOVE_HIGH = 40;
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    SimpleSSILocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set channel triggers, based on our settings
     */
    private void setChannelTriggers() {
        LocationRuleEngine eng = LocationRuleEngine.getLocationRuleEngine();
        /* If we're sensitive to which radios, add listeners for those radios */
        if ((chanlist != null) && (chanlist.size() > 0)) {
            our_channel_triggers = new ArrayList<RuleTrigger>();
            for (ReaderChannel chan : chanlist) {
                RuleTrigger rt = eng.registerRuleTriggerByChannel(this, chan);
                our_channel_triggers.add(rt);
            }
        }
    }
    /**
     * Clean up existing channel triggers
     */
    private void cleanupChannelTriggers() {
        /* If channel triggers defined, clean up */
        if (our_channel_triggers != null) {
            for (RuleTrigger rt : our_channel_triggers) {
                rt.unregisterRuleTrigger(this);
            }
            our_channel_triggers.clear();
            our_channel_triggers = null;
        }
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
        int newmin;
        int newhigh;
        /* Get SSI minimum value */
        Object v = attr.get(SimpleSSILocationRuleFactory.ATTRIB_SSIMINIMUM);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI minimum not set");
        }
        newmin = ((Integer) v).intValue();
        /* Get SSI high confidence value */
        v = attr.get(SimpleSSILocationRuleFactory.ATTRIB_SSIHIGHCONF);
        if ((v == null) || (!(v instanceof Integer))) {
            throw new BadParameterException("SSI high confidence not set");
        }
        newhigh = ((Integer) v).intValue();
        /* If high confidence is lower that minimum, bad */
        if (newhigh < newmin) {
            throw new BadParameterException(
                "SSI high confidence is below SSI miniumum");
        }
        /* Check for channel list */
        v = attr.get(IRLocatorLocationRuleFactory.ATTRIB_CHANNELLIST);
        Set<ReaderChannel> new_chanlist = null;
        if (v != null) {
            if (!(v instanceof Set)) {
                throw new BadParameterException("Channel set wrong type");
            }
            new_chanlist = new HashSet<ReaderChannel>();
            for(Object o : ((Set<?>)v)) {
                if(o instanceof ReaderChannel)
                    new_chanlist.add((ReaderChannel)o);
                else
                    throw new BadParameterException("Channel wrong type in set");
            }
        }
        if (chanlist != null) { /* Don't assert on default set */
            if ((v == null) || (((Set<?>) v).size() == 0))
                throw new BadParameterException(
                    "Channel list must have at least one channel");
        }
        chanlist = new_chanlist;
        ssi_minimum = newmin;
        ssi_highconf = newhigh;
        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        if (enable) { /* If we're enabled, set triggers */
            /*
             * If we're sensitive to which radios, add listeners for those
             * radios
             */
            setChannelTriggers();
        } else { /* Else, disabled - clean up triggers */
            cleanupChannelTriggers();
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        /*
         * Get the channels for the tag, and see find the max of those that are
         * in our list
         */
        int best_ssi = Integer.MIN_VALUE;
        Map<String, TagLink> taglinks = tag.getTagLinks();
        for (ReaderChannel c : chanlist) {
            TagLink t = taglinks.get(c.getID()); /* See if in active link set */
            if (t != null) {
                Object v = t.readTagLinkAttribute(TagLink.ATTRIB_SSI);
                if ((v != null) && (v instanceof Integer)) {
                    int ssi = ((Integer) v).intValue(); /* Get value */
                    if (ssi > best_ssi)
                        best_ssi = ssi; /* See if new best */
                }
            }
        }
        /* Now, compute confidence based on best SSI */
        if (best_ssi <= ssi_minimum) { /* If below minimum */
            return LocationRule.NOMATCH;
        } else if (best_ssi >= (ssi_highconf + DBM_MAX_ABOVE_HIGH)) { /* Max */
            return CONF_FOR_MAX;
        } else if (best_ssi >= ssi_highconf) { /* If above high-conf */
            return CONF_FOR_HIGH
                + ((CONF_FOR_MAX - CONF_FOR_HIGH)
                    * (double) (best_ssi - ssi_highconf) / (double) DBM_MAX_ABOVE_HIGH);
        } else { /* Between min and high */
            return CONF_FOR_HIGH * (double) (best_ssi - ssi_minimum)
                / (double) (ssi_highconf - ssi_minimum);
        }
    }
}
