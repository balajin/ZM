package com.rfcode.drivers.locationrules;

import java.util.Map;
import java.util.HashMap;
import com.rfcode.drivers.readers.ReaderEntity;

/**
 * Interface for representing a location rule factory, for handling a class of
 * location rules.
 * 
 * @author Mike Primm
 */
public interface LocationRuleFactory extends ReaderEntity {
    /** Rule attribute ID - channel list (set of ReaderChannel) */
    public static final String ATTRIB_CHANNELLIST = "channellist";
    /** Tag attribute ID for location zone attribute (String) */
    public static final String ATTRIB_ZONELOCATION = "locationzone";
    /** Tag attribute ID for gpsid attribute (String) */
    public static final String ATTRIB_GPSID = "gpsid";
    /** Tag attribute ID for lastgpsid attribute (String) */
    public static final String ATTRIB_LASTGPSID = "lastgpsid";
    /** Tag attribute ID for lastgpsts attribute (Long) */
    public static final String ATTRIB_LASTGPSTS = "lastgpsts";
    /** Value used for case of no matching location zone */
    public static final String ZONELOCATION_NOMATCH = "";
    /** Default value for location zone attribute - no match */
    public static final String ZONELOCATION_DEFAULT = ZONELOCATION_NOMATCH;
    /** Value used for case of no matching GPS */
    public static final String GPSID_NOMATCH = "";
    /** Default value for location zone attribute - no match */
    public static final String GPSID_DEFAULT = GPSID_NOMATCH;
    /** Label for location zone attribute */
    public static final String ZONELOCATION_LABEL = "Location Zone ID";
    /** Label for GPSID attribute */
    public static final String GPSID_LABEL = "Matching GPS ID";
    /** Label for LASTGPSID attribute */
    public static final String LASTGPSID_LABEL = "Last GPS ID";
    /** Label for LASTGPSTS attribute */
    public static final String LASTGPSTS_LABEL = "Last GPS Timestamp";

    /**
     * Tag attribute ID for map of location rule confidence values, keyed by
     * rule
     */
    public static final String ATTRIB_CONFIDENCE_BY_RULE = "confidencebyrule";
    /**
     * Default value for map of location rule confidence values, keyed by rule
     * ID
     */
    public static final Map<LocationRule, Double> CONFIDENCE_BY_RULE_DEFAULT = new HashMap<LocationRule, Double>(1);
    /**
     * Label for location rule confidence values
     */
    public static final String CONFIDENCE_BY_RULE_LABEL = "Location Rule Confidence";
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule();
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs();
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel();
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription();
    /**
     * Get copy of default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set
     */
    public Map<String, Object> getDefaultLocationRuleAttributes();
    /**
     * Set engine
     * 
     * @param eng -
     *            our LocationRuleEngine
     */
    public void setLocationRuleEngine(LocationRuleEngine eng);
    /**
     * Get engine
     * 
     * @reutrn our LocationRuleEngine
     */
    public LocationRuleEngine getLocationRuleEngine();
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion();
}
