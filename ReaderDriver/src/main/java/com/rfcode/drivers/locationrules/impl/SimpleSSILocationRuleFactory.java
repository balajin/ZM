package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Basic rule factory for simple SSI threshold based zone determination.
 * 
 * @author Mike Primm
 */
public class SimpleSSILocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "SimpleSSIRule";
    /** Attribute ID - SSI minimum (integer) */
    public static final String ATTRIB_SSIMINIMUM = "ssiminimum";
    /** Attribute ID - SSI high-confidence (integer) */
    public static final String ATTRIB_SSIHIGHCONF = "ssihighconf";
    /** Default SSI minimum */
    public static final int DEFAULT_SSIMINIMUM = -90;
    /** Default high-confidence SSI */
    public static final int DEFAULT_SSIHIGHCONF = -60;
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_SSIMINIMUM,
        ATTRIB_SSIHIGHCONF, ATTRIB_CHANNELLIST};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {
        Integer.valueOf(DEFAULT_SSIMINIMUM),
        Integer.valueOf(DEFAULT_SSIHIGHCONF), new HashSet<ReaderChannel>()};
    /** Our label */
    public static final String LABEL = "Match by strongest SSI";
    /** Our description */
    public static final String DESC = "Matches tag with location based on SSI threshold for a set of one or more radios.";
    /* Default value map */
    private static Map<String,Object> defmap = null;
    /**
     * Constructor
     */
    public SimpleSSILocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new SimpleSSILocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);    
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
