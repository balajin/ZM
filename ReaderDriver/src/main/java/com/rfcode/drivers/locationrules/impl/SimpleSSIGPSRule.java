package com.rfcode.drivers.locationrules.impl;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.GPSRule;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;

/**
 * Basic SSI threshold based GPS rule - same as the location rule, but only used
 * internally for the gpsid attribute.
 * 
 * @author Mike Primm
 */
public class SimpleSSIGPSRule extends SimpleSSILocationRule implements GPSRule {
    private GPS gps;
    private boolean fix;
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    public SimpleSSIGPSRule() {
        super(null);
    }
    /**
     * Get the rule's target GPS
     * 
     * @return GPS indicated when rule matches
     */
    public GPS getRuleTargetGPS() {
		return gps;
	}
    /**
     * Set the rule's target GPS
     * 
     * @param ourgps - GPS
     */
    public void setRuleTargetGPS(GPS ourgps) {
		gps = ourgps;
        fix = false;
        if(ourgps != null) {
            GPSData dat = ourgps.getGPSData();
            if(dat != null)
                fix = dat.hasFix();
        }
	}
    /**
     * Signal GPS fix status change
     */
    public void handlGPSFixChange(boolean newfix) {
        if(fix && (!newfix)) { /* Losing fix? */
            fix = false;
            /* Fire everyone matching us */
            LocationRuleEngine eng = LocationRuleEngine.getLocationRuleEngine();            
            eng.triggerAllTagsByRule(this);
        }
        /* Don't trigger for gaining fix - let tags coming in do that */
        else if((!fix) && newfix) {
            fix = true;
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        GPSData gpsd = gps.getGPSData();
        if((gpsd == null) || (gpsd.hasFix() == false))
            return LocationRule.NOMATCH;
        return super.evaluateRuleForTag(tag, old_conf);
    }
}
