package com.rfcode.drivers.locationrules.impl;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.locationrules.LocationRuleException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.ReaderEntityDirectory;

/**
 * Implementation of base LocationRuleException class
 * 
 * @author Mike Primm
 */
public class LocationRuleExceptionImpl implements LocationRuleException {
	private final String ruleid;
	private final String exceptiontype;
	private String exceptionid;
	private String[] related_ruleids;
	private final String description;
	private final long timestamp;
	
	public LocationRuleExceptionImpl(String ruleid, String exceptiontype, String desc) {
		this.ruleid = ruleid;
		this.exceptiontype = exceptiontype;
		this.description = desc;
		this.timestamp = System.currentTimeMillis(); 
	}
	@Override
	public String getID() {
		return exceptionid;
	}

	@Override
	public void setID(String id) {
		exceptionid = id;
	}

	@Override
	public void init() throws DuplicateEntityIDException, BadParameterException {
        if (exceptionid == null) {
            throw new BadParameterException("Location Rule ID not set");
        }
        ReaderEntityDirectory.addEntity(this);
	}

	@Override
	public void cleanup() {
        if (exceptionid != null) {
            ReaderEntityDirectory.removeEntity(this);
        }
	}

	@Override
	public String getRuleWithException() {
		return ruleid;
	}

	@Override
	public String[] getRulesRelatedToException() {
		return related_ruleids;
	}
	
	public void setRulesRelatedToException(String[] ids) {
		if (ids == null) {
			related_ruleids = null;
			return;
		}
		// Scrub ID of our rule from list
		int len = 0;
		for (String id : ids) {
			if (id.equals(ruleid)) continue;
			len++;
		}
		related_ruleids = new String[len];
		len = 0;
		for (String id : ids) {
			if (id.equals(ruleid)) continue;
			related_ruleids[len] = id;
			len++;
		}
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getExceptionType() {
		return exceptiontype;
	}

	@Override
	public long getExceptionTimestamp() {
		return timestamp;
	}
}
