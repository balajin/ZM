package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;
import com.rfcode.drivers.readers.ListListDouble;

/**
 * GPS-based location correlation rule factory: matches when a tag is associated with a GPS which is reporting
 * a current location matching a given set of coordinates.  Coordinate lists are ordered latitude/longitude
 * pairs, corresponding to the following:
 *     1) One pair implies a match if within 100m of the given latitude/longitude
 *     2) Two pairs implies a match if within the rectangle aligned N-S and E-W with the given points 
 *       at opposite corners)
 *     3) Three or more pairs implies a polygon with the given coordinates (projected on a Mercator 
 *       projection - N-S/E-W rectangular grid) representing the corners, and with the shape being closed
 *       by connecting the last point to the first (e.g. 3 points would be a triangle with edges from
 *       point 1 to 2, 2 to 3, and 3 to 1).
 * 
 * @author Mike Primm
 */
public class GPSRefLocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "GPSRefRule";
    /** Attribute ID - Confidence when matching (double 0-1) */
    public static final String ATTRIB_MATCHING_CONF = "matchingconf";
    /** Attribute ID - List of coordinates (List of 2 element Lists of Doubles) */
    public static final String ATTRIB_COORDLIST = "coordlist";
	/** Default for matching confidence */
	public static final double DEFAULT_MATCHING_CONF = 0.8;
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_COORDLIST,
        ATTRIB_MATCHING_CONF};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {
        new ListListDouble(),
		Double.valueOf(DEFAULT_MATCHING_CONF) };
    /** Our label */
    public static final String LABEL = "Match by GPS at coordinates";
    /** Our description */
    public static final String DESC = "Tag matches location if it matches a GPS whose current position corresponds to the provided coordinates.";
    /* Default value map */
    private static Map<String,Object> defmap = null;
    /**
     * Constructor
     */
    public GPSRefLocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new GPSRefLocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);    
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
