package com.rfcode.drivers.locationrules.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.LocationRuleEngine.RuleTrigger;
import com.rfcode.drivers.locationrules.LocationRuleEngine;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.locationrules.AbstractLocationRule;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.ranger.RangerServer;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.drivers.readers.GPSStatusListener;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.TagGroup;

/**
 * GPS-based location correlation rule: matches when a tag is associated with a GPS which is reporting
 * a current location matching a given set of coordinates.  Coordinate lists are ordered latitude/longitude
 * pairs, corresponding to the following:
 *     1) One pair implies a match if within 100m of the given latitude/longitude
 *     2) Two pairs implies a match if within the rectangle aligned N-S and E-W with the given points 
 *       at opposite corners)
 *     3) Three or more pairs implies a polygon with the given coordinates (projected on a Mercator 
 *       projection - N-S/E-W rectangular grid) representing the corners, and with the shape being closed
 *       by connecting the last point to the first (e.g. 3 points would be a triangle with edges from
 *       point 1 to 2, 2 to 3, and 3 to 1).
 * 
 * @author Mike Primm
 */
public class GPSRefLocationRule extends AbstractLocationRule {
	private static class LatLon {
		double lat;
		double lon;
	};
	private class GPSListener implements GPSStatusListener {
		/**
		 * GPS position data changed
	     * @param gps - GPS with new position data (access using gps.getGPSData())
		 * @param prev_data - previous GPS data for the GPS (value just replaced)
		 */
		public void gpsDataChanged(GPS gps, GPSData prev_data) {
			checkGPSPosition(gps);
		}
	    /**
	     * GPS created notification. Called after GPS has been created. Any initial
	     * attributes will have been added by the time this is called.
	     * 
	     * @param gps -
	     *            GPS being created
	     */
	    public void gpsLifecycleCreate(GPS gps) {
			checkGPSPosition(gps);
		}
	    /**
	     * GPS deleted notification. Called at start of GPS cleanup(), when GPS is
	     * about to be deleted.
	     * 
	     * @param gps -
	     *            GPS being deleted
	     */
	    public void gpsLifecycleDelete(GPS gps) {
			matching_gpsid.remove(gps.getID());
		}
	}
	private List<LatLon> coordlist;	/* List of coordinates for GPS match */
	private double match_conf;	/* Confidence to report when match */
    private RuleTrigger our_attrib_trigger;
	private Set<String> matching_gpsid = new HashSet<String>();
	private GPSListener our_gps_listener;

	public static final double DEFAULT_RADIUS = 100.0;	/* 100m is radius for single coordinate match */
	/* Parse lat/lon string - allowed formats:
     *    number{N || S}{space || comma}number{E || W}
     */
	private LatLon parseLatLon(String fmtstr) throws BadParameterException {
		fmtstr = fmtstr.toUpperCase().trim();
		/* Prepare string - replace commas, slashes, colons, quotes, doublequotes, degrees with spaces */
		if(fmtstr.indexOf('N') > 0)	fmtstr = fmtstr.replaceAll("N", " N");	/* Replace N with padded N (force token) */
		if(fmtstr.indexOf('S') > 0)	fmtstr = fmtstr.replaceAll("S", " S");	/* Replace S with padded S (force token) */
		if(fmtstr.indexOf('E') > 0)	fmtstr = fmtstr.replaceAll("E", " E");	/* Replace E with padded E (force token) */
		if(fmtstr.indexOf('W') > 0)	fmtstr = fmtstr.replaceAll("W", " W");	/* Replace W with padded W (force token) */
		fmtstr = fmtstr.replaceAll("[,:/'\"\u0080]", " ").trim();
		String[] tok = fmtstr.split(" ");	/* Now, split at the spaces */
		int tokcnt = 0;
		double lat = 0.0, lon = 0.0;

		for(String s : tok) {	/* Compact out the blanks */
			if(s.length() > 0) {
				tok[tokcnt] = s;
				tokcnt++;
			}
		}
		/* If only 2 non-blank tokens, have to be unitless lat lon */
		if(tokcnt == 2) {
			try {
				lat = Double.parseDouble(tok[0]);
				lon = Double.parseDouble(tok[1]);
			} catch (NumberFormatException nfx) {
	            throw new BadParameterException("Invalid lat-lon-coord - " + nfx.getMessage());
			}
		}
		else if(tokcnt >= 4) {	/* Else, at least 4, and we expect N/S and E/W */
			double latlon[] = new double[2];
			int intok = 0;	/* Start on degrees */
			int idx = 0;	/* Latitude is first */

			latlon[0] = latlon[1] = 0.0;

			for(int i = 0; i < tokcnt; i++) {
				if(idx > 1) {	/* Already done - extra tokens = bad */
		            throw new BadParameterException("Invalid lat-lon-coord");						
				}
				if(tok[i].equals("N") || tok[i].equals("S")) {	/* If N or S */
					if((idx != 0) || (intok == 0)) {	/* If not in latitude, or no degrees, error */
			            throw new BadParameterException("Invalid lat-lon-coord");						
					}
					idx = 1;	/* Advance to longitude */
					intok = 0;	/* Start at degrees again */
					if(tok[i].equals("S")) {		/* South? */
						latlon[0] = -latlon[0];	/* Invert value */
					}
				}
				else if(tok[i].equals("E") || tok[i].equals("W")) {	/* If E or W */
					if((idx != 1) || (intok == 0)) {	/* If not in longitude, or no degrees, error */
			            throw new BadParameterException("Invalid lat-lon-coord");						
					}
					idx = 2;	/* We're done */
					intok = 0;	/* Start at degrees again */
					if(tok[i].equals("W")) {		/* West? */
						latlon[1] = -latlon[1];	/* Invert value */
					}
				}
				else if(intok == 0) { 	/* Degrees? */
					try {
						latlon[idx] = Double.parseDouble(tok[i]);
						intok++;		/* On to minutes, if any */
					} catch (NumberFormatException nfx) {
			            throw new BadParameterException("Invalid lat-lon-coord - " + nfx.getMessage());
					}
				}
				else if(intok == 1) { 	/* Minutes? */
					try {
						latlon[idx] += Double.parseDouble(tok[i]) / 60.0;
						intok++;		/* On to seconds, if any */
					} catch (NumberFormatException nfx) {
			            throw new BadParameterException("Invalid lat-lon-coord - " + nfx.getMessage());
					}
				}
				else if(intok == 2) { 	/* Seconds? */
					try {
						latlon[idx] += Double.parseDouble(tok[i]) / 3600.0;
						intok++;		/* Shouldn't be any more besides NSEW */
					} catch (NumberFormatException nfx) {
			            throw new BadParameterException("Invalid lat-lon-coord - " + nfx.getMessage());
					}
				}
				else {				/* We're on to a token we shouldn't have */
		            throw new BadParameterException("Invalid lat-lon-coord");						
				}
			}
			/* If we're done, but not in proper final state, error */
			if(idx != 2) {
		        throw new BadParameterException("Invalid lat-lon-coord");						
			}
			lat = latlon[0];
			lon = latlon[1];
		}
		else {
	        throw new BadParameterException("Invalid lat-lon-coord");						
		}
		LatLon ll = new LatLon();
		ll.lat = lat; ll.lon = lon;
		return ll;
	}
	private static final double MSL_RADIUS = 6371000.0;	/* Mean Sea Level Radius of Earth */
	/* Compute distance between two coordinates, in meters */
	private static double distanceBetween(LatLon l1, double lat2, double lon2) {
		double lat1, lon1;		
		lat1 = Math.toRadians(l1.lat);
		lat2 = Math.toRadians(lat2);
		lon1 = Math.toRadians(l1.lon);
		lon2 = Math.toRadians(lon2);
		/* Use spherical law of cosines to compute horizontal distance */
		return Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
			Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon2-lon1)) * MSL_RADIUS;
	}
	/**
	 * Check current GPS position for match with rule
	 */
	private boolean checkGPSPosition(GPS gps) {
		boolean match = false;
		GPSData gpsd = gps.getGPSData();
		if(gpsd != null) {
			GPSData.Fix f = gpsd.getGPSFix();
			/* If coordinates are useful */
			if((f == GPSData.Fix.FIX_2D) || (f == GPSData.Fix.FIX_3D)) {
				double lat = gpsd.getLatitude();
				double lon = gpsd.getLongitude();
				/* If only one coordinate, check distance from point */
				if(coordlist.size() == 1) {
					double dist = distanceBetween(coordlist.get(0), 
						lat, lon);
					if(dist <= DEFAULT_RADIUS) {	/* Close enough? */
						match = true;
					}
				}
				/* If two coordinates, check if inside rectangle */
				else if(coordlist.size() == 2) {
					LatLon l1 = coordlist.get(0);
					LatLon l2 = coordlist.get(1);
					double minlat = Math.min(l1.lat, l2.lat);
					double maxlat = Math.max(l1.lat, l2.lat);
					double minlon = Math.min(l1.lon, l2.lon);
					double maxlon = Math.max(l1.lon, l2.lon);
					/* If we're within the ranges, we match */
					if((lat >= minlat) && (lat <= maxlat) &&
						(lon >= minlon) && (lon <= maxlon)) {
						match = true;
					}
				}
				/* If more than 2, its a polyline - check if we're inside */
				else if(coordlist.size() > 2) {
					/* Implement in-polygon detect, based on http://alienryderflex.com/polygon/ */
					/* Basically, we're inside if a ray from the point to "outside" crosses an  */
					/* odd number of line segments (in this case, the ray is horizontal to the  */
					/* west - so refers to lines that intersect our latitude at a lower longitude*/
					int i, j = coordlist.size()-1;
					boolean odd = false;
					for(i = 0; i < coordlist.size(); i++) {
						LatLon pi = coordlist.get(i);
						LatLon pj = coordlist.get(j);
						/* If point is vertically within range of line segment from i to j */
						if( ((pi.lat < lat) && (pj.lat >= lat)) ||
							((pj.lat < lat) && (pi.lat >= lat))) {
							/* If point is to the right of the line */
							if(pi.lon + (lat - pi.lat)/(pj.lat-pi.lat)*(pj.lon-pi.lon) < lon) {
								odd = !odd;	/* Toggle flag */
							}
						}
						j = i;
					}
					if(odd) match = true;	/* If odd number of lines crossed, we're inside */
				}
			}
		}
		/* If we are matching, and were not before, or we don't match, and we did, update */
		boolean change = false;
		if(match && (!matching_gpsid.contains(gps.getID()))) {
			matching_gpsid.add(gps.getID());
			change = true;
		}
		else if((!match) && matching_gpsid.contains(gps.getID())) {
			matching_gpsid.remove(gps.getID());
			change = true;
		}
		/* If we changed, need to re-evaluate rule for all tags matching this GPSID */
		if(change) {
	        LocationRuleEngine eng = getLocationRuleFactory().getLocationRuleEngine();
            /* Loop through tag types */
            for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
                /* Find the attribute */
                int attrib_idx = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_GPSID);
                if (attrib_idx >= 0) { /* If found, add listener */
                    /* Now, see if any tags of this type */
                    Map<String, TagGroup> tgs = TagGroup.getTagGroupsByTagType(tt);
                    for (TagGroup tg : tgs.values()) {
                        Map<String, Tag> tags = tg.getTags();
                        for (Tag tag : tags.values()) {
							Object v = tag.readTagAttribute(attrib_idx);	/* Get attribute */
							/* If it matches our GPS ID, trigger it */
							if((v != null) && (v instanceof String) &&
								gps.getID().equals(v)) {
	                            eng.triggerRule(tag, this);
							}
                        }
                        tags.clear();
                    }
                    tgs.clear();
                }
            }
		}
		return match;
	}
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    GPSRefLocationRule(LocationRuleFactory f) {
        super(f);
    }
    /**
     * Set the attributes for the location rule. The keys of the map provided
     * must match the attribute IDs described by the
     * getLocationRuleAttrbiuteIDs() method from the rule's factory.
     * 
     * @param attr -
     *            map of values, keyed by attribute ID
     */
    public void setLocationRuleAttributes(Map<String, Object> attr)
        throws BadParameterException {
		double newconf;
		boolean need_reformat = false;
        /* Get matching confidence value */
        Object v = attr.get(GPSRefLocationRuleFactory.ATTRIB_MATCHING_CONF);
        if ((v == null) || (!(v instanceof Double))) {
            throw new BadParameterException("Matching confidence not set");
        }
		newconf = ((Double) v).doubleValue();
		if((newconf < 0.0) || (newconf > 1.0)) {	/* Too high or too low */
            throw new BadParameterException("Matching confidence must be between 0.0 and 1.0");
        }
        /* Check for coordinate list */
        v = attr.get(GPSRefLocationRuleFactory.ATTRIB_COORDLIST);
       	List<LatLon> new_coordlist = null;
        if (v != null) {
            if (!(v instanceof List)) {
                throw new BadParameterException("Coordinate list wrong type");
            }
            new_coordlist = new ArrayList<LatLon>();
			int doublecnt = 0;
            for(Object o : ((List<?>)v)) {
				if(o instanceof List) {
					@SuppressWarnings("unchecked")
					List<Double> cl = (List<Double>)o;
					if(cl.size() != 2) 
		                throw new BadParameterException("Coordinate list wrong type");
					LatLon new_l_l = new LatLon();
					new_l_l.lat = cl.get(0);
					new_l_l.lon = cl.get(1);
                    new_coordlist.add(new_l_l);
				}
				else if(o instanceof Double) {	/* List of doubles - two per value */
					doublecnt++;
					if((doublecnt % 2) == 0) {	/* Got an even number to process */
						LatLon new_l_l = new LatLon();
						Object o1 = ((List<?>)v).get(doublecnt-2);
						new_l_l.lat = (Double)o1;
						new_l_l.lon = (Double)o;
	                    new_coordlist.add(new_l_l);
						need_reformat = true;	/* Need to shift to lat-lon-coord-list */
					}						
				}
				else if(o instanceof String) {	/* List of strings - either formatted values, or pairs of doubles - two per value */
					try {
						new_coordlist.add(parseLatLon((String)o));
					} catch (BadParameterException bpx) {	/* If not, assume doubles */
						doublecnt++;
						if((doublecnt % 2) == 0) {	/* Got an even number to process */
							LatLon new_l_l = new LatLon();
							Object o1 = ((List<?>)v).get(doublecnt-2);
							try {
								if(o1 instanceof String) 
									new_l_l.lat = Double.parseDouble((String)o1);
								else
									new_l_l.lat = (Double)o1;
								if(o instanceof String)
									new_l_l.lon = Double.parseDouble((String)o);
								else
									new_l_l.lon = (Double)o;
							} catch (NumberFormatException nfx) {
				                throw new BadParameterException("Coordinate list wrong format");
							}
		                    new_coordlist.add(new_l_l);
						}						
					}
					need_reformat = true;	/* Need to shift to lat-lon-coord-list */
				}
                else
                    throw new BadParameterException("Coordinate list - wrong type in list");
            }
			/* If noneven double count, bad */
			if((doublecnt % 2) != 0) {
                throw new BadParameterException("Coordinate list - improper value count in list");				
			}
        }
		/* Need at least one coordinate */
		if((new_coordlist == null) || (new_coordlist.size() < 1)) {
            throw new BadParameterException(
                    "Coordinate list needs at least one value");
        }
		coordlist = new_coordlist;
		match_conf = newconf;
		/* If needed to reformat, do it now */
		if(need_reformat) {
			List<List<Double>> newval = new ArrayList<List<Double>>();
			for(LatLon l : coordlist) {
				ArrayList<Double> newv = new ArrayList<Double>(2);
				newv.add(Double.valueOf(l.lat));
				newv.add(Double.valueOf(l.lon));
				newval.add(newv);
			}
			attr.put(GPSRefLocationRuleFactory.ATTRIB_COORDLIST, newval);
		}

        /* Call superclass to set values */
        super.setLocationRuleAttributes(attr);
    }
    /**
     * Set rule enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setRuleEnabled(boolean enable) {
        if (getRuleEnabled() == enable)
            return;
        super.setRuleEnabled(enable);

        LocationRuleEngine eng = getLocationRuleFactory().getLocationRuleEngine();
		if(enable) {
            /* Register trigger for the GPSID attribute */
            our_attrib_trigger = eng.registerRuleTriggerByTagAttribute(this,
                LocationRuleFactory.ATTRIB_GPSID, null); /* Any change*/
			/* Register GPS listener */
			our_gps_listener = new GPSListener();
			RangerServer.addGPSStatusListener(our_gps_listener);
			/* Check initial state for existing GPSs */
			for(GPS gps : ReaderEntityDirectory.getGPSs()) {
				checkGPSPosition(gps);
			}
        } else { /* Else, disabled - clean up triggers */
            if (our_attrib_trigger != null) {
                our_attrib_trigger.unregisterRuleTrigger(this);
            }
			if(our_gps_listener != null) {
				RangerServer.removeGPSStatusListener(our_gps_listener);
				our_gps_listener = null;
			}
        }
    }
    /**
     * Evaluate rule for given tag
     * 
     * @param tag -
     *            tag to be evaluated
     * @param old_conf -
     *            previous confidence for this rule and this tag
     * @return confidence of match (0.0 = no match, 1.0 is maximum)
     */
    public double evaluateRuleForTag(Tag tag, double old_conf) {
        int index = tag.getTagType().getTagAttributeIndex(
            LocationRuleFactory.ATTRIB_GPSID);
        if(index < 0)
            return LocationRule.NOMATCH; 
        Object v = tag.readTagAttribute(index); /* Read attribute for IR */
        if ((v == null) || (!(v instanceof String))) {
            return LocationRule.NOMATCH;
        }
		String gpsid = (String)v;
		if(gpsid.length() == 0)
            return LocationRule.NOMATCH;
		/* If this is one of the matching GPS IDs, we're a match */
		if(matching_gpsid.contains(gpsid)) {
			return match_conf;
		}
		return 0.0;
    }
}
