package com.rfcode.drivers.locationrules.impl;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.locationrules.AbstractLocationRuleFactory;

/**
 * Basic rule factory for portal-based location detection, based on having
 * one or more "inside the door" channels, and one or more "outside the door"
 * channels.  If an inside channel matches better than an outside channel,
 * the tag is reported to match (with similar confidence to SimpleSSIRule).
 * If outside matches better, rule does not match.  If neither matches, and
 * the previous report was a match ("inside"), and low confidence match is
 * reported.
 * 
 * @author Mike Primm
 */
public class PortalSSILocationRuleFactory extends AbstractLocationRuleFactory {
    /** Our factory ID */
    public static final String FACTORY_ID = "PortalSSIRule";
    /** Attribute ID - inside channel list (set of ReaderChannel) */
    public static final String ATTRIB_INSIDECHANNELLIST = "insidechannellist";
    /** Attribute ID - outside channel list (set of ReaderChannel) */
    public static final String ATTRIB_OUTSIDECHANNELLIST = "outsidechannellist";
    /** Attribute ID - SSI minimum (integer) */
    public static final String ATTRIB_SSIMINIMUM = "ssiminimum";
    /** Attribute ID - SSI high-confidence (integer) */
    public static final String ATTRIB_SSIHIGHCONF = "ssihighconf";
    /** Attribute ID - confidence if inside but no longer visible (double) */
    public static final String ATTRIB_INSIDEUNREPORTEDCONF = "insideunreportedconf";
    /** Default SSI minimum */
    public static final int DEFAULT_SSIMINIMUM = -90;
    /** Default high-confidence SSI */
    public static final int DEFAULT_SSIHIGHCONF = -60;
    /** Default inside-but-unreported confidence */
    public static final double DEFAULT_INSIDEUNREPORTEDCONF = 0.01;
    /** Our attribute list */
    public static final String[] ATTRIBS = {ATTRIB_SSIMINIMUM,
        ATTRIB_SSIHIGHCONF, ATTRIB_INSIDEUNREPORTEDCONF, 
        ATTRIB_INSIDECHANNELLIST, ATTRIB_OUTSIDECHANNELLIST};
    /** Our attribute defaults */
    private static final Object[] DEFAULTS = {
        Integer.valueOf(DEFAULT_SSIMINIMUM),
        Integer.valueOf(DEFAULT_SSIHIGHCONF),
        Double.valueOf(DEFAULT_INSIDEUNREPORTEDCONF),
        new HashSet<ReaderChannel>(),
        new HashSet<ReaderChannel>()};
    /** Our label */
    public static final String LABEL = "Match by entry portal SSI";
    /** Our description */
    public static final String DESC = "Matches tag with location based on SSI threshold for a set of one or more radios set as 'inside' the location, versus one or more radios set as 'ouside'.";
    /* Default value map */
    private static Map<String,Object> defmap = null;
    /**
     * Constructor
     */
    public PortalSSILocationRuleFactory() {
    }
    /**
     * Location rule factory method
     * 
     * @return new rule instace
     */
    public LocationRule createLocationRule() {
        return new PortalSSILocationRule(this);
    }
    /**
     * Get factory ID - must be distinct and not change for a given factory
     */
    public String getID() {
        return FACTORY_ID;
    }
    /**
     * Get ordered list of attribute IDs required by Location Rule instances
     * created by this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getLocationRuleAttributeIDs() {
        return ATTRIBS;
    }
    /**
     * Get label for location rule factory - describes type of rule supported
     * 
     * @return label
     */
    public String getLocationRuleFactoryLabel() {
        return LABEL;
    }
    /**
     * Get description for location rule factory - more verbose than label.
     * 
     * @return description
     */
    public String getLocationRuleFactoryDescription() {
        return DESC;
    }
    /**
     * Get default attribute set for a rule. This method should be used to
     * initialize the attribute set ultimately passed to the Location Rule
     * instances, in order to handle cases where code updates add new
     * attributes.
     * 
     * @return default attribute set (read-only)
     */
    public Map<String, Object> getDefaultLocationRuleAttributes() {
        if(defmap == null) {
            defmap = new HashMap<String, Object>();
            for (int i = 0; i < ATTRIBS.length; i++)
                defmap.put(ATTRIBS[i], DEFAULTS[i]);
        }
        return new HashMap<String, Object>(defmap);    
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
