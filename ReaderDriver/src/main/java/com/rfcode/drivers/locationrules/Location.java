package com.rfcode.drivers.locationrules;

import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.ReaderEntity;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.BadParameterException;

import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/**
 * Object representing a location, and acting as a container for a user-defined
 * set of attribute values for that location. Locations are arranged into
 * hierarchies, allowing for simple containment relationships (i.e the parent of
 * a location representing a rack might be a location representing the whole
 * room containing the rack and other racks). Attributes are aggregated from
 * parent to child, so setting an attribute on a parent causes the value to
 * appear as the default for that attrbiute on all its children.
 * 
 * @author Mike Primm
 */
public class Location implements ReaderEntity {
    private Map<String, Object> attribs;
    private Map<String, Object> attribs_ro; /* Read only facade */
    private Location parent;
    private ArrayList<Location> children;
    private List<Location> children_ro;
    private String id;
    private boolean did_init;
    private boolean is_ir_domain;   /* True if defines IR domain */
    private Location in_ir_domain;  /* Set if location is in IR domain */
    
	public static final String LOCATTRIB_EXCLUDETAGS = "excludedtags";	/* Comma separated list of tag IDs to exclude from matching location */
    public static final String LOCATTRIB_DEFINESIRDOMAIN = "definesirdomain"; /* Boolean - true if location is top node of IR domain */
	private Set<String> excludedtags = Collections.emptySet();
    /**
     * Constructor
     */
    public Location() {
        attribs = Collections.emptyMap();
        attribs_ro = attribs; 
        children = null;
        children_ro = Collections.emptyList();
        did_init = false;
    }
    /**
     * Set parent location for this location
     * 
     * @param parent -
     *            parent location (null=no parent)
     */
    public void setParent(Location parent) throws BadParameterException {
        /* Make sure parent isn't going to cause loop */
        Location p = parent;
        while (p != null) {
            if (p == this)
                throw new BadParameterException("Bad parent : causes loop");
            p = p.parent;
        }
        if ((this.parent != null) && (this.parent.children != null)) {
            this.parent.children.remove(this);
            if (this.parent.children.isEmpty()) {
                this.parent.children = null;
                this.parent.children_ro = Collections.emptyList();;
            }
        }
        this.parent = parent;
        if (parent != null) {
            if (parent.children == null) {
                parent.children = new ArrayList<Location>();
                parent.children_ro = Collections.unmodifiableList(parent.children);
            }
            parent.children.add(this);
        }
        /* If we don't define IR domain, apply new parent's domain to us if needed */
        if(!is_ir_domain) {
            if ((parent == null) || (parent.in_ir_domain == null)) {
                setIRDomainRecursive(null);
            }
            else {
                setIRDomainRecursive(parent.in_ir_domain);
            }
        }
    }
    /**
     * Get parent location for this location
     * 
     * @return parent (null=none)
     */
    public Location getParent() {
        return parent;
    }
    /**
     * Get ID of entity - unique among all reader entities in system
     * 
     * @return ID
     */
    public String getID() {
        return id;
    }
    /**
     * Assign unique ID to entity - does not apply until init() invoked
     * 
     * @param id -
     *            ID to be assigned
     */
    public void setID(String id) {
        this.id = id;
    }
    /**
     * Initialization method - must be called once attributes set. init() method
     * MUST call addEntity() to add entity to directory
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        if (id == null)
            throw new BadParameterException("ID not set");
        ReaderEntityDirectory.addEntity(this);
        did_init = true;
        /* Call lifecycle listeners */
        for(LocationLifecycleListener listen : LocationRuleEngine.getLocationLifecycleListeners()) {
            listen.locationLifecycleCreate(this);
        }
    }
    /**
     * Cleanup method. cleanup() method MUST call removeEntity() to remove
     * entity from directory
     */
    public void cleanup() {
        /* Call lifecycle listeners */
        for(LocationLifecycleListener listen : LocationRuleEngine.getLocationLifecycleListeners()) {
            listen.locationLifecycleDelete(this);
        }
        did_init = false;
        /* Make sure we're not setting IR domain */
        if(is_ir_domain)
            setDefinesIRDomain(false);
        /* Remove ourselves as parent of our children : pass to our parent */
        ArrayList<Location> clist = children;
        children = null;
        children_ro = Collections.emptyList();
        if(clist != null) {
            for(Location child : clist) {
                child.parent = null;
                if(parent != null) {
                    try {                    
                        child.setParent(parent);
                    } catch (BadParameterException bpx) {
                    }
                }
            }
        }
        /* Disconnect from our parent too */
        try {
            setParent(null);
        } catch (BadParameterException bpx) {
        }
        ReaderEntityDirectory.removeEntity(this);
    }
    /**
     * Get attribute values - not including inherited values
     * 
     * @return copy of our attributes (read-only)
     */
    public Map<String, Object> getAttributes() {
        return attribs_ro;
    }
    /**
     * Get attribute values - including inherited values
     * 
     * @return copy of our attributes, combined with parent's attributes
     */
    public Map<String, Object> getFullAttributes() {
        Map<String, Object> rslt;
        if (parent != null) {
            rslt = parent.getFullAttributes();
            rslt.putAll(attribs); /* Add our over ones from parent */
        } else {
            rslt = new HashMap<String,Object>(attribs);
        }
        return rslt;
    }
    /**
     * Set attribute values - any defined values will supercede those inherited
     * from the parent.
     * 
     * @param attr -
     *            attributes to be set
     */
    public void setAttibutes(Map<String, Object> attr) {
        Map<String, Object> oldattr = attribs;
        attribs = new HashMap<String, Object>(attr);
        attribs_ro = Collections.unmodifiableMap(attribs);
		/* Check for excluded tag list : parse it */
		Object v = attribs.get(LOCATTRIB_EXCLUDETAGS);
		Object oldv = oldattr.get(LOCATTRIB_EXCLUDETAGS);
		if(v != oldv) {
			Set<String> newexcluded = null;
			if((v != null) && (v instanceof String)) {
				String[] lst = ((String)v).split(",");
				for(String s : lst) {
					if(newexcluded == null)
						newexcluded = new HashSet<String>();
					newexcluded.add(s);
				}
			}
			if(newexcluded == null) newexcluded = Collections.emptySet();
			Set<String> oldexcluded = excludedtags;
			excludedtags = newexcluded;	/* Update value */

			/* If we're active and have changed, figure out who we need to trigger reevaluation on */
			if(did_init && (newexcluded.equals(oldexcluded) == false)) {
				/* Figure out tags that changed */
				Set<String> chg = new HashSet<String>(oldexcluded); /* Start with old values */
				for(String id : newexcluded) {	/* Go through new ones, remove dups and add non-dups */
					if(chg.contains(id))
						chg.remove(id);
					else
						chg.add(id);
				}
				/* If non-empty, find which rules to trigger on them */
				if(chg.size() > 0) {
					LocationRuleEngine eng = LocationRuleEngine.getLocationRuleEngine();
					/* Call location engine to trigger rules for this location */
					for(LocationRule r : ReaderEntityDirectory.getLocationRules()) {
						Location rl = r.getRuleTargetLocation();
						if((rl != null) && (rl.isDescendedFrom(this))) {	/* If matches us, need to trigger it */
							for(String tid : chg) {
								Tag t = TagGroup.findTagInAnyGroup(tid);	/* See if it exists yet */
								if(t != null) {
									eng.triggerRule(t, r);	/* Trigger rule for tag */
								}
							}
						}
					}
				}
			}
		}
		v = attribs.get(LOCATTRIB_DEFINESIRDOMAIN);
        if (v == null) {
            v = Boolean.FALSE;
        }
        else if ((v instanceof Boolean) && (((Boolean)v).booleanValue() == false)) {
            attribs.remove(LOCATTRIB_DEFINESIRDOMAIN);
        }
		oldv = oldattr.get(LOCATTRIB_DEFINESIRDOMAIN);
        if (oldv == null) oldv = Boolean.FALSE;
        if(!v.equals(oldv)) {
            setDefinesIRDomain(((Boolean)v).booleanValue());
        }    
        /* Call lifecycle listeners */
        if(did_init) {
            for(LocationLifecycleListener listen : LocationRuleEngine.getLocationLifecycleListeners()) {
                listen.locationLifecycleAttributeChange(this, oldattr);
            }
        }
    }
    /**
     * Get children of this location, if any
     * 
     * @return list of children (read-only)
     */
    public List<Location> getChildren() {
        return children_ro;
    }
    /**
     * Get all chilren (recursive)
     * 
     * @return list of all children, grandchildren, etc.
     */
    public List<Location> getChildrenRecursive() {
        List<Location> rslt;
        if (children != null) {
            rslt = new ArrayList<Location>();
            doGetChildrenRecursive(rslt);   /* Add our children */
        }
        else {
            rslt = Collections.emptyList();
        }
        return rslt;
    }
    private void doGetChildrenRecursive(List<Location> accum) {
        if(children == null)
            return;
        accum.addAll(children);  /* Add our children */
        /* And recurse over them to add theirs */
        for(Location child : children) {
            child.doGetChildrenRecursive(accum);
        }
    }
    /**
     * Get location path (i.e. "grandparent/parent/child")
     * 
     * @return '/' separated parent->child path
     */
    public String getPath() {
        if (parent != null) {
            return parent.getPath() + "/" + id;
        }
        return id;
    }
	/**
	 * Test if tag is excluded from this location (or any of its parents)
	 */
	public boolean isTagExcluded(String tagid) {
		boolean rc = false;
		if(excludedtags.contains(tagid)) {
			rc = true;
		}
		else if(parent != null)
			rc = parent.isTagExcluded(tagid);
		return rc;
	}
	/*
	 * Test if matching location, or ancestor of given location
	 * @return true if provided location is equal to or ancestor of this location
	 */
	public boolean isDescendedFrom(Location loc) {
		if(loc == null) return false;
		Location me = this;
		while(me != null) {
			if(me == loc) return true;
			me = me.parent;
		}
		return false;
	}
    /**
     * Get IR domain associated with location
     * @return location defining domain, or null if not under domain
     */
    public Location getIRDomain() {
        return in_ir_domain;
    }
    private void setIRDomainRecursive(Location irdom) {
        if(is_ir_domain) return;    /* Quit if we define one for ourselves */
        in_ir_domain = irdom;   /* Set self */
        if(children != null) {
            for(Location c : children) {
                c.setIRDomainRecursive(irdom);
            }
        }
    }
    /**
     * Set whether location defines IR Domain
     * @param is_domain - true if defines domain, false if not    
     */
    private void setDefinesIRDomain(boolean is_domain) {
        if(is_ir_domain == is_domain)
            return;
        is_ir_domain = is_domain;
        /* Update in_ir_domain for child heirarchy */
        if (is_ir_domain)
            in_ir_domain = this;
        else
            in_ir_domain = null;
        if(children != null) {
            for(Location c : children) {
                c.setIRDomainRecursive(in_ir_domain);
            }
        }
    }
    /**
     * Test whether location defines IR domain
     * @return true if it defines an IR domain, false if not
     */
    public boolean getDefinesIRDomain() {
        return is_ir_domain;
    }
}
