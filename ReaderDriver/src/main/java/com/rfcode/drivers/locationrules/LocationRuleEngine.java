package com.rfcode.drivers.locationrules;

import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.LinkedList;
import java.util.ArrayList;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagLinkStatusListener;
import com.rfcode.drivers.readers.TagStatusListener;
import com.rfcode.drivers.readers.TagAndLinkStatusListener;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.ranger.RangerProcessor;

/**
 * Core engine and API for location rule implementations. Provides common
 * services and processing for implementation of location rules.
 * 
 * @author Mike Primm
 */
public class LocationRuleEngine {
    /* Our session factory */
    private SessionFactory sf;
    /* List of our global location lifecycle listeners */
    private static ArrayList<LocationLifecycleListener> location_listeners = 
        new ArrayList<LocationLifecycleListener>();
    /* List of our global location rule exception listeners */
    private static ArrayList<LocationRuleExceptionListener> exception_listeners = 
        new ArrayList<LocationRuleExceptionListener>();
    /*
     * Update processing queue - round-robin list of delayed processing elements
     * used to give the updates from multiple readers time to be processed
     * before triggering a re-evaluation of the location rules for a given tag
     */
    private static final int TIME_DELAY_INCREMENT = 100; /* 100 ms increment */
    private static final int TIME_DELAY_COUNT = 10; /* Delay processing 1000 ms */
    /* Base processing delay for triggers (in msec) */
    public static final int TRIGGER_DELAY = TIME_DELAY_INCREMENT
        * TIME_DELAY_COUNT;
    /* Time to add to actual last GPS timestamp for lastgpsts - avoid duplicating update times that spaces out AM/SM */
    private static final long   LASTGPSTS_SHIFT = 100;

    private LinkedList<TagUpdate> update_queues; /* Update queue */
    private HashMap<String, TagUpdate> updatesbytag; /*
                                                         * Map of pending
                                                         * updates, by tag guid
                                                         */
    private int current_tick; /* Current time tick */
    private boolean is_active = false; /* Are we active? */
    /** Table of existing attribute listeners */
    private HashMap<String, TagAttribListener> tag_attrib_listeners;

    /** Table of expected tag locations, keyed by tag ID */
    private HashMap<String, List<String>> exp_tag_loc;
    /** Bonus to confidence for rules associated with expected location */
    private static final double EXP_LOC_BUMP_DEFAULT = 0.0000001;
    private double  exp_loc_bump = EXP_LOC_BUMP_DEFAULT;

	private static final double SMALL_DIFF = 0.1 * EXP_LOC_BUMP_DEFAULT;
    /* Simple cache for tag type attributes we look up ALOT */
	private HashMap<String, int[]> indx_cache = new HashMap<String, int[]>();
	private static final int CACHE_CONF = 0;
	private static final int CACHE_GPSID = 1;
	private static final int CACHE_LASTGPSID = 2;
	private static final int CACHE_LASTGPSTS = 3;
	private static final int CACHE_ZONE = 4;	/* Needs to be last */
	private static final int CACHE_LEN = 5;

    private static final Double NoMatchConf = Double
        .valueOf(LocationRule.NOMATCH);

    /* Simple set object to track a list of rules */
    private static class LocationRuleSet implements Iterable<LocationRule> {
        private IdentityHashMap<LocationRule, Boolean> rules = new IdentityHashMap<LocationRule, Boolean>();

        public void add(LocationRule toAdd) {
            rules.put(toAdd, Boolean.TRUE);
        }

        public void addAll(LocationRuleSet toAdd) {
            for (LocationRule rule : toAdd) {
                rules.put(rule, Boolean.TRUE);
            }
        }

        public void remove(LocationRule rule) {
            rules.remove(rule);
        }

        public void clear() {
            rules.clear();
        }

        public boolean contains(LocationRule rule) {
            return rules.containsKey(rule);
        }

        public int size() {
            return rules.size();
        }

        public Iterator<LocationRule> iterator() {
            return rules.keySet().iterator();
        }
    }

    /* Tag update element - accumulates set of rules to be fired for this tag */
    private static class TagUpdate {
        Tag tag; /* Tag with pending updates */
        LocationRuleSet rules; /* Rules needing to be evaluated */
        int target_tick; /* Target tick to complete our delay */
        int change_cnt; /* Number of periods update has been held */
        String pend_change; /* Pending changed value */
    }
    /* Our session action - handles periodic callbacks from session */
    private class SessionAct implements Runnable {
        public SessionAct() {
            /* Add delayed action to session handler */
            RangerProcessor.addDelayedAction(this, TIME_DELAY_INCREMENT);
        }
        public void run() {
            if (!is_active) { /* No longer active? */
                return; /* Quit */
            }
            handleSessionAction();
            /* Re-enqueue our action */
            /* Add delayed action to session handler */
            RangerProcessor.addDelayedAction(this, TIME_DELAY_INCREMENT);
        }
    }
    /**
     * Rule trigger interface - defined interface for cleaning up registered
     * trigger
     */
    public interface RuleTrigger {
        /**
         * Unregister rule trigger
         */
        public void unregisterRuleTrigger(LocationRule rule);
    }
    /**
     * TagLink listener for triggering specific rule
     */
    private class TagLinkListener implements TagLinkStatusListener, RuleTrigger {
        private LocationRule rule;
        private ReaderChannel channel;
        /**
         * Tag link attributes changed notification - called when any of the tag
         * link's attributes have been changed from their previous values.
         * 
         * @param taglink -
         *            tag link with change attributes
         */
        public void tagLinkStatusAttributeChange(TagLink taglink) {
            triggerRule(taglink.getTag(), rule);
        }
        /**
         * TagLink added notificiation. Called after taglink has been added to
         * tag.
         * 
         * @param tag -
         *            tag with new link
         * @param taglink -
         *            link added to tag
         */
        public void tagLinkAdded(Tag tag, TagLink taglink) {
            triggerRule(tag, rule);
        }
        /**
         * TagLink removed notificiation. Called after taglink has been removed
         * from tag.
         * 
         * @param tag -
         *            tag with removed link
         * @param taglink -
         *            link removed from tag
         */
        public void tagLinkRemoved(Tag tag, TagLink taglink) {
            triggerRule(tag, rule);
        }
        /**
         * Unregister rule trigger
         */
        public void unregisterRuleTrigger(LocationRule rule) {
            channel.removeTagLinkListener(this);
            /* Now, see if any tags already on channel - need to force trigger */
            List<TagLink> tags = new ArrayList<TagLink>(channel.getTagLinks().values());
            for (TagLink tagl : tags) {
                triggerRule(tagl.getTag(), rule);
            }
        }
    }
    /**
     * Tag attribute listener for triggering specific rule
     */
    private class TagAttribListener implements RuleTrigger {
        private ArrayList<TagTypeAttribListener> listeners;
        private LocationRuleSet rules;
        private HashMap<Object, LocationRuleSet> rules_by_attribval;
        private IdentityHashMap<LocationRule, Object> attribval_by_rule;
        private LocationRuleSet anychange_rules;
        private String attribid;

        TagAttribListener(String aid) {
            rules = new LocationRuleSet();
            rules_by_attribval = new HashMap<Object, LocationRuleSet>();
            attribval_by_rule = new IdentityHashMap<LocationRule, Object>();
            anychange_rules = new LocationRuleSet();
            listeners = new ArrayList<TagTypeAttribListener>();
            attribid = aid;
        }
        /**
         * Unregister rule trigger
         */
        public void unregisterRuleTrigger(LocationRule rule) {
            /* Remove from rules set */
            rules.remove(rule);
	    anychange_rules.remove(rule);
	    Object v = attribval_by_rule.get(rule);
            if(v != null) {
		attribval_by_rule.remove(rule);
                LocationRuleSet lrs = rules_by_attribval.get(v);
                if(lrs != null) {
                   lrs.remove(rule);
                   if(lrs.size() == 0) {	/* Empty? */
                      rules_by_attribval.remove(v);
                   }
                }
            }
            if (rules.size() > 0) { /* Still more? */
                return; /* Nothing more to do */
            }
            /* Remove from table */
            tag_attrib_listeners.remove(attribid);
            /* Unregister listeners */
            for (TagTypeAttribListener ttal : listeners) {
                ttal.tagtype.removeTagLifecycleListener(ttal);
            }
            listeners.clear();
        }

        public void tagLifecycleChange(TagUpdate upd) {
            Tag tag = upd.tag;
            int attrib_idx = tag.getTagType().getTagAttributeIndex(attribid);
            if (attrib_idx != -1) {
                Object val = tag.readTagAttribute(attrib_idx);
                if (val != null) {
                    LocationRuleSet rules = rules_by_attribval.get(val);
                    if (rules != null) {
                        upd.rules.addAll(rules);
                    }
                }
                upd.rules.addAll(anychange_rules);
            }
        }

	public void addRule(LocationRule rule, Object val) {
	    rules.add(rule);
	    if(val != null) {
                LocationRuleSet lrs = rules_by_attribval.get(val);
		if(lrs == null) {
                    lrs = new LocationRuleSet();
	    	    rules_by_attribval.put(val,lrs);
                }
                lrs.add(rule);
	        attribval_by_rule.put(rule, val);
            } else {
		anychange_rules.add(rule);
	    }
        }
	public void doValueTrigger(Tag tag, Object v, Object oldv) {
	    /* Trigger rules that didn't match but now do */
            LocationRuleSet lrs = rules_by_attribval.get(v);
            if(lrs != null)
		triggerRules(tag, lrs);
	    /* Trigger rules that did match but don't */
	    lrs = rules_by_attribval.get(oldv);
            if(lrs != null)
                triggerRules(tag, lrs);
	    /* Trigger rules that want any change */
	    if(anychange_rules.size() > 0)
		triggerRules(tag, anychange_rules);
	}
    }
    /**
     * TagType specific listeners - pre-resolves the attribute index for each
     * tag type
     */
    private class TagTypeAttribListener implements TagStatusListener {
        int attrib_idx;
        private TagAttribListener tagattriblistener;
        TagType tagtype;
        /**
         * Constructor
         */
        TagTypeAttribListener(TagType tt, int a_idx, TagAttribListener tal) {
            tagtype = tt;
            attrib_idx = a_idx;
            tagattriblistener = tal;
        }
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than
         *            the number of attributes for the given tag). Ordered to
         *            match attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
            Object v = tag.readTagAttribute(attrib_idx);
            if (v != oldval[attrib_idx]) { /* Ours changed? */
		tagattriblistener.doValueTrigger(tag, v, oldval[attrib_idx]);
            }
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
        }
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
            triggerRulesForTagLifecycleChange(tag, tagattriblistener); /* Make sure we see lost links */
        }
        public void tagLifecycleCreate(Tag tag, String cause) {
            triggerRulesForTagLifecycleChange(tag, tagattriblistener);
        }
        public void tagLifecycleDelete(Tag tag) {
            /* Remove pending update */
            TagUpdate tu = updatesbytag.remove(tag.getTagGUID());
            if (tu != null)
                update_queues.remove(tu);
        }
    }

    /** Map of location rule lists, by tag ID */
    private HashMap<String, List<LocationRule>> rules_by_tagid = 
        new HashMap<String, List<LocationRule>>();
    /** Our listener singleton */
    private TagIDListener our_listener = new TagIDListener();

    /**
     * TagLink listener for triggering specific rules for specific tag IDs
     */
    private class TagIDLinkListener implements RuleTrigger {
        /** List of tag IDs for given rule trigger */
        private List<String> tagids; 

        /** Constructor - given list of tag IDs to watch for rule */
        public TagIDLinkListener(Set<String> tags, LocationRule rule) {
            tagids = new ArrayList<String>(tags);
            for(String tagid : tagids) {
                List<LocationRule> lst = rules_by_tagid.get(
                    tagid);
                if(lst == null) {
                    lst = new ArrayList<LocationRule>();
                    rules_by_tagid.put(tagid, lst);
                }
                lst.add(rule);      /* Add rule to list */
            }
        }
        /**
         * Unregister rule trigger
         */
        public void unregisterRuleTrigger(LocationRule rule) {
            for(String tagid : tagids) {
                List<LocationRule> lst = rules_by_tagid.get(
                    tagid);
                if(lst != null) {
                    lst.remove(rule);       /* Remove rule */
                    if(lst.size() == 0) {   /* Empty? */
                         rules_by_tagid.remove(tagid);   /* Remove it */
                    }
                }
            }
        }
    }

    /* Internal listener - singleton to prevent too many with large rule set */
    private class TagIDListener implements TagAndLinkStatusListener {
        /**
         * Tag attributes changed notification - called when any of the tag's
         * attributes have been changed from their previous values.
         * 
         * @param tag -
         *            tag with change attributes
         * @param oldval -
         *            array of previous tag attribute values (may be longer than
         *            the number of attributes for the given tag). Ordered to
         *            match attribute IDs from TagType.getTagAttributes()
         */
        public void tagStatusAttributeChange(Tag tag, Object[] oldval) {
        }
        /**
         * Tag triggered message notification - called when a tag has been
         * triggered to send a command message
         * 
         * @param tag - tag
         * @param cmd - command ID
         * @param attrids - list of attribute IDs
         * @param attrvals - list of attribute vals
         */
        public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, 
            Object[] attrvals) {
        }
        public void tagStatusLinkAdded(Tag tag, TagLink taglink) {
            List<LocationRule> lst = rules_by_tagid.get(
                tag.getTagGUID());
            if(lst != null) {
                for(LocationRule rule : lst) {
                    triggerRule(tag, rule);
                }
            }
        }
        public void tagStatusLinkRemoved(Tag tag, TagLink taglink) {
            List<LocationRule> lst = rules_by_tagid.get(
                tag.getTagGUID());
            if(lst != null) {
                for(LocationRule rule : lst) {
                    triggerRule(tag, rule);
                }
				/* If we have no links left, check if we have a gpsid */
				/* Check here, since we can't have a GPS match if we have no rules matching */
				if(tag.getTagLinkCount() == 0) {	
					int[] idx = getCachedTTIdx(tag.getTagType());
					String gpsid = (String)tag.readTagAttribute(idx[CACHE_GPSID]);
					if((gpsid != null) && (gpsid.equals(LocationRuleFactory.GPSID_NOMATCH) == false)) {
						GPS gps = ReaderEntityDirectory.findGPS(gpsid);
                        /* Try to get last GPS data for this tag */
                        long ts = 0;
                        if(gps != null) ts = gps.getLastGPSTimestampForTag(tag.getTagGUID());
                        long now = System.currentTimeMillis();
                        if(ts != 0) {  /* Got last value for GPS - try to use it for last ts */
                            /* In case we only got one report, we want to avoid having lastgpsts = the one report (freaks out AM/SM) */
                            if((ts + LASTGPSTS_SHIFT) < now)
                                ts = ts + LASTGPSTS_SHIFT;  /* Add 0.1 second - below quanta of update times for GPS, so no real error */
                            else
                                ts = 1 + ((ts + now) / 2);    /* Else, split the difference, but make sure it is different */
                        }
                        if(ts <= 0) {  /* No timestamp - estimate from tag ageout */
    						Reader rdr = null;
	    					if(gps != null) rdr = gps.getReader();
	    					ts = now;
	    					if(rdr != null) {
	    						ts = ts - (1000*rdr.getTagTimeout()) + LASTGPSTS_SHIFT;	/* Use ageout to adjust + shift forward 100ms to avoid dup */
	    					}
                        }
						/* Update GPSID and LASTGPSTS */
						tag.updateTagAttributes(new int[] { idx[CACHE_GPSID], idx[CACHE_LASTGPSTS] },
							 new Object[] { LocationRuleFactory.GPSID_NOMATCH,	Long.valueOf(ts) }, 2);
					}
				}
            }
        }
        public void tagLifecycleCreate(Tag tag, String cause) {
            boolean did_trigger = false;
            List<LocationRule> lst = rules_by_tagid.get(
                tag.getTagGUID());
            if(lst != null) {
                for(LocationRule rule : lst) {
                    triggerRule(tag, rule);
                    did_trigger = true;
                }
            }
            if(!did_trigger) {  /* If none, just force one to let location and gps get primed */
                triggerRule(tag, null);
            }
			Tag ptag = tag.getParentTag();
			if(ptag != null) {	/* If new subtag */
				triggerRule(tag, null);	/* Copy parent's info into subtags - but not during create */
			}
        }
        public void tagLifecycleDelete(Tag tag) {
            List<LocationRule> lst = rules_by_tagid.get(
                tag.getTagGUID());
            if(lst != null) {
                for(LocationRule rule : lst) {
                    triggerRule(tag, rule);
                }
            }
        }
        /**
         * Tag link attributes changed notification - called when any of the tag
         * link's attributes have been changed from their previous values.
         * 
         * @param taglink -
         *            tag link with change attributes
         */
        public void tagLinkStatusAttributeChange(TagLink taglink) {
            Tag tag = taglink.getTag();
            List<LocationRule> lst = rules_by_tagid.get(
                tag.getTagGUID());
            if(lst != null) {
                for(LocationRule rule : lst) {
                    triggerRule(tag, rule);
                }
            }
        }
    }
    /** Our singleton */
    private static LocationRuleEngine engine;
    /**
     * Get location rule engine (currently a singleton)
     * 
     * @return engine
     */
    public static LocationRuleEngine getLocationRuleEngine() {
        if (engine == null)
            engine = new LocationRuleEngine();
        return engine;
    }
    /**
     * Our constructor
     */
    private LocationRuleEngine() {
        update_queues = new LinkedList<TagUpdate>();
        updatesbytag = new HashMap<String, TagUpdate>();
        current_tick = 0;

        exp_tag_loc = new HashMap<String, List<String>>();
    }
    /**
     * Set our session factory
     * 
     * @param fact -
     *            session factory
     */
    public void setSessionFactory(SessionFactory fact) {
        sf = fact;
    }
    /**
     * Get our session factory
     */
    public SessionFactory getSessionFactory() {
        return sf;
    }
    /**
     * Init method for factory - needs to be called once when all settings are
     * ready
     */
    public void init() throws BadParameterException {
        if (sf == null) {
            throw new BadParameterException("Missing session factory");
        }
        /* Now, register the location attribute with all the tag types */
        for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
            /* Register the zone location attribute */
            tt.registerTagAttribute(LocationRuleFactory.ATTRIB_ZONELOCATION,
                LocationRuleFactory.ZONELOCATION_DEFAULT,
                LocationRuleFactory.ZONELOCATION_LABEL);
            /* Register the gpsid attribute */
            tt.registerTagAttribute(LocationRuleFactory.ATTRIB_GPSID,
                LocationRuleFactory.GPSID_DEFAULT,
                LocationRuleFactory.GPSID_LABEL);
            /* Register the lastgpsid attribute */
            tt.registerTagAttribute(LocationRuleFactory.ATTRIB_LASTGPSID,
                LocationRuleFactory.GPSID_DEFAULT,
                LocationRuleFactory.LASTGPSID_LABEL);
            /* Register the lastgpsts attribute */
            tt.registerTagAttribute(LocationRuleFactory.ATTRIB_LASTGPSTS,
                AbstractTagType.NULL_DEFAULT_LONG, LocationRuleFactory.LASTGPSTS_LABEL);
            /* Register the rule confidence map */
            tt.registerTagAttribute(
                LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE,
                LocationRuleFactory.CONFIDENCE_BY_RULE_DEFAULT,
                LocationRuleFactory.CONFIDENCE_BY_RULE_LABEL);
        }
        is_active = true; /* We're active */
        /* And set up periodic job with the session scheduler */
        new SessionAct();
        /* Add global listener */
        AbstractTagType.addGlobalTagLifecycleListener(our_listener);
    }
	private int[] getCachedTTIdx(TagType tt) {
		int[] ttidx = indx_cache.get(tt.getID());
		if(ttidx == null) {
			ttidx = new int[CACHE_LEN];
            ttidx[CACHE_CONF] = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE);
			ttidx[CACHE_ZONE] = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_ZONELOCATION);
            ttidx[CACHE_GPSID] = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_GPSID);
            ttidx[CACHE_LASTGPSID] = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_LASTGPSID);
            ttidx[CACHE_LASTGPSTS] = tt.getTagAttributeIndex(LocationRuleFactory.ATTRIB_LASTGPSTS);
			indx_cache.put(tt.getID(), ttidx);
		}
		return ttidx;
	}
    /**
     * Apply confidence bump - prevent non-1.0 confidence from becoming more than 1.0
     */
    private double applyBump(double rawconf, double bump) {
        if(bump == 0.0) return rawconf;
        if(rawconf < 1.0) {
            double v = rawconf + bump;
            if(v > 0.999) { /* If above 0.999, scale extra to preserve relative order without exceeding 1.0 */
                if(v >= 1.999) v = 1.999;    /* Cap at 1.999 */
                v = 0.999 + (0.0009 * (v - 0.999));
            }
            return v;
        }
        /* Allow 1.0 confidence to bump normally */
        return rawconf + bump;
    }
    /**
     * Handle periodic callback - this drives our processing
     */
    @SuppressWarnings("unchecked")
	private void handleSessionAction() {
        TagUpdate update;

        current_tick++; /* Bump the current tick count */
        do {
            update = update_queues.peek(); /* Check the first one */
            if (update == null) { /* If nothing, we're done */
                continue;
            }
            /* If too soon for the update, we're done */
            if (update.target_tick > current_tick) {
                update = null;
                continue;
            }
            update = update_queues.poll();  /* Actually remove it */
            /* Remove update from by-tag table */
            updatesbytag.remove(update.tag.getTagGUID());
            /* If tag is no longer valid, just quit */
            if (update.tag.isValid() == false)
                continue;
            /* If its a subtag, just update from parent */
            if(update.tag.getParentTag() != null) {
				updateSubTag(update.tag);	/* Copy parent's info into subtags */
                continue;
            }
            /* Get attribute indexes for confidence and location */
			int[] cached_idx = getCachedTTIdx(update.tag.getTagType());
            int conf_idx = cached_idx[CACHE_CONF];
            /* Get our existing confidence vector */
            boolean change = false;
            Map<LocationRule, Double> conf = null;
            Map<LocationRule, Double> oldconf;
            Object val = update.tag.readTagAttribute(conf_idx);
            if (val == null) { /* If null, quit */
                continue;
            }
            oldconf = (Map<LocationRule, Double>) val;
//			System.out.println("tag " + update.tag.getTagGUID());
            /* Get current location */
            String curloc = (String)update.tag.readTagAttribute(cached_idx[CACHE_ZONE]);
            /* Check if tag is in IR domain zone */
            Location current_location = ReaderEntityDirectory.findLocation(curloc);
            Location ir_domain = null;
            if (current_location != null) {
                ir_domain = current_location.getIRDomain();
                if(ir_domain != null) { /* If in IR domain, see if tag is IR */
                    if(update.tag.isIRDetectingTag() == false) {   /* If not, ignore domain - we want all rules active */
                        ir_domain = null;
                    }
                }
            }
            /* Get ready to remember updated rules */
            LocationRuleSet upd_set = null;
            /* Now loop through our changed rules */
            for (LocationRule rule : update.rules) {
                Double oldval;
                /* Get old value */
                oldval = oldconf.get(rule);
                if (oldval == null) {
                    oldval = NoMatchConf;
                }
                /* Evaluate the rule for the tag */
                double matchconf = LocationRule.NOMATCH;
                Location ruletarget = rule.getRuleTargetLocation();
                if(rule.getRuleEnabled()) {
                    matchconf = rule.evaluateRuleForTag(update.tag, oldval.doubleValue());
					if(matchconf > LocationRule.NOMATCH) {		/* If match, see if we need to suppress (most rules don't match most tags) */
						if((ruletarget != null) && ruletarget.isTagExcluded(update.tag.getTagGUID())) { /* If tag is excluded from this location */
							matchconf = LocationRule.NOMATCH;
                        }
					}
				}
//				System.out.println("rule " + rule.getID() + ": match=" + matchconf);
                if (matchconf <= LocationRule.NOMATCH) { /* If miss */
                    /* Was in map? */
                    if (!oldval.equals(NoMatchConf)) {
                        /* Lazy copy if needed */
                        if (conf == null) {
                            conf = new HashMap<LocationRule, Double>(oldconf);
                        }
                        /* Remove from map */
                        conf.remove(rule);
                        change = true; /* Map changed */
                    }
                } else { /* Else, possible hit */
                    /* If not defined, or different */
                    if (Math.abs(oldval.doubleValue()-matchconf) > SMALL_DIFF) {
                        /* Lazy copy if needed */
                        if (conf == null) {
                            conf = new HashMap<LocationRule, Double>(oldconf);
                        }
                        /* Put value in map */
                        conf.put(rule, Double.valueOf(matchconf));
                        change = true; /* Map changed */
                        /* And remember it */
                        if (upd_set == null) {
                            upd_set = new LocationRuleSet();
                        }
                        upd_set.add(rule);
                    }
                }
            }
            if(!change) {   /* If no change, see if still default location or gps */
                if(update.tag.isDefaultFlagged(cached_idx[CACHE_ZONE]) || update.tag.isDefaultFlagged(cached_idx[CACHE_GPSID])) {
                    change = true;
                }
            }
            /* If anything changed, need to evaluate */
            if (change) {
                double best = -1.0; /* Find best for location */
                double bestgps = -1.0; /* Find best for GPS rules */
                LocationRule bestrule = null;
                GPSRule bestgpsrule = null;
				List<String> exp = null;
//				System.out.println("change=true");
                if(exp_loc_bump > 0) {
					exp = exp_tag_loc.get(update.tag.getTagGUID());
					if((exp != null) && (exp.size() == 0)) {
						exp = null;
                    }
				}

                double match_boost = update.tag.getTagGroup().getLocationMatchBoost();

                if(conf == null) {
                    conf = Collections.emptyMap();
                }

                for(Map.Entry<LocationRule, Double> row : conf.entrySet()) {
                    double v = row.getValue().doubleValue();
                    double vbump = 0.0;
					LocationRule rule = row.getKey();
					Location l = rule.getRuleTargetLocation();
                    boolean isgpsrule = rule instanceof GPSRule;

                    /* If we're in IR domain, and rule corresponds to location other than current domain, skip it (cannot win) */
                    if((!isgpsrule) && (!rule.isIRDomainDetermining()) && (l.getIRDomain() != null) && (ir_domain != null) && (l.getIRDomain() != ir_domain)) {
                        continue;
                    }
                    /* See if rule corresponds to expected for tag */
					if(exp != null) {
	                    while(l != null) {
       		                if(exp.contains(l.getID())) {    /* Hit */
       		                    vbump = exp_loc_bump;  /* Add bump */
                                l = null;   /* We're done */
                            } else {
   		                        l = l.getParent();  /* Continue up */
                            }
   		                }
					}
					if(isgpsrule) {	/* GPS rule? */
                        v = applyBump(v, vbump);
	                    if (v > bestgps) { /* Definitely better */
    	                    bestgpsrule = (GPSRule)rule;
    	                    bestgps = v;
    	                } else if (Math.abs(v-bestgps) < SMALL_DIFF) { /* Same? */
    	                    /* If its one of our changes, assume its better */
    	                    if ((upd_set != null)
    	                        && (upd_set.contains(rule))) {
    	                        bestgpsrule = (GPSRule)rule;
    	                    }
    	                }
                    } else {
                        /* For non-GPS rules, see about match boost */
                        if((match_boost > 0.0) && (!curloc.equals(""))) {
                            l = rule.getRuleTargetLocation();
                            if(l.getID().equals(curloc)) {
                                long ts = update.tag.getLocationMatchTimestamp();   /* Since when? */
                                long now = System.currentTimeMillis();
                                if(ts < now) {
                                    double boost = match_boost * (1.0 - Math.exp(-(double)(now-ts) / (1000.0*update.tag.getTagGroup().getLocationMatchTime())));
                                    vbump += boost;
                                }
                            }
                        }
                        v = applyBump(v, vbump);
	                    if (v > best) { /* Definitely better */
    	                    bestrule = rule;
    	                    best = v;
    	                } else if (Math.abs(v-best) < SMALL_DIFF) { /* Same? */
    	                    /* If its one of our changes, assume its better */
    	                    if ((upd_set != null)
    	                        && (upd_set.contains(rule))) {
    	                        bestrule = rule;
    	                    }
    	                }
					}
                }
//				System.out.println("bestid=" + bestrule.getID() + ", best=" + best);
                /* Get the rule's location value */
                String newloc = LocationRuleFactory.ZONELOCATION_NOMATCH;
                Location new_loc = null;
                if (bestrule != null) {
                    new_loc = bestrule.getRuleTargetLocation();
                    newloc = new_loc.getID();
                }
				GPS new_gps = null;
				String newgps = LocationRuleFactory.GPSID_NOMATCH;
				if(bestgpsrule != null) {
					new_gps = bestgpsrule.getRuleTargetGPS();
					newgps = new_gps.getID();
				}
                /* Check any rules that need result feedback */
                Map<LocationRule, Double> conf2 = conf;
                conf = null;
                for (Map.Entry<LocationRule, Double> row : conf2.entrySet()) {
                    LocationRule r = row.getKey();
                    if(r instanceof LocationRuleWithResultFeedback) {
						Double nc = row.getValue();
                        double c = ((LocationRuleWithResultFeedback)r).
                            reportLocationResult(update.tag, 
                                new_loc, nc);
                        if(c <= LocationRule.NOMATCH) {
                            if(conf == null) 
                                conf = new HashMap<LocationRule,Double>(conf2);
                            conf.remove(r);
                        } else if (Math.abs(c-nc.doubleValue()) > SMALL_DIFF) {
                            if(conf == null) 
                                conf = new HashMap<LocationRule,Double>(conf2);
                            conf.put(r, Double.valueOf(c));
                        }
                    }
                }
                /* If not updated, use original */
                if(conf == null)
                    conf = conf2;
                /* If we're proposing to change current value */
                if(!newloc.equals(curloc)) {
                    /* If new change, initialize count */
                    if(update.change_cnt == 0) {
                        /* Only delay if not initial value - let first one go quick */
                    	/* Also, only set if matching rule doesn't override the update delay */
                        if((!update.tag.isDefaultFlagged(cached_idx[CACHE_ZONE])) &&
                        		((bestrule == null) || (!bestrule.overrideLocationUpdateDelay()))) {
                            update.change_cnt = update.tag.getTagGroup().getLocationUpdateDelay();
                        }
                    }
                    // Else, if new rule overrides delay, clear it
                    else if ((bestrule != null) && bestrule.overrideLocationUpdateDelay()) {
                        update.change_cnt = 0;
                    } else {  /* Else, count down */
                        update.change_cnt--;
                    }
                    update.pend_change = newloc;    /* Save pending change */
                }
                else {  /* Else, don't delay (no reason to delay non-change) */
                    update.change_cnt = 0;
                }
                /* Build new value set to write to tag */
                /* Indexes match zone and conf indexes */
                Object[] newvals = new Object[CACHE_LEN];
                newvals[CACHE_CONF] = conf;
                newvals[CACHE_ZONE] = newloc;
                newvals[CACHE_GPSID] = newgps;
				/* If newgps is defined, update last to match and clear time */
				if(new_gps != null) {
					newvals[CACHE_LASTGPSID] = newgps;
					newvals[CACHE_LASTGPSTS] = null;
                } else {  /* Else, maintain last and set time if needed */
					newvals[CACHE_LASTGPSID] = update.tag.readTagAttribute(cached_idx[CACHE_LASTGPSID]);
					newvals[CACHE_LASTGPSTS] = update.tag.readTagAttribute(cached_idx[CACHE_LASTGPSTS]);
					/* We've got a last GPS, but no last time */
					if((newvals[CACHE_LASTGPSID].equals(LocationRuleFactory.GPSID_NOMATCH) == false) && 
						(newvals[CACHE_LASTGPSTS] == null)) {
						long now = System.currentTimeMillis();	/* Get current time */
                        long ts = now;
						/* Find reader for our last GPS */
						GPS lastgps = ReaderEntityDirectory.findGPS((String)newvals[CACHE_LASTGPSID]);
						Reader rdr = null;
						if(lastgps != null) {
                            long last_ts = lastgps.getLastGPSTimestampForTag(update.tag.getTagGUID());
                            if(last_ts != 0) {    /* Got a last time? */
                                ts = last_ts + LASTGPSTS_SHIFT;  /* Go 0.1 second after last report */
                            } else {
                                rdr = lastgps.getReader();
                            }
                        }
						if(rdr != null) {
							boolean foundit = false;
							/* See if any taglinks for this reader */
							for(ReaderChannel rc : rdr.getChannels()) {
								if(update.tag.findTagLink(rc) != null) {	/* Match? */
									foundit = true;
									break;
								}
							}
							/* If not found, we're lost to the old reader - last time is based on ageout */
							if(!foundit) {
								ts = ts - (1000*rdr.getTagTimeout()) + LASTGPSTS_SHIFT;	/* No beacons since then, so the time of the last beacon is best estimate */
																		/* Note: as with the below case, 1/2 a beacon period after may be better estimate */
                            } else {
								/* Otherwise, sometime beween now and last beacon strong enough for match - assume current time is OK, although 1/2 time
								 * since last strong beacon would probably be better */
							}
						}
						newvals[CACHE_LASTGPSTS] = Long.valueOf(ts);
					}
				}
                /* If no remaining delay, update to new location, otherwise just the other ones */
                if(update.change_cnt <= 0) {
                    update.tag.updateTagAttributes(cached_idx, newvals, CACHE_LEN);
                    /* If new matching location, set timestamp */
                    if((curloc != null) && (!newloc.equals(curloc))) {
                        update.tag.setLocationMatchTimestamp(System.currentTimeMillis());
                    }
                }
                else {
                    update.tag.updateTagAttributes(cached_idx, newvals, CACHE_LEN-1);
                }
				/* If subtags, replicate to them */
				if(update.tag.getTagGroup().hasSubGroups()) {
					updateSubTags(update.tag);
				}
                /* If going from unmatched to matched, lock tag */
                if(oldconf.isEmpty() && (!conf.isEmpty())) 
                    update.tag.lockUnlockTag(true);
                /* If going from matched to unmatched, unlock tag */
                else if((!oldconf.isEmpty()) && conf.isEmpty())
                    update.tag.lockUnlockTag(false);
            }
            /* Else, if pending change */
            else if(update.change_cnt > 0) {
                update.change_cnt--;
                if(update.change_cnt == 0) {    /* Done */
                    /* Indexes match zone and conf indexes */
                    Object[] newvals = new Object[1];
                    newvals[0] = update.pend_change;
                    int[] idx = new int[1];
                    idx[0] = cached_idx[CACHE_ZONE];
                    /* If no remaining delay, update to new location */
                    if(update.tag.updateTagAttributes(idx, newvals, 1)) {
                        /* Update timestamp */
                        update.tag.setLocationMatchTimestamp(System.currentTimeMillis());
                    }
					/* If subtags, replicate to them */
					if(update.tag.getTagGroup().hasSubGroups()) {
						updateSubTags(update.tag);
					}
                }
            }
            /* If pending, requeue it */
            if((update.change_cnt > 0) && update.tag.isValid()) {
                /* Get current location */
                Object lval = update.tag.readTagAttribute(cached_idx[CACHE_ZONE]);
                /* If we're a change, add back to queue */
                if(((update.pend_change == null)?
                    (lval == null):
                    (update.pend_change.equals(lval))) == false) {
                    update.target_tick = current_tick + TIME_DELAY_COUNT;
                    update.rules.clear();          /* Clear pending rule triggers */
                    /* And add to the hash */
                    updatesbytag.put(update.tag.getTagGUID(), update);
                    /* And add to end of update queue */
                    update_queues.add(update);
                }
            }
        } while (update != null);
    }
	/**
	 * Update subtags of given tag with new location and confidence data
	 */
	private void updateSubTags(Tag tag) {
		Set<Tag> st = tag.getSubTags();
		if(st == null) return;
		TagType tt = tag.getTagType();
		int[] idx = getCachedTTIdx(tt);
		Object[] val = new Object[idx.length];
		int i;

		/* Get current values from parent */
		for(i = 0; i < idx.length; i++) 
			val[i] = tag.readTagAttribute(idx[i]);

		/* And copy to subtags */
		for(Tag stag : st) {
			if(tt != stag.getTagType()) {	/* If not right indexes */
				tt = stag.getTagType();
				idx = getCachedTTIdx(tt);
			}
            stag.updateTagAttributes(idx, val, idx.length);
		}
	}
	/*
	 * Update single subtag from its parent
	 */
	private void updateSubTag(Tag stag) {
		Tag tag = stag.getParentTag();
		if(tag == null)
			return;
		TagType tt = tag.getTagType();
		int[] idx = getCachedTTIdx(tt);
		Object[] val = new Object[idx.length];
		int i;

		/* Get current values from parent */
		for(i = 0; i < idx.length; i++) 
			val[i] = tag.readTagAttribute(idx[i]);

		idx = getCachedTTIdx(stag.getTagType());	/* Get cached indices for subtag */

        stag.updateTagAttributes(idx, val, idx.length);
	}
    /**
     * Register rule to be triggered by TagLink updates on a given channel.
     * 
     * @param rule -
     *            rule to be triggered
     * @param channel -
     *            channel rule is sensitive to
     * @return RuleTrigger object, to be used to unregister later
     */
    public RuleTrigger registerRuleTriggerByChannel(LocationRule rule,
        ReaderChannel channel) {
        /* Create tag link listener */
        TagLinkListener tll = new TagLinkListener();
        tll.rule = rule;
        tll.channel = channel;
        /* And register with channel */
        channel.addTagLinkListener(tll);
        /* Now, see if any tags already on channel - need to force trigger */
        List<TagLink> tags = new ArrayList<TagLink>(channel.getTagLinks().values());
        for (TagLink tagl : tags) {
            triggerRule(tagl.getTag(), rule);
        }
        /* Return to caller, for later cleanup */
        return tll;
    }
    /**
     * Register rule to be triggered by TagLink updates for given tag IDs
     * 
     * @param rule -
     *            rule to be triggered
     * @param tagids -
     *            set of tag IDs rule is sensitive to
     * @return RuleTrigger object, to be used to unregister later
     */
    public RuleTrigger registerRuleTriggerByTagIDs(LocationRule rule,
        Set<String> tagids) {
        /* Create tag link listener */
        TagIDLinkListener tll = new TagIDLinkListener(tagids, rule);
        /* Now, see if any tags already found - need to force trigger */
        for(String tagid : tagids) {
            Tag tag = TagGroup.findTagInAnyGroup(tagid);
            if(tag != null) {
                triggerRule(tag, rule);
            }
        }
        /* Return to caller, for later cleanup */
        return tll;
    }
    /**
     * Register rule to be triggered by Tag updates of a given attribute ID.
     * 
     * @param rule -
     *            rule to be triggered
     * @param attribid -
     *            attribute ID
     * @return RuleTrigger object, to be used to unregister later
     */
    public RuleTrigger registerRuleTriggerByTagAttribute(LocationRule rule,
        String attribid, Object attribval) {
        TagAttribListener tal;
        /* See if already listener for this attribute */
        if (tag_attrib_listeners == null) {
            tag_attrib_listeners = new HashMap<String, TagAttribListener>();
        }
        tal = tag_attrib_listeners.get(attribid);
        /* If not, create new one */
        if (tal == null) {
            tal = new TagAttribListener(attribid);
            /* Loop through tag types - add rule for each with the attribute */
            for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
                /* Find the attribute */
                int attrib_idx = tt.getTagAttributeIndex(attribid);
                if (attrib_idx >= 0) { /* If found, add listener */
                    TagTypeAttribListener ttal = new TagTypeAttribListener(tt,
                        attrib_idx, tal);
                    tal.listeners.add(ttal); /* Add to list */
                    tt.addTagLifecycleListener(ttal); /* And listen */
                }
            }
            tag_attrib_listeners.put(attribid, tal);
        }
        tal.addRule(rule, attribval); /* And add rule to set we're watching */
        /* Walk through tag types, fire rule for any that now match */
        for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
            /* Find the attribute */
            int attrib_idx = tt.getTagAttributeIndex(attribid);
            if (attrib_idx >= 0) { /* If found, add listener */
                /* Now, see if any tags of this type - need to force trigger */
                Map<String, TagGroup> tgs = TagGroup.getTagGroupsByTagType(tt);
                for (Map.Entry<String, TagGroup> te : tgs.entrySet()) {
                    Set<Map.Entry<String, Tag>> tagiter = te.getValue().getTagIterator();
                    for (Map.Entry<String, Tag> tage : tagiter) {
                        triggerRule(tage.getValue(), rule);
                    }
                }
                tgs.clear();
            }
        }
        /* Return to caller, for later cleanup */
        return tal;
    }

    /**
     * Enqueue rule trigger for given tag
     * 
     * @param tag -
     *            tag changed
     * @param rule -
     *            rule to be triggered
     */
    public void triggerRule(Tag tag, LocationRule rule) {
        TagUpdate upd = getTagUpdate(tag);
        if(rule != null)
            upd.rules.add(rule); /* Add rule, if needed */
    }
    /**
     * Enqueue rule trigger for given tag
     * 
     * @param tag -
     *            tag changed
     * @param rules -
     *            set of rules to be triggered
     */
    private void triggerRules(Tag tag, LocationRuleSet rules) {
        TagUpdate upd = getTagUpdate(tag);
        upd.rules.addAll(rules); /* Add rules */
    }
    /**
     * Enqueue rule trigger for given tag lifecycle create or delete
     * 
     * @param tag -
     *            tag changed
     * @param tal -
     *            Tag attribute listener
     */
    private void triggerRulesForTagLifecycleChange(Tag tag, TagAttribListener tal) {
        TagUpdate upd = getTagUpdate(tag);
        tal.tagLifecycleChange(upd);
    }

    /**
     * Return the existing or a new tag update for given tag
     * 
     * @param tag -
     *            tag changed
     */
    private TagUpdate getTagUpdate(Tag tag) {
        /* Fetch pending update, if already there */
        TagUpdate upd = updatesbytag.get(tag.getTagGUID());
        if (upd == null) { /* No? new update */
            upd = new TagUpdate();
            upd.tag = tag;
            upd.rules = new LocationRuleSet();
            upd.target_tick = current_tick + TIME_DELAY_COUNT;
            /* And add to the hash */
            updatesbytag.put(tag.getTagGUID(), upd);
            /* And add to end of update queue */
            update_queues.add(upd);
        }
        return upd;
    }

    /**
     * Add LocationLifecycleListener for all locations
     * 
     * @param listen -
     *            listener to be added
     */
    public static void addLocationLifecycleListener(LocationLifecycleListener listen) {
        /* Add to global list */
        location_listeners.add(listen);
    }
    /**
     * Remove LocationLifecycleListener for all locations
     * 
     * @param listen -
     *            listener to be removed
     */
    public static void removeLocationLifecycleListener(
        LocationLifecycleListener listen) {
        location_listeners.remove(listen);
    }
    /**
     * Add LocationRuleExceptionListener
     * 
     * @param listen -
     *            listener to be added
     */
    public static void addLocationRuleExceptionListener(LocationRuleExceptionListener listen) {
        /* Add to global list */
        exception_listeners.add(listen);
    }
    /**
     * Remove LocationRuleExceptionListener for all locations
     * 
     * @param listen -
     *            listener to be removed
     */
    public static void removeLocationRuleExceptionListener(
		LocationRuleExceptionListener listen) {
    	exception_listeners.remove(listen);
    }
    /**
     * Access tag lifecycle listener list (read-only)
     * 
     * @return list of listeners
     */
    public static List<LocationLifecycleListener> getLocationLifecycleListeners() {
        return Collections.unmodifiableList(location_listeners);
    }
    /**
     * Broadcast trigger for all tags matching rule - used for rule disable or delete
     * @param rule - location rule
     */
    public void triggerAllTagsByRule(LocationRule rule) {
        for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
            int idx = tg.getTagType().getTagAttributeIndex(LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE);
            if(idx < 0)
                continue;
            for(Map.Entry<String, Tag> te : tg.getTagIterator()) {
                Tag tag = te.getValue();
                @SuppressWarnings("unchecked")
				Map<LocationRule,Double> conf = (Map<LocationRule,Double>)tag.readTagAttribute(idx);
                if(conf.containsKey(rule)) {
                    triggerRule(tag, rule);
                }
            }
        }
    }
    /**
     * Get expected location confidence bump
     * @return amount to add to confidence on rules matching expected location for tag
     */
    public double getTagExpectedLocationBump() {
        return exp_loc_bump;
    }
    /**
     * Set expected location confidence bump
     * @param newbump - new expected location bump
     */
    public void setTagExpectedLocationBump(double newbump) {
        if(newbump != exp_loc_bump) {
            exp_loc_bump = newbump; /* Update value */
            /* Trigger rules for all tags with expected locations and matching rules */
            for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                int idx = tg.getTagType().
                    getTagAttributeIndex (LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE);
                if(idx < 0)
                    continue;
                for(Map.Entry<String, Tag> te : tg.getTagIterator()) {
                    Tag tag = te.getValue();
                    List<String> exp = exp_tag_loc.get(tag.getTagGUID());
                    if(exp == null)
                        continue;
                    @SuppressWarnings("unchecked")
					Map<LocationRule,Double> conf = (Map<LocationRule,Double>)tag.readTagAttribute(idx);
                    if((conf == null) || (conf.size() == 0))
                        continue;
                    for(LocationRule rule : conf.keySet()) {
                        if(rule.getRuleEnabled()) {
                            triggerRule(tag, rule);
                        }
                    }
                }
            }
        }
    }
    /**
     * Reset all expected tag locations
     */
    public void resetTagExpectedLocations() {
        exp_tag_loc.clear();
        /* Trigger everyone with matches */
        for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
            int idx = tg.getTagType().
                getTagAttributeIndex (LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE);
            if(idx < 0)
                continue;
            for(Map.Entry<String, Tag> te : tg.getTagIterator()) {
                Tag tag = te.getValue();
                @SuppressWarnings("unchecked")
				Map<LocationRule,Double> conf = (Map<LocationRule,Double>)tag.readTagAttribute(idx);
                if((conf == null) || (conf.size() == 0))
                    continue;
                for(LocationRule rule : conf.keySet()) {
                    if(rule.getRuleEnabled()) {
                        triggerRule(tag, rule);
                    }
                }
            }
        }
    }
    /**
     * Set expected tag locations for given tag
     * @param guid - tag ID to be set
     * @param explist - list of expected (null=none)
     */
    public boolean setTagExpectedLocations(String guid, List<String> explist) {
        List<String> old = exp_tag_loc.get(guid);   /* Get current one */
        if((explist != null) && (explist.size() == 0))
            explist = null;
        if((old == null)?(explist == null):(old.equals(explist))) {  /* If same */
            return false; /* Quit - nothing to do */
        }
        if(explist == null)
            exp_tag_loc.remove(guid);
        else
            exp_tag_loc.put(guid, new ArrayList<String>(explist));
        /* Now find tag, if it exists right now */
        Tag tag = TagGroup.findTagInAnyGroup(guid);
        if(tag == null) {   /* Not there, nothing left to do */
//            System.out.println("tag " + guid + " not found");
            return true;
        }
        /* Now, trigger all matching rules for the tag for recomputing */
        int idx = tag.getTagType().
            getTagAttributeIndex (LocationRuleFactory.ATTRIB_CONFIDENCE_BY_RULE);
        if(idx < 0) {
//            System.out.println("tag attrib index not found");
            return true;
        }
        @SuppressWarnings("unchecked")
		Map<LocationRule,Double> conf = (Map<LocationRule,Double>)tag.readTagAttribute(idx);
        if((conf != null) && (conf.size() > 0)) {
            for(LocationRule rule : conf.keySet()) {
                if(rule.getRuleEnabled()) {
                    triggerRule(tag, rule);
                }
            }
        }
        return true;
    }
    /**
     * Get tag expected location for given tag
     * @param guid - tag guid
     */
    public List<String> getTagExpectedLocations(String guid) {
        List<String> old = exp_tag_loc.get(guid);   /* Get current one */
        if(old != null)
            return new ArrayList<String>(old);
        else
            return new ArrayList<String>();
    }
    /**
     * Get list of tags with expected locations defined
     */
    public Set<String>  getTagsWithExpectedLocations() {
        return exp_tag_loc.keySet();
    }
    /**
     * Signal rule exception create
     */
    public static void triggerRuleExceptionCreate(LocationRuleException lrx) {
    	for (LocationRuleExceptionListener lsn : exception_listeners) {
    		lsn.locationRuleExceptionCreated(lrx);
    	}
    }
    /**
     * Signal rule exception update
     */
    public static void triggerRuleExceptionUpdate(LocationRuleException lrx) {
    	for (LocationRuleExceptionListener lsn : exception_listeners) {
    		lsn.locationRuleExceptionUpdated(lrx);
    	}
    }
    /**
     * Signal rule exception resolved
     */
    public static void triggerRuleExceptionResolved(LocationRuleException lrx) {
    	for (LocationRuleExceptionListener lsn : exception_listeners) {
    		lsn.locationRuleExceptionResolved(lrx);
    	}
    }
}
