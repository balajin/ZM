package com.rfcode.drivers;

/**
 * Interface representing a chunk of data successfully read from a specific
 * session. Delivered via the SessionHandler.sessionDataRead() method - must be
 * assumed to be a transient object only valid during the callback used to
 * deliver it.
 * 
 * @author Mike Primm
 * 
 */
public interface SessionRead {
    /**
     * Session that the data was read from
     * 
     * @return session
     */
    public Session getSession();
    /**
     * Amount of data available to be read
     * 
     * @return available data, in bytes
     */
    public int getAvailableBytes();
    /**
     * Read data into provided buffer - consumes bytes in process.
     * 
     * @param buf -
     *            buffer
     * @param off -
     *            starting offset
     * @param len -
     *            bytes to be read
     * @return bytes returned
     */
    public int readBytes(byte[] buf, int off, int len);
    /**
     * Return all available bytes.
     * 
     * @return buffer containing read bytes.
     */
    public byte[] readBytes();
}
