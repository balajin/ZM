package com.rfcode.drivers;

import java.net.InetSocketAddress;
import java.net.InetAddress;
import javax.net.ssl.SSLContext;

/**
 * Interface for TCP-connection specific sessions.
 * 
 * @author Mike Primm
 */
public interface TCPIPSession extends Session {
    /**
     * Set the TCP/IP address and port for the session to connect to.
     * 
     * @param ip -
     *            address
     */
    public void setIPAddress(InetSocketAddress ip);
    /**
     * Get the TCP/IP address and port for the session to connect to.
     * 
     * @return ip address and port
     */
    public InetSocketAddress getIPAddress();
	/**
	 * Get address sessions is connected to (remote peer)
     *
	 * @return ip address and port of peer, or null if not connected
	 */
    public InetAddress getRemoteAddress();
    /**
     * Start SSL on socket (starttls style)
     * @param ctx - SSL context to use for session
     * @param isclient - if true, negotiate as client, else as server
     * @param lastmsg - if defined, message to be sent before starting negotiation
     * @throws SessionException if failed to start (or already started)
     */
    public void startTLS(SSLContext ctx, boolean isclient, byte[] lastmsg) throws SessionException;
    /**
     * Is SSL active on session
     */
    public boolean getTLSStarted();
}
