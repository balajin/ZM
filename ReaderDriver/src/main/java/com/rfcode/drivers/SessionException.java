package com.rfcode.drivers;

import java.io.IOException;

/**
 * Base class for Session-specific exceptions.
 * 
 * @author mprimm
 */
public class SessionException extends IOException {
    public static final long serialVersionUID = 0x1597010857109875L;
    /**
     * Default constructor for generic exceptions
     */
    public SessionException() {
        super("Session Exception");
    }
    /**
     * Constructor with exception message
     * 
     * @param msg -
     *            exception message
     */
    public SessionException(String msg) {
        super(msg);
    }
}
