package com.rfcode.drivers;

import org.apache.log4j.Logger;

/**
 * Session factory interface for driver communications.
 * 
 * @author Mike Primm
 */
public interface SessionFactory {
    /**
     * Create a TCP/IP session. Need to set the IP address on the session
     * returned before requesting a connection.
     * 
     * @return TCPIPSession instance
     */
    public TCPIPSession createTCPIPSession() throws SessionException;
    /**
     * Create a TCP/IP server socket. Need to set the IP address on the session
     * returned before requesting a bind.
     * 
     * @return TCPIPServerSocket instance
     * @throws SessionException
     *             if error creating session
     */
    public TCPIPServerSocket createTCPIPServerSocket() throws SessionException;
    /**
     * Create a serial port session. Need to set the port settings on the
     * session returned before requesting a connection.
     * 
     * @return SerialPortSession instance
     */
    public SerialPortSession createSerialPortSession() throws SessionException;
    /**
     * Get the factory's logger
     */
    public Logger getLogger();
    /**
     * Init method - used to activate the configured factory
     */
    public void init() throws SessionException;
    /**
     * Cleanup method - used to deactivate and clean up the factory
     */
    public void cleanup();
}
