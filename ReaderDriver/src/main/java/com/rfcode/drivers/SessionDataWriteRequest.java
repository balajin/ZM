package com.rfcode.drivers;

/**
 * Interface representing a pending request to asynchronously write data to a
 * session. The associated object is valid from when it was returned from the
 * Session.requestDataWrite() method call until the
 * SessionHandler.sessionDataWriteComplete() callback completes OR until the
 * SessionHandler.sessionDisconnectCompleted() callback is called.
 * 
 * @author mprimm
 */
public interface SessionDataWriteRequest {
    /**
     * Get the session the request was directed to.
     * 
     * @return session
     */
    public Session getSession();
    /**
     * Get the data write handler associated with the request
     * 
     * @return data write handler
     */
    public SessionDataWriteHandler getDataWriteHandler();
    /**
     * Number of bytes successfully written
     * 
     * @return count of bytes
     */
    public int getBytesWritten();
    /**
     * Tests if write was completed successfully
     * 
     * @return true if was successful, false otherwise
     */
    public boolean wasSuccessful();
}
