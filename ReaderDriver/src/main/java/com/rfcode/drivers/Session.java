package com.rfcode.drivers;

/**
 * Interface representing functions for a specific session.
 * 
 * @author Mike Primm
 */
public interface Session {
    /**
     * Get our session factory
     * 
     * @returns session factory
     */
    public SessionFactory getSessionFactory();
    /**
     * Set the session handler for this session - used to report session
     * lifecycle events (connect, disconnect), as well as deliver read data.
     * Only one handler per session.
     * 
     * @param handler -
     *            session hander (null=none, use default handler)
     */
    public void setSessionHandler(SessionHandler handler);
    /**
     * Get the session handler for this session.
     * 
     * @return session handler (if not set, a default handler is returned)
     */
    public SessionHandler getSessionHandler();
    /**
     * Asynchronous request that session be disconnected, if currently
     * connected. sessionDisconnected(Session) will be called one disconnect is
     * completed.
     */
    public void requestSessionDisconnect() throws SessionException;
    /**
     * Asynchronous request that session attempt to connect, if currently
     * disconnected. sessionConnected(Session) will be called once connect
     * attempt is completed.
     */
    public void requestSessionConnect() throws SessionException;
    /**
     * Enqueue asynchrononous request to write data to the session. The data in
     * the provided buffer MUST be kept stable until the write request is
     * completed.
     * 
     * @param data -
     *            buffer of data to write
     * @param off -
     *            offset within buffer for start of data
     * @param len -
     *            length of data to be sent
     * @param reqhand -
     *            request handler to be notified when request is completed
     * @return object representing the requested write
     */
    public SessionDataWriteRequest requestDataWrite(byte[] data, int off,
        int len, SessionDataWriteHandler reqhand) throws SessionException;
    /**
     * Cleanup session
     */
    public void cleanup();
    /**
     * Test if session is still valid (connected)
     * 
     * @return true if valid, false if closed
     */
    public boolean isValid();
}
