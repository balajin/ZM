package com.rfcode.drivers.impl;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.TCPIPServerSocket;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.TCPIPServerHandler;
import com.rfcode.drivers.TCPIPSession;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.buffer.IoBuffer;

/**
 * Default implementation for the TCPIPServerSocket interface.
 * 
 * @author Mike Primm
 */
public class TCPIPServerSocketImpl implements TCPIPServerSocket {
    /* Our handler */
    private TCPIPServerHandler our_handler;
    /* Our session factory impl */
    protected SessionFactoryImpl our_sess_factory;
    /* Our bind address */
    private InetSocketAddress our_ip;
    /* Our IoAcceptor */
    private SocketAcceptor our_acceptor;

    /* Do-nothing default handler */
    private static class DefaultHandler implements TCPIPServerHandler {
        /**
         * Session accepted - used to notify when a client connection has been
         * established with the server
         * 
         * @param server -
         *            our server
         * @param session -
         *            session with client
         */
        public void sessionAcceptCompleted(TCPIPServerSocket server,
            TCPIPSession session) {
            /* Just clost the accepted session in the default handler */
            try {
                session.requestSessionDisconnect();
            } catch (SessionException sx) {
                session.getSessionFactory().getLogger().error(sx);
            }
        }
        /**
         * Server socket bind completed
         * 
         * @param server -
         *            our server
         * @param is_bound -
         *            true if successful, false if failed
         */
        public void serverBindCompleted(TCPIPServerSocket server,
            boolean is_bound) {
        }
        /**
         * Server socket unbind completed
         * 
         * @param server -
         *            our server
         */
        public void serverUnbindCompleted(TCPIPServerSocket server) {
        }
    }
    
    /* Our handler for server socket state */
    private class OurIoHandler extends IoHandlerAdapter {
        /* New session started */
        @Override
        public void sessionCreated(IoSession newsess) {
//            System.out.println("sessionCreated(" + newsess + ")");
        }
        /* New session started */
        @Override
        public void sessionOpened(IoSession newsess) {
//            System.out.println("sessionOpened(" + newsess + ")");
            try {
                /* Create session to handle it */
                TCPIPSessionImpl sess = new TCPIPSessionImpl(our_sess_factory, newsess);
                /* Now call our handler, if we've got one */
                our_handler.sessionAcceptCompleted(TCPIPServerSocketImpl.this, sess);
                /* Finally, call connect handler for the session */
                sess.getSessionHandler().sessionConnectCompleted(sess, true);
            } catch (IOException iox) {
                System.out.println("sessionOpened(" + newsess + ") - exception " + iox);
            }
        }
        /**
         * Message received - called when input received and is done being processed by filters
         */
        @Override
        public void messageReceived(IoSession sess, Object msg) {
            //System.out.println("messageReceived(" + sess + "," + msg + ")");
            TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
            if(tcpsess != null)
                tcpsess.processRead((IoBuffer)msg);
        }
        /**
         * Exception caught - called on any session exception
         */
        @Override
        public void exceptionCaught(IoSession sess, Throwable cause) {
        	try {
        		if (cause instanceof Error) {
        			throw ((Error)cause);
        		}
//            System.out.println("exceptionCaught(" + sess + "," + cause + ")");
        	} finally {
        		TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
        		if(tcpsess != null)
        			tcpsess.processDisconnect();
        	}
        }
        /**
         * Session closed
         */
        @Override
        public void sessionClosed(IoSession sess) {
//            System.out.println("sessionClosed(" + sess + ")");
            TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
            if(tcpsess != null)
                tcpsess.processDisconnect();
        }
    }

    private static DefaultHandler defhand = new DefaultHandler();

    /**
     * Default contructor
     */
    TCPIPServerSocketImpl(SessionFactoryImpl our_factory)
        throws SessionException {
        /* Remember our factory */
        our_sess_factory = our_factory;
        /* Set to default "do nothing" handler */
        our_handler = defhand;
        /* Default to generic port */
        our_ip = new InetSocketAddress(23);
    }
    /**
     * Set the server socket handler for this server - used to report
     * connections
     * 
     * @param handler -
     *            server hander (null=none)
     */
    public void setServerHandler(TCPIPServerHandler handler) {
        if (handler == null)
            our_handler = defhand;
        else
            our_handler = handler;
    }
    /**
     * Get the server socket handler for this session.
     * 
     * @return server socket handler (null=none)
     */
    public TCPIPServerHandler getServerHandler() {
        if (our_handler == defhand)
            return null;
        else
            return our_handler;
    }
    /**
     * Set the TCP/IP address and port for the server to bind to. Use
     * InetSocketAddress(int port) for "any" interface.
     * 
     * @param ip -
     *            address
     */
    public void setIPAddress(InetSocketAddress ip) {
        our_ip = ip;
    }
    /**
     * Get the TCP/IP address and port the server is to bind to.
     * 
     * @return ip address and port
     */
    public InetSocketAddress getIPAddress() {
        return our_ip;
    }
    /**
     * Request that the server socket bind (which makes it active)
     */
    public void requestBind() throws SessionException {
        boolean did_bind = false;
        try {
            if (our_acceptor != null) { /* We're already bound */
                did_bind = true;
                return; /* Return success */
            }
            if (our_acceptor != null) { /* Clean up channel */
                our_acceptor.unbind();
                our_acceptor.dispose();
                our_acceptor = null;
            }
            try {
                our_acceptor = our_sess_factory.bindAcceptor(our_ip, 
                    new OurIoHandler());
                did_bind = true;
            } catch (IOException iox) {
                if (our_acceptor != null) { /* Clean up channel */
                    our_acceptor.unbind();
                    our_acceptor.dispose();
                    our_acceptor = null;
                }
                throw new SessionException("Error opening socket");
            }
        } finally {
            /* And call our handler, if we've got one */
            our_handler.serverBindCompleted(this, did_bind);
        }
    }
    /**
     * Request that the server socket unbind (which makes it inactive)
     */
    public void requestUnbind() throws SessionException {
        try {
            if (our_acceptor != null) { /* Clean up channel */
                our_acceptor.unbind();
                our_acceptor.dispose();
                our_acceptor = null;
            }
        } finally {
            /* And call our handler, if we've got one */
            our_handler.serverUnbindCompleted(this);
        }
    }
    /**
     * Get our session factory
     * 
     * @returns session factory
     */
    public SessionFactory getSessionFactory() {
        return our_sess_factory;
    }
}
