package com.rfcode.drivers.impl;

import com.rfcode.drivers.Session;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.SessionHandler;
import com.rfcode.drivers.SessionRead;

public abstract class SessionImpl {
    /* Our handler */
    private SessionHandler our_handler;
    /* Our session factory impl */
    protected SessionFactoryImpl our_sess_factory;
    /* Do-nothing default handler */
    private static class DefaultHandler implements SessionHandler {
        public void sessionConnectCompleted(Session s, boolean is_connected) {
        }
        public void sessionDisconnectCompleted(Session s) {
        }
        public void sessionDataRead(SessionRead sdr) {
        }
        public void sessionStartingConnect(Session s) {
        }
    }
    private static DefaultHandler defhand = new DefaultHandler();

    /**
     * Default contructor
     */
    SessionImpl(SessionFactoryImpl our_factory) throws SessionException {
        /* Remember our factory */
        our_sess_factory = our_factory;
        /* Set to default "do nothing" handler */
        our_handler = defhand;
    }
    /**
     * Set the session handler for this session - used to report session
     * lifecycle events (connect, disconnect), as well as deliver read data.
     * Only one handler per session.
     * 
     * @param handler -
     *            session hander (null=none, use default handler)
     */
    public void setSessionHandler(SessionHandler handler) {
        if (handler == null)
            our_handler = defhand;
        else
            our_handler = handler;
    }
    /**
     * Get the session handler for this session.
     * 
     * @return session handler (if not set, a default handler is returned)
     */
    public SessionHandler getSessionHandler() {
        return our_handler;
    }
    /**
     * Get our session factory
     * 
     * @returns session factory
     */
    public SessionFactory getSessionFactory() {
        return our_sess_factory;
    }
}
