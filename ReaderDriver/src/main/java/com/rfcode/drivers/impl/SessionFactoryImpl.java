package com.rfcode.drivers.impl;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.rfcode.drivers.SerialPortSession;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.TCPIPSession;
import com.rfcode.drivers.TCPIPServerSocket;
import org.apache.log4j.Logger;
import org.apache.mina.core.service.SimpleIoProcessorPool;
import org.apache.mina.transport.socket.nio.NioSession;
import org.apache.mina.transport.socket.nio.NioProcessor;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.apache.mina.transport.socket.SocketAcceptor;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.buffer.IoBuffer;
/**
 * Base session factory for driver communications. This is intended to be a thin
 * wrapper around the use of Java NIO for asynchronous TCP and serial
 * communications, as well as a simple scheduler/timeout handler. The main goal
 * is for each factory instance to use a single thread to handle a potentially
 * arbitrary number of concurrent connections efficiently.
 * 
 * @author Mike Primm
 * 
 */
public class SessionFactoryImpl implements SessionFactory {
    /* Our logger */
    private static Logger log = Logger.getLogger("RFCodeSessionFactory");
    /* Size for our read/write direct buffer */
    private static final int BUFFER_SIZE = 16384;
	/* MINA: our I/O processor pool */
	private SimpleIoProcessorPool<NioSession> our_processor;

	/* MINA: our I/O processor pool */
	private SimpleIoProcessorPool<NioSession> our_server_processor;

	/* MINA: socket connector for outbound connections */
	private NioSocketConnector	our_connector;

    /**
     * Our Mina client handler
     */
    private class OurClientHandler extends IoHandlerAdapter {
        public OurClientHandler() { }

        /**
         * Session opened notice - called when connect has completed
         */
        @Override
        public void sessionCreated(IoSession sess) {
//            System.out.println("sessionCreated(" + sess + ")");
        }
       
        /**
         * Session opened notice - called when connect has completed
         */
        @Override
        public void sessionOpened(IoSession sess) {
//            System.out.println("sessionOpened(" + sess + ")");
            TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
            if(tcpsess != null)
                tcpsess.processConnect(sess);
        }
        /**
         * Message received - called when input received and is done being processed by filters
         */
        @Override
        public void messageReceived(IoSession sess, Object msg) {
            //System.out.println("messageReceived(" + sess + "," + msg + ")");
            TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
            if(tcpsess != null)
                tcpsess.processRead((IoBuffer)msg);
        }
        /**
         * Exception caught - called on any session exception
         */
        @Override
        public void exceptionCaught(IoSession sess, Throwable cause) {
            //System.out.println("exceptionCaught(" + sess + "," + cause + ")");
        	try {
        		if (cause instanceof Error) {
        			throw ((Error)cause);
        		}

        		if(cause != null)
        			return;
        	} finally {
        		TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
        		if(tcpsess != null)
        			tcpsess.processDisconnect();
        	}
        }
        /**
         * Session closed
         */
        @Override
        public void sessionClosed(IoSession sess) {
//            System.out.println("sessionClosed(" + sess + ")");
            TCPIPSessionImpl tcpsess = (TCPIPSessionImpl)sess.getAttribute(TCPIPSessionImpl.SESSION_KEY);  /* Get the session handle */
            if(tcpsess != null)
                tcpsess.processDisconnect();
        }
    }

    /**
     * Default constructor - we're following the Spring "dependency injection"
     * pattern, so we're a JavaBean with all our settings provided via attribute
     * get/set methods.
     */
    public SessionFactoryImpl() throws SessionException {
    }
    /**
     * Init method - used to activate the configured factory
     */
    public void init() throws SessionException {
		/* Build our I/O processor */
		our_processor = new SimpleIoProcessorPool<NioSession>(NioProcessor.class);
		our_server_processor = new SimpleIoProcessorPool<NioSession>(NioProcessor.class);
		/* And build our connector */
		our_connector = new NioSocketConnector(our_processor);
        our_connector.getSessionConfig().setReadBufferSize(BUFFER_SIZE);   /* Default read buffer */
//        our_connector.getFilterChain().addLast("logger", new LoggingFilter());  /* DEBUG: add logging filter for now */
        our_connector.setHandler(new OurClientHandler());
    }
    /**
     * Cleanup method - used to deactivate and clean up the factory
     */
    public void cleanup() {
		if(our_connector != null) {
			our_connector.dispose();
			our_connector = null;
		}
		if(our_processor != null) {
			our_processor.dispose();
			our_processor = null;
		}
		if(our_server_processor != null) {
			our_server_processor.dispose();
			our_server_processor = null;
		}
    }

    /**
     * Create a TCP/IP session. Need to set the IP address on the session
     * returned before requesting a connection.
     * 
     * @return TCPIPSession instance
     * @throws SessionException
     *             if error creating session
     */
    public TCPIPSession createTCPIPSession() throws SessionException {
        return new TCPIPSessionImpl(this);
    }
    /**
     * Create a TCP/IP server socket. Need to set the IP address on the session
     * returned before requesting a bind.
     * 
     * @return TCPIPServerSocket instance
     * @throws SessionException
     *             if error creating session
     */
    public TCPIPServerSocket createTCPIPServerSocket() throws SessionException {
        return new TCPIPServerSocketImpl(this);
    }
    /**
     * Create a serial port session. Need to set the port settings on the
     * session returned before requesting a connection.
     * 
     * @return SerialPortSession instance
     */
    public SerialPortSession createSerialPortSession() {
        return null;
    }
    /**
     * Get our logger
     */
    public Logger getLogger() {
        return log;
    }
    /**
     * Get our connector
     */
    IoConnector getConnector() {
        return our_connector;
    }
    /**
     * Bind acceptor
     */
    SocketAcceptor bindAcceptor(InetSocketAddress sock, IoHandler handler) throws IOException {
        SocketAcceptor accept;

		accept = new NioSocketAcceptor(our_server_processor);
//        accept.getFilterChain().addLast("logger", new LoggingFilter());  /* DEBUG: add logging filter for now */
        accept.setHandler(handler);
        accept.bind(sock);

        return accept;
    }
}
