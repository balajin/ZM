package com.rfcode.drivers.impl;

import java.net.InetSocketAddress;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.UnresolvedAddressException;
import javax.net.ssl.SSLContext;

import com.rfcode.drivers.Session;
import com.rfcode.drivers.SessionHandler;
import com.rfcode.drivers.SessionRead;
import com.rfcode.drivers.SessionDataWriteHandler;
import com.rfcode.drivers.SessionDataWriteRequest;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.TCPIPSession;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSessionInitializer;
import org.apache.mina.filter.ssl.SslFilter;

/**
 * Default implementation for the TCPIPSession interface.
 * 
 * @author mprimm
 */
public class TCPIPSessionImpl extends SessionImpl implements TCPIPSession {
    private InetSocketAddress our_ip;

    /* Connect future - used while connect is in progress */
    private ConnectFuture our_connect;
    /* Our session, once connected */
    private IoSession our_session;

    static final String SESSION_KEY = "tcpipsessionimpl";

    /* Our session read result object */
    private static class SessionReadRslt implements SessionRead {
        TCPIPSessionImpl sess;
        ByteBuffer buffer;
        /**
         * Session that the data was read from
         * 
         * @return session
         */
        public Session getSession() {
            return sess;
        }
        /**
         * Amount of data available to be read
         * 
         * @return available data, in bytes
         */
        public int getAvailableBytes() {
            return buffer.remaining();
        }
        /**
         * Read data into provided buffer - consumes bytes in process.
         * 
         * @param buf -
         *            buffer
         * @param off -
         *            starting offset
         * @param len -
         *            bytes to be read
         * @return bytes returned
         */
        public int readBytes(byte[] buf, int off, int len) {
            if (len > buffer.remaining())
                len = buffer.remaining();
            buffer.get(buf, off, len);
            return len;
        }
        /**
         * Return all available bytes.
         * 
         * @return buffer containing read bytes.
         */
        public byte[] readBytes() {
            byte[] b = new byte[buffer.remaining()];
            buffer.get(b, 0, b.length);
            return b;
        }
    }

    /**
     * Default contructor
     */
    TCPIPSessionImpl(SessionFactoryImpl our_factory) throws SessionException {
        super(our_factory);
        /* Default to localhost telnet (why not?) */
        our_ip = new InetSocketAddress(23);
    }
    /**
     * Contructor for accepted connection
     */
    TCPIPSessionImpl(SessionFactoryImpl our_factory, IoSession sess)
        throws SessionException {
        super(our_factory);
        /* Save our IP address */
        our_ip = (InetSocketAddress)sess.getRemoteAddress();
        our_session = sess;
        our_session.setAttribute(SESSION_KEY, this);     /* Set session's attribute to refer to us */
    }
    /**
     * Cleanup session
     */
    public void cleanup() {
        if(our_connect != null) {
            our_connect.cancel();
            our_connect = null;
        }
        if(our_session != null) {
            our_session.removeAttribute(SESSION_KEY);
            if(our_session.isConnected())
                our_session.close(true);
            our_session = null;
        }
        if (our_ip != null)
            our_ip = null;
    }
    /**
     * Test if session is still valid (connected)
     * 
     * @return true if valid, false if closed
     */
    public boolean isValid() {
        return ((our_session != null) && (our_session.isConnected()));
    }
    /**
     * Set the TCP/IP address and port for the session to connect to.
     * 
     * @param ip -
     *            address
     */
    public void setIPAddress(InetSocketAddress ip) {
        our_ip = ip; /* Save reference to address */
    }
    /**
     * Get the TCP/IP address and port for the session to connect to.
     * 
     * @return ip address and port
     */
    public InetSocketAddress getIPAddress() {
        return our_ip;
    }
	/**
	 * Get address session is connected to (remote peer)
     *
	 * @return ip address and port of peer, or null if not connected
	 */
    public InetAddress getRemoteAddress() {
		if((our_session != null) && (our_session.isConnected()))
			return ((InetSocketAddress)our_session.getRemoteAddress()).getAddress();
		return null;
	}
    /**
     * Asynchronous request that session be disconnected, if currently
     * connected. sessionDisconnected(Session) will be called one disconnect is
     * completed.
     * 
     * @throws SessionException
     *             if error or session invalid
     */
    public void requestSessionDisconnect() throws SessionException {
    	IoSession sess = our_session;
        if(sess != null) {
            if(sess.isConnected()) {
                sess.close(false);
            }
        }
    }
    /**
     * Asynchronous request that session attempt to connect, if currently
     * disconnected. sessionConnected(Session) will be called once connect
     * attempt is completed.
     * 
     * @throws SessionException
     *             if error or session invalid
     */
    public void requestSessionConnect() throws SessionException {
        if(getTLSStarted() == false) {  /* If TLS active, need to restart session */
            if((our_connect != null) && (our_connect.isConnected())) {
                return; /* Return success if already connected */
            }
            if((our_session != null) && (our_session.isConnected())) {
                return; /* Return success if already connected */
            }
        }
        /* Clean up old, if not good */
        if(our_session != null) {
            our_session.removeAttribute(SESSION_KEY);
            if(our_session.isConnected())
                our_session.close(true);
            our_session = null;
        }
        if(our_connect != null) {
            our_connect.cancel();
            our_connect = null;
        }
        /* Call session handler to notify of connection start */
        getSessionHandler().sessionStartingConnect(this);
        /* And start nonblocking connect with our target */
        try {
            our_connect = our_sess_factory.getConnector().connect(our_ip,
                new IoSessionInitializer<ConnectFuture>() {
                    public void initializeSession(IoSession ios, ConnectFuture f) {
                        ios.setAttribute(SESSION_KEY, TCPIPSessionImpl.this);
                    }
                });
            our_connect.addListener(new IoFutureListener<ConnectFuture>() {
                public void operationComplete(ConnectFuture f) {
                    if(f.isConnected()) {   /* Did we succeed? */
                        /* We'll get called by factory */
                    }
                    else {
                        our_session = null;
                        our_connect = null;
                        /* Call session handler to notify of failure of connection */
                        getSessionHandler().sessionConnectCompleted(TCPIPSessionImpl.this, false);
                    }    
                }
            });
        } catch (UnresolvedAddressException uax) {
            our_connect = null;
            throw new SessionException("Error unresolved address");
        }
    }
    /**
     * Enqueue asynchrononous request to write data to the session. The data in
     * the provided buffer MUST be kept stable until the write request is
     * completed.
     * 
     * @param data -
     *            buffer of data to write
     * @param off -
     *            offset within buffer for start of data
     * @param len -
     *            length of data to be sent
     * @param reqhand -
     *            request handler to be notified when request is completed
     * @return object representing the requested write
     * @throws SessionException
     *             if error or session invalid
     */
    public SessionDataWriteRequest requestDataWrite(byte[] data, int off,
        int len, SessionDataWriteHandler reqhand) throws SessionException {
        if(our_session != null) {
            IoBuffer buf = null;
            WriteFuture f = null;
            if(len > 0) {
                buf = IoBuffer.wrap(data, off, len);
                f = our_session.write(buf);
            }
            /* Create request object to track request */
            SessionDataWriteRequestImpl req = new SessionDataWriteRequestImpl(
                this, reqhand, buf, f);
            if(len == 0) {
                req.operationComplete(null);
            }
            return req;
        }
        else {
            throw new SessionException("Write while session not connected");
        }
    }
    /**
     * Process disconnect - called from factory worker thread
     */
    void processDisconnect() {
        /* Close the session */
        if (our_session != null) {
            our_session.removeAttribute(SESSION_KEY);
            if(our_session.isConnected())
                our_session.close(true);
            our_session = null;
        }
        /* Call session handler to notify of disconnection */
        getSessionHandler().sessionDisconnectCompleted(this);
    }
    /**
     * Process readable state
     */
    void processRead(IoBuffer msg) {
        SessionReadRslt rr = null;
        SessionHandler sh = getSessionHandler();

        rr = new SessionReadRslt();
        rr.buffer = msg.buf();
        rr.sess = this;
        sh.sessionDataRead(rr);
    }

    void processConnect(IoSession ios) {
        our_session = ios;       /* Save our session */
        our_connect = null;                     /* Clean up */
        /* Call session handler to notify of success/failure of connection */
        getSessionHandler().sessionConnectCompleted(this, true);
    }

    /**
     * Start SSL on socket (starttls style)
     * @param ctx - SSL context to use for session
     * @param isclient - if true, negotiate as client, else as server
     * @param lastmsg - if defined, message to be sent before starting negotiation
     * @throws SessionException if failed to start (or already started)
     */
    public void startTLS(SSLContext ctx, boolean isclient, byte[] lastmsg) throws SessionException {
        if((our_session == null) || (our_session.isConnected() == false))
            throw new SessionException("startTLS exception: session not connected");
        if(our_session.getFilterChain().contains("tlsfilter"))    /* Already TLS? */
            throw new SessionException("startTLS exception: TLS already active");
        SslFilter flt = new SslFilter(ctx); /* Create filter instance */
        flt.setUseClientMode(isclient);     /* Set client/server mode */
        our_session.getFilterChain().addFirst("tlsfilter", flt); /* Add at front of filter chain */
        if((lastmsg != null) && (lastmsg.length > 0)) { /* Message to send first? */
            our_session.setAttribute(SslFilter.DISABLE_ENCRYPTION_ONCE, Boolean.TRUE);
            /* Send it */
            IoBuffer buf = IoBuffer.wrap(lastmsg);
            our_session.write(buf);
        }
    }
    /**
     * Is SSL active on session
     */
    public boolean getTLSStarted() {
        if((our_session != null) && (our_session.getFilterChain().contains("tlsfilter")))
            return true;
        return false;
    }
}
