package com.rfcode.drivers.impl;

import com.rfcode.drivers.Session;
import com.rfcode.drivers.SessionDataWriteHandler;
import com.rfcode.drivers.SessionDataWriteRequest;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.buffer.IoBuffer;

/**
 * Default class implementing the SessionDataWriteRequest interface. Provides a
 * write queue handling element for session data writes.
 * 
 * @author Mike Primm
 */
public class SessionDataWriteRequestImpl implements SessionDataWriteRequest, IoFutureListener<WriteFuture> {
    private SessionDataWriteHandler our_handler;
    private Session our_session;
    private IoBuffer buff;
    /**
     * Package-local constructor
     * 
     * @param sess -
     *            our session
     * @param hnd -
     *            our handler
     * @param data -
     *            our data buffer
     */
    SessionDataWriteRequestImpl(Session sess, SessionDataWriteHandler hnd,
        IoBuffer data, WriteFuture f) {
        our_session = sess;
        our_handler = hnd;
        buff = data;
        if(f != null)
            f.addListener(this);
    }
    /**
     * Get the session the request was directed to.
     * 
     * @return session
     */
    public Session getSession() {
        return our_session;
    }
    /**
     * Get the data write handler associated with the request
     * 
     * @return data write handler
     */
    public SessionDataWriteHandler getDataWriteHandler() {
        return our_handler;
    }
    /**
     * Number of bytes successfully written
     * 
     * @return count of bytes
     */
    public int getBytesWritten() {
        if(buff != null)
            return buff.capacity() - buff.remaining();
        else
            return 0;
    }
    /**
     * Tests if write was completed successfully
     * 
     * @return true if was successful, false otherwise
     */
    public boolean wasSuccessful() {
        return ((buff == null) || (buff.remaining() == 0));
    }
    /**
     * Write complete handler
     */
    public void operationComplete(WriteFuture f) {
        if(f != null)
            f.removeListener(this);
        if(our_handler != null) {
            our_handler.sessionDataWriteComplete(this);
        }
        if(buff != null) {
            buff.free();
            buff = null;
        }
    }
}
