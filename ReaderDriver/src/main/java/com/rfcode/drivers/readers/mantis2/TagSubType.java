package com.rfcode.drivers.readers.mantis2;

import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.AbstractTagType;

/**
 * Base class for tag subtypes used in Mantis-family TagTypes.
 * 
 * @author Mike Primm
 */
public class TagSubType extends AbstractTagType {
    /** Tag attribute - subindex (required attribute of subtags */
    public static final String SUBINDEX_ATTRIB = "subindex";
    public static final String SUBINDEX_ATTRIB_LABEL = "Index";
    /** Tag attribute - parent tag (required attribute of subtags */
    public static final String PARENTTAG_ATTRIB = "parenttag";
    public static final String PARENTTAG_ATTRIB_LABEL = "Parent Tag";

    /**
     * Constructor
     */
    protected TagSubType() {
		setTagGroupAttributes(new String[0]);
    }
    /**
     * Factory method for creating TagGroup instances for this TagType.
     * Resulting TagGroup must have attributes set, and its init() method
     * called. This version generates a MantisII specific tag group.
     * 
     * @return new TagGroup
     */
    public TagGroup createTagGroup() {
        return null;
    }
	/**
	 * Factory method for making subgroup for this tag sub-type.  Only used if
     * isSubType() is true.  Resulting TagGroup must have its init() method
     * called.
     * 
     * @return new TagGroup (or null, if isSubType() = false)
	 */
	public TagGroup createTagSubGroup(TagGroup parentgrp) {
		return new TagSubGroup(this, parentgrp);
	}
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
    /**
     * Set tag to offline state
     * @param tag - tag to offline
     */
    public void setTagOffline(MantisIITag tag) {
    }
    /**
     * Update boolean attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new boolean value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateBoolean(SubTag tag, int index,
        boolean val) {
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Boolean))
            || (((Boolean) o).booleanValue() != val) ||
            tag.isDefaultFlagged(index)) {
            /* Use common references - save memory */
            if (val)
                obj[index] = Boolean.TRUE;
            else
                obj[index] = Boolean.FALSE;
            tag.clearDefaultFlag(index);
            return true;
        } else
            return false;
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateInt(SubTag tag, int index, int val) {
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Integer))
            || (((Integer) o).intValue() != val) ||
            tag.isDefaultFlagged(index)) {
            obj[index] = Integer.valueOf(val);
            tag.clearDefaultFlag(index);
            return true;
        } else
            return false;
    }
    /**
     * Update double attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new double value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateDouble(SubTag tag, int index, double val) {
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Double))
            || (((Double) o).doubleValue() != val) ||
            tag.isDefaultFlagged(index)) {
            obj[index] = Double.valueOf(val);
            tag.clearDefaultFlag(index);
            return true;
        } else
            return false;
    }
    /**
     * Update string attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new stringr value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateString(SubTag tag, int index, String val) {
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof String))
            || (!((String) o).equals(val)) ||
            tag.isDefaultFlagged(index)) {
            obj[index] = val;
            tag.clearDefaultFlag(index);
            return true;
        } else
            return false;
    }
    /**
     * Update attribute value at given index to null, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateNull(SubTag tag, int index) {
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o != null) || tag.isDefaultFlagged(index)) {
            obj[index] = null;
            tag.clearDefaultFlag(index);
            return true;
        } else
            return false;
    }
	/**
	 * Is tag type a subtype?  If so, used internally by other tag types and cannot be used
     * to create a group directly.
	 */
	public boolean isSubType() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return null;
	}

}
