package com.rfcode.drivers.readers;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.Tag;

/**
 * Abstract base class for defining a type of tag. These are intended to be
 * metadata objects, allowing readers to describe the types of tags they
 * support, and providing applications with the descriptions of those tag types.
 * 
 * @author Mike Primm
 */
public abstract class AbstractTagType implements TagType {
    /* Special value used for null default for long value attributes */
    public static final Long NULL_DEFAULT_LONG = Long.valueOf(Long.MIN_VALUE);

    private static final int TIME_INT = 10; /* 10 second ref interval */
    /* Our ID */
    private String typeid;
    /* Our list of tag group attribute IDs */
    private String[] taggrpattribs;
    /* Our list of tag group attribute IDs that make a group distinct */
    private String[] taggrpdistinct;
    /* Our list of tag attribute IDs */
    private String[] tagattribs;
    /* Our list of tag attribute default values */
    private Object[] tagattribdefs;
    /* Our list of tag attribute labels */
    private String[] tagattriblabels;
    /* Number of base tag attributes (before any extras registered) */
    private int     numbasetagattribs;
    /* Our label */
    private String label;
    /* Our default attribute map */
    private Map<String, Object> defattribs;
    /* List of our tag lifecycle listeners */
    private ArrayList<TagLifecycleListener> tag_listeners;
    /* List of our global tag lifecycle listeners */
    private static ArrayList<TagLifecycleListener> global_tag_listeners = new ArrayList<TagLifecycleListener>();
    /* List of all tag event listeners */
    private static ArrayList<TagBeaconListener> tagbeacon_listeners = null;
    /* Linked hash map of delayed tags to be processed */
    private static LinkedHashMap<String,DelayedTag> delayedtags = new LinkedHashMap<String,DelayedTag>();

    /* Delayed record - we process in units of 10 seconds, and requeue items needing more
     * time */
    private static class DelayedTag {
        long    enq_ts;     /* Enqueued ts */
        int     cnt_to_requeue; /* Countdown on reenqueues (when 0 at time to process, process */
        Tag     tag;
    }
        
    /**
     * Constructor for AbstractTagType
     */
    protected AbstractTagType() {
        tagattribs = null;
        tagattribdefs = null;
        tagattriblabels = null;
        numbasetagattribs = 0;
        if (global_tag_listeners.size() > 0) {
            tag_listeners = new ArrayList<TagLifecycleListener>(global_tag_listeners);
        }
        else {
            tag_listeners = new ArrayList<TagLifecycleListener>();
        }
    }
    /**
     * Initialize method - must be called once all attibutes are set
     * 
     * @throws DuplicateEntityIDException
     *             if duplicate tag type
     * @throws InvalidArgumentException
     *             if bad or missing attribute
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        ReaderEntityDirectory.testID(typeid); /* ID already in table */
        if (taggrpattribs == null)
            throw new BadParameterException("Missing tagGroupAttributes");
        if (tagattribs == null)
            throw new BadParameterException("Missing tagAttributes");
        ReaderEntityDirectory.addEntity(this);/* Add ourselves to the map */
        if (label == null) /* Use as default label, if not provided */
            label = typeid;
        if (taggrpdistinct == null) /*
                                     * If distinct not set, assume all must
                                     * match
                                     */
            taggrpdistinct = taggrpattribs;
    }
    /**
     * Discard the TagType
     */
    public void cleanup() {
        if (typeid != null) { /* Already defined? Remove from map */
            ReaderEntityDirectory.removeEntity(this);
            typeid = null;
        }
        if (tag_listeners != null) {
            tag_listeners.clear();
            tag_listeners = null;
        }
    }
    /**
     * Set tag type id - this is required and must be unique, and cannot be
     * changed
     * 
     * @param id -
     *            tag type id
     */
    public void setID(String id) {
        if (typeid == null)
            typeid = id;
    }
    /**
     * Get ID for the tag type - this is a "well known" ID that will be returned
     * by tag tag reader drivers when indicating support for various tag
     * families.
     * 
     * @return TagType ID
     */
    public String getID() {
        return typeid;
    }
    /**
     * Set list of attributes required for TagGroup to be defined for this
     * TagType. Must be set, and cannot be changed once set.
     * 
     * @param list -
     *            list of attribute IDs
     */
    public void setTagGroupAttributes(String[] list) {
        if (taggrpattribs == null) {
            taggrpattribs = list.clone();
        }
    }
    /**
     * Get list of attributes required for TagGroup to be defined for this
     * TagType.
     * 
     * @returns list of attibute IDs
     */
    public String[] getTagGroupAttributes() {
        return taggrpattribs;
    }
    /**
     * Get copy of default attributes set for TagGroup.
     * 
     * @returns map of attibute IDs
     */
    public Map<String, Object> getTagGroupDefaultAttributes() {
        /* If not set, generate default of blank strings */
        if (defattribs == null) {
            defattribs = new HashMap<String, Object>();
            if (taggrpattribs != null) {
                for (int i = 0; i < taggrpattribs.length; i++) {
                    defattribs.put(taggrpattribs[i], "");
                }
            }
        }
        return new HashMap<String,Object>(defattribs);
    }
    /**
     * Set default attributes set for TagGroup.
     * 
     * @param def -
     *            map of attributes
     */
    protected void setTagGroupDefaultAttributes(Map<String, Object> def) {
        defattribs = new HashMap<String, Object>(def);
    }
    /**
     * Set list of group attribute IDs that determing group distinction. If not
     * set, assumed to be all tag group attibutes. Cannot be changed once set.
     * 
     * @param list -
     *            list of attribute IDs determining distinctiveness
     */
    protected void setTagGroupDistinctAttributes(String[] list) {
        if (taggrpdistinct == null)
            taggrpdistinct = list.clone();
    }
    /**
     * Get list of the attributes required for TagGroup that determine if the
     * group is distinct (that is, which much be different from other groups for
     * the same tag type in order to have a distinct tag population). If list is
     * empty, all the group attributes are assumed to need to match in order to
     * have a duplicate group.
     * 
     * @return list of attribute IDs determining distinct group
     */
    public String[] getTagGroupDistinctAttributes() {
        return taggrpdistinct;
    }

    /**
     * Set list of attributes reported by tags of this type. Must be set, and
     * cannot be changed once set.
     * 
     * @param list -
     *            list of attribute IDs
     */
    protected void setTagAttributes(String[] list) {
        if (tagattribs == null) {
            tagattribs = list.clone();
            if(tagattriblabels == null)
                tagattriblabels = list.clone();
            numbasetagattribs = tagattribs.length;
        }
        if (tagattribdefs == null)
            tagattribdefs = new Object[list.length];
    }
    /**
     * Get list of attributes reported by tags of this type
     * 
     * @returns list of attibute IDs
     */
    public String[] getTagAttributes() {
        return tagattribs;
    }
    /**
     * Get copy of list of default attribute values for newly found tags of this
     * type.
     * 
     * @returns list of attibute default values
     */
    public Object[] getTagAttributeDefaultValues() {
        if (tagattribdefs != null)
            return tagattribdefs.clone();
        else
            return new Object[0];
    }
    /**
     * Make copy of basic constant object - make sure we don't newly assigned value for default with '=' test 
     */
    private Object cloneDefaultValue(Object def) {
        Object o = def;
        if(o instanceof Integer)
            o = new Integer(((Integer)o).intValue());                
        else if(o instanceof Boolean)
            o = new Boolean(((Boolean)o).booleanValue());            
        else if(o instanceof Double)
            o = new Double(((Double)o).doubleValue());                
        else if(o instanceof String)
            o = new String((String)o);                
        return o;
    }
    /**
     * Set list of default attribute values for newly found tags of this type.
     * If not set, default will be all null values.
     * 
     * @param list -
     *            list of values
     */
    protected void setTagAttributeDefaultValues(Object[] list) {
        tagattribdefs = new Object[list.length];
        for(int i = 0; i < list.length; i++) {
            if(list[i] != null) {
                tagattribdefs[i] = cloneDefaultValue(list[i]);
            }
        }
    }
    /**
     * Get list of labels for attributes reported by tags of this type (ordered to
     * match same index as IDs)
     * 
     * @returns list of attibute labels
     */
    public String[] getTagAttributeLabels() {
        if(tagattriblabels != null)
            return tagattriblabels;
        else
            return new String[0];
    }
    /**
     * Set labels for attributes
     * 
     * @returns list of attibute labels
     */
    protected void setTagAttributeLabels(String[] list) {
        tagattriblabels = list.clone();
    }
    /**
     * Get label for given attribute ID
     * @param id - attribute ID
     * @return label (or ID if not found)
     */
    public String getTagAttributeLabel(String id) {
        if(tagattribs != null) {
            for(int i = 0; i < tagattribs.length; i++) {
                if(tagattribs[i].equals(id)) {
                    return tagattriblabels[i];
                }
            }
        }
        return id;
    }
    /**
     * Set label for tag type
     * 
     * @param lab -
     *            label
     */
    public void setLabel(String lab) {
        label = lab;
    }
    /**
     * Get label for tag type
     * 
     * @return Label
     */
    public String getLabel() {
        return label;
    }
    /**
     * Factory method for creating TagGroup instances for this TagType.
     * Resulting TagGroup must have attributes set, and its init() method
     * called.
     * 
     * @return new TagGroup
     */
    public abstract TagGroup createTagGroup();
	/**
	 * Factory method for making subgroup for this tag sub-type.  Only used if
     * isSubType() is true.  Resulting TagGroup must have its init() method
     * called.
     * 
     * @return new TagGroup (or null, if isSubType() = false)
	 */
	public TagGroup createTagSubGroup(TagGroup parentgrp) {
		return null;
	}
    /**
     * Access tag lifecycle listener list (read-only)
     * 
     * @return list of listeners
     */
    public ArrayList<TagLifecycleListener> getTagLifecycleListeners() {
        return tag_listeners;
    }
    /**
     * Add TagLifecycleListener or TagStatusListener for all tags in this group.
     * 
     * @param listen -
     *            listener to be added
     */
    public void addTagLifecycleListener(TagLifecycleListener listen) {
        ArrayList<TagLifecycleListener> newlst = new ArrayList<TagLifecycleListener>(tag_listeners);
        newlst.add(listen);
        tag_listeners = newlst;
    }
    /**
     * Remove TagLifecycleListener or TagStatusListener for all tags in this
     * group.
     * 
     * @param listen -
     *            listener to be removed
     */
    public void removeTagLifecycleListener(TagLifecycleListener listen) {
        ArrayList<TagLifecycleListener> newlst = new ArrayList<TagLifecycleListener>(tag_listeners);
        newlst.remove(listen);
        tag_listeners = newlst;
    }
    /**
     * Add TagLifecycleListener or TagStatusListener for all tags
     * 
     * @param listen -
     *            listener to be added
     */
    public static void addGlobalTagLifecycleListener(TagLifecycleListener listen) {
        /* Add to global list */
        global_tag_listeners.add(listen);
        /* And to all existing types */
        for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
            tt.addTagLifecycleListener(listen);
        }
    }
    /**
     * Remove TagLifecycleListener or TagStatusListener for all tags
     * 
     * @param listen -
     *            listener to be removed
     */
    public static void removeGlobalTagLifecycleListener(
        TagLifecycleListener listen) {
        global_tag_listeners.remove(listen);
        /* And from all existing types */
        for (TagType tt : ReaderEntityDirectory.getTagTypes()) {
            tt.removeTagLifecycleListener(listen);
        }
    }
    /**
     * Register new tag-type specific attribute. Once added, cannot be runtime
     * removed. Returned index is only good for life of current run, and must be
     * assumed to change whenever the attribute is registered on future runs.
     * Also, the assigned index is NOT common across different TagTypes. Can
     * only be called AFTER TagType.init() has completed.
     * 
     * @param aid -
     *            attribute ID
     * @param defval -
     *            default value
     * @param label -
     *            attribute label
     * @return attribute's assigned index for this tag type
     */
    public int registerTagAttribute(String aid, Object defval, String label) {
        int i;
        /* Check for match with existing attribute */
        for (i = 0; i < tagattribs.length; i++) {
            if (tagattribs[i].equals(aid)) { /* Found existing match? */
                /* Update default, if needed                          */
                tagattribdefs[i] = cloneDefaultValue(defval);
                tagattriblabels[i] = label; /* Update label, if needed */
                return i; /* Return existing index */
            }
        }
        /* Make new copy with extra slot */
        String[] newaids = new String[tagattribs.length + 1]; /* Bump length */
        Object[] newdefs = new Object[tagattribs.length + 1]; /* Bump length */
        String[] newlabs = new String[tagattribs.length + 1]; /* Bump length */
        System.arraycopy(tagattribs, 0, newaids, 0, tagattribs.length);
        System.arraycopy(tagattribdefs, 0, newdefs, 0, tagattribs.length);
        System.arraycopy(tagattriblabels, 0, newlabs, 0, tagattribs.length);
        newaids[i] = aid;
        newdefs[i] = cloneDefaultValue(defval);
        newlabs[i] = label;
        /* Replace existing ones */
        tagattribs = newaids;
        tagattribdefs = newdefs;
        tagattriblabels = newlabs;
        /* Return new values */
        return i;
    }
    /**
     * Get count of attributes reported by tags of this type
     * 
     * @returns count of attibute IDs (length of list from getTagAttributes()
     */
    public int getTagAttributeCount() {
        return tagattribs.length;
    }
    /**
     * Find attribute index for given attribute ID
     * 
     * @param id -
     *            attribute ID
     * @return index, or -1 if not found
     */
    public int getTagAttributeIndex(String id) {
        /* Start at end - if we're asking, its probably a registered attribute */
        for (int i = (tagattribs.length - 1); i >= 0; i--) {
            if (tagattribs[i].equals(id))
                return i;
        }
        return -1;
    }
    /**
     * Get base number of tag attributes (before extras registered)
     * @return base attrib count
     */
    public int getBaseTagAttributeCount() {
        return numbasetagattribs;
    }
    /**
     * Process tag ageouts - used to help tag type implementations
     * that need to efficiently support age-out processing
     */
    public static void processTagTypeAgeOut() {
        do {
            Iterator<DelayedTag> it = delayedtags.values().iterator();
            if(it.hasNext() == false)
                return;
            DelayedTag dt = it.next();
            long t = System.currentTimeMillis();;
            /* Due for processing? */
            if((t - dt.enq_ts) < (1000*TIME_INT)) {
                return;     /* If not, quit */
            }
            String tid = dt.tag.getTagGUID();
            delayedtags.remove(tid);    /* Remove from map */
            /* If done delaying, process it */
            if(dt.cnt_to_requeue <= 0) {    /* Done delaying */
                if(dt.tag.isValid())
                    dt.tag.getTagType().processDelayedTag(dt.tag);
            }
            else {  /* Else, decrement delay and requeue */
                dt.cnt_to_requeue--;
                dt.enq_ts = t;      /* Reset our time */
                delayedtags.put(tid, dt);       /* Put in queue */
            }
        } while (true); /* Repeat until empty or next guy is not due */
    }    
    /**
     * Queue tag for delayed processing
     * @param t - tag
     * @param delay - delay in seconds (will be rounded up to 10 second unit).  If <=0, remove
     *    from queue if queued
     */
    protected static void enqueueTagForDelay(Tag t, int delay) {
        enqueueTagForDelay(t, delay, false);
    }
    /**
     * Queue tag for delayed processing
     * @param t - tag
     * @param delay - delay in seconds (will be rounded up to 10 second unit).  If <=0, remove
     *    from queue if queued
     * @param onlyifnotdelayed - if true, only enqueue if not already enqueued (false=replace)
     */
    protected static void enqueueTagForDelay(Tag t, int delay, boolean onlyifnotdelayed) {
        String tid = t.getTagGUID();
        if(delayedtags.containsKey(tid)) {  /* If was in map, pull it */
            if(onlyifnotdelayed)
                return;
            delayedtags.remove(tid);
        }
        if(delay <= 0)
            return;
        DelayedTag dt = new DelayedTag();
        dt.enq_ts = System.currentTimeMillis();
        dt.cnt_to_requeue = (delay-1) / TIME_INT;
        dt.tag = t;
        delayedtags.put(tid, dt);       /* Put in queue */
    }
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param tag - delayed tag
     */
    public void processDelayedTag(Tag tag) {}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return false;	/* Default is no - override for types that do */
	}
    /**
     * Report raw tag event
     */
    public static void reportRawTagEvent(Tag tag, long timestamp, long payload, Reader reader, int[] ssi) {
    	if (tagbeacon_listeners != null) {
    		for (TagBeaconListener tl : tagbeacon_listeners) {
    			tl.tagBeacon(tag, timestamp, payload, reader, ssi);
    		}
    	}
    }
    /**
     * Add TagBeacnoListener for all tags
     * 
     * @param listen -
     *            listener to be added
     */
    public static void addTagBeaconListener(TagBeaconListener listen) {
        ArrayList<TagBeaconListener> newlist = new ArrayList<TagBeaconListener>();
        if (tagbeacon_listeners != null) {
        	newlist.addAll(tagbeacon_listeners);
        }
        newlist.add(listen);
        tagbeacon_listeners = newlist;
    }
    /**
     * Remove TagBeacnoListener for all tags
     * 
     * @param listen -
     *            listener to be removed
     */
    public static void removeTagBeaconListener(TagBeaconListener listen) {
        ArrayList<TagBeaconListener> newlist = new ArrayList<TagBeaconListener>();
        if (tagbeacon_listeners != null) {
        	newlist.addAll(tagbeacon_listeners);
        }
        newlist.remove(listen);
        if (newlist.isEmpty()) {
        	newlist = null;
        }
        tagbeacon_listeners = newlist;
    }
	/**
	 * Test if valid sensor definition for given tag type
	 * @param sd - sensor definition
	 * @return true, or false if not valid
	 */
    @Override
	public boolean isValidSensorDefinition(SensorDefinition sd) {
    	return false;
    }
	/**
	 * Notification that tag group attributes have been set/updated
	 * 
	 * @param grp - tag group
	 */
    @Override
	public void tagGroupAttributesUpdated(TagGroup grp) {
	}
	/**
	 * Notification that tag group has been deleted
	 * 
	 * @param grp - tag group
	 */
    @Override
	public void tagGroupDeleted(TagGroup grp) {
    }
}

