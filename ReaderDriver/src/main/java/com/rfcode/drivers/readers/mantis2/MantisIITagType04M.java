package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import com.rfcode.ranger.RangerServer;
/**
 * Tag type for treatment 04M tags - Mantis II Tags - Treatment 04M
 * See "Protocol and Interface Specification for Server Technology CDU Interface Tag" for details.
 *
 * @author Mike Primm
 */
public class MantisIITagType04M extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04M";
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID,
		PduOutletTagSubType.TAGTYPEID,
		PduBreakerTagSubType.TAGTYPEID,
		PduFeedLineTagSubType.TAGTYPEID
	};

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        LOWBATT_ATTRIB,
        PDUDISCONNECT_ATTRIB,
		PDUMODEL_ATTRIB,
		PDUSERIAL_ATTRIB,
		PDUMSGLOSS_ATTRIB,
		PDUTRUEPOWER_ATTRIB,
		PDUAPPPOWER_ATTRIB,
		PDUPWRFACT_ATTRIB,
		PDUWATTHOURS_ATTRIB,
		PDUAPPPWRHOURS_ATTRIB,
		PDUWATTHOURSTS_ATTRIB,
		PDUSERIALLIST_ATTRIB,
		PDUMODELLIST_ATTRIB,
		PDUTOWERDISCONNECT_ATTRIB,
		PDUDISCONNECTEDTOWERS_ATTRIB,
		PDUTOWERCOUNT_ATTRIB
	};
    private static final String[] TAGATTRIBLABS = {
        LOWBATT_ATTRIB_LABEL,
		PDUDISCONNECT_ATTRIB_LABEL,
		PDUMODEL_ATTRIB_LABEL,
		PDUSERIAL_ATTRIB_LABEL,
		PDUMSGLOSS_ATTRIB_LABEL,
		PDUTRUEPOWER_ATTRIB_LABEL,
		PDUAPPPOWER_ATTRIB_LABEL,
		PDUPWRFACT_ATTRIB_LABEL,
		PDUWATTHOURS_ATTRIB_LABEL,
		PDUAPPPWRHOURS_ATTRIB_LABEL,
		PDUWATTHOURSTS_ATTRIB_LABEL,
		PDUSERIALLIST_ATTRIB_LABEL,
		PDUMODELLIST_ATTRIB_LABEL,
		PDUTOWERDISCONNECT_ATTRIB_LABEL,
		PDUDISCONNECTEDTOWERS_ATTRIB_LABEL,
		PDUTOWERCOUNT_ATTRIB_LABEL };

    private static final int LOWBATT_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUMODEL_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUSERIAL_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
	private static final int PDUMSGLOSS_ATTRIB_INDEX = 4;

	private static final int PDUTRUEPOWER_ATTRIB_INDEX = 5;
	private static final int PDUAPPPOWER_ATTRIB_INDEX = 6;
	private static final int PDUPWRFACTOR_ATTRIB_INDEX = 7;
	private static final int PDUWATTHOURS_ATTRIB_INDEX = 8;
	private static final int PDUAPPPWRHOURS_ATTRIB_INDEX = 9;
	private static final int PDUWATTHOURSTS_ATTRIB_INDEX = 10;
	private static final int PDUSERIALLIST_ATTRIB_INDEX = 11;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 12;
	private static final int PDUTOWERDISCONNECT_ATTRIB_INDEX = 13;
	private static final int PDUDISCONNECTEDTOWERS_ATTRIB_INDEX = 14;
	private static final int PDUTOWERCOUNT_ATTRIB_INDEX = 15;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        Boolean.FALSE, Boolean.FALSE, "", "", null,
		null, null, null, null, null, null, null, null, Boolean.FALSE, 
		new ArrayList<Long>(), null };

	private static final int MIN_FLAGS_PAYLOAD = 0760;
	private static final int MAX_FLAGS_PAYLOAD = 0777;

	private static final int FLAGS_PDUDISCONNECT = 0x01;
	private static final int FLAGS_LOWBATT = 0x02;
	private static final int FLAGS_LINKEDPDUDISCONNECT = 0x04;
	private static final int FLAGS_MASTERPDUDISCONNECT = 0x08;


	private static final int MIN_START_PAYLOAD = 0400;
	private static final int MAX_START_PAYLOAD = 0577;

	private static final int MIN_CONT_PAYLOAD = 0000;
	private static final int MAX_CONT_PAYLOAD = 0377;

	private static final int MIN_ALARM_PAYLOAD = 0600;
	private static final int MAX_ALARM_PAYLOAD = 0637;

	private static final int MIN_OUTSTAT_PAYLOAD1 = 0640;
	private static final int MAX_OUTSTAT_PAYLOAD1 = 0677;

	private static final int MIN_OUTSTAT_PAYLOAD2 = 0700;
	private static final int MAX_OUTSTAT_PAYLOAD2 = 0737;

	private static final long DEF_PKT_INTERVAL = 10000;	/* 10 seconds */
	private static final long MAX_OUTSTAT_PERIOD = 1000;
	private static final int REBOOT_DELAY = 5;		/* 5 seconds */
    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;

	private static class OurPhaseState {
		SubTag	tag;			/* Tag object for phase */
		int	last_voltsx2;		/* Last reported P-N voltage x 2 */
		int	last_voltsx2_idx0;	/* Last reported P-N voltage x 2(index 0) */
		int	last_ampsx10;		/* Last amperage x 10 */
		int last_watts;			/* Last watts */
		int	last_kwatthours_x10;	/* Last kw-hr x 10 */
		double last_diff;		/* Last watthours added - used for VA-hours */
		double	sum_watts;		/* Sum of watts values for hour - used for hourly PF estimate */
		double	sum_voltamps;	/* Sum of volt-amp values for hour - used for hourly PF estimate */
		double total_watthours;	/* Accumulated watthours */
		double total_vahours;	/* Accumulate VA-hours */
		long	watthours_ts;	/* Start time of watthours accumulator */
		String	phid;
		Long    feedsetid;

		public OurPhaseState() {
			last_voltsx2 = -1;	/* None yet */
			last_voltsx2_idx0 = -1;
			last_watts = -1;
			last_ampsx10 = -1;
			last_diff = 0.0;
			last_kwatthours_x10 = -1;
			sum_watts = sum_voltamps = 0.0;
			total_watthours = -1.0;
			total_vahours = -1.0;
			watthours_ts = 0;
		}
	}
	private static final int SWITCH_ON = 0;
	private static final int SWITCH_OFF = 1;
	private static final int SWITCH_REBOOT = 2;

	private static class OurOutletState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;		/* Last reported amperage (x10) */
		int		last_watthours;		/* Last watthours */
		boolean last_vavail;	/* Last voltage availability status */
		long	last_watthours_ts;	/* Last time of watthours accumulator */
		double	last_watts;		/* Last calculated watts */
		double	total_watthours;	/* Accumulated watthours */
		double	total_vahours;	/* Accumulated VA-hours */
		double	last_diff;		/* Last watthours added - used for VA-hours */
		long	watthours_ts;	/* Start time of watthours accumulator */
		String	phid;
		String  label;
		Long    feedsetid;

		int		switch_state;		/* 0=on, 1=off, 2=reboot */
		boolean	sw_signalled;	/* true if switch_off state changed by signal */
		boolean sw_pending_upd;	/* If verify active, used to indicate an update is pending */		

		public OurOutletState() {
			last_amps_x10 = -1;
			last_vavail = true;
			last_watthours = -1;
			last_watthours_ts = -1;
			last_watts = -1.0;
			total_watthours = -1.0;
			total_vahours = -1.0;
			last_diff = 0.0;
			watthours_ts = 0;
			switch_state = SWITCH_ON;
			sw_signalled = false;
			sw_pending_upd = false;
		}
	}
	private static class OurBreakerState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;	/* Last amps, in 10ths */
		boolean	last_tripped;	/* Last tripped state */
		boolean	was_signalled;	/* true if state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */
		String	phid;
		Long    feedsetid;

		public OurBreakerState() {
			last_amps_x10 = -1;
			last_tripped = false;
			was_signalled = false;
			pending_upd = false;
		}
	}
	private static class OurFeedLineState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;	/* Last amps, in 10ths */

		boolean overload;		/* Line overload state */
		boolean	was_signalled;	/* true if overload state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */		

		boolean loadwarn;		/* Load warning state */
		String	feedid;
		Long    feedsetid;

		public OurFeedLineState() {
			last_amps_x10 = -1;
			overload = loadwarn = false;
			was_signalled = false;
			pending_upd = false;
		}
	}

	private static class OurMsgAccum {	/* Need one for each reader to maintain sequentiality */
		private int[] accum;		/* Accumulator for message bytes */
		private int num_accum;		/* Number of bytes accumulated */
		private long last_ts;

		public OurMsgAccum() {
			accum = new int[6];
			num_accum = 0;
			last_ts = 0;
		}
	}

	private enum PhaseConfig {
		N_A,
		NONE,
		SINGLE_PHASE,
		THREE_PHASE_WYE,
		THREE_PHASE_DELTA,
		SINGLE_PHASE_DUAL_FEED
	};

	private static class OurTowerState {
		StringBuilder serial_sb;
		String serial;
		StringBuilder model_sb;
		String model;
		int outlets_per_phase;
		boolean brk_alarms;
		PhaseConfig phase_cfg;
		boolean switched_outlets;

		OurPhaseState phasetags[];
		OurOutletState outtags[];
		OurBreakerState brktags[];
		OurFeedLineState feedtags[];

		public OurTowerState() {
			serial_sb = new StringBuilder();
			model_sb = new StringBuilder();
			serial = null;
			model = null;
			outlets_per_phase = 0;
			phase_cfg = PhaseConfig.N_A;
			switched_outlets = false;
		}
		/* Cleanup breakers */
		void cleanupBreakers() {
			if(brktags != null) {
				for(int i = 0; i < brktags.length; i++) {
					if(brktags[i] != null) {
						if(brktags[i].tag != null) {
							brktags[i].tag.lockUnlockTag(false);	/* Unlock it */
							brktags[i].tag = null;
						}
						brktags[i] = null;
					}
				}
				brktags = null;
			}
		}
		/* Cleanup outlets */
		void cleanupOutlets() {			
			if(outtags != null) {
				for(int i = 0; i < outtags.length; i++) {
					if(outtags[i] != null) {
						if(outtags[i].tag != null) {
							outtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							outtags[i].tag = null;
						}
						outtags[i] = null;
					}
				}
				outtags = null;
			}
		}
		/* Cleanup phases */
		void cleanupPhases() {
			if(phasetags != null) {
				for(int i = 0; i < phasetags.length; i++) {
					if(phasetags[i] != null) {
						if(phasetags[i].tag != null) {
							phasetags[i].tag.lockUnlockTag(false);	/* Unlock it */
							phasetags[i].tag = null;
						}
						phasetags[i] = null;
					}
				}
				phasetags = null;
			}
		}
		/* Cleanup feed lines */
		void cleanupFeedLines() {
			if(feedtags != null) {
				for(int i = 0; i < feedtags.length; i++) {
					if(feedtags[i] != null) {
						if(feedtags[i].tag != null) {
							feedtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							feedtags[i].tag = null;
						}
						feedtags[i] = null;
					}
				}
				feedtags = null;
			}
		}
	}
	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		private HashMap<String, OurMsgAccum> rdraccum;
		private int[]	accum;

		private int tower_count;
		private OurTowerState towers[];
		private boolean initdone;
		private boolean new10min;
		private boolean newhourly;

		/* Message state info - used to detect 10-minute and hour boundaries */
		private int last_phase_index;	/* Index reported by last phase msg */
		private int last_phase_id;		/* Phase ID reported by last phase msg */
		private int last_outlet_index;	/* Last outlet index reported (pwruse msg) */
		private int last_outletamps_index;	/* Last outlet index (amps message) */
		private int msg_cnt;
		private boolean did_low_batt;	/* If we saw low battery during hour */
		private boolean is_20_2hr;		/* If set, we're on 20 minute/2 hour schedule */

		long	watthours_ts;	/* Start time of watthours accumulator */
		double base_watthours;	/* total at start time watthours */
		double base_vahours;	/* total at start time VA-hours */

		private LinkedList<OurOutletState> rebooted_outlets;

		private int PHASE_WATTHOURS_MAX = 0x7FF;
		private int OUTLET_WATTHOURS_MAX = 0xFFFF;
		private int OUTLET_AMPS_NA = 0x1FF;
		private int PHASE_VOLTS_NA = 0x3FF;
		private int PHASE_AMPS_NA = 0x3FF;
		private int PHASE_ACTPOW_NA = 0xFFFF;
		private int FEED_AMPS_NA = 0x3FF;

		public OurTagState(MantisIITag tag) {
			tower_count = 1;	/* Default to 1 tower */
			resetDeviceConfiguration(tag, false);
			msg_cnt = -1;
			rdraccum = new HashMap<String, OurMsgAccum>();
			accum = null;
			is_20_2hr = false;
		}
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t) {
			cleanupTowers();
		}

		/**
		 * Clean up towers
		 */
		private void cleanupTowers() {
			if(towers != null) {
				for(int i = 0; i < towers.length; i++) {
					if(towers[i] != null) {
						towers[i].cleanupPhases();		/* Cleanup phases */
						towers[i].cleanupOutlets();		/* Cleanup outlets */
						towers[i].cleanupBreakers();		/* Cleanup breakers */
						towers[i].cleanupFeedLines();		/* Cleanup feed lines */
					}
				}
				towers = null;
			}
			initdone = false;
			new10min = false;
		}
		/**
		 * Receive message byte to accumulate
		 */
		public boolean accumulateByte(MantisIITag tag, byte data, long ts, boolean first, MantisIIReader rdr) {
			boolean change = false;

			if(rdr == null) return change;
			/* Find and/or init accumulator */
			OurMsgAccum a = rdraccum.get(rdr.getID());
			if(a == null) {
				a = new OurMsgAccum();
				rdraccum.put(rdr.getID(), a);
			}			

			if(first) {	/* If first one */
				a.last_ts = ts;		/* Save ts */
				a.num_accum = 1;
				a.accum[0] = 0xFF & (int)data;
			}
			else {
				long last_int = ts - a.last_ts;

				/* If received more than N times 110% of interval + 60% of interval after start packet */
//				if(last_int > ((11*a.num_accum + 6)*DEF_PKT_INTERVAL/10)) {
				/* If more than 4 seconds early or 4 seconds late, its bad */
				if( (last_int > ((10*a.num_accum + 4)*DEF_PKT_INTERVAL/10)) ||
					(last_int < ((10*a.num_accum - 4)*DEF_PKT_INTERVAL/10))) {
					/* Missing packet, so reset accumulator */
					a.num_accum = 0;
				}
				else if(a.num_accum == 0) {	/* No start, just skip it */
				}
				else {						/* Else, add to accumulator */
					a.accum[a.num_accum] = 0xFF & (int)data;	/* Add it */
					a.num_accum++;
					if(a.num_accum == 6) {	/* Got all of them? */
						/* Check parity count */
						int cnt = 0;
						for(int i = 3; i < 48; i++) {
							if((a.accum[i/8] & (0x80>>(i%8))) != 0) {
								cnt++;
							}
						}
						if((cnt % 4) == (a.accum[0]>>5)) {	/* If message checks */
							a.accum[0] = (a.accum[0] & 0x1F);		/* Trim off parity */
							/* Now, see if its the same message from a different reader than the last message - if so,
							 * its a duplicate we need to ignore */
							if((accum != a.accum) && (accum != null) && Arrays.equals(accum, a.accum)) {
								/* Skip */
							}
							else {
								accum = a.accum;		/* Point to new message, and process it */
								change = processMessage(tag) || change;
							}
						}
						else {
//							System.out.println("Checksum error " + (cnt % 4) + "!=" + (a.accum[0]>>5));
						}
						a.num_accum = 0;
					}
				}
			}
			return change;
		}
		/* Read bit range from accumulator */
		private int getBits(int start, int end) {
			int rslt = 0;
			if(start > 44) start = 44;
			if(end < 0) end = 0;
			for(int i = start; i >= end; i--) {
				rslt = rslt << 1;
				if((accum[5 - (i>>3)] & (1 << (i & 7))) != 0) {
					rslt |= 1;
				}
			}
			return rslt;		
		}
		private int getBit(int s) {
			return getBits(s, s);
		}

		/**
	     * Device configuration reset - due to model change, serial change, or other configuration inconsistency
		 */
		private boolean	resetDeviceConfiguration(MantisIITag tag, boolean change) {
			cleanupTowers();		/* Cleanup towers */
			towers = new OurTowerState[tower_count];
			for(int i = 0; i < tower_count; i++)
				towers[i] = new OurTowerState();
			initdone = false;
			last_phase_index = -1;
			last_phase_id = -1;
			last_outlet_index = -1;
			last_outletamps_index = -1;
			watthours_ts = 0;
			base_watthours = 0.0;
			base_vahours = 0.0;
			is_20_2hr = false;
			/* Clean out PDU settings */
			change = updateNull(tag, PDUMODEL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIAL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUPWRFACTOR_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIALLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERCOUNT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMSGLOSS_ATTRIB_INDEX) || change;

			return change;
		}
		/**
		 * Flush hour data, if any accumulated
		 */
		private boolean	flushHourData(MantisIITag tag, boolean change) {
			int i, j, k;
			change = flush10MinuteData(tag, change);
			if(!newhourly)
				return change;

//			if(is_20_2hr)
//				System.out.println(tag.getTagGUID() + ": Flush 2 hour data");
//			else
//				System.out.println(tag.getTagGUID() + ": Flush hour data");

			last_outlet_index = -1;
			last_outletamps_index = -1;
			newhourly = false;

			if(!initdone)
				return change;
			if(towers == null)
				return change;

			double tot_wh = 0.0;
			double tot_vah = 0.0;
			/* Loop through towers */
			for(j = 0; j < towers.length; j++) {
				OurTowerState tow = towers[j];
				Long towerid = Long.valueOf(j+1);
				/* Loop through phases and update energy used */
				for(i = 0; i < tow.phasetags.length; i++) {
					Double wh = null;
					OurPhaseState ps = tow.phasetags[i];

					if(ps.total_watthours >= 0)
						wh = Double.valueOf(ps.total_watthours);
					Long	wh_ts = null;
					if(ps.watthours_ts > 0)
						wh_ts = Long.valueOf(ps.watthours_ts);
					Double vah = null;
					/* Compute update to volt-amp-hours, if we can estimate PF */
					if((ps.sum_voltamps > 0.0) && (ps.last_diff > 0.0)) {
						double pf = (100.0 * ps.sum_watts / ps.sum_voltamps);
						if(pf > 0.0) {
							double last_wh = 100.0 * ps.last_diff;
							if(pf > 100.0)
								ps.total_vahours = ps.total_vahours + last_wh;
							else
								ps.total_vahours = ps.total_vahours + 
									(0.1 * Math.round(1000.0 * last_wh / pf));
						}
					}
					ps.sum_voltamps = ps.sum_watts = 0.0;
					ps.last_diff = 0.0;

					if(ps.total_vahours >= 0)
						vah = Double.valueOf(ps.total_vahours);
					/* Update phase tag */
					PduPhaseTagSubType.updateTag(ps.tag, null, null, null, null, null, null,
						(wh!=null)?wh:PduPhaseTagSubType.VALUE_NA, 
						(wh_ts!=null)?wh_ts:PduPhaseTagSubType.TS_VALUE_NA, 
						(vah!=null)?vah:PduPhaseTagSubType.VALUE_NA,
						null, towerid, ps.feedsetid);

					/* Accumulate watt-hours */
					if(wh != null) {
						if(tot_wh >= 0.0) tot_wh += wh;
					} else {
						tot_wh = -1.0;	/* Incomplete - can't do it now */
					}
					/* Accumulate volt-amp-hours */
					if(vah != null) {
						if(tot_vah >= 0.0) tot_vah += vah;
					} else {
						tot_vah = -1.0;	/* Incomplete - can't do it now */
					}
					/* Loop through outlets on phase */
					for(k = 0; k < tow.outlets_per_phase; k++) {
						OurOutletState out = tow.outtags[(i*tow.outlets_per_phase) + k];
					
						Double svolts = null;
						if(out.last_vavail == false)
							svolts = Double.valueOf(0.0);
						else if((ps.last_voltsx2_idx0 >= 0) && 
							(ps.last_voltsx2_idx0 < PHASE_VOLTS_NA))
							svolts = Double.valueOf(0.5 * ps.last_voltsx2_idx0);
						Double struepwr = null;
						if(out.last_watts >= 0)
							struepwr = Double.valueOf(out.last_watts);
						Double samps = null;
						if((out.last_amps_x10 >= 0) && (out.last_amps_x10 < OUTLET_AMPS_NA))
							samps = Double.valueOf(0.1 * out.last_amps_x10);
						/* If we have volts and amps, compute apparent power */
						Double sapppwr = null;				
						Double spowfact = null;
						if((svolts != null) && (samps != null)) {
							sapppwr = Double.valueOf(Math.round(svolts.doubleValue() * samps.doubleValue()));
							/* If we have apparent power and active power, computer power factor */
							if((sapppwr != null) && (sapppwr.doubleValue() > 0.0) && (struepwr != null)) {
								double pf = Math.round(100.0 * struepwr.doubleValue() / 
									sapppwr.doubleValue());
								if(pf < 0.0) pf = 0.0;
								if(pf > 100.0) pf = 100.0;
								spowfact = Double.valueOf(pf);
							}
						}
						String cfg = out.phid;
						wh = (out.total_watthours >= 0)?Double.valueOf(out.total_watthours):null;
						wh_ts = (out.watthours_ts > 0)?Long.valueOf(out.watthours_ts):null;
						vah = (out.total_vahours >= 0)?Double.valueOf(out.total_vahours):null;
						/* If watt-hours added, accumulate with VA-hours if PF is available */
						if(out.last_diff > 0.0) {
							double pf = (spowfact != null)?spowfact.doubleValue():0.0;
							if(pf > 0.0) {
								if(pf > 100.0)
									out.total_vahours = out.total_vahours + out.last_diff;
								else
									out.total_vahours = out.total_vahours + 
										(0.1 * Math.round(100.0 * 10.0 * out.last_diff / pf));
							}
							out.last_diff = 0.0;
						}
						/* Get switch state, if supported */
						Boolean sw = null;
						if(tow.switched_outlets) {
							if(out.sw_signalled) {
								sw = Boolean.valueOf(out.switch_state == SWITCH_ON);
							}
						}
						out.sw_signalled = false;
						PduOutletTagSubType.updateTag(out.tag, 
							(svolts!=null)?svolts:PduOutletTagSubType.VALUE_NA,
							(struepwr!=null)?struepwr:PduOutletTagSubType.VALUE_NA,
							(spowfact!=null)?spowfact:PduOutletTagSubType.VALUE_NA,
							(sapppwr!=null)?sapppwr:PduOutletTagSubType.VALUE_NA,
							(samps!=null)?samps:PduOutletTagSubType.VALUE_NA,
							cfg, 
							(wh!=null)?wh:PduOutletTagSubType.VALUE_NA,
							(wh_ts!=null)?wh_ts:PduOutletTagSubType.TS_VALUE_NA,
							(vah!=null)?vah:PduOutletTagSubType.VALUE_NA,
							towerid, out.label, out.feedsetid, sw);
					}
				}
				/* Loop for breakers */
				if(tow.brktags != null) {
					for(i = 0; i < tow.brktags.length; i++) {
						Double amps = null;
						if((tow.brktags[i].last_amps_x10 >= 0) && (tow.brktags[i].last_amps_x10 < PHASE_AMPS_NA))
							amps = Double.valueOf(tow.brktags[i].last_amps_x10/10.0);
						Boolean tripped = null;
						if(tow.brktags[i].was_signalled)
							tripped = Boolean.valueOf(tow.brktags[i].last_tripped);
						PduBreakerTagSubType.updateTag(tow.brktags[i].tag, 
							(amps!=null)?amps:PduBreakerTagSubType.VALUE_NA, tripped, towerid,
							tow.phasetags[i].phid, tow.brktags[i].feedsetid);
						tow.brktags[i].was_signalled = false;	/* Clear triggered flag */
					}
				}
				/* Loop through feed lines */
				if(tow.feedtags != null) {
					for(i = 0; i < tow.feedtags.length; i++) {
						Double amps = null;
						if((tow.feedtags[i].last_amps_x10 >= 0) && (tow.feedtags[i].last_amps_x10 < FEED_AMPS_NA))
							amps = Double.valueOf(0.1 * tow.feedtags[i].last_amps_x10);
						String config = tow.feedtags[i].feedid;
						Boolean overload = null;
						Boolean loadwarn = Boolean.valueOf(tow.feedtags[i].loadwarn || tow.feedtags[i].overload);

						if(tow.feedtags[i].was_signalled) {
							overload = Boolean.valueOf(tow.feedtags[i].overload);
						}

						PduFeedLineTagSubType.updateTag(tow.feedtags[i].tag, 
							(amps!=null)?amps:PduFeedLineTagSubType.VALUE_NA,
							config, towerid, overload, loadwarn, tow.feedtags[i].feedsetid);
						tow.feedtags[i].was_signalled = false;	/* Clear triggered flag */
					}
				}
			}
			/* Compute new message loss rate */
			if(msg_cnt >= 0) {	/* 60 messages per hour nominal */
				double v;
				if(is_20_2hr) {				/* If 20minute/2hr cycle? */
					if(did_low_batt)			/* If low battery signalled? */
						v = Math.round(100.0 - (msg_cnt/1.02));	/* 102 msgs per 2 hour */
					else
						v = Math.round(100.0 - (msg_cnt/1.20));	/* 120 msgs per 2 hour */
				}
				else {
					if(did_low_batt)			/* If low battery signalled? */
						v = Math.round(100.0 - (msg_cnt/0.51));	/* 51 msgs per hour */
					else
						v = Math.round(100.0 - (msg_cnt/0.60));	/* 60 msgs per hour */
				}
				if(v < 0.0) v = 0.0;
	            change = updateDouble(tag, PDUMSGLOSS_ATTRIB_INDEX, v) || change;
//				System.out.println(tag.getTagGUID() + ": msg_cnt=" + msg_cnt + ", loss=" + v);
			}
			/* If total watt-hours is defined */
			if(tot_wh >= 0.0) {
				if(watthours_ts == 0) {	/* First time? */
					watthours_ts = System.currentTimeMillis();
					base_watthours = tot_wh;
					if(tot_vah >= 0.0)
						base_vahours = tot_vah;
					else
						base_vahours = 0.0;
				}
				change = updateDouble(tag, PDUWATTHOURS_ATTRIB_INDEX, 
						Double.valueOf(tot_wh - base_watthours)) || change;
				change = updateLong(tag, PDUWATTHOURSTS_ATTRIB_INDEX, 
						Long.valueOf(watthours_ts)) || change;
				if(tot_vah >= 0.0) {
					change = updateDouble(tag, PDUAPPPWRHOURS_ATTRIB_INDEX, 
						Double.valueOf(tot_vah - base_vahours)) || change;
				}
				else {
					change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
				}
			}
			else {
				watthours_ts = 0;
				base_watthours = 0.0;
				base_vahours = 0.0;
				change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
				change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
				change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			}

			/* Clear message count */
			msg_cnt = 0;
			did_low_batt = false;	/* Reset flag */

			return change;
		}
		/**
		 * Flush 10 minute data, if any accumulated
		 */
		private boolean	flush10MinuteData(MantisIITag tag, boolean change) {
			if(!new10min)
				return change;

//			if(is_20_2hr)
//				System.out.println(tag.getTagGUID() + ": Flush 20-min data");
//			else
//				System.out.println(tag.getTagGUID() + ": Flush 10-min data");

			new10min = false;
			
			if(!initdone)
				return change;
			if(towers == null)
				return change;

			int i, j;
			double tot_tp = 0.0;
			double tot_ap = 0.0;

			/* Loop through towers */
			for(j = 0; j < towers.length; j++) {
				OurTowerState tow = towers[j];
				Long towerid = Long.valueOf(j+1);
				/* Loop through phases */
				for(i = 0; i < tow.phasetags.length; i++) {
					Double volts = null;
					Double vn = null;
					Double vp = null;
					if((tow.phasetags[i].last_voltsx2 >= 0) && (tow.phasetags[i].last_voltsx2 < PHASE_VOLTS_NA))
						volts = Double.valueOf(0.5 * tow.phasetags[i].last_voltsx2);
					if(tow.phase_cfg == PhaseConfig.THREE_PHASE_DELTA)
						vp = volts;
					else
						vn = volts;
					Double amps = null;
					if((tow.phasetags[i].last_ampsx10 >= 0) && (tow.phasetags[i].last_ampsx10 < PHASE_AMPS_NA))
						amps = Double.valueOf(0.1 * tow.phasetags[i].last_ampsx10);
					Double tp = null;
					if((tow.phasetags[i].last_watts >= 0) && (tow.phasetags[i].last_watts < PHASE_ACTPOW_NA))
						tp = Double.valueOf(tow.phasetags[i].last_watts);
					/* Now, if we have amps and volts, make apparent power */
					Double ap = null;
					Double pf = null;
					if((volts != null) && (amps != null)) {
						ap = Double.valueOf(Math.round(volts.doubleValue() * amps.doubleValue()));
						/* If we have apparent power and active power, computer power factor */
						if((ap != null) && (ap.doubleValue() > 0.0) && (tp != null)) {
							pf = Double.valueOf(Math.round(100.0 * tp.doubleValue() / ap.doubleValue()));
							if(pf.doubleValue() > 100.0) pf = Double.valueOf(100.0);
							/* Accumulate watts and voltamps for hourly PF estimate - gives weight by power use */
							tow.phasetags[i].sum_watts += tp.doubleValue();
							tow.phasetags[i].sum_voltamps += ap.doubleValue();
						}
					}
					String cfg = tow.phasetags[i].phid;
					/* Update phase tag */
					PduPhaseTagSubType.updateTag(tow.phasetags[i].tag, 
						(vn!=null)?vn:PduPhaseTagSubType.VALUE_NA,
						(vp!=null)?vp:PduPhaseTagSubType.VALUE_NA,  
						(tp!=null)?tp:PduPhaseTagSubType.VALUE_NA, 
						(pf!=null)?pf:PduPhaseTagSubType.VALUE_NA, 
						(ap!=null)?ap:PduPhaseTagSubType.VALUE_NA, 
						(amps!=null)?amps:PduPhaseTagSubType.VALUE_NA, 
						null, null, null, cfg, towerid, tow.phasetags[i].feedsetid);
					/* Accumulate for all phases */
					if(tp != null) {
						if(tot_tp >= 0.0) tot_tp += tp;
					}
					else
						tot_tp = -1.0;		/* Can't do total without all phases */
					if(ap != null) {
						if(tot_ap >= 0.0) tot_ap += ap;
					}
					else 
						tot_ap = -1.0;		/* Can't do total without all phases */
				}
				/* Loop through feed lines for load warning */
				if(tow.feedtags != null) {
					for(i = 0; i < tow.feedtags.length; i++) {
						Boolean loadwarn = Boolean.valueOf(tow.feedtags[i].loadwarn || tow.feedtags[i].overload);

						PduFeedLineTagSubType.updateTag(tow.feedtags[i].tag, 
							null, null, null, null, loadwarn, null);
					}
				}
			}
			/* Update top-level tag data, if we can */
			if(tot_tp < 0.0)
				change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			else
				change = updateDouble(tag, PDUTRUEPOWER_ATTRIB_INDEX, Double.valueOf(tot_tp)) || change;
			if(tot_ap < 0.0)
				change = updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX) || change;
			else
				change = updateDouble(tag, PDUAPPPOWER_ATTRIB_INDEX, Double.valueOf(tot_ap)) || change;
			if((tot_ap > 0.0) && (tot_tp >= 0.0)) {	/* If both defined, and AP > 0, do PF */
				double pf = Math.round(100.0 * tot_tp / tot_ap);
				if(pf < 0.0) pf = 0.0;
				if(pf > 100.0) pf = 100.0;
				change = updateDouble(tag, PDUPWRFACTOR_ATTRIB_INDEX, pf) || change;
			}
			else {
				change = updateNull(tag, PDUPWRFACTOR_ATTRIB_INDEX) || change;
			}
			return change;
		}

		/**
		 * Initialize device if needed, and we have full set of configuration data
		 */
		private void	initDeviceConfiguration(MantisIITag tag) {
			int i;
			TagSubGroup tg;
			if(initdone)
				return;
			if((towers == null) || (towers.length == 0)) {
				return;
			}
			for(i = 0; i < towers.length; i++) {
				if((towers[i].model == null) || (towers[i].serial == null) || 
					(towers[i].phase_cfg == PhaseConfig.N_A))
					return;
			}
			/* Initialize all towers */
			for(int j = 0; j < tower_count; j++) {
				OurTowerState ts = towers[j];
				String id_suffix = (j == 0)?"":"B";
				String tow_prefix = (tower_count == 1)?"":((j == 0)?"A:":"B:");
				Long towerid = Long.valueOf(j+1);
				/* Initialize phases */
				int pcnt = 1;
				String[] phid = { "L1-N" };
				String[] phlbl = { "L1" };
				Long[]   phfeedsetid = { new Long(1) };
				String[] flid = { "L1" };
				Long[]   flfeedsetid = { new Long(1) };
				switch(ts.phase_cfg) {
					case SINGLE_PHASE:
						break;
					case THREE_PHASE_WYE:
						pcnt = 3;
						phid = new String[]{ "L1-N", "L2-N", "L3-N" };
						phlbl = new String[]{ "L1-", "L2-", "L3-" };
						phfeedsetid = new Long[]{ new Long(1), new Long(1), new Long(1) };
						flid = new String[]{ "L1", "L2", "L3", "N" };
						flfeedsetid = new Long[]{ new Long(1), new Long(1), new Long(1), new Long(1) };
						break;
					case THREE_PHASE_DELTA:
						pcnt = 3;
						phid = new String[]{ "L1-L2", "L2-L3", "L3-L1" };
						phlbl = new String[]{ "XY-", "YZ-", "ZX-" };
						phfeedsetid = new Long[]{ new Long(1), new Long(1), new Long(1) };
						flid = new String[]{ "L1", "L2", "L3" };
						flfeedsetid = new Long[]{ new Long(1), new Long(1), new Long(1) };
						break;
					case SINGLE_PHASE_DUAL_FEED:
						pcnt = 2;
						phid = new String[]{ "L1-N", "L1-N" };
						phlbl = new String[]{ "A-", "B-" };
						phfeedsetid = new Long[]{ new Long(1), new Long(2) };
						flid = new String[]{ "L1", "L1" };
						flfeedsetid = new Long[]{ new Long(1), new Long(2) };
						break;
					default:
						break;
				}
				ts.phasetags = new OurPhaseState[pcnt];
				tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
				for(i = 0; i < ts.phasetags.length; i++) {
					ts.phasetags[i] = new OurPhaseState();
					ts.phasetags[i].phid = phid[i];
					ts.phasetags[i].feedsetid = phfeedsetid[i];
					ts.phasetags[i].tag = SubTag.findOrCreateTag(tg, tag, 
						String.format("phase%d%s",i+1, id_suffix), false,
						new int[] { PduPhaseTagSubType.PDUCONFIG_INDEX, 
							PduPhaseTagSubType.PDUFEEDSETID_INDEX,
							PduPhaseTagSubType.PDUTOWERID_INDEX },
						new Object[] { ts.phasetags[i].phid, ts.phasetags[i].feedsetid, towerid } );
					ts.phasetags[i].tag.lockUnlockTag(true);
//					PduPhaseTagSubType.updateTag(ts.phasetags[i].tag, null, null, null, null, null, null,
	//					null, null, null, ts.phasetags[i].phid, towerid, ts.phasetags[i].feedsetid);
				}
				/* Initialize outlets */
				ts.outtags = new OurOutletState[pcnt * ts.outlets_per_phase];
				tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
				for(i = 0; i < ts.outtags.length; i++) {
					int phidx = i/ts.outlets_per_phase;
					ts.outtags[i] = new OurOutletState();
					ts.outtags[i].phid = phid[phidx];
					ts.outtags[i].feedsetid = phfeedsetid[phidx];
					ts.outtags[i].label = tow_prefix + phlbl[phidx] + ((i%ts.outlets_per_phase)+1);
					ts.outtags[i].tag = SubTag.findOrCreateTag(tg, tag, 
						String.format("outlet%02d%s",i+1, id_suffix), false,
						new int[] { PduOutletTagSubType.PDUCONFIG_INDEX, 
							PduOutletTagSubType.PDUFEEDSETID_INDEX,
							PduOutletTagSubType.PDUTOWERID_INDEX,
							PduOutletTagSubType.PDULABEL_INDEX },
						new Object[] { ts.outtags[i].phid, ts.outtags[i].feedsetid, towerid, ts.outtags[i].label });
					ts.outtags[i].tag.lockUnlockTag(true);
//					PduOutletTagSubType.updateTag(ts.outtags[i].tag, null, null, null,
//						null, null, ts.outtags[i].phid, null, null, null, towerid, ts.outtags[i].label, 
//						ts.outtags[i].feedsetid);
				}
				/* Initialize breakers, if supported */
				if(ts.brk_alarms) {
					ts.brktags = new OurBreakerState[pcnt];
					tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduBreakerTagSubType.TAGTYPEID);
					for(i = 0; i < ts.brktags.length; i++) {
						ts.brktags[i] = new OurBreakerState();
						ts.brktags[i].phid = phid[i];
						ts.brktags[i].feedsetid = phfeedsetid[i];
						ts.brktags[i].tag = SubTag.findOrCreateTag(tg, tag,
							String.format("breaker%02d%s",i+1, id_suffix), false,
							new int[] { PduBreakerTagSubType.PDUCONFIG_INDEX,
								PduBreakerTagSubType.PDUFEEDSETID_INDEX,
								PduBreakerTagSubType.PDUTOWERID_INDEX },
							new Object[] { ts.brktags[i].phid, ts.brktags[i].feedsetid, towerid });
						ts.brktags[i].tag.lockUnlockTag(true);
//						PduBreakerTagSubType.updateTag(ts.brktags[i].tag, null, null, towerid,
//							ts.brktags[i].phid, ts.brktags[i].feedsetid);
					}
				}
				/* Initialize feed lines */
				ts.feedtags = new OurFeedLineState[flid.length];
				tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduFeedLineTagSubType.TAGTYPEID);
				for(i = 0; i < ts.feedtags.length; i++) {
					ts.feedtags[i] = new OurFeedLineState();
					ts.feedtags[i].feedid = flid[i];
					ts.feedtags[i].feedsetid = flfeedsetid[i];
					ts.feedtags[i].tag = SubTag.findOrCreateTag(tg, tag,
						String.format("feedline%s%s%s", flid[i],
						(flfeedsetid[i].longValue()==1)?"":("_"+flfeedsetid[i]),
						id_suffix), false,
						new int[] { PduFeedLineTagSubType.PDUCONFIG_INDEX,
							PduFeedLineTagSubType.PDUFEEDSETID_INDEX,
							PduFeedLineTagSubType.PDUTOWERID_INDEX },
						new Object[] { ts.feedtags[i].feedid, ts.feedtags[i].feedsetid, towerid } );
					ts.feedtags[i].tag.lockUnlockTag(true);
//					PduFeedLineTagSubType.updateTag(ts.feedtags[i].tag, 
//						null, ts.feedtags[i].feedid, towerid, null, null, ts.feedtags[i].feedsetid);
				}
			}
			/* If more than 1 tower AND we have outlets, we're on 20min/2hr cycle */
			if((towers.length > 1) && ((towers[0].outtags.length+towers[1].outtags.length) > 0))
				is_20_2hr = true;
			else
				is_20_2hr = false;
//			System.out.println("is_20_2hr=" + is_20_2hr);

			initdone = true;
		}
		/* Handle per phase message */
		private boolean	handlePerPhaseMessage(MantisIITag tag, boolean change, int m_s, int phid, int index,
			boolean loadwarn, int voltsx2, int ampsx10, int watts) {

//			System.out.println(tag.getTagGUID() + ": Per Phase Volts/Amps/Power: P" + (phid+1) + 
//				", m_s=" + m_s + ", period-index="
//				+ index + ", volts=" + ((voltsx2 == PHASE_VOLTS_NA)?"---":(0.5 * voltsx2)) + "V, amps=" + 
//				((ampsx10 == PHASE_AMPS_NA)?"---":(0.1 * ampsx10)) + "A, ActPower=" +
//				((watts == PHASE_ACTPOW_NA)?"---":watts) + "W");

			if((towers == null) || (towers.length <= m_s) || (towers[m_s] == null))
				return change;
			OurTowerState tow = towers[m_s];
			if((tow.phasetags == null) || (phid >= tow.phasetags.length))
				return change;
			int tow_ph_id = (10 * m_s) + phid;	/* Make global level phase ID */
			/* See if we're due to flush hour */
			if((last_phase_index != 0) && (index == 0)) {	/* if first zero index */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index > index) {			/* We missed zero */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index != index) {		/* If new one */
				change = flush10MinuteData(tag, change);
			}
			else if(last_phase_id >= tow_ph_id) {
				change = flush10MinuteData(tag, change);
			}
			last_phase_index = index;
			last_phase_id = tow_ph_id;

			if(!initdone)
				return change;

			/* Update values */
			tow.phasetags[phid].last_voltsx2 = voltsx2;		/* Update last seen voltage */
			if(index == 0) {	/* If zero value, update it */
				tow.phasetags[phid].last_voltsx2_idx0 = voltsx2;
			}
			else if(tow.phasetags[phid].last_voltsx2_idx0 < 0) {	/* Or if its missing */
				tow.phasetags[phid].last_voltsx2_idx0 = voltsx2;
			}				
			tow.phasetags[phid].last_ampsx10 = ampsx10;
			tow.phasetags[phid].last_watts = watts;
			/* If we've got breakers, copy there too */
			if((tow.brktags != null) && (phid < tow.brktags.length)) {
				tow.brktags[phid].last_amps_x10 = ampsx10;
			}
			/* Update load warning on feed line, if defined */
			if((tow.feedtags != null) && (tow.feedtags.length > phid) && (tow.feedtags[phid] != null)) {
				tow.feedtags[phid].loadwarn = loadwarn;
			}
			new10min = newhourly = true;

			return change;
		}
		/* Handle per phase energy message */
		private boolean	handlePerPhaseEnergyMessage(MantisIITag tag, boolean change, int m_s, 
			boolean[] lineover, boolean[] brkstat, int[] kwatthours_x10) {
			int i;

//			System.out.print(tag.getTagGUID() + ": Phase Energy: m_s=" + m_s);
//			for(i = 0; i < 3; i++) {
//				System.out.print(", lineover" + i + "=" + lineover[i] + ", brkstat" + i + "=" + brkstat[i] + ", kwh" + i + "="
//					+ ((kwatthours_x10[i] == PHASE_WATTHOURS_MAX)?"N/A":(0.1*kwatthours_x10[i]) + "kW-hr"));
//			}
//			System.out.println();

			if((towers == null) || (towers.length <= m_s) || (towers[m_s] == null))
				return change;
			OurTowerState tow = towers[m_s];
			if(tow.phasetags == null)
				return change;
			
			int cnt = 3;	/* 3 are passed in - trim to actual phase count */
			if(cnt > tow.phasetags.length) cnt = tow.phasetags.length;
			long ts = System.currentTimeMillis();
			/* Loop through phases */
			for(i = 0; i < cnt; i++) {
				int diff = 0;
				int wh = kwatthours_x10[i];
				if(wh < PHASE_WATTHOURS_MAX) {	/* If valid watthours value */
					if(tow.phasetags[i].last_kwatthours_x10 >= 0) {	/* If we had old value */
						diff = (wh + PHASE_WATTHOURS_MAX - tow.phasetags[i].last_kwatthours_x10) %
							PHASE_WATTHOURS_MAX;
					}
					tow.phasetags[i].last_kwatthours_x10 = wh;
	
					if(tow.phasetags[i].total_watthours < 0.0) {	/* If no accumulator, start one */
						tow.phasetags[i].total_watthours = 0.0;
						tow.phasetags[i].total_vahours = 0.0;
						tow.phasetags[i].watthours_ts = ts;		/* Save time reference */
						tow.phasetags[i].last_diff = 0.0;
					}
					else {
						tow.phasetags[i].total_watthours += (100.0 * diff); /* Add diff as watt-hours */
						tow.phasetags[i].last_diff = diff;	/* Save diff - accumulate VA-hrs once we have PF */
					}
				}
				else {
					tow.phasetags[i].last_kwatthours_x10 = -1;
					tow.phasetags[i].total_watthours = -1.0;
					tow.phasetags[i].total_vahours = -1.0;
					tow.phasetags[i].last_diff = 0.0;
					tow.phasetags[i].watthours_ts = 0;
				}
			}
			/* Loop through feed lines */
			cnt = 3;
			if(cnt > tow.feedtags.length) cnt = tow.feedtags.length;
			for(i = 0; i < cnt; i++) {
				if(tow.feedtags[i].was_signalled) {	/* Already updated by alarm signal */
					/* Do nothing */
				}
				else {
					tow.feedtags[i].overload = lineover[i];
					tow.feedtags[i].was_signalled = true;
				}
			}
			/* Loop through breakers, if defined */
			if(tow.brktags != null) {
				cnt = 3;
				if(cnt > tow.brktags.length) cnt = tow.brktags.length;
				for(i = 0; i < cnt; i++) {
					if(tow.brktags[i].was_signalled) {	/* If signalled, ignore this update (too old) */
					}
					else {
						tow.brktags[i].last_tripped = brkstat[i];
						tow.brktags[i].was_signalled = true;
					}
				}
			}
			return change;
		}

		/* Handle PDU model/serial messages */
		private boolean	handlePDUSerialMessage(MantisIITag tag, boolean change) {
			int idx = getBits(39,38);
			int m_s = getBit(37);	/* Master/slave flag */
			int i;
//			System.out.println(tag.getTagGUID() + ": PDU serial " + (idx+1) + ": m/s=" + m_s + ", done=" + ((idx==0)?0:getBit(0)));
			/* If first index, check for tower count, outlet count */
			if(idx == 0) {
				int newcnt = 1 + getBit(6);
				if(tower_count != newcnt) {	/* New count? */
					tower_count = newcnt;	/* Update count */
					change = resetDeviceConfiguration(tag, change);	/* Reset all */
				}
	            change = updateLong(tag, PDUTOWERCOUNT_ATTRIB_INDEX, tower_count) || change;
//				System.out.println("tower_count=" + tower_count);
			}
			if(m_s >= tower_count) {	/* If slave, and we haven't heard about master, skip for now */
				return change;
			}
			/* Update buffer length, if needed */
			if(towers[m_s].serial_sb.length() < (5 + (idx*6)))
				towers[m_s].serial_sb.setLength(5 + (idx*6));
			/* If first packet, handle 5 characters plus outlet count */
			if(idx == 0) {
				for(i = 0; i < 5; i++) {
					towers[m_s].serial_sb.setCharAt(i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				int nout = getBits(5, 0);		/* Get outlets per phase count */
				/* If mismatch on number of outlets per phase, reset device configuration */
				if(towers[m_s].outlets_per_phase != nout) {
					if(towers[m_s].outlets_per_phase != 0) {	/* If we had different value before, reset everything */
						change = resetDeviceConfiguration(tag, change);
					}
					towers[m_s].outlets_per_phase = nout;
				}
//				System.out.println("outlets_per_phase[" + m_s + "]=" + nout);
			}
			else {	/* Second or later has 6 characters, and may be end */
				for(i = 0; i < 6; i++) {
					towers[m_s].serial_sb.setCharAt((idx*6) + i - 1, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				/* If end packet, and we've got no gaps, process serial number */
				if((getBit(0) != 0) && (towers[m_s].serial_sb.indexOf("\0") < 0)) {	/* No nulls left? */
					String newserial = towers[m_s].serial_sb.toString().trim();	/* Make trimmed number */
					/* If different from old one, reset configuration */
					if((towers[m_s].serial != null) && (!towers[m_s].serial.equals(newserial))) {
						int nout = towers[m_s].outlets_per_phase;
						change = resetDeviceConfiguration(tag, change);
						towers[m_s].serial = newserial;
						towers[m_s].outlets_per_phase = nout;	/* Preseve this, since it came with new serial */
					}
					else if(towers[m_s].serial == null) {	/* If no old, don't reset */
						towers[m_s].serial = newserial;
					}
		            /* Set serial, if for first tower */
					if(m_s == 0) {
			            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX, towers[m_s].serial)
							|| change;
					}
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					for(i = 0; i < tower_count; i++) {
						if(towers[i].serial != null) {
							sn_lst.add(towers[i].serial);
						}
						else {
							sn_lst.add("");
						}
					}
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) 
						|| change;
//					System.out.println("serial[" + m_s + "]=" + newserial);
					towers[m_s].serial_sb.setLength(0);	/* Reset accumulator */
					/* Initialize device configuration, if needed and ready */
					initDeviceConfiguration(tag);
				}
			}
			return change;
		}
		/* Handle PDU model message */
		private boolean handlePDUModelMessage(MantisIITag tag, boolean change) {
			int i;
			int idx = getBits(39,38);
			int m_s = getBit(37);	/* Master/slave flag */

//			System.out.println(tag.getTagGUID() + ": PDU model " + (idx+1) + ": m/s=" + m_s + ", done=" + ((idx==0)?0:getBit(0)));

			if(m_s >= tower_count) {	/* If slave, and we haven't heard about master, skip for now */
				return change;
			}
			/* Update buffer length, if needed */
			if(towers[m_s].model_sb.length() < (5 + (idx*6)))
				towers[m_s].model_sb.setLength(5 + (idx*6));
			/* If first packet, handle 5 characters plus phase and breaker/fuse alarm support */
			if(idx == 0) {
				for(i = 0; i < 5; i++) {
					towers[m_s].model_sb.setCharAt(i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				/* Check for phase configuration */
				PhaseConfig newcfg = PhaseConfig.N_A;
				switch(getBits(5,3)) {
					case 0:
						newcfg = PhaseConfig.NONE;
						break;
					case 1:
						newcfg = PhaseConfig.SINGLE_PHASE;
						break;
					case 2:
						newcfg = PhaseConfig.THREE_PHASE_WYE;
						break;
					case 3:
						newcfg = PhaseConfig.THREE_PHASE_DELTA;
						break;
					case 4:
						newcfg = PhaseConfig.SINGLE_PHASE_DUAL_FEED;
						break;
					default:
						newcfg = PhaseConfig.N_A;
						break;
				}
				/* If mismatch on phase config, reset device */
				if(towers[m_s].phase_cfg != newcfg) {
					if(towers[m_s].phase_cfg != PhaseConfig.N_A) {
						change = resetDeviceConfiguration(tag, change);
					}
					towers[m_s].phase_cfg = newcfg;
				}
//				System.out.println("phasecfg=" + newcfg);
				/* Check for breaker alarm support */
				towers[m_s].brk_alarms = (getBit(6) == 1);
//				System.out.println("brk_alarms=" + towers[m_s].brk_alarms);
				/* Check for switched outlet support */
				towers[m_s].switched_outlets = (getBit(2) == 1);
//				System.out.println(tag.getTagGUID() + ": tower[" + m_s + "].switched_outlets=" + towers[m_s].switched_outlets);
			}
			else {	/* Else, not first model packet - add 6 characters and see if we're done */
				for(i = 0; i < 6; i++) {
					towers[m_s].model_sb.setCharAt(5 + (6*(idx-1)) + i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				/* If end, and no more gaps, process model number */
				if((getBit(0) != 0) && (towers[m_s].model_sb.indexOf("\0") < 0)) {
					String nmodel = towers[m_s].model_sb.toString().trim();
					/* If mismatch, reset configuration */
					if((towers[m_s].model != null) && (!towers[m_s].model.equals(nmodel))) {
						boolean brk = towers[m_s].brk_alarms;
						boolean sw = towers[m_s].switched_outlets;
						PhaseConfig cfg = towers[m_s].phase_cfg;
						change = resetDeviceConfiguration(tag, change);
						/* Preserve data that came with model packet sequence we just processed */
						towers[m_s].model = nmodel;
						towers[m_s].brk_alarms = brk;
						towers[m_s].switched_outlets = sw;
						towers[m_s].phase_cfg = cfg;
					}
					else if(towers[m_s].model == null)
						towers[m_s].model = nmodel;
//					System.out.println("model[" + m_s + "]=" + nmodel);
		            /* Set model, if first PDU */
					if(m_s == 0) {
			            change = updateString(tag, PDUMODEL_ATTRIB_INDEX, towers[m_s].model)
							|| change;
					}
					/* Update model number list */
					List<String> mn_lst = new ArrayList<String>();
					for(i = 0; i < tower_count; i++) {
						if(towers[i].serial != null) {
							mn_lst.add(towers[i].model);
						}
						else {
							mn_lst.add("");
						}
					}
		            change = updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, mn_lst) 
						|| change;
					/* Clear accumulator */
					towers[m_s].model_sb.setLength(0);
					/* Initialize device configuration, if needed and ready */
					initDeviceConfiguration(tag);
				}
			}
			return change;
		}
		/* Handle per-outlet power use message */
		private boolean handlePerOutletPowerUseMessage(MantisIITag tag, boolean change,
			int m_s, int phid, int oindex, boolean vavail[], int w_hr[], boolean swoff[]) {
			int i;
			int oind = oindex + (m_s * 1000) + (phid * 100);	/* Make global-level index */
			/* If this is below last one, flush hour */
			if(oind < last_outlet_index) {
				change = flushHourData(tag, change);
			}				
			last_outlet_index = oind;

//			System.out.print(tag.getTagGUID() + ": Per Outlet Watt-Hour: m_s=" + m_s + ", phid=" + phid + ", oindex=" + oindex);
//			for(i = 0; i < 2; i++) {
//				System.out.print(", vavail" + i + "=" + vavail[i] + ", w-hr" + i + "=" + 
//					((w_hr[i] != OUTLET_WATTHOURS_MAX)?w_hr[i]:"---") + "W-Hr");
//			}
//			System.out.println();

			if(!initdone)
				return change;
			if(m_s >= tower_count) {	/* If slave, and we haven't heard about master, skip for now */
				return change;
			}
			OurTowerState tow = towers[m_s];
			int obase = (phid * tow.outlets_per_phase) + (2*oindex);
			if((tow.outtags == null) || (obase >= tow.outtags.length)) {
				return change;
			}

			long ts = System.currentTimeMillis();	/* Get time in milliseconds */
			for(i = 0; i < 2; i++) {
				if((obase + i) < tow.outtags.length) {
					tow.outtags[obase+i].last_vavail = vavail[i];
					int diff = 0;
					if(w_hr[i] < OUTLET_WATTHOURS_MAX) {	/* If valid watthours value */
						if(tow.outtags[obase+i].last_watthours >= 0) {	/* If we had old value */
							long elapsed = ts - tow.outtags[obase+i].last_watthours_ts;
							if(elapsed <= 0) elapsed = 3600000;	/* Assume 1 hour if bad */
							diff = (w_hr[i] + OUTLET_WATTHOURS_MAX - tow.outtags[obase+i].last_watthours) %
								OUTLET_WATTHOURS_MAX;
							/* Adjust for period between readings */
							tow.outtags[obase+i].last_watts = Math.round(diff * 3600000.0 / (double)elapsed);
						}
						tow.outtags[obase+i].last_watthours = w_hr[i];
						tow.outtags[obase+i].last_watthours_ts = ts;
	
						if(tow.outtags[obase+i].total_watthours < 0.0) {	/* If no accumulator, start one */
							tow.outtags[obase+i].total_watthours = 0.0;
							tow.outtags[obase+i].total_vahours = 0.0;
							tow.outtags[obase+i].watthours_ts = ts;		/* Save time reference */
							tow.outtags[obase+i].last_diff = 0.0;
						}
						else {
							tow.outtags[obase+i].total_watthours += diff;
							tow.outtags[obase+i].last_diff = diff;	/* Save diff - accumulate VA-hrs once we have PF */
						}
					}
					else {
						tow.outtags[obase+i].last_watthours = -1;
						tow.outtags[obase+i].last_watthours_ts = -1;
						tow.outtags[obase+i].last_watts = -1.0;
						tow.outtags[obase+i].total_watthours = -1.0;
						tow.outtags[obase+i].total_vahours = -1.0;
						tow.outtags[obase+i].last_diff = 0.0;
						tow.outtags[obase+i].watthours_ts = 0;
					}
					if(tow.switched_outlets) {
						if(tow.outtags[obase+i].sw_signalled) {	/* Already updated by alarm signal */
							/* Do nothing - already better value */
						}
						else {
							tow.outtags[obase+i].switch_state = (swoff[i]?SWITCH_OFF:SWITCH_ON);
							tow.outtags[obase+i].sw_signalled = true;
						}
					}
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle per-outlet current message */
		private boolean handlePerOutletCurrentMessage(MantisIITag tag, boolean change, int m_s, int phid, 
			int oindex, int amps_x10[]) {
			int i;
			int oind = oindex + (m_s * 1000) + (phid * 100);	/* Make global-level index */
			/* If this is below last one, flush hour */
			if(oind < last_outletamps_index) {
				change = flushHourData(tag, change);
			}				
			last_outletamps_index = oind;

//			System.out.print(tag.getTagGUID() + ": Per Outlet Current: m_s=" + m_s + ", phid=" + phid + ", oindex=" + oindex);
//			for(i = 0; i < 4; i++) {
//				System.out.print(", ampsx10" + i + "=" + ((amps_x10[i] != 0x1FF)?0.1*amps_x10[i]:"---") + "A");
//			}
//			System.out.println();

			if(!initdone)
				return change;
			if(m_s >= tower_count) {	/* If slave, and we haven't heard about master, skip for now */
				return change;
			}
			OurTowerState tow = towers[m_s];
			int obase = (phid * tow.outlets_per_phase) + (4*oindex);
			if((tow.outtags == null) || (obase >= tow.outtags.length)) {
				return change;
			}

			for(i = 0; i < 4; i++) {
				if((obase + i) < tow.outtags.length) {
					tow.outtags[obase + i].last_amps_x10 = amps_x10[i];
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle feed line current message */
		private boolean handleFeedLineCurrentMessage(MantisIITag tag, boolean change,
			int m_s, int[] amps_x10) {
			int i;

//			System.out.print(tag.getTagGUID() + ": Feed Line Current: m_s=" + m_s);
//			for(i = 0; i < 4; i++) {
//				if(amps_x10[i] != FEED_AMPS_NA) {
//					if(i < 3)
//						System.out.print(", L" + (i+1) + "=" + (0.1 * amps_x10[i]) + "A");
//					else
//						System.out.print(", N=" + (0.1 * amps_x10[i]) + "A");
//				}
//			}
//			System.out.println();

			if(m_s >= tower_count) {	/* If slave, and we haven't heard about master, skip for now */
				return change;
			}
			OurTowerState tow = towers[m_s];
			if(tow.feedtags == null)
				return change;
			if(!initdone)
				return change;
			int cnt = tow.feedtags.length;
			if(cnt > 4) cnt = 4;
			for(i = 0; i < cnt; i++) {
				tow.feedtags[i].last_amps_x10 = amps_x10[i];
			}
			newhourly = true;

			return change;
		}
		/* Process valid message buffer */
		private boolean processMessage(MantisIITag tag) {
			int i;
			boolean change = false;
			String mtype = null;

//			System.out.print(tag.getTagGUID() + ": message: ");
//			for(i = 0; i < 6; i++) {
//				System.out.print(String.format("%02x", accum[i]));
//			}
//			System.out.println();

			/* If we've hit first hour flush, count messages */
			if(msg_cnt >= 0)
				msg_cnt++;

			if(getBits(44, 43) == 0x03) {	/* If 44-43 = 1-1, Per Phase Voltage */
				int m_s = getBit(42);
				int ph = getBits(41,40);
				change = handlePerPhaseMessage(tag, change, getBit(42), getBits(41,40), getBits(39,37),
					(getBit(36) == 1), getBits(35,26), getBits(25,16), getBits(15,0));
				mtype = "Per-Phase Power:" + m_s + ":" + ph;
			}
			/* If 44-40 = 00000, PDU serial */
			else if(getBits(44,40) == 0x00) {
				change = handlePDUSerialMessage(tag, change);
				mtype = "Serial:" + getBit(37) + ":" + getBits(39,38);
			}
			/* If 44-40 = 00001, PDU model */
			else if(getBits(44,40) == 0x01) {
				change = handlePDUModelMessage(tag, change);
				mtype = "Model:" + getBit(37) + ":" + getBits(39,38);
			}
			/* If 44-42 = 101, phase cumulative energy */
			else if(getBits(44,42) == 0x05) {
				boolean[] lineover = new boolean[3];
				boolean[] brkstat = new boolean[3];
				int[] kwatthours_x10 = new int[3];
				for(i = 0; i < 3; i++) {
					lineover[i] = (getBit(40-i) == 1);
					brkstat[i] = (getBit(37-i) == 1);
					kwatthours_x10[i] = getBits(32-(11*i),22-(11*i));
				}
				change = handlePerPhaseEnergyMessage(tag, change, getBit(41), lineover,
					brkstat, kwatthours_x10);
				mtype = "Phase-Energy:" + getBit(41);
			}
			/* If 44-43 = 0-1, per outlet */
			else if(getBits(44,43) == 0x01) {
				boolean[] vavail = new boolean[2];
				boolean[] swoff = new boolean[2];
				int[] w_h = new int[2];
				for(i = 0; i < 2; i++) {
					vavail[i] = (getBit(35-i) == 1);
					w_h[i] = getBits(31-(16*i), 16-(16*i));
					swoff[i] = (getBit(33-i) == 1);
				}
				change = handlePerOutletPowerUseMessage(tag, change, getBit(42), getBits(41, 40),
					getBits(39, 36), vavail, w_h, swoff);
				mtype = "Per-Outlet-Energy:" + getBit(42) + ":" + getBits(41, 40) + ":" + getBits(39, 36);
			}
			/* If 44-42 = 100, per outlet current */
			else if(getBits(44,42) == 0x04) {
				int[] ampsx10 = new int[4];
				for(i = 0; i < 4; i++) {
					ampsx10[i] = getBits(35-(9*i), 27-(9*i));
				}
				change = handlePerOutletCurrentMessage(tag, change, getBit(41), getBits(40,39), 
					getBits(38,36), ampsx10);
				mtype = "Per-Outlet-Current:" + getBit(41) + ":" + getBits(40, 39) + ":" + getBits(38, 36);
			}
			/* If 44-41 = 0011, feed line current */
			else if(getBits(44,41) == 0x03) {
				int[] ampsx10 = new int[4];
				for(i = 0; i < 4; i++) {
					ampsx10[i] = getBits(39-(10*i), 30-(10*i));
				}
				change = handleFeedLineCurrentMessage(tag, change, getBit(40), ampsx10);
				mtype = "Feed-Line-Current:" + getBit(40);
			}
			else {
				mtype = "unknown";
			}			
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), accum, -1, false, mtype);

			return change;
		}
		/* Handle alarm message */
		public boolean handleAlarm(MantisIITag tag, boolean trip, int alarmidx, boolean change, boolean verify) {
			int m_s;
//			System.out.println("Alarm event! idx=" + alarmidx + ", tripped=" + trip);
			String mtype = null;
			/* If breaker/fuse? */
			if((alarmidx >= 0) && (alarmidx < 6)) {
				m_s = (alarmidx / 3);
				int phid = (alarmidx % 3);
				mtype = "breaker-alarm:" + m_s + ":" + phid + ":" + trip;
				if((towers != null) && (towers.length > m_s) && (towers[m_s] != null)) {
					if((towers[m_s].brktags != null) && (towers[m_s].brktags.length > phid) &&
						(towers[m_s].brktags[phid] != null)) {
						if(towers[m_s].brktags[phid].last_tripped == trip) {	/* Same as before? */
							towers[m_s].brktags[phid].pending_upd = false;	/* Nothing pending */
						}
						else {
							/* If unverified, or already pending, do update */
							if((!verify) || towers[m_s].brktags[phid].pending_upd) {
								towers[m_s].brktags[phid].last_tripped = trip;		/* Update value */
								towers[m_s].brktags[phid].was_signalled = true;		/* Mark as updated */
								PduBreakerTagSubType.updateTag(towers[m_s].brktags[phid].tag, null, 
									Boolean.valueOf(trip));
								towers[m_s].brktags[phid].pending_upd = false;	/* Nothing pending */					
								change = true;
							}
							else {	/* Else, verifying and this is first request */
								towers[m_s].brktags[phid].pending_upd = true;
							}
						}
					}
				}
			}
			/* Else, if feed line overload */
			else if((alarmidx >= 6) && (alarmidx < 12)) {
				m_s = (alarmidx-6)/3;
				int feedidx = (alarmidx-6)%3;
				mtype = "feedline-alarm:" + m_s + ":" + feedidx + ":" + trip;
				if((towers != null) && (towers.length > m_s) && (towers[m_s] != null)) {
					/* If valid tower and feed line */
					if((towers[m_s].feedtags != null) && (towers[m_s].feedtags.length > feedidx) &&
						(towers[m_s].feedtags[feedidx] != null)) {
						if(towers[m_s].feedtags[feedidx].overload == trip) {	/* Same as before? */
							towers[m_s].feedtags[feedidx].pending_upd = false;	/* Nothing pending */
						}
						else {
							/* If unverified, or already pending, do update */
							if((!verify) || towers[m_s].feedtags[feedidx].pending_upd) {
								towers[m_s].feedtags[feedidx].overload = trip;		/* Update value */
								towers[m_s].feedtags[feedidx].was_signalled = true;		/* Mark as updated */
								Boolean loadwarn = null;
								if(trip) {	/* If true, force warning too */
									loadwarn = Boolean.valueOf(trip);
								}
								PduFeedLineTagSubType.updateTag(towers[m_s].feedtags[feedidx].tag, 
									null, null, null, Boolean.valueOf(trip), loadwarn, null);
								towers[m_s].feedtags[feedidx].pending_upd = false;	/* Nothing pending */					
								change = true;
							}
							else {	/* Else, verifying and this is first request */
								towers[m_s].feedtags[feedidx].pending_upd = true;
							}
						}

					}
				}
			}
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), null, alarmidx, trip, mtype);

			return change;
		}

		/* Handle outlet state change */
		public boolean handleOutletState(MantisIITag tag, int m_s, int chg, int phid, int outid, boolean verify,
			MantisIITagType04M tt, boolean nested) {
			boolean change = false;

			if((towers != null) && (towers.length > m_s) && (towers[m_s] != null)) {
				int oidx = phid * towers[m_s].outlets_per_phase + outid;
//				System.out.println(tag.getTagGUID() + ": outletstate: tow[" + m_s + "].switched_outlets=" + towers[m_s].switched_outlets);
				/* Valid phase and outlet */
				if(towers[m_s].switched_outlets && (towers[m_s].phasetags != null) &&
					(towers[m_s].outtags != null) &&
					(phid < towers[m_s].phasetags.length) &&
					(oidx < towers[m_s].outtags.length)) {
					/* If new state is same, do nothing */
					if(towers[m_s].outtags[oidx].switch_state == chg) {
						towers[m_s].outtags[oidx].sw_pending_upd = false;	/* Nothing pending */
					}
					else {
						/* If unverified, or already pending, do update */
						if((!verify) || towers[m_s].outtags[oidx].sw_pending_upd) {
							towers[m_s].outtags[oidx].switch_state = chg;		/* Update value */
							towers[m_s].outtags[oidx].sw_signalled = true;		/* Mark as updated */
							PduOutletTagSubType.updateTag(towers[m_s].outtags[oidx].tag, 
								null, null, null, null, null, null, null, null, null,
								null, null, null, Boolean.valueOf(chg == SWITCH_ON));
							towers[m_s].outtags[oidx].sw_pending_upd = false;	/* Nothing pending */					
							change = true;
							/* If reboot, add to reset queue */
							if(chg == SWITCH_REBOOT) {
								if(rebooted_outlets == null) {
									rebooted_outlets = new LinkedList<OurOutletState>();
								}
								rebooted_outlets.add(towers[m_s].outtags[oidx]);
								tt.enqueueRebootDelay(tag);
							}
						}
						else {	/* Else, verifying and this is first request */
							towers[m_s].outtags[oidx].sw_pending_upd = true;
						}
					}
				}
				/* Special case, all outlets */
				else if(towers[m_s].switched_outlets && (phid == 3) && (outid == 31) &&
					(towers[m_s].outtags != null)) {
					for(int i = 0; i < towers[m_s].outtags.length; i++) {
						change = handleOutletState(tag, m_s, chg, i / towers[m_s].outlets_per_phase,
							i % towers[m_s].outlets_per_phase, verify, tt, change);
					}
				}
			}

			/* Log message, if needed */
			if(!nested)
				RangerServer.logPDUTagMessage(tag.getTagGUID(), null, (m_s*1000)+(phid*100)+outid, 
					(chg==SWITCH_ON), "outlet-switch-"+ m_s + "-" + phid + "-" + outid + "-" + chg);

			return change;
		}
	}
    /**
     * 
     * Constructor
     */
    public MantisIITagType04M() {
        super(4, 'M', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04M");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting payload 
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(tag);
			tag.setTagState(ts);
			change = true;
		}
        /* If payload is flags payload */
        if((cur_payload >= MIN_FLAGS_PAYLOAD) && (cur_payload <= MAX_FLAGS_PAYLOAD)) {
            /* Set low battery flag - verify on set*/
			boolean v = ((cur_payload & FLAGS_LOWBATT) != 0);

            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Remember that we saw low battery during hour - 7 vs 6 beacons per message */
			if((cur_payload & FLAGS_LOWBATT) != 0)
				ts.did_low_batt = true;
            /* Set disconnect flag - verify on set */
			v = ((cur_payload & FLAGS_PDUDISCONNECT) != 0);
			boolean dchg = updateBooleanVerify(tag, PDUDISCONNECT_ATTRIB_INDEX, v, verify && v);
            change |= dchg;
			/* If disconnected, reset the device configuration */
			if(v && dchg)
				change = ts.resetDeviceConfiguration(tag, change);
			/* Check for PDU tower disconnect (master or linked) */
			v = ((cur_payload & (FLAGS_LINKEDPDUDISCONNECT | FLAGS_MASTERPDUDISCONNECT)) != 0);
			change = updateBooleanVerify(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, v, verify && v) || change;
			ArrayList<Long> distow = new ArrayList<Long>();
			if((cur_payload & FLAGS_MASTERPDUDISCONNECT) != 0) {	/* Master disconnect? */
				distow.add(Long.valueOf(1));
			}
			if((cur_payload & FLAGS_LINKEDPDUDISCONNECT) != 0) {	/* Linked disconnect? */
				distow.add(Long.valueOf(2));
			}
			change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX, 
				distow, verify) || change;
        }
        /* If start of message */
        else if((cur_payload >= MIN_START_PAYLOAD) && (cur_payload <= MAX_START_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0x7F), cur_timestamp, true, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If continuation of payload */
		else if((cur_payload >= MIN_CONT_PAYLOAD) && (cur_payload <= MAX_CONT_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0xFF), cur_timestamp, false, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
			/* If we're still defaulted on tower disconnect, assume we're good after 3 messages */
			if(tag.isDefaultFlagged(PDUTOWERDISCONNECT_ATTRIB_INDEX) && (ts.msg_cnt > 2)) {
				change = updateBooleanVerify(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, false, false) || change;
				change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX,
					new ArrayList<Long>(), false) || change;
			}
		}
		/* If alarm payload */
		else if((cur_payload >= MIN_ALARM_PAYLOAD) && (cur_payload <= MAX_ALARM_PAYLOAD)) {
			change = ts.handleAlarm(tag, (cur_payload & 0x10) != 0, cur_payload & 0x0F, change, verify) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If we're on second outlet payload, and first one was good */
		else if((cur_payload >= MIN_OUTSTAT_PAYLOAD2) && (cur_payload <= MAX_OUTSTAT_PAYLOAD2) &&
			(payloads[0] >= MIN_OUTSTAT_PAYLOAD1) && (payloads[0] <= MAX_OUTSTAT_PAYLOAD1) &&
			((cur_timestamp - payloadage[0]) < MAX_OUTSTAT_PERIOD)) {
			change = ts.handleOutletState(tag, (cur_payload & 0x10) >> 4,
				(cur_payload & 0x0C) >> 2, cur_payload & 0x03, payloads[0] & 0x1F, verify, 
				this, false) || change;
		}

        return change;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return SUBTYPES;
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
        MantisIITag tag = (MantisIITag)t;

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null)
			return;
		if(ts.rebooted_outlets != null) {
			OurOutletState s;
			while((s = ts.rebooted_outlets.poll()) != null) {
				if(s.switch_state == SWITCH_REBOOT) {	/* Still reboot */
					s.switch_state = SWITCH_ON;
					PduOutletTagSubType.updateTag(s.tag, 
						null, null, null, null, null, null, null, null, null,
						null, null, null, Boolean.valueOf(true));
				}
			}
			ts.rebooted_outlets = null;
		}
    }
	private void enqueueRebootDelay(MantisIITag tag) {
		enqueueTagForDelay(tag, REBOOT_DELAY);
	}
}
