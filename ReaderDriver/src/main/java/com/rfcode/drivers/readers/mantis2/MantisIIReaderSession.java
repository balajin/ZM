package com.rfcode.drivers.readers.mantis2;

import java.net.InetSocketAddress;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import com.rfcode.drivers.*;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.RangerServer;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;

/**
 * Implements a basic asynchronous communications session with a Mantis-class
 * reader.
 * 
 * @author Mike Primm
 */
public class MantisIIReaderSession {
    private static final int READ_MESSAGE_TIMEOUT = 10000;
    private static final int RECONNECT_RETRY_PERIOD = 10000;
    private static final int DIAG_MESSAGE_PERIOD = 30000;
    /* Our session factory */
    private SessionFactory our_factory;
    /* Our reader ID */
    private String readerid;
    /* Our session */
    private Session sess;
    /* Our session state */
    private SessionStates state;
    private StringBuilder sb = new StringBuilder();
    private boolean got_msg = false;
    private SessionTimeoutAction sta = new SessionTimeoutAction();
    private ReconnectAction ra = null;
    /* Our command queue */
    private ConcurrentLinkedQueue<CommandRequest> cmd_queue;
    /* Currently active command */
    private CommandRequest cur_cmd;
    /* Our tag message listeners */
    private ArrayList<MantisIIReaderTagMessageListener> tag_listeners;
    /* Our session handler */
    private MantisIIReaderSessionHandler our_handler;
    /* Our target IP address and port number */
    private InetSocketAddress our_ip;
    /* Is active? */
    private boolean is_active;
    /* Is upconnect? */
    private boolean is_upconnect;
    /* Is ready for commands */
    private boolean is_ready;
	/* Is ping/diag suppressed? */
	private boolean ping_suppress;
    /**
     * Session states
     */
    public static enum SessionStates {
        Disconnected, Connecting, Connected, Disconnecting, Reconnecting
    };

    /* Reader command request */
    private static class CommandRequest implements Runnable {
        private String cmdstr;
        private MantisIIReaderCommandResponseListener rsplistener;
        private ArrayList<String> rsp;
        private MantisIIReaderSession rsess;
        private static final long CMD_TIMEOUT = 5000;
		private static final int BIG_CMD_LEN = 256;
        private static final long BIG_CMD_TIMEOUT_SCALE = 500;	/* Add 500 ms per 256 bytes of length */
        private boolean m_retry;
        /**
         * Constructor for command request. The command is issued, and followed
         * by an inert command ("M\r"), and all lines returned up to the
         * response of the "M\r" command are treated as the response to the
         * command.
         */
        public CommandRequest(MantisIIReaderSession rs,
            MantisIIReaderCommandResponseListener listen, String cmd) {
            rsess = rs;
            rsplistener = listen;
            /* If command is NOT "M\r" command, save it */
            if ((cmd != null) && (!cmd.equals("M")) && (!cmd.equals("m")))
                cmdstr = cmd;
            else
                cmdstr = null;
            rsp = new ArrayList<String>();
        }
        /**
         * Start the command
         */
        public void startCommand() {
			boolean is_fw = false;
            boolean is_starttls = false;
			long timeout = CMD_TIMEOUT;
            if (cmdstr != null) {
				int cmdstrlen = cmdstr.length();
                /* If not blank, send command with "\r" */
                if (cmdstrlen > 0) {
                    sendString(cmdstr + "\r"); /* Send the command */
					if(cmdstr.startsWith(":fw,"))
						is_fw = true;
                    if(cmdstr.startsWith("STARTTLS"))
                        is_starttls = true;
					timeout += (cmdstrlen / BIG_CMD_LEN) * BIG_CMD_TIMEOUT_SCALE;
                }
            }
			if((!is_fw) && (!is_starttls))
	            sendString("M\r"); /* Send "M\r" to drive response */
			/* Set command complete timeout */
			RangerProcessor.addDelayedAction(this, timeout);
        }
        /**
         * Handle command response
         */
        public void handleResponseString(String rspstr) {
            if (!rsess.is_active)
                return;
            /* If M response (M get security failures too - should get two) */
            if (rspstr.startsWith("=,M,") || rspstr.startsWith("=,fw") || rspstr.startsWith("=,STARTTLS")
                || (rspstr.startsWith("E,8") && (rsp.size() > 0))
				|| (rspstr.startsWith("E,7") && (rsp.size() > 0))) {
                if ((cmdstr == null) || cmdstr.startsWith(":fw,")) {
                    rsp.add(rspstr);
                }
                else if(cmdstr.startsWith("STARTTLS")) {
                    rsp.add(rspstr);
                    if(rspstr.startsWith("=,STARTTLS,READY")) {
                        try {
                            SSLContext ctx = RangerServer.getSSLContext();
                            if(rsess.sess instanceof TCPIPSession)
                                ((TCPIPSession)rsess.sess).startTLS(ctx, true, null);
                        } catch (SessionException sx) {
                            System.err.println("Error starting TLS");
                            rsess.closeSession();
                        }
                        return;
                    }
                }
                /* Remote timeout */
                RangerProcessor.removeDelayedAction(this);
                /* Complete and start next one */
                completeAndStartNext(true);
            } else { /* Otherwise, add to response */
                rsp.add(rspstr);
            }
        }
        /**
         * Timeout handler
         */
        public void run() {
            if (!rsess.is_active)
                return;
            /* We're not current - must be cancelled command */
            if(this != rsess.cur_cmd) {
                //System.out.println("Timeout on cancelled command : " + cmdstr);
                return;
            }
            if ((m_retry == false) && (cmdstr.length() < BIG_CMD_LEN)) { /* No retry on M yet, or on big command */
                sendString("M\r"); /* Send "M\r" to drive response */
                m_retry = true;
				/* Set command complete timeout */
				RangerProcessor.addDelayedAction(this, CMD_TIMEOUT);
            } else {
                completeAndStartNext(!rsp.isEmpty()); /*
                                                         * Complete - error if
                                                         * no rsp
                                                         */
            }
        }
        /**
         * Complete command and start next one
         */
        private void completeAndStartNext(boolean completed) {
            /* Now, call our callback */
            if (rsplistener != null) {
                rsplistener.commandResponseCallback(rsess, rsp, completed);
                rsplistener = null;
            }
            /* We're done - advance to next command in queue */
            rsess.cur_cmd = rsess.cmd_queue.poll();
            if (rsess.cur_cmd != null) { /* If we have another, start it now */
                rsess.cur_cmd.startCommand();
            }
            else {  /* Empty command queue?  Do pseudotags */
                rsess.handlePseudoTags();
            }
        }
        /**
         * Send string to the reader's session
         * 
         * @param st -
         *            string to send
         */
        private void sendString(String st) {
            byte[] b = st.getBytes();
            try {
                rsess.sess.requestDataWrite(b, 0, b.length, null);
            } catch (SessionException sx) {
                rsess.our_factory.getLogger().error(rsess.readerid + ": " + sx);
            }
        }
    }
    /**
     * Close and clean session
     */
    public void closeSession() {
        /* Notify listeners of close */
        for (MantisIIReaderTagMessageListener t : tag_listeners) {
            t.processReaderTagMessage(this, null);
        }
        tag_listeners.clear(); /* And clear the list */

        /* If pending command, complete them */
        if (cur_cmd != null) {
            cur_cmd.rsplistener.commandResponseCallback(this, cur_cmd.rsp,
                false);
            cur_cmd = null;
        }
        for (CommandRequest r : cmd_queue) {
            r.rsplistener.commandResponseCallback(this, r.rsp, false);
        }
        cmd_queue.clear();
        /* If session open, close it down */
        if (sess != null) {
            Session sess2 = sess;
            sess = null;
            /* Request disconnect */
            try {
                state = SessionStates.Disconnecting;
                sess2.requestSessionDisconnect();
            } catch (SessionException sx) {
                our_factory.getLogger().error(readerid + ": " + sx);
	            handleDisconnectCompleted(sess2);
            }
        }
        if (sta != null) {
            RangerProcessor.removeDelayedAction(sta);
            sta = null;
        }
        if (ra != null) {
            RangerProcessor.removeDelayedAction(ra);
            ra = null;
        }
    }

    /**
     * Our internal session handler - handles raw communications events
     */
    private class OurSessionHandler implements SessionHandler {
        /**
         * Connection completed callback
         * 
         * @param s -
         *            our session
         * @param is_connected -
         *            true if successful connect, false if not
         */
        public void sessionConnectCompleted(final Session s, final boolean is_connected) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            handleConnectCompleted(s, is_connected);
				}
            });
        }
        /**
         * Session disconnected callback
         * 
         * @param s -
         *            our session
         */
        public void sessionDisconnectCompleted(final Session s) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            handleDisconnectCompleted(s);
				}
			});
        }
        /**
         * Session data read callbac
         * 
         * @param sdr -
         *            session data read container
         */
        public void sessionDataRead(final SessionRead sdr) {
			final byte[] b = sdr.readBytes();
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            handleDataRead(b);
				}
			});
        }
        /**
         * Session about to attempt connect - used to notify when the session is
         * about to attempt a connection.
         * 
         * @param s -
         *            session
         */
        public void sessionStartingConnect(final Session s) {
			RangerProcessor.addImmediateAction(new Runnable() {
				public void run() {
		            handleStartingConnect(s);
				}
			});
        }
    }
    /**
     * Connection completed callback
     * 
     * @param s -
     *            our session
     * @param is_connected -
     *            true if successful connect, false if not
     */
    private void handleConnectCompleted(Session s, boolean is_connected) {
        /* Update state, based on connection result */
        if (is_connected) {
            state = SessionStates.Connected;
        } else {
            state = SessionStates.Disconnected;
        }
        /* Call our handler */
        if (our_handler != null)
            our_handler.readerSessionConnectCompleted(this, is_connected);

        if (is_connected == false) { /* If connect failed */
            ra = new ReconnectAction();
            /* Retry connection in 5 seconds */
            RangerProcessor.addDelayedAction(ra, RECONNECT_RETRY_PERIOD);
        } else { /* Else, successful connection */
			/* And set timeout for session activity */
			got_msg = false;
			RangerProcessor.addDelayedAction(sta, READ_MESSAGE_TIMEOUT);
        }
    }

    /**
     * Session disconnected callback
     * 
     * @param s -
     *            our session
     */
    private void handleDisconnectCompleted(Session s) {
        state = SessionStates.Disconnected;
        /* Reset any pending or queued commands */
        cur_cmd = null;
        cmd_queue.clear();
        /* Clean up reconnect action - we'll add it again if needed */
        if (ra != null) {
            RangerProcessor.removeDelayedAction(ra);
            ra = null;
        }
        if (sta != null) {
            RangerProcessor.removeDelayedAction(sta);
        }
        /* Call our handler */
        if (our_handler != null)
            our_handler.readerSessionDisconnectCompleted(this);
        /* If we're closed on purpose, or upconnect, nothing to do */
        if ((!is_active) || (is_upconnect)) {
            return;
        }
        ra = new ReconnectAction();
        /* Retry connection in 5 seconds */
        RangerProcessor.addDelayedAction(ra, RECONNECT_RETRY_PERIOD);
    }

    /**
     * Session data read callback
     * 
     * @param sdr -
     *            session data read container
     */
    private void handleDataRead(byte[] b) {
        for (int i = 0; i < b.length; i++) { /* Loop through them */
            char c = (char) b[i]; /* We're ASCII, so we can do this */
            if ((c == '\r') || (c == '\n')) { /* End of line? */
                if (sb.length() > 0) {
                    char firstchar = sb.charAt(0);
                    boolean istagmsg = (firstchar == 'H') || (firstchar == 'M')
                        || (firstchar == 'L') || 
                        ((firstchar == '=') && (sb.indexOf("=,sdata,") == 0));
                    /* If its a command response */
                    if (!istagmsg) {
						boolean is_gps = (firstchar == 'P');
						if(is_gps) {
							broadcastGPSMsg(sb);
						}
                        else {
							if (cur_cmd != null) { /* Active command? */
                            	/* Handle it */
	                            cur_cmd.handleResponseString(sb.toString());
    	                    }
    	                    got_msg = true; /* We got command message */
						}
                    } else { /* Else, tag message */
                        broadcastTagMsg(sb);
                    }
                    sb.setLength(0); /* Clear it */
                }
            } else {
                sb.append(c);
            }
        }
    }
    /**
     * Session about to attempt connect - used to notify when the session is
     * about to attempt a connection.
     * 
     * @param s -
     *            session
     */
    private void handleStartingConnect(Session s) {
        /* Update state */
        state = SessionStates.Connecting;
        /* Call our handler */
        if (our_handler != null)
            our_handler.readerSessionStartingConnect(this);
    }
    /**
     * Broadcast message to our tag listeners
     * 
     * @param msg -
     *            message to send
     */
    private void broadcastTagMsg(StringBuilder msg) {
        int size = tag_listeners.size();
        for (int i = 0; i < size; i++) {
            MantisIIReaderTagMessageListener tl = tag_listeners.get(i);
            tl.processReaderTagMessage(this, msg);
        }
    }
    /**
     * Broadcast GPS message to our tag listeners
     * 
     * @param msg -
     *            message to send
     */
    private void broadcastGPSMsg(StringBuilder msg) {
        for (MantisIIReaderTagMessageListener tl : tag_listeners) {
            tl.processReaderGPSMessage(this, msg);
        }
    }
    /**
     * Enqueue command string to reader
     * 
     * @param cmd -
     *            command string to be sent
     * @param rsp -
     *            response listener to receive results
     */
    public void enqueueCommand(String cmd,
        MantisIIReaderCommandResponseListener rsp) {
        if(state != SessionStates.Connected) {
            System.out.println("enqueueCommand while not connected : " + cmd);
            if (rsp != null)
                rsp.commandResponseCallback(this, new ArrayList<String>(),
                    false);
            return;
        }
        CommandRequest cr = new CommandRequest(MantisIIReaderSession.this, rsp,
            cmd);
        if (cur_cmd == null) { /* If nothing active, start now */
            cur_cmd = cr;
            cur_cmd.startCommand();
        } else { /* Else, add to queue */
            cmd_queue.add(cr);
        }
    }
    /**
     * Simple action for initiating a reconnect attempt
     */
    private class ReconnectAction implements Runnable {
        /**
         * Method for implementing reconnect attempt
         */
        public void run() {
            ra = null;
            /* If session is closed, cancel action */
            if ((!is_active) || (is_upconnect))
                return;
            /* Request connect */
            try {
                state = SessionStates.Reconnecting;
                sess.requestSessionConnect();
            } catch (SessionException sx) {
                our_factory.getLogger().error(readerid + ": " + sx);
	            handleConnectCompleted(sess, false);
            }
        }
    }
    /**
     * Action for processing reader session timeouts - used to detect broken
     * session.
     */
    private class SessionTimeoutAction implements Runnable,
        MantisIIReaderCommandResponseListener {
        private boolean sent_ping;
        private int diag_timer = 0;
        private int ping_to_cnt = 0;
        private static final int PING_TO_CNT_MAX = 3;
        /**
         * Method for checking for session timeouts
         */
        public void run() {
            if (!is_active) { /* If we closed, exit quietly */
                return;
            }
            if(!is_ready) { /* If not ready, requeue */
                RangerProcessor.addDelayedAction(this, READ_MESSAGE_TIMEOUT);
                return;
            }
            if(diag_timer >= DIAG_MESSAGE_PERIOD) {
                diag_timer -= DIAG_MESSAGE_PERIOD;
				if(!ping_suppress) {
	                enqueueCommand("DD", this); /* Send ping command */
    	            /* If we have pseudotags, inject message once each diag request */
    	            handlePseudoTags();
				}
            }
            if (got_msg) { /* If we got one since last run */
                got_msg = false; /* Clear flag */
                sent_ping = false;
                /* And reenqueue it for 10 seconds from now */
                RangerProcessor.addDelayedAction(this, READ_MESSAGE_TIMEOUT);
                diag_timer += READ_MESSAGE_TIMEOUT;
            } else if (!sent_ping) { /* No ping yet? */
				if(!ping_suppress) {
	                enqueueCommand("", null); /* Send ping command */
    	            sent_ping = true;
				}
   	            ping_to_cnt = 0;
                /* And reenqueue it for 10 seconds from now */
	            RangerProcessor.addDelayedAction(this, READ_MESSAGE_TIMEOUT);
                diag_timer += READ_MESSAGE_TIMEOUT;
            } else if(ping_to_cnt < PING_TO_CNT_MAX) {
                ping_to_cnt++;
                /* And reenqueue it for 10 seconds from now */
                RangerProcessor.addDelayedAction(this, READ_MESSAGE_TIMEOUT);
                diag_timer += READ_MESSAGE_TIMEOUT;
            } else { /* Else, timeout again after ping */
                /* Request disconnect */
                try {
                    sess.requestSessionDisconnect();
                    diag_timer = 0;
                } catch (SessionException sx) {
                    our_factory.getLogger().error(readerid + ": " + sx);
		            handleDisconnectCompleted(sess);	/* Drive through disconnect complete */
                }
                got_msg = sent_ping = false;
                ping_to_cnt = 0;
            }
        }
        /**
         * Callback method delivering command response strings - for D message
         */
        public void commandResponseCallback(MantisIIReaderSession sess,
            List<String> rsp, boolean completed) {
            if(!completed)
                return;
            if(rsp.size() < 1)
                return;
            String[] vs = rsp.get(0).split(","); /* Split it */                
            int[] mps = null;
            int tagdbpct = -1;

            if((vs.length >= 5) && vs[0].startsWith("DD")) {
                mps = new int[2];
                try {
                    mps[0] = Integer.parseInt(vs[3]);
                    mps[1] = Integer.parseInt(vs[4]);
                } catch (NumberFormatException nfx) {
                    mps = null;
                }
            }

            if((vs.length >= 7) && vs[0].startsWith("DD")) {
                try {
                    int curnum = Integer.parseInt(vs[5]);
                    int maxnum = Integer.parseInt(vs[6]);
                    tagdbpct = (int)Math.floor(100.0 * (double)curnum / (double)maxnum);
                } catch (NumberFormatException nfx) {
                    tagdbpct = -1;
                }
            }
            if((vs.length >= 3) && (vs[0].startsWith("D"))) {
                try {
					int noise[] = new int[2];                
                    noise[0] = -Integer.parseInt(vs[1]);
                    noise[1] = -Integer.parseInt(vs[2]);
                    if (our_handler != null) {
                        our_handler.readerSesssionChannelDiag(sess, noise, mps, tagdbpct);
					}
                } catch (NumberFormatException nfx) {
                }
            }
        }
    }
    /**
     * Constructor for reader session
     */
    public MantisIIReaderSession() {
        cmd_queue = new ConcurrentLinkedQueue<CommandRequest>();
        state = SessionStates.Disconnected;
        tag_listeners = new ArrayList<MantisIIReaderTagMessageListener>();
        our_factory = null;
		ping_suppress = false;
    }
    /**
     * Set our session factory
     * 
     * @param sf -
     *            session factory
     */
    public void setSessionFactory(SessionFactory sf) {
        our_factory = sf;
    }
    /**
     * Get our session factory
     * 
     * @return session factory
     */
    public SessionFactory getSessionFactory() {
        return our_factory;
    }
    /**
     * Set the TCP/IP address and port for the session to connect to (if TCP/IP
     * session)
     * 
     * @param ip -
     *            address
     */
    public void setIPAddress(InetSocketAddress ip) {
        our_ip = ip;
        /* Set IP address for the session */
        if ((sess != null) && (sess instanceof TCPIPSession))
            ((TCPIPSession) sess).setIPAddress(our_ip);
    }
    /**
     * Get the TCP/IP address and port for the session to connect to (if TCP/IP
     * session)
     * 
     * @return ip address and port
     */
    public InetSocketAddress getIPAddress() {
        return our_ip;
    }
	/**
	 * Get address sessions is connected to (remote peer)
	 *
	 * @return ip address and port of peer, or null if not connected
	 */
	public InetAddress getRemoteAddress() {
		if((sess != null) && (sess instanceof TCPIPSession))
			return ((TCPIPSession)sess).getRemoteAddress();
		return null;
	}
    /**
     * Set reader ID (required - must be unique)
     * 
     * @param id -
     *            reader ID
     */
    public void setReaderID(String id) {
        readerid = id;
    }
    /**
     * Get reader ID
     * 
     * @return reader ID
     */
    public String getReaderID() {
        return readerid;
    }
    /**
     * Set ready for commands (called by reader)
     */
    void setToReady(boolean rdy) {
        is_ready = rdy;
    }
    /**
     * Initialize method - must be called once all the attributes are set
     */
    public void init() throws BadParameterException, SessionException {
        if ((our_ip == null) && (!is_upconnect))
            throw new BadParameterException("Missing IP address");
        if (our_factory == null)
            throw new BadParameterException("Missing Session Factory");
        if (readerid == null)
            throw new BadParameterException("Missing Reader ID");
        /* Initialize our handler */
        SessionHandler our_hand = new OurSessionHandler();
        /* Create our session */
        try {
            TCPIPSession tcpsess = our_factory.createTCPIPSession();
            if(!is_upconnect) { /* If not upconnect, set IP and handler */
                tcpsess.setIPAddress(our_ip); /* Set IP address for the session */
                tcpsess.setSessionHandler(our_hand); /* Set our session handler */
            }
            sess = tcpsess;
        } catch (SessionException sx) {
            throw sx;
        }
    }
    /**
     * Cleanup method
     */
    public void cleanup() {
        CommandRequest r;
        /* Mark as inactive */
        is_active = false;
        /* Clean up current command */
        if (cur_cmd != null) {
            if (cur_cmd.rsplistener != null)
                cur_cmd.rsplistener.commandResponseCallback(this, cur_cmd.rsp,
                    false);
        }
        /* Clean up pending commands */
        while ((r = cmd_queue.poll()) != null) {
            if (r.rsplistener != null)
                r.rsplistener.commandResponseCallback(this, r.rsp, false);
        }
        /* Clean up session */
        if (sess != null) {
            sess.setSessionHandler(null);
            try {
                sess.requestSessionDisconnect();
            } catch (SessionException sx) {
	            handleDisconnectCompleted(sess);
            }
            sess = null;
        }
        tag_listeners.clear();
        our_handler = null;
    }
    /**
     * Add tag listener
     * 
     * @param listen -
     *            listener
     */
    public void addTagListener(MantisIIReaderTagMessageListener listen) {
        tag_listeners.add(listen);
    }
    /**
     * Remove tag listener
     * 
     * @param listen -
     *            listener
     */
    public void removeTagListener(MantisIIReaderTagMessageListener listen) {
        tag_listeners.remove(listen);
    }
    /**
     * Set the reader to active or inactive
     * 
     * @param active -
     *            if true, activate reader. If false, deactivate it
     */
    public void setActive(boolean active) throws SessionException {
        if (is_active == active) { /* If no change, quit */
            return;
        }
        if (active) { /* If making active */
            if(!is_upconnect)   /* If not up-connect */
                sess.requestSessionConnect(); /* Request connection */
        } else { /* Else, request disconnect */
            sess.requestSessionDisconnect();
        }
        is_active = active; /* Update setting */
    }
    /**
     * Get active/inactive setting
     * 
     * @return true if active, false if inactive
     */
    public boolean getActive() {
        return is_active;
    }

    /**
     * Get encryption status
     * 
     * @return true if encrypted, false if not
     */
    public boolean getEncrypted() {
        if((sess != null) && sess.isValid() && (sess instanceof TCPIPSession))
            return ((TCPIPSession)sess).getTLSStarted();
        return false;
    }
    /**
     * Set up-connection state
     * @param upconn - true if up connect enabled, false if not
     */
    public void setUpConnection(boolean upconn) {
        if(is_upconnect != upconn) {
            is_upconnect = upconn;
            if (is_active && (!upconn)) { /* if active and no longer upconnect */
                try {
                    sess.requestSessionConnect(); /* Request connection */
                } catch (SessionException sx) {
					handleConnectCompleted(sess, false);
                }
            }
        }
    }
    /**
     * Get up connection state
     * @return true if enabled, false if not
     */
    public boolean getUpConnection() {
        return is_upconnect;
    }
    /**
     * Up connection session connected - passes in established session
     * @param s - session
     */
    void upConnectionEstablished(Session s) {
        if(is_upconnect) {
            /* Clean up session, if needed */
            if (sess != null) {
                sess.setSessionHandler(null);
                try {
                    sess.requestSessionDisconnect();
                } catch (SessionException sx) {
		            handleDisconnectCompleted(sess);
                }
                sess = null;
            }
            sess = s;
            /* set up handler for new one */
            sess.setSessionHandler(new OurSessionHandler());
            /* Call connection starting */
            handleStartingConnect(sess);
            /* Call connection complete handler */
            handleConnectCompleted(sess, true);
        }
    }    
    /**
     * Get state
     * 
     * @return state
     */
    public SessionStates getState() {
        return state;
    }
    /**
     * Set our session handler
     * 
     * @param sh -
     *            session handler
     */
    public void setReaderSessionHandler(MantisIIReaderSessionHandler sh) {
        our_handler = sh;
    }
    /**
     * Get our session handler
     * 
     * @return session handler
     */
    public MantisIIReaderSessionHandler getReaderSessionHandler() {
        return our_handler;
    }
    /**
     * Get our error logger
     * 
     * @return logger
     */
    public Logger getLogger() {
        return our_factory.getLogger();
    }
    /**
     * Force disconnect - request disconnect without disabling
     */
    public void forceDisconnect() throws SessionException {
        if (is_active
            && ((state == SessionStates.Connecting) || (state == SessionStates.Connected))) {
            sess.requestSessionDisconnect(); /* Request connection */
        }
    }
    /**
     * Handle pseudo-tags
     */
    private void handlePseudoTags() {
        String pseudogc = MantisIITagGroup.getPseudoTagGroupCode();
        if(pseudogc != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("H,00000000,G").append(pseudogc).append(",P000,A40,B40");
            broadcastTagMsg(sb);
        }
    }
	/**
	 * Set diag/ping supporess (used during upgrades)
	 */
	void setDiagPingSuppress(boolean do_suppress) {
		ping_suppress = do_suppress;
	}
}
