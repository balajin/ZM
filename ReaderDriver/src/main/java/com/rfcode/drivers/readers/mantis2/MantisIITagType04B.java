package com.rfcode.drivers.readers.mantis2;

/**
 * Tag type for treatment 04B tags - Mantis II Tags - Treatment 04B
 * 
 * @author Mike Primm
 */
public class MantisIITagType04B extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04B";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {MantisIITagType.USERPAYLOAD_ATTRIB};
    private static final String[] TAGATTRIBLABS = {MantisIITagType.USERPAYLOAD_ATTRIB_LABEL};
    private static final int USERPAYLOAD_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {Integer.valueOf(0)};

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 20000;
    /**
     * Constructor
     */
    public MantisIITagType04B() {
        super(4, 'B', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04B");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag) {
        boolean change = false;
        /* If second half of two part payload (low part) */
        if (cur_payload < 0400) {
            /* If previous was high value, and not too old */
            if ((payloads[0] >= 0400)
                && ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                /* We've got matching set : build value */
                change = updateInt(tag, USERPAYLOAD_ATTRIB_INDEX,
                    ((payloads[0] & 0xFF) << 8) | (cur_payload & 0xFF))
                    || change;
            }
        }
        return change;
    }
}
