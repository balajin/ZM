package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;
import java.util.Map;

/**
 * Tag type for treatment 04V tags - Mantis II Tags - Treatment 04V (temperature/humidity)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04V extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04V";

	/** Tag type attribute - temperature rounding increment */
	public static final String TAGTYPEATTRIB_TMPROUND = "tmpround";
    /* Default rounding increment */
    public static final double TMPROUND_DEFAULT = 0.1;
	/** Tag type attribute - humidity rounding increment */
	public static final String TAGTYPEATTRIB_HUMROUND = "humround";
    /* Default rounding increment */
    public static final double HUMROUND_DEFAULT = 0.1;
	/** Tag type attribute - no data when low battery */
	public static final String TAGTYPEATTRIB_NODATALOWBATT = "nodatalowbatt";

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.TEMPERATURE_ATTRIB,
        MantisIITagType.HUMIDITY_ATTRIB,
        MantisIITagType.DEWPOINT_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB,
		MSGLOSS_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.TEMPERATURE_ATTRIB_LABEL,
        MantisIITagType.HUMIDITY_ATTRIB_LABEL,
        MantisIITagType.DEWPOINT_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL,
		MSGLOSS_ATTRIB_LABEL
        };
    private static final int TEMPERATURE_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int HUMIDITY_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int DEWPOINT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 3; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
	private static final int MSGLOSS_ATTRIB_INDEX = 4;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, null, null,
        Boolean.FALSE, null};

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 2;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 20000;
    /* Return to normal delay - how long without flags report before auto RTN */
    private static final int RTN_DELAY = 240;    /* 240 seconds longer than longest ageout*/

    private static final double DEWP_A = 17.271;
    private static final double DEWP_B = 237.7;

	private static final double MAX_UNVERIFIED_TMP_DELTA = 2.0;	/* 2C max change without verify */
	private static final double MAX_UNVERIFIED_HUM_DELTA = 2.0;	/* 2% max change without verify */
	private static final double MAX_UNVERIFIED_DEW_DELTA = 2.0;	/* 2C max change without verify */

	private static final int	MSGCOUNT_PERIOD = 60*60*1000;	/* 1 hour */
	private static final int	NOMINAL_BEACON_PERIOD = 10050;	/* 10 seconds + jitter */

    private static final int    MAX_FLAGS_IN_A_ROW = 16;    /* If 16 flags without any data?  Drop readings */
	/**
	 * Tag state - use to accumulate message loss data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		long	start_ts;
		int		msg_count;
		int		flgs_count;
        int     flgs_in_a_row;
        boolean low_batt;
        boolean low_sensor_voltage;
        boolean saturation;

		public OurTagState(long ts) {
			start_ts = ts;
		}
		public void cleanupTag(MantisIITag t) { }
	}

    /**
     * Constructor
     */
    public MantisIITagType04V() {
        super(4, 'V', PREV_PAYLOAD_CNT, new String[] { TAGTYPEATTRIB_TMPROUND, TAGTYPEATTRIB_HUMROUND, TAGTYPEATTRIB_NODATALOWBATT });
        setLabel("RF Code Mantis II Tag - Treatment 04V");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_TMPROUND, Double.valueOf(TMPROUND_DEFAULT));	/* Default to 0.1 */
		defs.put(TAGTYPEATTRIB_HUMROUND, Double.valueOf(HUMROUND_DEFAULT));	/* Default to 0.1 */
		defs.put(TAGTYPEATTRIB_NODATALOWBATT, Boolean.valueOf(false));	/* Default to false */
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean verify = false;
        boolean lowbatt;		
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);
		/* If needed, intialize our tag state */
		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(cur_timestamp);
			tag.setTagState(ts);
		}
    	Map<String,Object> grpattrib = tag.getTagGroup().getTagGroupAttributes();
		
        switch((cur_payload>>7) & 0x03) {
            case 0:  /* First part of humidity payload - nothing yet */
            	ts.flgs_in_a_row = 0;
                break;
            case 1: /* Second part of humitiy payload, start of temp - nothing yet */
            	ts.flgs_in_a_row = 0;
                break;
            case 2: /* second part of temperture payload - process both (since
                      humidity may need temperature correction */
                /* If previous two payloads are proper, and not too old, process */
                if(((payloads[0] >> 7) == 1) && ((payloads[1] >> 7) == 0) &&
                    ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD) &&
                    ((cur_timestamp - payloadage[1]) < (2*MAX_HIGH_LOW_PERIOD))) {
                	
                    /* Get low sensor voltage flag */
                    ts.low_sensor_voltage = ((cur_payload & 0x01) != 0);
                    lowbatt = ts.low_sensor_voltage || ts.low_batt;

                	/* If low battery and we're set to not do attributes on low battery */
                    if (lowbatt && (Boolean.TRUE.equals(grpattrib.get(TAGTYPEATTRIB_NODATALOWBATT)))) {
                        change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX) || change;
                        change = updateNull(tag, HUMIDITY_ATTRIB_INDEX) || change;
                        change = updateNull(tag, DEWPOINT_ATTRIB_INDEX) || change;
                    }
                    else {
                    	/* Compute temperature */
                    	int vt = ((payloads[0] & 0x3F) << 6) + ((cur_payload & 0x7E) >> 1);
                    	double rawtmp = -46.85 + (175.72 * vt) / 4096.0;
                    	/* Apply rounding */
                    	double tmp = rawtmp;
                    	Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_TMPROUND);
                    	double rnd = TMPROUND_DEFAULT;
                    	if(tc instanceof Double) {
                    		rnd = ((Double)tc).doubleValue();
                    	}
                    	if(rnd > 0.0)
                    		tmp = Math.rint(tmp / rnd) * rnd;
                    	/* Compute humidity */
                    	int vh = ((payloads[1] & 0x7F) << 1) + ((payloads[0] & 0x40) >> 6);
                    	double rawhumi = -6.0 + (125.0*vh) / 256.0;
                    	if(rawhumi < 0.0) rawhumi = 0.0;
                    	if(rawhumi > 100.0) rawhumi = 100.0;
                    	/* Apply rounding */
                    	double humi = rawhumi;
                    	Object hr = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_HUMROUND);
                    	double hrnd = HUMROUND_DEFAULT;
                    	if(hr instanceof Double)
                    		hrnd = ((Double)hr).doubleValue();
                    	if(hrnd > 0.0)
                    		humi = Math.rint(humi / hrnd) * hrnd;
                    	if(rawhumi < 0.5) rawhumi = 0.5;    /* Assume 0.5% RH minimum for dew point calculation */
                    	double dew = (DEWP_A * rawtmp / (DEWP_B + rawtmp)) + Math.log(rawhumi/100.0);
                    	dew = DEWP_B * dew / (DEWP_A - dew);

                    	/* Apply rounding to dew point */
                    	if(rnd > 0.0)
                    		dew = Math.rint(dew / rnd) * rnd;
                    	change = updateDoubleVerify(tag, TEMPERATURE_ATTRIB_INDEX, tmp, 
                    			verify, MAX_UNVERIFIED_TMP_DELTA) || change;
                    	/* If above 95%, we're saturated */
                    	if (humi >= 95.0) {
                    		ts.saturation = true;
                    	}
                    	else if (ts.saturation && (humi < 90.0) && (humi > 30.0)) {
                    		ts.saturation = false;
                    	}
                    	if ((!ts.saturation) || (humi >= 90.0)) {	/* Update if not saturated OR legit high value */
                    		change = updateDoubleVerify(tag, HUMIDITY_ATTRIB_INDEX, humi, verify,
                    				MAX_UNVERIFIED_HUM_DELTA)
                    				|| change;
                    		change = updateDoubleVerify(tag, DEWPOINT_ATTRIB_INDEX, dew, verify, 
                    				MAX_UNVERIFIED_DEW_DELTA)
                    				|| change;
                    	}
                    }
                    /* Check to see if anything else is still defaulted - need to handle
                     * RTN timeout in case our flags are actually clear (and we don't get message)
                     */
                    if(tag.isDefaultFlagged(LOWBATT_ATTRIB_INDEX) && (!lowbatt)) {
                        enqueueTagForDelay(tag, 2*RTN_DELAY, true); /* Enqueue timeout */
                    }
                    else {
                        change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX,
                            lowbatt, verify && lowbatt) || change;
                    }
					/* Add to message count */
					ts.msg_count++;
				
                }
                ts.flgs_in_a_row = 0;
                break;
            case 3: /* Flags */
                /* Get low battery value */
				ts.low_batt = ((cur_payload & 0x02) != 0);
				/* Only require verify if setting flag */
                lowbatt = ts.low_batt || ts.low_sensor_voltage;
                change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, lowbatt, verify && lowbatt) || change;
                if(!ts.low_batt) {  /* Cleared? */
                    enqueueTagForDelay(tag, -1); /* Dequeue if needed */
                }
                else {
                    enqueueTagForDelay(tag,
                        tag.getLongestAgeOut() + RTN_DELAY); /* Enqueue timeout */
                }
            	/* If low battery and we're set to not do attributes on low battery */
                if (lowbatt && Boolean.TRUE.equals(grpattrib.get(TAGTYPEATTRIB_NODATALOWBATT))) {
                    change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX) || change;
                    change = updateNull(tag, HUMIDITY_ATTRIB_INDEX) || change;
                    change = updateNull(tag, DEWPOINT_ATTRIB_INDEX) || change;
                }
				ts.flgs_count++;
                ts.flgs_in_a_row++;
                break;
        }
		/* Now, consider if an hour has elapsed on message count */
		if(ts.start_ts < (cur_timestamp - MSGCOUNT_PERIOD)) {
			int act_count = (3 * ts.msg_count) + ts.flgs_count;	/* How many good beacons we received? */
			int exp_count = (MSGCOUNT_PERIOD / NOMINAL_BEACON_PERIOD);
			double pctloss = Math.round(100.0 * (exp_count - act_count) / (double)exp_count);
			if(pctloss < 0.0) pctloss = 0.0;
			if(pctloss > 100.0) pctloss = 100.0;
            change = updateDouble(tag, MSGLOSS_ATTRIB_INDEX, pctloss) || change;
			/* Reset accumulators */
			ts.start_ts = cur_timestamp;
			ts.flgs_count = 0;
			ts.msg_count = 0;
		}
        // Too many flags with no readings?
        if(ts.flgs_in_a_row == MAX_FLAGS_IN_A_ROW) {
            change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX) || change;
            change = updateNull(tag, HUMIDITY_ATTRIB_INDEX) || change;
            change = updateNull(tag, DEWPOINT_ATTRIB_INDEX) || change;
        }
        return change;
    }
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
        MantisIITag tag = (MantisIITag)t;
        if(tag.getTagLinkCount() == 0)  /* If no links, don't do it (leave attributes alone) */
            return;
        /* We do this when we've missed the flags-clear notice (760) */
        int cnt = getTagAttributes().length;
        Object[] oldvals = new Object[cnt];
        tag.readTagAttributes(oldvals, 0, cnt); /* Read em */
        /* If different, report change */
        if (tag.updateUsingPayload(3 << 7,
            System.currentTimeMillis())) {
            MantisIITagGroup tg = (MantisIITagGroup)tag.getTagGroup();
            tg.reportTagStatusAttributeChange(tag, oldvals);
        }
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}

}
