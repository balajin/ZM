package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;

/**
 * Tag type for treatment 04H tags - Mantis II Tags - Treatment 04S (Aurora badge tags)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04S extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04S";

	/* Proximity attributes - proximity ID (string) */
	public static final String LASTPROXID_ATTRIB = "lastProxID";
	public static final String LASTPROXID_ATTRIB_LABEL = "Last Proximity ID";
	/* Proximity attributes - proximity match (boolean) */
	public static final String LASTPROXMATCH_ATTRIB = "lastProxMatched";
	public static final String LASTPROXMATCH_ATTRIB_LABEL = "Last Proximity Matched";
	/* Proximity attributes - proximity timestamp (long - UTC milliseconds) */
	public static final String LASTPROXTS_ATTRIB = "lastProxTS";
	public static final String LASTPROXTS_ATTRIB_LABEL = "Last Proximity Timestamp";

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.IRLOCATOR_ATTRIB, MantisIITagType.BUTTON0_ATTRIB,
        MantisIITagType.BUTTON1_ATTRIB, MantisIITagType.BUTTON2_ATTRIB,
		LASTPROXID_ATTRIB, LASTPROXMATCH_ATTRIB, LASTPROXTS_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB};
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.IRLOCATOR_ATTRIB_LABEL, MantisIITagType.BUTTON0_ATTRIB_LABEL,
        MantisIITagType.BUTTON1_ATTRIB_LABEL, MantisIITagType.BUTTON2_ATTRIB_LABEL,
		LASTPROXID_ATTRIB, LASTPROXMATCH_ATTRIB, LASTPROXTS_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB_LABEL};
    private static final int IRLOCATOR_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int BUTTON0_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int BUTTON1_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int BUTTON2_ATTRIB_INDEX = 3; /*                                                         * Index in list of                                                         * attribute                                                         */
	private static final int LASTPROXID_ATTRIB_INDEX = 4;
	private static final int LASTPROXMATCH_ATTRIB_INDEX = 5;
	private static final int LASTPROXTS_ATTRIB_INDEX = 6;
    private static final int LOWBATT_ATTRIB_INDEX = 7; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {null, Boolean.FALSE,
        Boolean.FALSE, Boolean.FALSE,
		null, Boolean.FALSE, null,
		Boolean.FALSE};

    /* Payload value - minimum flags payload */
    private static final int MIN_FLAGS_PAYLOAD = 0760;
    /* Previous payloads requested - we want 4 to check for missed flags clear */
    private static final int PREV_PAYLOAD_CNT = 4;

    /* Return to normal delay - how long without flags report before auto RTN */
    private static final int RTN_DELAY = 60;    /* 60 seconds longer than longest ageout */

    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 5000;

	private static final int DUPLICATE_PROX_EVENT_PERIOD = 4000;
    /**
     * Constructor
     */
    public MantisIITagType04S() {
        super(4, 'S', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04S");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        switch((cur_payload>>7) & 0x03) {
            case 0:  /* First part of IR payload - nothing yet */
                break;
            case 1: /* Second part of IR payload */
                /* If previous payload is proper, and not too old, process */
                if(((payloads[0] >> 7) == 0) &&
                    ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                    /* Compute IR payload */
                    int ir = ((payloads[0] & 0x7F) << 7) + (cur_payload & 0x7F);
                    /* See if series 1 IR code */
                    String v = "";
                    if((ir >= 0x3E00) && (ir <= 0x3FEF)) {
                        ir = ir - 0x3E00;
                        v = Integer.toOctalString(ir);
                        if(ir < 010)
                            v = "00" + v;
                        else if(ir < 0100)
                            v = "0" + v;
                    }
                    else if(ir == 0) {  /* Not found, return compatable code */
                        v = "000";
                    }
                    else if((ir >= 0x0001) && (ir <= 0x270F)) { /* Series 2 code */
                        v = Integer.toString(ir);
                        if(ir < 10)
                            v = "000" + v;
                        else if(ir < 100)
                            v = "00" + v;
                        else if(ir < 1000)
                            v = "0" + v;
                    }
                    change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, v) || change;
                    /* Check to see if anything else is still defaulted - need to handle
                     * RTN timeout in case our flags are actually clear (and we don't get message)
                     */
                    if(tag.isDefaultFlagged(BUTTON0_ATTRIB_INDEX) ||
                        tag.isDefaultFlagged(BUTTON1_ATTRIB_INDEX) ||
                        tag.isDefaultFlagged(BUTTON2_ATTRIB_INDEX) ||
                        tag.isDefaultFlagged(LOWBATT_ATTRIB_INDEX)) {
                        enqueueTagForDelay(tag, 2*RTN_DELAY, true); /* Enqueue timeout */
                    }
                }
                break;
			case 2:	/* Payload 3 - wait for 4 */
				break;
            case 3: /* Flags or payload 4*/
				if(cur_payload >= MIN_FLAGS_PAYLOAD) {	/* If flags */
					boolean vv;
    	            /* Set button 0 flag */
					vv = ((cur_payload & 0x01) != 0);
	                change = updateBooleanVerify(tag, BUTTON0_ATTRIB_INDEX, vv, verify && vv) || change;
	                /* Set button 1 flag - verify on set */
					vv = ((cur_payload & 0x04) != 0);
	                change = updateBooleanVerify(tag, BUTTON1_ATTRIB_INDEX, vv, verify && vv) || change;
	                /* Set button 2 flag - verify on set */
					vv = ((cur_payload & 0x08) != 0);
	                change = updateBooleanVerify(tag, BUTTON2_ATTRIB_INDEX, vv, verify && vv) || change;
		            /* Set low battery flag - verify on set */
					vv = ((cur_payload & 0x02) != 0);
	                change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
	                /* If we have locator value, make sure we still are receiving */
	                if(tag.readTagAttribute(IRLOCATOR_ATTRIB_INDEX) != null) {
	                    boolean found = false;
	                    for (int i = 0; i < PREV_PAYLOAD_CNT; i++) {
	                        if ((payloads[i] < MIN_FLAGS_PAYLOAD) || /* There is one */
	                            (payloads[i] < 0)) { /* Or not enough history */
	                            found = true;
	                        }
	                    }
	                    if(!found) {    /* Not found, clear it out */
	                        change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, 
	                            null) || change;
	                    }
					}
                    /* If IR device, need to handle missing 760 flags timeout */
                    if(cur_payload == MIN_FLAGS_PAYLOAD) {  /* Cleared? */
                        enqueueTagForDelay(tag, -1); /* Dequeue if needed */
                    }
                    else {
                        enqueueTagForDelay(tag, 
                            tag.getLongestAgeOut() + RTN_DELAY); /* Enqueue timeout */
                    }
                }
				else {	/* Else, payload 4 */
	                /* If previous payload is proper, and not too old, process */
	                if(((payloads[0] >> 7) == 2) &&
	                    ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
	                    /* Compute prox payload */
	                    int prox = ((payloads[0] & 0x7E) >> 1) + ((cur_payload & 0x7F) << 6);
						String proxcode = Integer.toString(prox);
                        if(prox < 10)
                            proxcode = "000" + proxcode;
                        else if(prox < 100)
                            proxcode = "00" + proxcode;
                        else if(prox < 1000)
                            proxcode = "0" + proxcode;
						/* Get closest flag */
						boolean grant = ((payloads[0] & 0x01) != 0);
						/* Get timestamp */
						long ts = payloadage[0];
						/* Update proxcode and grant flag */
						boolean proxchg = updateString(tag, LASTPROXID_ATTRIB_INDEX, proxcode);
						proxchg = updateBoolean(tag, LASTPROXMATCH_ATTRIB_INDEX, grant) || proxchg;
						/* If lastproxts is less than 4 seconds ago, don't update if same attributes */
						Long lastts = (Long)tag.readTagAttribute(LASTPROXTS_ATTRIB_INDEX);
						if(proxchg || (lastts == null) || ((ts - lastts.longValue()) > DUPLICATE_PROX_EVENT_PERIOD)) {
							change = updateLong(tag, LASTPROXTS_ATTRIB_INDEX, Long.valueOf(ts)) || change;
						}
						change = change || proxchg;
					}
				}
                break;
        }
        return change;
    }
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
        MantisIITag tag = (MantisIITag)t;
        if(tag.getTagLinkCount() == 0)  /* If no links, don't do it (leave attributes alone) */
            return;
        /* We do this when we've missed the flags-clear notice (600) */
        int cnt = getTagAttributes().length;
        Object[] oldvals = new Object[cnt];
        tag.readTagAttributes(oldvals, 0, cnt); /* Read em */
        /* If different, report change */
        if (tag.updateUsingPayload(MIN_FLAGS_PAYLOAD,
            System.currentTimeMillis())) {
            MantisIITagGroup tg = (MantisIITagGroup)tag.getTagGroup();
            tg.reportTagStatusAttributeChange(tag, oldvals);
        }
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
