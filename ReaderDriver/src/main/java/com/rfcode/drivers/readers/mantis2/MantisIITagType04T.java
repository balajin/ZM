package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;

/**
 * Tag type for treatment 04H tags - Mantis II Tags - Treatment 04T (Aurora dispenser tags)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04T extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04T";

	/* Proximity attributes - proximity ID (string) */
	public static final String PROXID_ATTRIB = "proxID";
	public static final String PROXID_ATTRIB_LABEL = "Proximity ID";
	/* Proximity attributes - proximity match - input 1 (boolean) */
	public static final String LASTPROXMATCH1_ATTRIB = "lastProxMatched1";
	public static final String LASTPROXMATCH1_ATTRIB_LABEL = "Last Proximity Matched #1";
	/* Proximity attributes - proximity match - input 2 (boolean) */
	public static final String LASTPROXMATCH2_ATTRIB = "lastProxMatched2";
	public static final String LASTPROXMATCH2_ATTRIB_LABEL = "Last Proximity Matched #2";
	/* Proximity attributes - activation timestamp (long - UTC milliseconds) */
	public static final String LASTACTTS1_ATTRIB = "lastActTS1";
	public static final String LASTACTTS1_ATTRIB_LABEL = "Last Activation Timestamp #1";
	/* Proximity attributes - proximity timestamp (long - UTC milliseconds) */
	public static final String LASTACTTS2_ATTRIB = "lastActTS2";
	public static final String LASTACTTS2_ATTRIB_LABEL = "Last Activation Timestamp #2";
	/* Promixity attributes - activation counter 1 (long - cumulative) */
	public static final String ACTCOUNT1_ATTRIB = "actCount1";
	public static final String ACTCOUNT1_ATTRIB_LABEL = "Activation Counter #1";
	/* Promixity attributes - activation counter 2 (long - cumulative) */
	public static final String ACTCOUNT2_ATTRIB = "actCount2";
	public static final String ACTCOUNT2_ATTRIB_LABEL = "Activation Counter #2";
	/* Proximity attributes - activation counter start timestamp (long - UTC milliseconds) */
	public static final String ACTCOUNTTS_ATTRIB = "actCountTS";
	public static final String ACTCOUNTTS_ATTRIB_LABEL = "Counter Start Timestamp";
	/* Proximity attributes - input 1 state (boolean) */
	public static final String ACTINPUT1_ATTRIB = "actInput1";
	public static final String ACTINPUT1_ATTRIB_LABEL = "Activation Input #1";
	/* Proximity attributes - input 2 state (boolean) */
	public static final String ACTINPUT2_ATTRIB = "actInput2";
	public static final String ACTINPUT2_ATTRIB_LABEL = "Activation Input #2";


    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.IRLOCATOR_ATTRIB, PROXID_ATTRIB, 
		ACTCOUNT1_ATTRIB, ACTINPUT1_ATTRIB, LASTACTTS1_ATTRIB, LASTPROXMATCH1_ATTRIB,
		ACTCOUNT2_ATTRIB, ACTINPUT2_ATTRIB, LASTACTTS2_ATTRIB, LASTPROXMATCH2_ATTRIB,
        ACTCOUNTTS_ATTRIB, MantisIITagType.LOWBATT_ATTRIB};
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.IRLOCATOR_ATTRIB_LABEL,	PROXID_ATTRIB_LABEL, 
		ACTCOUNT1_ATTRIB_LABEL, ACTINPUT1_ATTRIB_LABEL, LASTACTTS1_ATTRIB_LABEL, LASTPROXMATCH1_ATTRIB_LABEL,
		ACTCOUNT2_ATTRIB_LABEL, ACTINPUT1_ATTRIB_LABEL, LASTACTTS2_ATTRIB_LABEL, LASTPROXMATCH2_ATTRIB_LABEL,
        ACTCOUNTTS_ATTRIB_LABEL, MantisIITagType.LOWBATT_ATTRIB_LABEL };

    private static final int IRLOCATOR_ATTRIB_INDEX = 0;	/* Index in attribute array */
    private static final int PROXID_ATTRIB_INDEX = 1;	/* Index in attribute array */
    private static final int ACTCOUNT1_ATTRIB_INDEX = 2;	/* Index in attribute array */
    private static final int ACTINPUT1_ATTRIB_INDEX = 3;	/* Index in attribute array */
    private static final int LASTACTTS1_ATTRIB_INDEX = 4;	/* Index in attribute array */
    private static final int LASTPROXMATCH1_ATTRIB_INDEX = 5;	/* Index in attribute array */
    private static final int ACTCOUNT2_ATTRIB_INDEX = 6;	/* Index in attribute array */
    private static final int ACTINPUT2_ATTRIB_INDEX = 7;	/* Index in attribute array */
    private static final int LASTACTTS2_ATTRIB_INDEX = 8;	/* Index in attribute array */
    private static final int LASTPROXMATCH2_ATTRIB_INDEX = 9;	/* Index in attribute array */
    private static final int ACTCOUNTTS_ATTRIB_INDEX = 10;	/* Index in attribute array */
    private static final int LOWBATT_ATTRIB_INDEX = 11;	/* Index in attribute array */

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {null, null, 
		null, null, null, null,
		null, null, null, null,
		null, Boolean.FALSE};

    /* Previous payloads requested - we want 4 to check for missed flags clear */
    private static final int PREV_PAYLOAD_CNT = 4;

    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 15000;

	/**
	 * Tag state - use to accumulate count data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		long init_ts;	/* Timestamp of last accumulated reading */
		long input_accum[] = new long[2];
		int	input_lastcnt[] = new int[2];

		public OurTagState(long ts) {
			init_ts = ts;
			input_lastcnt[0] = input_lastcnt[1] = -1;		/* Mark so that we don't assume its valid */
		}
		public void cleanupTag(MantisIITag t) { }
	}
	
    /**
     * Constructor
     */
    public MantisIITagType04T() {
        super(4, 'T', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04T");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);
		/* If needed, intialize our tag state */
		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(cur_timestamp);
			tag.setTagState(ts);
		}

        switch((cur_payload>>7) & 0x03) {
            case 0:  /* First part of IR payload - nothing yet */
                break;
            case 1: /* Second part of IR payload */
                /* If previous payload is proper, and not too old, process */
                if(((payloads[0] >> 7) == 0) &&
                    ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                    /* Compute IR payload */
                    int ir = ((payloads[0] & 0x7F) << 7) + (cur_payload & 0x7F);
                    /* See if series 1 IR code */
                    String v = "";
                    if((ir >= 0x3E00) && (ir <= 0x3FEF)) {
                        ir = ir - 0x3E00;
                        v = Integer.toOctalString(ir);
                        if(ir < 010)
                            v = "00" + v;
                        else if(ir < 0100)
                            v = "0" + v;
                    }
                    else if(ir == 0) {  /* Not found, return compatable code */
                        v = "000";
                    }
                    else if((ir >= 0x0001) && (ir <= 0x270F)) { /* Series 2 code */
                        v = Integer.toString(ir);
                        if(ir < 10)
                            v = "000" + v;
                        else if(ir < 100)
                            v = "00" + v;
                        else if(ir < 1000)
                            v = "0" + v;
                    }
                    change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, v) || change;
                }
                break;
			case 2:	/* Payload 3 or 4 - wait for 5 */
				break;
            case 3: /* Flags or payload 5, 6, or 7*/
				if((cur_payload & 0x1C0) == 0x180) {	/* Payload 5? */
	                /* If previous payloads are proper, and not too old, process */
	                if(((payloads[0] >> 6) == 5) &&
	                    ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD) &&
						((payloads[1] >> 6) == 4) &&
	                    ((cur_timestamp - payloadage[1]) < (2*MAX_HIGH_LOW_PERIOD))) {
						int prox = ((payloads[1] >> 1) & 0x1F) |
							((payloads[0] << 5) & 0x7E0) |
							((cur_payload << 7) & 0x1800);
						String proxcode = Integer.toString(prox);
                        if(prox < 10)
                            proxcode = "000" + proxcode;
                        else if(prox < 100)
                            proxcode = "00" + proxcode;
                        else if(prox < 1000)
                            proxcode = "0" + proxcode;
	                    change = updateString(tag, PROXID_ATTRIB_INDEX, proxcode) || change;
					}
    	            /* Set input 1 flag */
					boolean vv = ((cur_payload & 0x01) != 0);
	                change = updateBoolean(tag, ACTINPUT1_ATTRIB_INDEX, vv) || change;
	                /* Set input 2 flag */
					vv = ((cur_payload & 0x02) != 0);
	                change = updateBoolean(tag, ACTINPUT2_ATTRIB_INDEX, vv) || change;
		            /* Set low battery flag - verify on set */
					vv = ((cur_payload & 0x04) != 0);
	                change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
				}
				/* Else, if payload 6 or 7 */
				else {
					/* Set our initial count time - we're waiting since we may never get one of these */					
					change = updateLong(tag, ACTCOUNTTS_ATTRIB_INDEX, Long.valueOf(ts.init_ts)) || change;
					/* Read granted from packet */
					boolean grnt = ((cur_payload & 0x10) != 0);
					/* Get count from packet */
					int cnt = (cur_payload & 0xF);
					
					/* Get input index */
					int chid = ((cur_payload & 0x20) >> 5);
					boolean dup = false;
					/* If first event for this input, assume zero count */
					if(ts.input_lastcnt[chid] < 0) {
						ts.input_accum[chid] = 0;
					}
					else if(cnt == ts.input_lastcnt[chid]) {	/* Same as last - assume duplicate */
						dup = true;
					}
					else {
						ts.input_accum[chid] += (16 + cnt - ts.input_lastcnt[chid]) % 16;
					}
					ts.input_lastcnt[chid] = cnt;
					/* Update fields */
					if(chid == 0) {
		                change = updateBoolean(tag, LASTPROXMATCH1_ATTRIB_INDEX, grnt) || change;
						if(!dup) {
							change = updateLong(tag, LASTACTTS1_ATTRIB_INDEX, Long.valueOf(cur_timestamp)) || change;
							change = updateLong(tag, ACTCOUNT1_ATTRIB_INDEX, Long.valueOf(ts.input_accum[0])) || change;
						}
					}
					else {
		                change = updateBoolean(tag, LASTPROXMATCH2_ATTRIB_INDEX, grnt) || change;
						if(!dup) {
							change = updateLong(tag, LASTACTTS2_ATTRIB_INDEX, Long.valueOf(cur_timestamp)) || change;
							change = updateLong(tag, ACTCOUNT2_ATTRIB_INDEX, Long.valueOf(ts.input_accum[1])) || change;
						}
					}
				}
                break;
        }
        return change;
    }
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
