package com.rfcode.drivers.readers;

public interface SensorDefTag extends Tag {
	/**
	 * Set sensor definition for tag
	 * @param sd - sensor definition
	 */
	public void setSensorDefinition(SensorDefinition sd);
	/**
	 * Get sensor definition for tag
	 * @return definition, or null if none
	 */
	public SensorDefinition getSensorDefinition();
	
}
