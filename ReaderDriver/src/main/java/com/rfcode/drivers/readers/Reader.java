package com.rfcode.drivers.readers;

import java.net.InetAddress;
import java.util.Map;
import com.rfcode.drivers.BadParameterException;

/**
 * General interface for tag readers. Each must be supplied with values for the
 * attributes indicated by the reader's factory - via the
 * getReaderAttributeIDs() method.
 * 
 * @author Mike Primm
 */
public interface Reader extends ReaderEntity {
    /**
     * Get reader label
     */
    public String getLabel();
    /**
     * Set reader label
     * @param lab - label
     */
    public void setLabel(String lab);
    /**
     * Set the attributes for the reader. The keys of the map provided must
     * match the attribute IDs described by the getReaderAttrbiuteIDs() method
     * from the reader's factory.
     * 
     * @param attribs -
     *            map of values, keyed by attribute ID
     */
    public void setReaderAttributes(Map<String, Object> attribs)
        throws BadParameterException;
    /**
     * Get current value of attributes for the reader. The set returned will
     * include values for all attribute IDs indicated by the
     * getReaderAttributeIDs() method of the reader's factory.
     * 
     * @return map of attributes, keyed by attribute ID
     */
    public Map<String, Object> getReaderAttributes();
    /**
     * Get the reader's factory
     * 
     * @return factory
     */
    public ReaderFactory getReaderFactory();
    /**
     * Set reader enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setReaderEnabled(boolean enable);
    /**
     * Get reader enabled/disabled
     * 
     * @return enable setting
     */
    public boolean getReaderEnabled();
    /**
     * Get reader status
     * 
     * @return current status
     */
    public ReaderStatus getReaderStatus();
    /**
     * Get reader status message - more specific than reader status
     * 
     * @return current status message
     */
    public String getReaderStatusMsg();
    /**
     * Get reader firmware version
     * 
     * @return firmware version
     */
    public String getReaderFirmwareVersion();
    /**
     * Get reader firmware family
     *      * @return firmware family
     */
    public String getReaderFirmwareFamily();
    /**
     * Add reader status listener - multiple listeners are supported
     * 
     * @param listener -
     *            status listener
     */
    public void addReaderStatusListener(ReaderStatusListener listener);
    /**
     * Remove reader status listener
     * 
     * @param listener -
     *            status listener
     */
    public void removeReaderStatusListener(ReaderStatusListener listener);
    /**
     * Get list of TagGroups configured for reader to observe
     * 
     * @return list of TagGroups
     */
    public TagGroup[] getReaderTagGroups();
    /**
     * Set list of TagGroups for reader to observe
     * 
     * @param grps -
     *            list of TagGroups
     */
    public void setReaderTagGroups(TagGroup[] grps)
        throws BadParameterException;
    /**
     * Get maximum number of TagGroup IDs supported by reader. This limits the
     * number of IDs that can be supplied via setReaderTagGroupIDs().
     * 
     * @return max number of TagGroup IDs
     */
    public int getReaderMaxTagGroupCount();
    /**
     * Get channels associated with the reader (channels are distinctive radios
     * or scanners - detected tags are associated with one or more channels,
     * versus readers).
     * 
     * @return channels
     */
    public ReaderChannel[] getChannels();
    /**
     * Get tag capacity usage (percent)
     * @return usage (0-100), -1 if N/A
     */
    public int getUsedTagCapacityPercent();
	/**
	 * Request (or cancel) firmware upgrade
	 * @return true if request successfully started
	 */
	public boolean requestFirmwareUpgrade(boolean do_cancel);
	/**
	 * Check upgrade process (percent done - -1 if not active) 
	 */
	public int firmwareUpgradeProgress();
	/**
	 * Check if upgrade available
	 */
	public boolean firmwareUpgradeAvailable();
	/**
	 * Freshen DNS name - called from thread allowed to block
	 */
	public void freshenDNS();
	/**
	 * Get remote address of connection to/from reader (may be NAT translated)
	 *
	 * @return ip address and port of peer, or null if not connected
	 */
	public InetAddress getRemoteAddress();
	/**
	 * Get tag timeout, in seconds
	 */
    public int getTagTimeout();
	/**
	 * Get GPS tied to reader, if any
	 */
	public GPS getGPS();
    /**
     * Get reader startup time
     * @return 0 if unknown, else utc-msec from epoch
     */
    public long getReaderStartupTime();
    /**
     * Request reader network interface status
     */
    public void requestReaderNetworkInterfaceStatus(ReaderNetworkInterfaceStatusListener callback);
    /**
     * Return reader connection encryption status
     */
    public boolean getReaderConnectionEncrypted();
    /**
     * Set license for reader
     */
    public boolean setReaderLicense(boolean enabled);
    /**
     * Add reader status attributes (non-configuration)
     */
    public void addReaderStatusAttributes(Map<String, Object> map);
}
