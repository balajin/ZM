package com.rfcode.drivers.readers.mantis2;
import java.util.Map;

/**
 * Tag type for treatment 04D tags - Mantis II Tags - Treatment 04D (temperature)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04D extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04D";

	/** Tag type attribute - temperature rounding increment */
	public static final String TAGTYPEATTRIB_TMPROUND = "tmpround";
    /* Default rounding increment */
    public static final double TMPROUND_DEFAULT = 0.5;
	/** Tag type attribute - no data when low battery */
	public static final String TAGTYPEATTRIB_NODATALOWBATT = "nodatalowbatt";

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.TEMPERATURE_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB,
		MantisIITagType.SENSORDISCONNECT_ATTRIB,
		MSGLOSS_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.TEMPERATURE_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL,
		MantisIITagType.SENSORDISCONNECT_ATTRIB_LABEL,
		MSGLOSS_ATTRIB_LABEL
        };
    private static final int TEMPERATURE_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

	private static final int SENSORDISCONNECT_ATTRIB_INDEX = 2;
	private static final int MSGLOSS_ATTRIB_INDEX = 3;
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null,
        Boolean.FALSE, Boolean.FALSE, null};

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 18000;
    private static final int MIN_PERIOD = 5000;

	private static final double MAX_UNVERIFIED_TMP_DELTA = 2.0;	/* 2C max change without verify */

	private static final int	MSGCOUNT_PERIOD = 60*60*1000;	/* 1 hour */
	private static final int	NOMINAL_BEACON_PERIOD = 10050;	/* 10 seconds + jitter */
	/**
	 * Tag state - use to accumulate message loss data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		long	start_ts;
		int		msg_count;
		boolean lowbatt;
	
		public OurTagState(long ts) {
			start_ts = ts;
		}
		public void cleanupTag(MantisIITag t) { }
	}

    /**
     * Constructor
     */
    public MantisIITagType04D() {
        super(4, 'D', PREV_PAYLOAD_CNT, new String[] { TAGTYPEATTRIB_TMPROUND, TAGTYPEATTRIB_NODATALOWBATT } );
        setLabel("RF Code Mantis II Tag - Treatment 04D");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_TMPROUND, Double.valueOf(TMPROUND_DEFAULT));	/* Default to 0.5 */
		defs.put(TAGTYPEATTRIB_NODATALOWBATT, Boolean.valueOf(false));	/* Default to false */		
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);
		/* If needed, intialize our tag state */
		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(cur_timestamp);
			tag.setTagState(ts);
		}

        /* If second half of two part payload (low part) */
        if (cur_payload < 0400) {
			Boolean unplugged = null;

            Map<String,Object> grp_attrib = tag.getTagGroup().getTagGroupAttributes();
            /* Get low battery value - verify on setting flag */
			ts.lowbatt = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, ts.lowbatt, verify && ts.lowbatt) || change;
            
            boolean setval = true;
            // If low battery and we're ignoring suspect sensor values
            if (ts.lowbatt && Boolean.TRUE.equals(grp_attrib.get(TAGTYPEATTRIB_NODATALOWBATT))) {
            	change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX);
            	setval = false;
            }
			/* If previous was high value, and not too old */
            if ((payloads[0] >= 0400)
                && ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                int v = ((payloads[0] & 0xFF) << 8) | (cur_payload & 0xFF);
                v >>= 4;     /* Roll off other flags */
                if(v > 0x7FF) { /* If negative, sign extend */
                    v |= (-1 - 0xFFF);
                }
                /* Compute temperature */
                double tmp = 0.0625 * (double)v;
                /* Apply rounding, if defined */
				Object tc = grp_attrib.get(TAGTYPEATTRIB_TMPROUND);
                double rnd = TMPROUND_DEFAULT;
                if(tc instanceof Double) {
                    rnd = ((Double)tc).doubleValue();
                }
                if(rnd > 0.0)
                    tmp = Math.rint(tmp / rnd) * rnd;
                if (setval) {
                	change = updateDoubleVerify(tag, TEMPERATURE_ATTRIB_INDEX, tmp, verify,
            			MAX_UNVERIFIED_TMP_DELTA) || change;
                }
				unplugged = Boolean.valueOf(false);
				ts.msg_count++;	/* Bump message count */
            }
			/* If previous payload was 00x, and so is this one, and less than max high-low period, sensor is unplugged */
			else if((payloads[0] < 0xF) && (cur_payload < 0xF) && ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD) &&
				((cur_timestamp - payloadage[0]) >= MIN_PERIOD)) {
				unplugged = Boolean.valueOf(true);
				if (setval) {
					change = updateNullVerify(tag, TEMPERATURE_ATTRIB_INDEX, verify);
				}
			}
			/* Set unplugged status, if needed */
			if(unplugged != null)
				change = updateBooleanVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, unplugged, verify && unplugged) || change;
			/* If same payload as last time, and long enough to not be from another reader, update its timestamp */
			if((payloads[0] == cur_payload) && ((cur_timestamp - payloadage[0]) > MIN_PERIOD))
				payloadage[0] = cur_timestamp;
        }
		/* Now, consider if an hour has elapsed on message count */
		if(ts.start_ts < (cur_timestamp - MSGCOUNT_PERIOD)) {
			int act_count = (2 * ts.msg_count);	/* How many good beacons we received? */
			int exp_count = (MSGCOUNT_PERIOD / NOMINAL_BEACON_PERIOD);
			double pctloss = Math.round(100.0 * (exp_count - act_count) / (double)exp_count);
			if(pctloss < 0.0) pctloss = 0.0;
			if(pctloss > 100.0) pctloss = 100.0;
            change = updateDouble(tag, MSGLOSS_ATTRIB_INDEX, pctloss) || change;
			/* Reset accumulators */
			ts.start_ts = cur_timestamp;
			ts.msg_count = 0;
		}

        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
}
