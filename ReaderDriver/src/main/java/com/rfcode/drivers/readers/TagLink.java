package com.rfcode.drivers.readers;

import java.util.Map;

/**
 * This interface provides a model for representing the state of a specific tag
 * being viewed or observed by a specific channel of a specific reader. It acts
 * as a container for attributes associated with the status of this view - SSI
 * value, for example. It is intended that reader drivers will create and
 * maintain instances of object implementing this interface for each tag being
 * observed by each channel of each reader, and will remove/delete the
 * corresponding instances when a tag is no longer being observed by said
 * channel and reader (generally, after some sort of timeout). These should be
 * lightweight objects, as their population can be quite large.
 * 
 * @author Mike Primm
 */
public interface TagLink {
    /* Common attribute ID - SSI (integer dbm) */
    public static final String ATTRIB_SSI = "ssi";
    /* Common value: undefined SSI (no packet) */
    public static final int SSI_VALUE_NA = Integer.MIN_VALUE;
    /**
     * Get the Tag object associated with this TagLink
     * 
     * @return Tag associated with the link
     */
    public Tag getTag();
    /**
     * Get the ReaderChannel object associated with this TagLink
     * 
     * @return ReaderChannel associated with the link
     */
    public ReaderChannel getChannel();
    /**
     * Return values of tag link attributes in provided array. The attributes
     * are indexed in the same order as the attribute IDs returned by the
     * getTagLinkAttributeIDs() call of the ReaderFactory for the Reader
     * observing the tag. This is intended to be a high-performance interface.
     * 
     * @param val -
     *            array of length matching the number of attributes to return
     * @param start -
     *            index (relative to getTagLinkAttributeIDs()) of first
     *            attribute to return (goes in val[0])
     * @param count -
     *            number of consecutive attributes to return
     * @return number of attribute values returned
     */
    public int readTagLinkAttributes(Object[] val, int start, int count);
    /**
     * Read tag link attribute at given index
     * 
     * @param index -
     *            index of attribute to be read
     * @return attribute value, or null if not defined
     */
    public Object readTagLinkAttribute(int index);
    /**
     * Read tag link attribute with given ID
     * 
     * @param id -
     *            attribute ID
     * @return attribute value, or null if not defined
     */
    public Object readTagLinkAttribute(String id);
    /**
     * Get a copy of the curret tag link attributes values
     * 
     * @return map containing copy of attribute values, keyed by attribute ID
     */
    public Map<String, Object> getTagLinkAttributes();
    /**
     * Test if link is lost or disconnected
     * 
     * @return true if link to tag is lost, false if not
     */
    public boolean isLinkLost();
    /**
     * Get attribute count
     * 
     * @return number of attributes
     */
    public int getTagLinkAttributeCount();
    /**
     * Cleanup the tag link - NOT responsible for unhooking from Tag
     */
    public void cleanup();
}
