package com.rfcode.drivers.readers.mantis2;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import com.rfcode.ranger.RangerServer;
/**
 * Tag type for treatment 04O tags - Mantis II Tags - Treatment 04O
 * See "Protocol and Interface Specification for Emerson PDU Interface Tag" for details.
 *
 * @author Mike Primm
 */
public class MantisIITagType04O extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04O";
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID,
		PduBreakerTagSubType.TAGTYPEID,
		PduFeedLineTagSubType.TAGTYPEID
	};

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        LOWBATT_ATTRIB,
        PDUDISCONNECT_ATTRIB,
		PDUMODEL_ATTRIB,
		PDUSERIAL_ATTRIB,
		PDUMSGLOSS_ATTRIB,
		PDUTRUEPOWER_ATTRIB,
		PDUWATTHOURS_ATTRIB,
		PDUWATTHOURSTS_ATTRIB,
		PDUSERIALLIST_ATTRIB,
		PDUMODELLIST_ATTRIB,
		PDUFIRMWAREINCOMPAT_ATTRIB	};
    private static final String[] TAGATTRIBLABS = {
        LOWBATT_ATTRIB_LABEL,
		PDUDISCONNECT_ATTRIB_LABEL,
		PDUMODEL_ATTRIB_LABEL,
		PDUSERIAL_ATTRIB_LABEL,
		PDUMSGLOSS_ATTRIB_LABEL,
		PDUTRUEPOWER_ATTRIB_LABEL,
		PDUWATTHOURS_ATTRIB_LABEL,
		PDUWATTHOURSTS_ATTRIB_LABEL,
		PDUSERIALLIST_ATTRIB_LABEL,
		PDUMODELLIST_ATTRIB_LABEL,
		PDUFIRMWAREINCOMPAT_ATTRIB_LABEL };

    private static final int LOWBATT_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUMODEL_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUSERIAL_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
	private static final int PDUMSGLOSS_ATTRIB_INDEX = 4;

	private static final int PDUTRUEPOWER_ATTRIB_INDEX = 5;
	private static final int PDUWATTHOURS_ATTRIB_INDEX = 6;
	private static final int PDUWATTHOURSTS_ATTRIB_INDEX = 7;
	private static final int PDUSERIALLIST_ATTRIB_INDEX = 8;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 9;
	private static final int PDUFIRMWAREINCOMPAT_ATTRIB_INDEX = 10;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        Boolean.FALSE, Boolean.FALSE, "", "", null,
		null, null, null, null, null, Boolean.FALSE };

	private static final int MIN_FLAGS_PAYLOAD = 0760;
	private static final int MAX_FLAGS_PAYLOAD = 0777;

	private static final int FLAGS_PDUDISCONNECT = 0x01;
	private static final int FLAGS_LOWBATT = 0x02;

	private static final int BRANCHSTAT_OPEN = 1;
	private static final int BRANCHSTAT_NA = 2;

	private static final int FEEDSTAT_OVERLOAD = 1;
	private static final int FEEDSTAT_NA = 2;

	private static final int MIN_START_PAYLOAD = 0400;
	private static final int MAX_START_PAYLOAD = 0577;

	private static final int MIN_CONT_PAYLOAD = 0000;
	private static final int MAX_CONT_PAYLOAD = 0377;

	private static final int MIN_ALARM_PAYLOAD = 0600;
	private static final int MAX_ALARM_PAYLOAD = 0637;

	private static final long DEF_PKT_INTERVAL = 10000;	/* 10 seconds */

	private static class OurPhaseState {
		SubTag	tag;			/* Tag object for phase */
		int	last_volts_l_n_x10;	/* Last reported L-N voltage x 10 */
		int	last_volts_l_l_x10;	/* Last reported L-L voltage x 10 */
		String	phid;

		public OurPhaseState() {
			last_volts_l_l_x10 = -1;	/* None yet */
			last_volts_l_n_x10 = -1;
		}
	}
	private static class OurBreakerState {
		SubTag	tag;			/* Tag object for outlet */

		boolean	last_tripped;	/* Last tripped state */
		boolean	was_signalled;	/* true if state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */
		String	phid;

		public OurBreakerState() {
			last_tripped = false;
			was_signalled = false;
			pending_upd = false;
		}
	}
	private static class OurFeedLineState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;	/* Last amps, in 10ths */

		boolean	was_signalled;	/* true if overload state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */		
		boolean overload;		/* Load overload */

		String	feedid;

		public OurFeedLineState() {
			last_amps_x10 = -1;
			overload = false;
			was_signalled = false;
			pending_upd = false;
		}
	}

	private static class OurMsgAccum {	/* Need one for each reader to maintain sequentiality */
		private int[] accum;		/* Accumulator for message bytes */
		private int num_accum;		/* Number of bytes accumulated */
		private long last_ts;

		public OurMsgAccum() {
			accum = new int[6];
			num_accum = 0;
			last_ts = 0;
		}
	}

	private enum PhaseConfig {
		NA,
		SINGLE_PHASE_3_WIRE,
		TWO_PHASE_3_WIRE,
		THREE_PHASE_4_WIRE,
		THREE_PHASE_5_WIRE
	};

	private enum BranchSrcConfig {
		SRC_UNKNOWN("N/A"),
		SRC_L1_N("L1-N"),
		SRC_L2_N("L2-N"),
		SRC_L3_N("L3-N"),
		SRC_L_N("L1-N"),
		SRC_L1_L2("L1-L2"),
		SRC_L2_L3("L2-L3"),
		SRC_L3_L1("L3-L1"),
		SRC_L_L("L1-L2"),
		SRC_L1_N_L1_L2("L1-N/L1-L2"),
		SRC_L2_N_L2_L3("L2-N/L2-L3"),
		SRC_L3_N_L3_L1("L3-N/L3-L1"),
		SRC_N_A(null);

		private String phid;

		public String getPhase() { return phid; }

		private BranchSrcConfig(String p1) {
			phid = p1;
		}
	};

	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		private HashMap<String, OurMsgAccum> rdraccum;
		private int[]	accum;

		private StringBuilder serial_sb;
		private String serial;
		private StringBuilder model_sb;
		private String model;
		private PhaseConfig phase_cfg;
		private BranchSrcConfig[] branch_cfg;

		private OurPhaseState phasetags[];
		private OurBreakerState brktags[];
		private OurFeedLineState feedtags[];

		private boolean initdone;
		private boolean new10min;
		private boolean newhourly;

		boolean	fw_was_signalled;	/* true if fw incompat state changed by signal (newer than next flush) */
		boolean fw_pending_upd;	/* If verify active, used to indicate an update is pending */		
		boolean fwincompat;		/* Firmware incompatible flag */

		/* Message state info - used to detect 10-minute and hour boundaries */
		private int last_phase_index;	/* Index reported by last phase msg */
		private int last_phase_id;		/* Phase ID reported by last phase msg */
		private int msg_cnt;
		private boolean did_low_batt;	/* If we saw low battery during hour */

		long	watthours_ts;	/* Start time of watthours accumulator */
		double base_watthours;	/* total at start time watthours */
		double total_watts;	/* Total watts */
		double total_watthours;
		long tot_kwh;
		int	prev_kwh;

		private int PHASE_VOLTS_NA = 0xFFFF;
		private int FEED_AMPS_NA = 0x3FF;
		private int PDU_WATTS_NA = 0xFFFF;
		private int PDU_KWH_MAX = 0x10000;

		public OurTagState(MantisIITag tag) {
			resetDeviceConfiguration(tag, false);
			msg_cnt = -1;
			rdraccum = new HashMap<String, OurMsgAccum>();
			accum = null;
			serial_sb = new StringBuilder();
			model_sb = new StringBuilder();
			serial = null;
			model = null;
			branch_cfg = null;
			phase_cfg = PhaseConfig.NA;
			total_watthours = -1.0;
			total_watts = -1.0;
			base_watthours = 0.0;
			prev_kwh = -1;
			tot_kwh = 0;
		}
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t) {
			cleanupTowers();
		}
		/* Cleanup breakers */
		void cleanupBreakers() {
			if(brktags != null) {
				for(int i = 0; i < brktags.length; i++) {
					if(brktags[i] != null) {
						if(brktags[i].tag != null) {
							brktags[i].tag.lockUnlockTag(false);	/* Unlock it */
							brktags[i].tag = null;
						}
						brktags[i] = null;
					}
				}
				brktags = null;
			}
		}
		/* Cleanup phases */
		void cleanupPhases() {
			if(phasetags != null) {
				for(int i = 0; i < phasetags.length; i++) {
					if(phasetags[i] != null) {
						if(phasetags[i].tag != null) {
							phasetags[i].tag.lockUnlockTag(false);	/* Unlock it */
							phasetags[i].tag = null;
						}
						phasetags[i] = null;
					}
				}
				phasetags = null;
			}
		}
		/* Cleanup feed lines */
		void cleanupFeedLines() {
			if(feedtags != null) {
				for(int i = 0; i < feedtags.length; i++) {
					if(feedtags[i] != null) {
						if(feedtags[i].tag != null) {
							feedtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							feedtags[i].tag = null;
						}
						feedtags[i] = null;
					}
				}
				feedtags = null;
			}
		}

		/**
		 * Clean up towers
		 */
		private void cleanupTowers() {
			cleanupPhases();		/* Cleanup phases */
			cleanupBreakers();		/* Cleanup breakers */
			cleanupFeedLines();		/* Cleanup feed lines */

			initdone = false;
			new10min = false;
			newhourly = false;
		}
		/**
		 * Receive message byte to accumulate
		 */
		public boolean accumulateByte(MantisIITag tag, byte data, long ts, boolean first, MantisIIReader rdr) {
			boolean change = false;

			if(rdr == null) return change;
			/* Find and/or init accumulator */
			OurMsgAccum a = rdraccum.get(rdr.getID());
			if(a == null) {
				a = new OurMsgAccum();
				rdraccum.put(rdr.getID(), a);
			}			

			if(first) {	/* If first one */
				a.last_ts = ts;		/* Save ts */
				a.num_accum = 1;
				a.accum[0] = 0xFF & (int)data;
			}
			else {
				long last_int = ts - a.last_ts;

				/* If received more than N times 110% of interval + 60% of interval after start packet */
//				if(last_int > ((11*a.num_accum + 6)*DEF_PKT_INTERVAL/10)) {
				/* If more than 4 seconds early or 4 seconds late, its bad */
				if( (last_int > ((10*a.num_accum + 4)*DEF_PKT_INTERVAL/10)) ||
					(last_int < ((10*a.num_accum - 4)*DEF_PKT_INTERVAL/10))) {
					/* Missing packet, so reset accumulator */
					a.num_accum = 0;
				}
				else if(a.num_accum == 0) {	/* No start, just skip it */
				}
				else {						/* Else, add to accumulator */
					a.accum[a.num_accum] = 0xFF & (int)data;	/* Add it */
					a.num_accum++;
					if(a.num_accum == 6) {	/* Got all of them? */
						/* Check parity count */
						int cnt = 0;
						for(int i = 3; i < 48; i++) {
							if((a.accum[i/8] & (0x80>>(i%8))) != 0) {
								cnt++;
							}
						}
						if((cnt % 4) == (a.accum[0]>>5)) {	/* If message checks */
							a.accum[0] = (a.accum[0] & 0x1F);		/* Trim off parity */
							/* Now, see if its the same message from a different reader than the last message - if so,
							 * its a duplicate we need to ignore */
							if((accum != a.accum) && (accum != null) && Arrays.equals(accum, a.accum)) {
								/* Skip */
							}
							else {
								accum = a.accum;		/* Point to new message, and process it */
							
								change = processMessage(tag) || change;
							}
						}
						else {
//							System.out.println("Checksum error " + (cnt % 4) + "!=" + (a.accum[0]>>5));
						}
						a.num_accum = 0;
					}
				}
			}
			return change;
		}
		/* Read bit range from accumulator */
		private int getBits(int start, int end) {
			int rslt = 0;
			if(start > 44) start = 44;
			if(end < 0) end = 0;
			for(int i = start; i >= end; i--) {
				rslt = rslt << 1;
				if((accum[5 - (i>>3)] & (1 << (i & 7))) != 0) {
					rslt |= 1;
				}
			}
			return rslt;		
		}
		private int getBit(int s) {
			return getBits(s, s);
		}

		/**
	     * Device configuration reset - due to model change, serial change, or other configuration inconsistency
		 */
		private boolean	resetDeviceConfiguration(MantisIITag tag, boolean change) {
			cleanupTowers();		/* Cleanup towers */
			initdone = false;
			last_phase_index = -1;
			last_phase_id = -1;
			watthours_ts = 0;
			total_watthours = -1.0;
			prev_kwh = -1;
			tot_kwh = 0;
			total_watts = -1.0;
			base_watthours = 0.0;
			branch_cfg = null;
			/* Clean out PDU settings */
			change = updateNull(tag, PDUMODEL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIAL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIALLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMSGLOSS_ATTRIB_INDEX) || change;

			return change;
		}
		/**
		 * Flush 10 minute data, if any accumulated
		 */
		private boolean	flush10MinuteData(MantisIITag tag, boolean change) {
			if(!new10min)
				return change;

			new10min = false;
			
			if(!initdone)
				return change;

			int i;

			Long towerid = Long.valueOf(1);
			Long feedsetid = Long.valueOf(1);
			/* Loop through phases */
			for(i = 0; i < phasetags.length; i++) {
				Double vn = null;
				Double vp = null;

				if((phasetags[i].last_volts_l_l_x10 >= 0) && (phasetags[i].last_volts_l_l_x10 < PHASE_VOLTS_NA))
					vp = Double.valueOf(0.1 * phasetags[i].last_volts_l_l_x10);
				if((phasetags[i].last_volts_l_n_x10 >= 0) && (phasetags[i].last_volts_l_n_x10 < PHASE_VOLTS_NA))
					vn = Double.valueOf(0.1 * phasetags[i].last_volts_l_n_x10);
				String cfg = phasetags[i].phid;
				/* Update phase tag */
				PduPhaseTagSubType.updateTag(phasetags[i].tag, 
					(vn!=null)?vn:PduPhaseTagSubType.VALUE_NA,
					(vp!=null)?vp:PduPhaseTagSubType.VALUE_NA,  
					null, null, null, null, null, null, null, cfg, towerid, feedsetid);
			}
			/* Loop through feed lines for load warning and amps*/
			if(feedtags != null) {
				for(i = 0; i < feedtags.length; i++) {
					Boolean overload = null;
					Double amps = null;

					if(feedtags[i].was_signalled)
						overload = Boolean.valueOf(feedtags[i].overload);

					if((feedtags[i].last_amps_x10 >= 0) && (feedtags[i].last_amps_x10 < FEED_AMPS_NA))
						amps = Double.valueOf(0.1 * (double)feedtags[i].last_amps_x10);

					PduFeedLineTagSubType.updateTag(feedtags[i].tag, 
						amps, null, towerid, overload, null, feedsetid);

					feedtags[i].was_signalled = false;	/* Clear triggered flag */
				}
			}
			/* Loop through branch tags for breaker state */
			if(brktags != null) {
				for(i = 0; i < brktags.length; i++) {
					Boolean trip = null;

					if(brktags[i] == null)
						continue;

					if(brktags[i].was_signalled)
						trip = Boolean.valueOf(brktags[i].last_tripped);

					PduBreakerTagSubType.updateTag(brktags[i].tag, null, trip);

					brktags[i].was_signalled = false;	/* Clear triggered flag */
				}
			}
			/* Update top-level tag data, if we can */
			if((total_watts < 0) || (total_watts >= PDU_WATTS_NA))
				change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			else
				change = updateDouble(tag, PDUTRUEPOWER_ATTRIB_INDEX, Double.valueOf(total_watts)) || change;

			/* If total watt-hours is defined */
			if(total_watthours >= 0.0) {
				if(watthours_ts == 0) {	/* First time? */
					watthours_ts = System.currentTimeMillis();
					base_watthours = total_watthours;
				}
				change = updateDouble(tag, PDUWATTHOURS_ATTRIB_INDEX, 
						Double.valueOf(total_watthours - base_watthours)) || change;
				change = updateLong(tag, PDUWATTHOURSTS_ATTRIB_INDEX, 
						Long.valueOf(watthours_ts)) || change;
			}
			else {
				watthours_ts = 0;
				base_watthours = 0.0;
				change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
				change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			}
			/* If fw-incompat flag set, compute update */
			if(fw_was_signalled) {
				change = updateBoolean(tag, PDUFIRMWAREINCOMPAT_ATTRIB_INDEX, 
					fwincompat) || change;
				fw_was_signalled = false;
			}

			return change;
		}

		/**
		 * Flush hour data, if any accumulated
		 */
		private boolean	flushHourData(MantisIITag tag, boolean change) {
			change = flush10MinuteData(tag, change);

			if(!newhourly)
				return change;

			newhourly = false;

			if(!initdone)
				return change;
			/* Compute new message loss rate */
			if(msg_cnt >= 0) {	/* 60 messages per hour nominal */
				double v;
				if(did_low_batt)			/* If low battery signalled? */
					v = Math.round(100.0 - (msg_cnt/0.51));	/* 51 msgs per hour */
				else
					v = Math.round(100.0 - (msg_cnt/0.60));	/* 60 msgs per hour */
				if(v < 0.0) v = 0.0;
	            change = updateDouble(tag, PDUMSGLOSS_ATTRIB_INDEX, v) || change;
//				System.out.println(tag.getTagGUID() + ": msg_cnt=" + msg_cnt + ", loss=" + v);
			}

			/* Clear message count */
			msg_cnt = 0;
			did_low_batt = false;	/* Reset flag */

			return change;
		}

		/**
		 * Initialize device if needed, and we have full set of configuration data
		 */
		private void	initDeviceConfiguration(MantisIITag tag) {
			int i;
			TagSubGroup tg;
			if(initdone)
				return;
			/* If incomplete, keep going */
			if((serial == null) || (model == null) || (branch_cfg == null)) {
				return;
			}
			Long towerid = Long.valueOf(1);

			/* Initialize phases */
			int pcnt = 1;
			String[] phid = { "L1-N" };
			String[] flid = { "L1", "N" };
			Long feedsetid = Long.valueOf(1);
			switch(phase_cfg) {
				case SINGLE_PHASE_3_WIRE:
					break;
				case TWO_PHASE_3_WIRE:
					phid = new String[] { "L1-L2" };
					flid = new String[] { "L1", "L2" };
					break;
				case THREE_PHASE_5_WIRE:
					pcnt = 3;
					phid = new String[]{ "L1-N", "L2-N", "L3-N" };
					flid = new String[]{ "L1", "L2", "L3", "N" };
					break;
				case THREE_PHASE_4_WIRE:
					pcnt = 3;
					phid = new String[]{ "L1-L2", "L2-L3", "L3-L1" };
					flid = new String[]{ "L1", "L2", "L3" };
					break;
				default:
					break;
			}
			phasetags = new OurPhaseState[pcnt];
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
			for(i = 0; i < phasetags.length; i++) {
				phasetags[i] = new OurPhaseState();
				phasetags[i].phid = phid[i];
				phasetags[i].tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("phase%d",i+1), false,
					new int[] { PduPhaseTagSubType.PDUCONFIG_INDEX, 
						PduPhaseTagSubType.PDUFEEDSETID_INDEX,
						PduPhaseTagSubType.PDUTOWERID_INDEX },
					new Object[] { phasetags[i].phid, feedsetid, towerid } );
				phasetags[i].tag.lockUnlockTag(true);
			}
			/* Initialize branches */
			updateBranches(tag, false);
			/* Initialize feed lines */
			feedtags = new OurFeedLineState[flid.length];
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduFeedLineTagSubType.TAGTYPEID);
			for(i = 0; i < feedtags.length; i++) {
				feedtags[i] = new OurFeedLineState();
				feedtags[i].feedid = flid[i];
				feedtags[i].tag = SubTag.findOrCreateTag(tg, tag,
					String.format("feedline%s", flid[i]), false,
					new int[] { PduFeedLineTagSubType.PDUCONFIG_INDEX,
						PduFeedLineTagSubType.PDUFEEDSETID_INDEX,
						PduFeedLineTagSubType.PDUTOWERID_INDEX },
					new Object[] { feedtags[i].feedid, feedsetid, towerid } );
				feedtags[i].tag.lockUnlockTag(true);
			}
			initdone = true;
		}
		/* Handle per phase message */
		private boolean	handlePerPhaseMessage(MantisIITag tag, boolean change, int phid, int index,
			int volts_l_n_x10, int volts_l_l_x10) {

//			System.out.println(tag.getTagGUID() + ": Per Phase Volts: P" + (phid+1) + 
//				", period-index=" + index + 
//				", volts(L-N)=" + ((volts_l_n_x10 == PHASE_VOLTS_NA)?"---":(0.1 * volts_l_n_x10)) + 
//				"V, volts(L-L)=" + ((volts_l_l_x10 == PHASE_VOLTS_NA)?"---":(0.1 * volts_l_l_x10)) + "V");

			if((phasetags == null) || (phid >= phasetags.length))
				return change;

			/* See if we're due to flush hour */
			if((last_phase_index != 0) && (index == 0)) {	/* if first zero index */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index > index) {			/* We missed zero */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index != index) {		/* If new one */
				change = flush10MinuteData(tag, change);
			}
			else if(last_phase_id >= phid) {
				change = flush10MinuteData(tag, change);
			}
			last_phase_index = index;
			last_phase_id = phid;

			if(!initdone)
				return change;

			/* Update values */
			phasetags[phid].last_volts_l_l_x10 = volts_l_l_x10;
			phasetags[phid].last_volts_l_n_x10 = volts_l_n_x10;

			new10min = newhourly = true;

			return change;
		}
		/* Handle PDU energy and power message */
		private boolean	handlePDUEnergyMessage(MantisIITag tag, boolean change, 
			int watts, int kwh) {

			if(!initdone)
				return change;

			/* If valid watts, calculate value */
			if((watts >= 0) && (watts < PDU_WATTS_NA)) {
				total_watts = (double)watts;
			}
			else {
				total_watts = -1.0;
			}
			/* If valid kwh, calculate value to accumulate */
			if(kwh >= 0) {
				if(prev_kwh < 0) {	/* No previous? */
					tot_kwh = 0;
					total_watthours = 0.0;
				}
				else {
					int diff = (kwh - prev_kwh + PDU_KWH_MAX) % PDU_KWH_MAX;
					tot_kwh += diff;	/* Add to raw total */
					total_watthours = Math.round((double)tot_kwh * 100.0);
				}
				prev_kwh = kwh;
			}
			else {
				total_watthours = -1.0;
				prev_kwh = -1;
				tot_kwh = 0;
			}
			new10min = newhourly = true;

//			System.out.println("PDUEnergy: watts=" + 
//				((total_watts<0.0)?"---":total_watts) + "W, watt-hours=" +
//				((total_watthours < 0.0)?"---":total_watthours));

			return change;
		}
		/* Handle PDU model/serial messages */
		private boolean	handlePDUSerialMessage(MantisIITag tag, int idx, boolean change) {
			int i;

			/* Update buffer length, if needed */
			if(serial_sb.length() < (5 + (idx*6)))
				serial_sb.setLength(5 + (idx*6));
			/* If first packet, handle 5 characters plus outlet count */
			if(idx == 0) {
				for(i = 0; i < 5; i++) {
					serial_sb.setCharAt(i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
			}
			else {	/* Second or later has 6 characters, and may be end */
				for(i = 0; i < 6; i++) {
					serial_sb.setCharAt((idx*6) + i - 1, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				/* If end packet, and we've got no gaps, process serial number */
				if((getBit(0) != 0) && (serial_sb.indexOf("\0") < 0)) {	/* No nulls left? */
					String newserial = serial_sb.toString().trim();	/* Make trimmed number */
					/* If different from old one, reset configuration */
					if((serial != null) && (!serial.equals(newserial))) {
						change = resetDeviceConfiguration(tag, change);
						serial = newserial;
					}
					else if(serial == null) {	/* If no old, don't reset */
						serial = newserial;
					}
		            /* Set serial */
		            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX, serial) || change;
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					sn_lst.add(serial);
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) || change;
					
					serial_sb.setLength(0);	/* Reset accumulator */
					/* Initialize device configuration, if needed and ready */
					initDeviceConfiguration(tag);
				}
			}
			return change;
		}
		/* Handle PDU model message */
		private boolean handlePDUModelMessage(MantisIITag tag, int idx, boolean change) {
			int i;

			/* Update buffer length, if needed */
			if(model_sb.length() < (5 + (idx*6)))
				model_sb.setLength(5 + (idx*6));
			/* If first packet, handle 5 characters plus phase and breaker/fuse alarm support */
			if(idx == 0) {
				for(i = 0; i < 5; i++) {
					model_sb.setCharAt(i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
//				System.out.println("brk_alarms=" + brk_alarms);
				/* Check for phase configuration */
				PhaseConfig newcfg = PhaseConfig.NA;
				switch(getBits(5,4)) {
					case 0:
						newcfg = PhaseConfig.SINGLE_PHASE_3_WIRE;
						break;
					case 1:
						newcfg = PhaseConfig.TWO_PHASE_3_WIRE;
						break;
					case 2:
						newcfg = PhaseConfig.THREE_PHASE_4_WIRE;
						break;
					case 3:
						newcfg = PhaseConfig.THREE_PHASE_5_WIRE;
						break;
				}
				/* If mismatch on phase config, reset device */
				if(phase_cfg != newcfg) {
					if(phase_cfg != PhaseConfig.NA) {	/* If previous isn't default */
						change = resetDeviceConfiguration(tag, change);
					}
					phase_cfg = newcfg;
				}
			}
			else {	/* Else, not first model packet - add 6 characters and see if we're done */
				for(i = 0; i < 6; i++) {
					model_sb.setCharAt(5 + (6*(idx-1)) + i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				/* If end, and no more gaps, process model number */
				if((getBit(0) != 0) && (model_sb.indexOf("\0") < 0)) {
					String nmodel = model_sb.toString().trim();
					/* If mismatch, reset configuration */
					if((model != null) && (!model.equals(nmodel))) {
						PhaseConfig ph = phase_cfg;
						change = resetDeviceConfiguration(tag, change);
						model = nmodel;
						phase_cfg = ph;	/* Save - got it with this */
					}
					else if(model == null)
						model = nmodel;
		            /* Set model */
		            change = updateString(tag, PDUMODEL_ATTRIB_INDEX, model) || change;
					/* Update model number list */
					List<String> mn_lst = new ArrayList<String>();
					mn_lst.add(model);
		            change = updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, mn_lst) || change;
					/* Clear accumulator */
					model_sb.setLength(0);
					/* Initialize device configuration, if needed and ready */
					initDeviceConfiguration(tag);
				}
			}
			return change;
		}
		/* Update branch objects with new branch info */
		private boolean updateBranches(MantisIITag tag, boolean change) {
			int i;
			TagSubGroup tg;
			if(branch_cfg == null)
				return change;
			/* If needed, create breaker array - up to 9 */
			if(brktags == null)
				brktags = new OurBreakerState[branch_cfg.length];

			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduBreakerTagSubType.TAGTYPEID);
			Long towerid = Long.valueOf(1);
			Long feedsetid = Long.valueOf(1);
			/* Now, loop through them, and initialze/verify as appropriate */
			for(i = 0; i < branch_cfg.length; i++) {
				switch(branch_cfg[i]) {
					case SRC_N_A:		/* No branch present? */
						/* Clean it up, if needed */
						if(brktags[i] != null) {
							if(brktags[i].tag != null) {
								brktags[i].tag.lockUnlockTag(false);	/* Unlock it */
								brktags[i].tag = null;
							}
							brktags[i] = null;
						}
						break;
					default:
						if(brktags[i] == null) {
							brktags[i] = new OurBreakerState();
						}
						brktags[i].phid = branch_cfg[i].getPhase();
						if(brktags[i].tag == null) {	/* No tag? */
							brktags[i].tag = SubTag.findOrCreateTag(tg, tag,
								String.format("breaker%02d",i+1), false,
								new int[] { PduBreakerTagSubType.PDUCONFIG_INDEX,
									PduBreakerTagSubType.PDUFEEDSETID_INDEX,
									PduBreakerTagSubType.PDUTOWERID_INDEX },
								new Object[] { brktags[i].phid,
									feedsetid, towerid });
							brktags[i].tag.lockUnlockTag(true);
						}
						else {	/* Else, update it */
							PduBreakerTagSubType.updateTag(brktags[i].tag, null,
								null, towerid, brktags[i].phid, feedsetid);
						}
						break;
				}
			}
			return change;
		}

		/* Handle PDU branch config message */
		private boolean handlePDUBranchMessage(MantisIITag tag, boolean change) {
			int i;
			boolean chg = false;

//			System.out.print("PDUBranches:");
//			for(i = 0; i < 9; i++) {
//				System.out.print(" b" + (i+1) + "=" + getBits((4*i) + 3, 4*i));
//			}
//			System.out.println();

			if(branch_cfg == null) chg = true;	/* Definitely new if no previous */
			/* Parse branch config data */
			BranchSrcConfig branches[] = new BranchSrcConfig[9];	/* 9 possible branches */
			for(i = 0; i < branches.length; i++) {
				int cfg = getBits(4*i + 3, 4*i);
				switch(cfg) {
					case 0:
					default:
						branches[i] = BranchSrcConfig.SRC_UNKNOWN;
						break;
					case 1:
						branches[i] = BranchSrcConfig.SRC_L1_N;
						break;
					case 2:
						branches[i] = BranchSrcConfig.SRC_L2_N;
						break;
					case 3:
						branches[i] = BranchSrcConfig.SRC_L3_N;
						break;
					case 4:
						branches[i] = BranchSrcConfig.SRC_L_N;
						break;
					case 5:
						branches[i] = BranchSrcConfig.SRC_L1_L2;
						break;
					case 6:
						branches[i] = BranchSrcConfig.SRC_L2_L3;
						break;
					case 7:
						branches[i] = BranchSrcConfig.SRC_L3_L1;
						break;
					case 8:
						branches[i] = BranchSrcConfig.SRC_L_L;
						break;
					case 9:
						branches[i] = BranchSrcConfig.SRC_L1_N_L1_L2;
						break;
					case 10:
						branches[i] = BranchSrcConfig.SRC_L2_N_L2_L3;
						break;
					case 11:
						branches[i] = BranchSrcConfig.SRC_L3_N_L3_L1;
						break;
					case 15:
						branches[i] = BranchSrcConfig.SRC_N_A;
						break;
				}
				/* See if change versus previous value */
				if((branch_cfg != null) && (branch_cfg[i] != branches[i]))
					chg = true;
			}
			/* If no change, nothing to do */
			if(!chg)
				return change;
			/* Otherwise, update settings */
			branch_cfg = branches;
			/* If we were initialized, update branch definitions */
			if(initdone) {
				change = updateBranches(tag, change);
			}	
			else {	/* Else, try to initialze */
				/* Initialize device configuration, if needed and ready */
				initDeviceConfiguration(tag);
			}
			return change;
		}

		/* Handle feed line current message */
		private boolean handleFeedLineCurrentMessage(MantisIITag tag, boolean change,
			int[] amps_x10) {
			int i;

//			System.out.print("FeedLineCurrents:");
//			for(i = 0; i < amps_x10.length; i++) {
//				System.out.print(" amps" + i + "=" + ((amps_x10[i] == FEED_AMPS_NA)?"---":(0.1 * amps_x10[i])));
//			}
//			System.out.println();

			if(feedtags == null)
				return change;
			if(!initdone)
				return change;

			if(phase_cfg == PhaseConfig.SINGLE_PHASE_3_WIRE) {	/* L-N */
				/* Move N to second position */
				amps_x10[1] = amps_x10[3];
			}
			else if(phase_cfg == PhaseConfig.TWO_PHASE_3_WIRE) {	/* L1-L2 */
				amps_x10[1] = amps_x10[0];	/* L2 is same as L1 */
			}
			int cnt = feedtags.length;
			if(cnt > 4) cnt = 4;
			for(i = 0; i < cnt; i++) {
				feedtags[i].last_amps_x10 = amps_x10[i];
			}
			new10min = newhourly = true;

			return change;
		}

		/* Handle branch and feed line status message */
		private boolean handleBranchFeedStatusMessage(MantisIITag tag, boolean change,
			int[] branchstat, int[] feedstat, boolean fw_incompat) {
			int i;

//			System.out.print("BranchFeedStatus:");
//			for(i = 0; i < branchstat.length; i++) {
//				System.out.print(" branch" + i + "=" + branchstat[i]);
//			}
//			for(i = 0; i < feedstat.length; i++) {
//				System.out.print(" overload" + i + "=" + feedstat[i]);
//			}
//			System.out.println();

			if(!initdone)
				return change;

			/* Handle firmware flag */
			if(!fw_was_signalled) {
				fwincompat = fw_incompat;
				fw_was_signalled = true;
			}

			int cnt = brktags.length;
			if(cnt > branchstat.length) cnt = branchstat.length;
			for(i = 0; i < cnt; i++) {	/* Go through branches */
				if(brktags[i] == null)
					continue;
				if(branchstat[i] < BRANCHSTAT_NA) {	/* Good state */
					if(brktags[i].was_signalled) {
						/* Do nothing */
					}
					else {
						brktags[i].last_tripped = (branchstat[i] == BRANCHSTAT_OPEN);
					}
    				brktags[i].was_signalled = true;
				}
			}

			/* Make mapping from feed line tags to feed status fields */
			int linetoidx[];
			switch(phase_cfg) {
				case SINGLE_PHASE_3_WIRE:
					linetoidx = new int[]{ 1, 1 };
					break;
				case TWO_PHASE_3_WIRE:
					linetoidx = new int[]{ 1, 1 };
					break;
				default:
				case THREE_PHASE_5_WIRE:
					linetoidx = new int[]{ 2, 3, 4, 0 };
					break;
				case THREE_PHASE_4_WIRE:
					linetoidx = new int[]{ 2, 3, 4 };
					break;
			}
			cnt = feedtags.length;
			if(cnt > linetoidx.length) cnt = linetoidx.length;
			for(i = 0; i < cnt; i++) {
				int idx = linetoidx[i];
				if(feedstat[idx] < FEEDSTAT_NA) {	/* Valid value? */
					if(feedtags[i].was_signalled) {	/* Already updated by alarm signal */
						/* Do nothing */
					}
					else {
						feedtags[i].overload = (feedstat[idx] == FEEDSTAT_OVERLOAD);
					}
    				feedtags[i].was_signalled = true;
				}
			}
			new10min = newhourly = true;

			return change;
		}

		/* Process valid message buffer */
		private boolean processMessage(MantisIITag tag) {
			int i;
			boolean change = false;
			String mtype = null;

//			System.out.print(tag.getTagGUID() + ": message: ");
//			for(i = 0; i < 6; i++) {
//				System.out.print(String.format("%02x", accum[i]));
//			}
//			System.out.println();

			/* If we've hit first hour flush, count messages */
			if(msg_cnt >= 0)
				msg_cnt++;

			if(getBits(44, 42) == 0x06) {	/* If 44-42 = 1-1-0, Per Phase Voltage */
				change = handlePerPhaseMessage(tag, change, getBits(41,40), getBits(39,37),
					getBits(31,16), getBits(15, 0));
				mtype = "Per-Phase Power:" + getBits(41,40);
			}
			/* If 44-40 = 00000, PDU serial */
			else if(getBits(44,40) == 0x00) {
				change = handlePDUSerialMessage(tag, getBits(39,37), change);
				mtype = "Serial:" + getBits(39,37);
			}
			/* If 44-40 = 00001, PDU model */
			else if(getBits(44,40) == 0x01) {
				change = handlePDUModelMessage(tag, getBits(39,37), change);
				mtype = "Model:" + getBits(39,37);
			}
			/* If 44-40 = 00011, PDU branch config */
			else if(getBits(44,40) == 0x03) {
				change = handlePDUBranchMessage(tag, change);
				mtype = "BranchConfig";
			}
			/* If 44-41 = 1010, pdu cumulative energy */
			else if(getBits(44,41) == 0x0A) {
				change = handlePDUEnergyMessage(tag, change, 
					getBits(31,16), (getBit(32) == 1)?getBits(15, 0):-1);
				mtype = "PDU-Energy";
			}
			/* If 44-41 = 0011, feed line current */
			else if(getBits(44,40) == 0x06) {
				int[] ampsx10 = new int[4];
				for(i = 0; i < 4; i++) {
					ampsx10[i] = getBits(39-(10*i), 30-(10*i));
				}
				change = handleFeedLineCurrentMessage(tag, change, ampsx10);
				mtype = "Feed-Line-Current";
			}
			/* If 44-40 = 00010, branch/feed status */
			else if(getBits(44,40) == 0x02) {
				int[] brnchstat = new int[9];
				int[] linestat = new int[5];
				for(i = 0; i < 5; i++) {
					linestat[i] = getBits(27-(2*i), 26-(2*i));
				}
				for(i = 0; i < 9; i++) {
					brnchstat[i] = getBits(2*i+1, 2*i);
				}
				change = handleBranchFeedStatusMessage(tag, change, brnchstat, linestat,
					getBit(28)==1);
				mtype = "Branch-Feed-Status";
			}
			else {
				mtype = "unknown";
			}			
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), accum, -1, false, mtype);

			return change;
		}
		/* Handle alarm message */
		public boolean handleAlarm(MantisIITag tag, boolean trip, int alarmidx, boolean change, boolean verify) {
//			System.out.println("Alarm event! idx=" + alarmidx + ", tripped=" + trip);
			String mtype = null;
			/* If breaker/fuse? */
			if((alarmidx >= 0) && (alarmidx < 9)) {
				mtype = "breaker-alarm:" + alarmidx + ":" + trip;
				if((brktags != null) && (brktags.length > alarmidx) &&
					(brktags[alarmidx] != null)) {
					if(brktags[alarmidx].last_tripped == trip) {	/* Same as before? */
						brktags[alarmidx].pending_upd = false;	/* Nothing pending */
					}
					else {
						/* If unverified, or already pending, do update */
						if((!verify) || brktags[alarmidx].pending_upd) {
							brktags[alarmidx].last_tripped = trip;		/* Update value */
							brktags[alarmidx].was_signalled = true;		/* Mark as updated */
							PduBreakerTagSubType.updateTag(brktags[alarmidx].tag, null, 
								Boolean.valueOf(trip));
							brktags[alarmidx].pending_upd = false;	/* Nothing pending */					
							change = true;
						}
						else {	/* Else, verifying and this is first request */
							brktags[alarmidx].pending_upd = true;
						}
					}
				}
			}
			/* Else, if feed line load warning */
			else if((alarmidx >= 9) && (alarmidx <= 13)) {
				int feedidx = alarmidx-9;

				mtype = "feedline-alarm:" + feedidx + ":" + trip;

				if((phase_cfg == PhaseConfig.SINGLE_PHASE_3_WIRE) ||
					(phase_cfg == PhaseConfig.TWO_PHASE_3_WIRE)){	/* If L-N or L-L */
					/* Index is for L, which is first, or N */
					if(feedidx == 4) {	/* If N */
						feedidx = 1;
					}
				}
				else if(phase_cfg == PhaseConfig.TWO_PHASE_3_WIRE){	/* If L-L */
					/* Index is for L, which is first */
					if(feedidx == 2) {	/* If L2 */
						feedidx = 1;
					}
				}
				else {	/* Anything else, shift down 1 (since L only used in single) */
					feedidx--;
				}
				/* If valid feed line */
				if((feedtags != null) && (feedtags.length > feedidx) &&
					(feedtags[feedidx] != null)) {
					if(feedtags[feedidx].overload == trip) {	/* Same as before? */
						feedtags[feedidx].pending_upd = false;	/* Nothing pending */
					}
					else {
						/* If unverified, or already pending, do update */
						if((!verify) || feedtags[feedidx].pending_upd) {
							feedtags[feedidx].overload = trip;		/* Update value */
							feedtags[feedidx].was_signalled = true;		/* Mark as updated */
							PduFeedLineTagSubType.updateTag(feedtags[feedidx].tag, 
								null, null, null, Boolean.valueOf(trip), null, null);
							feedtags[feedidx].pending_upd = false;	/* Nothing pending */					
							change = true;
						}
						else {	/* Else, verifying and this is first request */
							feedtags[feedidx].pending_upd = true;
						}
					}
				}
			}
			/* Else, if firmware incompatabile warning */
			else if(alarmidx == 14) {
				mtype = "firmware-incompat-alarm";

				if(fwincompat == trip) {	/* Same as before? */
					fw_pending_upd = false;
				}
				else {
					/* If unverified, or already pending, do update */
					if((!verify) || fw_pending_upd) {
						fwincompat = trip;		/* Update value */
						fw_was_signalled = true;		/* Mark as updated */
						change = updateBoolean(tag, PDUFIRMWAREINCOMPAT_ATTRIB_INDEX, fwincompat);
						fw_pending_upd = false;	/* Nothing pending */					
					}
					else {	/* Else, verifying and this is first request */
						fw_pending_upd = true;
					}
				}
			}

			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), null, alarmidx, trip, mtype);

			return change;
		}
	}
    /**
     * 
     * Constructor
     */
    public MantisIITagType04O() {
        super(4, 'O', 0);
        setLabel("RF Code Mantis II Tag - Treatment 04O");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting payload 
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(tag);
			tag.setTagState(ts);
			change = true;
		}
        /* If payload is flags payload */
        if((cur_payload >= MIN_FLAGS_PAYLOAD) && (cur_payload <= MAX_FLAGS_PAYLOAD)) {
            /* Set low battery flag - verify on set*/
			boolean v = ((cur_payload & FLAGS_LOWBATT) != 0);

            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Remember that we saw low battery during hour - 7 vs 6 beacons per message */
			if((cur_payload & FLAGS_LOWBATT) != 0)
				ts.did_low_batt = true;
            /* Set disconnect flag - verify on set */
			v = ((cur_payload & FLAGS_PDUDISCONNECT) != 0);
			boolean dchg = updateBooleanVerify(tag, PDUDISCONNECT_ATTRIB_INDEX, v, verify && v);
            change |= dchg;
			/* If disconnected, reset the device configuration */
			if(v && dchg)
				change = ts.resetDeviceConfiguration(tag, change);
        }
        /* If start of message */
        else if((cur_payload >= MIN_START_PAYLOAD) && (cur_payload <= MAX_START_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0x7F), cur_timestamp, true, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If continuation of payload */
		else if((cur_payload >= MIN_CONT_PAYLOAD) && (cur_payload <= MAX_CONT_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0xFF), cur_timestamp, false, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If alarm payload */
		else if((cur_payload >= MIN_ALARM_PAYLOAD) && (cur_payload <= MAX_ALARM_PAYLOAD)) {
			change = ts.handleAlarm(tag, (cur_payload & 0x10) != 0, cur_payload & 0x0F, change, verify);
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}

        return change;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return SUBTYPES;
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
