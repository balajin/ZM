package com.rfcode.drivers.readers.mantis2;

/**
 * PDU breaker-specific data tag subtype
 * 
 * @author Mike Primm
 */
public class PduBreakerTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduBreaker";

	/* Tag attribute - breaker amperage (amps) (double) */
	public static final String PDUAMPS_ATTRIB = "brkAmps";
	public static final String PDUAMPS_ATTRIB_LABEL = "Amperage";

	/* Tag attribute - breaker state (boolean) */
	public static final String PDUTRIPPED_ATTRIB = "brkTripped";
	public static final String PDUTRIPPED_ATTRIB_LABEL = "Breaker Tripped";

	/* Tag attribute - breaker configuration (string) */
	public static final String PDUCONFIG_ATTRIB = "brkConfig";
	public static final String PDUCONFIG_ATTRIB_LABEL = "Breaker Configuration";

	/* Tag attribute - breaker bank index (int) */
	public static final String PDUBANKID_ATTRIB = "brkBank";
	public static final String PDUBANKID_ATTRIB_LABEL = "Bank ID";
	
	/* Tag attribute - bank overload (boolean) */
	public static final String PDUOVERLOAD_ATTRIB = "brkOverload";
	public static final String PDUOVERLOAD_ATTRIB_LABEL = "Bank Overload";
	
	public static final int PDUBANKID_INDEX = 2;
	public static final int PDUAMPS_INDEX = 3;
	public static final int PDUTRIPPED_INDEX = 4;
	public static final int PDUCONFIG_INDEX = 5;
	public static final int PDUFEEDSETID_INDEX = 6;
	public static final int PDUTOWERID_INDEX = 7;
	public static final int PDUOVERLOAD_INDEX = 8;

	public static final Double VALUE_NA = Double.valueOf(-1.0);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, PDUBANKID_ATTRIB, PDUAMPS_ATTRIB,
		PDUTRIPPED_ATTRIB, PDUCONFIG_ATTRIB, MantisIITagType.PDUFEEDSETID_ATTRIB, 
		MantisIITagType.PDUTOWERID_ATTRIB, PDUOVERLOAD_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, PDUBANKID_ATTRIB_LABEL, PDUAMPS_ATTRIB_LABEL,
		PDUTRIPPED_ATTRIB_LABEL, PDUCONFIG_ATTRIB_LABEL, MantisIITagType.PDUFEEDSETID_ATTRIB_LABEL,
		MantisIITagType.PDUTOWERID_ATTRIB_LABEL, PDUOVERLOAD_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null, null,
		null, null, null,
		null, null };
    /**
     * Constructor
     */
    public PduBreakerTagSubType() {
        setLabel("PDU Breaker Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }

	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double amps, Boolean tripped) {
		updateTag(tag, amps, tripped, null, null, null, null, null);
	}
	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double amps, Boolean tripped, Long towerid, String config, Long feedsetid) {
		updateTag(tag, amps, tripped, towerid, config, feedsetid, null, null);
	}
	public static void updateTag(SubTag tag, Double amps, Boolean tripped, Long towerid, String config, Long feedsetid, Integer bankid, Boolean overload) {
		if(tag == null)
			return;
		int[] idx = new int[7];
		Object[] val = new Object[7];
		int cnt = 0;
		if(amps != null) {
			idx[cnt] = PDUAMPS_INDEX;
			if(amps.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = amps;
			cnt++;
		}
		if(tripped != null) {
			idx[cnt] = PDUTRIPPED_INDEX;
			val[cnt] = tripped;
			cnt++;
		}
		if(config != null) {
			idx[cnt] = PDUCONFIG_INDEX;
			val[cnt] = config;
			cnt++;
		}
		if(feedsetid != null) {
			idx[cnt] = PDUFEEDSETID_INDEX;
			val[cnt] = feedsetid;
			cnt++;
		}
		if(towerid != null) {
			idx[cnt] = PDUTOWERID_INDEX;
			val[cnt] = towerid;
			cnt++;
		}
		if(bankid != null) {
			idx[cnt] = PDUBANKID_INDEX;
			val[cnt] = bankid;
			cnt++;
		}
		if(overload != null) {
			idx[cnt] = PDUOVERLOAD_INDEX;
			val[cnt] = overload;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}

}
