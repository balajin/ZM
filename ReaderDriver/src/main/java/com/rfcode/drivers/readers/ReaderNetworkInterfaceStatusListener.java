package com.rfcode.drivers.readers;

/**
 * Callback for results of request for a reader's network interface status
 * @author Mike Primm
 */
public interface ReaderNetworkInterfaceStatusListener {
    /**
     * Method invoked to deliver response to network status request
     */
    public void readerNetworkInterfaceResponse(Reader reader, ReaderNetworkInterface[] interfaces);
}

