package com.rfcode.drivers.readers;

/**
 * General interface for listening for reader status changes, including service
 * status events (such as serial output)
 * 
 * @author Mike Primm
 */
public interface ReaderServiceStatusListener extends ReaderStatusListener {
    /**
     * Report for service message
     * @param reader - reader reporting message
     * @param svcid - service ID
     * @param svcmsg - service message
     */
    public void reportServiceMessage(Reader reader, String svcid, String svcmsg);
}
