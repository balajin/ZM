package com.rfcode.drivers.readers;

/**
 * Extension of tag lifecycle listener, adding notifications for tag attribute
 * changes, tag link adds and tag link removes.
 * 
 * @author Mike Primm
 */
public interface TagStatusListener extends TagLifecycleListener {
    /**
     * Tag attributes changed notification - called when any of the tag's
     * attributes have been changed from their previous values.
     * 
     * @param tag -
     *            tag with change attributes
     * @param oldval -
     *            array of previous tag attribute values (may be longer than the
     *            number of attributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    public void tagStatusAttributeChange(Tag tag, Object[] oldval);
    /**
     * TagLink added notificiation. Called after taglink has been added to tag.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    public void tagStatusLinkAdded(Tag tag, TagLink taglink);
    /**
     * TagLink removed notificiation. Called after taglink has been removed from
     * tag.
     * 
     * @param tag -
     *            tag with removed link
     * @param taglink -
     *            link removed from tag
     */
    public void tagStatusLinkRemoved(Tag tag, TagLink taglink);
    /**
     * Tag triggered message notification - called when a tag has been
     * triggered to send a command message
     * 
     * @param tag - tag
     * @param cmd - command ID
     * @param attrids - list of attribute IDs
     * @param attrvals - list of attribute vals
     */
    public void tagStatusTriggeredMsg(Tag tag, String cmd, String[] attrids, Object[] attrvals);

}
