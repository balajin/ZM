package com.rfcode.drivers.readers;

import java.util.Map;
import java.util.ArrayList;
import com.rfcode.drivers.BadParameterException;

/**
 * General interface for defining a type of tag. These are intended to be
 * metadata objects, allowing readers to describe the types of tags they
 * support, and providing applications with the descriptions of those tag types.
 * 
 * @author Mike Primm
 */
public interface TagType extends ReaderEntity {
    /**
     * Initialize method - must be called once all attibutes are set
     * 
     * @throws DuplicateTagTypeException
     *             if duplicate tag type
     * @throws BadParameterException
     *             if bad or missing attribute
     */
    public void init() throws DuplicateEntityIDException, BadParameterException;
    /**
     * Discard the TagType
     */
    public void cleanup();
    /**
     * Set tag type id - this is required and must be unique, and cannot be
     * changed
     * 
     * @param id -
     *            tag type id
     */
    public void setID(String id);
    /**
     * Get ID for the tag type - this is a "well known" ID that will be returned
     * by tag tag reader drivers when indicating support for various tag
     * families.
     * 
     * @return TagType ID
     */
    public String getID();
    /**
     * Set list of attributes required for TagGroup to be defined for this
     * TagType. Must be set, and cannot be changed once set.
     * 
     * @param list -
     *            list of attribute IDs
     */
    public void setTagGroupAttributes(String[] list);
    /**
     * Get list of attributes required for TagGroup to be defined for this
     * TagType.
     * 
     * @returns list of attibute IDs
     */
    public String[] getTagGroupAttributes();
    /**
     * Get copy of default attributes set for TagGroup.
     * 
     * @returns map of attibute IDs
     */
    public Map<String, Object> getTagGroupDefaultAttributes();
    /**
     * Get list of the attributes required for TagGroup that determine if the
     * group is distinct (that is, which much be different from other groups for
     * the same tag type in order to have a distinct tag population). If list is
     * empty, all the group attributes are assumed to need to match in order to
     * have a duplicate group.
     * 
     * @return list of attribute IDs determining distinct group
     */
    public String[] getTagGroupDistinctAttributes();
    /**
     * Get list of attributes reported by tags of this type
     * 
     * @returns list of attibute IDs
     */
    public String[] getTagAttributes();
    /**
     * Get list of labels for attributes reported by tags of this type (ordered to
     * match same index as IDs)
     * 
     * @returns list of attibute labels
     */
    public String[] getTagAttributeLabels();
    /**
     * Get label for given attribute ID
     * @param id - attribute ID
     * @return label (or ID if not found)
     */
    public String getTagAttributeLabel(String id);
    /**
     * Get count of attributes reported by tags of this type
     * 
     * @returns count of attibute IDs (length of list from getTagAttributes()
     */
    public int getTagAttributeCount();
    /**
     * Find attribute index for given attribute ID
     * 
     * @param id -
     *            attribute ID
     * @return index, or -1 if not found
     */
    public int getTagAttributeIndex(String id);
    /**
     * Get copy of list of default attribute values for newly found tags of this
     * type.
     * 
     * @returns list of attibute default values
     */
    public Object[] getTagAttributeDefaultValues();
    /**
     * Set label for tag type
     * 
     * @param lab -
     *            label
     */
    public void setLabel(String lab);
    /**
     * Get label for tag type
     * 
     * @return Label
     */
    public String getLabel();
    /**
     * Factory method for creating TagGroup instances for this TagType.
     * Resulting TagGroup must have attributes set, and its init() method
     * called.
     * 
     * @return new TagGroup (or null, if isSubType() = true)
     */
    public TagGroup createTagGroup();
	/**
	 * Factory method for making subgroup for this tag sub-type.  Only used if
     * isSubType() is true.  Resulting TagGroup must have its init() method
     * called.
     * 
     * @return new TagGroup (or null, if isSubType() = false)
	 */
	public TagGroup createTagSubGroup(TagGroup parentgrp);
    /**
     * Add TagLifecycleListener or TagStatusListener for all tags of this type.
     * 
     * @param listen -
     *            listener to be added
     */
    public void addTagLifecycleListener(TagLifecycleListener listen);
    /**
     * Remove TagLifecycleListener or TagStatusListener for all tags of this
     * type.
     * 
     * @param listen -
     *            listener to be removed
     */
    public void removeTagLifecycleListener(TagLifecycleListener listen);
    /**
     * Access tag lifecycle listener list (read-only)
     * 
     * @return list of listeners
     */
    public ArrayList<TagLifecycleListener> getTagLifecycleListeners();
    /**
     * Register new tag-type specific attribute. Once added, cannot be runtime
     * removed. Returned index is only good for life of current run, and must be
     * assumed to change whenever the attribute is registered on future runs.
     * Also, the assigned index is NOT common across different TagTypes. Can
     * only be called AFTER TagType.init() has completed.
     * 
     * @param aid -
     *            attribute ID
     * @param defval -
     *            default value
     * @param label -
     *            attribute label
     * @return attribute's assigned index for this tag type
     */
    public int registerTagAttribute(String aid, Object defval, String label);
    /**
     * Get tag type version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion();
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param tag - delayed tag
     */
    public void processDelayedTag(Tag tag);
	/**
	 * Is tag type a subtype?  If so, used internally by other tag types and cannot be used
     * to create a group directly.
	 */
	public boolean isSubType();
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes();
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported();
	/**
	 * Test if valid sensor definition for given tag type
	 * @param sd - sensor definition
	 * @return true, or false if not valid
	 */
	public boolean isValidSensorDefinition(SensorDefinition sd);
	/**
	 * Notification that tag group attributes have been set/updated
	 * 
	 * @param grp - tag group
	 */
	public void tagGroupAttributesUpdated(TagGroup grp);
	/**
	 * Notification that tag group has been deleted
	 * 
	 * @param grp - tag group
	 */
	public void tagGroupDeleted(TagGroup grp);
}
