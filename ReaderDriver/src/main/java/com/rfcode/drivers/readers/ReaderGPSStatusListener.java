package com.rfcode.drivers.readers;

/**
 * General interface for listening for reader status changes, extended to include GPS fix status changes
 * 
 * @author Mike Primm
 */
public interface ReaderGPSStatusListener extends ReaderStatusListener {
    /**
     * Callback for reporing reader GPS status change. Handler MUST process
     * immediately and without blocking.
     * 
     * @param reader -
     *            reader reporting change
     * @param newstatus -
     *            new GPS fix status for reader's GPS
     * @param oldstatus -
     *            previous GPS fix status for reader's GPS
     */
    public void readerGPSFixStatusChanged(Reader reader, GPSData.Fix newstatus,
        GPSData.Fix oldstatus);
}
