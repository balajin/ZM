package com.rfcode.drivers.readers.mantis2;

/**
 * Interface class for tag message listener
 * 
 * @author Mike Primm
 */
public interface MantisIIReaderTagMessageListener {
    /**
     * Callback for delivering tag messages - callback MUST NOT block or do
     * significant processing
     * 
     * @param sess -
     *            Reader session
     * @param msg -
     *            tag message - set to null when session is closed
     */
    public void processReaderTagMessage(MantisIIReaderSession sess,
        StringBuilder msg);
    /**
     * Callback for delivering gps messages - callback MUST NOT block or do
     * significant processing
     * 
     * @param sess -
     *            Reader session
     * @param msg -
     *            tag message - set to null when session is closed
     */
    public void processReaderGPSMessage(MantisIIReaderSession sess,
        StringBuilder msg);
}
