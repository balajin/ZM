package com.rfcode.drivers.readers;

/**
 * Listener interface for subscribing to reader lifecycle events for readers
 * from a given ReaderFactory. Reports events for reader creation, deletion,
 * enabling and disabling.
 * 
 * @author Mike Primm
 */
public interface ReaderLifecycleListener {
    /**
     * Reader creation notification - called when new reader object creation
     * completed (at the end of its init() call).
     * 
     * @param reader -
     *            reader object created
     */
    public void readerLifecycleCreate(Reader reader);
    /**
     * Reader deletion notification - called at start of reader's cleanup()
     * method, when reader is about to be deleted.
     * 
     * @param reader -
     *            reader object being deleted
     */
    public void readerLifecycleDelete(Reader reader);
    /**
     * Reader enable change notification - called when reader has just changed
     * its enabled state (after the setReaderEnabled() method has been called).
     * 
     * @param reader -
     *            reader object being enabled or disabled
     * @param enable -
     *            true if now enabled, false if now disabled
     */
    public void readerLifecycleEnable(Reader reader, boolean enable);
}
