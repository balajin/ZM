package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import com.rfcode.ranger.RangerServer;
/**
 * Tag type for treatment 04Q tags - Mantis II Tags - Treatment 04Q
 * See "Protocol and Interface Specification for APC PDU Interface Tag" for details.
 *
 * @author Mike Primm
 */
public class MantisIITagType04Q extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04Q";
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID,
		PduOutletTagSubType.TAGTYPEID,
		PduBreakerTagSubType.TAGTYPEID,
		PduFeedLineTagSubType.TAGTYPEID
	};

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        LOWBATT_ATTRIB,
        PDUDISCONNECT_ATTRIB,
		PDUMODEL_ATTRIB,
		PDUSERIAL_ATTRIB,
		PDUMSGLOSS_ATTRIB,
		PDUTRUEPOWER_ATTRIB,
		PDUAPPPOWER_ATTRIB,
		PDUPWRFACT_ATTRIB,
		PDUWATTHOURS_ATTRIB,
		PDUAPPPWRHOURS_ATTRIB,
		PDUWATTHOURSTS_ATTRIB,
		PDUSERIALLIST_ATTRIB,
		PDUMODELLIST_ATTRIB,
		PDUTOWERDISCONNECT_ATTRIB,
		PDUDISCONNECTEDTOWERS_ATTRIB,
		PDUTOWERCOUNT_ATTRIB
	};
    private static final String[] TAGATTRIBLABS = {
        LOWBATT_ATTRIB_LABEL,
		PDUDISCONNECT_ATTRIB_LABEL,
		PDUMODEL_ATTRIB_LABEL,
		PDUSERIAL_ATTRIB_LABEL,
		PDUMSGLOSS_ATTRIB_LABEL,
		PDUTRUEPOWER_ATTRIB_LABEL,
		PDUAPPPOWER_ATTRIB_LABEL,
		PDUPWRFACT_ATTRIB_LABEL,
		PDUWATTHOURS_ATTRIB_LABEL,
		PDUAPPPWRHOURS_ATTRIB_LABEL,
		PDUWATTHOURSTS_ATTRIB_LABEL,
		PDUSERIALLIST_ATTRIB_LABEL,
		PDUMODELLIST_ATTRIB_LABEL,
		PDUTOWERDISCONNECT_ATTRIB_LABEL,
		PDUDISCONNECTEDTOWERS_ATTRIB_LABEL,
		PDUTOWERCOUNT_ATTRIB_LABEL };

    private static final int LOWBATT_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUMODEL_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUSERIAL_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
	private static final int PDUMSGLOSS_ATTRIB_INDEX = 4;

	private static final int PDUTRUEPOWER_ATTRIB_INDEX = 5;
	private static final int PDUAPPPOWER_ATTRIB_INDEX = 6;
	private static final int PDUPWRFACTOR_ATTRIB_INDEX = 7;
	private static final int PDUWATTHOURS_ATTRIB_INDEX = 8;
	private static final int PDUAPPPWRHOURS_ATTRIB_INDEX = 9;
	private static final int PDUWATTHOURSTS_ATTRIB_INDEX = 10;
	private static final int PDUSERIALLIST_ATTRIB_INDEX = 11;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 12;
	private static final int PDUTOWERDISCONNECT_ATTRIB_INDEX = 13;
	private static final int PDUDISCONNECTEDTOWERS_ATTRIB_INDEX = 14;
	private static final int PDUTOWERCOUNT_ATTRIB_INDEX = 15;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        Boolean.FALSE, Boolean.FALSE, "", "", null,
		null, null, null, null, null, null, null, null, Boolean.FALSE, 
		new ArrayList<Long>(), null };

	private static final int MIN_FLAGS_PAYLOAD = 0770;
	private static final int MAX_FLAGS_PAYLOAD = 0777;

	private static final int FLAGS_PDUDISCONNECT = 0x01;
	private static final int FLAGS_LOWBATT = 0x02;


	private static final int MIN_START_PAYLOAD = 0400;
	private static final int MAX_START_PAYLOAD = 0577;

	private static final int MIN_CONT_PAYLOAD = 0000;
	private static final int MAX_CONT_PAYLOAD = 0377;

	private static final int MIN_ALARM_PAYLOAD = 0600;
	private static final int MAX_ALARM_PAYLOAD = 0677;

	private static final int MIN_OUTSTAT_PAYLOAD1 = 0700;
	private static final int MAX_OUTSTAT_PAYLOAD1 = 0737;

	private static final int MIN_OUTSTAT_PAYLOAD2 = 0740;
	private static final int MAX_OUTSTAT_PAYLOAD2 = 0767;

	private static final long DEF_PKT_INTERVAL = 10000;	/* 10 seconds */
	private static final long MAX_OUTSTAT_PERIOD = 1000;
	private static final int REBOOT_DELAY = 5;		/* 5 seconds */
    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;

	private static class OurPhaseState {
		SubTag	tag;			/* Tag object for phase */
		int	last_ampsx10;		/* Last amperage x 10 */
		int last_volts;			/* RMS volts */
		int last_volts_ln;			/* Last volts outlets Lx-N */
		int last_volts_ll;		/* Last volts outlets Lx-Lx+1 */
		int last_wattsx10;		/* Last watts x 10 */
		String	phid;
		Long    feedsetid;

		public OurPhaseState() {
			last_wattsx10 = -1;
			last_volts_ln = -1;
			last_volts_ll = -1;
			last_volts = -1;
			last_ampsx10 = -1;
		}
	}
	private static final int SWITCH_ON = 0;
	private static final int SWITCH_OFF = 1;
	private static final int SWITCH_REBOOT = 2;

    private static class OurBankState {
		SubTag	tag;			/* Tag object for bank/breakr */
        String bank_id; /* 0=A, 1=B, etc */
        String phid;    /* Phase ID (L1-N, L2-N, etc) */
        int phidx;
        boolean isLtoL;
		boolean	overload;
		boolean	ol_signalled;	/* true if overload state changed by signal */
		boolean ol_pending_upd;	/* If verify active, used to indicate an update is pending */		
        
        OurOutletState[] outlets;
        OurBankState(String id) {
        	tag = null;
            bank_id = id;
            phid = null;
            phidx = -1;
            isLtoL = false;
            outlets = null;
            overload = false;
            ol_signalled = false;
            ol_pending_upd = false;
        }           
    }

	private static class OurOutletState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;		/* Last reported amperage (x10) */
		int		last_watthours;		/* Last watthours */
		long	last_watthours_ts;	/* Last time of watthours accumulator */
		double	last_watts;		/* Last calculated watts */
		double	total_watthours;	/* Accumulated watthours */
		double	total_vahours;	/* Accumulated VA-hours */
		double	last_diff;		/* Last watthours added - used for VA-hours */
		long	watthours_ts;	/* Start time of watthours accumulator */
		String	phid;
		String  label;
		Long    feedsetid;

		int		switch_state;		/* 0=on, 1=off, 2=reboot */
		boolean	sw_signalled;	/* true if switch_off state changed by signal */
		boolean sw_pending_upd;	/* If verify active, used to indicate an update is pending */		

		public OurOutletState() {
			last_amps_x10 = -1;
			last_watthours = -1;
			last_watthours_ts = -1;
			last_watts = -1.0;
			total_watthours = -1.0;
			total_vahours = -1.0;
			last_diff = 0.0;
			watthours_ts = 0;
			switch_state = SWITCH_ON;
			sw_signalled = false;
			sw_pending_upd = false;
		}
	}
	private static class OurFeedLineState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;	/* Last amps, in 10ths */

		boolean overload;		/* Line overload state */
		boolean	was_signalled;	/* true if overload state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */		

		String	feedid;
		Long    feedsetid;

		public OurFeedLineState() {
			last_amps_x10 = -1;
			overload = false;
			was_signalled = false;
			pending_upd = false;
		}
	}

	private static class OurMsgAccum {	/* Need one for each reader to maintain sequentiality */
		private int[] accum;		/* Accumulator for message bytes */
		private int num_accum;		/* Number of bytes accumulated */
		private long last_ts;

		public OurMsgAccum() {
			accum = new int[6];
			num_accum = 0;
			last_ts = 0;
		}
	}

	private enum PhaseConfig {
		N_A,
		NONE,
		SINGLE_PHASE,
		THREE_PHASE_WYE,
		THREE_PHASE_DELTA,
		THREE_PHASE_DELTA_4WIRE
	};

	private static class OurTowerState {
		StringBuilder serial_sb;
		String serial;
		StringBuilder model_sb;
		String model;
		PhaseConfig phase_cfg;
		boolean switched_outlets;
		boolean bank_monitored;
		
		int	last_wattsx10;
		int last_voltampsx10;
		int last_watthoursx10;
		double tot_watthours;

		OurPhaseState phasetags[];
		OurBankState banks[];
		OurFeedLineState feedtags[];

		public OurTowerState() {
			serial_sb = new StringBuilder();
			model_sb = new StringBuilder();
			serial = null;
			model = null;
			phase_cfg = PhaseConfig.N_A;
			switched_outlets = false;
			bank_monitored = false;
			last_wattsx10 = OurTagState.DEVICE_WATTS_NA;
			last_voltampsx10 = OurTagState.DEVICE_VA_NA;
			last_watthoursx10 = OurTagState.DEVICE_KWH_NA;
			tot_watthours = 0.0;
		}
		/* Cleanup outlets */
		void cleanupOutlets() {			
			if(banks != null) {
                for(int i = 0; i < banks.length; i++) {
                	if (banks[i] == null) continue;
                    if(banks[i].outlets != null) {
                        for(int j = 0; j < banks[i].outlets.length; j++) {
    						if(banks[i].outlets[j].tag != null) {
    							banks[i].outlets[j].tag.lockUnlockTag(false);	/* Unlock it */
    							banks[i].outlets[j].tag = null;
    						}
						}
						banks[i].outlets = null;
					}
					if(banks[i].tag != null) {
						banks[i].tag.lockUnlockTag(false);	/* Unlock it */
						banks[i].tag = null;
					}
                    banks[i] = null;
				}
                banks = null;
    		}
		}
		/* Cleanup phases */
		void cleanupPhases() {
			if(phasetags != null) {
				for(int i = 0; i < phasetags.length; i++) {
					if(phasetags[i] != null) {
						if(phasetags[i].tag != null) {
							phasetags[i].tag.lockUnlockTag(false);	/* Unlock it */
							phasetags[i].tag = null;
						}
						phasetags[i] = null;
					}
				}
				phasetags = null;
			}
		}
		/* Cleanup feed lines */
		void cleanupFeedLines() {
			if(feedtags != null) {
				for(int i = 0; i < feedtags.length; i++) {
					if(feedtags[i] != null) {
						if(feedtags[i].tag != null) {
							feedtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							feedtags[i].tag = null;
						}
						feedtags[i] = null;
					}
				}
				feedtags = null;
			}
		}
	}
	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		private HashMap<String, OurMsgAccum> rdraccum;
		private int[]	accum;

		private int tower_count;
		private OurTowerState tower;
		private boolean initdone;
		private boolean new10min;
		private boolean newhourly;

		/* Message state info - used to detect 10-minute and hour boundaries */
		private int last_phase_index;	/* Index reported by last phase msg */
		private int last_phase_id;		/* Phase ID reported by last phase msg */
		private int last_outlet_index;	/* Last outlet index reported (pwruse msg) */
		private int last_outletamps_index;	/* Last outlet index (amps message) */
		private int msg_cnt;
		private boolean did_low_batt;	/* If we saw low battery during hour */

		long	watthours_ts;	/* Start time of watthours accumulator */
		double base_watthours;	/* total at start time watthours */

		private LinkedList<OurOutletState> rebooted_outlets;

		private static final int OUTLET_WATTHOURS_MAX = 0xFF;
		private static final int OUTLET_AMPS_NA = 0x1FF;
		private static final int PHASE_VOLTS_NA = 0x3FF;
		private static final int PHASE_AMPS_NA = 0x3FF;
		private static final int PHASE_WATTS_NA = 0x1FFF;
		private static final int FEED_AMPS_NA = 0x3FF;
		private static final int DEVICE_WATTS_NA = 0x1FFF;
		private static final int DEVICE_VA_NA = 0x1FFF;
		private static final int DEVICE_KWH_NA = 0xFFFF;

		public OurTagState(MantisIITag tag) {
			tower_count = 1;	/* Default to 1 tower */
			resetDeviceConfiguration(tag, false);
			msg_cnt = -1;
			rdraccum = new HashMap<String, OurMsgAccum>();
			accum = null;
		}
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t) {
			cleanupTower();
		}

		/**
		 * Clean up tower
		 */
		private void cleanupTower() {
			if(tower != null) {
				tower.cleanupPhases();		/* Cleanup phases */
				tower.cleanupOutlets();		/* Cleanup outlets */
				tower.cleanupFeedLines();		/* Cleanup feed lines */
				tower = null;
			}
			initdone = false;
			new10min = false;
		}
		/**
		 * Receive message byte to accumulate
		 */
		public boolean accumulateByte(MantisIITag tag, byte data, long ts, boolean first, MantisIIReader rdr) {
			boolean change = false;

			if(rdr == null) return change;
			/* Find and/or init accumulator */
			OurMsgAccum a = rdraccum.get(rdr.getID());
			if(a == null) {
				a = new OurMsgAccum();
				rdraccum.put(rdr.getID(), a);
			}			
			if(first) {	/* If first one */
				a.last_ts = ts;		/* Save ts */
				a.num_accum = 1;
				a.accum[0] = 0xFF & (int)data;
			}
			else {
				long last_int = ts - a.last_ts;

				/* If received more than N times 110% of interval + 60% of interval after start packet */
//				if(last_int > ((11*a.num_accum + 6)*DEF_PKT_INTERVAL/10)) {
				/* If more than 4 seconds early or 4 seconds late, its bad */
				if( (last_int > ((10*a.num_accum + 4)*DEF_PKT_INTERVAL/10)) ||
					(last_int < ((10*a.num_accum - 4)*DEF_PKT_INTERVAL/10))) {
					/* Missing packet, so reset accumulator */
					a.num_accum = 0;
				}
				else if(a.num_accum == 0) {	/* No start, just skip it */
				}
				else {						/* Else, add to accumulator */
					a.accum[a.num_accum] = 0xFF & (int)data;	/* Add it */
					a.num_accum++;
					if(a.num_accum == 6) {	/* Got all of them? */
						/* Check parity count */
						int cnt = 0;
						for(int i = 3; i < 48; i++) {
							if((a.accum[i/8] & (0x80>>(i%8))) != 0) {
								cnt++;
							}
						}
						if((cnt % 4) == (a.accum[0]>>5)) {	/* If message checks */
							a.accum[0] = (a.accum[0] & 0x1F);		/* Trim off parity */
							/* Now, see if its the same message from a different reader than the last message - if so,
							 * its a duplicate we need to ignore */
							if((accum != a.accum) && (accum != null) && Arrays.equals(accum, a.accum)) {
								/* Skip */
							}
							else {
								accum = a.accum;		/* Point to new message, and process it */
								change = processMessage(tag) || change;
							}
						}
						else {
//							System.out.println("Checksum error " + (cnt % 4) + "!=" + (a.accum[0]>>5));
						}
						a.num_accum = 0;
					}
				}
			}
			return change;
		}
		/* Read bit range from accumulator */
		private int getBits(int start, int end) {
			int rslt = 0;
			if(start > 44) start = 44;
			if(end < 0) end = 0;
			for(int i = start; i >= end; i--) {
				rslt = rslt << 1;
				if((accum[5 - (i>>3)] & (1 << (i & 7))) != 0) {
					rslt |= 1;
				}
			}
			return rslt;		
		}
		private int getBit(int s) {
			return getBits(s, s);
		}

		/**
	     * Device configuration reset - due to model change, serial change, or other configuration inconsistency
		 */
		private boolean	resetDeviceConfiguration(MantisIITag tag, boolean change) {
			//System.out.println("resetDeviceConfiguration");
			cleanupTower();		/* Cleanup tower */
			
			tower = new OurTowerState();
			initdone = false;
			last_phase_index = -1;
			last_phase_id = -1;
			last_outlet_index = -1;
			last_outletamps_index = -1;
			watthours_ts = 0;
			base_watthours = 0.0;
			/* Clean out PDU settings */
			change = updateNull(tag, PDUMODEL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIAL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUPWRFACTOR_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIALLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERCOUNT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMSGLOSS_ATTRIB_INDEX) || change;

			return change;
		}
		/**
		 * Flush hour data, if any accumulated
		 */
		private boolean	flushHourData(MantisIITag tag, boolean change) {
			int j, k;
			change = flush10MinuteData(tag, change);
			if(!newhourly)
				return change;

//			if(is_20_2hr)
//				System.out.println(tag.getTagGUID() + ": Flush 2 hour data");
//			else
//				System.out.println(tag.getTagGUID() + ": Flush hour data");

			last_outlet_index = -1;
			last_outletamps_index = -1;
			newhourly = false;

			if(!initdone)
				return change;
			if(tower == null)
				return change;

			//System.out.println(tag.getTagGUID() + ": Flush hour data");
			
			OurTowerState tow = tower;
			Long towerid = Long.valueOf(1);
			/* Loop through banks of outlets */
			for(j = 0; j < tow.banks.length; j++) {
				OurBankState bank = tow.banks[j];
				if(bank.outlets == null) {
					//System.out.println("No outlets on bank " + j);
					continue;
				}
				Double svolts = null;
				if((bank.phidx >= 0) && (bank.phidx < tow.phasetags.length)) {
					int v;
					if(bank.isLtoL)
						v = tow.phasetags[bank.phidx].last_volts_ll;
					else
						v = tow.phasetags[bank.phidx].last_volts_ln;
					if (v != PHASE_VOLTS_NA) {
						svolts = Double.valueOf(v);
					}
				}
				//System.out.println("bank" + j + ": volts=" + svolts);
				/* Loop through outlets on bank */
				for(k = 0; k < bank.outlets.length; k++) {
					OurOutletState out = bank.outlets[k];
					Double struepwr = null;
					if(out.last_watts >= 0)
						struepwr = Double.valueOf(out.last_watts);
					Double samps = null;
					if((out.last_amps_x10 >= 0) && (out.last_amps_x10 < OUTLET_AMPS_NA))
						samps = Double.valueOf(0.1 * out.last_amps_x10);
					/* If we have volts and amps, compute apparent power */
					Double sapppwr = null;				
					Double spowfact = null;
					if((svolts != null) && (samps != null)) {
						sapppwr = Double.valueOf(Math.round(svolts.doubleValue() * samps.doubleValue()));
						/* If we have apparent power and active power, computer power factor */
						if((sapppwr != null) && (sapppwr.doubleValue() > 0.0) && (struepwr != null)) {
							double pf = Math.round(100.0 * struepwr.doubleValue() / 
									sapppwr.doubleValue());
							if(pf < 0.0) pf = 0.0;
							if(pf > 100.0) pf = 100.0;
							spowfact = Double.valueOf(pf);
						}
					}
					String cfg = out.phid;
					Double wh = null;
					Long	wh_ts = null;
					Double vah = null;
					wh = (out.total_watthours >= 0)?Double.valueOf(out.total_watthours):null;
					wh_ts = (out.watthours_ts > 0)?Long.valueOf(out.watthours_ts):null;
					/* If watt-hours added, accumulate with VA-hours if PF is available */
					if(out.last_diff > 0.0) {
						double pf = (spowfact != null)?spowfact.doubleValue():0.0;
						if(pf > 0.0) {
							if(pf > 100.0)
								out.total_vahours = out.total_vahours + out.last_diff;
							else
								out.total_vahours = out.total_vahours + (0.1 * Math.round(100.0 * 10.0 * out.last_diff / pf));
						}
						out.last_diff = 0.0;
					}
					vah = (out.total_vahours >= 0)?Double.valueOf(out.total_vahours):null;
					/* Get switch state, if supported */
					Boolean sw = null;
					if(tow.switched_outlets) {
						if(out.sw_signalled) {
							sw = Boolean.valueOf(out.switch_state == SWITCH_ON);
						}
					}
					out.sw_signalled = false;
					//System.out.println("updateOutlet " + out.label + ",amps=" + out.last_amps_x10 + "(" + samps + ")");
					PduOutletTagSubType.updateTag(out.tag, 
						(svolts!=null)?svolts:PduOutletTagSubType.VALUE_NA,
						(struepwr!=null)?struepwr:PduOutletTagSubType.VALUE_NA,
						(spowfact!=null)?spowfact:PduOutletTagSubType.VALUE_NA,
						(sapppwr!=null)?sapppwr:PduOutletTagSubType.VALUE_NA,
						(samps!=null)?samps:PduOutletTagSubType.VALUE_NA,
						cfg, 
						(wh!=null)?wh:PduOutletTagSubType.VALUE_NA,
						(wh_ts!=null)?wh_ts:PduOutletTagSubType.TS_VALUE_NA,
						(vah!=null)?vah:PduOutletTagSubType.VALUE_NA,
						towerid, out.label, out.feedsetid, sw);
				}
				/* Update bank */
				Boolean overload = null;
				if (tower.bank_monitored) {
					if (!bank.ol_signalled) {
						overload = bank.overload;
                    }
				}
				PduBreakerTagSubType.updateTag(bank.tag, null, null, null, null, null, null, overload);
                bank.ol_signalled = false;
			}
			/* Loop through feed lines */
			if(tow.feedtags != null) {
				for(int i = 0; i < tow.feedtags.length; i++) {
					Double amps = null;
					if((tow.feedtags[i].last_amps_x10 >= 0) && (tow.feedtags[i].last_amps_x10 < FEED_AMPS_NA))
						amps = Double.valueOf(0.1 * tow.feedtags[i].last_amps_x10);
					String config = tow.feedtags[i].feedid;

					PduFeedLineTagSubType.updateTag(tow.feedtags[i].tag, 
							(amps!=null)?amps:PduFeedLineTagSubType.VALUE_NA,
									config, towerid, null, null, tow.feedtags[i].feedsetid);
				}
			}
			/* Compute new message loss rate */
			if(msg_cnt >= 0) {	/* 60 messages per hour nominal */
				double v;
				if(did_low_batt)			/* If low battery signalled? */
					v = Math.round(100.0 - (msg_cnt/0.51));	/* 51 msgs per hour */
				else
					v = Math.round(100.0 - (msg_cnt/0.60));	/* 60 msgs per hour */
				if(v < 0.0) v = 0.0;
	            change = updateDouble(tag, PDUMSGLOSS_ATTRIB_INDEX, v) || change;
//				System.out.println(tag.getTagGUID() + ": msg_cnt=" + msg_cnt + ", loss=" + v);
			}
			double tot_wh = tower.tot_watthours;
			/* If total watt-hours is defined */
			if(tot_wh >= 0.0) {
				if(watthours_ts == 0) {	/* First time? */
					watthours_ts = System.currentTimeMillis();
					base_watthours = tot_wh;
				}
				change = updateDouble(tag, PDUWATTHOURS_ATTRIB_INDEX, 
						Double.valueOf(tot_wh - base_watthours)) || change;
				change = updateLong(tag, PDUWATTHOURSTS_ATTRIB_INDEX, 
						Long.valueOf(watthours_ts)) || change;
				change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
			}
			else {
				watthours_ts = 0;
				base_watthours = 0.0;
				change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
				change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
				change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			}

			/* Clear message count */
			msg_cnt = 0;
			did_low_batt = false;	/* Reset flag */

			return change;
		}
		/**
		 * Flush 10 minute data, if any accumulated
		 */
		private boolean	flush10MinuteData(MantisIITag tag, boolean change) {
			if(!new10min)
				return change;

			new10min = false;

			if(!initdone)
				return change;
			if(tower == null)
				return change;

			int i;
			double tot_tp = -1.0;
			double tot_ap = -1.0;
			
			if(tower.last_wattsx10 != DEVICE_WATTS_NA)
				tot_tp = 10.0 * tower.last_wattsx10;
			if(tower.last_voltampsx10 != DEVICE_VA_NA)
				tot_ap = 10.0 * tower.last_voltampsx10;

			OurTowerState tow = tower;
			Long towerid = Long.valueOf(1);
			/* Loop through phases */
			for(i = 0; i < tow.phasetags.length; i++) {
				Double volts = null;
				Double vn = null;
				Double vp = null;
				if((tow.phasetags[i].last_volts_ln >= 0) && (tow.phasetags[i].last_volts_ln < PHASE_VOLTS_NA)) {
					vn = Double.valueOf(tow.phasetags[i].last_volts_ln); 
				}
				if((tow.phasetags[i].last_volts_ll >= 0) && (tow.phasetags[i].last_volts_ll < PHASE_VOLTS_NA)) {
					vp = Double.valueOf(tow.phasetags[i].last_volts_ll); 
				}
				if((tow.phasetags[i].last_volts >= 0) && (tow.phasetags[i].last_volts < PHASE_VOLTS_NA)) {
					volts = Double.valueOf(tow.phasetags[i].last_volts); 
				}
				if(volts != null) {
					if(tower.phase_cfg == PhaseConfig.THREE_PHASE_DELTA)
						vp = volts;
					else
						vn = volts;
				}
				Double amps = null;
				if((tow.phasetags[i].last_ampsx10 >= 0) && (tow.phasetags[i].last_ampsx10 < PHASE_AMPS_NA))
					amps = Double.valueOf(0.1 * tow.phasetags[i].last_ampsx10);
				Double tp = null;
				if((tow.phasetags[i].last_wattsx10 >= 0) && (tow.phasetags[i].last_wattsx10 < PHASE_WATTS_NA))
					tp = Double.valueOf(10 * tow.phasetags[i].last_wattsx10);
				/* Now, if we have amps and volts, make apparent power */
				Double ap = null;
				Double pf = null;
				if((volts != null) && (amps != null)) {
					ap = Double.valueOf(Math.round(volts.doubleValue() * amps.doubleValue()));
					/* If we have apparent power and active power, computer power factor */
					if((ap != null) && (ap.doubleValue() > 0.0) && (tp != null)) {
						pf = Double.valueOf(Math.round(100.0 * tp.doubleValue() / ap.doubleValue()));
						if(pf.doubleValue() > 100.0) pf = Double.valueOf(100.0);
					}

				}
				String cfg = tow.phasetags[i].phid;
				/* Update phase tag */
				PduPhaseTagSubType.updateTag(tow.phasetags[i].tag, 
						(vn!=null)?vn:PduPhaseTagSubType.VALUE_NA,
						(vp!=null)?vp:PduPhaseTagSubType.VALUE_NA,  
						(tp!=null)?tp:PduPhaseTagSubType.VALUE_NA, 
						(pf!=null)?pf:PduPhaseTagSubType.VALUE_NA, 
						(ap!=null)?ap:PduPhaseTagSubType.VALUE_NA, 
						(amps!=null)?amps:PduPhaseTagSubType.VALUE_NA, 
						null, null, null, cfg, towerid, tow.phasetags[i].feedsetid);
			}
			/* Loop through feed lines for load warning */
			if(tow.feedtags != null) {
				for(i = 0; i < tow.feedtags.length; i++) {
					Boolean overload = null;
                    Boolean loadwarn = null;
                    if(!tow.feedtags[i].was_signalled) {
                        overload = loadwarn = Boolean.valueOf(tow.feedtags[i].overload);
                    }
					PduFeedLineTagSubType.updateTag(tow.feedtags[i].tag, 
							null, null, null, overload, loadwarn, null);
					tow.feedtags[i].was_signalled = false;	/* Clear triggered flag */
				}
			}
			/* Update top-level tag data, if we can */
			if(tot_tp < 0.0)
				change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			else
				change = updateDouble(tag, PDUTRUEPOWER_ATTRIB_INDEX, Double.valueOf(tot_tp)) || change;
			if(tot_ap < 0.0)
				change = updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX) || change;
			else
				change = updateDouble(tag, PDUAPPPOWER_ATTRIB_INDEX, Double.valueOf(tot_ap)) || change;
			if((tot_ap > 0.0) && (tot_tp >= 0.0)) {	/* If both defined, and AP > 0, do PF */
				double pf = Math.round(100.0 * tot_tp / tot_ap);
				if(pf < 0.0) pf = 0.0;
				if(pf > 100.0) pf = 100.0;
				change = updateDouble(tag, PDUPWRFACTOR_ATTRIB_INDEX, pf) || change;
			}
			else {
				change = updateNull(tag, PDUPWRFACTOR_ATTRIB_INDEX) || change;
			}
			return change;
		}

		/**
		 * Initialize device if needed, and we have full set of configuration data
		 */
		private void	initDeviceConfiguration(MantisIITag tag) {
			int i;
			TagSubGroup tg;
			if(initdone)
				return;
			if(tower == null) {
				return;
			}
			if((tower.model == null) || (tower.serial == null) || 
				(tower.phase_cfg == PhaseConfig.N_A)) {
				return;
			}
			if(tower.banks == null) {
				return;
			}
			for(i = 0; i < tower.banks.length; i++) {
				if(tower.banks[i] == null)
					return;
			}
			OurTowerState ts = tower;
			String id_suffix = "";
			Long towerid = Long.valueOf(1);
			Long feedsetid = Long.valueOf(1);
			/* Initialize phases */
			int pcnt = 1;
			String[] phid = { "L1-N" };
			String[] flid = { "L1" };
			switch(ts.phase_cfg) {
				case SINGLE_PHASE:
					break;
				case THREE_PHASE_WYE:
					pcnt = 3;
					phid = new String[]{ "L1-N", "L2-N", "L3-N" };
					flid = new String[]{ "L1", "L2", "L3", "N" };
					break;
				case THREE_PHASE_DELTA:
				case THREE_PHASE_DELTA_4WIRE:
					pcnt = 3;
					phid = new String[]{ "L1-L2", "L2-L3", "L3-L1" };
					flid = new String[]{ "L1", "L2", "L3" };
					break;
				default:
					break;
			}
			ts.phasetags = new OurPhaseState[pcnt];
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
			for(i = 0; i < ts.phasetags.length; i++) {
				ts.phasetags[i] = new OurPhaseState();
				ts.phasetags[i].phid = phid[i];
				ts.phasetags[i].feedsetid = feedsetid;
				ts.phasetags[i].tag = SubTag.findOrCreateTag(tg, tag, 
						String.format("phase%d%s",i+1, id_suffix), false,
						new int[] { PduPhaseTagSubType.PDUCONFIG_INDEX, 
					PduPhaseTagSubType.PDUFEEDSETID_INDEX,
					PduPhaseTagSubType.PDUTOWERID_INDEX },
					new Object[] { ts.phasetags[i].phid, ts.phasetags[i].feedsetid, towerid } );
				ts.phasetags[i].tag.lockUnlockTag(true);
				//					PduPhaseTagSubType.updateTag(ts.phasetags[i].tag, null, null, null, null, null, null,
				//					null, null, null, ts.phasetags[i].phid, towerid, ts.phasetags[i].feedsetid);
			}
			/* Initialize feed lines */
			ts.feedtags = new OurFeedLineState[flid.length];
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduFeedLineTagSubType.TAGTYPEID);
			for(i = 0; i < ts.feedtags.length; i++) {
				ts.feedtags[i] = new OurFeedLineState();
				ts.feedtags[i].feedid = flid[i];
				ts.feedtags[i].feedsetid = feedsetid;
				ts.feedtags[i].tag = SubTag.findOrCreateTag(tg, tag,
					String.format("feedline%s%s", flid[i], id_suffix), false,
					new int[] { PduFeedLineTagSubType.PDUCONFIG_INDEX,
						PduFeedLineTagSubType.PDUFEEDSETID_INDEX,
						PduFeedLineTagSubType.PDUTOWERID_INDEX },
					new Object[] { ts.feedtags[i].feedid, ts.feedtags[i].feedsetid, towerid } );
				ts.feedtags[i].tag.lockUnlockTag(true);
			}
			/* Initialize banks */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduBreakerTagSubType.TAGTYPEID);
			for (i = 0; i < tower.banks.length; i++) {
				OurBankState bs = tower.banks[i];
				bs.tag = SubTag.findOrCreateTag(tg, tag, 
					String.format("bank%s", bs.bank_id), false,
					new int[] { PduBreakerTagSubType.PDUCONFIG_INDEX, 
						PduBreakerTagSubType.PDUFEEDSETID_INDEX,
						PduBreakerTagSubType.PDUTOWERID_INDEX,
						PduBreakerTagSubType.PDUBANKID_INDEX },
					new Object[] { bs.phid, feedsetid, towerid, i+1 });
				bs.tag.lockUnlockTag(true);
			}
			/* Initialize outlets */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
			for (i = 0; i < tower.banks.length; i++) {
				OurBankState bs = tower.banks[i];
				if (bs.outlets == null) continue;
				for(int j = 0; j < bs.outlets.length; j++) {
					OurOutletState os = new OurOutletState();
					bs.outlets[j] = os;
					os.phid = bs.phid;
					os.feedsetid = feedsetid;
					os.label = bs.bank_id + "-" + (j + 1);
					os.tag = SubTag.findOrCreateTag(tg, tag, 
						String.format("outlet%s%02d", bs.bank_id, j+1), false,
						new int[] { PduOutletTagSubType.PDUCONFIG_INDEX, 
							PduOutletTagSubType.PDUFEEDSETID_INDEX,
							PduOutletTagSubType.PDUTOWERID_INDEX,
							PduOutletTagSubType.PDULABEL_INDEX,
							PduOutletTagSubType.PDUBANKID_INDEX },
						new Object[] { os.phid, os.feedsetid, towerid, os.label, i+1 });
					os.tag.lockUnlockTag(true);
				}
			}
			initdone = true;
		}
		/* Handle per phase message */
		private boolean	handlePerPhaseMessage(MantisIITag tag, boolean change) {
			if (tower == null)
				return change;
			OurTowerState tow = tower;
			int phid = getBits(41,40);
			if((tow.phasetags == null) || (phid >= tow.phasetags.length))
				return change;
			int index = getBits(39, 37);
			//System.out.println("PerPhase:" + phid + ":index=" + index);
			/* See if we're due to flush hour */
			if((last_phase_index != 0) && (index == 0)) {	/* if first zero index */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index > index) {			/* We missed zero */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index != index) {		/* If new one */
				change = flush10MinuteData(tag, change);
			}
			else if(last_phase_id >= phid) {
				change = flush10MinuteData(tag, change);
			}
			last_phase_index = index;
			last_phase_id = phid;

			if(!initdone)
				return change;

			/* Update values */
			tow.phasetags[phid].last_ampsx10 = getBits(26, 17);
			tow.phasetags[phid].last_volts = getBits(35, 27);
			tow.phasetags[phid].last_wattsx10 = getBits(16,4);
			if((tow.phase_cfg == PhaseConfig.THREE_PHASE_DELTA) || (tow.phase_cfg == PhaseConfig.THREE_PHASE_DELTA_4WIRE)) {
				tow.phasetags[phid].last_volts_ll = tow.phasetags[phid].last_volts;
			}
			else {
				tow.phasetags[phid].last_volts_ln = tow.phasetags[phid].last_volts;
			}

			/* Update load warning on feed line, if defined */
			if((tow.feedtags != null) && (tow.feedtags.length > phid) && (tow.feedtags[phid] != null)) {
				tow.feedtags[phid].overload = (getBit(36) == 1);
			}
			new10min = newhourly = true;

			return change;
		}
		/* Handle phase voltages (Lx-N), bank overload */
		private boolean handlePhaseL2NMessage(MantisIITag tag, boolean change) {
			if((tower == null) || (tower.phasetags == null) || (tower.banks == null)) 
				return change;			
			switch(tower.phase_cfg) {
				case SINGLE_PHASE:
					tower.phasetags[0].last_volts_ln = getBits(39,31);
					break;
				case THREE_PHASE_WYE:
				case THREE_PHASE_DELTA:
				case THREE_PHASE_DELTA_4WIRE:
					tower.phasetags[0].last_volts_ln = getBits(39,31);
					tower.phasetags[1].last_volts_ln = getBits(30,22);
					tower.phasetags[2].last_volts_ln = getBits(21,13);
					break;
				default:	
					break;
			}
			for(int i = 0; i < tower.banks.length; i++) {
				if(!tower.banks[i].ol_signalled) {
					tower.banks[i].overload = (getBit(12-i) == 1);
					tower.banks[i].ol_signalled = true;
				}
			}
			newhourly = true;
			
			return change;
		}
		/* Handle phase voltages (Lx-L) */
		private boolean handlePhaseL2LMessage(MantisIITag tag, boolean change) {
			if((tower == null) || (tower.phasetags == null) || (tower.banks == null)) 
				return change;			
			switch(tower.phase_cfg) {
				case SINGLE_PHASE:
					tower.phasetags[0].last_volts_ll = getBits(39,31);
					break;
				case THREE_PHASE_WYE:
				case THREE_PHASE_DELTA:
				case THREE_PHASE_DELTA_4WIRE:
					tower.phasetags[0].last_volts_ll = getBits(39,31);
					tower.phasetags[1].last_volts_ll = getBits(30,22);
					tower.phasetags[2].last_volts_ll = getBits(21,13);
					break;
				default:	
					break;
			}
			for(int i = 0; i < tower.banks.length; i++) {
				if(!tower.banks[i].ol_signalled) {
					tower.banks[i].overload = (getBit(12-i) == 1);
					tower.banks[i].ol_signalled = true;
				}
			}

			newhourly = true;
			
			return change;
		}

		/* Handle PDU model/serial messages */
		private boolean	handlePDUSerialMessage(MantisIITag tag, boolean change) {
			int idx = getBit(38);
			int i;
			/* If second index, check for bank count, feed config, etc */
			if(idx == 1) {
                int newbankcnt = getBits(15, 12);
                if(newbankcnt > 12) newbankcnt = 0;

				/* Check for phase configuration */
				PhaseConfig newcfg = PhaseConfig.N_A;
				switch(getBits(11, 9)) {
					case 0:
						newcfg = PhaseConfig.NONE;
						break;
					case 1:
						newcfg = PhaseConfig.SINGLE_PHASE;
						break;
					case 2:
						newcfg = PhaseConfig.THREE_PHASE_WYE;
						break;
					case 3:
						newcfg = PhaseConfig.THREE_PHASE_DELTA;
						break;
					case 4:
						newcfg = PhaseConfig.THREE_PHASE_DELTA_4WIRE;
						break;
					default:
						newcfg = PhaseConfig.N_A;
						break;
				}
				/* If mismatch on phase config or bank count, reset device */
				if((tower.phase_cfg != newcfg) || (tower.banks == null) || (tower.banks.length != newbankcnt)) {
					if((tower.phase_cfg != PhaseConfig.N_A) || (tower.banks != null)) {
						change = resetDeviceConfiguration(tag, change);
					}
					tower.phase_cfg = newcfg;
					tower.banks = new OurBankState[newbankcnt];
				}
				/* Check for switched outlet support */
				tower.switched_outlets = (getBit(8) == 1);
				/* Check if banks monitoed */
				tower.bank_monitored = (getBit(6) == 1);
				//System.out.println("switched=" + tower.switched_outlets + ", outlet_power=" + tower.per_outlet_cumulative + ", banks_monitored=" + tower.bank_monitored +", bank_cnt=" + tower.banks.length);
				if(tower.serial_sb.length() < 12) {
					tower.serial_sb.setLength(12);
				}
				tower.serial_sb.setCharAt(7, (char)(0x20 + getBits(37, 32)));
				tower.serial_sb.setCharAt(8, (char)(0x30 + getBits(31, 28)));
				tower.serial_sb.setCharAt(9, (char)(0x30 + getBits(27, 24)));
				tower.serial_sb.setCharAt(10, (char)(0x30 + getBits(23, 20)));
				tower.serial_sb.setCharAt(11, (char)(0x30 + getBits(19, 16)));
				/* We've got no gaps, process serial number */
				if(tower.serial_sb.indexOf("\0") < 0) {	/* No nulls left? */
					String newserial = tower.serial_sb.toString().trim();	/* Make trimmed number */
					/* If different from old one, reset configuration */
					if((tower.serial != null) && (!tower.serial.equals(newserial))) {
						change = resetDeviceConfiguration(tag, change);
						tower.serial = newserial;
					}
					else if(tower.serial == null) {	/* If no old, don't reset */
						tower.serial = newserial;
					}
		            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX, tower.serial)
						|| change;
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					for(i = 0; i < tower_count; i++) {
						if(tower.serial != null) {
							sn_lst.add(tower.serial);
						}
						else {
							sn_lst.add("");
						}
					}
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) 
						|| change;
					tower.serial_sb.setLength(0);	/* Reset accumulator */
					/* Initialize device configuration, if needed and ready */
					initDeviceConfiguration(tag);
				}
			}
			else if(idx == 0) {
				if(tower.serial_sb.length() < 7) {
					tower.serial_sb.setLength(7);
				}
				tower.serial_sb.setCharAt(0, (char)(0x20 + getBits(37, 32)));
				tower.serial_sb.setCharAt(1, (char)(0x20 + getBits(31, 26)));
				tower.serial_sb.setCharAt(2, (char)(0x30 + getBits(25, 22)));
				tower.serial_sb.setCharAt(3, (char)(0x30 + getBits(21, 18)));
				tower.serial_sb.setCharAt(4, (char)(0x30 + getBits(17, 14)));
				tower.serial_sb.setCharAt(5, (char)(0x30 + getBits(13, 10)));
				tower.serial_sb.setCharAt(6, (char)(0x20 + getBits(9, 4)));
			}
			return change;
		}
		/* Handle PDU model message */
		private boolean handlePDUModelMessage(MantisIITag tag, boolean change) {
			int i;
			int idx = getBits(39,38);

			/* Update buffer length, if needed */
			if(tower.model_sb.length() < ((idx+1)*6)) {
				tower.model_sb.setLength((idx + 1) * 6);
			}
			/* Accumulate model number characters */
			for(i = 0; i < 6; i++) {
				tower.model_sb.setCharAt((6 * idx) + i, (char)(0x20 + getBits(37 - (6 * i), 32 - (6 * i))));
			}
			/* If end, and no more gaps, process model number */
			if((getBit(0) != 0) && (tower.model_sb.indexOf("\0") < 0)) {
				String nmodel = tower.model_sb.toString().trim();
				/* If mismatch, reset configuration */
				if((tower.model != null) && (!tower.model.equals(nmodel))) {
					change = resetDeviceConfiguration(tag, change);
				}
				else if(tower.model == null) {
					tower.model = nmodel;
				}
	            change = updateString(tag, PDUMODEL_ATTRIB_INDEX, tower.model) || change;
				/* Update model number list */
				List<String> mn_lst = new ArrayList<String>();
				mn_lst.add(tower.model);
	            change = updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, mn_lst) || change;
				/* Clear accumulator */
				tower.model_sb.setLength(0);
				/* Initialize device configuration, if needed and ready */
				initDeviceConfiguration(tag);
			}
			return change;
		}
		/* Handle PDU bank configuration message */
		private boolean handlePDUBankMessage(MantisIITag tag, boolean change) {
			int i;
			int idx = getBits(39,38);

			if(tower.banks == null) {
				return change;
			}
			for (i = 0; i < 4; i++) {
				int bankid = (idx * 4) + i;
				if(bankid >= tower.banks.length) continue;
				String phid = "N/A";
				int phidx = -1;
				boolean isLtoL = false;
				int phcfg = getBits(37 - (i * 3), 35 - (i * 3));
				switch(phcfg) {
					case 0:
						phid = "L1-N";
						isLtoL = false;
						phidx = 0;
						break;
					case 1:
						phid = "L2-N";
						isLtoL = false;
						phidx = 1;
						break;
					case 2:
						phid = "L3-N";
						isLtoL = false;
						phidx = 2;
						break;
					case 3:
						phid = "L1-L2";
						isLtoL = true;
						phidx = 0;
						break;
					case 4:
						phid = "L2-L3";
						isLtoL = true;
						phidx = 1;
						break;
					case 5:
						phid = "L3-L1";
						isLtoL = true;
						phidx = 2;
						break;
				}
				OurBankState bank = tower.banks[bankid];
				int outcnt = 1 + getBits(25 - (5 * i), 21 - (5 * i));
				if((bank == null) || (phidx != bank.phidx) || (isLtoL != bank.isLtoL) || (bank.outlets.length != outcnt)) {
					if(bank != null) {
						change = resetDeviceConfiguration(tag, change);
						return change;
					}
					else {
						tower.banks[bankid] = new OurBankState(Character.toString((char)('A' + bankid)));
						tower.banks[bankid].phid = phid;
						tower.banks[bankid].phidx = phidx;
						tower.banks[bankid].isLtoL = isLtoL;
						tower.banks[bankid].outlets = new OurOutletState[outcnt];
					}
				}
			}
			/* If all banks prepped, see if we can initialize */
			boolean done = true;
			for(i = 0; i < tower.banks.length; i++) {
				if(tower.banks[i] == null) done = false;
			}
			if(done) {
				/* Initialize device configuration, if needed and ready */
				initDeviceConfiguration(tag);
			}
			return change;
		}

		/* Handle per-outlet power use message */
		private boolean handlePerOutletPowerUseMessage(MantisIITag tag, boolean change) {
			int i;
			int bankid = getBits(42,39);
			int oindex = getBits(38,36);
			int oind = oindex + (bankid * 100);	/* Make global-level index */
			/* If this is below last one, flush hour */
			if(oind < last_outlet_index) {
				change = flushHourData(tag, change);
			}				
			last_outlet_index = oind;

			if(!initdone)
				return change;
			OurTowerState tow = tower;
			if((tow.banks == null) || (tow.banks.length <= bankid)) {
				return change;
			}
			OurBankState bank = tow.banks[bankid];
			int obase = (4*oindex);
			if((bank.outlets == null) || (obase >= bank.outlets.length)) {
				return change;
			}

			long ts = System.currentTimeMillis();	/* Get time in milliseconds */
			for(i = 0; i < 4; i++) {
				if((obase + i) < bank.outlets.length) {
					int w_hr = getBits(31 - (8 * i), 24 - (8 * i));
					//System.out.println("outlet " + (obase+i) + ": w_h=" + w_hr + ", sw=" + getBit(35 - i));
					int diff = 0;
					if(w_hr < OUTLET_WATTHOURS_MAX) {	/* If valid watthours value */
						w_hr = w_hr * 100;  // Make to watt-hours
						if(bank.outlets[obase+i].last_watthours >= 0) {	/* If we had old value */
							long elapsed = ts - bank.outlets[obase+i].last_watthours_ts;
							if(elapsed <= 0) elapsed = 3600000;	/* Assume 1 hour if bad */
							diff = (w_hr + OUTLET_WATTHOURS_MAX - bank.outlets[obase+i].last_watthours) %
								OUTLET_WATTHOURS_MAX;
							/* Adjust for period between readings */
							bank.outlets[obase+i].last_watts = Math.round(diff * 3600000.0 / (double)elapsed);
						}
						bank.outlets[obase+i].last_watthours = w_hr;
						bank.outlets[obase+i].last_watthours_ts = ts;
	
						if(bank.outlets[obase+i].total_watthours < 0.0) {	/* If no accumulator, start one */
							bank.outlets[obase+i].total_watthours = 0.0;
							bank.outlets[obase+i].total_vahours = 0.0;
							bank.outlets[obase+i].watthours_ts = ts;		/* Save time reference */
							bank.outlets[obase+i].last_diff = 0.0;
						}
						else {
							bank.outlets[obase+i].total_watthours += diff;
							bank.outlets[obase+i].last_diff = diff;	/* Save diff - accumulate VA-hrs once we have PF */
						}
					}
					else {
						bank.outlets[obase+i].last_watthours = -1;
						bank.outlets[obase+i].last_watthours_ts = -1;
						bank.outlets[obase+i].last_watts = -1.0;
						bank.outlets[obase+i].total_watthours = -1.0;
						bank.outlets[obase+i].total_vahours = -1.0;
						bank.outlets[obase+i].last_diff = 0.0;
						bank.outlets[obase+i].watthours_ts = 0;
					}
					if(tow.switched_outlets) {
						if(bank.outlets[obase+i].sw_signalled) {	/* Already updated by alarm signal */
							/* Do nothing - already better value */
						}
						else {
							bank.outlets[obase+i].switch_state = ((getBit(35 - i) == 1)?SWITCH_OFF:SWITCH_ON);
							bank.outlets[obase+i].sw_signalled = true;
						}
					}
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle per-outlet current message */
		private boolean handlePerOutletCurrentMessage(MantisIITag tag, boolean change) {
			int i;
			int bankid = getBits(42,39);
			int oindex = getBits(38,36);
			int oind = oindex + (bankid * 100);	/* Make global-level index */
			/* If this is below last one, flush hour */
			if(oind < last_outletamps_index) {
				change = flushHourData(tag, change);
			}				
			last_outletamps_index = oind;

			if(!initdone)
				return change;
			OurTowerState tow = tower;
			if((tow.banks == null) || (bankid >= tow.banks.length)) {
				return change;
			}
			OurBankState bank = tow.banks[bankid];
			int obase = 4*oindex;
			if((bank.outlets == null) || (obase >= bank.outlets.length)) {
				return change;
			}

			for(i = 0; i < 4; i++) {
				if((obase + i) < bank.outlets.length) {
					bank.outlets[obase + i].last_amps_x10 = getBits(35 - (9 * i), 27 - (9 * i));
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle feed device power/energy message */
		private boolean handleDevicePowerEnergyMessage(MantisIITag tag, boolean change) {
			if(tower == null)
				return change;
			OurTowerState tow = tower;
			if(!initdone)
				return change;
			tow.last_wattsx10 = getBits(41, 29);
			tow.last_voltampsx10 = getBits(28, 16);
			int kwhrs = getBits(15, 0);
			
			if(kwhrs == OurTagState.DEVICE_KWH_NA) {	/* N/A? */
				tow.tot_watthours = 0.0;
			}
			else if(tow.last_watthoursx10 != OurTagState.DEVICE_KWH_NA) {
				int delta = kwhrs - tow.last_watthoursx10;
				if(delta < 0) delta += OurTagState.DEVICE_KWH_NA;
				tow.tot_watthours += 10 * delta;
			}
			tow.last_watthoursx10 = kwhrs;

			newhourly = true;

			return change;
		}
		/* Process valid message buffer */
		private boolean processMessage(MantisIITag tag) {
			boolean change = false;
			String mtype = null;

			/* If we've hit first hour flush, count messages */
			if(msg_cnt >= 0)
				msg_cnt++;


			if(getBits(44, 42) == 0x04) {	/* If 44-42 = 1-0.0, Per Phase Voltage */
				change = handlePerPhaseMessage(tag, change);
				mtype = "Per-Phase Power:" + getBits(41, 40);
			}
			/* If 44-39 = 000000, PDU serial */

			else if(getBits(44,39) == 0x00) {
				change = handlePDUSerialMessage(tag, change);
				mtype = "Serial:" + getBit(38);
			}
			/* If 44-38 = 00001, PDU model */
			else if(getBits(44,40) == 0x01) {
				change = handlePDUModelMessage(tag, change);
				mtype = "Model:" + getBits(39,38);
			}
			/* If 44-38 = 00010, PDU bank config */
			else if(getBits(44,40) == 0x02) {
				change = handlePDUBankMessage(tag, change);
				mtype = "Bank:" + getBits(39,38);
			}
			/* If 44-42 = 101, device power/energy */
			else if(getBits(44,42) == 0x05) {
				change = handleDevicePowerEnergyMessage(tag, change);
				mtype = "DevicePowerEnergy";
			}
			/* If 44-40=00110, Phase Lx-N outlet info*/
			else if(getBits(44,40) == 0x06) {
				change = handlePhaseL2NMessage(tag, change);
				mtype = "PhaseVoltsL2N";
			}
			/* If 44-40=00111, Phase Lx-Lx+1 outlet info*/
			else if(getBits(44,40) == 0x07) {
				change = handlePhaseL2LMessage(tag, change);
				mtype = "PhaseVoltsL2L";
			}
			/* If 44-43=11, outlet current */
			else if(getBits(44,43) == 0x3) {
				change = handlePerOutletCurrentMessage(tag, change);
				mtype = "OutletCurrent:" + getBits(42,39) + ":" + getBits(38, 36);
			}
			/* If 44-43=01, outlet power */
			else if(getBits(44,43) == 0x01) {
				change = handlePerOutletPowerUseMessage(tag, change);
				mtype = "OutletPower:" + getBits(42,39) + ":" + getBits(38,36); 
			}
			/* If 44-42 = 101, phase cumulative energy */
//			else if(getBits(44,42) == 0x05) {
//				boolean[] lineover = new boolean[3];
//				boolean[] brkstat = new boolean[3];
//				int[] kwatthours_x10 = new int[3];
//				for(i = 0; i < 3; i++) {
//					lineover[i] = (getBit(40-i) == 1);
//					brkstat[i] = (getBit(37-i) == 1);
//					kwatthours_x10[i] = getBits(32-(11*i),22-(11*i));
//				}
//				change = handlePerPhaseEnergyMessage(tag, change, getBit(41), lineover,
//					brkstat, kwatthours_x10);
//				mtype = "Phase-Energy:" + getBit(41);
//			}
//			/* If 44-43 = 0-1, per outlet */
//			else if(getBits(44,43) == 0x01) {
//				boolean[] vavail = new boolean[2];
//				boolean[] swoff = new boolean[2];
//				int[] w_h = new int[2];
//				for(i = 0; i < 2; i++) {
//					vavail[i] = (getBit(35-i) == 1);
//					w_h[i] = getBits(31-(16*i), 16-(16*i));
//					swoff[i] = (getBit(33-i) == 1);
//				}
//				change = handlePerOutletPowerUseMessage(tag, change, getBit(42), getBits(41, 40),
//					getBits(39, 36), vavail, w_h, swoff);
//				mtype = "Per-Outlet-Energy:" + getBit(42) + ":" + getBits(41, 40) + ":" + getBits(39, 36);
//			}
//			/* If 44-42 = 100, per outlet current */
//			else if(getBits(44,42) == 0x04) {
//				int[] ampsx10 = new int[4];
//				for(i = 0; i < 4; i++) {
//					ampsx10[i] = getBits(35-(9*i), 27-(9*i));
//				}
//				change = handlePerOutletCurrentMessage(tag, change, getBit(41), getBits(40,39), 
//					getBits(38,36), ampsx10);
//				mtype = "Per-Outlet-Current:" + getBit(41) + ":" + getBits(40, 39) + ":" + getBits(38, 36);
//			}
			/* If 44-41 = 0011, feed line current */
//			else if(getBits(44,41) == 0x03) {
//				int[] ampsx10 = new int[4];
//				for(i = 0; i < 4; i++) {
//					ampsx10[i] = getBits(39-(10*i), 30-(10*i));
//				}
//				change = handleFeedLineCurrentMessage(tag, change, getBit(40), ampsx10);
//				mtype = "Feed-Line-Current:" + getBit(40);
//			}
			else {
				mtype = "unknown";
			}			
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), accum, -1, false, mtype);

			//System.out.print((System.currentTimeMillis() / 1000) + ": " + tag.getTagGUID() + ": message: ");
			//for(i = 0; i < 6; i++) {
			//	System.out.print(String.format("%02x", accum[i]));
			//}
			//System.out.println(" [" + mtype + "]");

			return change;
		}
		/* Handle alarm message */
		public boolean handleAlarm(MantisIITag tag, boolean trip, int alarmidx, boolean change, boolean verify) {
//			System.out.println("Alarm event! idx=" + alarmidx + ", tripped=" + trip);
			String mtype = null;
			/* If bank overload */
			if((alarmidx >= 0) && (alarmidx < 12)) {
				int bankid = alarmidx;
				mtype = "bank-load-alarm:" + bankid + ":" + trip;
				if((tower != null) && (tower.banks != null) && (bankid < tower.banks.length)) {
					OurBankState bs = tower.banks[bankid];
					if(bs != null) {
						if(bs.overload == trip) {	/* Same as before? */
							bs.ol_pending_upd = false;	/* Nothing pending */
						}
						else {
							/* If unverified, or already pending, do update */
							if((!verify) || bs.ol_pending_upd) {
								bs.overload = trip;		/* Update value */
								bs.ol_signalled = true;		/* Mark as updated */
								PduBreakerTagSubType.updateTag(bs.tag, null, null, null, null, null, null, trip);
								bs.ol_pending_upd = false;	/* Nothing pending */					
								change = true;
							}
							else {	/* Else, verifying and this is first request */
								bs.ol_pending_upd = true;
							}
						}

					}
				}
			}
			// Feed line alarm
			else if((alarmidx >= 12) && (alarmidx < 15)) {
				int feedidx = alarmidx-12;
				mtype = "feedline-alarm:" + feedidx + ":" + trip;
				if((tower != null) && (tower.feedtags != null) && (feedidx < tower.feedtags.length)) {
					OurFeedLineState fs = tower.feedtags[feedidx]; 
					/* If valid tower and feed line */
					if (fs != null) {
						if(fs.overload == trip) {	/* Same as before? */
							fs.pending_upd = false;	/* Nothing pending */
						}
						else {
							/* If unverified, or already pending, do update */
							if((!verify) || fs.pending_upd) {
								fs.overload = trip;		/* Update value */
								fs.was_signalled = true;		/* Mark as updated */
								Boolean loadwarn = Boolean.valueOf(trip);
								Boolean overload = loadwarn;
								PduFeedLineTagSubType.updateTag(fs.tag, 
									null, null, null, overload, loadwarn, null);
								fs.pending_upd = false;	/* Nothing pending */					
								change = true;
							}
							else {	/* Else, verifying and this is first request */
								fs.pending_upd = true;
							}
						}

					}
				}
			}
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), new int[0], alarmidx, trip, mtype);

			//System.out.print((System.currentTimeMillis() / 1000) + ": " + tag.getTagGUID() + ": alarm: " + alarmidx + "=" + trip);
			//System.out.println(" [" + mtype + "]");

			return change;
		}

		private static final int OUTLETS_ALL = 0xFF;
		/* Handle outlet state change */
		public boolean handleOutletState(MantisIITag tag, int chg, int outid, boolean verify,
			MantisIITagType04Q tt, boolean nested) {
			boolean change = false;

			if((tower != null) && (tower.banks != null) && (outid < OUTLETS_ALL)) {
				/* Scan banks to figure out bank ID and outlet index */
				int bankid;
				int oidx = outid;
				OurBankState[] bs = tower.banks;
				for(bankid = 0; (bankid < bs.length) && (bs[bankid].outlets != null) && (oidx >= bs[bankid].outlets.length); bankid++) {
					oidx -= tower.banks[bankid].outlets.length;
				}
				if((bankid >= bs.length) || (bs[bankid].outlets == null))
					return change;
				
				OurBankState bank = bs[bankid];
				
				/* Valid bank and outlet */
				if(tower.switched_outlets && (bank != null) && (bank.outlets != null) && 
						(bank.outlets.length > oidx)) {
					/* If new state is same, do nothing */
					if(bank.outlets[oidx].switch_state == chg) {
						bank.outlets[oidx].sw_pending_upd = false;	/* Nothing pending */
					}
					else {
						/* If unverified, or already pending, do update */
						if((!verify) || bank.outlets[oidx].sw_pending_upd) {
							bank.outlets[oidx].switch_state = chg;		/* Update value */
							bank.outlets[oidx].sw_signalled = true;		/* Mark as updated */
							PduOutletTagSubType.updateTag(bank.outlets[oidx].tag, 
								null, null, null, null, null, null, null, null, null,
								null, null, null, Boolean.valueOf(chg == SWITCH_ON));
							bank.outlets[oidx].sw_pending_upd = false;	/* Nothing pending */					
							change = true;
							/* If reboot, add to reset queue */
							if(chg == SWITCH_REBOOT) {
								if(rebooted_outlets == null) {
									rebooted_outlets = new LinkedList<OurOutletState>();
								}
								rebooted_outlets.add(bank.outlets[oidx]);
								tt.enqueueRebootDelay(tag);
							}
						}
						else {	/* Else, verifying and this is first request */
							bank.outlets[oidx].sw_pending_upd = true;
						}
					}
				}
				/* Special case, all outlets */
				else if(tower.switched_outlets && (outid == OUTLETS_ALL) && (tower.banks != null)) {
					int idx = 0;
					for(int i = 0; i < tower.banks.length; i++) {
						for(int j = 0; (tower.banks[i].outlets != null) && (j < tower.banks[i].outlets.length); j++, idx++) {
							change = handleOutletState(tag, chg, idx, verify, tt, change);
						}
					}
				}
			}

			/* Log message, if needed */
			if(!nested) {
				RangerServer.logPDUTagMessage(tag.getTagGUID(), new int[0], outid, 
					(chg==SWITCH_ON), "outlet-switch-" + outid + "-" + chg);
			}
			//System.out.print((System.currentTimeMillis() / 1000) + ": " + tag.getTagGUID() + ": outlet: " + outid + "=" + chg);
			//System.out.println(" [" + mtype + "]");
			
			return change;
		}
	}
    /**
     * 
     * Constructor
     */
    public MantisIITagType04Q() {
        super(4, 'Q', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04Q");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting payload 
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(tag);
			tag.setTagState(ts);
			change = true;
		}
		
        /* If payload is flags payload */
        if((cur_payload >= MIN_FLAGS_PAYLOAD) && (cur_payload <= MAX_FLAGS_PAYLOAD)) {
        	/* Set low battery flag - verify on set*/
			boolean v = ((cur_payload & FLAGS_LOWBATT) != 0);

            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Remember that we saw low battery during hour - 7 vs 6 beacons per message */
			if((cur_payload & FLAGS_LOWBATT) != 0)
				ts.did_low_batt = true;
            /* Set disconnect flag - verify on set */
			v = ((cur_payload & FLAGS_PDUDISCONNECT) != 0);
			boolean dchg = updateBooleanVerify(tag, PDUDISCONNECT_ATTRIB_INDEX, v, verify && v);
            change |= dchg;
			/* If disconnected, reset the device configuration */
			if(v && dchg)
				change = ts.resetDeviceConfiguration(tag, change);
			/* Check for PDU tower disconnect */
			change = updateBooleanVerify(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, v, verify && v) || change;
			ArrayList<Long> distow = new ArrayList<Long>();
			if((cur_payload & FLAGS_PDUDISCONNECT) != 0) {	/* Master disconnect? */
				distow.add(Long.valueOf(1));
			}
			change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX, 
				distow, verify) || change;
        }
        /* If start of message */
        else if((cur_payload >= MIN_START_PAYLOAD) && (cur_payload <= MAX_START_PAYLOAD)) {
        	change = ts.accumulateByte(tag, (byte)(cur_payload & 0x7F), cur_timestamp, true, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If continuation of payload */
		else if((cur_payload >= MIN_CONT_PAYLOAD) && (cur_payload <= MAX_CONT_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0xFF), cur_timestamp, false, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
			/* If we're still defaulted on tower disconnect, assume we're good after 3 messages */
			if(tag.isDefaultFlagged(PDUTOWERDISCONNECT_ATTRIB_INDEX) && (ts.msg_cnt > 2)) {
				change = updateBooleanVerify(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, false, false) || change;
				change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX,
					new ArrayList<Long>(), false) || change;
			}
		}
		/* If alarm payload */
		else if((cur_payload >= MIN_ALARM_PAYLOAD) && (cur_payload <= MAX_ALARM_PAYLOAD)) {
			change = ts.handleAlarm(tag, (cur_payload & 0x20) != 0, cur_payload & 0x01F, change, verify) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If we're on second outlet payload, and first one was good */
		else if((cur_payload >= MIN_OUTSTAT_PAYLOAD2) && (cur_payload <= MAX_OUTSTAT_PAYLOAD2) &&
			(payloads[0] >= MIN_OUTSTAT_PAYLOAD1) && (payloads[0] <= MAX_OUTSTAT_PAYLOAD1) &&
			((cur_timestamp - payloadage[0]) < MAX_OUTSTAT_PERIOD)) {
			change = ts.handleOutletState(tag,
				(cur_payload & 0x18) >> 3, ((cur_payload & 0x07) << 5) | (payloads[0] & 0x1F), verify, 
				this, false) || change;
		}

        return change;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return SUBTYPES;
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
        MantisIITag tag = (MantisIITag)t;

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null)
			return;
		if(ts.rebooted_outlets != null) {
			OurOutletState s;
			while((s = ts.rebooted_outlets.poll()) != null) {
				if(s.switch_state == SWITCH_REBOOT) {	/* Still reboot */
					s.switch_state = SWITCH_ON;
					PduOutletTagSubType.updateTag(s.tag, 
						null, null, null, null, null, null, null, null, null,
						null, null, null, Boolean.valueOf(true));
				}
			}
			ts.rebooted_outlets = null;
		}
    }
	private void enqueueRebootDelay(MantisIITag tag) {
		enqueueTagForDelay(tag, REBOOT_DELAY);
	}
}
