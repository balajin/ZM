package com.rfcode.drivers.readers.mantis2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;

import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagLinkLifecycleListener;
import com.rfcode.drivers.SessionFactory;

/**
 * Class representing a channel (radio) on a Mantis-class reader
 * 
 * @author Mike Primm
 */
public class MantisIIReaderChannel implements ReaderChannel {
    private String id;
    private String rel_id;
    private String label;
    private MantisIIReader myreader;
    private int ssi_age_out;
    private int cutoff_ssi;
    private int bias;
    private int noise_dbm;
    private int evt_per_sec;
    private boolean valid;
    /* Our tag links, keyed by tag guid */
    private HashMap<String, TagLink> our_links;
    private Map<String, TagLink> our_links_ro;
    /* List of tag link listeners for this channel */
    private ArrayList<TagLinkLifecycleListener> listeners;
    private List<TagLinkLifecycleListener> listeners_ro;
    /* Empty list */
    private static List<TagLinkLifecycleListener> empty_list = Collections.emptyList();
    /**
     * Constructor
     * 
     * @param c_id -
     *            channel ID
     * @param c_label -
     *            channel label
     */
    MantisIIReaderChannel(String c_id, String c_label, MantisIIReader reader) {
        rel_id = c_id;
        label = c_label;
        myreader = reader;
        id = reader.getID() + "_channel_" + rel_id;
        our_links = new HashMap<String, TagLink>();
        our_links_ro = Collections.unmodifiableMap(our_links);
        valid = true;
    }
    /**
     * Initialization method - must be called once attributes set. init() method
     * MUST call addEntity() to add entity to directory
     */
    public void init() throws DuplicateEntityIDException {
        ReaderEntityDirectory.addEntity(this);
    }
    /**
     * Cleanup method. cleanup() method MUST call removeEntity() to remove
     * entity from directory
     */
    public void cleanup() {
        ArrayList<MantisIITag> tags = new ArrayList<MantisIITag>();
        /* Get list of tags to be unlinked */
        for (TagLink tl : our_links.values()) {
            tags.add(((MantisIITagLink)tl).getMantisIITag());
        }
        /*
         * Then unhook em (need extra list because unlink calls back to alter
         * our_links during iteration)
         */
        for (MantisIITag tag : tags) {
            /* Unhook the link from the tag */
            tag.insertTagLink(this, null);
        }
        ReaderEntityDirectory.removeEntity(this);
        valid = false;
    }
    /**
     * Get channel ID (unique to all other channels). Usually combination of
     * reader ID and relative channel ID (<readerid>_channel_<rel-channel-id>)
     * 
     * @return id
     */
    public String getID() {
        return id;
    }
    /**
     * Set channel ID - unsupported (channel ID derived from reader ID and
     * relative ID)
     * 
     * @param id
     */
    public void setID(String id) {
    }

    /**
     * Get channel ID (unique relative to other channels on the same reader)
     * 
     * @return id
     */
    public String getRelativeChannelID() {
        return rel_id;
    }
    /**
     * Get channel label (presentation label)
     * 
     * @return label
     */
    public String getChannelLabel() {
        return label;
    }
    /**
     * Set channel label (presentation label)
     * 
     * @param lab -
     *            channel label
     */
    public void setChannelLabel(String lab) {
        label = lab;
    }
    /**
     * Get the reader that owns the channel
     * 
     * @return Reader
     */
    public Reader getReader() {
        return myreader;
    }
    /**
     * Get the SSI age-out time for TagLinks in this channel. Controls how many
     * seconds after a reader reports a tag message that excludes an SSI for
     * this channel before the TagLink is assumed lost.
     * 
     * @return number of SSI_VALUE_NA reports before link retired
     */
    public int getMissingSSIAgeOutTime() {
        return ssi_age_out;
    }
    /**
     * Set the SSI age-out time - used by MantisIIReader driver
     * 
     * @param ageout -
     *            number seconds of missing SSI values before link tossed
     */
    void setMissingSSIAgeOutTime(int ageout) {
        ssi_age_out = ageout;
    }
    /**
     * Add link to channel - used by MantisIITagLink constructor
     * 
     * @param tl -
     *            tag link to add
     */
    void addLink(MantisIITagLink tl) {
        our_links.put(tl.getTag().getTagGUID(), tl);
    }
    /**
     * Remove link to channel - used by MantisIITagLink cleanup
     * 
     * @param tl -
     *            tag link to remove
     */
    void removeLink(MantisIITagLink tl) {
        our_links.remove(tl.getTag().getTagGUID());
    }
    /**
     * Get map of tag links associated with this channel (read-only)
     * 
     * @return Map of links, keyed by Tag GUID (read-only)
     */
    public Map<String, TagLink> getTagLinks() {
        return our_links_ro;
    }
    /**
     * Get number of tag links associated with this channel
     * 
     * @return number of links
     */
    public int getTagLinkCount() {
        return our_links.size();
    }
    /**
     * Add tag link lifecycle or status listener for any tag links on this
     * channel.
     * 
     * @param listen -
     *            listener to add
     */
    public void addTagLinkListener(TagLinkLifecycleListener listen) {
        if (listeners == null) {
            listeners = new ArrayList<TagLinkLifecycleListener>();
            listeners_ro = Collections.unmodifiableList(listeners);
        }
        listeners.add(listen);
    }
    /**
     * Remove tag link lifecycle or status listener for any tag links on this
     * channel.
     * 
     * @param listen -
     *            listener to remove
     */
    public void removeTagLinkListener(TagLinkLifecycleListener listen) {
        if (listeners != null) {
            listeners.remove(listen);
            if (listeners.size() == 0) {
                listeners = null;
                listeners_ro = null;
            }
        }
    }
    /**
     * Get list of tag link lifecycle and status listeners for this channel.
     * 
     * @return list of listeners (read-only)
     */
    public List<TagLinkLifecycleListener> getTagLinkListeners() {
        if (listeners == null)
            return empty_list;
        return listeners_ro;
    }
    /**
     * Use channel-ID as toString()
     */
    public String toString() {
        return id;
    }
    /**
     * Get our session factory
     * 
     * @return session factory from our reader
     */
    public SessionFactory getSessionFactory() {
        return myreader.getReaderSession().getSessionFactory();
    }
    /**
     * Get cutoff SSI value
     * 
     * @return cutoff, or 0 if no cutoff
     */
    public int getSSICutoff() {
        return cutoff_ssi;
    }
    /**
     * Set cutoff SSI value
     * 
     * @param cutoff -
     *            value in dBm, or 0 if no cutoff
     */
    void setSSICutoff(int cutoff) {
        cutoff_ssi = cutoff;
    }
    /**
     * Test if channel is still valid (not cleaned up)
     * 
     * @return true if valid
     */
    public boolean isValid() {
        return valid;
    }
    /**
     * Set channel bias value
     * @param bias - bias in dBm (value added to reported SSI)
     */
    void setChannelBias(int bias) {
        this.bias = bias;
    }
    /**
     * Get channel bias value
     * @return bias in dBm
     */
    public int getChannelBias() {
        return bias;
    }
    /**
     * Get channel noise level, in dbm
     */
    public int getChannelNoiseLevel() {
        return noise_dbm;
    }
    /**
     * Set channel noise level, in dbm
     * @param noise - noise in dbm
     */
    void setChannelNoiseLevel(int noise) {
        noise_dbm = noise;
    }
    /**
     * Get channel est events per sec
     */
    public int getChannelEventsPerSec() {
        return evt_per_sec;
    }
    /**
     * Set channel est events per sec
     * @param eps - events per sec
     */
    void setChannelEventsPerSec(int eps) {
        evt_per_sec = eps;
    }
}
