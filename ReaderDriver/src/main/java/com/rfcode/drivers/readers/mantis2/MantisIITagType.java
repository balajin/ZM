package com.rfcode.drivers.readers.mantis2;

import com.rfcode.drivers.readers.CurrentLoopSensorDefinition;
import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.Voltage0To5SensorDefinition;

import java.util.List;
/**
 * Base class for Mantis-family specific TagType. Includes attributes to help
 * readers with using given tag type. Mantis tag types are always for a specific
 * treatment code and subcode, so these are included in the attributes. Also,
 * implementers of this interface are used to provide tag payload parsing
 * functionality, since this is also tag-type specific.
 * 
 * @author Mike Primm
 */
public abstract class MantisIITagType extends AbstractTagType {
    /** Tag attribute - motion flag (boolean - true=motion) */
    public static final String MOTION_ATTRIB = "motion";
    public static final String MOTION_ATTRIB_LABEL = "Motion";
    /** Tag attribute - tamper flag (boolean - true=detected) */
    public static final String TAMPER_ATTRIB = "tamper";
    public static final String TAMPER_ATTRIB_LABEL = "Tamper";
    /** Tag attribute - low battery flag (boolean - true=detected) */
    public static final String LOWBATT_ATTRIB = "lowbattery";
    public static final String LOWBATT_ATTRIB_LABEL = "Low Battery";
    /** Tag attribute - panic flag (boolean - true=panic) */
    public static final String PANIC_ATTRIB = "panic";
    public static final String PANIC_ATTRIB_LABEL = "Panic";
    /** Tag attribute - panic flag (boolean - true=door open) */
    public static final String DOOR_ATTRIB = "dooropen";
    public static final String DOOR_ATTRIB_LABEL = "Door Open";
    /** Tag attribute - lock open detect (boolean - true=detected) */
    public static final String LOCKOPEN_ATTRIB = "lockopen";
    public static final String LOCKOPEN_ATTRIB_LABEL = "Lock Open";
    /** Tag attribute - lock closed detect (boolean - true=detected) */
    public static final String LOCKCLOSED_ATTRIB = "lockclosed";
    public static final String LOCKCLOSED_ATTRIB_LABEL = "Lock Closed";
    /** Tag attribute - IR locator code (string, "" if none detect or error) */
    public static final String IRLOCATOR_ATTRIB = "irlocator";
    public static final String IRLOCATOR_ATTRIB_LABEL = "IR Locator";
    /** Tag attribute - motion counter (integer) */
    public static final String MOTIONCOUNT_ATTRIB = "motioncount";
    public static final String MOTIONCOUNT_ATTRIB_LABEL = "Motion Count";
    /** Tag attribute - lock counter (integer) */
    public static final String LOCKCOUNT_ATTRIB = "lockcount";
    public static final String LOCKCOUNT_ATTRIB_LABEL = "Lock Count";
    /** Tag attribute - user payload (integer) */
    public static final String USERPAYLOAD_ATTRIB = "userpayload";
    public static final String USERPAYLOAD_ATTRIB_LABEL = "User Payload";
    /** Tag attribute - temperature (double - degrees C) */
    public static final String TEMPERATURE_ATTRIB = "temp";
    public static final String TEMPERATURE_ATTRIB_LABEL = "Temperature (C)";
    /** Tag attribute - humidity (double - %RH) */
    public static final String HUMIDITY_ATTRIB = "humidity";
    public static final String HUMIDITY_ATTRIB_LABEL = "Humidity (%)";
    /** Tag attribute - pressure (double - PSI) */
    public static final String PRESSURE_ATTRIB = "pressure";
    public static final String PRESSURE_ATTRIB_LABEL = "Pressure (PSI)";
    /** Tag attribute - sensor low battery flag (boolean - true=detected) */
    public static final String SENSOR_LOWBATT_ATTRIB = "sensorlowbatt";
    public static final String SENSOR_LOWBATT_ATTRIB_LABEL = "Low Sensor Battery";
    /** Tag attribute - dry contact flag (boolean - true=contact open) */
    public static final String DRY_ATTRIB = "dryopen";
    public static final String DRY_ATTRIB_LABEL = "Dry Contact Open";
    /** Tag attribute - dry contact 2 flag (boolean - true=contact open) */
    public static final String DRY2_ATTRIB = "dryopen2";
    public static final String DRY2_ATTRIB_LABEL = "Dry Contact #2 Open";
    /** Tag attribute - dry contact 3 flag (boolean - true=contact open) */
    public static final String DRY3_ATTRIB = "dryopen3";
    public static final String DRY3_ATTRIB_LABEL = "Dry Contact #3 Open";
    /** Tag attribute - fluid detected flag (boolean - true=detected) */
    public static final String FLUID_ATTRIB = "fluid";
    public static final String FLUID_ATTRIB_LABEL = "Fluid Detected";
    /** Tag attribute - sensor disconnected flag (boolean - true=disconnected) */
    public static final String SENSORDISCONNECT_ATTRIB = "disconnect";
    public static final String SENSORDISCONNECT_ATTRIB_LABEL = "Sensor Disconnected";
    /** Tag attribute - dew point (double - degrees C) */
    public static final String DEWPOINT_ATTRIB = "dewpoint";
    public static final String DEWPOINT_ATTRIB_LABEL = "Dew Point (C)";
    /** Tag attribute - PDU disconnected flag (boolean - true=disconnected) */
    public static final String PDUDISCONNECT_ATTRIB = "pduDisconnect";
    public static final String PDUDISCONNECT_ATTRIB_LABEL = "PDU Disconnected";
    /** Tag attribute - PDU model (string) */
    public static final String PDUMODEL_ATTRIB = "pduModel";
    public static final String PDUMODEL_ATTRIB_LABEL = "Model";
    /** Tag attribute - PDU serial number (string) */
    public static final String PDUSERIAL_ATTRIB = "pduSerial";
    public static final String PDUSERIAL_ATTRIB_LABEL = "Serial Number";
    /** Tag attribute - message loss (double percent) */
    public static final String MSGLOSS_ATTRIB = "msgLoss";
    public static final String MSGLOSS_ATTRIB_LABEL = "Message Loss Rate(%)";
    public static final String PDUMSGLOSS_ATTRIB = MSGLOSS_ATTRIB;
    public static final String PDUMSGLOSS_ATTRIB_LABEL = MSGLOSS_ATTRIB_LABEL;
	/* Tag attribute - PDU total true power (watts) (double) */
	public static final String PDUTRUEPOWER_ATTRIB = "pduTruePower";
	public static final String PDUTRUEPOWER_ATTRIB_LABEL = "Active Power";
	/* Tag attribute - PDU total apparent power (VA) (double) */
	public static final String PDUAPPPOWER_ATTRIB = "pduAppPower";
	public static final String PDUAPPPOWER_ATTRIB_LABEL = "Apparent Power";
	/* Tag attribute - PDU total power factor (double percent) */
	public static final String PDUPWRFACT_ATTRIB = "pduPwrFactor";
	public static final String PDUPWRFACT_ATTRIB_LABEL = "Power Factor";
	/* Tag attribute - PDU total watt-hours (double) */
	public static final String PDUWATTHOURS_ATTRIB = "pduTotalWH";
	public static final String PDUWATTHOURS_ATTRIB_LABEL = "Total True Power (W-h)";
	/* Tag attribute - PDU total volt-amp-hours (double) */
	public static final String PDUAPPPWRHOURS_ATTRIB = "pduTotalVAH";
	public static final String PDUAPPPWRHOURS_ATTRIB_LABEL = "Total Apparent Power (VA-h)";
	/* Tag attribute - PDU watt-hours start time (UTC milliseconds) (double) */
	public static final String PDUWATTHOURSTS_ATTRIB = "pduTotalPwrTS";
	public static final String PDUWATTHOURSTS_ATTRIB_LABEL = "Total Power Start Time";
    /** Tag attribute - differential pressure (double - Pascals) */
    public static final String DIFFPRESS_ATTRIB = "diffpress";
    public static final String DIFFPRESS_ATTRIB_LABEL = "Differential Pressure (Pa)";
    /** Tag attribute - differential pressure (double - Pascals) */
    public static final String AVGDIFFPRESS_ATTRIB = "avgdiffpress";
    public static final String AVGDIFFPRESS_ATTRIB_LABEL = "Average Differential Pressure (Pa)";
	/* Tag attribute - tower ID (long) */
	public static final String PDUTOWERID_ATTRIB = "towerID";
	public static final String PDUTOWERID_ATTRIB_LABEL = "Tower ID";
	/* Tag attribute - feedline set ID (long) */
	public static final String PDUFEEDSETID_ATTRIB = "feedSetID";
	public static final String PDUFEEDSETID_ATTRIB_LABEL = "Feed Line Set ID";
	/* Tag attribute - PDU Serial Number List (string-list) */
	public static final String PDUSERIALLIST_ATTRIB = "pduSerialList";
	public static final String PDUSERIALLIST_ATTRIB_LABEL = "Tower Serial Numbers";
	/* Tag attribute - PDU Model Number List (string-list) */
	public static final String PDUMODELLIST_ATTRIB = "pduModelList";
	public static final String PDUMODELLIST_ATTRIB_LABEL = "Tower Model Numbers";
	/* Tag attribute - PDU Tower Disconnect (bool) */
	public static final String PDUTOWERDISCONNECT_ATTRIB = "pduTowerDisconnect";
	public static final String PDUTOWERDISCONNECT_ATTRIB_LABEL = "Tower Disconnect";
	/* Tag attribute - PDU Disconnected Towers (long-list) */
	public static final String PDUDISCONNECTEDTOWERS_ATTRIB = "pduDisconnectedTowers";
	public static final String PDUDISCONNECTEDTOWERS_ATTRIB_LABEL = "Disconnected Towers";
	/* Tag attribute - PDU Tower Count (long) */
	public static final String PDUTOWERCOUNT_ATTRIB = "pduTowerCount";
	public static final String PDUTOWERCOUNT_ATTRIB_LABEL = "Tower Count";
	/* Tag attribute - PDU Firmware Incompatible (boolean) */
	public static final String PDUFIRMWAREINCOMPAT_ATTRIB = "pduFWUpgradeNeeded";
	public static final String PDUFIRMWAREINCOMPAT_ATTRIB_LABEL = "Firmware Upgrade Needed";
	/* Tag attribute - First button (boolean) */
	public static final String BUTTON0_ATTRIB = "button0";
	public static final String BUTTON0_ATTRIB_LABEL = "1st Button";
	/* Tag attribute - Second button (boolean) */
	public static final String BUTTON1_ATTRIB = "button1";
	public static final String BUTTON1_ATTRIB_LABEL = "2nd Button";
	/* Tag attribute - Third button (boolean) */
	public static final String BUTTON2_ATTRIB = "button2";
	public static final String BUTTON2_ATTRIB_LABEL = "3rd Button";
    /** Tag attribute - tamper armed flag (boolean - true=armed) */
    public static final String TAMPER_ARMED_ATTRIB = "tamperArmed";
    public static final String TAMPER_ARMED_ATTRIB_LABEL = "Tamper Armed";
    /** Tag attribute - current loop reading (double) */
    public static final String CURRENT_LOOP_READING_ATTRIB = CurrentLoopSensorDefinition.CURRENT_LOOP_READING_ATTRIB;
    public static final String CURRENT_LOOP_READING_ATTRIB_LABEL = "Current Loop Reading";
    /** Tag attribute - custom field value (CustomFieldValue) */
    public static final String CUSTOM_FIELD_ATTRIB = "customField";
    public static final String CUSTOM_FIELD_ATTRIB_LABEL = "Custom Field";
    /** Tag attribute - sensor voltage reading (double) */
    public static final String SENSOR_VOLTAGE_READING_ATTRIB = Voltage0To5SensorDefinition.VOLTAGE_0TO5_ATTRIB;
    public static final String SENSOR_VOLTAGE_READING_ATTRIB_LABEL = "Sensor Voltage Reading";
    
    /* Treatment code */
    private int treatmentcode;
    /* Treatment subcode */
    private char treatmentsubcode;
    /* Payload count */
    private int payloadcount;

    private int ir_index;
    /**
     * Constructor - pass in treatment code, subcode, payload count
     * 
     * @param tc -
     *            treatment code
     * @param tsc -
     *            treatment subcode
     * @param pc -
     *            payload count
     */
    protected MantisIITagType(int tc, char tsc, int pc, String[] extra_attribs) {
        treatmentcode = tc;
        treatmentsubcode = tsc;
        payloadcount = pc;
		if(extra_attribs != null) {
			String[] list = new String[MantisIITagGroup.GROUPATTRIBLIST.length + extra_attribs.length];
			System.arraycopy(MantisIITagGroup.GROUPATTRIBLIST, 0, list, 0, MantisIITagGroup.GROUPATTRIBLIST.length);
			System.arraycopy(extra_attribs, 0, list, MantisIITagGroup.GROUPATTRIBLIST.length, extra_attribs.length);
	        setTagGroupAttributes(list);
			setTagGroupDistinctAttributes(MantisIITagGroup.GROUPATTRIBLIST);
		}
		else {
	        setTagGroupAttributes(MantisIITagGroup.GROUPATTRIBLIST);
		}
    }
    /**
     * Constructor - pass in treatment code, subcode, payload count
     * 
     * @param tc -
     *            treatment code
     * @param tsc -
     *            treatment subcode
     * @param pc -
     *            payload count
     */
    protected MantisIITagType(int tc, char tsc, int pc) {
        this(tc, tsc, pc, null);
    }
    /**
     * Get treatment code for tag type
     * 
     * @return treatment code
     */
    public int getTreatmentCode() {
        return treatmentcode;
    }
    /**
     * Get treatment subcode for tag type
     * 
     * @return treatment subcode
     */
    public char getTreatmentSubcode() {
        return treatmentsubcode;
    }
    /**
     * Get number of previous payloads needed for payload parsing. This is used
     * to handle tag types where payloads are spread across multiple messages.
     * 
     * @return number of payloads needed
     */
    public int getRequiredPayloadCount() {
        return payloadcount;
    }
    /**
     * Set list of attributes reported by tags of this type. Must be set, and
     * cannot be changed once set.
     * 
     * @param list -
     *            list of attribute IDs
     */
    protected void setTagAttributes(String[] list) {
        super.setTagAttributes(list);   /* Use default */
        /* Cache IR attribute index, if defined */
        ir_index = getTagAttributeIndex(IRLOCATOR_ATTRIB);
    }

    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag) {
        return false;
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting message
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
		return parsePayload(cur_payload, cur_timestamp, payloads, payloadage,
			tag);
	}
    /**
     * Factory method for creating TagGroup instances for this TagType.
     * Resulting TagGroup must have attributes set, and its init() method
     * called. This version generates a MantisII specific tag group.
     * 
     * @return new TagGroup
     */
    public TagGroup createTagGroup() {
        return new MantisIITagGroup(this);
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
    /**
     * Set tag to offline state
     * @param tag - tag to offline
     */
    public void setTagOffline(MantisIITag tag) {
    }
    /**
     * Update boolean attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new boolean value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateBoolean(MantisIITag tag, int index,
        boolean val) {
		return updateBooleanVerify(tag, index, val, false);
    }
    /**
     * Update boolean attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new boolean value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateBooleanVerify(MantisIITag tag, int index,
        boolean val, boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Boolean))
            || (((Boolean) o).booleanValue() != val) ||
            tag.isDefaultFlagged(index)) {

			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            /* Use common references - save memory */
    	        if (val)
    	            obj[index] = Boolean.TRUE;
    	        else
    	            obj[index] = Boolean.FALSE;
    	        tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
		return false;
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateInt(MantisIITag tag, int index, int val) {
		return updateIntVerify(tag, index, val, false);
	}
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateIntVerify(MantisIITag tag, int index, int val, boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Integer))
            || (((Integer) o).intValue() != val) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = Integer.valueOf(val);
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateLong(MantisIITag tag, int index, long val) {
		return updateLongVerify(tag, index, val, false);
	}
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateLongVerify(MantisIITag tag, int index, long val,
		boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Long))
            || (((Long) o).longValue() != val) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = Long.valueOf(val);
    	        tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update double attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new double value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateDouble(MantisIITag tag, int index, double val) {
		return updateDoubleVerify(tag, index, val, false, 0.0);
    }
    /**
     * Update double attribute value at given index, if needed - enhanced verify supported attribute
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new double value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateDoubleVerify(MantisIITag tag, int index, double val, boolean verify) {
		return updateDoubleVerify(tag, index, val, verify, 0.0);
	}
    /**
     * Update double attribute value at given index, if needed - enhanced verify supported attribute
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new double value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateDoubleVerify(MantisIITag tag, int index, double val, boolean verify,
		double max_unverified_delta) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Double))
            || (((Double) o).doubleValue() != val) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index) ||
				((o != null) && (Math.abs(((Double)o).doubleValue()-val)<max_unverified_delta))) {
	            obj[index] = Double.valueOf(val);
    	        tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update string attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateString(MantisIITag tag, int index, String val) {
		return updateStringVerify(tag, index, val, false);
	}
    /**
     * Update string attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateStringVerify(MantisIITag tag, int index, String val,
		boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof String))
            || (!((String) o).equals(val)) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = val;
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update custom field attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param field -
     *            field ID           
     * @param val -
     *            new value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateCustomField(MantisIITag tag, int index, String field, Object val) {
		return updateCustomFieldVerify(tag, index, field, val, false);
	}
    /**
     * Update custom field attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param field -
     *            field ID           
     * @param val -
     *            new string value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateCustomFieldVerify(MantisIITag tag, int index, String field, Object val,
		boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof CustomFieldValue)) || (!o.equals(val)) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = new CustomFieldValue(field, val);
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update string list attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string list value
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateStringList(MantisIITag tag, int index, List<String> val) {
		return updateStringListVerify(tag, index, val, false);
	}
    /**
     * Update string list attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new string list value
     * @return true if value changed, false if unchanged
     */
    @SuppressWarnings("rawtypes")
	protected static final boolean updateStringListVerify(MantisIITag tag, int index, List<String> val,
		boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof List))
            || (!((List) o).equals(val)) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = val;
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update long list attribute value at given index, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @param val -
     *            new long list value
     * @return true if value changed, false if unchanged
     */
    @SuppressWarnings("rawtypes")
	protected static final boolean updateLongListVerify(MantisIITag tag, int index, List<Long> val,
		boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof List))
            || (!((List) o).equals(val)) ||
            tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = val;
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
    /**
     * Update attribute value at given index to null, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateNull(MantisIITag tag, int index) {
		return updateNullVerify(tag, index, false);
	}
    /**
     * Update attribute value at given index to null, if needed
     * 
     * @param tag -
     *            tag
     * @param index -
     *            index in list
     * @return true if value changed, false if unchanged
     */
    protected static final boolean updateNullVerify(MantisIITag tag, int index, boolean verify) {
		if(tag.isBlockedUpdateTagAttrib(index))
			return false;
        Object[] obj = tag.getRawAttribs();
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o != null) || tag.isDefaultFlagged(index)) {
			if((!verify) || tag.getPendingUpdate(index) || tag.isDefaultFlagged(index)) {
	            obj[index] = null;
	            tag.clearDefaultFlag(index);
				tag.clearPendingUpdate(index);
	            return true;
			}
			else {
				tag.setPendingUpdate(index);
			}
        } else {
			tag.clearPendingUpdate(index);
		}
        return false;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return false;
	}
	/**
	 * Is tag type a subtype?  If so, used internally by other tag types and cannot be used
     * to create a group directly.
	 */
	public boolean isSubType() {
		return false;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return null;
	}
    /**
     * Get IR attribute index, if defined
     * @return index, or -1 if not defined
     */
    public int getIRIndex() {
        return ir_index;
    }
    /**
     * Create tag instance
     * @param tg - tag group
     * @param guid - tag guid
     * @return tag
     */
    public MantisIITag makeTag(MantisIITagGroup grp, String guid) {
    	return new MantisIITag(grp, guid);
    }
    /**
     * Get default sensor definition for group (group matches tag type)
     * @param grp - group
     * @return null if none
     */
    public SensorDefinition getDefaultSensorDefinition(MantisIITagGroup grp) {
    	return null;
    }
}
