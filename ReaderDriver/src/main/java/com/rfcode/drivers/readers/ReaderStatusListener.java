package com.rfcode.drivers.readers;

/**
 * General interface for listening for reader status changes
 * 
 * @author Mike Primm
 */
public interface ReaderStatusListener {
    /**
     * Callback for reporing reader status change. Handler MUST process
     * immediately and without blocking.
     * 
     * @param reader -
     *            reader reporting change
     * @param newstatus -
     *            new status for reader
     * @param oldstatus -
     *            previous status for reader
     */
    public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
        ReaderStatus oldstatus);
}
