package com.rfcode.drivers.readers;

import java.util.ArrayList;
/**
 * Special list subclass for list-of-doubles (used for coordinate)
 * 
 * @author Mike Primm
 */
public class ListDouble extends ArrayList<Double> {
	private static final long serialVersionUID = -3393790007184790191L;
	
	public ListDouble() {
    }
    public ListDouble(Double[] v) {
        for(Double d: v)
            add(d);
    }
}
