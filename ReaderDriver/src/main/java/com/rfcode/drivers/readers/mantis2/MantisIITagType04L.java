package com.rfcode.drivers.readers.mantis2;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import com.rfcode.ranger.RangerServer;
/**
 * Tag type for treatment 04L tags - Mantis II Tags - Treatment 04L
 * 
 * @author Mike Primm
 */
public class MantisIITagType04L extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04L";
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID, PduOutletTagSubType.TAGTYPEID, PduBreakerTagSubType.TAGTYPEID
	};

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        LOWBATT_ATTRIB,
        PDUDISCONNECT_ATTRIB,
		PDUMODEL_ATTRIB,
		PDUSERIAL_ATTRIB,
		PDUMSGLOSS_ATTRIB,
		PDUTRUEPOWER_ATTRIB,
		PDUAPPPOWER_ATTRIB,
		PDUPWRFACT_ATTRIB,
		PDUWATTHOURS_ATTRIB,
		PDUAPPPWRHOURS_ATTRIB,
		PDUWATTHOURSTS_ATTRIB,
		PDUSERIALLIST_ATTRIB,
		PDUMODELLIST_ATTRIB,
		PDUTOWERDISCONNECT_ATTRIB,
		PDUDISCONNECTEDTOWERS_ATTRIB,
		PDUTOWERCOUNT_ATTRIB };
    private static final String[] TAGATTRIBLABS = {
        LOWBATT_ATTRIB_LABEL,
		PDUDISCONNECT_ATTRIB_LABEL,
		PDUMODEL_ATTRIB_LABEL,
		PDUSERIAL_ATTRIB_LABEL,
		PDUMSGLOSS_ATTRIB_LABEL,
		PDUTRUEPOWER_ATTRIB_LABEL,
		PDUAPPPOWER_ATTRIB_LABEL,
		PDUPWRFACT_ATTRIB_LABEL,
		PDUWATTHOURS_ATTRIB_LABEL,
		PDUAPPPWRHOURS_ATTRIB_LABEL,
		PDUWATTHOURSTS_ATTRIB_LABEL,
		PDUSERIALLIST_ATTRIB_LABEL,
		PDUMODELLIST_ATTRIB_LABEL,
		PDUTOWERDISCONNECT_ATTRIB_LABEL,
		PDUDISCONNECTEDTOWERS_ATTRIB_LABEL,
		PDUTOWERCOUNT_ATTRIB_LABEL  };

    private static final int LOWBATT_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUMODEL_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUSERIAL_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
	private static final int PDUMSGLOSS_ATTRIB_INDEX = 4;

	private static final int PDUTRUEPOWER_ATTRIB_INDEX = 5;
	private static final int PDUAPPPOWER_ATTRIB_INDEX = 6;
	private static final int PDUPWRFACT_ATTRIB_INDEX = 7;
	private static final int PDUWATTHOURS_ATTRIB_INDEX = 8;
	private static final int PDUAPPPWRHOURS_ATTRIB_INDEX = 9;
	private static final int PDUWATTHOURSTS_ATTRIB_INDEX = 10;

	private static final int PDUSERIALLIST_ATTRIB_INDEX = 11;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 12;
	private static final int PDUTOWERDISCONNECT_ATTRIB_INDEX = 13;
	private static final int PDUDISCONNECTEDTOWERS_ATTRIB_INDEX = 14;
	private static final int PDUTOWERCOUNT_ATTRIB_INDEX = 15;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        Boolean.FALSE, Boolean.FALSE, "", "", null, null,
		null, null, null, null, null, null, null, Boolean.FALSE, 
		new ArrayList<Long>(), null };

	private static final int MIN_FLAGS_PAYLOAD = 0760;
	private static final int MAX_FLAGS_PAYLOAD = 0777;

	private static final int MIN_START_PAYLOAD = 0400;
	private static final int MAX_START_PAYLOAD = 0577;

	private static final int MIN_CONT_PAYLOAD = 0000;
	private static final int MAX_CONT_PAYLOAD = 0377;

	private static final int MIN_BREAKER_PAYLOAD = 0600;
	private static final int MAX_BREAKER_PAYLOAD = 0637;

	private static final long DEF_PKT_INTERVAL = 10000;	/* 10 seconds */

	private static class OurPhaseState {
		SubTag	tag;			/* Tag object for phase */
		int	last_volts;			/* Last reported P-N voltage */
		int	last_volts_idx0;	/* Last reported P-N voltage (index 0) */
		int	last_pf;			/* Last power factor */
		int last_watthours;		/* Last watthours */
		long	last_watthours_ts;	/* Last time of watthours accumulator */
		double	last_watts;		/* Last calculated watts */
		double total_watthours;	/* Accumulated watthours */
		double total_vahours;	/* Accumulate VA-hours */
		long	watthours_ts;	/* Start time of watthours accumulator */

		public OurPhaseState() {
			last_volts = -1;	/* None yet */
			last_volts_idx0 = -1;
			last_pf = -1;
			last_watthours = -1;
			last_watthours_ts = -1;
			last_watts = -1.0;
			total_watthours = -1.0;
			total_vahours = -1.0;
			watthours_ts = 0;
		}
	}
	private static class OurOutletState {
		SubTag	tag;			/* Tag object for outlet */
		int	last_pf;			/* Last power factor */
		int last_watthours;		/* Last watthours */
		int last_cfg;			/* Last configuration index */
		long	last_watthours_ts;	/* Last time of watthours accumulator */
		double	last_watts;		/* Last calculated watts */
		double total_watthours;	/* Accumulated watthours */
		double total_vahours;	/* Accumulated VA-hours */
		double last_diff;		/* Last watthours added - used for VA-hours */
		long	watthours_ts;	/* Start time of watthours accumulator */

		public OurOutletState() {
			last_pf = -1;
			last_cfg = -1;
			last_watthours = -1;
			last_watthours_ts = -1;
			last_watts = -1.0;
			total_watthours = -1.0;
			total_vahours = -1.0;
			last_diff = 0.0;
			watthours_ts = 0;
		}
	}
	private static class OurBreakerState {
		SubTag	tag;			/* Tag object for outlet */
		int		last_amps_x10;	/* Last amps, in 10ths */
		boolean	last_tripped;	/* Last tripped state */
		boolean	was_signalled;	/* true if state changed by signal (newer than next hour flush) */
		boolean pending_upd;	/* If verify active, used to indicate an update is pending */

		public OurBreakerState() {
			last_amps_x10 = -1;
			last_tripped = false;
			was_signalled = false;
			pending_upd = false;
		}
	}

	private static class OurMsgAccum {	/* Need one for each reader to maintain sequentiality */
		private int[] accum;		/* Accumulator for message bytes */
		private int num_accum;		/* Number of bytes accumulated */
		private long last_ts;

		public OurMsgAccum() {
			accum = new int[6];
			num_accum = 0;
			last_ts = 0;
		}
	}

	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		private HashMap<String, OurMsgAccum> rdraccum;
		private int[]	accum;

		private StringBuilder serial_sb;
		private String serial;
		private StringBuilder model_sb;
		private String model;
		private OurPhaseState phasetags[];
		private OurOutletState outtags[];
		private OurBreakerState brktags[];
		private boolean initdone;
		private boolean new10min;
		private boolean newhourly;
		private int phase_cfg; 	/* 0=none, 1=wye, 2=delta */
		private static final int PHASE_CFG_NONE = 0;
		private static final int PHASE_CFG_WYE = 1;
		private static final int PHASE_CFG_DELTA = 0;
		/* Message state info - used to detect 10-minute and hour boundaries */
		private int last_phase_index;	/* Index reported by last phase msg */
		private int last_phase_id;		/* Phase ID reported by last phase msg */
		private int last_outlet_index;	/* Last outlet index reported (pwruse msg) */
		private int last_breaker_index;	/* Last breaker index reported */
		private int last_outletpf_index;	/* Last outlet index (PF message) */
		private int msg_cnt;
		private boolean did_low_batt;	/* If we saw low battery during hour */

		long	watthours_ts;	/* Start time of watthours accumulator */
		double base_watthours;	/* total at start time watthours */
		double base_vahours;	/* total at start time VA-hours */

		private int PHASE_WATTHOURS_MAX = 0x3FFFFF;
		private int OUTLET_WATTHOURS_MAX = 0xFFFF;
		private int PHASE_VOLTS_NA = 0x1FF;
		private int PHASE_PF_NA = 0x7F;
		private int OUTLET_PF_NA = 0x7F;

		public OurTagState(MantisIITag tag) {
			serial_sb = new StringBuilder();
			model_sb = new StringBuilder();
			resetDeviceConfiguration(tag, false);
			msg_cnt = -1;
			rdraccum = new HashMap<String, OurMsgAccum>();
			accum = null;
		}
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t) {
			cleanupPhases();		/* Cleanup phases */
			cleanupOutlets();		/* Cleanup outlets */
			cleanupBreakers();		/* Cleanup breakers */
			initdone = false;
		}
		/* Cleanup breakers */
		private void cleanupBreakers() {
			if(brktags != null) {
				for(int i = 0; i < brktags.length; i++) {
					if(brktags[i] != null) {
						if(brktags[i].tag != null) {
							brktags[i].tag.lockUnlockTag(false);	/* Unlock it */
							brktags[i].tag = null;
						}
						brktags[i] = null;
					}
				}
				brktags = null;
			}
			initdone = false;
		}
		/* Cleanup outlets */
		private void cleanupOutlets() {			
			if(outtags != null) {
				for(int i = 0; i < outtags.length; i++) {
					if(outtags[i] != null) {
						if(outtags[i].tag != null) {
							outtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							outtags[i].tag = null;
						}
						outtags[i] = null;
					}
				}
				outtags = null;
			}
			initdone = false;
		}
		/* Cleanup phases */
		private void cleanupPhases() {
			if(phasetags != null) {
				for(int i = 0; i < phasetags.length; i++) {
					if(phasetags[i] != null) {
						if(phasetags[i].tag != null) {
							phasetags[i].tag.lockUnlockTag(false);	/* Unlock it */
							phasetags[i].tag = null;
						}
						phasetags[i] = null;
					}
				}
				phasetags = null;
			}
			initdone = false;
			new10min = false;
		}
		/**
		 * Receive message byte to accumulate
		 */
		public boolean accumulateByte(MantisIITag tag, byte data, long ts, boolean first, MantisIIReader rdr) {
			boolean change = false;

			if(rdr == null) return change;
			/* Find and/or init accumulator */
			OurMsgAccum a = rdraccum.get(rdr.getID());
			if(a == null) {
				a = new OurMsgAccum();
				rdraccum.put(rdr.getID(), a);
			}			

			if(first) {	/* If first one */
				a.last_ts = ts;		/* Save ts */
				a.num_accum = 1;
				a.accum[0] = 0xFF & (int)data;
			}
			else {
				long last_int = ts - a.last_ts;

				/* If received more than N times 110% of interval + 60% of interval after start packet */
//				if(last_int > ((11*a.num_accum + 6)*DEF_PKT_INTERVAL/10)) {
				/* If more than 4 seconds early or 4 seconds late, its bad */
				if( (last_int > ((10*a.num_accum + 4)*DEF_PKT_INTERVAL/10)) ||
					(last_int < ((10*a.num_accum - 4)*DEF_PKT_INTERVAL/10))) {
					/* Missing packet, so reset accumulator */
					a.num_accum = 0;
				}
				else if(a.num_accum == 0) {	/* No start, just skip it */
				}
				else {						/* Else, add to accumulator */
					a.accum[a.num_accum] = 0xFF & (int)data;	/* Add it */
					a.num_accum++;
					if(a.num_accum == 6) {	/* Got all of them? */
						/* Check parity count */
						int cnt = 0;
						for(int i = 3; i < 48; i++) {
							if((a.accum[i/8] & (0x80>>(i%8))) != 0) {
								cnt++;
							}
						}
						if((cnt % 4) == (a.accum[0]>>5)) {	/* If message checks */
							a.accum[0] = (a.accum[0] & 0x1F);		/* Trim off parity */
							/* Now, see if its the same message from a different reader than the last message - if so,
							 * its a duplicate we need to ignore */
							if((accum != a.accum) && (accum != null) && Arrays.equals(accum, a.accum)) {
								/* Skip */
							}
							else {
								accum = a.accum;		/* Point to new message, and process it */
								change = processMessage(tag) || change;
							}
						}
						a.num_accum = 0;
					}
				}
			}
			return change;
		}
		/* Read bit range from accumulator */
		private int getBits(int start, int end) {
			int rslt = 0;
			if(start > 44) start = 44;
			if(end < 0) end = 0;
			for(int i = start; i >= end; i--) {
				rslt = rslt << 1;
				if((accum[5 - (i>>3)] & (1 << (i & 7))) != 0) {
					rslt |= 1;
				}
			}
			return rslt;		
		}
		private int getBit(int s) {
			return getBits(s, s);
		}
		private static final String outletcfg[] = { "N/A", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1", "reserved" };


		/**
	     * Device configuration reset - due to model change, serial change, or other configuration inconsistency
		 */
		private boolean	resetDeviceConfiguration(MantisIITag tag, boolean change) {
			cleanupPhases();		/* Cleanup phases */
			cleanupOutlets();		/* Cleanup outlets */
			cleanupBreakers();		/* Cleanup breakers */
			serial = null;
			model = null;
			initdone = false;
			last_phase_index = -1;
			last_phase_id = -1;
			last_outlet_index = -1;
			last_breaker_index = -1;
			last_outletpf_index = -1;
			watthours_ts = 0;
			base_watthours = 0.0;
			base_vahours = 0.0;
			phase_cfg = PHASE_CFG_NONE;
			/* Clean out PDU settings */
			change = updateNull(tag, PDUMODEL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIAL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTRUEPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPOWER_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUPWRFACT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUAPPPWRHOURS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUWATTHOURSTS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIALLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERCOUNT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMSGLOSS_ATTRIB_INDEX) || change;

			return change;
		}
		/**
		 * Flush hour data, if any accumulated
		 */
		private boolean	flushHourData(MantisIITag tag, boolean change) {
			int i;
			change = flush10MinuteData(tag, change);
			if(!newhourly)
				return change;
//			System.out.println("Flush hour data");

			last_outlet_index = -1;
			last_breaker_index = -1;
			last_outletpf_index = -1;
			newhourly = false;

			if(!initdone)
				return change;

			Double volts[] = new Double[8];	/* Voltage, indexed by configuration */
			/* Loop through phases, and get voltages */
			for(i = 0; i < phasetags.length; i++) {
				/* Do L-N for given phase */
				volts[i+1] = (phasetags[i].last_volts_idx0 >= 0)?Double.valueOf(phasetags[i].last_volts_idx0):null;
				/* If 3 phase, and we have needed values, compute L-L+1 */
				if((phasetags.length == 3) && (phasetags[i].last_volts_idx0 >= 0) && 
					(phasetags[(i+1)%3].last_volts_idx0 >= 0)) {
					volts[4+i] = Double.valueOf(Math.round(
						Math.sqrt(phasetags[i].last_volts_idx0*phasetags[i].last_volts_idx0 +
						phasetags[i].last_volts_idx0*phasetags[(i+1)%3].last_volts_idx0 +
						phasetags[(i+1)%3].last_volts_idx0*phasetags[(i+1)%3].last_volts_idx0)));
				}
			}
			/* Loop through outlets */
			for(i = 0; i < outtags.length; i++) {
				Double svolts = (outtags[i].last_cfg > 0)?volts[outtags[i].last_cfg]:null;
				Double struepwr = (outtags[i].last_watts >= 0)?Double.valueOf(outtags[i].last_watts):null;
				Double spowfact = (outtags[i].last_pf >= 0)?Double.valueOf(outtags[i].last_pf):null;
				Double sapppwr = null;
				Double samps = null;
				String cfg = (outtags[i].last_cfg > 0)?outletcfg[outtags[i].last_cfg]:null;
				Double wh = (outtags[i].total_watthours >= 0)?Double.valueOf(outtags[i].total_watthours):null;
				Long	wh_ts = (outtags[i].watthours_ts > 0)?Long.valueOf(outtags[i].watthours_ts):null;
				Double vah = (outtags[i].total_vahours >= 0)?Double.valueOf(outtags[i].total_vahours):null;

				/* If watt-hours added, accumulate with VA-hours if PF is available */
				if(outtags[i].last_diff > 0.0) {
					double pf = (spowfact != null)?spowfact.doubleValue():0.0;
					if(pf != 0.0) {
						if(pf < 0.0)
							outtags[i].total_vahours = outtags[i].total_vahours + outtags[i].last_diff;
						else
							outtags[i].total_vahours = outtags[i].total_vahours + 
								(0.1 * Math.round(100.0 * 10.0 * outtags[i].last_diff / pf));
					}
					outtags[i].last_diff = 0.0;
				}
				/* If true power and power factor, compute apparent power */
				if(struepwr != null) {
					double pf = (spowfact != null)?spowfact.doubleValue():-1.0;
					if((pf > 0.0) && (pf <= 100.0))
						sapppwr = Double.valueOf(Math.round(struepwr.doubleValue() * 100.0 / spowfact.doubleValue()));
					else if(pf == 0.0) {		/* If zero, apparent power is undefined */
						sapppwr = null;
					}	
					else
						sapppwr = struepwr;	/* Undefined - assume apparent power equals true power */
					if((sapppwr != null) && (svolts != null)) {	/* If we have volts, do amps */
						samps = Double.valueOf(0.1 * Math.round(10.0 * sapppwr.doubleValue() / svolts.doubleValue()));
					}
				}
				PduOutletTagSubType.updateTag(outtags[i].tag, 
					(svolts!=null)?svolts:PduOutletTagSubType.VALUE_NA,
					(struepwr!=null)?struepwr:PduOutletTagSubType.VALUE_NA,
					(spowfact!=null)?spowfact:PduOutletTagSubType.VALUE_NA,
					(sapppwr!=null)?sapppwr:PduOutletTagSubType.VALUE_NA,
					(samps!=null)?samps:PduOutletTagSubType.VALUE_NA,
					cfg, 
					(wh!=null)?wh:PduOutletTagSubType.VALUE_NA,
					(wh_ts!=null)?wh_ts:PduOutletTagSubType.TS_VALUE_NA,
					(vah!=null)?vah:PduOutletTagSubType.VALUE_NA);
			}
			/* Loop for breakers */
			for(i = 0; i < brktags.length; i++) {
				Double amps = (brktags[i].last_amps_x10 >= 0)?Double.valueOf(brktags[i].last_amps_x10/10.0):PduBreakerTagSubType.VALUE_NA;
				Boolean tripped = brktags[i].was_signalled?Boolean.valueOf(brktags[i].last_tripped):null;
				PduBreakerTagSubType.updateTag(brktags[i].tag, amps, tripped);
				brktags[i].was_signalled = false;	/* Clear triggered flag */
			}
			/* Compute new message loss rate */
			if(msg_cnt >= 0) {	/* 60 messages per hour nominal */
					double v;
					if(did_low_batt)			/* If low battery signalled? */
						v = Math.round(100.0 - (msg_cnt/0.51));	/* 51 msgs per hour */
					else
						v = Math.round(100.0 - (msg_cnt/0.60));	/* 60 msgs per hour */
					if(v < 0.0) v = 0.0;
		            change = updateDouble(tag, PDUMSGLOSS_ATTRIB_INDEX, v) || change;
//					System.out.println(tag.getTagGUID() + ": msg_cnt=" + msg_cnt + ", loss=" + v);
			}

			/* Clear message count */
			msg_cnt = 0;
			did_low_batt = false;	/* Reset flag */

			return change;
		}
		/**
		 * Flush 10 minute data, if any accumulated
		 */
		private boolean	flush10MinuteData(MantisIITag tag, boolean change) {
			if(!new10min)
				return change;
//			System.out.println("Flush 10-min data");

			new10min = false;
			
			if(!initdone)
				return change;

			int i;
			double tot_tp = 0.0;
			double tot_ap = 0.0;
			double tot_wh = 0.0;
			double tot_vah = 0.0;
			/* Loop through our phases, and compute updates for each */
			for(i = 0; i < phasetags.length; i++) {
				Double vn = (phasetags[i].last_volts >= 0)?Double.valueOf(phasetags[i].last_volts):null;
				Double vp = null;
				Double pf = (phasetags[i].last_pf >= 0)?Double.valueOf(phasetags[i].last_pf):null;
				Double tp = (phasetags[i].last_watts >= 0)?Double.valueOf(phasetags[i].last_watts):null;
				Double ap = null;
				Double amps = null;
				Double wh = (phasetags[i].total_watthours >= 0)?Double.valueOf(phasetags[i].total_watthours):null;
				Long	wh_ts = (phasetags[i].watthours_ts > 0)?Long.valueOf(phasetags[i].watthours_ts):null;
				Double vah = (phasetags[i].total_vahours >= 0)?Double.valueOf(phasetags[i].total_vahours):null;
				String cfg = null;

				if(phase_cfg == PHASE_CFG_WYE) {	/* Y config? */
					cfg = outletcfg[1+i];
				}
				else if(phase_cfg == PHASE_CFG_DELTA) {	/* Delta config? */
					cfg = outletcfg[4+i];
				}
				/* If we have true power and power factor, compute apparent power */
				if(tp != null) {
					double fact = (pf != null)?pf.doubleValue():-1.0;
					if((fact > 0.0) && (fact <= 100.0))
						ap = Double.valueOf(Math.round(tp.doubleValue() * 100.0 / pf.doubleValue()));
					else if(fact == 0.0)			/* Undefined apparent power at 0% PF */
						ap = null;
					else	/* Undefined PF imples apparent power = active power (best guess) */
						ap = tp;
					if((ap != null) && (vn != null)) {
						amps = Double.valueOf(0.1 * Math.round(10.0 * ap.doubleValue() / vn.doubleValue()));
					}
				}
				/* if 3-phase and we've got voltages for L-N and L+1-N, compute L-L+1 */
				if((phasetags.length == 3) && (vn != null) && (phasetags[(i+1)%3].last_volts >= 0)) {
					vp = Double.valueOf(Math.round(Math.sqrt(phasetags[i].last_volts*phasetags[i].last_volts +
						phasetags[i].last_volts*phasetags[(i+1)%3].last_volts +
						phasetags[(i+1)%3].last_volts*phasetags[(i+1)%3].last_volts)));
				}
				PduPhaseTagSubType.updateTag(phasetags[i].tag, 
					(vn!=null)?vn:PduPhaseTagSubType.VALUE_NA,
					(vp!=null)?vp:PduPhaseTagSubType.VALUE_NA,
					(tp!=null)?tp:PduPhaseTagSubType.VALUE_NA,
					(pf!=null)?pf:PduPhaseTagSubType.VALUE_NA,
					(ap!=null)?ap:PduPhaseTagSubType.VALUE_NA,
					(amps!=null)?amps:PduPhaseTagSubType.VALUE_NA,
					(wh!=null)?wh:PduPhaseTagSubType.VALUE_NA,
					(wh_ts!=null)?wh_ts:PduPhaseTagSubType.TS_VALUE_NA,
					(vah!=null)?vah:PduPhaseTagSubType.VALUE_NA,
					cfg);
				/* Accumulate for all phases */
				if(tp != null) {
					if(tot_tp >= 0.0) tot_tp += tp;
				}
				else tot_tp = -1.0;		/* Can't do total without all phases */
				if(ap != null) {
					if(tot_ap >= 0.0) tot_ap += ap;
				}
				else tot_ap = -1.0;		/* Can't do total without all phases */
				if(wh != null) {
					if(tot_wh >= 0.0) tot_wh += wh;
				}
				else tot_wh = -1.0;
				if(vah != null) {
					if(tot_vah >= 0.0) tot_vah += vah;
				}
				else tot_vah = -1.0;
			}
			/* Update top-level tag data, if we can */
			Object[] vals = new Object[6];
			int[] idx = { PDUTRUEPOWER_ATTRIB_INDEX, PDUAPPPOWER_ATTRIB_INDEX, PDUWATTHOURS_ATTRIB_INDEX,
				PDUAPPPWRHOURS_ATTRIB_INDEX, PDUWATTHOURSTS_ATTRIB_INDEX, PDUPWRFACT_ATTRIB_INDEX };
			vals[0] = (tot_tp >= 0.0)?Double.valueOf(tot_tp):null;
			vals[1] = (tot_ap >= 0.0)?Double.valueOf(tot_ap):null;
			if((tot_ap > 0.0) && (tot_tp >= 0.0)) {
				double pf = Math.round(100.0 * tot_tp / tot_ap);
				if(pf < 0.0) pf = 0.0;
				if(pf > 100.0) pf = 100.0;
				vals[5] = Double.valueOf(pf);
			}
			if(tot_wh >= 0.0) {	/* We have a total */
				if(watthours_ts == 0) {	/* First time? */
					watthours_ts = System.currentTimeMillis();
					base_watthours = tot_wh;
					if(tot_vah >= 0.0)
						base_vahours = tot_vah;
					else
						base_vahours = 0.0;
				}
				vals[2] = Double.valueOf(tot_wh - base_watthours);
				vals[4] = Long.valueOf(watthours_ts);
				if(tot_vah >= 0.0) {
					vals[3] = Double.valueOf(tot_vah - base_vahours);
				}
			}
			else {
				watthours_ts = 0;
				base_watthours = 0.0;
				base_vahours = 0.0;
				vals[2] = null;
				vals[3] = null;
				vals[4] = null;
			}
			for(i = 0; i < vals.length; i++) {
				if(vals[i] == null)
					change = updateNull(tag, idx[i]) || change;
				else if(vals[i] instanceof Double)
					change = updateDouble(tag, idx[i], (Double)vals[i]) || change;
				else
					change = updateLong(tag, idx[i], ((Long)vals[i]).longValue()) || change;
			}
			return change;
		}

		/**
		 * Initialize device if needed, and we have full set of configuration data
		 */
		private boolean	initDeviceConfiguration(MantisIITag tag, boolean change) {
			int i;
			TagSubGroup tg;
			if(initdone)
				return change;
			if((model == null) || (serial == null) || (outtags == null) || (phasetags == null) ||
				(brktags == null)) {
				return change;		/* Not ready to init yet */
			}
			/* Set tower count to 1 */
            change = updateLong(tag, PDUTOWERCOUNT_ATTRIB_INDEX, 1) || change;
			/* Tower discoonect not supported, so set these appropriately */
			change = updateBoolean(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, false);
			change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX, 
				new ArrayList<Long>(), false);

			Long feedsetid = Long.valueOf(1);	/* Only one feed line set */
			Long towerid = Long.valueOf(1);		/* Only one tower */
			/* Initialize outlets */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
			for(i = 0; i < outtags.length; i++) {
				outtags[i].tag = SubTag.findOrCreateTag(tg, tag, String.format("outlet%02d",i+1), false,
					new int[] { PduOutletTagSubType.PDUFEEDSETID_INDEX,
						PduOutletTagSubType.PDUTOWERID_INDEX },
					new Object[] { feedsetid, towerid });
				outtags[i].tag.lockUnlockTag(true);
			}
			/* Initialize phases */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
			for(i = 0; i < phasetags.length; i++) {
				phasetags[i].tag = SubTag.findOrCreateTag(tg, tag, String.format("phase%d",i+1), false,
					new int[] { PduPhaseTagSubType.PDUFEEDSETID_INDEX,
						PduPhaseTagSubType.PDUTOWERID_INDEX },
					new Object[] { feedsetid, towerid });
				phasetags[i].tag.lockUnlockTag(true);
			}
			/* Initialize breakers */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduBreakerTagSubType.TAGTYPEID);
			for(i = 0; i < brktags.length; i++) {
				brktags[i].tag = SubTag.findOrCreateTag(tg, tag, String.format("breaker%02d",i+1), false,
					new int[] { PduBreakerTagSubType.PDUFEEDSETID_INDEX,
						PduBreakerTagSubType.PDUTOWERID_INDEX },
					new Object[] { feedsetid, towerid });
				brktags[i].tag.lockUnlockTag(true);
			}
			initdone = true;

			return change;
		}
		/* Handle per phase message */
		private boolean	handlePerPhaseMessage(MantisIITag tag, boolean change, 
			int phase, int index, int volts, int pf, int watthours) {
			if((phasetags == null) || (phase >= phasetags.length))
				return change;
			/* See if we're due to flush hour */
			if((last_phase_index != 0) && (index == 0)) {	/* if first zero index */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index > index) {			/* We missed zero */
				change = flushHourData(tag, change);
			}
			else if(last_phase_index != index) {		/* If new one */
				change = flush10MinuteData(tag, change);
			}
			else if(last_phase_id >= phase) {
				change = flush10MinuteData(tag, change);
			}
			last_phase_index = index;
			last_phase_id = phase;

			if(!initdone)
				return change;

//			System.out.println("Per Phase Voltage/Power: P" + (phase+1) + "-N, period-index="
//				+ index + ", volts=" + volts + "V, PF=" + 
//				((pf==127)?"N/A":(pf + "%")) +
//				", watt-hrs=" + watthours);
			/* Update values */
			if(volts == PHASE_VOLTS_NA)
				volts = -1;		/* Map to N/A value */
			phasetags[phase].last_volts = volts;		/* Update last seen voltage */
			if(index == 0) {	/* If zero value, update it */
				phasetags[phase].last_volts_idx0 = volts;
			}
			else if(phasetags[phase].last_volts_idx0 < 0) {	/* Or if its missing */
				phasetags[phase].last_volts_idx0 = volts;
			}
			if(pf == PHASE_PF_NA)
				pf = -1;		/* Map to N/A power factor */
			phasetags[phase].last_pf = pf;
			
			long ts = System.currentTimeMillis();	/* Get time in milliseconds */
			int diff = 0;
			if(watthours < PHASE_WATTHOURS_MAX) {	/* If valid watthours value */
				if(phasetags[phase].last_watthours >= 0) {	/* If we had old value */
					long elapsed = ts - phasetags[phase].last_watthours_ts;
					if(elapsed <= 0) elapsed = 600000;	/* Assume 10 minutes if bad */
					diff = (watthours + PHASE_WATTHOURS_MAX - phasetags[phase].last_watthours) %
						PHASE_WATTHOURS_MAX;
					/* Adjust for period between readings */
					phasetags[phase].last_watts = Math.round(diff * 3600000.0 / (double)elapsed);
				}
				phasetags[phase].last_watthours = watthours;
				phasetags[phase].last_watthours_ts = ts;	/* Update timestamp */
				if(phasetags[phase].total_watthours < 0.0) {	/* If no accumulator, start one */
					phasetags[phase].total_watthours = 0.0;
					phasetags[phase].total_vahours = 0.0;
					phasetags[phase].watthours_ts = ts;			/* Save time reference */
				}
				else {
					phasetags[phase].total_watthours += diff;
					if(pf != 0) {
						if(pf < 0) {	/* Assume apparent=active, if PF is undefined */
							phasetags[phase].total_vahours += diff;
						}
						else {
							phasetags[phase].total_vahours += 
								0.1 * Math.round(10.0 * 100.0 * diff / pf);
						}
					}
				}
			}
			else {	/* Else, invalid watt-hours, reset accumulator */
				phasetags[phase].last_watthours = -1;
				phasetags[phase].last_watthours_ts = -1;
				phasetags[phase].last_watts = -1.0;
				phasetags[phase].total_watthours = -1.0;
				phasetags[phase].total_vahours = -1.0;
				phasetags[phase].watthours_ts = 0;
			}
			new10min = newhourly = true;

			return change;
		}
		/* Handle PDU model/serial messages */
		private boolean	handlePDUSerialMessage(MantisIITag tag, boolean change) {
			int idx = getBits(38,37);
			int i;
//			System.out.print("PDU serial " + (idx+1) + ":");
			if(serial_sb.length() < ((idx+1) * 6))
				serial_sb.setLength((idx+1) * 6);
			for(i = 0; i < 5; i++) {
				serial_sb.setCharAt((6*idx+i), (char)(0x20 + getBits(35-(i*6), 30-(i*6))));
			}
			if(getBit(36) != 0) {	/* If end */
				serial_sb.setLength(serial_sb.length()-1);
				if(serial_sb.indexOf("\0") < 0) {	/* No nulls left? */
					String newserial = serial_sb.toString().trim();
					/* If different from old one, reset configuration */
					if((serial != null) && (!serial.equals(newserial))) {
						change = resetDeviceConfiguration(tag, change);
						serial = newserial;
					}
					else if(serial == null)
						serial = newserial;
		            /* Set serial */
		            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX,
		                serial) || change;
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					sn_lst.add(serial);
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) 
						|| change;

//					System.out.print(" serial=" + serial);
					serial_sb.setLength(0);
				}
				int nout = getBits(5, 0);
//				System.out.print(" #outlets=" + nout);
				/* If mismatch on number of outlets, reset device configuration */
				if((outtags != null) && (outtags.length != nout)) {
					change = resetDeviceConfiguration(tag, change);
				}
				if(outtags == null) {
					tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
					outtags = new OurOutletState[nout];
					for(i = 0; i < outtags.length; i++) {
						outtags[i] = new OurOutletState();
					}
				}
				/* Initialize device configuration, if needed and ready */
				change = initDeviceConfiguration(tag, change);
			}
			else {
				serial_sb.setCharAt((6*idx+5), (char)(0x20 + getBits(5, 0)));
			}
//			System.out.println();
			return change;
		}
		/* Handle PDU model message */
		private boolean handlePDUModelMessage(MantisIITag tag, boolean change) {
			int i;
			int idx = getBits(38,37);
//			System.out.print("PDU model " + (idx+1) + ": ");
			if(model_sb.length() < ((idx+1) * 6))
				model_sb.setLength((idx+1) * 6);
			for(i = 0; i < 5; i++) {
				model_sb.setCharAt((6*idx+i), (char)(0x20 + getBits(35-(i*6), 30-(i*6))));
			}
			if(getBit(36) != 0) {	/* If end */
				model_sb.setLength(model_sb.length()-1);
				if(model_sb.indexOf("\0") < 0) {	/* No nulls left? */
					String nmodel = model_sb.toString().trim();
					/* If mismatch, reset configuration */
					if((model != null) && (!model.equals(nmodel))) {
						change = resetDeviceConfiguration(tag, change);
						model = nmodel;
					}
					else if(model == null)
						model = nmodel;
		            /* Set model */
		            change = updateString(tag, PDUMODEL_ATTRIB_INDEX,
		                model) || change;
					/* Update model number list */
					List<String> mn_lst = new ArrayList<String>();
					mn_lst.add(model);
		            change = updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, mn_lst) 
						|| change;
//					System.out.print(" model=" + model);
					model_sb.setLength(0);
				}
				int nphases = getBits(1,0);
				int nbreak = getBits(5,2);
//				System.out.print(" #breakers=" + nbreak + " #phases=" + nphases);
				/* If mismatch on number of phases */
				if((phasetags != null) && (phasetags.length != nphases)) {
					change = resetDeviceConfiguration(tag, change);
				}
				if(phasetags == null) {
					phasetags = new OurPhaseState[nphases];
					for(i = 0; i < phasetags.length; i++) {
						phasetags[i] = new OurPhaseState();
					}
				}
				/* If mismatch on number of breakers */
				if((brktags != null) && (brktags.length != nbreak)) {
					change = resetDeviceConfiguration(tag, change);
				}
				if(brktags == null) {
					brktags = new OurBreakerState[nbreak];
					for(i = 0; i < brktags.length; i++) {
						brktags[i] = new OurBreakerState();
					}
				}
				/* Initialize device configuration, if needed and ready */
				change = initDeviceConfiguration(tag, change);
			}
			else {
				model_sb.setCharAt((6*idx+5), (char)(0x20 + getBits(5, 0)));
			}
//			System.out.println();
			return change;
		}
		/* Handle per-outlet power use message */
		private boolean handlePerOutletPowerUseMessage(MantisIITag tag, boolean change,
			int oindex, int cfg0, int watthours0, int cfg1,	int watthours1) {
			/* If this is below last one, flush hour */
			if(oindex < last_outlet_index) {
				change = flushHourData(tag, change);
			}				
			last_outlet_index = oindex;

			if(!initdone)
				return change;

//			System.out.println("Per Outlet Watt-Hour/Cfg:");

			long ts = System.currentTimeMillis();	/* Get time in milliseconds */

			for(int i = 2*oindex; i < 2*oindex+2; i++) {
				int cfg;
				int wh;
				if((i&1) == 0) {
					cfg = cfg0;
					wh = watthours0;
				}
				else {
					cfg = cfg1;
					wh = watthours1;
				}
				if((i < outtags.length) && (cfg != 0)) {
					outtags[i].last_cfg = cfg;	/* Save configuration */

					if((phase_cfg == PHASE_CFG_NONE) && (cfg > 0)) {
						switch(outtags[i].last_cfg) {
							case 1:
							case 2:
							case 3:
								phase_cfg = PHASE_CFG_WYE;
								break;
							case 4:
							case 5:
							case 6:
								phase_cfg = PHASE_CFG_DELTA;
								break;
						}
					}
					int diff = 0;
					if(wh < OUTLET_WATTHOURS_MAX) {	/* If valid watthours value */
						if(outtags[i].last_watthours >= 0) {	/* If we had old value */
							long elapsed = ts - outtags[i].last_watthours_ts;
							if(elapsed <= 0) elapsed = 3600000;	/* Assume 1 hour if bad */
							diff = (wh + OUTLET_WATTHOURS_MAX - outtags[i].last_watthours) %
								OUTLET_WATTHOURS_MAX;
							/* Adjust for period between readings */
							outtags[i].last_watts = Math.round(diff * 3600000.0 / (double)elapsed);
						}
						outtags[i].last_watthours = wh;
						outtags[i].last_watthours_ts = ts;
	
						if(outtags[i].total_watthours < 0.0) {	/* If no accumulator, start one */
							outtags[i].total_watthours = 0.0;
							outtags[i].total_vahours = 0.0;
							outtags[i].watthours_ts = ts;		/* Save time reference */
							outtags[i].last_diff = 0.0;
						}
						else {
							outtags[i].total_watthours += diff;
							outtags[i].last_diff = diff;	/* Save diff - accumulate VA-hrs once we have PF */
						}
//						System.out.println("  outlet " + i + ": cfg=" 
//							+ outletcfg[cfg] + ", watthrs="
//							+ wh);
					}
					else {
						outtags[i].last_watthours = -1;
						outtags[i].last_watthours_ts = -1;
						outtags[i].last_watts = -1.0;
						outtags[i].total_watthours = -1.0;
						outtags[i].total_vahours = -1.0;
						outtags[i].last_diff = 0.0;
						outtags[i].watthours_ts = 0;
					}
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle per-outlet power factor message */
		private boolean handlePerOutletPowerFactorMessage(MantisIITag tag, boolean change) {
			int ol = getBits(38,35);

			/* If this is below last one, flush hour */
			if(ol <= last_outletpf_index) {
				change = flushHourData(tag, change);
			}				
			last_outletpf_index = ol;

			if(!initdone)
				return change;

//			System.out.println("Per Outlet Power Factor:");
			for(int i = 0; i < 5; i++) {
				int ol_id = (5*ol + i);

				if(ol_id < outtags.length) {
					int pf = getBits(34-(i*7), 28-(i*7));

					if(pf == OUTLET_PF_NA)
						pf = -1;

					outtags[ol_id].last_pf = pf;

//					if(pf != 0x7F) {
//						System.out.println("  outlet " + (5*ol+i) + ": pf=" + pf + "%");
//					}
//					else {
//						System.out.println("  outlet " + (5*ol+i) + ": pf=N/A");
//					}
				}
			}
			newhourly = true;

			return change;
		}
		/* Handle per-breaker message */
		private boolean handlePerBreakerMessage(MantisIITag tag, boolean change) {
			int ol = getBits(41,40);

			/* If this is below last one, flush hour */
			if(ol <= last_breaker_index) {
				change = flushHourData(tag, change);
			}				
			last_breaker_index = ol;

			if(!initdone)
				return change;

//			System.out.println("Per Breaker:");
			for(int i = 0; i < 5; i++) {
				int idx = 5*ol + i;
				if(idx >= brktags.length)
					continue;
//				System.out.print("  breaker " + (5*ol+i) + ":");
				int cur = getBits(39-(i*8), 32-(i*8));
				if(cur == 0xFF) {
//					System.out.println("status=tripped, current=0.0");
					brktags[idx].last_amps_x10 = 0;
					if(brktags[idx].was_signalled) {	/* If signalled, ignore this update (too old) */
					}
					else {
						brktags[idx].last_tripped = true;
						brktags[idx].was_signalled = true;
					}
				}
				else {
//					System.out.println("status=normal, current=" + (0.1 * cur));
					brktags[idx].last_amps_x10 = cur;
					if(brktags[idx].was_signalled) {	/* If signalled, ignore this update (too old) */
					}
					else {
						brktags[idx].last_tripped = false;
						brktags[idx].was_signalled = true;
					}
				}
			}
			newhourly = true;

			return change;
		}
		/* Process valid message buffer */
		private boolean processMessage(MantisIITag tag) {
			boolean change = false;
			String mtype = null;

//			System.out.print("message: ");
//			for(i = 0; i < 6; i++) {
//				System.out.print(String.format("%02x", accum[i]));
//			}
//			System.out.println();
			/* If we've hit first hour flush, count messages */
			if(msg_cnt >= 0)
				msg_cnt++;
			if(getBits(44, 43) == 0x03) {	/* If 44-43 = 1-1, Per Phase Voltage */
				int ph = getBits(42,41);
				if(ph != 0) {
					change = handlePerPhaseMessage(tag, change, ph-1, getBits(40,38), getBits(37,29),
						getBits(28,22), getBits(21,0));
				}
				mtype = "per-phase-volts:" + ph;
			}
			/* If 44-40 = 00010, PDU serial or model */
			else if(getBits(44,40) == 0x02) {
				if(getBit(39) == 0) {	/* PDU serial? */
					change = handlePDUSerialMessage(tag, change);
					mtype = "serial-" + getBits(38,37);
				}
				else {
					change = handlePDUModelMessage(tag, change);
					mtype = "model-" + getBits(38,37);
				}
			}
			/* If 44-45 = 0-1, per outlet */
			else if(getBits(44,43) == 0x01) {
				change = handlePerOutletPowerUseMessage(tag, change, getBits(42,38), getBits(37,35), getBits(31,16), 
					getBits(34,32), getBits(15,0));
				mtype = "per-outlet-energy:" + getBits(42,38);
			}
			/* If 44-39 = 001001, per outlet power factor */
			else if(getBits(44,39) == 0x09) {
				change = handlePerOutletPowerFactorMessage(tag, change);
				mtype = "per-outlet-pf:" + getBits(38,35);
			}
			/* If 44-42 = 100, per breaker */
			else if(getBits(44,42) == 0x04) {
				change = handlePerBreakerMessage(tag, change);
				mtype = "per-breaker:" + getBits(41,40);
			}
			else {
//				System.out.println("Unknown message");
				mtype = "unknown";
			}			
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), accum, -1, false, mtype);

			return change;
		}
		/* Handle breaker message */
		public boolean handleBreaker(MantisIITag tag, boolean trip, int brkidx, boolean change, boolean verify) {
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), null, brkidx, trip);
			/* If valid */
			if((brktags != null) && (brkidx < brktags.length)) {
				if(brktags[brkidx].last_tripped == trip) {	/* Same as before? */
					brktags[brkidx].pending_upd = false;	/* Nothing pending */
				}
				else {
					/* If unverified, or already pending, do update */
					if((!verify) || brktags[brkidx].pending_upd) {
						brktags[brkidx].last_tripped = trip;		/* Update value */
						brktags[brkidx].was_signalled = true;		/* Mark as updated */
//						System.out.println("Breaker event! idx=" + brkidx + ", tripped=" + trip);
						PduBreakerTagSubType.updateTag(brktags[brkidx].tag, null, Boolean.valueOf(trip));
						brktags[brkidx].pending_upd = false;	/* Nothing pending */					
						change = true;
					}
					else {	/* Else, verifying and this is first request */
						brktags[brkidx].pending_upd = true;
					}
				}
			}
			return change;
		}
	}
    /**
     * 
     * Constructor
     */
    public MantisIITagType04L() {
        super(4, 'L', 0);
        setLabel("RF Code Mantis II Tag - Treatment 04L");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting payload 
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(tag);
			tag.setTagState(ts);
			change = true;
		}
        /* If payload is flags payload */
        if((cur_payload >= MIN_FLAGS_PAYLOAD) && (cur_payload <= MAX_FLAGS_PAYLOAD)) {
            /* Set low battery flag - verify on set*/
			boolean v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Remember that we saw low battery during hour - 7 vs 6 beacons per message */
			if((cur_payload & 0x02) != 0)
				ts.did_low_batt = true;
            /* Set disconnect flag - verify on set */
			v = ((cur_payload & 0x01) != 0);
			boolean dchg = updateBooleanVerify(tag, PDUDISCONNECT_ATTRIB_INDEX, v, verify && v);
            change |= dchg;
			/* If disconnected, reset the device configuration */
			if(v && dchg)
				change = ts.resetDeviceConfiguration(tag, change);
        }
        /* If start of message */
        else if((cur_payload >= MIN_START_PAYLOAD) && (cur_payload <= MAX_START_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0x7F), cur_timestamp, true, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If continuation of payload */
		else if((cur_payload >= MIN_CONT_PAYLOAD) && (cur_payload <= MAX_CONT_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0xFF), cur_timestamp, false, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If breaker payload */
		else if((cur_payload >= MIN_BREAKER_PAYLOAD) && (cur_payload <= MAX_BREAKER_PAYLOAD)) {
			change = ts.handleBreaker(tag, (cur_payload & 0x10) != 0, cur_payload & 0x0F, change, verify);
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}

        return change;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return SUBTYPES;
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
