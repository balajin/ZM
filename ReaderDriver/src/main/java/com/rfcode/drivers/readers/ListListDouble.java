package com.rfcode.drivers.readers;

import java.util.List;
import java.util.ArrayList;
/**
 * Special list subclass for list-of-double-lists (used for coordinate lists)
 * 
 * @author Mike Primm
 */
public class ListListDouble extends ArrayList<List<Double>> {
	private static final long serialVersionUID = 2788134829989437352L;

}
