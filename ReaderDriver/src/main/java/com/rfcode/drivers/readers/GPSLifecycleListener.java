package com.rfcode.drivers.readers;

/**
 * GPS lifecycle listener - used for reporting GPS creation and deletion events.
 * 
 * @author Mike Primm
 */
public interface GPSLifecycleListener {
    /**
     * GPS created notification. Called after GPS has been created. Any initial
     * attributes will have been added by the time this is called.
     * 
     * @param gps -
     *            GPS being created
     */
    public void gpsLifecycleCreate(GPS gps);
    /**
     * GPS deleted notification. Called at start of GPS cleanup(), when GPS is
     * about to be deleted.
     * 
     * @param gps -
     *            GPS being deleted
     */
    public void gpsLifecycleDelete(GPS gps);
}
