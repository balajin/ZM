package com.rfcode.drivers.readers.mantis2;

import java.util.List;

/**
 * Handler interface for callbacks delivering responses to command requests
 * 
 * @author Mike Primm
 */
public interface MantisIIReaderCommandResponseListener {
    /**
     * Callback method delivering command response strings
     * 
     * @param sess -
     *            Reader session completing command
     * @param rsp -
     *            list of response strings for command
     * @param completed -
     *            true if response completed, false if timeout or failure to get
     *            response
     */
    public void commandResponseCallback(MantisIIReaderSession sess,
        List<String> rsp, boolean completed);
}
