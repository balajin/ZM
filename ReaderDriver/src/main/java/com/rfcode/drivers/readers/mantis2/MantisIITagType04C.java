package com.rfcode.drivers.readers.mantis2;
import com.rfcode.drivers.readers.Tag;

/**
 * Tag type for treatment 04C tags - Mantis II Tags - Treatment 04C
 * 
 * @author Mike Primm
 */
public class MantisIITagType04C extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04C";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.IRLOCATOR_ATTRIB, MantisIITagType.MOTION_ATTRIB,
        MantisIITagType.TAMPER_ATTRIB, MantisIITagType.DOOR_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB};
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.IRLOCATOR_ATTRIB_LABEL, MantisIITagType.MOTION_ATTRIB_LABEL,
        MantisIITagType.TAMPER_ATTRIB_LABEL, MantisIITagType.DOOR_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL};
    private static final int IRLOCATOR_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int MOTION_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int TAMPER_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int DOOR_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int LOWBATT_ATTRIB_INDEX = 4; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {null, Boolean.FALSE,
        Boolean.FALSE, Boolean.FALSE, Boolean.FALSE};

    /* Payload value - minimum flags payload */
    private static final int MIN_FLAGS_PAYLOAD = 0760;
    /* Previous payloads requested - we want 4 to check for missed flags clear */
    private static final int PREV_PAYLOADS = 4;

    /* Return to normal delay - how long without flags report before auto RTN */
    private static final int RTN_DELAY = 30;    /* 30 seconds longer than longest timeout */
    /**
     * 
     * Constructor
     */
    public MantisIITagType04C() {
        super(4, 'C', PREV_PAYLOADS);
        setLabel("RF Code Mantis II Tag - Treatment 04C");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
        boolean found = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        /* If payload is IR payload */
        if (cur_payload < MIN_FLAGS_PAYLOAD) {
            /* Set IR locator ID */
            String v = Integer.toOctalString(cur_payload);
            if(cur_payload < 010)
                v = "00" + v;
            else if(cur_payload < 0100)
                v = "0" + v;
            change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, v) || change;

            /* Check to see if anything else is still defaulted - need to handle
             * RTN timeout in case our flags are actually clear (and we don't get message)
             */
            if(tag.isDefaultFlagged(MOTION_ATTRIB_INDEX) ||
                tag.isDefaultFlagged(TAMPER_ATTRIB_INDEX) ||
                tag.isDefaultFlagged(DOOR_ATTRIB_INDEX) ||
                tag.isDefaultFlagged(LOWBATT_ATTRIB_INDEX)) {
                enqueueTagForDelay(tag, 2*RTN_DELAY, true); /* Enqueue timeout */
            }
        }
        /* If payload is a flags payload */
        if (cur_payload >= MIN_FLAGS_PAYLOAD) {
			boolean v;
            /* Set motion flag */
			v = ((cur_payload & 0x04) != 0);
            change = updateBoolean(tag, MOTION_ATTRIB_INDEX, v) || change;
            /* Set tamper flag - verify only on setting it */
			v = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, v, verify && v) || change;
            /* Set door flag - verify on either setting or clearing (multibeacon on both) */
			v = ((cur_payload & 0x01) != 0);
            change = updateBooleanVerify(tag, DOOR_ATTRIB_INDEX, v, verify && v) || change;
            /* Set low battery flag - verify only on setting it */
			v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
            /* If we have locator value, make sure we still are receiving */
            if(tag.readTagAttribute(IRLOCATOR_ATTRIB_INDEX) != null) {
                found = false;
                for (int i = 0; i < PREV_PAYLOADS; i++) {
                    if ((payloads[i] < MIN_FLAGS_PAYLOAD) || /* There is one */
                        (payloads[i] < 0)) { /* Or not enough history */
                        found = true;
                    }
                }
                if(!found) {    /* Not found, clear it out */
                    change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, 
                        null) || change;
                }
                /* If we have IR, need to manage timeout for missing 760 */
                if(cur_payload == MIN_FLAGS_PAYLOAD) {  /* Cleared? */
                    enqueueTagForDelay(tag, -1); /* Dequeue if needed */
                }
                else {
                    enqueueTagForDelay(tag, 
                        tag.getLongestAgeOut() + RTN_DELAY); /* Enqueue timeout */
                }
            }
        }
        return change;
    }
    /**
     * Process delayed tag action - callback when delayed tag timeout has elapsed
     * (see AbstractTagType.enqueueTagForDelay()).
     * @param t - delayed tag
     */
    public void processDelayedTag(Tag t) {
        MantisIITag tag = (MantisIITag)t;
        if(tag.getTagLinkCount() == 0)  /* If no links, don't do it (leave attributes alone) */
            return;
        /* We do this when we've missed the flags-clear notice (760) */
        int cnt = getTagAttributes().length;
        Object[] oldvals = new Object[cnt];
        tag.readTagAttributes(oldvals, 0, cnt); /* Read em */
        /* If different, report change */
        if (tag.updateUsingPayload(MIN_FLAGS_PAYLOAD,
            System.currentTimeMillis())) {
            MantisIITagGroup tg = (MantisIITagGroup)tag.getTagGroup();
            tg.reportTagStatusAttributeChange(tag, oldvals);
        }
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
}
