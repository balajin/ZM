package com.rfcode.drivers.readers.mantis2;

/**
 * PDU Phase-specific data tag subtype
 * 
 * @author Mike Primm
 */
public class PduPhaseTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduPhase";

	/* Tag attribute - phase-to-neutral RMS voltage (double) */
	public static final String PDUVOLTS_P2N_ATTRIB = "phVoltsN";
	public static final String PDUVOLTS_P2N_ATTRIB_LABEL = "Volts (phase-to-neutral)";

	/* Tag attribute - phase-to-phase RMS voltage (double) */
	public static final String PDUVOLTS_P2P_ATTRIB = "phVoltsP";
	public static final String PDUVOLTS_P2P_ATTRIB_LABEL = "Volts (phase-to-phase)";

	/* Tag attribute - phase true power (watts) (double) */
	public static final String PDUTRUEPOWER_ATTRIB = "phTruePower";
	public static final String PDUTRUEPOWER_ATTRIB_LABEL = "Active Power";

	/* Tag attribute - phase power factor (%) (double) */
	public static final String PDUPWRFACTOR_ATTRIB = "phPwrFactor";
	public static final String PDUPWRFACTOR_ATTRIB_LABEL = "Power Factor (%)";

	/* Tag attribute - phase apparent power (VA) (double) */
	public static final String PDUAPPPOWER_ATTRIB = "phAppPower";
	public static final String PDUAPPPOWER_ATTRIB_LABEL = "Apparent Power";

	/* Tag attribute - phase amperage (amps) (double) */
	public static final String PDUAMPS_ATTRIB = "phAmps";
	public static final String PDUAMPS_ATTRIB_LABEL = "Amperage";

	/* Tag attribute - phase watt-hours (double) */
	public static final String PDUWATTHOURS_ATTRIB = "phTotalWH";
	public static final String PDUWATTHOURS_ATTRIB_LABEL = "Total True Power (W-h)";

	/* Tag attribute - phase volt-amp-hours (double) */
	public static final String PDUAPPPWRHOURS_ATTRIB = "phTotalVAH";
	public static final String PDUAPPPWRHOURS_ATTRIB_LABEL = "Total Apparent Power (VA-h)";

	/* Tag attribute - phase watt-hours start time (UTC milliseconds) (double) */
	public static final String PDUWATTHOURSTS_ATTRIB = "phTotalPwrTS";
	public static final String PDUWATTHOURSTS_ATTRIB_LABEL = "Total Power Start Time";

	/* Tag attribute - phase configuration (string) */
	public static final String PDUCONFIG_ATTRIB = "phConfig";
	public static final String PDUCONFIG_ATTRIB_LABEL = "Phase Configuration";

	public static final int SUBINDEX_INDEX = 0;
	public static final int PARENTTAG_INDEX = 1;
	public static final int PDUVOLTS_P2N_INDEX = 2;
	public static final int PDUVOLTS_P2P_INDEX = 3;
	public static final int PDUTRUEPOWER_INDEX = 4;
	public static final int PDUPWRFACTOR_INDEX = 5;
	public static final int PDUAPPPOWER_INDEX = 6;
	public static final int PDUAMPS_INDEX = 7;
	public static final int PDUWATTHOURS_INDEX = 8;
	public static final int PDUAPPPWRHOURS_INDEX = 9;
	public static final int PDUWATTHOURSTS_INDEX = 10;
	public static final int PDUCONFIG_INDEX = 11;
	public static final int PDUFEEDSETID_INDEX = 12;
	public static final int PDUTOWERID_INDEX = 13;

	public static final Double VALUE_NA = Double.valueOf(-1.0);
	public static final Long TS_VALUE_NA = Long.valueOf(-1);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, PDUVOLTS_P2N_ATTRIB, 
		PDUVOLTS_P2P_ATTRIB, PDUTRUEPOWER_ATTRIB, PDUPWRFACTOR_ATTRIB,
		PDUAPPPOWER_ATTRIB, PDUAMPS_ATTRIB, PDUWATTHOURS_ATTRIB, 
		PDUAPPPWRHOURS_ATTRIB, PDUWATTHOURSTS_ATTRIB, PDUCONFIG_ATTRIB,
		MantisIITagType.PDUFEEDSETID_ATTRIB, MantisIITagType.PDUTOWERID_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, PDUVOLTS_P2N_ATTRIB_LABEL, 
		PDUVOLTS_P2P_ATTRIB_LABEL, PDUTRUEPOWER_ATTRIB_LABEL, PDUPWRFACTOR_ATTRIB_LABEL,
		PDUAPPPOWER_ATTRIB_LABEL, PDUAMPS_ATTRIB_LABEL, PDUWATTHOURS_ATTRIB_LABEL, 
		PDUAPPPWRHOURS_ATTRIB_LABEL, PDUWATTHOURSTS_ATTRIB_LABEL, PDUCONFIG_ATTRIB_LABEL,
		MantisIITagType.PDUFEEDSETID_ATTRIB_LABEL, MantisIITagType.PDUTOWERID_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null,
		null, null, null,
		null, null, null, 
		null, null, null,
		null, null };
    /**
     * Constructor
     */
    public PduPhaseTagSubType() {
        setLabel("PDU Phase Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double voltsn, Double voltsp,
		Double truepower, Double powfact, Double apppower, Double amps, Double tot_wh, Long wh_ts,
		Double tot_vah, String config) {
		updateTag(tag, voltsn, voltsp, truepower, powfact, apppower, amps, tot_wh, wh_ts, tot_vah, config, null, null);
	}
	public static void updateTag(SubTag tag, Double voltsn, Double voltsp,
		Double truepower, Double powfact, Double apppower, Double amps, Double tot_wh, Long wh_ts,
		Double tot_vah, String config, Long towerid, Long feedsetid) {
		if(tag == null)
			return;
		int[] idx = new int[12];
		Object[] val = new Object[12];
		int cnt = 0;
		if(voltsn != null) {
			idx[cnt] = PDUVOLTS_P2N_INDEX;
			if(voltsn.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = voltsn;
			cnt++;
		}
		if(voltsp != null) {
			idx[cnt] = PDUVOLTS_P2P_INDEX;
			if(voltsp.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = voltsp;
			cnt++;
		}
		if(truepower != null) {
			idx[cnt] = PDUTRUEPOWER_INDEX;
			if(truepower.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = truepower;
			cnt++;
		}
		if(powfact != null) {
			idx[cnt] = PDUPWRFACTOR_INDEX;
			if(powfact.doubleValue() < 0.0) {	/* Invalid? */
				val[cnt] = null;	/* Set to N/A */
			}
			else {
				val[cnt] = powfact;
			}
			cnt++;
		}
		if(apppower != null) {
			idx[cnt] = PDUAPPPOWER_INDEX;
			if(apppower.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = apppower;
			cnt++;
		}
		if(amps != null) {
			idx[cnt] = PDUAMPS_INDEX;
			if(amps.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = amps;
			cnt++;
		}
		if(tot_wh != null) {
			idx[cnt] = PDUWATTHOURS_INDEX;
			if(tot_wh.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = tot_wh;
			cnt++;
		}
		if(wh_ts != null) {
			idx[cnt] = PDUWATTHOURSTS_INDEX;
			if(wh_ts.longValue() < 0)
				val[cnt] = null;	
			else
				val[cnt] = wh_ts;
			cnt++;
		}
		if(tot_vah != null) {
			idx[cnt] = PDUAPPPWRHOURS_INDEX;
			if(tot_vah.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = tot_vah;
			cnt++;
		}
		if(config != null) {
			idx[cnt] = PDUCONFIG_INDEX;
			val[cnt] = config;
			cnt++;
		}
		if(towerid != null) {
			idx[cnt] = PDUTOWERID_INDEX;
			val[cnt] = towerid;
			cnt++;
		}
		if(feedsetid != null) {
			idx[cnt] = PDUFEEDSETID_INDEX;
			val[cnt] = feedsetid;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}

}
