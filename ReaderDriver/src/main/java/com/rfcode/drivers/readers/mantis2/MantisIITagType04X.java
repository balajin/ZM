package com.rfcode.drivers.readers.mantis2;

import java.util.HashMap;
import java.util.Map;

import com.rfcode.drivers.readers.CurrentLoopSensorDefinition;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.TagGroup;

/**
 * Tag type for treatment 04X tags - Mantis II Tags - Treatment 04X (4-20mA)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04X extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04X";

	/** Tag type attribute - current rounding increment */
	public static final String TAGTYPEATTRIB_CURRENTROUND = "currentRound";
	/** Tag type attribute - default sensor definition */
	public static final String TAGTYPEATTRIB_DEFSENSORDEF = "defaultSensorDef";
	
	
    /* Default rounding increment */
    public static final double CURRENTROUND_DEFAULT = 0.01;

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
    	CURRENT_LOOP_READING_ATTRIB,
    	CUSTOM_FIELD_ATTRIB,
        LOWBATT_ATTRIB,
		SENSORDISCONNECT_ATTRIB,
		MSGLOSS_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
    	CURRENT_LOOP_READING_ATTRIB_LABEL,
    	CUSTOM_FIELD_ATTRIB_LABEL,
        LOWBATT_ATTRIB_LABEL,
		SENSORDISCONNECT_ATTRIB_LABEL,
		MSGLOSS_ATTRIB_LABEL
        };
    private static final int CURRENT_LOOP_READING_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int CUSTOM_FIELD_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

	private static final int SENSORDISCONNECT_ATTRIB_INDEX = 3;
	private static final int MSGLOSS_ATTRIB_INDEX = 4;
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, null,
        Boolean.FALSE, Boolean.FALSE, null};

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 18000;
    private static final int MIN_PERIOD = 5000;

	private static final double MAX_UNVERIFIED_MA_DELTA = 0.1;	/* 0.1mA max change without verify */
	
	private static final double MIN_VALID_MA = 0.1;	/* Minimum valid value (mA) */

	private static final int	MSGCOUNT_PERIOD = 60*60*1000;	/* 1 hour */
	private static final int	NOMINAL_BEACON_PERIOD = 10050;	/* 10 seconds + jitter */
	/**
	 * Tag state - use to accumulate message loss data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		long	start_ts;
		int		msg_count;
	
		public OurTagState(long ts) {
			start_ts = ts;
		}
		public void cleanupTag(MantisIITag t) { }
	}

    /**
     * Constructor
     */
    public MantisIITagType04X() {
        super(4, 'X', PREV_PAYLOAD_CNT, new String[] { TAGTYPEATTRIB_CURRENTROUND, TAGTYPEATTRIB_DEFSENSORDEF } );
        setLabel("RF Code Mantis II Tag - Treatment 04X");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_CURRENTROUND, Double.valueOf(CURRENTROUND_DEFAULT));	/* Default to 0.5 */
		defs.put(TAGTYPEATTRIB_DEFSENSORDEF, "");
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);
		/* If needed, intialize our tag state */
		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(cur_timestamp);
			tag.setTagState(ts);
		}

        /* If second half of two part payload (low part) */
        if (cur_payload < 0400) {
			Boolean unplugged = null;
            /* If previous was high value, and not too old */
            if ((payloads[0] >= 0400)
                && ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                int v = ((payloads[0] & 0xFF) << 8) | (cur_payload & 0xFF);
                v >>= 2;     /* Roll off other flags */
                /* Compute current */
                double current = 0.00125 * (double)v;
                /* Apply rounding, if defined */
				Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_CURRENTROUND);
                double rnd = CURRENTROUND_DEFAULT;
                if(tc instanceof Double) {
                    rnd = ((Double)tc).doubleValue();
                }
                if(rnd > 0.0)
                    current = Math.rint(current / rnd) * rnd;
                SensorDefinition sd = ((MantisIISensorDefTag) tag).getSensorDefinition();
                
                if (current <= MIN_VALID_MA) {	// Less than 0.1 mA?
                	unplugged = Boolean.TRUE;
    				change = updateNullVerify(tag, CURRENT_LOOP_READING_ATTRIB_INDEX, verify) || change;
    				if (sd == null) {
        				change = updateNull(tag, CUSTOM_FIELD_ATTRIB_INDEX) || change;
    				}
    				else {
    					change = updateCustomFieldVerify(tag, CUSTOM_FIELD_ATTRIB_INDEX, sd.getDestinationAttrib(), null, verify) || change;
    				}
                }
                else {
                	unplugged = Boolean.FALSE;
                    change = updateDoubleVerify(tag, CURRENT_LOOP_READING_ATTRIB_INDEX, current, verify,
        					MAX_UNVERIFIED_MA_DELTA) || change;
    				if (sd == null) {
        				change = updateNull(tag, CUSTOM_FIELD_ATTRIB_INDEX) || change;
    				}
    				else {
    					change = updateCustomFieldVerify(tag, CUSTOM_FIELD_ATTRIB_INDEX, sd.getDestinationAttrib(), 
    							((CurrentLoopSensorDefinition)sd ).getValue(current), verify) || change;
    				}
                }
				ts.msg_count++;	/* Bump message count */
            }
            /* Get low battery value - verify on setting flag */
			boolean vv = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
			/* Set unplugged status, if needed */
			if(unplugged != null)
				change = updateBooleanVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, unplugged, verify && unplugged) || change;
			/* If same payload as last time, and long enough to not be from another reader, update its timestamp */
			if((payloads[0] == cur_payload) && ((cur_timestamp - payloadage[0]) > MIN_PERIOD))
				payloadage[0] = cur_timestamp;
        }
		/* Now, consider if an hour has elapsed on message count */
		if(ts.start_ts < (cur_timestamp - MSGCOUNT_PERIOD)) {
			int act_count = (2 * ts.msg_count);	/* How many good beacons we received? */
			int exp_count = (MSGCOUNT_PERIOD / NOMINAL_BEACON_PERIOD);
			double pctloss = Math.round(100.0 * (exp_count - act_count) / (double)exp_count);
			if(pctloss < 0.0) pctloss = 0.0;
			if(pctloss > 100.0) pctloss = 100.0;
            change = updateDouble(tag, MSGLOSS_ATTRIB_INDEX, pctloss) || change;
			/* Reset accumulators */
			ts.start_ts = cur_timestamp;
			ts.msg_count = 0;
		}

        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	
    /**
     * Create tag instance
     * @param tg - tag group
     * @param guid - tag guid
     * @return tag
     */
	@Override
    public MantisIITag makeTag(MantisIITagGroup grp, String guid) {
    	return new MantisIISensorDefTag(grp, guid);
    }

	@Override
	public boolean isValidSensorDefinition(SensorDefinition sd) {
		return (sd instanceof CurrentLoopSensorDefinition);
	}
	
    /**
     * Get default sensor definition for group (group matches tag type)
     * @param grp - group
     * @return null if none
     */
	@Override
    public SensorDefinition getDefaultSensorDefinition(MantisIITagGroup grp) {
    	SensorDefinition sd = grp_to_sd.get(grp.getID());
    	if ((sd != null) && (sd.getID() == null)) {	// deleted?
    		grp_to_sd.put(grp.getID(), null);
    		sd = null;
    	}
    	return sd;
    }
	private HashMap<String, SensorDefinition> grp_to_sd = new HashMap<String, SensorDefinition>();
	/**
	 * Notification that tag group attributes have been set/updated
	 * 
	 * @param grp - tag group
	 */
	public void tagGroupAttributesUpdated(TagGroup grp) {
		super.tagGroupAttributesUpdated(grp);
		
		Object tc = grp.getTagGroupAttributes().get(TAGTYPEATTRIB_DEFSENSORDEF);
		SensorDefinition sd = null;
		if (tc instanceof String) {
			sd = ReaderEntityDirectory.findSensorDefinition((String)tc);
		}
		grp_to_sd.put(grp.getID(), sd);
	}
	/**
	 * Notification that tag group has been deleted
	 * 
	 * @param grp - tag group
	 */
    @Override
	public void tagGroupDeleted(TagGroup grp) {
    	super.tagGroupDeleted(grp);
    	
    	grp_to_sd.remove(grp.getID());
    }

}
