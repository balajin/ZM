package com.rfcode.drivers.readers.mantis2;
import com.rfcode.ranger.RangerServer;
import java.util.HashMap;
import java.util.Arrays;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;

/**
 * Tag type for treatment 04N tags - Mantis II Tags - Treatment 04N
 * 
 * @author Mike Primm
 */
public class MantisIITagType04N extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04N";
	/** Our subtypes */
	private static final String[] SUBTYPES = { 
		PduPhaseTagSubType.TAGTYPEID,
		PduOutletTagSubType.TAGTYPEID,
		PduBreakerTagSubType.TAGTYPEID,
		PduFeedLineTagSubType.TAGTYPEID,
		PduChannelTagSubType.TAGTYPEID
	};

	public static final String PDUMODELMATCH_ATTRIB = "pduModelMatch";
	public static final String PDUMODELMATCH_ATTRIB_LABEL = "Model Found";

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        LOWBATT_ATTRIB,
        PDUDISCONNECT_ATTRIB,
		PDUMODEL_ATTRIB,
		PDUSERIAL_ATTRIB,
		PDUMSGLOSS_ATTRIB,
		PDUSERIALLIST_ATTRIB,
		PDUMODELLIST_ATTRIB,
		PDUTOWERDISCONNECT_ATTRIB,
		PDUDISCONNECTEDTOWERS_ATTRIB,
		PDUTOWERCOUNT_ATTRIB,
		PDUMODELMATCH_ATTRIB };
    private static final String[] TAGATTRIBLABS = {
        LOWBATT_ATTRIB_LABEL,
		PDUDISCONNECT_ATTRIB_LABEL,
		PDUMODEL_ATTRIB_LABEL,
		PDUSERIAL_ATTRIB_LABEL,
		PDUMSGLOSS_ATTRIB_LABEL,
		PDUSERIALLIST_ATTRIB_LABEL,
		PDUMODELLIST_ATTRIB_LABEL,
		PDUTOWERDISCONNECT_ATTRIB_LABEL,
		PDUDISCONNECTEDTOWERS_ATTRIB_LABEL,
		PDUTOWERCOUNT_ATTRIB_LABEL,
		PDUMODELMATCH_ATTRIB_LABEL };

    private static final int LOWBATT_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUDISCONNECT_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUMODEL_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PDUSERIAL_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
	private static final int PDUMSGLOSS_ATTRIB_INDEX = 4;

	private static final int PDUSERIALLIST_ATTRIB_INDEX = 5;
	private static final int PDUMODELLIST_ATTRIB_INDEX = 6;
	private static final int PDUTOWERDISCONNECT_ATTRIB_INDEX = 7;
	private static final int PDUDISCONNECTEDTOWERS_ATTRIB_INDEX = 8;
	private static final int PDUTOWERCOUNT_ATTRIB_INDEX = 9;
	private static final int PDUMODELMATCH_ATTRIB_INDEX = 10;

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        Boolean.FALSE, Boolean.FALSE, "", "", null, null, null, Boolean.FALSE, 
		new ArrayList<Long>(), null, null };

	private static final int MIN_FLAGS_PAYLOAD = 0760;
	private static final int MAX_FLAGS_PAYLOAD = 0777;

	private static final int MIN_START_PAYLOAD = 0400;
	private static final int MAX_START_PAYLOAD = 0577;

	private static final int MIN_CONT_PAYLOAD = 0000;
	private static final int MAX_CONT_PAYLOAD = 0377;

	private static final int MIN_ALARM_PAYLOAD = 0600;
	private static final int MAX_ALARM_PAYLOAD = 0637;

	private static final long DEF_PKT_INTERVAL = 10000;	/* 10 seconds */

	private static final String MAPPING_FILENAME = "GeistPDU.mapping";

	/* Bits for different input types associated with channel (can be more than one) */
	private static final int INPUT_TYPE_FEEDLINE = 0x01;
	private static final int INPUT_TYPE_PHASE = 0x02;
	private static final int INPUT_TYPE_BREAKER = 0x04;
	private static final int INPUT_TYPE_CHANNEL = 0x00;	/* Only used if no other matches */

	private static class OurInputState {
		SubTag	channel_tag;	/* Generic channel tag, if any */
		SubTag	breaker_tag;	/* Breaker tag, if any */
		SubTag	phase_tag;		/* Phase tag, if any */
		SubTag	feedline_tag;	/* Feedline tag, if any */
		int		last_amps_x10;	/* Last reported amps, x 10 */
		int		type;			/* Input type, based on mapping */
		String	feedcfg;		/* Config attribute (feedline) */
		String	phasecfg;		/* Config attribute (phase + breaker) */
		Long	feedsetid;		/* Feedline set ID */

		public OurInputState() {
			channel_tag = breaker_tag = phase_tag = feedline_tag = null;
			last_amps_x10 = -1;
			type = INPUT_TYPE_CHANNEL;
			feedcfg = phasecfg = null;
		}
	}

	private static class OurOutletState { 
		SubTag	tag;			/* Tag associated with the given channel */
		int		last_amps_x10;	/* Last reported amps, x 10 */
		String	label;			/* Outlet label */
		String	cfg;			/* Outlet phase config */
		Long	feedsetid;		/* Feedline set ID */

		public OurOutletState() {
			tag = null;
			last_amps_x10 = -1;
			label = null;
			cfg = null;
		}
	}

	private static class OurMsgAccum {	/* Need one for each reader to maintain sequentiality */
		private int[] accum;		/* Accumulator for message bytes */
		private int num_accum;		/* Number of bytes accumulated */
		private long last_ts;

		public OurMsgAccum() {
			accum = new int[6];
			num_accum = 0;
			last_ts = 0;
		}
	}

	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		private HashMap<String, OurMsgAccum> rdraccum;
		private int[]	accum;

		private StringBuilder serial_sb;
		private String serial;
		private StringBuilder model_sb;
		private String model;
		private OurInputState inputtags[];
		private OurOutletState outtags[];
		private boolean	mapping_matched;	/* If true, we found a mapping to match the model */

		private boolean initdone;
		private boolean new10min;
		private boolean newhourly;

		/* Message state info - used to detect 10-minute and hour boundaries */
		private int last_input_index;	/* Index reported by last input msg */
		private int last_input_id;		/* Phase ID reported by last input msg */
		private int last_outlet_index;	/* Last outlet index reported (pwruse msg) */
		private int msg_cnt;
		private boolean did_low_batt;	/* If we saw low battery during hour */

		private int OUTLET_AMPS_MAX = 0x7FF;
		private int INPUT_AMPS_MAX = 0x7FF;

		public OurTagState(MantisIITag tag) {
			serial_sb = new StringBuilder();
			model_sb = new StringBuilder();
			resetDeviceConfiguration(tag, false);
			msg_cnt = -1;
			rdraccum = new HashMap<String, OurMsgAccum>();
			accum = null;
		}
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t) {
			cleanupInputs();		/* Cleanup phases */
			cleanupOutlets();		/* Cleanup outlets */
			initdone = false;
		}
		/* Cleanup inputs */
		private void cleanupInputs() {
			if(inputtags != null) {
				for(int i = 0; i < inputtags.length; i++) {
					if(inputtags[i] != null) {
						if(inputtags[i].channel_tag != null) {
							inputtags[i].channel_tag.lockUnlockTag(false);	/* Unlock it */
							inputtags[i].channel_tag = null;
						}
						if(inputtags[i].feedline_tag != null) {
							inputtags[i].feedline_tag.lockUnlockTag(false);	/* Unlock it */
							inputtags[i].feedline_tag = null;
						}
						if(inputtags[i].phase_tag != null) {
							inputtags[i].phase_tag.lockUnlockTag(false);	/* Unlock it */
							inputtags[i].phase_tag = null;
						}
						if(inputtags[i].breaker_tag != null) {
							inputtags[i].breaker_tag.lockUnlockTag(false);	/* Unlock it */
							inputtags[i].breaker_tag = null;
						}
						inputtags[i] = null;
					}
				}
				inputtags = null;
			}
			initdone = false;
			new10min = false;
		}
		/* Cleanup outlets */
		private void cleanupOutlets() {			
			if(outtags != null) {
				for(int i = 0; i < outtags.length; i++) {
					if(outtags[i] != null) {
						if(outtags[i].tag != null) {
							outtags[i].tag.lockUnlockTag(false);	/* Unlock it */
							outtags[i].tag = null;
						}
						outtags[i] = null;
					}
				}
				outtags = null;
			}
			initdone = false;
		}
		/**
		 * Receive message byte to accumulate
		 */
		public boolean accumulateByte(MantisIITag tag, byte data, long ts, boolean first, MantisIIReader rdr) {
			boolean change = false;

			if(rdr == null) return change;
			/* Find and/or init accumulator */
			OurMsgAccum a = rdraccum.get(rdr.getID());
			if(a == null) {
				a = new OurMsgAccum();
				rdraccum.put(rdr.getID(), a);
			}			

			if(first) {	/* If first one */
				a.last_ts = ts;		/* Save ts */
				a.num_accum = 1;
				a.accum[0] = 0xFF & (int)data;
			}
			else {
				long last_int = ts - a.last_ts;

				/* If received more than N times 110% of interval + 60% of interval after start packet */
//				if(last_int > ((11*a.num_accum + 6)*DEF_PKT_INTERVAL/10)) {
				/* If more than 4 seconds early or 4 seconds late, its bad */
				if( (last_int > ((10*a.num_accum + 4)*DEF_PKT_INTERVAL/10)) ||
					(last_int < ((10*a.num_accum - 4)*DEF_PKT_INTERVAL/10))) {
					/* Missing packet, so reset accumulator */
					a.num_accum = 0;
					a.last_ts = 0;
				}
				else if(a.num_accum == 0) {	/* No start, just skip it */
				}
				else {						/* Else, add to accumulator */
					a.accum[a.num_accum] = 0xFF & (int)data;	/* Add it */
					a.num_accum++;
					if(a.num_accum == 6) {	/* Got all of them? */
						/* Check parity count */
						int cnt = 0;
						for(int i = 3; i < 48; i++) {
							if((a.accum[i/8] & (0x80>>(i%8))) != 0) {
								cnt++;
							}
						}
						if((cnt % 4) == (a.accum[0]>>5)) {	/* If message checks */
							a.accum[0] = (a.accum[0] & 0x1F);		/* Trim off parity */
							/* Now, see if its the same message from a different reader than the last message - if so,
							 * its a duplicate we need to ignore */
							if((accum != a.accum) && (accum != null) && Arrays.equals(accum, a.accum)) {
								/* Skip */
							}
							else {
								accum = a.accum;		/* Point to new message, and process it */
								change = processMessage(tag) || change;
							}
						}
						a.num_accum = 0;
						a.last_ts = 0;
					}
				}
			}
			return change;
		}
		/* Read bit range from accumulator */
		private int getBits(int start, int end) {
			int rslt = 0;
			if(start > 44) start = 44;
			if(end < 0) end = 0;
			for(int i = start; i >= end; i--) {
				rslt = rslt << 1;
				if((accum[5 - (i>>3)] & (1 << (i & 7))) != 0) {
					rslt |= 1;
				}
			}
			return rslt;		
		}
		private int getBit(int s) {
			return getBits(s, s);
		}

		/**
	     * Device configuration reset - due to model change, serial change, or other configuration inconsistency
		 */
		private boolean	resetDeviceConfiguration(MantisIITag tag, boolean change) {
			cleanupInputs();		/* Cleanup inputs */
			cleanupOutlets();		/* Cleanup outlets */
			serial = null;
			model = null;
			initdone = false;
			last_input_index = -1;
			last_input_id = -1;
			last_outlet_index = -1;
			/* Clean out PDU settings */
			change = updateNull(tag, PDUMODEL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIAL_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUSERIALLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELLIST_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUTOWERCOUNT_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMSGLOSS_ATTRIB_INDEX) || change;
			change = updateNull(tag, PDUMODELMATCH_ATTRIB_INDEX) || change;
			return change;
		}
		/**
		 * Flush hour data, if any accumulated
		 */
		private boolean	flushHourData(MantisIITag tag, boolean change) {
			int i;
			change = flush10MinuteData(tag, change);
			if(!newhourly)
				return change;
//			System.out.println(tag.getTagGUID() + ": Flush hour data");

			last_outlet_index = -1;
			last_input_index = -1;
			newhourly = false;

			if(!initdone)
				return change;

			/* Loop through outlets, and update amperages */
			for(i = 0; i < outtags.length; i++) {
				Double samps = null;

				if((outtags[i].last_amps_x10 >= 0) && (outtags[i].last_amps_x10 < OUTLET_AMPS_MAX)) {
					samps = Double.valueOf(0.1 * outtags[i].last_amps_x10);
				}
				PduOutletTagSubType.updateTag(outtags[i].tag, null, null, null,
					null, 
					(samps!=null)?samps:Double.valueOf(PduOutletTagSubType.VALUE_NA),
					outtags[i].cfg, null, null, null, null, outtags[i].label,
					outtags[i].feedsetid);
			}
			/* Compute new message loss rate */
			if(msg_cnt >= 0) {	/* 60 messages per hour nominal */
				double v;
				if(did_low_batt)			/* If low battery signalled? */
					v = Math.round(100.0 - (msg_cnt/0.51));	/* 51 msgs per hour */
				else
					v = Math.round(100.0 - (msg_cnt/0.60));	/* 60 msgs per hour */
				if(v < 0.0) v = 0.0;
	            change = updateDouble(tag, PDUMSGLOSS_ATTRIB_INDEX, v) || change;
				//System.out.println(tag.getTagGUID() + ": msg_cnt=" + msg_cnt + ", loss=" + v);
			}

			/* Clear message count */
			msg_cnt = 0;
			did_low_batt = false;	/* Reset flag */

			return change;
		}
		/**
		 * Flush 10 minute data, if any accumulated
		 */
		private boolean	flush10MinuteData(MantisIITag tag, boolean change) {
			if(!new10min)
				return change;
//			System.out.println(tag.getTagGUID() + ": Flush 10-min data");

			new10min = false;
			
			if(!initdone)
				return change;

			int i;
			/* Loop through our inputs. and compute updates for each */
			for(i = 0; i < inputtags.length; i++) {
				Double amps = null;
				if((inputtags[i].last_amps_x10 >= 0) && (inputtags[i].last_amps_x10 < INPUT_AMPS_MAX)) {
					amps = Double.valueOf(0.1 * inputtags[i].last_amps_x10);
				}
				/* Update, based on type */
				if(inputtags[i].feedline_tag != null) {
					PduFeedLineTagSubType.updateTag(inputtags[i].feedline_tag, 
						(amps!=null)?amps:Double.valueOf(PduFeedLineTagSubType.VALUE_NA),
						inputtags[i].feedcfg, null, null, null,
						inputtags[i].feedsetid);
				}
				if(inputtags[i].phase_tag != null) {
					PduPhaseTagSubType.updateTag(inputtags[i].phase_tag, null, null, null, null, null, 
						(amps!=null)?amps:Double.valueOf(PduPhaseTagSubType.VALUE_NA),
						null, null, null, inputtags[i].phasecfg, null, inputtags[i].feedsetid);
				}
				if(inputtags[i].breaker_tag != null) {
					PduBreakerTagSubType.updateTag(inputtags[i].breaker_tag, 
						(amps!=null)?amps:Double.valueOf(PduBreakerTagSubType.VALUE_NA),
						null, null, inputtags[i].phasecfg,
						inputtags[i].feedsetid);
				}
				if(inputtags[i].channel_tag != null) {
					PduChannelTagSubType.updateTag(inputtags[i].channel_tag, 
						(amps!=null)?amps:Double.valueOf(PduChannelTagSubType.VALUE_NA),
						null);
				}
			}
			return change;
		}
		private void	applyMapping() {
//			System.out.println("applyMapping()");
			mapping_matched = false;
            /* Now load the mapping file, if defined */
            File mapfile = new File(RangerServer.getDataDir(), MAPPING_FILENAME);
            if (!mapfile.exists())		/* No mapping, quit */
				return;
			Properties p = new Properties();
            /* Now, load mapping file into properties object */
            FileInputStream r = null;
            try {
                r = new FileInputStream(mapfile);
                p.load(r); /* Do load */
            } catch (IOException iox) {
                System.err.println("Error loading PDU mapping from "
                    + mapfile.getName());
            } finally {
                if (r != null) {
                    try {
                        r.close();
                    } catch (IOException iox) {
                    }
                    r = null;
                }
            }
//			System.out.println("map loaded");
			/* Now, search for matching models.N */
			int n = 0;
			boolean match = false;
			while(!match) {
				String mod = p.getProperty("models." + n);
				if(mod == null) {	/* No match found? */
					n = -1;
					match = true;
					continue;
				}
				String[] tok = mod.split(",");	/* Split at commas */
				for(String s : tok) {
					s = s.trim();
					if(s.endsWith("*")) {	/* End with wildcard */
						if(model.startsWith(s.substring(0, s.length()-1))) {	/* See if model matches at start */
							match = true;
						}
					}
					else {	/* Else, see if full match */
						if(model.equals(s)) {
							match = true;
						}
					}
				}
				if(!match) n++;
			}
			/* If no match, we're done */
			if(n < 0) {
//				System.out.println("no match");
				return;
			}
//			System.out.println("Match on models." + n);
			mapping_matched = true;	/* Remember that we matched */
			String nstr = String.valueOf(n);	/* Get string representation of index */
			/* Now, traverse properties looking for relevant attributes */
			for(Enumeration<?> e = p.propertyNames(); e.hasMoreElements();) {
				String pn = (String)e.nextElement();
				String val = p.getProperty(pn);
				int ival = -1;
				try {
					ival = Integer.parseInt(val) - 1;	/* Try to parse to integer (usually needed) */
				} catch (NumberFormatException nfx) {
					ival = -1;
				}
				String[] pntok = pn.split("\\.");	/* Split based on period seperators */
				if(pntok.length < 4) {	/* All tokens of interest have at least 3 parts */
					continue;
				}
				if(!pntok[1].equals(nstr)) {	/* Wrong mapping index? */
					continue;
				}
				if(pntok[0].equals("feedline")) {	/* Matching feedline attribute? */
					if((ival >= 0) && (ival < inputtags.length)) { /* Matches an input */
						inputtags[ival].type |= INPUT_TYPE_FEEDLINE;	/* Its a feedline */
						inputtags[ival].feedcfg = pntok[2];	/* Get line ID */
						try { inputtags[ival].feedsetid = Long.valueOf(pntok[3]); } catch (NumberFormatException nfx) {}
					}
				}
				else if(pntok[0].equals("phase")) {	/* Matching phase */
					if((ival >= 0) && (ival < inputtags.length)) { /* Matches an input */
						inputtags[ival].type |= INPUT_TYPE_PHASE;	/* Its a phase */
						inputtags[ival].phasecfg = pntok[2].replace('_','-');
						try { inputtags[ival].feedsetid = Long.valueOf(pntok[3]); } catch (NumberFormatException nfx) {}
					}
				}
				else if(pntok[0].equals("breaker") && (pntok.length == 5)) {	/* Matching breaker */
					if((ival >= 0) && (ival < inputtags.length)) { /* Matches an input */
						inputtags[ival].type |= INPUT_TYPE_BREAKER;	/* Its a breaker */
						inputtags[ival].phasecfg = pntok[2].replace('_','-');
						try { inputtags[ival].feedsetid = Long.valueOf(pntok[3]); } catch (NumberFormatException nfx) {}
					}
				}
				else if(pntok[0].equals("outlet") && (pntok.length == 5)) {	/* Matching outlet */
					int oval = ival-inputtags.length; /* outlet index offset by number of inputs */
					if((oval >= 0) && (oval < outtags.length)) { /* Matches an outlet */					
						outtags[oval].cfg = pntok[2].replace('_', '-');
						try { outtags[oval].feedsetid = Long.valueOf(pntok[3]); } catch (NumberFormatException nfx) {}
						outtags[oval].label = p.getProperty("outlet-label." + 
							pntok[1] + "." + pntok[2] + "." + pntok[3] + "." + pntok[4],
							pntok[2] + "-" + pntok[3] + "-" + pntok[4]);
					}
				}
			}
		}
		/**
		 * Initialize device if needed, and we have full set of configuration data
		 */
		private boolean	initDeviceConfiguration(MantisIITag tag, boolean change) {
			int i;
			int j;
			TagSubGroup tg;
			if(initdone)
				return change;
			if((model == null) || (serial == null) || (outtags == null) || (inputtags == null)) {
				return change;		/* Not ready to init yet */
			}
			/* Now, apply mapping data */
			applyMapping();

			/* Set tower count to 1 */
            change = updateLong(tag, PDUTOWERCOUNT_ATTRIB_INDEX, 1) || change;
			/* Tower discoonect not supported, so set these appropriately */
			change = updateBoolean(tag, PDUTOWERDISCONNECT_ATTRIB_INDEX, false);
			change = updateLongListVerify(tag, PDUDISCONNECTEDTOWERS_ATTRIB_INDEX, 
				new ArrayList<Long>(), false);
			/* Set mapping match */
			change = updateBoolean(tag, PDUMODELMATCH_ATTRIB_INDEX, mapping_matched);

			Long towerid = Long.valueOf(1);
			/* Initialize outlets */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduOutletTagSubType.TAGTYPEID);
			for(i = 0; i < outtags.length; i++) {
				outtags[i].tag = SubTag.findOrCreateTag(tg, tag, String.format("outlet%02d",i+1), false,
					new int[] { PduOutletTagSubType.PDUCONFIG_INDEX, 
						PduOutletTagSubType.PDUFEEDSETID_INDEX,
						PduOutletTagSubType.PDULABEL_INDEX, PduOutletTagSubType.PDUTOWERID_INDEX },
					new Object[] { outtags[i].cfg, outtags[i].feedsetid, outtags[i].label, towerid });
				outtags[i].tag.lockUnlockTag(true);
//				PduOutletTagSubType.updateTag(outtags[i].tag, null, null, null,
//					null, null, outtags[i].cfg, null, null, null, null, outtags[i].label, outtags[i].feedsetid);
			}
			/* Initialize phases */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduPhaseTagSubType.TAGTYPEID);
			for(i = 0, j = 0; i < inputtags.length; i++) {
				if((inputtags[i].type & INPUT_TYPE_PHASE) != 0) {
					inputtags[i].phase_tag = SubTag.findOrCreateTag(tg, tag, String.format("phase%d",j+1), false,
						new int[] { PduPhaseTagSubType.PDUCONFIG_INDEX, 
							PduPhaseTagSubType.PDUFEEDSETID_INDEX, PduPhaseTagSubType.PDUTOWERID_INDEX },
						new Object[] { inputtags[i].phasecfg, inputtags[i].feedsetid, towerid } );
					inputtags[i].phase_tag.lockUnlockTag(true);
//					PduPhaseTagSubType.updateTag(inputtags[i].phase_tag, null, null, null, null, null, 
//						null, null, null, null, inputtags[i].phasecfg, null, inputtags[i].feedsetid);
					j++;
				}
			}
			/* Initialize breakers */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduBreakerTagSubType.TAGTYPEID);
			for(i = 0, j = 0; i < inputtags.length; i++) {
				if((inputtags[i].type & INPUT_TYPE_BREAKER) != 0) {
					inputtags[i].breaker_tag = SubTag.findOrCreateTag(tg, tag, String.format("breaker%02d",j+1), false,
						new int[] { PduBreakerTagSubType.PDUCONFIG_INDEX,
							PduBreakerTagSubType.PDUFEEDSETID_INDEX, PduBreakerTagSubType.PDUTOWERID_INDEX },
						new Object[] { inputtags[i].phasecfg, inputtags[i].feedsetid, towerid });
					inputtags[i].breaker_tag.lockUnlockTag(true);
					PduBreakerTagSubType.updateTag(inputtags[i].breaker_tag, null, null, null, inputtags[i].phasecfg,
						inputtags[i].feedsetid);
					j++;
				}
			}
			/* Initialize feedlines */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduFeedLineTagSubType.TAGTYPEID);
			for(i = 0, j = 0; i < inputtags.length; i++) {
				if((inputtags[i].type & INPUT_TYPE_FEEDLINE) != 0) {
					inputtags[i].feedline_tag = SubTag.findOrCreateTag(tg, tag, String.format("feedline%02d",j+1), false,
						new int[] { PduFeedLineTagSubType.PDUCONFIG_INDEX,
							PduFeedLineTagSubType.PDUFEEDSETID_INDEX, PduFeedLineTagSubType.PDUTOWERID_INDEX },
						new Object[] { inputtags[i].feedcfg, inputtags[i].feedsetid, towerid } );
					inputtags[i].feedline_tag.lockUnlockTag(true);
//					PduFeedLineTagSubType.updateTag(inputtags[i].feedline_tag, null, inputtags[i].feedcfg, null, null, null,
//						inputtags[i].feedsetid);
					j++;
				}
			}
			/* Initialize generic channels */
			tg = (TagSubGroup)tag.getTagGroup().getSubGroup(PduChannelTagSubType.TAGTYPEID);
			for(i = 0; i < inputtags.length; i++) {
				if(inputtags[i].type == INPUT_TYPE_CHANNEL) {
					/* Use raw channel ID for index, versus channel sequence, to preserve ID if mapping added/updated and
					 * channel is still present */
					inputtags[i].channel_tag = SubTag.findOrCreateTag(tg, tag, String.format("channel%02d",i+1), false,
						new int[] { PduChannelTagSubType.PDUTOWERID_INDEX },
						new Object[] { towerid } );
					inputtags[i].channel_tag.lockUnlockTag(true);
				}
			}
			initdone = true;

			return change;
		}
		/* Handle per input message */
		private boolean	handlePerInputMessage(MantisIITag tag, boolean change, 
			int igrp, int index, int amps_x10[]) {
			int i;

//			System.out.print(tag.getTagGUID() + ": Per-Input Amps: igrp=" + igrp + ", index=" + index);
//			for(i = 0; i < 3; i++) {
//				System.out.print(", amps_x10[" + i + "]=" + 
//					((amps_x10[i] == INPUT_AMPS_MAX)?"---":0.1*amps_x10[i]));
//			}
//			System.out.println();

			if(inputtags == null)
				return change;
			/* See if we're due to flush hour */
			if((last_input_index != 5) && (index == 5)) {	/* Firmware is doing hours on 4->5, not 5->0 */
				change = flushHourData(tag, change);
			}
			else if((last_input_index > index) && (last_input_index != 5)) {			/* We missed end of hour */
				change = flushHourData(tag, change);
			}
			else if(last_input_index != index) {		/* If new one */
				change = flush10MinuteData(tag, change);
			}
			else if(last_input_id >= igrp) {
				change = flush10MinuteData(tag, change);
			}
			last_input_index = index;
			last_input_id = igrp;

			if(!initdone)
				return change;

			/* Update values */
			for(i = 0; i < 3; i++) {
				int idx = 3*igrp + i;
				if(idx >= inputtags.length)
					continue;
				inputtags[idx].last_amps_x10 = amps_x10[i];
			}
			new10min = newhourly = true;

			return change;
		}
		/* Handle PDU model/serial messages */
		private boolean	handlePDUSerialMessage(MantisIITag tag, boolean change) {
			int idx = getBits(39,38);
			int i;
//			System.out.print(tag.getTagGUID() + ": PDU serial " + (idx+1) + ":");
			if(serial_sb.length() < ((idx+1) * 6))
				serial_sb.setLength((idx+1) * 6);
			for(i = 0; i < 6; i++) {
				serial_sb.setCharAt((6*idx+i), (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
			}
			if(getBit(0) != 0) {	/* If end */
				if(serial_sb.indexOf("\0") < 0) {	/* No nulls left? */
					String newserial = serial_sb.toString().trim();
					/* If different from old one, reset configuration */
					if((serial != null) && (!serial.equals(newserial))) {
						change = resetDeviceConfiguration(tag, change);
						serial = newserial;
					}
					else if(serial == null)
						serial = newserial;
//					System.out.print("serial=" + serial);
		            /* Set serial */
		            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX,
		                serial) || change;
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					sn_lst.add(serial);
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) 
						|| change;
					serial_sb.setLength(0);
				}
				/* Initialize device configuration, if needed and ready */
				change = initDeviceConfiguration(tag, change);
			}
//			System.out.println();

			return change;
		}
		/* Handle PDU model message */
		private boolean handlePDUModelMessage(MantisIITag tag, boolean change) {
			int i;
			int idx = getBits(39,38);

//			System.out.print(tag.getTagGUID() + ": PDU model " + (idx+1) + ":");

			if(model_sb.length() < (4 + (idx * 6)))
				model_sb.setLength(4 + (idx * 6));
			if(idx == 0) {
				for(i = 0; i < 4; i++) {
					model_sb.setCharAt(i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
				int ninp = getBits(6,3) + 1;	/* Get number of inputs */
//				System.out.print(" ninp=" + ninp);
				/* If mismatch on number of inputs */
				if((inputtags != null) && (inputtags.length != ninp)) {
					change = resetDeviceConfiguration(tag, change);
				}
				if(inputtags == null) {
					inputtags = new OurInputState[ninp];
					for(i = 0; i < inputtags.length; i++) {
						inputtags[i] = new OurInputState();
					}
				}
				int nout = getBits(12,7);	/* Get number of outlets */
//				System.out.print(", nout=" + nout);
				/* If mismatch on number of outlets */
				if((outtags != null) && (outtags.length != nout)) {
					change = resetDeviceConfiguration(tag, change);
				}
				if(outtags == null) {
					outtags = new OurOutletState[nout];
					for(i = 0; i < outtags.length; i++) {
						outtags[i] = new OurOutletState();
					}
				}
				/* Test if no serial */
				if(getBit(2) == 0) {	/* No serial? */
					String newserial = "";
					/* If different from old one, reset configuration */
					if((serial != null) && (!serial.equals(newserial))) {
						change = resetDeviceConfiguration(tag, change);
						serial = newserial;
					}
					else if(serial == null)
						serial = newserial;
//					System.out.print("No serial");
		            /* Set serial */
		            change = updateString(tag, PDUSERIAL_ATTRIB_INDEX,
		                newserial) || change;
					/* Update serial number list */
					List<String> sn_lst = new ArrayList<String>();
					sn_lst.add(serial);
		            change = updateStringList(tag, PDUSERIALLIST_ATTRIB_INDEX, sn_lst) 
						|| change;
				}
			}
			else {
				for(i = 0; i < 6; i++) {
					model_sb.setCharAt(4 + ((idx-1)*6) + i, (char)(0x20 + getBits(36-(i*6), 31-(i*6))));
				}
			}
//			System.out.println();
			if(getBit(0) != 0) {	/* If end */
				if(model_sb.indexOf("\0") < 0) {	/* No nulls left? */
					String nmodel = model_sb.toString().trim();
					/* If mismatch, reset configuration */
					if((model != null) && (!model.equals(nmodel))) {
						change = resetDeviceConfiguration(tag, change);
						model = nmodel;
					}
					else if(model == null)
						model = nmodel;
		            /* Set model */
		            change = updateString(tag, PDUMODEL_ATTRIB_INDEX,
		                model) || change;
//					System.out.println(" model=" + model);
					/* Update model number list */
					List<String> mn_lst = new ArrayList<String>();
					mn_lst.add(model);
		            change = updateStringList(tag, PDUMODELLIST_ATTRIB_INDEX, mn_lst) 
						|| change;
					model_sb.setLength(0);
				}
				/* Initialize device configuration, if needed and ready */
				change = initDeviceConfiguration(tag, change);
			}
			return change;
		}
		/* Handle per-outlet message */
		private boolean handlePerOutletMessage(MantisIITag tag, boolean change, 
			int ogrp, int amps_x10[]) {
			int i;

//			System.out.print(tag.getTagGUID() + ": Per-Outlet Amps: ogrp=" + ogrp);
//			for(i = 0; i < 3; i++) {
//				System.out.print(", amps_x10[" + i + "]=" + 
//					((amps_x10[i] == OUTLET_AMPS_MAX)?"---":0.1*amps_x10[i]));
//			}
//			System.out.println();

			/* If this is below last one, flush hour */
			if(ogrp <= last_outlet_index) {
				change = flushHourData(tag, change);
			}				
			last_outlet_index = ogrp;

			if(!initdone)
				return change;

			/* Update values */
			for(i = 0; i < 3; i++) {
				int idx = 3*ogrp + i;
				if(idx >= outtags.length)
					continue;
				outtags[idx].last_amps_x10 = amps_x10[i];
			}
			newhourly = true;

			return change;
		}
		/* Process valid message buffer */
		private boolean processMessage(MantisIITag tag) {
			int i;
			boolean change = false;
			String msg = null;

//			System.out.print(tag.getTagGUID() + ": message: ");
//			for(i = 0; i < 6; i++) {
//				System.out.print(String.format("%02x", accum[i]));
//			}
//			System.out.println();
			/* If we've hit first hour flush, count messages */
			if(msg_cnt >= 0)
				msg_cnt++;
			if(getBits(44, 42) == 0x02) {	/* If 44-42 = 010, Per input */
				int amps_x10[] = new int[3];
				for(i = 0; i < 3; i++) {
					amps_x10[i] = getBits(32-(11*i), 22-(11*i));
				}
				change = handlePerInputMessage(tag, change, getBits(41,39), 
					getBits(38,36), amps_x10);
				msg = "per-input:" + getBits(41,39) + ":" + getBits(38,36);
			}
			else if(getBits(44,40) == 0x00) {	/* 44-39 = 000000, PDU serial */
				change = handlePDUSerialMessage(tag, change);
				msg = "serial:" + getBits(39,38);
			}
			else if(getBits(44,40) == 0x01) {
				change = handlePDUModelMessage(tag, change);
				msg = "model:" + getBits(39,38);
			}
			/* If 44-42 = 0-1-1, per outlet */
			else if(getBits(44, 42) == 0x03) {
				int amps_x10[] = new int[3];
				for(i = 0; i < 3; i++) {
					amps_x10[i] = getBits(32-(11*i), 22-(11*i));
				}
				change = handlePerOutletMessage(tag, change, getBits(41,37), amps_x10);
				msg = "per-outlet:" + getBits(41,37);
			}
			else {
//				System.out.println("Unknown message");
			}			
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), accum, -1, false, msg);

			return change;
		}
		/* Handle alarm message */
		public boolean handleAlarm(MantisIITag tag, boolean trip, int almidx, boolean change, boolean verify) {
			/* Log message, if needed */
			RangerServer.logPDUTagMessage(tag.getTagGUID(), null, almidx, trip);
			return change;
		}
	}
    /**
     * 
     * Constructor
     */
    public MantisIITagType04N() {
        super(4, 'N', 0);
        setLabel("RF Code Mantis II Tag - Treatment 04N");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
	 * @param rdr - reader reporting payload 
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState(tag);
			tag.setTagState(ts);
			change = true;
		}
        /* If payload is flags payload */
        if((cur_payload >= MIN_FLAGS_PAYLOAD) && (cur_payload <= MAX_FLAGS_PAYLOAD)) {
            /* Set low battery flag - verify on set*/
			boolean v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Remember that we saw low battery during hour - 7 vs 6 beacons per message */
			if((cur_payload & 0x02) != 0)
				ts.did_low_batt = true;
            /* Set disconnect flag - verify on set */
			v = ((cur_payload & 0x01) != 0);
			boolean dchg = updateBooleanVerify(tag, PDUDISCONNECT_ATTRIB_INDEX, v, verify && v);
            change |= dchg;
			/* If disconnected, reset the device configuration */
			if(v && dchg)
				change = ts.resetDeviceConfiguration(tag, change);
        }
        /* If start of message */
        else if((cur_payload >= MIN_START_PAYLOAD) && (cur_payload <= MAX_START_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0x7F), cur_timestamp, true, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If continuation of payload */
		else if((cur_payload >= MIN_CONT_PAYLOAD) && (cur_payload <= MAX_CONT_PAYLOAD)) {
			change = ts.accumulateByte(tag, (byte)(cur_payload & 0xFF), cur_timestamp, false, rdr) || change;
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}
		/* If alarm payload */
		else if((cur_payload >= MIN_ALARM_PAYLOAD) && (cur_payload <= MAX_ALARM_PAYLOAD)) {
			change = ts.handleAlarm(tag, (cur_payload & 0x10) != 0, cur_payload & 0x0F, change, verify);
            /* Clear disconnect flag - we don't get these payloads when its disconnected */
            change = updateBoolean(tag, PDUDISCONNECT_ATTRIB_INDEX, false) || change;
		}

        return change;
    }
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	/**
	 * Get list of subtypes used by this type, if any.  Returns null if none
	 */
	public String[] getSubTypes() {
		return SUBTYPES;
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
