package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.Collections;

import com.rfcode.drivers.readers.CustomFieldValue;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.ranger.RangerServer;

/**
 * Class representing a specific Mantis-class asset tag, as viewed by one or
 * more channels of one or more readers.
 * 
 * @author Mike Primm
 * 
 */
public class MantisIITag implements Tag {
    private MantisIITagGroup our_group; /* Our group (and through it, tag type) */
    private Object[] attribs; 		/* Attribute values, indexed to match array of */
    private boolean[] updattribs;   /* Flags indicating which attributes are not default */
	private boolean[] pendingupd;	/* Flags for pending update request - used for enhanced verify */
    /* attribute IDs returned from our TagType */
    private String tag_guid;		/* Our GUID : <6-character-groupcode><8-digit-id> */
    private int[] prev_payloads;	/* Older payloads, if needed */
    private long[] prev_payload_times; /* Times for older payloads, if needed */
    private int lock_cnt;			/* Tag lock count */
    private boolean valid_payloads;
    /* Offsets within the GUID string */
    private static final int OFFSET_TAGID = 6;
    private Map<String, TagLink> links; /* Our TagLinks */
	private MantisIITagState	tagstate;
    /* Multireader verify fields: track ts, value, and count of matching payloads */
    private long last_payload_ts;
    private int last_payload;
    private int last_payload_matchcount;
    private MantisIIReader last_payload_rdr;
    private boolean multipayload_verify_active;

    private long loc_match_ts;  /* Timestamp for last location match change */

    public static final long SAME_BEACON_LIMIT = 1000;   /* 1 second */

	/* Interface for tag state object - used for tag-type specific state tracking */
	public interface MantisIITagState {		
		/**
		 * Tag delete notification - called when tag object is being cleaned up
		 */
		public void cleanupTag(MantisIITag t);
	}
    /**
     * Constructor for tag
     * 
     * @param grp -
     *            our group
     * @param guid-
     *            our tag guid
     */
    MantisIITag(MantisIITagGroup grp, String guid) {
        our_group = grp; 			/* Remember group */
        tag_guid = guid; 			/* Save guid */
        attribs = grp.getTagType().getTagAttributeDefaultValues(); /*
                                                                     * Get
                                                                     * default
                                                                     * attributes
                                                                     */
        updattribs = new boolean[attribs.length];
		/* Make update-blocked attributes non-defaulted (since we will never set them) */
		for(int i = 0;i < updattribs.length; i++)
			updattribs[i] = isBlockedUpdateTagAttrib(i);
		pendingupd = new boolean[attribs.length];

        links = new HashMap<String, TagLink>();
        int pcnt = grp.getMantisIITagType().getRequiredPayloadCount();
        if (pcnt > 0) { 			/* Need old payloads? */
            prev_payloads = new int[pcnt];
            prev_payload_times = new long[pcnt];
        }
    }
    /**
     * Finish initialization of tag - must be called once attributes set
     */
    public void init() {
        our_group.addMantisIITag(this);
    }
    /**
     * Cleanup tag - unhook and make ready for discard
     */
    public void cleanup() {
		/* If any custom tag state, nofify that it is being cleaned up */
		if(tagstate != null) {
			tagstate.cleanupTag(this);
			tagstate = null;
		}
        if (links.isEmpty() != false) {
            for (TagLink link : links.values())
                link.cleanup();
            links.clear();
        }
        if (our_group != null) {
            our_group.reportTagLifecycleDelete(this);
            our_group.removeMantisIITag(this);
            our_group = null;
        }
        MantisIIGPS.cleanupTag(tag_guid);

        attribs = null;
        updattribs = null;
		pendingupd = null;
    }
    /**
     * Is tag valid? Used to test if tag has been cleaned-up
     * 
     * @return true if tag is still valid
     */
    public boolean isValid() {
        return (attribs != null);
    }
    private static final char pad[] = { '0', '0', '0', '0', '0', '0', '0', '0' };
    /**
     * Build GUID for tag with given group and tagid
     * 
     * @param grp -
     *            our group
     * @param tagid -
     *            our tagid (if -1, its pseudotag)
     * @param rdrid -
     *            readerid (if pseudotag, used for tagid)
     * @return GUID
     */
    public static String buildTagGUID(MantisIITagGroup grp, int tagid, String rdrid) {
        StringBuilder sb = new StringBuilder(grp.getGroupCode());
        if(sb.length() > 6) sb.setLength(6);
        if(tagid >= 0) {
            String tid = Integer.toString(tagid);
            int tidlen = tid.length();
            if(tidlen < 8) sb.append(pad, 0, 8-tidlen);
            sb.append(tid);
        }
        else {
            sb.append("_").append(rdrid);   
        }
        return sb.toString();
    }
    /**
     * Find tag, if exists, or create new tag
     * 
     * @param grp -
     *            our group
     * @param tagid -
     *            our tagid (-1 if pseudotag)
     * @param no_create -
     *            if true, do not do create if missing
     * @param readerid - ID of reader - used for pseudotags
     * @return tag instance
     */
    static MantisIITag findOrCreateTag(MantisIITagGroup grp, int tagid,
        boolean no_create, String readerid) {
        String guid = buildTagGUID(grp, tagid, readerid);
        MantisIITag tag = (MantisIITag) grp.findTag(guid); /* Try to find it */
        if ((tag == null) && (!no_create)) { /* Not found, make new one */
        	tag = grp.getMantisIITagType().makeTag(grp, guid);
            tag.init();
        }
        return tag;
    }
    /**
     * Validate our attribute set - make sure we fault in new values
     */
    private void validateAttribs() {
        int cnt = getTagType().getTagAttributeCount();
        if (attribs.length >= cnt) {
            return;
        }
        /* Get defaults and overlay our values */
        Object[] newattr = getTagType().getTagAttributeDefaultValues();
        System.arraycopy(attribs, 0, newattr, 0, attribs.length);
        attribs = newattr; /* And switch to new stuff */
        boolean[] newupd = new boolean[newattr.length];
        System.arraycopy(updattribs, 0, newupd, 0, updattribs.length);
        updattribs = newupd;
		newupd = new boolean[newattr.length];
        System.arraycopy(pendingupd, 0, newupd, 0, updattribs.length);
        pendingupd = newupd;
    }
    /**
     * Return values of tag attributes in provided array. The attributes are
     * indexed in the same order as the attribute IDs returned by the
     * getTagAttributes() call of the TagType defining the tag. This is intended
     * to be a high-performance interface.
     * 
     * @param val -
     *            array of length matching the number of attributes to return
     * @param start -
     *            index (relative to getTagAttributes()) of first attribute to
     *            return (goes in val[0])
     * @param count -
     *            number of consecutive attributes to return
     * @return number of attribute values returned
     */
    public int readTagAttributes(Object[] val, int start, int count) {
        int i, j;
        boolean did_validate = false;
        for (i = start, j = 0; j < count; i++, j++) {
            /*
             * If in range, return value (just let negatives throw
             * ArrayIndexOutOfBounds - its what we'd do
             */
            if (i < attribs.length) {
                val[j] = attribs[i];
            } else { /* Past end */
                if (!did_validate) {
                    validateAttribs(); /* Update if needed once */
                    did_validate = true;
                }
                if (i < attribs.length) /* If in bounds now, get it */
                    val[j] = attribs[i];
                else
                    /* Else, undefined */
                    val[j] = null;
            }
        }
        return count; /* Return number of values copied */
    }
    /**
     * Read tag attribute at given index
     * 
     * @param index -
     *            index of attribute to be read
     * @return attribute value, or null if not defined
     */
    public Object readTagAttribute(int index) {
        if (index < attribs.length)
            return attribs[index];
        validateAttribs();
        if (index < attribs.length)
            return attribs[index];
        return null;
    }

    /**
     * Get a copy of the current tag attributes values
     * 
     * @return map containing copy of attribute values, keyed by attribute ID
     */
    public Map<String, Object> getTagAttributes() {
        validateAttribs(); /* Update if needed */

        HashMap<String, Object> map = new HashMap<String, Object>();
        String[] ids = getTagType().getTagAttributes();

        for (int i = 0; i < attribs.length; i++) { /* Add values to map */
        	if(attribs[i] instanceof CustomFieldValue) {
        		CustomFieldValue cfv = (CustomFieldValue) attribs[i];
        		map.put(cfv.getField(), cfv.getValue());
        	}
        	else
        		map.put(ids[i], attribs[i]);
        }
        return map;
    }
    /**
     * Replace set of tag attributes.
     * 
     * @param idx -
     *            array of indexes of values to be written
     * @param val -
     *            array of new values to be written (note: not copied)
     * @param count -
     *            number of value to write (arrays may be longer than needed)
     * @return true if any of the attributes were changed
     */
    public boolean updateTagAttributes(int[] idx, Object[] val, int count) {
        if((attribs == null) || (idx == null) || (val == null))
            return false;
        validateAttribs();
        Object oldval[] = null;
        for (int i = 0; i < count; i++) {
            int ix = idx[i];
			if(isBlockedUpdateTagAttrib(ix))
				continue;
            /* If was null or is different */
            boolean diff = (attribs[ix] == null)?(val[i] != null):
                (!attribs[ix].equals(val[i]));
            if (diff || (!updattribs[ix])) {
                /* Back up old values, if not already done */
                if (oldval == null)
                    oldval = attribs.clone();
                attribs[ix] = val[i];
                updattribs[ix] = true;
				pendingupd[ix] = false;
            }
        }
        /* If changes made, report them */
        if (oldval != null) {
            our_group.reportTagStatusAttributeChange(this, oldval);
        }
        return (oldval != null);
    }
    /**
     * Get the tag's globally unique ID. This MUST be consistent for all
     * observers of the tag.
     * 
     * @return GUID string
     */
    public String getTagGUID() {
        return tag_guid;
    }
    /**
     * Get the tag's ID, relative to its TagGroup. This may or may not be the
     * same as the unique ID, but it will be unique relative to the TagGroup.
     * 
     * @return Tag ID
     */
    public String getTagID() {
        return tag_guid.substring(OFFSET_TAGID);
    }
    /**
     * Get the TagGroup for the tag.
     * 
     * @return Tag's TagGroup
     */
    public TagGroup getTagGroup() {
        return our_group;
    }
    /**
     * Get TagLink for given ReaderChannel
     * 
     * @param channel -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(ReaderChannel channel) {
        return links.get(channel.getID());
    }
    /**
     * Get TagLink for given ReaderChannel ID
     * 
     * @param channelid -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(String channelid) {
        return links.get(channelid);
    }
    /**
     * Set TagLink for given ReaderChannel - replaces existing if one is
     * present.
     * 
     * @param channel -
     *            Channel to be accessed
     * @param taglink -
     *            TagLink to be inserted: null to remove existing link
     */
    public void insertTagLink(ReaderChannel channel, TagLink taglink) {
        if(our_group == null)
            return;
        if (taglink == null) {
            taglink = links.remove(channel.getID());
            if (taglink != null) { /* Was something removed? */
                our_group.reportTagStatusLinkRemoved(this, taglink);
                taglink.cleanup(); /* Clean it up */
                /* If no more links and not locked, retire the tag */
                if (links.isEmpty()) {
                    if(lock_cnt <= 0) {
                        cleanup();
                    }
                    else {  /* Else, reset attributes */
                        our_group.getMantisIITagType().setTagOffline(this);
                    }
                }
            }
        } else {
            links.put(channel.getID(), taglink);
            our_group.reportTagStatusLinkAdded(this, taglink);
        }
    }
    /**
     * Get current set of TagLinks for Tag (read-only)
     * 
     * @return Map of TagLinks, keyed by ReaderChannel ID
     */
    public Map<String, TagLink> getTagLinks() {
        return Collections.unmodifiableMap(links);
    }
    /**
     * Update attributes using given payload
     * 
     * @param payload -
     *            new payload
     * @param payload_ts -
     *            new payload timestamp
	 * @param rdr -
     *            reporting reader
     * @return true if changed any attribute values, false if all unchanged
     */
    public boolean updateUsingPayload(int payload, long payload_ts, MantisIIReader rdr) {
        boolean change = false;
        int multicnt = our_group.getMultiReaderVerifyCount();
        if((rdr != null) && (multicnt > 0)) {
            /* If same reader OR too long since last beacon */
            if((last_payload_rdr == rdr) ||
                (payload_ts > (last_payload_ts + SAME_BEACON_LIMIT))) {   
                /* Did we have enough for multipayload to be active? */
                boolean was_active = multipayload_verify_active;
                if(was_active)
                    multipayload_verify_active = (last_payload_matchcount >= 2);
                else
                    multipayload_verify_active = (last_payload_matchcount >= multicnt);
                /* Save new payload values - set count to 1 */
                last_payload_ts = payload_ts;
                last_payload = payload;
                last_payload_matchcount = 1;
                last_payload_rdr = rdr;
                /* First report of new beacon with verify active?  Skip it */
                if(multipayload_verify_active)
                    return change;
            }
            /* Else, another copy of same beacon - check if it disagrees */
            else if (payload != last_payload) { /* If payload disagreement, something is bad */
                if(multipayload_verify_active) {  /* If verify is active, ignore the new one */
                    RangerServer.info(String.format("%s: multireader payload mismatch - payloads=%03o/%03o, rdrs=%s/%s, ts=%d/%d",
                        getTagGUID(), payload, last_payload, rdr.getID(), last_payload_rdr.getID(), payload_ts, last_payload_ts));
                    return change;
                }
            }
            else {   /* Else, another matching payload from another reader */
                last_payload_matchcount++;
                /* If we're doing verify, and this is the confirming payload, process the first one before new one */
                if(multipayload_verify_active && (last_payload_matchcount == 2)) {
                    change = processPayload(last_payload, last_payload_ts, last_payload_rdr) || change;
                }
            }
        }
        change = processPayload(payload, payload_ts, rdr) || change;

        return change;
    }
    private boolean processPayload(int payload, long payload_ts, MantisIIReader rdr) {
        boolean change = our_group.getMantisIITagType().parsePayload(payload,
            payload_ts, prev_payloads, prev_payload_times, this, rdr);
        /* If old payloads tracked and if changed, push new value and timestamp */
		if((prev_payloads != null) && (payload != prev_payloads[0])) {
            /* Shift rows down */
            for (int i = (prev_payloads.length - 2); i >= 0; i--) {
                prev_payloads[i + 1] = prev_payloads[i];
                prev_payload_times[i + 1] = prev_payload_times[i];
            }
            prev_payloads[0] = payload;
            prev_payload_times[0] = payload_ts;
        }
        valid_payloads = true;
        return change;
    }
    /**
     * Update attributes using given payload
     * 
     * @param payload -
     *            new payload
     * @param payload_ts -
     *            new payload timestamp
     * @return true if changed any attribute values, false if all unchanged
     */
    public boolean updateUsingPayload(int payload, long payload_ts) {
		return updateUsingPayload(payload, payload_ts, null);
	}
    /**
     * Get current number of TagLinks for this tag
     * 
     * @return number of active TagLinks
     */
    public int getTagLinkCount() {
        return links.size();
    }
    /**
     * Get the TagType for the tag.
     * 
     * @return Tag's TagType
     */
    public TagType getTagType() {
        if(our_group != null)
            return our_group.getTagType();
        else
            return null;
    }
    /**
     * Get the MantisIITagType for the tag.
     * 
     * @return Tag's MantisIITagType
     */
    public MantisIITagType getMantisIITagType() {
        if(our_group != null)
            return our_group.getMantisIITagType();
        else
            return null;
    }
    /**
     * Lock/unlock tag - used to keep tag from being cleaned up when no
     * links see it.  Each call with lock=true increments lock count.
     * Each call with lock=false decrements it.
     * @param lock - true to lock, false to unlock
     * @return new lock count
     */
    public int lockUnlockTag(boolean lock) {
        if(lock) {
            lock_cnt++;
        }
        else {
            lock_cnt--;
            if(lock_cnt <= 0) {
                if (links.isEmpty()) {
                    cleanup();
                }
            }
        }
        return lock_cnt;
    }
    /**
     * Equals handler
     */
    public boolean equals(Object o) {
        if((o != null) && (o instanceof Tag)) {
            return tag_guid.equals(((Tag)o).getTagGUID());
        }
        return false;
    }
    /**
     * Get set of defaulted tag attributes
     *
     * @return Set<String> of attribute IDs, or null if none
     */
    public Set<String> getDefaultedTagAttributes() {
        Set<String> rslt = null;
        String[] ids = null;
        for(int i = 0; i < updattribs.length; i++) {
            if(updattribs[i] == false) { /* If defaulted? */
				if(isBlockedUpdateTagAttrib(i)) {
					updattribs[i] = true;
				}
				else {
	                if(ids == null)
    	                ids = getTagType().getTagAttributes();
    	            if(rslt == null)
    	                rslt = new TreeSet<String>();
    	            rslt.add(ids[i]);
				}
            }
        }
        return rslt;
    }
    /**
     * Access raw attribute array (internal use only)
     */
    Object[] getRawAttribs() {
        return attribs;
    }
    /**
     * Test if attribute is still default
     */
    public boolean isDefaultFlagged(int idx) {
        return !updattribs[idx];
    }
    /**
     * Mark attribute as updated from default
     */
    void clearDefaultFlag(int idx) {
        updattribs[idx] = true;
    }
    /**
     * Compute longest age-out time among readers with active links
     */
    int getLongestAgeOut() {
        int t = 0;
        for(Map.Entry<String,TagLink> me : links.entrySet()) {
            MantisIIReader rdr = (MantisIIReader)(me.getValue().getChannel().getReader());
            int to = rdr.getTagTimeout();
            if(to > t) t = to;            
        }
        return t;
    }
    /**
     * Test if any valid payloads
     */
    boolean hadValidPayloads() {
        return valid_payloads;
    }
	/**
	 * Set custom tag state
	 */
	void setTagState(MantisIITagState ts) {
		if(tagstate != ts) {
			if(tagstate != null)	/* If old one, delete it */
				tagstate.cleanupTag(this);
			tagstate = ts;
		}
	}
	/**
	 * Get custom tag state
	 */
	MantisIITagState getTagState() {
		return tagstate;
	}
	/**
	 * Get set of subtags (null if none)
	 */
	public Set<Tag>	getSubTags() {
		return SubTag.getSubTagsByTag(tag_guid);
	}
	/**
	 * Get parent tag, if subtag
	 */
	public Tag getParentTag() {
		return null;
	}
	/**
	 * Test if given attribute index is update blocked
	 */
	public boolean isBlockedUpdateTagAttrib(int idx) {
		return our_group.isBlockedUpdateTagAttrib(idx);
	}
	/**
	 * Set pending change status for given attrib
	 */
	void setPendingUpdate(int idx) {
		pendingupd[idx] = true;
	}
	/**
	 * Set pending change status for given attrib
	 */
	void clearPendingUpdate(int idx) {
		pendingupd[idx] = false;
	}
	/**
	 * Test pending change status for given attrib
	 */
	boolean getPendingUpdate(int idx) {
		return pendingupd[idx];
	}
    /**
     * Set location match timestamp
     */
    public void setLocationMatchTimestamp(long ts) {
        loc_match_ts = ts;
    }
    /**
     * Get location match timestamp
     */
    public long getLocationMatchTimestamp() {
        return loc_match_ts;
    }
    /**
     * Test if tag is IR detecting tag
     * Note: may return false for tags that are IR, but have not yet reported a value
     * @param true if definitely IR detecting tag, false if may not be
     */
    public boolean isIRDetectingTag() {
        int idx = our_group.getMantisIITagType().getIRIndex();
        if(idx >= 0) {
            return (attribs[idx] != null);
        }
        return false;
    }
}
