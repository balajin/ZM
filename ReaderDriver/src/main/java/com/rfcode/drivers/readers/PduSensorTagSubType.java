package com.rfcode.drivers.readers;

import com.rfcode.drivers.readers.mantis2.MantisIITagType;
import com.rfcode.drivers.readers.mantis2.SubTag;
import com.rfcode.drivers.readers.mantis2.TagSubType;

/**
 * PDU sensor tag subtype
 * 
 * @author Mike Primm
 */
public class PduSensorTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduSensor";

	public static final int PDUTEMP_INDEX = 2;
	public static final int PDUHUMIDITY_INDEX = 3;
	public static final int PDUDEWPOINT_INDEX = 4;

	public static final Double VALUE_NA = Double.valueOf(-1.0);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, MantisIITagType.TEMPERATURE_ATTRIB,
		MantisIITagType.HUMIDITY_ATTRIB, MantisIITagType.DEWPOINT_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, MantisIITagType.TEMPERATURE_ATTRIB_LABEL,
		MantisIITagType.HUMIDITY_ATTRIB_LABEL, MantisIITagType.DEWPOINT_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null, null, null };
    /**
     * Constructor
     */
    public PduSensorTagSubType() {
        setLabel("PDU Sensor Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }

	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double temp, Double humid, Double dew) {
		if(tag == null)
			return;
		int[] idx = new int[3];
		Object[] val = new Object[3];
		int cnt = 0;
		if(temp != null) {
			idx[cnt] = PDUTEMP_INDEX;
			val[cnt] = temp;
			cnt++;
		}
		if(humid != null) {
			idx[cnt] = PDUHUMIDITY_INDEX;
			if(humid.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = humid;
			cnt++;
		}
		if(dew != null) {
			idx[cnt] = PDUDEWPOINT_INDEX;
			val[cnt] = dew;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}

}
