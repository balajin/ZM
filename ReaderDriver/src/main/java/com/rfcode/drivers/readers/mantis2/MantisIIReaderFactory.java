package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.ArrayList;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderLifecycleListener;
import com.rfcode.drivers.readers.SessionUsingReaderFactory;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.ListDouble;
import com.rfcode.drivers.BadParameterException;

/**
 * Mantis-family reader factory (abstract)
 * 
 * @author Mike Primm
 */
public abstract class MantisIIReaderFactory implements
    SessionUsingReaderFactory {
    /* Attribute ID for hostname/ip address */
    public static final String ATTRIB_HOSTNAME = "hostname";
    /* Attribute ID for port number */
    public static final String ATTRIB_PORTNUM = "port";
    /* Attribute ID for user-id */
    public static final String ATTRIB_USERID = "userid";
    /* Attribute ID for password */
    public static final String ATTRIB_PASSWORD = "password";
    /* Attribute ID for tag timeout (integer seconds) */
    public static final String ATTRIB_TAGTIMEOUT = "tagtimeout";
    /* Attribute ID for tag age-in count (integer beacons) */
    public static final String ATTRIB_AGEINCOUNT = "ageincount";
    /* Attribute ID for SSI change threshold (integer dBm) */
    public static final String ATTRIB_SSIDELTATHRESH = "ssideltathresh";
    /* Attribute ID for offline reader link ageout (integer seconds) */
    public static final String ATTRIB_OFFLINELINKAGEOUT = "offlinelinkageout";
    /* Attribute ID for channel age-out time (integer seconds) */
    public static final String ATTRIB_AGEOUTTIME[] = {"ageouttimeA",
        "ageouttimeB"};
    /* Attribute ID for channel SSI cutoff (integer dBm) */
    public static final String ATTRIB_SSICUTOFF[] = {"ssicutoffA", "ssicutoffB"};
    /* Attribute ID for channel bias (integer dbm) */
    public static final String ATTRIB_CHANNEL_BIAS[] = {"channelbiasA",
        "channelbiasB"};
    /* Attribute ID for serial port driver (string) */
    public static final String ATTRIB_SERPORT_DRVR = "serialdriver";
    /* Attribute ID for serial port baud rate (int) */
    public static final String ATTRIB_SERPORT_BAUD = "serialbaud";
    /* Attribute ID for noise threshold (int) */
    public static final String ATTRIB_NOISETHRESH = "noisethresh";
    /* Attribute ID for report triggers (bool) */
    public static final String ATTRIB_REPORTTRIGGERS = "reporttriggers";
    /* Attribute ID for merging channels (bool) */
    public static final String ATTRIB_MERGECHANNELS = "mergechannels";
    /* Attribute ID for upconnect enabled (bool) */
    public static final String ATTRIB_UPCONNENABLED = "upconnenabled";
    /* Attribute ID for upconnect reader ID (string) */
    public static final String ATTRIB_UPCONNRDRID = "upconnrdrid";
    /* Attribute ID for upconnect password (string) */
    public static final String ATTRIB_UPCONNPWD = "upconnpwd";
    /* Attribute ID for reader partition count (int) */
    public static final String ATTRIB_PARTCOUNT = "partcount";
    /* Attribute ID for reader partition index (int) */
    public static final String ATTRIB_PARTINDEX = "partindex";
    /* Attribute ID for reader partition rotatino period (int) */
    public static final String ATTRIB_PARTPERIOD = "partrotperiod";
    /* Attribute ID for event-per-sec threshold (int) */
    public static final String ATTRIB_EVTPERSECTHRESH = "evtpersecthresh";
    /* Attribute ID for joining channels (bool) */
    public static final String ATTRIB_JOINCHANNELS = "joinchannels";
    /* Attribute ID for reader name (String) */
    public static final String ATTRIB_READERNAME = "name";
    /* Attribute ID for payload fault in mask (int) */
    public static final String ATTRIB_PAYLOADFAULTINMASK = "pfimask";
    /* Attribute ID for payload fault in value (int) */
    public static final String ATTRIB_PAYLOADFAULTINVALUE = "pfivalue";
    /* Attribute ID for payload change ignore mask (int) */
    public static final String ATTRIB_PAYLOADCHGIGNOREMASK = "pchgignmask";
	/* Attribute ID for GPS min SSI (int) */
	public static final String ATTRIB_GPS_MIN_SSI = "gpsminssi";
    /** Default timeout for tags (tag lost reporting) */
    public static final int DEFAULT_TAGTIMEOUT = 60;
    /** Default age-in count for tags (min beacons before tag reported) */
    public static final int DEFAULT_AGEINCOUNT = 0;
    /** Default SSH change threshold */
    public static final int DEFAULT_SSIDELTATHRESH = 3;
    /** Default offline reader link ageout */
    public static final int DEFAULT_OFFLINELINKAGEOUT = 60;
    /** Default channel-specific age-out time */
    public static final int DEFAULT_CHANNEL_AGEOUTTIME = 30;
    /** Default channel-specific SSI cutoff (0=no cutoff) */
    public static final int DEFAULT_CHANNEL_SSICUTOFF = 0;
    /** Default channel bias */
    public static final int DEFAULT_CHANNEL_BIAS = 0;
    /** Default serial port driver (string) */
    public static final String DEFAULT_SERPORT_DRVR = "bridge";
    /** Default serial port baud rate (int) */
    public static final int DEFAULT_SERPORT_BAUD = 115200;
    /* Attribute ID for noise threshold (int) */
    public static final int DEFAULT_NOISETHRESH = -80;
    /* Attribute ID for report triggers (bool) */
    public static final boolean DEFAULT_REPORTTRIGGERS = false;
    /* Attribute ID for merge channels (bool) */
    public static final boolean DEFAULT_MERGECHANNELS = false;
    /* Attribute ID for upconnect enabled (bool) */
    public static final boolean DEFAULT_UPCONNENABLED = false;
    /* Attribute ID for upconnect reader ID (string) */
    public static final String DEFAULT_UPCONNRDRID = "";
    /* Attribute ID for upconnect password (string) */
    public static final String DEFAULT_UPCONNPWD = "";
    /* Attribute ID for reader partition count (int) */
    public static final int DEFAULT_PARTCOUNT = 1;
    /* Attribute ID for reader partition index (int) */
    public static final int DEFAULT_PARTINDEX = 0;
    /* Attribute ID for reader partition rotation period (int) */
    public static final int DEFAULT_PARTPERIOD = 0;
    /* Default for event-per-sec threshold (int) */
    public static final int DEFAULT_EVTPERSECTHRESH = 0;
    /* Default for join channels (bool) */
    public static final boolean DEFAULT_JOINCHANNELS = false;
    /* Default for reader name (string) */
    public static final String DEFAULT_READERNAME = "";
    /* Attribute ID for payload fault in mask (int) */
    public static final int DEFAULT_PAYLOADFAULTINMASK = 0;
    /* Attribute ID for payload fault in value (int) */
    public static final int DEFAULT_PAYLOADFAULTINVALUE = 0;
    /* Attribute ID for payload change ignore mask (int) */
    public static final int DEFAULT_PAYLOADCHGIGNOREMASK = 0;
	/* Attribute ID for GPS min SSI (int) */
	public static final int    GPS_MIN_SSI_NOMIN = 0;
	public static final int    DEFAULT_GPS_MIN_SSI = GPS_MIN_SSI_NOMIN;

	/* Attribute ID - position source */
	public static final String ATTRIB_POSITIONSRC = "positionsrc";
    public static final String ATTRIB_POSITIONSRC_NONE = "NONE";
    public static final String ATTRIB_POSITIONSRC_GPS = "GPS";
    public static final String ATTRIB_POSITIONSRC_STATIC = "STATIC";
	public static final String DEFAULT_POSITIONSRC = ATTRIB_POSITIONSRC_NONE;
    /* Attribute ID - static latitude/longitude */
    public static final String ATTRIB_STATICLATLON = "staticlatlon";
    public static final ListDouble DEFAULT_STATICLATLON = new ListDouble(new Double[] { 0.0, 0.0 });
    
    /* Table of supported tagtype IDs */
    private static final String[] SUPPORTED_TAGTYPEIDS = {
    	MantisIITagType04A.TAGTYPEID,
        MantisIITagType04B.TAGTYPEID, MantisIITagType04C.TAGTYPEID,
        MantisIITagType04D.TAGTYPEID, MantisIITagType04E.TAGTYPEID,
        MantisIITagType04F.TAGTYPEID, MantisIITagType04H.TAGTYPEID,  
        MantisIITagType04I.TAGTYPEID, MantisIITagType04J.TAGTYPEID,
		MantisIITagType04L.TAGTYPEID, MantisIITagType04M.TAGTYPEID,
		MantisIITagType04N.TAGTYPEID, MantisIITagType04O.TAGTYPEID,
		MantisIITagType04P.TAGTYPEID, MantisIITagType04Q.TAGTYPEID,
        MantisIITagType04R.TAGTYPEID, MantisIITagType04S.TAGTYPEID, 
        MantisIITagType04T.TAGTYPEID, MantisIITagType04U.TAGTYPEID,
        MantisIITagType04V.TAGTYPEID, MantisIITagType04W.TAGTYPEID,
        MantisIITagType04X.TAGTYPEID, MantisIITagType05A.TAGTYPEID,
        MantisIITagType05B.TAGTYPEID, MantisIITagType05C.TAGTYPEID,
        MantisIITagType06A.TAGTYPEID };

    private SessionFactory fact;
    private ArrayList<ReaderLifecycleListener> lifecycle_listeners;
    private int modenum;
    private String factoryid;
    private String label;
    private String desc;
    private int chan_cnt;
	private int max_grp_code;
    /**
     * Default constructor
     */
    public MantisIIReaderFactory() {
        fact = null;
        lifecycle_listeners = new ArrayList<ReaderLifecycleListener>();
        chan_cnt = 2; /* Default it to 2 channels */
		max_grp_code = 8;	/* Default to 8 - M250 will change this */
    }
    /**
     * Initialize the factory instance
     * 
     * @throws BadParameterException
     *             if bad attributes
     * @throws DuplicateEntityIDException
     *             if duplicates existing ID
     */
    public void init() throws BadParameterException, DuplicateEntityIDException {
        if (fact == null)
            throw new BadParameterException("Session Factory missing");
        ReaderEntityDirectory.addEntity(this);
    }
    /**
     * Cleanup the factory instance
     */
    public void cleanup() {
        if (lifecycle_listeners != null) {
            lifecycle_listeners.clear();
            lifecycle_listeners = null;
        }
    }
    /**
     * Get reader factory ID
     * 
     * @return factory ID
     */
    public String getID() {
        return factoryid;
    }
    /**
     * Set reader factory ID
     * 
     * @param factid -
     *            factory ID
     */
    public void setID(String factid) {
        factoryid = factid;
    }
    /**
     * Get ordered list of attribute IDs required by Reader instances created by
     * this factory
     * 
     * @return array of attribute IDs
     */
    public abstract String[] getReaderAttributeIDs();
    /**
     * Get label for reader factory - describes type of readers supported
     * 
     * @return label
     */
    public String getReaderFactoryLabel() {
        return label;
    }
    /**
     * Set label for reader factory
     * 
     * @param lab -
     *            label
     */
    protected void setReaderFactoryLabel(String lab) {
        label = lab;
    }
    /**
     * Get description for reader factory - more verbose than label.
     * 
     * @return description
     */
    public String getReaderFactoryDescription() {
        return desc;
    }
    /**
     * Set description for reader factory - more verbose than label.
     * 
     * @param descript -
     *            description
     */
    protected void setReaderFactoryDescription(String descript) {
        desc = descript;
    }
    /**
     * Get copy of default attribute set for a reader. This method should be used to
     * initialize the attribute set ultimately passed to the Reader instances,
     * in order to handle cases where code updates add new attributes.
     * 
     * @return default attribute set
     */
    public abstract Map<String, Object> getDefaultReaderAttributes();
    /**
     * Create uninitialized instance of Reader, as defined by this factory. The
     * resulting instance must be supplied with attributes, as indicated by the
     * getReaderAttributeIDs(), before the instance is usable.
     * 
     * @return Reader instance
     */
    public Reader createReader() {
        return new MantisIIReader(this);
    }
    /**
     * Get list of TagType IDs supported by Readers from this factory.
     * 
     * @return list of TagType IDs supported
     */
    public String[] getSupportedTagTypeIDs() {
        return SUPPORTED_TAGTYPEIDS;
    }
    /**
     * Inject the SessionFactory to be used for communications
     * 
     * @param sessfact -
     *            session communications factory
     */
    public void setSessionFactory(SessionFactory sessfact) {
        fact = sessfact;
    }
    /**
     * Get our session factory
     * 
     * @return session factory
     */
    public SessionFactory getSessionFactory() {
        return fact;
    }
    /**
     * Get operating mode number (M,x)
     */
    int getModeNumber() {
        return modenum;
    }
    /**
     * Set operating mode number
     * 
     * @param mode -
     *            mode number
     */
    protected void setModeNumber(int mode) {
        modenum = mode;
    }
    /**
     * Get list of TagLink attribute IDs reported for TagLinks.
     * 
     * @return list of TagLink attribute IDs
     */
    public String[] getTagLinkAttributeIDs() {
        return MantisIITagLink.TAGLINKATTRIBIDS;
    }
    /**
     * Add a reader lifecycle listener.
     * 
     * @param listener -
     *            lifecycle listener
     */
    public void addReaderLifecycleListener(ReaderLifecycleListener listener) {
        lifecycle_listeners.add(listener);
    }
    /**
     * Remove a reader lifecycle listener.
     * 
     * @param listener -
     *            lifecycle listener
     */
    public void removeReaderLifecycleListener(ReaderLifecycleListener listener) {
        lifecycle_listeners.remove(listener);
    }
    /**
     * Call reader lifecycle listeners for reader create
     * 
     * @param reader -
     *            reader created
     */
    void callReaderLifecycleForCreate(Reader reader) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleCreate(reader);
        }
    }
    /**
     * Call reader lifecycle listeners for reader delete
     * 
     * @param reader -
     *            reader deleted
     */
    void callReaderLifecycleForDelete(Reader reader) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleDelete(reader);
        }
    }
    /**
     * Call reader lifecycle listeners for reader enable/disable
     * 
     * @param reader -
     *            reader enabled/disabled
     * @param enable -
     *            true if enabled, false if disabled
     */
    void callReaderLifecycleForEnable(Reader reader, boolean enable) {
        for (ReaderLifecycleListener lsnr : lifecycle_listeners) {
            lsnr.readerLifecycleEnable(reader, enable);
        }
    }
    /**
     * Get channel count
     * 
     * @return count (2 in these models)
     */
    int getChannelCount() {
        return chan_cnt;
    }
    /**
     * Set channel count
     * 
     * @param count -
     *            channel count
     */
    protected void setChannelCount(int count) {
        chan_cnt = count;
    }
	/**
	 * Set max groupcode count
	 */
	protected void setGroupCodeMax(int count) {
		max_grp_code = count;
	}
	/**
	 * Get max groupcode count
	 */
	public int getGroupCodeMax() {
		return max_grp_code;
	}
	/**
	 * Get GPS support - override for factories supporting it
	 */
	public boolean getGPSSupport() {
		return false;
	}
    /* 
     * Get support for !interfacestatus command
     */
    public boolean getInterfaceStatusSupport() {
        return false;
    }
}
