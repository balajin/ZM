package com.rfcode.drivers.readers.mantis2;

/**
 * Tag type for treatment 04E tags - Mantis II Tags - Treatment 04E (pressure)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04E extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04E";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.PRESSURE_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB,
        MantisIITagType.SENSOR_LOWBATT_ATTRIB,
        MantisIITagType.BUTTON0_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.PRESSURE_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL,
        MantisIITagType.SENSOR_LOWBATT_ATTRIB_LABEL,
        MantisIITagType.BUTTON0_ATTRIB_LABEL
        };
    private static final int PRESSURE_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

    private static final int SENSOR_LOWBATT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int BUTTON0_ATTRIB_INDEX = 3; /*
                                                             * Index in list of

                                                             * attribute
                                                             */

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null,
        Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 20000;

	private static final double MAX_UNVERIFIED_PRESS_DELTA = 2.0;	/* 2 psi max change without verify */

    /**
     * Constructor
     */
    public MantisIITagType04E() {
        super(4, 'D', PREV_PAYLOAD_CNT);
        setLabel("RF Code Mantis II Tag - Treatment 04E");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        /* If second half of two part payload (low part) */
        if (cur_payload < 0400) {
            /* If previous was high value, and not too old */
            if ((payloads[0] >= 0400)
                && ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
                int v = ((payloads[0] & 0xFF) << 8) | (cur_payload & 0xFF);
                v >>= 4;     /* Roll off other flags */
                /* Compute pressure */
                double tmp = (double)v;
                change = updateDoubleVerify(tag, PRESSURE_ATTRIB_INDEX, tmp, verify,
					MAX_UNVERIFIED_PRESS_DELTA) || change;
            }
            /* Get low battery value - verify on set */
			boolean vv = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
            /* Get sesnor low battery value - verify on set */
			vv = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, SENSOR_LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
            /* Get button 0 value - verify on set */
			vv = ((cur_payload & 0x04) != 0);
            change = updateBooleanVerify(tag, BUTTON0_ATTRIB_INDEX, vv, verify && vv) || change;
        }
        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
}
