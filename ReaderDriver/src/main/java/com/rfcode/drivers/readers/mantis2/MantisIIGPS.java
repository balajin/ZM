package com.rfcode.drivers.readers.mantis2;

import java.util.HashMap;
import java.util.Collection;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ListDouble;
import com.rfcode.ranger.RangerServer;

/**
 * Class for supporting GPS attached to Mantis-family reader
 * 
 * @author Mike Primm
 */
public class MantisIIGPS implements GPS {
	private GPSData data;
	private String readerid;
	private String id;
    private String type;
    private static class TagGPSData {
        GPSData data;
        long    last_ts;
    };
    private HashMap<String, TagGPSData> last_gpsdata_by_tagid = new HashMap<String, TagGPSData>();
    /**
     * Constructor for GPS
     * 
     * @param rdrid -
     *            our reader ID
     */
    MantisIIGPS(String rdrid) {
		data = new GPSData(GPSData.Fix.NO_FIX);	/* Initial data state */
		readerid = rdrid;	/* Our reader */
        type = GPSTYPE_GPS;
    }
    /**
     * Constructor for static position source
     */
    MantisIIGPS(String rdrid, ListDouble pos) {
        if(pos.size() < 2) {
            pos = MantisIIReaderFactory.DEFAULT_STATICLATLON;
        }
        data = new GPSData(GPSData.Fix.FIX_2D, pos.get(0), pos.get(1), null, null, null, null, null);
        readerid = rdrid;
        type = GPSTYPE_STATIC;
    }
    /**
     * Initialization method - must be called once all attributes have been
     * initialized
     *
     * @throws DuplicateEntityIDException
     *             if TagGroup is duplicate of existing entity
     * @throws if
     *             missing required attribute
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        if (readerid == null) {
            throw new BadParameterException("Missing reader ID");
        }
        if (id == null) {
            throw new BadParameterException("Missing ID");
        }
		/* Finally, add ourselves to the directory */
        ReaderEntityDirectory.addEntity(this);
		/* And notify listeners */
		RangerServer.notifyGPSCreate(this);
    }
    /**
     * Discard the TagGroup
     */
    public void cleanup() {
		if(id != null) {
			/* Notify listeners */
			RangerServer.notifyGPSDelete(this);

			ReaderEntityDirectory.removeEntity(this);
			id = null;
		}
    }
    /**
     * Get ID of entity - unique among all reader entities in system
     * 
     * @return ID
     */
    public String getID() {
		return id;
	}
    /**
     * Assign unique ID to entity - does not apply until init() invoked
     * 
     * @param id -
     *            ID to be assigned
     */
    public void setID(String i_d) {
		id = i_d;
	}
	/**
	 * Get reader associated with GPS, if any
	 */
	public Reader getReader() {
		if(readerid != null) 
			return ReaderEntityDirectory.findReader(readerid);
		return null;
	}
	/**
	 * Get ID of reader associated with GPS, if any
	 */
	public String getReaderID() {
		return readerid;
	}
	/**
	 * Get current GPS readings
	 */
	public GPSData getGPSData() {
		return data;
	}
	/**
	 * Set current GPS readings
	 */
	void setGPSData(GPSData d) {
		GPSData prev = data;
		data = d;

		/* And notify listeners */
		RangerServer.notifyGPSDataUpdate(this, prev);
	}
    /**
     * Get Type of entity
     * 
     * @return "GPS" or "STATIC"
     */
    public String getType() {
		return type;
	}
    /**
     * Get last GPS data for given tag
     * @param guid - tag GUID
     */
    public GPSData getLastGPSDataForTag(String guid) {
        TagGPSData d = last_gpsdata_by_tagid.get(guid);
        if(d != null)
            return d.data;
        else
            return null;
    }
    /**
     * Get last GPS data timestamp for given tag
     * @param guid - tag GUID
     */
    public long getLastGPSTimestampForTag(String guid) {
        TagGPSData d = last_gpsdata_by_tagid.get(guid);
        if(d != null)
            return d.last_ts;
        else
            return 0;
    }
    /**
     * Update GPS data for tag with given values
     */
    public void setTagGPSData(String guid, GPSData dat, long ts) {
        if(dat != null) {
            TagGPSData d = last_gpsdata_by_tagid.get(guid);
            if(d == null) {
                d = new TagGPSData();
                last_gpsdata_by_tagid.put(guid, d);
            }
            d.data = dat;
            d.last_ts = ts;
        }
        else {
            last_gpsdata_by_tagid.remove(guid);
        }
    }
    /**
     * Update GPS data for tag with current value
     */
    public void setTagGPSData(String guid, long ts) {
        setTagGPSData(guid, data, ts);
    }
    /**
     * Clean up tag for all GPSs
     */
    static void cleanupTag(String guid) {
        Collection<GPS> gpss = ReaderEntityDirectory.getGPSs();
        for(GPS gps : gpss) {
            if(gps instanceof MantisIIGPS)
                ((MantisIIGPS)gps).last_gpsdata_by_tagid.remove(guid);
        }
    }
}
