package com.rfcode.drivers.readers.mantis2;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import com.rfcode.drivers.*;
import com.rfcode.drivers.impl.SessionFactoryImpl;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderStatusListener;

/**
 * Test application for MantisIIReaderFactory and associated classes
 * 
 * @author Mike Primm
 */
public class TestApp {
    private static final String PROMPT = "# ";
    private static M250ReaderFactory readerfactory;
    private static SessionFactory sf;

    /* Handler for inbound (command) sessions */
    private static class InboundSessionHandler implements SessionHandler,
        MantisIIReaderCommandResponseListener, ReaderStatusListener {
        private StringBuilder sb = new StringBuilder();
        private String sessid;
        private Session sess;
        private boolean cmdsess;
        private static List<InboundSessionHandler> active_sessions = new ArrayList<InboundSessionHandler>();
        private static OurTagMsgListener tag_listen = new OurTagMsgListener();
        /**
         * Our private tag message listener
         */
        private static class OurTagMsgListener implements
            MantisIIReaderTagMessageListener {
            /**
             * Callback for delivering tag messages - callback MUST NOT block or
             * do significant processing
             * 
             * @param sess -
             *            Reader session
             * @param msg -
             *            tag message - set to null when session is closed
             */
            public void processReaderTagMessage(MantisIIReaderSession sess,
                StringBuilder msg) {
                String m;
                if (msg != null)
                    m = sess.getReaderID() + ": " + msg.toString() + "\r\n";
                else
                    m = sess.getReaderID() + ": <disconnected>\r\n";
                broadcastString(m, false, true);
            }
		    /**
     		* Callback for delivering gps messages - callback MUST NOT block or do
		     * significant processing
		     * 
		     * @param sess -
		     *            Reader session
		     * @param msg -
		     *            tag message - set to null when session is closed
		     */
		    public void processReaderGPSMessage(MantisIIReaderSession sess,
        		StringBuilder msg) {}

        }
        /**
         * Constructor for inbound sessions
         * 
         * @param s -
         *            communications session
         * @param sessionid -
         *            session ID string
         * @param cmd_sess -
         *            if true, its a command session. Otherwise, its a tag event
         *            session
         */
        public InboundSessionHandler(Session s, String sessionid,
            boolean cmd_sess) {
            sessid = sessionid;
            sess = s;
            cmdsess = cmd_sess;
            active_sessions.add(this);
            sess.setSessionHandler(this);
        }
        public void sessionStartingConnect(Session s) {
        }

        public void sessionConnectCompleted(Session s, boolean is_connected) {
            sess = s;
            if (cmdsess) { /* Send logo and prompt for cmdsess */
                sendString("RF Code Reader Multiplexer - Command Server\r\n"
                    + PROMPT);
            } else
                sendString("RF Code Reader Multiplexer - Tag Event Server\r\n");
        }
        /**
         * Send string to our session
         * 
         * @param s -
         *            string to be sent
         */
        private void sendString(String s) {
            byte[] b = s.getBytes();
            try {
                sess.requestDataWrite(b, 0, b.length, null);
            } catch (SessionException sx) {
                sf.getLogger().error(sessid + ": " + sx);
            }
        }
        /**
         * Send string to all active command sessions, tag message sessions, or
         * both.
         * 
         * @param s -
         *            string to be sent
         * @param cmd_sess -
         *            if true, send to command sessions.
         * @param tag_sess -
         *            if tru, send to tag event sessions.
         */
        public static void broadcastString(String s, boolean cmd_sess,
            boolean tag_sess) {
            byte[] bb = s.getBytes();
            for (InboundSessionHandler sess : active_sessions) {
                if ((sess.cmdsess && cmd_sess) || ((!sess.cmdsess) && tag_sess)) {
                    try {
                        sess.sess.requestDataWrite(bb, 0, bb.length, null);
                    } catch (SessionException sx) {
                        sf.getLogger().error(sess.sessid + ": " + sx);
                    }
                }
            }
        }
        /**
         * Send string to all active sessions
         * 
         * @param s -
         *            string to be sent
         */
        //public static void broadcastString(String s) {
        //    broadcastString(s, true, true);
        //}
        /**
         * Session disconnected - used to notify when the session has become
         * disconnected from its target. Starting with this call, requests to
         * write data to the session can no longer be enqueued.
         * sessionDataRead() and sessionDataWriteComplete() callbacks will not
         * happen once the session is disconnected.
         * 
         * @param s -
         *            session
         */
        public void sessionDisconnectCompleted(Session s) {
            active_sessions.remove(s);
        }
        /**
         * Handle a session passthru command
         */
        private void handleSessionPassthru(StringBuilder sb) {
            int end = sb.indexOf(":"); /* Find end */
            if (end >= 0) {
                String id = sb.substring(1, end);
                /* Find the selected session */
                Reader sess = ReaderEntityDirectory.findReader(id);
                if ((sess != null) && (sess instanceof MantisIIReader)) { /*
                                                                             * If
                                                                             * found
                                                                             */
                    end++;
                    while ((end < sb.length()) && (sb.charAt(end) == ' '))
                        end++;
                    /* Create new command request */
                    ((MantisIIReader) sess).getReaderSession().enqueueCommand(
                        sb.substring(end), this);
                } else {
                    sendString("@" + id + ": <invalid target>\r\n");
                }
            }
        }
        /**
         * Handle reader create
         */
        private void handleReaderCreate(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            }
            String id = args[1];
            /* See if ID already used */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess == null) {
                /* Initialize the reader session */
                sess = readerfactory.createReader();
                /* Get defaults */
                Map<String, Object> attr = readerfactory
                    .getDefaultReaderAttributes();
                sess.setID(id);
                try {
                    /* Set the reader's attributes */
                    sess.setReaderAttributes(attr);
                    /* And finish init */
                    sess.init();
                } catch (DuplicateEntityIDException deix) {
                    sendString(id + ": <target exists>\r\n");
                    return;
                } catch (BadParameterException bpx) {
                    sendString(id + ": <" + bpx.getMessage() + ">\r\n");
                    return;
                }
                /* Add our tag listener */
                MantisIIReader rsess = (MantisIIReader) sess;
                rsess.getReaderSession().addTagListener(tag_listen);
                /* Set session handler */
                sess.addReaderStatusListener(this);
                sendString(id + ": <created>\r\n");
            } else {
                sendString(id + ": <target exists>\r\n");
            }
        }
        /**
         * Handle reader attribute set command
         */
        private void handleReaderSet(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            } else if (args.length < 3) {
                sendString("<error>: missing attribute id\r\n");
                return;
            } else if (args.length < 4) {
                sendString("<error>: missing attribute value\r\n");
                return;
            }
            String id = args[1];
            String attrid = args[2];
            String aval = args[3];
            /* See if ID exists */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess != null) {
                boolean update = false;
                /* Get attributes */
                Map<String, Object> attr = sess.getReaderAttributes();
                /* Get current attribute value */
                Object v = attr.get(attrid);
                /* If not defined, invalid attrib */
                if (v == null) {
                    sendString(id + ": <bad attribute id>\r\n");
                }
                /* Else, if its an integer */
                else if (v instanceof Integer) {
                    try {
                        Integer ival = Integer.parseInt(aval);
                        /* Update value */
                        attr.put(attrid, ival);
                        update = true;
                    } catch (NumberFormatException nfx) {
                        sendString(id + ": <bad attribute value>\r\n");
                    }
                } else { /* Else, put in a string */
                    attr.put(attrid, aval);
                    update = true;
                }
                /* Set the reader's attributes */
                if (update) {
                    try {
                        sess.setReaderAttributes(attr);
                    } catch (BadParameterException bpx) {
                        sendString(id + ": <" + bpx.getMessage() + ">\r\n");
                    }
                }
            } else {
                sendString(id + ": <invalid exists>\r\n");
            }
        }
        /**
         * Handle reader attribute get
         */
        private void handleReaderGet(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            }
            String id = args[1];
            String attrid = null;
            /* See if ID exists */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess != null) {
                /* Get attributes */
                Map<String, Object> attr = sess.getReaderAttributes();
                if (args.length > 2) {
                    attrid = args[2];
                    /* Get current attribute value */
                    Object v = attr.get(attrid);
                    /* If not defined, invalid attrib */
                    if (v == null) {
                        sendString(id + ": <bad attribute id>\r\n");
                    } else {
                        sendString(id + ": " + attrid + "=" + v.toString()
                            + "\r\n");
                    }
                } else { /* Else, dump all */
                    String[] k = sess.getReaderFactory()
                        .getReaderAttributeIDs();
                    for (int j = 0; j < k.length; j++) {
                        sendString(id + ": " + k[j] + "=" + attr.get(k[j])
                            + "\r\n");
                    }
                }
            } else {
                sendString(id + ": <invalid exists>\r\n");
            }
        }
        /**
         * Handle reader activate
         */
        private void handleReaderActivate(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            }
            String id = args[1];
            /* See if ID already used */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess != null) {
                if (sess.getReaderEnabled() == false) {
                    /* And get it started */
                    sess.setReaderEnabled(true);
                    sendString(id + ": <activated>\r\n");
                } else {
                    sendString(id + ": <already active>\r\n");
                }
            } else {
                sendString(id + ": <invalid target>\r\n");
            }
        }
        /**
         * Handle reader deactivate
         */
        private void handleReaderDeactivate(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            }
            String id = args[1];
            /* See if ID already used */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess != null) {
                if (sess.getReaderEnabled()) {
                    /* And get it stopped */
                    sess.setReaderEnabled(false);
                    sendString(id + ": <deactivated>\r\n");
                } else {
                    sendString(id + ": <already inactive>\r\n");
                }
            } else {
                sendString(id + ": <invalid target>\r\n");
            }
        }
        /**
         * Handle reader delete
         */
        private void handleReaderDelete(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing id\r\n");
                return;
            }
            String id = args[1]; /* Get ID */
            /* Find the selected session */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess != null) { /* If found */
                sess.cleanup();
                sendString(id + ": <deleted>\r\n");
            } else {
                sendString(id + ": <invalid target>\r\n");
            }
        }
        /**
         * Handle reader list
         */
        private void handleReaderList(String[] args) {
            for (Reader sess : ReaderEntityDirectory.getReaders()) {
                String s = sess.getID() + ": ";
                Map<String, Object> a = sess.getReaderAttributes();
                String[] k = sess.getReaderFactory().getReaderAttributeIDs();
                for (int j = 0; j < k.length; j++) {
                    s += k[j] + "=" + a.get(k[j]) + " ";
                }
                s += "enabled=" + sess.getReaderEnabled() + " ";
                s += "state=" + sess.getReaderStatus() + "\r\n";
                sendString(s);
            }
        }
        /**
         * Handle tag group create
         */
        private void handleGroupCreate(String[] args) {
            TagGroup tg;
            if (args.length < 2) {
                sendString("<error>: missing group id\r\n");
                return;
            } else if (args.length < 3) {
                sendString("<error>: missing tag type\r\n");
                return;
            }
            String id = args[1];
            String tt = args[2];
            /* Next, find the tag type */
            TagType ttype = ReaderEntityDirectory.findTagType(tt);
            if (ttype == null) { /* Not valid? */
                sendString("<error>: invalid tag type\r\n");
                return;
            }
            /* Now, get default attributes needed for tag group of type */
            Map<String, Object> gaids = ttype.getTagGroupDefaultAttributes();
            /* Loop through other arguments for values */
            for (int i = 3; i < args.length; i++) {
                int eq = args[i].indexOf('='); /* Find equals */
                if (eq < 0) {
                    sendString("<error>: attribute id=value format error\r\n");
                    return;
                }
                String aid = args[i].substring(0, eq); /* Get id */
                String aval = args[i].substring(eq + 1); /* Get value */
                /* Find default */
                Object oldval = gaids.get(aid);
                if (oldval == null) {
                    sendString("<error>: bad attribute id - " + aid);
                    return;
                }
                if (oldval instanceof Integer) { /* Is integer? */
                    try {
                        oldval = Integer.parseInt(aval); /* Parse value */
                    } catch (NumberFormatException nfx) {
                        sendString("<error>: bad attribute value - " + aval);
                        return;
                    }
                } else {
                    oldval = aval;
                }
                gaids.put(aid, oldval); /* Update value */
            }
            /* Now create tag group */
            tg = ttype.createTagGroup();
            tg.setID(id); /* Set ID */
            tg.setTagGroupAttributes(gaids); /* Set group attributes */
            try {
                tg.init(); /* And initialize it */
            } catch (BadParameterException bpx) {
                sendString("<error>: " + bpx.getMessage() + "\r\n");
                tg.cleanup();
                return;
            } catch (DuplicateEntityIDException dtgx) {
                sendString("<error>: tag group exists\r\n");
                tg.cleanup();
                return;
            }
        }
        /**
         * Handle tag group delete
         */
        private void handleGroupDelete(String[] args) {
            TagGroup tg;
            if (args.length < 2) {
                sendString("<error>: missing group id\r\n");
                return;
            }
            String id = args[1];
            /* Now find tag group */
            tg = ReaderEntityDirectory.findTagGroup(id);
            if (tg != null) { /* If found, delete it */
                tg.cleanup();
            } else {
                sendString("<error>: tag group does not exists\r\n");
            }
        }
        /**
         * Handle group list
         */
        private void handleGroupList(String[] args) {
            StringBuilder sb = new StringBuilder();

            for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
                sb.append(tg.getID());
                sb.append(": type=");
                sb.append(tg.getTagType().getID());
                Map<String, Object> v = tg.getTagGroupAttributes();
                String[] k = tg.getTagType().getTagGroupAttributes();
                for (int i = 0; i < k.length; i++) {
                    Object val = v.get(k[i]); /* Get attribute value */
                    sb.append(", ");
                    sb.append(k[i]);
                    sb.append("=");
                    sb.append(val.toString());
                }
                sb.append("\r\n");
                /* Output completed line */
                sendString(sb.toString());
                sb.setLength(0);
            }
        }
        /**
         * Add group to reader handler
         */
        private void handleReaderGroupAdd(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing reader id\r\n");
                return;
            }
            if (args.length < 3) {
                sendString("<error>: missing tag group id\r\n");
                return;
            }
            String id = args[1];
            String tgid = args[2];
            /* Find reader */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess == null) {
                sendString("<error>: invalid reader id\r\n");
                return;
            }
            /* Find tag group */
            TagGroup tg = ReaderEntityDirectory.findTagGroup(tgid);
            if (tg == null) {
                sendString("<error>: invalid tag group id\r\n");
                return;
            }
            /* Get existing group list for reader */
            TagGroup[] oldset = sess.getReaderTagGroups();
            /* Make sure we're not already in the list */
            for (int i = 0; i < oldset.length; i++) {
                /* Already in set? we're done */
                if (oldset[i].getID().equals(tgid)) {
                    return;
                }
            }
            /* Make sure we're not too many */
            if (oldset.length >= sess.getReaderMaxTagGroupCount()) {
                sendString("<error>: max tag groups for reader\r\n");
                return;
            }
            TagGroup[] newset = new TagGroup[oldset.length + 1];
            System.arraycopy(oldset, 0, newset, 0, oldset.length);
            newset[oldset.length] = tg; /* Append new group */
            /* And set it */
            try {
                sess.setReaderTagGroups(newset);
            } catch (BadParameterException iax) {
                sendString("<error>: " + iax.getMessage());
            }
        }
        /**
         * Remove group to reader handler
         */
        private void handleReaderGroupRemove(String[] args) {
            if (args.length < 2) {
                sendString("<error>: missing reader id\r\n");
                return;
            }
            if (args.length < 3) {
                sendString("<error>: missing tag group id\r\n");
                return;
            }
            String id = args[1];
            String tgid = args[2];
            /* Find reader */
            Reader sess = ReaderEntityDirectory.findReader(id);
            if (sess == null) {
                sendString("<error>: invalid reader id\r\n");
                return;
            }
            /* Get existing group list for reader */
            TagGroup[] oldset = sess.getReaderTagGroups();
            int idx = -1;
            /* Find us in the list */
            int i, j;
            for (i = 0; i < oldset.length; i++) {
                if (oldset[i].getID().equals(tgid)) {
                    idx = i;
                    break;
                }
            }
            /* If not found, quit (already gone) */
            if (idx < 0) {
                return;
            }
            /* Make smaller set */
            TagGroup[] newset = new TagGroup[oldset.length - 1];
            for (i = 0, j = 0; i < oldset.length; i++) {
                if (i == idx)
                    continue; /* Skip */
                newset[j] = oldset[i];
                j++;
            }
            /* And set it */
            try {
                sess.setReaderTagGroups(newset);
            } catch (BadParameterException iax) {
                sendString("<error>: " + iax.getMessage());
            }
        }
        /**
         * Data read from session - used to deliver raw data reads from session.
         * 
         * @param sdr -
         *            read request result - only valid for duration of
         *            sessionDataRead callback
         */
        public void sessionDataRead(SessionRead sdr) {
            /* Ignore input on tag event sessions */
            if (!cmdsess)
                return;
            byte[] b = sdr.readBytes(); /* Get our bytes */
            for (int i = 0; i < b.length; i++) { /* Loop through them */
                char c = (char) b[i]; /* We're ASCII, so we can do this */
                if (c == '\r') { /* End of line? */
                    if (sb.length() > 0) {
                        /* First, see if its a session targetted cmd */
                        if (sb.charAt(0) == '@') {
                            handleSessionPassthru(sb);
                        } else { /* Else, tokenize the command */
                            String[] cmd = sb.toString().split(" ");
                            if (cmd.length == 0) { /* No args? */
                                /* Skip */
                            } else if (cmd[0].equals("readercreate")) { /*
                                                                         * Create
                                                                         * reader
                                                                         */
                                handleReaderCreate(cmd);
                            } else if (cmd[0].equals("readerset")) { /*
                                                                         * Set
                                                                         * reader
                                                                         * attrib
                                                                         */
                                handleReaderSet(cmd);
                            } else if (cmd[0].equals("readerget")) { /*
                                                                         * Get
                                                                         * reader
                                                                         * attrib
                                                                         */
                                handleReaderGet(cmd);
                            } else if (cmd[0].equals("readeractivate")) { /*
                                                                             * Activate
                                                                             * command
                                                                             */
                                handleReaderActivate(cmd);
                            } else if (cmd[0].equals("readerdeactivate")) { /*
                                                                             * Deactivate
                                                                             * command
                                                                             */
                                handleReaderDeactivate(cmd);
                            } else if (cmd[0].equals("readerdelete")) {
                                handleReaderDelete(cmd);
                            } else if (cmd[0].equals("readerlist")) {
                                handleReaderList(cmd);
                            } else if (cmd[0].equals("groupcreate")) {
                                handleGroupCreate(cmd);
                            } else if (cmd[0].equals("groupdelete")) {
                                handleGroupDelete(cmd);
                            } else if (cmd[0].equals("grouplist")) {
                                handleGroupList(cmd);
                            } else if (cmd[0].equals("readeraddgroup")) {
                                handleReaderGroupAdd(cmd);
                            } else if (cmd[0].equals("readerremovegroup")) {
                                handleReaderGroupRemove(cmd);
                            } else {
                                sendString("<Error>: unknown command\r\n");
                            }
                        }
                        sb.setLength(0); /* Clear it */
                    }
                    sendString(PROMPT); /* And send prompt */
                } else if (c == '\n') { /* Ignore newline */
                } else if (c == '\u0008') { /* backspace */
                    if (sb.length() > 0) {
                        sb.setLength(sb.length() - 1);
                    }
                } else {
                    sb.append(c);
                }
            }
        }
        /**
         * Callback method delivering command response strings
         * 
         * @param sess -
         *            Reader session completing command
         * @param rsp -
         *            list of response strings for command
         * @param completed -
         *            true if response completed, false if timeout or failure to
         *            get response
         */
        public void commandResponseCallback(MantisIIReaderSession sess,
            List<String> rsp, boolean completed) {
            for (String s : rsp) {
                sendString("@" + sess.getReaderID() + ": " + s + "\r\n");
            }
            if (completed)
                sendString("@" + sess.getReaderID() + ": <completed>\r\n");
            else
                sendString("@" + sess.getReaderID() + ": <error>\r\n");
        }
        /**
         * Callback for reporing reader status change. Handler MUST process
         * immediately and without blocking.
         * 
         * @param reader -
         *            reader reporting change
         * @param newstatus -
         *            new status for reader
         * @param oldstatus -
         *            previous status for reader
         */
        public void readerStatusChanged(Reader reader, ReaderStatus newstatus,
            ReaderStatus oldstatus) {
            sendString("@" + reader.getID() + ": <" + newstatus.toString()
                + ">\r\n");
        }
    }

    private static class TestTCPIPServerHandler implements TCPIPServerHandler {
        private boolean cmdsess;

        public TestTCPIPServerHandler(boolean commandsessions) {
            cmdsess = commandsessions;
        }
        /**
         * Session accepted - used to notify when a client connection has been
         * established with the server
         * 
         * @param server -
         *            our server
         * @param session -
         *            session with client
         */
        public void sessionAcceptCompleted(TCPIPServerSocket server,
            TCPIPSession session) {
            new InboundSessionHandler(session, session.getIPAddress()
                .toString(), cmdsess);
        }
        /**
         * Server socket bind completed
         * 
         * @param server -
         *            our server
         * @param is_bound -
         *            true if successful, false if failed
         */
        public void serverBindCompleted(TCPIPServerSocket server,
            boolean is_bound) {
            // System.out.println("Bind completed: " + is_bound);
            if (is_bound == false) {
                sf.getLogger().error(
                    "Error binding to port " + server.getIPAddress().getPort());
                System.exit(1);
            }
        }
        /**
         * Server socket unbind completed
         * 
         * @param server -
         *            our server
         */
        public void serverUnbindCompleted(TCPIPServerSocket server) {
        }
    }
    /**
     * Main entry point for test app
     * 
     * @param args
     */
    public static void main(String[] args) {
        /* Load our reader factory */
        readerfactory = new M250ReaderFactory();

        try {
            /* Initialize our tag type beans */
            new MantisIITagType04A().init();
            new MantisIITagType04B().init();
            new MantisIITagType04C().init();
            new MantisIITagType04D().init();
            new MantisIITagType04E().init();
            new MantisIITagType04F().init();
            new MantisIITagType04H().init();
            
            /* Get our session factory */
            sf = new SessionFactoryImpl();
            /* Set session factory for our reader factory */
            readerfactory.setSessionFactory(sf);
            /* Create server socket on 6501 for command sessions */
            TCPIPServerSocket ss = sf.createTCPIPServerSocket();
            ss.setIPAddress(new InetSocketAddress(6501));
            ss.setServerHandler(new TestTCPIPServerHandler(true));
            ss.requestBind(); /* Activate it */
            /* Create server socket on 6502 for tag event sessions */
            ss = sf.createTCPIPServerSocket();
            ss.setIPAddress(new InetSocketAddress(6502));
            ss.setServerHandler(new TestTCPIPServerHandler(false));
            ss.requestBind(); /* Activate it */
        } catch (SessionException sx) {
            System.err.println(sx);
        } catch (DuplicateEntityIDException dttx) {
            System.err.println(dttx);
        } catch (BadParameterException bpx) {
            System.err.println("Error: " + bpx.getMessage());
        }
    }
}
