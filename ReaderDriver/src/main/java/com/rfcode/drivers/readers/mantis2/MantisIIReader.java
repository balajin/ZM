package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.net.InetAddress;
import java.net.UnknownHostException;
import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.ReaderFactory;
import com.rfcode.drivers.readers.ReaderStatus;
import com.rfcode.drivers.readers.ReaderStatusListener;
import com.rfcode.drivers.readers.ReaderServiceStatusListener;
import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.Session;
import com.rfcode.ranger.RangerServer;
import com.rfcode.ranger.RangerProcessor;
import com.rfcode.ranger.FirmwareLibrary;
import com.rfcode.ranger.TavisConcentratorDirectStreamHandler;
import com.rfcode.ranger.FirmwareLibrary.FirmwareStream;
import com.rfcode.drivers.readers.AbstractTagType;
import com.rfcode.drivers.readers.SerialSender;
import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.DuplicateEntityIDException;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.GPS;
import com.rfcode.drivers.readers.ListDouble;
import com.rfcode.drivers.readers.ReaderGPSStatusListener;
import com.rfcode.drivers.readers.ReaderNetworkInterfaceStatusListener;
import com.rfcode.drivers.readers.ReaderNetworkInterface;
import com.rfcode.drivers.BadParameterException;
import java.net.InetSocketAddress;
import java.io.IOException;
import com.rfcode.drivers.SessionException;
import com.rfcode.drivers.readers.GPSDefaults;
import com.rfcode.drivers.readers.GPSData;
import com.rfcode.drivers.locationrules.impl.SimpleSSIGPSRule;
import com.rfcode.drivers.locationrules.impl.SimpleSSILocationRuleFactory;
import com.rfcode.drivers.locationrules.impl.IRLocatorLocationRuleFactory;

/**
 * Driver for Mantis II family of readers. Code depends upon the v2.34 DSP
 * firmware for full functionality.
 * 
 * @author Mike Primm
 */
public class MantisIIReader implements Reader, SerialSender, GPSDefaults.UpdateListener {
	private static final String UPGMETH_FW = "fw";
	private static final String UPGMETH_TERMUPG = "termupg";

    private MantisIIReaderFactory factory; // Our factory
    private MantisIIReaderSession session; // Our session
    private String readerid; // Our unique reader ID
    private String readername;  // Our name/label (default to ID) 
    private String readernamesymbolic;  /* Version of name with spaces, non-alphanums removed */
    private ReaderStatus status; // Current reader status
    private boolean enabled; // Reader is enabled?
    private int conncnt;    // Number of connection attempts (reset on success)
    private Map<String, Object> attribs_ro = Collections.emptyMap();
    private ArrayList<ReaderStatusListener> status_listeners = new ArrayList<ReaderStatusListener>();
    private MantisIITagGroup[] taggrps; // List of current tag groups
    private MantisIITagGroup[] active_taggrps; // List of active tag groups (successfully configured)
    private boolean inited; // Has been been initialized?
    private MantisIIReaderChannel[] channels; // Channels associated with
    private int noise_thresh;
    private int evtpersec_thresh;
    private int tag_cap_pct;
    private boolean merge_channels;
    private boolean join_channels;
    private boolean upconn_enabled;
    private boolean ignore_lowconf_init;
    private String upconn_rdrid;
    private String upconn_pwd;
    private String last_upconn_mac;
    private int part_index;
    private int part_count;
    private int part_period;    /* Number of seconds per step in partition rotation (<=0 if no rotation) */
    private RotatePartAction rotact;
	private boolean upgrade_requested;
    private String sslmode = MantisIIReader.SSLMODE_OFF; /* OFF, IFAVAIL, REQUIRED, STRICT */
    public static final String SSLMODE_OFF = "OFF";
    public static final String SSLMODE_IFAVAIL = "IFAVAIL";
    public static final String SSLMODE_REQUIRED = "REQUIRED";
    public static final String SSLMODE_STRICT = "STRICT";
    private String positionsrc = MantisIIReaderFactory.ATTRIB_POSITIONSRC_NONE;
    // reader
    private ArrayList<String> init_cmds; // Current initialization sequence
    private InetSocketAddress sockaddr; // Current socket address
	private String hostname;	// Hostname for connection
	private int		portnum;	// Port number for connection
    private String fw_ver = ""; // Detected firmware version (compatable)
    private String fw_mod = "Mantis II"; // Detected firmware model
	private String fw_fam = "";
	private String true_fw_ver = "";	// "true" firmware version (not M200 compatable)

	private String upg_meth;	/* Upgrade method, if we're doing upgrade */
	private FirmwareStream upg_fw;	/* Input stream for firmware, if we're doing upgrade */
	private long upg_off;		/* Upgrade offset */
	private boolean verify_supported;	/* Is verify supported by the reader? */
	/* Our GPS */
	private MantisIIGPS our_gps;
	private SimpleSSIGPSRule our_gps_rule;	/* Rule for matching tags to our GPS */
    private long min_gps_period;    /* Minimum GPS reporting period, in msec */
    private int min_gps_ssi;        /* Minimum SSI to match GPS (0= any) */
    // HW info
    private String serialnum = null;
    private String servicedate = null;
    private String manufdate = null;
    private String macaddr = null;
    
    private String label;

    private long    startup_utc_msec = 0;
    /** ********************************************************************************* */
    /*
     * These variables are only used by the worker thread - don't mess with them
     * once the session is enabled
     */
    private MantisIITagType[] tagtypes; // List of tag types, from taggrps
    private String[] taggcode; // List of group codes, from taggrps
    private Object[] oldvals;
    /** ********************************************************************************* */
    /* Map of upconnection readers, by rdrid */
    private static HashMap<String,MantisIIReader> upconn_rdrs = new HashMap<String,MantisIIReader>();

    public static final int SLEEP_TAGLINK_AGEOUT = 5;   /* Age out tag links in 5 seconds for optima sleep */

	/* GPS info flags */
	private static final int GPS_INFO_FIX = (1<<0);
	private static final int GPS_INFO_LAT_LON = (1<<1);
	private static final int GPS_INFO_SPD_COURSE = (1<<2);
	private static final int GPS_INFO_ALT = (1<<3);
	private static final int GPS_INFO_EPE =	(1<<4);

    private InitializeReaderAction cur_act = null;
    
	private static Map<String,String> splitLine(String line) {
		String[] spl = line.split(", ");	// Split line
		HashMap<String, String> rslt = new HashMap<String, String>();
		for (String s : spl) {
			String[] v = s.split("=");	// Split at equals
			if (v.length == 2) {	// Valid token?
				if (v[1].startsWith("\"")) { // Quoted?
					v[1] = v[1].substring(1, v[1].length()-1);
				}
				rslt.put(v[0].trim(), v[1]);
			}
		}
		return rslt;
	}

    /* Handler class for initialization sequence for reader - M200 telnet mode */
    private class InitializeReaderAction implements Runnable,
        MantisIIReaderCommandResponseListener {
        private int stepnum;
        private ArrayList<String> steps;
        private MantisIITagGroup[] grps;

        public InitializeReaderAction() {
            active_taggrps = null;           /* Clear active - prevent stale message processing */
            steps = init_cmds; /* Save command set */
            grps = taggrps; /* Save tag group list */
            stepnum = 0;
            cur_act = this;
        }
        /* Action to run */
        public void run() {
            if(cur_act != this)
                return;
            if(factory == null) /* If deleted, quit */
                return;
            /* Set out state to initializing (or upgrading) */
			if(upgrade_requested)
	            updateReaderStatus(ReaderStatus.FIRMWAREUPGRADING);
			else
	            updateReaderStatus(ReaderStatus.INITIALIZING);
            /* First, update our group info - want to do this in worker thread */
            tagtypes = new MantisIITagType[grps.length];
            taggcode = new String[grps.length];
            for (int i = 0; i < grps.length; i++) {
                TagType tt = grps[i].getTagType();
                if (!(tt instanceof MantisIITagType)) {
                    RangerServer.error(
                        "Invalid tag type in group " + grps[i].getID());
                    updateReaderStatus(ReaderStatus.CONFIGFAILURE);
                    return;
                }
                tagtypes[i] = (MantisIITagType) tt;
                Map<String,Object> ga = grps[i].getTagGroupAttributes();
                if(ga != null) {
                    taggcode[i] = (String) ga.get(
                        MantisIITagGroup.GROUPCODE_GROUPATTRIB);
                }
                else {
                    updateReaderStatus(ReaderStatus.CONFIGFAILURE);
                    return;
                }
            }
			/* If doing upgrade, supress diag and ping processing */
			if(upgrade_requested)
				session.setDiagPingSuppress(true);
			else
				session.setDiagPingSuppress(false);
            /* Run the first command in the sequence */
            session.enqueueCommand(steps.get(stepnum), this);
        }
		private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
 		private String base64encode(byte[] buf, int len) {
			StringBuilder sb = new StringBuilder(((len+2)/3) * 4);
			int i, v;

			for(i = 0; i < (len-2); i += 3) {
	            v = ((buf[i] & 0xff) << 16) + ((buf[i+1] & 0xff) << 8) + (buf[i+2] & 0xff);
				sb.append(base64code.charAt((v >> 18) & 0x3f)).
					append(base64code.charAt((v >> 12) & 0x3f)).
					append(base64code.charAt((v >> 6) & 0x3f)).
					append(base64code.charAt(v & 0x3f));
			}
			/* If remainder, add needed codes */
			if((len % 3) == 1) {	/* One extra byte */
				v = ((buf[len-1] & 0xff) << 16);
				sb.append(base64code.charAt((v >> 18) & 0x3f)).
					append(base64code.charAt((v >> 12) & 0x3f)).
					append("==");
			}
			else if((len % 3) == 2) {	/* Two extra bytes */
	            v = ((buf[len-2] & 0xff) << 16) + ((buf[len-1] & 0xff) << 8);
				sb.append(base64code.charAt((v >> 18) & 0x3f)).
					append(base64code.charAt((v >> 12) & 0x3f)).
					append(base64code.charAt((v >> 6) & 0x3f)).
					append('=');
			}
			return sb.toString();
		}
		/**
		 * Add next firmware command
		 */
		private boolean add_next_fw_command() {
			byte[] buf;
			int len;
			boolean good = true;
			if(upg_fw == null)
				return false;
			try {
				if(upg_meth.equals(UPGMETH_FW)) {
					boolean isend = false;
					len = 0;
					buf = new byte[54];	/* Do 54 bytes at a time */
					while((!isend) && (len < buf.length)) {
						int len2 = upg_fw.stream.read(buf, len, buf.length-len);	/* Read them */
						if(len2 < 0)
							isend = true;
						else
							len += len2;
					}
					if(len <= 0) {	/* Nothing more, we're on last one */
						steps.set(stepnum, ":fw,done");		/* Make done */
//						RangerServer.info(steps.get(stepnum));
						stepnum--;		/* Rewind to get it to run */
						FirmwareLibrary.closeFirmware(upg_fw);
						upg_fw = null;	/* Close it up */
						upg_off = -1;	/* Mark as done */

						requestFirmwareUpgrade(true);			/* Cancel request */							
					}
					else {			/* Else, send bytes */
						if(isend && ((len%3) != 0)) {
							len += 3 - (len%3);
						}
						steps.set(stepnum, ":fw," + upg_off + "," + base64encode(buf, len));
//						RangerServer.info(steps.get(stepnum));
						stepnum--;
						upg_off += len;	/* Advance offset */
					}
				}
				else {	/* Else, termupg */
					if((upg_off == 0) && (steps.get(stepnum).equals("!softwareupgrade") == false)) {
						steps.set(stepnum, "!softwareupgrade");
//						RangerServer.info(steps.get(stepnum));
						stepnum--;
					}
					else {
						buf = new byte[64*1024];		/* 64K at a time */
						boolean isend = false;
						len = 0;
						while((!isend) && (len < buf.length)) {
							int len2 = upg_fw.stream.read(buf, len, buf.length-len);	/* Read them */
							if(len2 < 0)
								isend = true;
							else
								len += len2;
						}
						if(len <= 0) {	/* Nothing more, we're on last one */
							steps.set(stepnum, "!");		/* Make done */
//							RangerServer.info(steps.get(stepnum));
							stepnum--;		/* Rewind to get it to run */
							FirmwareLibrary.closeFirmware(upg_fw);
							upg_fw = null;	/* Close it up */
							upg_off = -1;	/* Mark as done */
							if(session != null)
								session.setDiagPingSuppress(false);
						}
						else {			/* Else, send bytes */
							steps.set(stepnum, "!" + base64encode(buf, len));
//							RangerServer.info("!" + upg_off);
							stepnum--;
							upg_off += len;
						}
					}
				}
			} catch (IOException iox) {
				if(upg_fw != null) {
					FirmwareLibrary.closeFirmware(upg_fw);
					upg_fw = null;	/* Close it up */
				}
				good = false;
			}
			return good;
		}
        /**
         * Callback method delivering command response strings
         * 
         * @param sess -
         *            Reader session completing command
         * @param rsp -
         *            list of response strings for command
         * @param completed -
         *            true if response completed, false if timeout or failure to
         *            get response
         */
        public void commandResponseCallback(MantisIIReaderSession sess,
            List<String> rsp, boolean completed) {
            if(cur_act != this)
                return;
            if(factory == null) /* If deleted, quit */
                return;
            if(RangerServer.isReaderInitLoggingActive()) {
                String s = "ReaderInit: reader=" + readerid + ", step=\'" + steps.get(stepnum) + "\', rsp={";
                for(int i = 0; i < rsp.size(); i++) {
                    if(i > 0) s += ",";
                    s += "\'" + rsp.get(i) + "\'";
                }
                s += "}, completed=" + completed;
                RangerServer.info(s);
            }
			/* Special case - if "M,0" or "V" command on upgrade, and "E,7" error, ignore */
			if(completed && (steps.get(stepnum).equals("M,0") || steps.get(stepnum).equals("V")) &&
				(rsp.size() > 0) &&	rsp.get(0).equals("E,7")) {
				rsp.clear();
				fw_fam = "";	/* Clear fw_fam, so that we use :ver one */
			}
            /* Special case - STARTTLS can fail if we're in IFAVAIL mode */
            if(completed && steps.get(stepnum).equals("STARTTLS,IFAVAIL") && (rsp.size() > 0) &&
                rsp.get(0).equals("E,2")) {
                rsp.clear();
            }
            /* If completed and no response or non-error response */
            if (completed
                && ((rsp.size() == 0) || (!rsp.get(0).startsWith("E,")))) {
				/* Handle :ver response as special */
				if(steps.get(stepnum).equals(":ver") && (rsp.size() > 0)) {
					String[] vs = rsp.get(0).split(",");	/* Split it */
					if((vs.length > 3) && vs[0].equals("=") && vs[1].equals("ver")) {
						if(fw_fam.equals(""))
							fw_fam = vs[3].trim();
						if(vs.length > 5) {
							true_fw_ver = vs[5].trim();
						}
						else {
							true_fw_ver = "";
						}
						/* Now, if firmware upgrade requested, try to get started */
						if(upgrade_requested) {
							if(upg_fw != null) { 	/* Clean of old stream, if any */
								FirmwareLibrary.closeFirmware(upg_fw);
								upg_fw = null; 
							}
							upg_meth = FirmwareLibrary.getFirmwareMethod(fw_fam);
							/* Unsupported method? */
							if((upg_meth == null) || 
								(!(upg_meth.equals(UPGMETH_FW) || upg_meth.equals(UPGMETH_TERMUPG)))) {
								requestFirmwareUpgrade(true);	/* Cancel request */
								return;			/* And quit */
							}
							upg_fw = FirmwareLibrary.openFirmware(fw_fam);	/* Open stream */
							upg_off = 0;
							if(upg_fw == null) {
								requestFirmwareUpgrade(true);	/* Cancel request */
								return;			/* And quit */
							}
							/* Now, add first firmware command (sets next command) */
							stepnum++;
							add_next_fw_command();
						}
					}
				}
                /* Handle V response as special */
                else if (steps.get(stepnum).equals("V") && (rsp.size() > 0)) {
                    String[] vs = rsp.get(0).split(","); /* Split it */
                    if ((vs.length > 3) && (vs[0].equals("V"))) {
                        boolean upd = false;

                        if (!fw_mod.equals(vs[1].trim())) { /* Model differs? */
                            fw_mod = vs[1].trim(); /* Update it */
                            upd = true;
                        }
                        if (!fw_ver.equals(vs[2].trim())) { /* Version differs? */
							fw_ver = vs[2].trim();
                            upd = true;
                        }
						String fam = splitFromVer(fw_ver, true);
						if(fam.equals("") == false)
							fw_fam = fam;
                        if (upd && (!upgrade_requested)) { /* Change and not upgrade? regen init */
                            if (generateInitializationCmds()) {
                                /*
                                 * Updated again - need to repeat - start again
                                 * in 2 seconds
                                 */
                                RangerProcessor.addImmediateAction(new InitializeReaderAction());

                                return;
                            }
                        }
                    }
                }
                /* Handle :fw response as special */
                else if (steps.get(stepnum).startsWith(":fw") && (rsp.size() > 0)) {
					if(rsp.get(0).startsWith("=,fw")) {	/* We're good? */
						if(steps.get(stepnum).equals(":fw,done")) {	/* We're done? */
			                try {
			                    session.forceDisconnect(); /* Force a disconnect */
			                } catch (SessionException sx) {
			                }
							return;	/* And quit */
						} else {
							add_next_fw_command();
						}
					}
					else if(upg_fw != null) {						/* Else, we're broken */
						FirmwareLibrary.closeFirmware(upg_fw);
						upg_fw = null; 
					}
				}
                else if(steps.get(stepnum).equals("!rdrstatus")) {
                    if((rsp.size() > 0) && (rsp.get(0).startsWith("readerstatus:"))) {   /* Good response */
                        String r = rsp.get(0);
                        int idx = r.indexOf("startuptime=");
                        if(idx >= 0) {
                            r = r.substring(idx + "startuptime=".length());
                            idx = r.indexOf(',');
                            if(idx >= 0)
                                r = r.substring(0, idx);
                            try {
                                double ts = Double.parseDouble(r);
                                startup_utc_msec = (long)(ts * 1000.0);
                            } catch (NumberFormatException nfx) {
                                startup_utc_msec = 0;   
                            }
                        }
                        else {
                            startup_utc_msec = 0;
                        }   
                    }
                }
                else if(steps.get(stepnum).equals("!version")) {
                    if((rsp.size() > 0) && (rsp.get(0).contains("config-hashcode="))) {   /* Good response */
                		Map<String, String> rslt = splitLine(rsp.get(0));
            			// Check family
                		String v = rslt.get("family");
                		if (v != null) {
                			fw_fam = v;
                		}
                		// Check release (true version)
                		v = rslt.get("release");
                		if (v != null) {
                			true_fw_ver = v;
                		}
                	    serialnum = rslt.get("serialnumber");
                	    servicedate = rslt.get("servicedate");
                	    manufdate = rslt.get("manufacturedate");
                    }
                }
                else if(steps.get(stepnum).equals("!interfacestatus")) {
                    if((rsp.size() > 0) && (rsp.get(0).startsWith("eth0:"))) {   /* Good response */
                		Map<String, String> rslt = splitLine(rsp.get(0).substring(5));
            			// Check hwaddr
                		macaddr = rslt.get("hwaddress");
                    }
                }
                /* Handle "!" response as special */
                else if (steps.get(stepnum).startsWith("!")) {
					if((rsp.size() > 0) && (rsp.get(0).startsWith("<error>") || rsp.get(0).startsWith("E,"))) { 	 
                        updateReaderStatus(ReaderStatus.CONFIGFAILURE); 	 
                        requestFirmwareUpgrade(true);                   /* Cancel request */ 	 
                    	try { 	 
                            session.forceDisconnect(); /* Force a disconnect */ 	 
                        } catch (SessionException sx) { 	 
                        } 	 
                        return; /* And quit */ 	 
                    }
					if(steps.get(stepnum).equals("!")) {	/* We're done? */
						requestFirmwareUpgrade(true);			/* Cancel request */							
		                try {
		                    session.forceDisconnect(); /* Force a disconnect */
		                } catch (SessionException sx) {
		                }
						return;	/* And quit */
					} else {
						add_next_fw_command();
					}
				}

                stepnum++; /* Advance to next step */
                if (stepnum >= steps.size()) { /* Are we done? */
                    /* We're all set */
                    updateReaderStatus(ReaderStatus.ACTIVE);
                    if(init_cmds == steps)  /* If we completed (still) current sequence */
                        active_taggrps = grps;  /* Save pointer to actual active ones */
                } else { /* Else, keep going */
                    session.enqueueCommand(steps.get(stepnum), this);
                }
            } else { /* Else, error - we're dead */
                if((rsp.size() > 0) && (rsp.get(0).startsWith("E,8"))) {
	                updateReaderStatus(ReaderStatus.ACCESSDENIED);
                }
			    else {
    				updateReaderStatus(ReaderStatus.CONFIGFAILURE);
                }
				String cmd = steps.get(stepnum);
				if(cmd.length() > 32) cmd = cmd.substring(0,32) + "...";
                RangerServer.error(
                    	"Config command failed: reader=" + readerid + ", cmd='"
                        + cmd + "', rsp='"
                        + ((rsp.size() > 0) ? rsp.get(0) : "<null>") + "'");
                try {
                    session.forceDisconnect(); /* Force a disconnect */
                } catch (SessionException sx) {
                }
            }
            /* See if we're in sync - might need to repeat */
            if (init_cmds != steps) {
                /* Set back to initializing state */
				if(upgrade_requested)
					updateReaderStatus(ReaderStatus.FIRMWAREUPGRADING);
				else
	                updateReaderStatus(ReaderStatus.INITIALIZING);

				if(upg_fw != null) {	/* Clean up fw lib, if active */
					FirmwareLibrary.closeFirmware(upg_fw);
					upg_fw = null; 
				}
                /* Updated again - need to repeat - start again in 1 second */
                RangerProcessor.addDelayedAction(new InitializeReaderAction(), 1000);
            }
        }
    }

    /**
     * Our handler class - used for sessions state changes
     */
    private class OurReaderSessionHandler implements MantisIIReaderSessionHandler {
        /**
         * Session connect starting. Called before the connect is initiated.
         * 
         * @param s -
         *            session
         */
        public void readerSessionStartingConnect(MantisIIReaderSession s) {
            if(factory == null) /* If deleted, quit */
                return;
            /* We're now connecting */
            updateReaderStatus(ReaderStatus.CONNECTING);
            if(sockaddr != null) {
                session.setIPAddress(sockaddr);
			}
        }
        /**
         * Session connected - used to notify when the session has completed an
         * attempt to connect to its target. If is_connected = true, the session
         * has connected successfully. If connected successfully, requests to
         * write data to the session can be enqueued and sessionDataRead()
         * callbacks can begin after this callback completes.
         * 
         * @param s -
         *            session
         * @param is_connected -
         *            if true, session was connected successfully. if false,
         *            connect failed.
         */
        public void readerSessionConnectCompleted(MantisIIReaderSession s,
            boolean is_connected) {
            if(factory == null) /* If deleted, quit */
                return;
            if (is_connected) {
                /* We're now initializing */
				if(upgrade_requested)
					updateReaderStatus(ReaderStatus.FIRMWAREUPGRADING);
				else
	                updateReaderStatus(ReaderStatus.INITIALIZING);

                RangerProcessor.addImmediateAction(new InitializeReaderAction());
            } else {
                updateReaderStatus(ReaderStatus.CONNECTFAILURE);
            }
        }
        /**
         * Session disconnected - used to notify when the session has become
         * disconnected from its target. Starting with this call, requests to
         * write data to the session can no longer be enqueued.
         * sessionDataRead() and sessionDataWriteComplete() callbacks will not
         * happen once the session is disconnected.
         * 
         * @param s -
         *            session
         */
        public void readerSessionDisconnectCompleted(MantisIIReaderSession s) {
            if(factory == null) /* If deleted, quit */
                return;
            updateReaderStatus(ReaderStatus.DISCONNECTED);
            last_upconn_mac = null;
        }
        /**
         * Channel diagnostic updates - returns dBm noise levels for channels
         * @param s - session
         * @param noise_dbm - array of noise levels, in dbm
         * @param evt_per_sec - array of events/sec estimate (null if not defined)
         * @param tagcappct - percent of tag database used (0-100, -1 if unsupported)
         */
        public void readerSesssionChannelDiag(MantisIIReaderSession s,
            int noise_dbm[], int evt_per_sec[], int tagcappct) {
            if(factory == null) /* If deleted, quit */
                return;
            boolean bad = false;
            boolean badeps = false;
            boolean upd = false;
            for(int i = 0; (i < channels.length) && (i < noise_dbm.length); i++) {
                if(channels[i].getChannelNoiseLevel() != noise_dbm[i]) {
                    channels[i].setChannelNoiseLevel(noise_dbm[i]);
                    upd = true;
                }
                if(noise_dbm[i] > noise_thresh) {
                    bad = true;
                }
                int eps = -1;
                if(evt_per_sec != null) {
                    eps = evt_per_sec[i];
                }
                if(channels[i].getChannelEventsPerSec() != eps) {
                    channels[i].setChannelEventsPerSec(eps);
                    upd = true;
                }
                if((eps > evtpersec_thresh) && (evtpersec_thresh > 0)) {
                    badeps = true;
                }
            }
            if(tag_cap_pct != tagcappct) {  /* If new capacity percent, report update */
                tag_cap_pct = tagcappct;
                upd = true;
            }
            if(bad) {       /* If noise is bad */
                if(ReaderStatus.isOnline(status) || upd)
                    updateReaderStatus(ReaderStatus.NOISEDETECTED, upd);
            }
            else if(badeps) {
                if(ReaderStatus.isOnline(status) || upd)
                    updateReaderStatus(ReaderStatus.HIGHTRAFFIC, upd);
            }
            else {
                if(ReaderStatus.isOnline(status) || upd)
                    updateReaderStatus(ReaderStatus.ACTIVE, upd);
            }
        }
    }

    /**
     * Our tag message handlr
     */
    private class OurTagMessageHandler implements
        MantisIIReaderTagMessageListener {
        /**
         * Callback for delivering tag messages - callback MUST NOT block or do
         * significant processing
         * 
         * @param sess -
         *            Reader session
         * @param msg -
         *            tag message - set to null when session is closed
         */
        public void processReaderTagMessage(MantisIIReaderSession sess,
            StringBuilder msg) {
            if(factory == null) /* If deleted, quit */
                return;
            handleTagMessage(sess, msg);
        }
	    /**
	     * Callback for delivering gps messages - callback MUST NOT block or do
	     * significant processing
	     * 
	     * @param sess -
	     *            Reader session
	     * @param msg -
	     *            tag message - set to null when session is closed
	     */
	    public void processReaderGPSMessage(MantisIIReaderSession sess,
	        StringBuilder msg) {
			if(factory == null) /* If deleted, quit */
				return;
			handleGPSMessage(sess, msg);
		}
    }
    /**
     * Constructor
     * 
     * @param f -
     *            our factory
     */
    MantisIIReader(MantisIIReaderFactory f) {
        factory = f;
        status = ReaderStatus.DISABLED;
        enabled = false;
        inited = false;
        conncnt = 0;
        tag_cap_pct = -1;
        merge_channels = false;
        join_channels = false;
        upconn_enabled = false;
        ignore_lowconf_init = false;
        upconn_rdrid = null;
        upconn_pwd = null;
        last_upconn_mac = null;
        part_index = 0;
        part_count = 1;
        readername = "";
        readernamesymbolic = "";
        label = "";
        taggrps = new MantisIITagGroup[0];
        oldvals = new Object[16]; /*
                                     * Work variable for tag attribute
                                     * comparison
                                     */
    }
    /**
     * Init method - must be called once the attributes have been set
     */
    public void init() throws BadParameterException, DuplicateEntityIDException {
        if (inited)
            return;
        if ((sockaddr == null) && (!upconn_enabled)) {
            throw new BadParameterException("Missing hostname/port");
        }
        if (readerid == null) {
            throw new BadParameterException("Missing reader ID");
        }
        /* Initialize for N channels */
        channels = new MantisIIReaderChannel[factory.getChannelCount()];
        for (int i = 0; i < channels.length; i++) {
            char ch = (char) ('A' + i);
            channels[i] = new MantisIIReaderChannel("" + ch, "Channel " + ch,
                this);
            channels[i].init();
        }
        /* Get channel age-out settings, and push into channels */
        updateChannelSettings();
        /* Initialize our session object */
        try {
            session = new MantisIIReaderSession();
            session.setReaderID(readerid);
            session.setSessionFactory(factory.getSessionFactory());
            session.setUpConnection(upconn_enabled);
            if(!upconn_enabled)
                session.setIPAddress(sockaddr);
            session.setReaderSessionHandler(new OurReaderSessionHandler());
            session.addTagListener(new OurTagMessageHandler());
            session.init();
            /* Finally, add ourselves to the directory */
            ReaderEntityDirectory.addEntity(this);
            inited = true;
            updateInitialization();
        } catch (SessionException sx) {
            throw new BadParameterException(sx.getMessage());
        } finally {
            if (!inited) {
                if (session != null) {
                    session.cleanup();
                    session = null;
                }
                ReaderEntityDirectory.removeEntity(this);
            }
        }
        /* If completed init(), call listeners */
        if (inited) {
            factory.callReaderLifecycleForCreate(this);
        }
    }
    /**
     * Cleanup method
     */
    public void cleanup() {

        /* If completed init(), call listeners */
        if (inited && (factory != null)) {
            factory.callReaderLifecycleForDelete(this);
            ReaderEntityDirectory.removeEntity(this);
            inited = false;
        }
        if (session != null) {
            session.cleanup();
            session = null;
        }
		if (our_gps_rule != null) {
			our_gps_rule.cleanup();
			our_gps_rule = null;
		}
		if (our_gps != null) {
			GPSDefaults.removeUpdateListener(this);
			our_gps.cleanup();
			our_gps = null;
		}
        factory = null;
        if (attribs_ro != null) {
            attribs_ro = null;
        }
        if (status_listeners != null) {
            status_listeners.clear();
            status_listeners = null;
        }
        if (channels != null) {
            for (int i = 0; i < channels.length; i++) {
                channels[i].cleanup();
                channels[i] = null;
            }
            channels = null;
        }
        if(upconn_rdrid != null) {
            upconn_rdrs.remove(upconn_rdrid);
        }
    }
    /**
     * Set the attributes for the reader. The keys of the map provided must
     * match the attribute IDs described by the getReaderAttrbiuteIDs() method
     * from the reader's factory.
     * 
     * @param newattribs -
     *            map of values, keyed by attribute ID
     */
    public void setReaderAttributes(Map<String, Object> newattribs)
        throws BadParameterException {
        int i;
        /* Replace existing map with copy of provided map */
        attribs_ro = new HashMap<String, Object>(newattribs);
        
        /* Get and check SSI cutoff values */
        for (i = 0; i < factory.getChannelCount(); i++) {
            int cutoff = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_SSICUTOFF[i],
                MantisIIReaderFactory.DEFAULT_CHANNEL_SSICUTOFF);
            if (cutoff > 0) { /* Migrate old positive values to negative */
                attribs_ro.put(MantisIIReaderFactory.ATTRIB_SSICUTOFF[i], Integer
                    .valueOf(-cutoff));
            }
        }
        /* Get noise threshold */
        noise_thresh = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_NOISETHRESH,
                MantisIIReaderFactory.DEFAULT_NOISETHRESH);

        /* Get events/sec threshold */
        evtpersec_thresh = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_EVTPERSECTHRESH,
                MantisIIReaderFactory.DEFAULT_EVTPERSECTHRESH);

        /* Get merge channels setting */
        merge_channels = getAttribBoolean(
                MantisIIReaderFactory.ATTRIB_MERGECHANNELS,
                MantisIIReaderFactory.DEFAULT_MERGECHANNELS);
        /* Get join channels setting */
        join_channels = getAttribBoolean(
                MantisIIReaderFactory.ATTRIB_JOINCHANNELS,
                MantisIIReaderFactory.DEFAULT_JOINCHANNELS);
        /* Get ignore lowconf initial beacon setting */
        ignore_lowconf_init = getAttribBoolean(
                M250ReaderFactory.IGNORE_LOWCONF_INITIAL,
                M250ReaderFactory.IGNORE_LOWCONF_INITIAL_DEFAULT);

        /* Get reader partition settings */
        int p_idx = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_PARTINDEX,
                MantisIIReaderFactory.DEFAULT_PARTINDEX);
        int p_cnt = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_PARTCOUNT,
                MantisIIReaderFactory.DEFAULT_PARTCOUNT);
        int p_period = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_PARTPERIOD,
                MantisIIReaderFactory.DEFAULT_PARTPERIOD);
        if((p_idx < 0) || (p_idx > p_cnt))
                throw new BadParameterException("partindex must be between 0 and partcount");        
        if((p_cnt < 0) || (p_cnt > 32))
                throw new BadParameterException("partcount must be between 1 and 32");        
        /* Get reader name */
        String rdrname = getAttribString(MantisIIReaderFactory.ATTRIB_READERNAME);
        if((rdrname == null) || (rdrname.length() == 0)) {
            rdrname = readerid; /* Use reader ID for default */             
            /* Update copy */
            attribs_ro.put(MantisIIReaderFactory.ATTRIB_READERNAME, rdrname);
        }

        /* Get upconnect enabled setting */
        boolean up_enab = getAttribBoolean(
                MantisIIReaderFactory.ATTRIB_UPCONNENABLED,
                MantisIIReaderFactory.DEFAULT_UPCONNENABLED);
        boolean up_chg = (up_enab != upconn_enabled);   /* Is changing? */

        /* Get upconnect reader ID */
        String rdrid = getAttribString(MantisIIReaderFactory.ATTRIB_UPCONNRDRID);
        if(up_enab) {
            if((rdrid == null) || (rdrid.length() == 0)) {
                throw new BadParameterException("upconnrdrid required when upconnenabled=true");
            }
            else {
                MantisIIReader oldrdr = upconn_rdrs.get(rdrid); /* Find existing */
                if((oldrdr != null) && (oldrdr != this)) {  /* If defined and not us */
                    throw new BadParameterException("upconnrdrid must be unique");
                }
            }
        }
        /* Get upconnect password */
        String upconnpwd = getAttribString(MantisIIReaderFactory.ATTRIB_UPCONNPWD);

        /* Switch to unmodifiable copy */
        attribs_ro = Collections.unmodifiableMap(attribs_ro);
        
        /* Get channel age-out settings, and push into channels */
        updateChannelSettings();

        /* Check SSL mode */
        String ssl_mode = getAttribString(M250ReaderFactory.SSLMODE);
        if(ssl_mode == null || ssl_mode.length() == 0) {
            ssl_mode = SSLMODE_OFF;
        }
        else if(ssl_mode.equals(SSLMODE_IFAVAIL) || ssl_mode.equals(SSLMODE_REQUIRED) ||
            ssl_mode.equals(SSLMODE_STRICT) || ssl_mode.equals(SSLMODE_OFF)) {
        }
        else {
            throw new BadParameterException("Invalid SSL mode - " + ssl_mode);
        }

        /* Update IP settings, in case they changed */
        String hn = getAttribString(MantisIIReaderFactory.ATTRIB_HOSTNAME);
        int pn = getAttribInteger(MantisIIReaderFactory.ATTRIB_PORTNUM,
            X200ReaderFactory.DEFAULT_PORTNUM);
        try {
            InetSocketAddress newsock = null;

            if(!up_enab) {
				hostname = hn;
				portnum = pn;
                newsock = new InetSocketAddress(hn, pn);
                if ((sockaddr == null) || (!newsock.equals(sockaddr)) ||
                    up_chg) { /*
                                                                         * Changed
                                                                         * connection
                                                                         * info?
                                                                         */
                    sockaddr = newsock;
                    upconn_enabled = up_enab;
                    if (session != null) {
                        session.setUpConnection(upconn_enabled);
                        session.forceDisconnect(); /* Force a disconnect */
                    }
                }
                if(upconn_rdrid != null)
                    upconn_rdrs.remove(upconn_rdrid);   /* Remove old reference */
            }
            /* Else if upconnect and parameter changes, force reconnect */
            else if((rdrid.equals(upconn_rdrid) == false) ||
                ((upconnpwd.equals(upconn_pwd) == false)) ||
                up_chg) {   /* New reader ID or password or upconnect change */
				hostname = null;

                if(upconn_rdrid != null)
                    upconn_rdrs.remove(upconn_rdrid);   /* Remove old reference */
                upconn_rdrid = rdrid;
                upconn_rdrs.put(upconn_rdrid, this);   /* Add new one */
                upconn_pwd = upconnpwd;
                upconn_enabled = up_enab;
                if (session != null) {
                    session.setUpConnection(upconn_enabled);
                    session.forceDisconnect(); /* Force a disconnect */
                }
            }
        } catch (IOException x) {
            throw new BadParameterException("Bad hostname or port");
        }
        part_index = p_idx;
        part_count = p_cnt;
        part_period = p_period;
        if(rotact != null) {
            RangerProcessor.removeDelayedAction(rotact);
            rotact = null;
        }
        /* If we're rotating, add action to do rotate */
        if((part_count > 1) && (part_period > 0)) {
            rotact = new RotatePartAction();
            RangerProcessor.addDelayedAction(rotact, 1000L * (long)part_period);
        }
        readername = rdrname;
        readernamesymbolic = "";
        for(i = 0; i < readername.length(); i++) {
            char c = readername.charAt(i);
            if(Character.isJavaIdentifierPart(c))
                readernamesymbolic += c;
            else
                readernamesymbolic += '_';
        }
		/* If GPS supported and enabled, make sure we have instance */
        String prevsrc = positionsrc;
        positionsrc = getAttribString(MantisIIReaderFactory.ATTRIB_POSITIONSRC);
        if(positionsrc == null) positionsrc = MantisIIReaderFactory.DEFAULT_POSITIONSRC;
        if(!positionsrc.equals(prevsrc)) {  /* Source changed? clean up old */
			if(our_gps_rule != null) {
				our_gps_rule.setRuleEnabled(false);
				our_gps_rule.cleanup();
				our_gps_rule = null;
			}
			if(our_gps != null) {
                GPSData prev = our_gps.getGPSData();
                notifyGPSFixStatusChange(GPSData.Fix.NO_GPS, (prev != null)?prev.getGPSFix():GPSData.Fix.NO_FIX);

				our_gps.cleanup();
				our_gps = null;
				GPSDefaults.removeUpdateListener(this);
			}
        }
		if(positionsrc.equals(MantisIIReaderFactory.ATTRIB_POSITIONSRC_GPS)) {
            if(factory.getGPSSupport() == false) {
                throw new BadParameterException("Reader does not support GPS");
            }
			if(our_gps == null) {
				our_gps = new MantisIIGPS(readerid);
				our_gps.setID(readerid + "__GPS");
				try {
					our_gps.init();
					GPSDefaults.addUpdateListener(this);
				} catch(DuplicateEntityIDException dex) {
					our_gps.cleanup();
					our_gps = null;
				} catch(BadParameterException bpx) {
					our_gps.cleanup();
					our_gps = null;
				}
			}
			/* Handle GPS rule once we're initialized */
		}
        else if(positionsrc.equals(MantisIIReaderFactory.ATTRIB_POSITIONSRC_STATIC)) {
            ListDouble pos = getAttribListDouble(MantisIIReaderFactory.ATTRIB_STATICLATLON);
            if((pos == null) || (pos.size() < 2)) {
                pos = MantisIIReaderFactory.DEFAULT_STATICLATLON;
            }
			if(our_gps == null) {
				our_gps = new MantisIIGPS(readerid, pos);
				our_gps.setID(readerid + "__STATIC");
				try {
					our_gps.init();
					GPSDefaults.addUpdateListener(this);
				} catch(DuplicateEntityIDException dex) {
					our_gps.cleanup();
					our_gps = null;
				} catch(BadParameterException bpx) {
					our_gps.cleanup();
					our_gps = null;
				}
			}
            else {  /* Update data with static position */
                GPSData d = new GPSData(GPSData.Fix.FIX_2D, pos.get(0), pos.get(1), null, null, null, null, null);
                our_gps.setGPSData(d);
                if(our_gps_rule != null)
                    our_gps_rule.handlGPSFixChange(true);
            }
			/* Handle GPS rule once we're initialized */
        }
        sslmode = ssl_mode;
        /* Update reader initialization */
        if (inited)
            updateInitialization();
    }
    /**
     * Update channel-specific settings
     */
    private void updateChannelSettings() {
        /* Get channel age-out settings, and push into channels */
        if (channels != null) {
            for (int i = 0; i < channels.length; i++) {
                channels[i].setMissingSSIAgeOutTime(getAttribInteger(
                    MantisIIReaderFactory.ATTRIB_AGEOUTTIME[i],
                    X200ReaderFactory.DEFAULT_CHANNEL_AGEOUTTIME));
                channels[i].setSSICutoff(getAttribInteger(
                    MantisIIReaderFactory.ATTRIB_SSICUTOFF[i],
                    MantisIIReaderFactory.DEFAULT_CHANNEL_SSICUTOFF));
                int new_cb = getAttribInteger(
                    MantisIIReaderFactory.ATTRIB_CHANNEL_BIAS[i],
                    MantisIIReaderFactory.DEFAULT_CHANNEL_BIAS);
                /* If changed channel bias, force reinit */
                if(new_cb != channels[i].getChannelBias()) {
                    channels[i].setChannelBias(new_cb);
                    if(inited)
                        init_cmds = null;
                }
            }
        }
    }
    /**
     * Get current value of attributes for the reader. The set returned will
     * include values for all attribute IDs indicated by the
     * getReaderAttributeIDs() method of the reader's factory.
     * 
     * @return map of attributes, keyed by attribute ID
     */
    public Map<String, Object> getReaderAttributes() {
        return attribs_ro;
    }
    /**
     * Get the reader's factory
     * 
     * @return factory
     */
    public ReaderFactory getReaderFactory() {
        return factory;
    }
    /**
     * Set reader enabled/disabled
     * 
     * @param enable -
     *            true if enabled, false if disabled
     */
    public void setReaderEnabled(boolean enable) {
        if (!inited)
            return;
        if (enabled == enable)
            return;
        try {
            if (enable) {
                if(upconn_enabled)
                    updateReaderStatus(ReaderStatus.DISCONNECTED);
                else
                    updateReaderStatus(ReaderStatus.CONNECTING);
            } else {
                updateReaderStatus(ReaderStatus.DISCONNECTING);
            }
            enabled = enable;
            conncnt = 0;
            session.setActive(enable);
        } catch (SessionException sx) {
//            updateReaderStatus(ReaderStatus.CONFIGFAILURE);
            updateReaderStatus(ReaderStatus.CONNECTFAILURE);	/* Be more optimistic - let us retry */
        }
        /* If completed init(), call listeners */
        factory.callReaderLifecycleForEnable(this, enable);
    }
    /**
     * Get reader enabled/disabled
     * 
     * @return enable setting
     */
    public boolean getReaderEnabled() {
        return enabled;
    }
    /**
     * Get reader status
     * 
     * @return current status
     */
    public ReaderStatus getReaderStatus() {
        return status;
    }
    /**
     * Update reader status (internal only)
     * 
     * @param s -
     *            new status
     */
    private void updateReaderStatus(ReaderStatus s) {
        updateReaderStatus(s, false);
    }
    /**
     * Kick timeout processing on all active links, in case we don't get back, or they
     * are gone before we do.
     */
    private void kickTagLinkTimeoutProcessing(boolean offline) {
        /* Get the timeout */
        int t_o;
       
        if(offline) {
            t_o = getAttribInteger(
                MantisIIReaderFactory.ATTRIB_OFFLINELINKAGEOUT,
                X200ReaderFactory.DEFAULT_OFFLINELINKAGEOUT);
        }
        else {
            t_o = getAttribInteger(MantisIIReaderFactory.ATTRIB_TAGTIMEOUT,
                MantisIIReaderFactory.DEFAULT_TAGTIMEOUT);
        }
        for (int i = 0; i < channels.length; i++) {
            for (Map.Entry<String,TagLink> tles : 
                channels[i].getTagLinks().entrySet()) {
                ((MantisIITagLink)tles.getValue()).startLinkAgeOut(t_o);
            }
        }
        tag_cap_pct = -1;    /* Reset capacity estimate */
    }
    /**
     * Update reader status (internal only)
     * 
     * @param s -
     *            new status
     * @param upd - force update
     */
    private void updateReaderStatus(ReaderStatus s, boolean upd) {
		/* Clean up firmware library - never stays active beyond state change */
		if(upg_fw != null) {	/* Clean up fw lib, if active */
			FirmwareLibrary.closeFirmware(upg_fw);
			upg_fw = null; 
		}
        if(upconn_enabled) {    /* Nothing special for upconnects */
        }
        /* If in-retry states, hide em */
        else if(s == ReaderStatus.CONNECTING) {
            conncnt++;
            if(conncnt == 2) {  /* On first reconnect, suppress */
                RangerServer.info("Reader " + readerid + ": attempting reconnect");
                return;
            }
        }
        /* Suppress first reconnect */
        else if((s == ReaderStatus.INITIALIZING) && (conncnt == 2)) {
            /* If we're initializing, kick online ageout */
            kickTagLinkTimeoutProcessing(false);

            return;
        }
        else if((s == ReaderStatus.DISCONNECTED) && (conncnt == 1) && enabled) {
            if(ReaderStatus.isOnline(status)) { /* If was online, kick timeout */
                kickTagLinkTimeoutProcessing(true);
            }
            return;
        }
        else if(ReaderStatus.isOnline(s)) {
            conncnt = 1;    /* On success, reset to first try */
        }
        /* If we're initializing, kick online ageout */
        if((s == ReaderStatus.INITIALIZING) || (s == ReaderStatus.FIRMWAREUPGRADING)) {
            kickTagLinkTimeoutProcessing(false);    /* Any existing links need to handle tag-timeout */
        }
        if ((s != status) || upd) {
            ReaderStatus old_s = status;
            status = s;
            /* Call our listeners */
            for (ReaderStatusListener l : status_listeners) {
                l.readerStatusChanged(this, status, old_s);
            }
            /*
             * If not active anymore, kick timeout processing on all active
             * links
             */
            if (ReaderStatus.isOnline(old_s) && (!ReaderStatus.isOnline(status))) {
                kickTagLinkTimeoutProcessing(true);
            }
        }
        if(session != null)
            session.setToReady(ReaderStatus.isOnline(status));
    }
    /**
     * Get reader status message - more specific than reader status
     * 
     * @return current status message
     */
    public String getReaderStatusMsg() {
        return getReaderStatus().toString();
    }
    /**
     * Add reader status listener - multiple listeners are supported
     * 
     * @param listener -
     *            status listener
     */
    public void addReaderStatusListener(ReaderStatusListener listener) {
        synchronized (status_listeners) {
            status_listeners.add(listener);
        }
    }
    /**
     * Remove reader status listener
     * 
     * @param listener -
     *            status listener
     */
    public void removeReaderStatusListener(ReaderStatusListener listener) {
        synchronized (status_listeners) {
            status_listeners.remove(listener);
        }
    }
    /**
     * Get list of TagGroups configured for reader to observe
     * 
     * @return list of TagGroups
     */
    public TagGroup[] getReaderTagGroups() {
        return taggrps;
    }
    /**
     * Set list of TagGroups for reader to observe
     * 
     * @param grps -
     *            list of TagGroups
     */
    public void setReaderTagGroups(TagGroup[] grps)
        throws BadParameterException {
        int i, j;
        int cnt = 0;
        /* Check for any pseudo-tag groups - strip from list */
        for (i = 0; i < grps.length; i++) {
            if(grps[i] == null) {
                throw new BadParameterException("Invalid tag group <null>");
            }
            else if (!(grps[i] instanceof MantisIITagGroup)) {
                throw new BadParameterException("Invalid tag group "
                    + grps[i].getID());
            }
            else if(((MantisIITagGroup)grps[i]).getMantisIITagType() 
                    instanceof MantisIIReaderPseudoTagType) {
                grps[i] = null;
            }
            else
                cnt++;
        }
        HashSet<String> gcodes = new HashSet<String>();
        if (cnt > factory.getGroupCodeMax())
            throw new BadParameterException("Too many group IDs");
        /* Scan through list - make sure they're all MantisIITagTypes */
        MantisIITagGroup[] newgrps = new MantisIITagGroup[cnt];
        for (i = 0, j = 0; i < grps.length; i++) {
            if(grps[i] == null)
                continue;
            newgrps[j] = (MantisIITagGroup) grps[i];
            j++;
            String gcode = (String) grps[i].getTagGroupAttributes().get(
                MantisIITagGroup.GROUPCODE_GROUPATTRIB);
            if ((gcode == null) || (gcode.length() != 6)) {
                throw new BadParameterException("setReaderTagGroups(): Invalid group code - " + gcode);
            }
            else if(gcodes.contains(gcode)) {
                throw new BadParameterException("Duplicate group code - " + gcode);
            }
            gcodes.add(gcode);
        }
        /* Replace existing settings */
        taggrps = newgrps;

        /* Update reader initialization */
        updateInitialization();
    }
    /**
     * Update reader initialization, and reinitialize if needed
     */
    private void updateInitialization() {
        /* Update initialization commands - check for change */
        if (generateInitializationCmds()) {
            /* If we're active OR had a config failure, try to init */
            if (ReaderStatus.isOnline(status) || (status == ReaderStatus.CONFIGFAILURE)) {
				if(upgrade_requested)
	                updateReaderStatus(ReaderStatus.FIRMWAREUPGRADING);
				else
	                updateReaderStatus(ReaderStatus.INITIALIZING);
                RangerProcessor.addImmediateAction(new InitializeReaderAction());
            }
        }
        else {  /* Else, no change - need to be sure active_taggrps matches taggrps */
            active_taggrps = taggrps;
        }
    }
    /**
     * Get maximum number of TagGroup IDs supported by reader. This limits the
     * number of IDs that can be supplied via setReaderTagGroupIDs().
     * 
     * @return max number of TagGroup IDs
     */
    public int getReaderMaxTagGroupCount() {
        return factory.getGroupCodeMax();
    }
    /**
     * Set reader ID (unique)
     * 
     * @param id -
     *            reader ID
     */
    public void setID(String id) {
        readerid = id;
        if(label.length() == 0) {
            label = id;
        }
    }
    /**
     * Get reader ID (unique)
     * 
     * @return - reader ID
     */
    public String getID() {
        return readerid;
    }
    /**
     * Set reader label
     * @param lab - label
     */
    public void setLabel(String lab) {
        label = lab;
    }
    /**
     * Get reader label
     */
    public String getLabel() {
        return label;
    }
    /**
     * Get channels associated with the reader (channels are distinctive radios
     * or scanners - detected tags are associated with one or more channels,
     * versus readers).
     * 
     * @return channels
     */
    public ReaderChannel[] getChannels() {
        return channels;
    }
    /**
     * Get reader session - for raw commands
     * 
     * @return reader session
     */
    public MantisIIReaderSession getReaderSession() {
        return session;
    }

	private static String splitFromVer(String v, boolean fam) {
        String vernums = "01234567890.";
		if(v == null) v = "";
        for (int i = 0; i < v.length(); i++) {
            if (vernums.indexOf(v.charAt(i)) < 0) {
				if(fam)
					return v.substring(i);
				else
					return v.substring(0, i);
            }
        }
		return "";
	}
    /**
     * Generate the initialization sequence for the reader, based on parameters
     * 
     * @return true if changed existing, false if same
     */
    private boolean generateInitializationCmds() {
        boolean change = false;
        ArrayList<String> cmds = new ArrayList<String>(); /* Start empty list */

        int i;
        String vn = fw_ver;
        double ver = 2.41;                 /* Assume current firmware */
		String fam;

		fam = splitFromVer(fw_ver, true);
		if(fam.equals("") == false)
			fw_fam = fam;
		vn = splitFromVer(fw_ver, false);
        try {
            ver = Double.parseDouble(vn); /* Get firmware version as number */
        } catch (NumberFormatException nfx) {
        }
        /* If SSL mode not disabled, add command */
        if(!sslmode.equals(SSLMODE_OFF))
            cmds.add("STARTTLS," + sslmode);
        /* Check for login command - might be needed */
        String uid = getAttribString(M250ReaderFactory.ATTRIB_USERID);
        String pwd = getAttribString(M250ReaderFactory.ATTRIB_PASSWORD);
        /* If either defined, need to login first */
        if (((uid != null) && (uid.length() > 0))
            || ((pwd != null) && (pwd.length() > 0))) {
            cmds.add(":login," + ((uid == null) ? "" : uid) + ","
                + ((pwd == null) ? "" : pwd));
        }
		/* If upgrade requested, can't do more until after the version info commands */
		if(upgrade_requested) {
	        cmds.add("M,0"); /* Quiesce the reader - if securite fail, we're done anyway */
	        cmds.add("V");	/* Do this first, since its a better answer for FW family */
	        cmds.add(":ver");	/* Do this second, since upgrade-botched M200 will fail on "V" - upgrade starts here			 */			
			cmds.add("M");	/* Bogus command - upgrade will replace this as it loops through execution */
		}
		else {
	        /* Start with version check */
	        cmds.add("V");	/* Do this first, since its a better answer for FW family */
	        cmds.add(":ver");	/* Do this second, since upgrade-botched M200 will fail on "V" */			
	        cmds.add("M,0"); 	/* Quiesce the reader - do after first V, in case of security */
	        cmds.add("V");		/* In case badly timed tag message before M,0 */
	        cmds.add(":ver");	/* Do this second, since upgrade-botched M200 will fail on "V" */
            if(ver >= 2.45) {
                cmds.add("!version"); /* Get version info */
                if(factory.getInterfaceStatusSupport()) {
                	cmds.add("!interfacestatus");
                }
                cmds.add("!rdrstatus"); /* Get reader status info */
            }
	        /* Set serial driver */
	        String sdrv = getAttribString(M250ReaderFactory.ATTRIB_SERPORT_DRVR);
	        int sbaud = getAttribInteger(MantisIIReaderFactory.ATTRIB_SERPORT_BAUD,
	            MantisIIReaderFactory.DEFAULT_SERPORT_BAUD);
	        if((sdrv != null) && (sdrv.length() > 0)) {
	            cmds.add(":ser," + sdrv + "," + sbaud);
	        }
	        cmds.add("GR"); /* Reset the group table */
	        boolean do_triggers = getAttribBoolean(MantisIIReaderFactory.ATTRIB_REPORTTRIGGERS,
	            MantisIIReaderFactory.DEFAULT_REPORTTRIGGERS);

			if(ver >= 2.44) {	/* payload verify supported? */
				verify_supported = true;
			}
			else {
				verify_supported = false;
			}

			boolean verbose = RangerServer.isTagVerboseModeActive();
	        /* Loop through the tag groups */
	        for (i = 0; i < taggrps.length; i++) {
	            MantisIITagType tt = (MantisIITagType) taggrps[i].getTagType();
	            if ((tt.getTreatmentCode() > 4) && (ver < 2.50)) {
	            	continue;
	            }
	            String gcode = (String) taggrps[i].getTagGroupAttributes().get(
	                MantisIITagGroup.GROUPCODE_GROUPATTRIB);
	            TagGroup.PayloadVerify verify = taggrps[i].getEnhPayloadVerify();
	
	            int pfimask = getAttribInteger(MantisIIReaderFactory.ATTRIB_PAYLOADFAULTINMASK,
	                MantisIIReaderFactory.DEFAULT_PAYLOADFAULTINMASK);
	            int pfival = getAttribInteger(MantisIIReaderFactory.ATTRIB_PAYLOADFAULTINVALUE,
	                MantisIIReaderFactory.DEFAULT_PAYLOADFAULTINVALUE);
	            int pchgignmask = getAttribInteger(MantisIIReaderFactory.ATTRIB_PAYLOADCHGIGNOREMASK,
	                MantisIIReaderFactory.DEFAULT_PAYLOADCHGIGNOREMASK);
	            /*
	             * Build G message - add group code and treatment, include payload
	             * and group ID
	             */
	            String gcmd = "G," + gcode + "," + tt.getTreatmentCode() + ",0,1";

				int pval = 1;
				if((ver >= 2.44) && tt.verboseBeaconsRequired()) {	/* If all beacons needed */
					pval |= 4;	/* Set verbose flag */
					if(do_triggers)	/* If optima too */
						pval |= 2;	/* Set optima flag */
				}
				else {
					if((ver >= 2.37) && (do_triggers))
	                	pval |= 2;
					if(tt.verboseBeaconsRequired())	/* If all required, and firmware too old, global verbose */
						verbose = true;
				}
                /* If definitely needed, but not supported, or if multireader verify mode, go verbose */
                switch(verify) {
                    case NO:
                        break;
                    case IFSUPPORTED:
                        if(verify_supported)
                            pval |= 8;
                        break;
                    case YES:
                        if(verify_supported)
                            pval |= 8;
                        else
                            verbose = true;
                        break;
                    default:
                        if(verify_supported)    /* If verify and per-group verbose supported */
                            pval |= 4;          /* Set this group to be verbose */
                        else
                            verbose = true;
                        break;
                }                    
				gcmd += "," + pval;
	            if((ver >= 2.43) && ((pfimask != 0) || (pchgignmask != 0))) {
	                String v = Integer.toString(pfimask);
	                v = "000".substring(v.length()) + v;
	                gcmd += "," + v;
	                v = Integer.toString(pfival);
	                v = "000".substring(v.length()) + v;
	                gcmd += "," + v;
	                v = Integer.toString(pchgignmask);
	                v = "000".substring(v.length()) + v;
	                gcmd += "," + v;
	            }
	            cmds.add(gcmd);
                if((ver >= 2.46) && (taggrps[i].getGroupTimeout() != TagGroup.GROUP_TIMEOUT_USEREADER)) {
                    cmds.add(":grouptimeout," + gcode + "," + taggrps[i].getGroupTimeout());
                }
	        }
	        /* If joining */
	        if(join_channels) {
	            /* If new enough, use mode 5 */
	            if(ver >= 2.41)
	                cmds.add("S,5");
	            else    /* Else, use mode 1 */
	                cmds.add("S,1");
	        }
	        else if(merge_channels) {   /* If merging, use S1 */
	            cmds.add("S,1");
	        }
	        else {
	            /* Set to SSI mode 2 - we want A and B channel */
	            cmds.add("S,2");
	        }
	        /*
	         * Set to "exception" mode, with loss reporting, and add timeout and
	         * age-in. Also, ignore low-confidence payload changes.
			 * If verbose needed, just do loss reporting
	         */
	        if (ver == 2.34) {
	            cmds.add((verbose?"O,2,":"O,3,") + 
	                + getAttribInteger(MantisIIReaderFactory.ATTRIB_TAGTIMEOUT,
	                    MantisIIReaderFactory.DEFAULT_TAGTIMEOUT)
	                + ","
	                + getAttribInteger(MantisIIReaderFactory.ATTRIB_AGEINCOUNT,
	                    MantisIIReaderFactory.DEFAULT_AGEINCOUNT));
	        } else if (ver >= 2.35) {
                int mode = (verbose?2:11);
                if ((ver >= 2.47) && ignore_lowconf_init) {
                    mode |= 16; // Set ignore lowconfidence flag
                }
	            cmds.add("O," + mode + "," +  
	                + getAttribInteger(MantisIIReaderFactory.ATTRIB_TAGTIMEOUT,
	                    MantisIIReaderFactory.DEFAULT_TAGTIMEOUT)
	                + ","
	                + getAttribInteger(MantisIIReaderFactory.ATTRIB_AGEINCOUNT,
	                    MantisIIReaderFactory.DEFAULT_AGEINCOUNT)
	                + ","
	                + getAttribInteger(MantisIIReaderFactory.ATTRIB_SSIDELTATHRESH,
	                    MantisIIReaderFactory.DEFAULT_SSIDELTATHRESH));
	        }
	        /* Full range */
	        cmds.add("R,8");
	        /* Build Z command (for SSI ranging */
	        StringBuilder sb = new StringBuilder();
	        sb.append("Z");
	        for (i = 0; i < channels.length; i++) {
	            int limit = getAttribInteger(
	                MantisIIReaderFactory.ATTRIB_SSICUTOFF[i],
	                MantisIIReaderFactory.DEFAULT_CHANNEL_SSICUTOFF);
	            if (limit >= -40) { /* If disabled */
	                limit = 0;
	            }
    	        sb.append(",").append(-limit);
    	    }
    	    cmds.add(sb.toString());
    	    /* Set 3 minute watchdog */
    	    cmds.add("N,3");
    	    /* No time messages */
		    cmds.add("U,0");
	        /* No diff thresholds */
            if (channels.length > 1) {
        	    if (ver == 2.34)
        	        cmds.add("ZD,255,255,255,255");
	            else if (ver >= 2.35)
        	        cmds.add("ZD,255,255,255,255,255,255");
            }
    	    /* If partitioning is active, send command */
    	    if((part_count > 1) || (part_index >= part_count)) {
    	        cmds.add("OP," + part_index + "," + part_count);
    	    } else if(ver >= 2.40) {    /* Else, if supported, make sure its off */
    		        cmds.add("OP,0,1");
    	    }
			/* If GPS supported, enabled, and version is high enough, generate commands */
			if(factory.getGPSSupport() && positionsrc.equals(MantisIIReaderFactory.ATTRIB_POSITIONSRC_GPS) &&
				(ver >= 2.45)) {
				int bitmap = GPS_INFO_FIX | GPS_INFO_LAT_LON;	/* Always fix and lat/lon */
				String dataset = getAttribString(M250ReaderFactory.GPS_DATASET);
				if(dataset == null) dataset = M250ReaderFactory.GPS_DATASET_DEFAULT;
				if(dataset.equals(M250ReaderFactory.GPS_DATASET_USE_GLOBAL))
					dataset = GPSDefaults.getDefault().getGPSDataSet();
				/* Check for '-spd' */
				if(dataset.contains("-spd"))
					bitmap |= GPS_INFO_SPD_COURSE;
				/* Check for '-alt' */
				if(dataset.contains("-alt"))
					bitmap |= GPS_INFO_ALT;
				/* Check for "-epe" */
				if(dataset.contains("-epe"))
					bitmap |= GPS_INFO_EPE;
				/* Check for period */
				int minper = getAttribInteger(M250ReaderFactory.GPS_MIN_PERIOD,
					M250ReaderFactory.GPS_MIN_PERIOD_DEFAULT);
				if(minper == M250ReaderFactory.GPS_MIN_PERIOD_USE_GLOBAL)
					minper = GPSDefaults.getDefault().getGPSMinPeriod();
				if(minper < 0) minper = 0;
                min_gps_period = (minper * 1000);   /* Save min period, in msec */
                if(min_gps_period < 1000) min_gps_period = 1000;
				/* Get min SSI */
				int minssi = getAttribInteger(MantisIIReaderFactory.ATTRIB_GPS_MIN_SSI,
					MantisIIReaderFactory.DEFAULT_GPS_MIN_SSI);
				if(minssi > 0) minssi = 0;
				/* Get min horizontal */
				double minh = getAttribDouble(M250ReaderFactory.GPS_MIN_HORIZ,
					M250ReaderFactory.GPS_MIN_HORIZ_DEFAULT);
				if(minh <= M250ReaderFactory.GPS_MIN_HORIZ_USE_GLOBAL)
					minh = GPSDefaults.getDefault().getGPSMinHoriz();
				if(minh < 0.0) minh = 0.0;
				/* Get min vertical */
				double minv = getAttribDouble(M250ReaderFactory.GPS_MIN_VERT,
					M250ReaderFactory.GPS_MIN_VERT_DEFAULT);
				if(minv <= M250ReaderFactory.GPS_MIN_VERT_USE_GLOBAL)
					minv = GPSDefaults.getDefault().getGPSMinVert();
				if(minv < 0.0) minv = 0.0;
				/* Add command */
				cmds.add(":gps,2," + bitmap + "," + minper + "," + (-minssi) +
					"," + minh + "," + minv);
            }
            /* If not disabled, add/update rule here */
            if(!positionsrc.equals(MantisIIReaderFactory.ATTRIB_POSITIONSRC_NONE)) {
				/* Build or update GPS rule here, since we're going active */
		        int mingpsssi = getAttribInteger(MantisIIReaderFactory.ATTRIB_GPS_MIN_SSI,
		            MantisIIReaderFactory.DEFAULT_GPS_MIN_SSI);
				if((mingpsssi == MantisIIReaderFactory.GPS_MIN_SSI_NOMIN) || 
					(mingpsssi < -120))
					mingpsssi = -120;
                min_gps_ssi = mingpsssi;
				if(our_gps != null) {
					boolean isnew = false;
					if(our_gps_rule == null) {	/* Make GPS rule too */
						our_gps_rule = new SimpleSSIGPSRule();	/* Create instance */
						our_gps_rule.setID(our_gps.getID() + "Rule");
						isnew = true;
					}
					/* Update rule's attributes, even if it exists */
					Map<String,Object> attr = new HashMap<String,Object>();
					HashSet<ReaderChannel> rcs = new HashSet<ReaderChannel>();
					for(ReaderChannel rc : channels)
						rcs.add(rc);
					attr.put(SimpleSSILocationRuleFactory.ATTRIB_SSIMINIMUM,
						Integer.valueOf(mingpsssi));
					attr.put(SimpleSSILocationRuleFactory.ATTRIB_SSIHIGHCONF,
						Integer.valueOf(mingpsssi+20));
					attr.put(IRLocatorLocationRuleFactory.ATTRIB_CHANNELLIST, rcs);
					try {
						our_gps_rule.setLocationRuleAttributes(attr);
						our_gps_rule.setRuleTargetGPS(our_gps);
						if(isnew) {
							our_gps_rule.init();
   						}
    					our_gps_rule.setRuleEnabled(true);
					} catch (BadParameterException bpx) {
						RangerServer.getLogger().error(bpx);
						our_gps_rule.cleanup();
						our_gps_rule = null;
			        } catch (DuplicateEntityIDException deix) {
						RangerServer.getLogger().error(deix);
						our_gps_rule.cleanup();
						our_gps_rule = null;
					}
				}
			}
	        /* Finally, set to our operating mode */
    	    cmds.add("M," + factory.getModeNumber());
		}
	    /* Now, compare with existing - see if we're stale */
        if ((init_cmds == null) || (!init_cmds.equals(cmds))) {
            /* If different, replace with new set and set change flag */
            init_cmds = cmds;
            change = true;
        }
        return change;
    }
    /**
     * Get attribute as string
     * 
     * @param id -
     *            attribute ID
     * @return string or null if n/a
     */
    private String getAttribString(String id) {
        Object v = attribs_ro.get(id); /* Get value */
        if (v == null)
            return null;
        if (v instanceof String)
            return (String) v;
        else
            return v.toString();
    }
    /**
     * Get attribute as integer, with default if invalid
     * 
     * @param id -
     *            attribute ID
     * @param def -
     *            default for bad values
     * @return value
     */
    private int getAttribInteger(String id, int def) {
        Object v = attribs_ro.get(id); /* Get value */
        if ((v != null) && (v instanceof Integer))
            return ((Integer) v).intValue();
        else
            return def;
    }
    /**
     * Get attribute as double, with default if invalid
     * 
     * @param id -
     *            attribute ID
     * @param def -
     *            default for bad values
     * @return value
     */
    private double getAttribDouble(String id, double def) {
        Object v = attribs_ro.get(id); /* Get value */
        if ((v != null) && (v instanceof Double))
            return ((Double) v).doubleValue();
        else
            return def;
    }
    /**
     * Get attribute as boolean
     * 
     * @param id -
     *            attribute ID
     * @return 
     */
    private boolean getAttribBoolean(String id, boolean def) {
        Object v = attribs_ro.get(id); /* Get value */
        if (v == null)
            return def;
        if (v instanceof Boolean)
            return ((Boolean) v).booleanValue();
        else
            return def;
    }
    /**
     * Get attribute as ListDouble
     * 
     * @param id -
     *            attribute ID
     * @return string or null if n/a
     */
    private ListDouble getAttribListDouble(String id) {
        Object v = attribs_ro.get(id); /* Get value */
        if (v == null)
            return null;
        if (v instanceof ListDouble)
            return (ListDouble) v;
        else
            return null;
    }

    private static final String[] optima_cmd = {
        "beacon",   /* Beacon now = 000 */
        "wake",     /* Wakeup = 001 */
        "sleep",    /* Sleep = 010 */
        "suspend",  /* Suspend = 011 */
        "cmd100",
        "cmd101",
        "cmd110",
        "cmd111" };
    //private static final int OPTIMA_CC_BEACON = 0;   /* Beacon command */
    //private static final int OPTIMA_CC_WAKE = 1;   /* Beacon command */
    private static final int OPTIMA_CC_SLEEP = 2;   /* Sleep command */
    private static final int OPTIMA_CC_SUSPEND = 3;   /* Suspend command */

    private static final int STATE_INIT = 0;
    private static final int STATE_COMMA = 1;
    private static final int STATE_TAGID = 2;
    private static final int STATE_COMMA2 = 3;
    private static final int STATE_GRPCODE = 4;
    private static final int STATE_PAYLOAD = 5;
    private static final int STATE_SSIA = 6;
    private static final int STATE_SSIB = 7;
    private static final int STATE_SKIPTOCOMMA = 8;
    private static final int STATE_SVCID = 9; 
    private static final int STATE_SVCMSG = 10;
    private static final int STATE_OPTIMA_PAYLOAD = 11;
    private static final int STATE_SSIS = 12;
    private static final int STATE_ERROR = -1;
    /**
     * Handle raw tag messages
     * 
     * @param sess -
     *            session sending messages
     * @param msg -
     *            message buffer
     */
    private static StringBuilder grpcode = new StringBuilder();
    private void handleTagMessage(MantisIIReaderSession sess, StringBuilder msg) {
        int i, len = msg.length();
        int tagid = -1;
		int tagidcnt = 0;
        int payload = -1;
        int payloadcnt = 0;
        int optima_payload = -1;
        int ssi[] = {TagLink.SSI_VALUE_NA,  /* A */
            TagLink.SSI_VALUE_NA /* B */ };
        int ssis = TagLink.SSI_VALUE_NA; /* S */
        boolean is_lost = false;
        boolean is_sleep = false;
        int state = STATE_INIT;
        String svcid = null;
        String svcmsg = null;
        boolean has_gpsinfo = false;
        /* Ignore tag messages when we're not valid state */
        if(!ReaderStatus.isOnline(status)) {
            return;
        }
        /* If active groups not matching configured ones, quit */
        if(active_taggrps != taggrps)
            return;

        RangerServer.logTagEvent(readerid, msg);
        grpcode.setLength(0);
        /* Loop through message */
        for (i = 0; (state != STATE_ERROR) && (i < len); i++) {
            char c = msg.charAt(i); /* Get next letter */
            switch (state) {
                case STATE_INIT: /* Initial state */
                    if (c == 'L') { /* Lost tag message */
                        is_lost = true;
                        state = STATE_COMMA;
                        tagid = 0;
						tagidcnt = 0;
                    } else if ((c == 'H') || (c == 'M')) { /* Message */
                        is_lost = false;
                        state = STATE_COMMA;
                        tagid = 0;
						tagidcnt = 0;
                    } else if (c == '=') {
                        state = STATE_COMMA;
                        svcid = "";
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_COMMA: /* First comma - before tagid */
                    if (c == ',') {
                        if(svcid == null)
                            state = STATE_TAGID;
                        else
                            state = STATE_SVCID;
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_TAGID:
                    if ((c >= '0') && (c <= '9')) { /* Digit */
                        /* Accumulate tag ID */
                        tagid = (tagid * 10) + (int) (c - '0');
						tagidcnt++;
                    } else if ((c == ',') && (tagidcnt == 8) && (tagid != 0)) { /* Next comma found, and valid tagid format */
                        state = STATE_COMMA2;
                    } else { /* Else, bad message */
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_SVCID:
                    if ((c >= 'a') && (c <= 'z')) { /* letter */
                        svcid += c;
                    } else if (c == ',') { /* Next comma found */
                        state = STATE_SVCMSG;
                        svcmsg = "";
                    } else { /* Else, bad message */
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_SVCMSG:
                    svcmsg += c;
                    break;
                case STATE_COMMA2: /* Comma in ID<value> list */
                    if (c == 'G') { /* Group code? */
                        state = STATE_GRPCODE;
                    } else if (c == 'P') { /* Payload (confident) */
                        state = STATE_PAYLOAD;
                        payload = payloadcnt = 0;
                    } else if (c == '#') { /* Optima Payload (confident) */
                        state = STATE_OPTIMA_PAYLOAD;
                        optima_payload = 0;
                    } else if (c == 'A') { /* SSI A */
                        state = STATE_SSIA;
                        ssi[0] = 0;
                    } else if (c == 'B') { /* SSI B */
                        state = STATE_SSIB;
                        ssi[1] = 0;
                    } else if (c == 'S') { /* SSI S */
                        state = STATE_SSIS;
                        ssi[1] = 0;
                    } else if(c == 't') { /* time marker for GPS info on tag lost? */
                        state = STATE_SKIPTOCOMMA;  /* Skip it - will parse with other gps info */
                        has_gpsinfo = true;
                    } else { /* Other we don't care */
                        state = STATE_SKIPTOCOMMA;
                    }
                    break;
                case STATE_SKIPTOCOMMA: /* Skip to next comma */
                    if (c == ',') { /* Found next comma */
                        state = STATE_COMMA2; /* Check it out */
                    } else { /* Else, skip */

                    }
                    break;
                case STATE_GRPCODE: /* Process group index */
                    if ((c >= 'A') && (c <= 'Z')) {
                        grpcode.append(c);
                    } else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_PAYLOAD: /* Process payload */
                    if ((c >= '0') && (c <= '9')) {
                        payload = (payload * 16) + (int) (c - '0');
                        payloadcnt++;
                    }
                    else if ((c >= 'A') && (c <= 'F')) {
                        payload = (payload * 16) + (int) (c - 'A' + 10);
                        payloadcnt++;
                    }
                    else if ((c >= 'a') && (c <= 'f')) {
                        payload = (payload * 16) + (int) (c - 'a' + 10);
                        payloadcnt++;
                    }
                    else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    }
                    else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_OPTIMA_PAYLOAD: /* Process optima payload */
                    if ((c >= '0') && (c <= '7')) {
                        optima_payload = (optima_payload * 8) + (int) (c - '0');
                    } else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_SSIA: /* Process SSI A */
                    if ((c >= '0') && (c <= '9')) {
                        ssi[0] = (ssi[0] * 10) + (int) (c - '0');
                    } else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_SSIB: /* Process SSI B */
                    if ((c >= '0') && (c <= '9')) {
                        ssi[1] = (ssi[1] * 10) + (int) (c - '0');
                    } else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
                case STATE_SSIS: /* Process SSI S */
                    if ((c >= '0') && (c <= '9')) {
                        ssis = (ssis * 10) + (int) (c - '0');
                    } else if (c == ',') { /* Comma? */
                        state = STATE_COMMA2; /* On to next */
                    } else {
                        state = STATE_ERROR;
                    }
                    break;
            }
        }
        /* If payload was less than 4 digits, adjust for octal values */
        if ((payloadcnt > 0) && (payloadcnt < 4)) {
        	payload = ((payload & 0x700) >> 2) | ((payload & 0x070) >> 1) | (payload & 0x007);
        }
        /* If we had problem, log bad message */
        if (state == STATE_ERROR) {
            RangerServer.error("Bad tag message: " + msg.toString());
            return;
        }
        /* If service message, handle it */
        if(svcid != null) {
            handleServiceMessage(svcid, svcmsg);
            return;
        }
        /* If SSIS, use for both channels */
        if(ssis != TagLink.SSI_VALUE_NA) {
            ssi[0] = ssis;
            ssi[1] = ssis;
        }
        /* Now, check to see if SSI values are below cutoff */
        int ssi_good_cnt = 0;
        for (i = 0; i < channels.length; i++) {
            if (ssi[i] != TagLink.SSI_VALUE_NA) {
                ssi[i] = -ssi[i]; /* Switch to dBm */
                ssi[i] += channels[i].getChannelBias(); /* Add bias */
                /* Get and check for cutoff */
                int cutoff = channels[i].getSSICutoff();
                if ((cutoff != 0) && (ssi[i] < cutoff)) {
                    /* Force to N/A if below cutoff */
                    ssi[i] = TagLink.SSI_VALUE_NA;
                } else
                    ssi_good_cnt++;
            }
        }

        /* Find tag group - make sure index is sane */
        MantisIITagGroup tg = null;
        String gcode = grpcode.toString();
        for(i = 0; (tg == null) && (i < active_taggrps.length); i++) {
            if(active_taggrps[i].getGroupCode().equals(gcode))
                tg = active_taggrps[i];
        }
        if(tg == null) {
            tg = MantisIITagGroup.findPseudoTag(gcode);
            if(tg != null) {    /* Pseudotag? */
                tagid = -1; /* Mark with negative tag ID */
            }
        }
        if (tg == null) {
            if((active_taggrps != null) && (active_taggrps.length > 0))
                RangerServer.error("handleTagMessage: Invalid group code: " + grpcode + ", msg='" + msg + "'");
            return;
        }
        MantisIITagLink tl;
        /* Now, try to find the tag, or make it if needed (don't if lost) */
        MantisIITag tag = MantisIITag.findOrCreateTag(tg, tagid, is_lost
            || (ssi_good_cnt == 0), readernamesymbolic);
        if (tag == null) { /* Its a bogus tag loss message - just quit */
            return;
        }
        /* Get timestamp for message */
        long now = System.currentTimeMillis();
        /* Log raw beacon, if anyone is interested */
        if ((!is_lost) && (optima_payload < 0)) {
        	AbstractTagType.reportRawTagEvent(tag, now, payload, this, ssi);
        }
        
        /* If we have GPS */
        if(our_gps != null) {
            /* If tag lost */
            if(is_lost) {
                /* If it has GPS info, update tag GPS data */
                if(has_gpsinfo) {
                    long ts = now;
                    GPSData gpsd = parseGPSData(msg.toString());
                    if(gpsd.getTimestamp() != 0) {
                        ts = gpsd.getTimestamp();   /* Use timestamp derived from message, if any */
                    }
                    our_gps.setTagGPSData(tag.getTagGUID(), gpsd, ts);
                }
            }
            /* If strong enough to match AND currently good fix, update tag GPS data with reader's current data */
            else if(((ssi[0] >= min_gps_ssi) || (ssi[1] >= min_gps_ssi)) && (our_gps.getGPSData() != null) && our_gps.getGPSData().hasFix()) {
                our_gps.setTagGPSData(tag.getTagGUID(), our_gps.getGPSData(), now);
            }
        }
        /* If we lost the tag, need to pull our TagLinks from the tag */
        if (is_lost) {
            for (i = 0; i < channels.length; i++) {
                tag.insertTagLink(channels[i], null); /* Drop all of em */
            }
        } else { /* Else, we got updates for the tag, so process em */
            boolean is_new = false;
            /* If no links yet, new tag */
            if (tag.getTagLinkCount() == 0) {
                is_new = true;
            }
            /* Now pass in the payload for parsing and updating attributes */
            if (payload >= 0) {
                if (!is_new) { /* Only need old values on updated tags */
                    int cnt = tg.getTagType().getTagAttributes().length;
                    /* Get values before update */
                    if (cnt > oldvals.length) { /* Make sure enough room */
                        oldvals = new Object[cnt];
                    }
                    tag.readTagAttributes(oldvals, 0, cnt); /* Read em */
                    /* If different, report change */
                    if (tag.updateUsingPayload(payload, now, this)) {
                        tg.reportTagStatusAttributeChange(tag, oldvals);
                    }
                } else { /* Else, for new tag */
                    tag.updateUsingPayload(payload, now, this); /* just process */
                }
            }
            if (is_new) { /* If new, report it (before links added) */
                String cause = null;
                if(optima_payload >= 0) {
                    cause = optima_cmd[(optima_payload/64)%8];
                }            
                tg.reportTagLifecycleCreate(tag, cause);
            }
            /* If optima message, need to report it */
            if(optima_payload >= 0) {
                int cc = (optima_payload/64) % 8;
                int wid = optima_payload%64;
                tg.reportTagOptimaMsg(tag, optima_cmd[cc], wid);
                /* If sleep, or if no valid payloads yet, do 5 second link timeout */
                if((cc == OPTIMA_CC_SLEEP) || (cc == OPTIMA_CC_SUSPEND) ||
                    (!tag.hadValidPayloads())) {
                    is_sleep = true;
                }
            }
            /* Next, handle our channels - add or remove as needed */
            for (i = 0; i < channels.length; i++) {
                boolean change = false;
                /* Find the link, if it exists */
                tl = (MantisIITagLink) tag.findTagLink(channels[i]);
                /* If SSI is NA */
                if (ssi[i] == TagLink.SSI_VALUE_NA) {
                    if (tl != null) { /* Link exists - report SSI */
                        change = tl.reportSSI(TagLink.SSI_VALUE_NA)
                            || change;
                        /* If we're lost - drop the link */
                        if (tl.isLinkLost()) {
                            tag.insertTagLink(channels[i], null);
                            tl = null;
                        } else if (change) { /* If changed, report it */
                            tl.reportTagLinkAttributeChange();
                            tg.reportTagLinkUpdated(tag, tl);
                        }
                    }
                } else { /* Else, got a value */
                    if (tl == null) { /* If link not defined, make it */
                        tl = new MantisIITagLink(tag, channels[i]); /* Make it */
                        tl.reportSSI(ssi[i]); /* Update it */
                        tag.insertTagLink(channels[i], tl); /* And add it */
                    } else { /* Else, just update it */
                        change = tl.reportSSI(ssi[i]) || change; /* Update it */
                        if (change) { /* If changed, report it */
                            tl.reportTagLinkAttributeChange();
                            tg.reportTagLinkUpdated(tag, tl);
                        }
                    }
                }
            }
            if(is_sleep) {
                for (i = 0; i < channels.length; i++) {
                    /* Find the link, if it exists */
                    tl = (MantisIITagLink) tag.findTagLink(channels[i]);
                    if(tl != null) {
                        tl.stopLinkAgeOut();
                        tl.startLinkAgeOut(SLEEP_TAGLINK_AGEOUT);
                    }
                }
            }
            if(optima_payload < 0)
                TavisConcentratorDirectStreamHandler.postMessage(this, tag, ssi, ssis, is_lost, payload, oldvals);
        }
    }
	/**
	 * Parse message for GPS data
	 */
	public static GPSData parseGPSData(String line) {
		GPSData.Fix f = GPSData.Fix.NO_FIX;
		Double lat = null;
		Double lon = null;
		Double spd = null;
		Double crse = null;
		Double alt = null;
		Double epeh = null;
		Double epev = null;
		boolean err = false;
		String[] tok = line.split(",");
		String[] t2;
        long ts = 0;
		/* Process the tokens */
		for(String s : tok) {
			char c = s.charAt(0);
			switch(c) {
				case 'f':	/* Fix status */
					try {
						int v = Integer.parseInt(s.substring(1));
						if(v < 0)
							f = GPSData.Fix.NO_GPS;
						else if(v == 0)
							f = GPSData.Fix.NO_FIX;
						else if(v == 1)
							f = GPSData.Fix.FIX_2D;	
						else if(v >= 2)
							f = GPSData.Fix.FIX_3D;	
					} catch (NumberFormatException nfx) {
						err = true;
					}
					break;
				case 'l':	/* Lat/Lon */
					t2 = s.substring(1).split(":");
					if(t2.length >= 2) {
						try {
							lat = Double.valueOf(t2[0]);
							lon = Double.valueOf(t2[1]);
						} catch (NumberFormatException nfx) {
							err = true;
						}
					}
					break;
				case 's':	/* Speed/course */
					t2 = s.substring(1).split(":");
					if((t2.length >= 2) && (t2[1].length() > 0)) {
						try {
							spd = Double.valueOf(t2[0]);
							crse = Double.valueOf(t2[1]);
						} catch (NumberFormatException nfx) {
							err = true;
						}
					}
					else {
						try {
							spd = Double.valueOf(t2[0]);
							crse = null;
						} catch (NumberFormatException nfx) {
							err = true;
						}
					}
					break;
				case 'h':	/* Altitude */
					try {
						alt = Double.valueOf(s.substring(1));
					} catch (NumberFormatException nfx) {
						err = true;
					}
					break;
				case 'e':	/* EPE */
					t2 = s.substring(1).split(":");
					if((t2.length >= 2) && (t2[1].length() > 0)) {
						try {
							epeh = Double.valueOf(t2[0]);
							epev = Double.valueOf(t2[1]);
						} catch (NumberFormatException nfx) {
							err = true;
						}
					}
					else {
						try {
							epeh = Double.valueOf(t2[0]);
							epev = null;
						} catch (NumberFormatException nfx) {
							err = true;
						}
					}
					break;
				case 't':	/* Seconds ago */
					try {
						ts = System.currentTimeMillis() - (1000L * Long.valueOf(s.substring(1)));
					} catch (NumberFormatException nfx) {
						err = true;
					}
					break;
				default:
					break;
			}
		}
		/* If no error, make updated GPSData record */
		if(!err) {
            if(ts > 0)
    			return new GPSData(f, lat, lon, alt, crse, spd, epeh, epev, ts);
            else
    			return new GPSData(f, lat, lon, alt, crse, spd, epeh, epev);
		}
		else {
			return null;
		}
	}
	/* Handle GPS message */
    private void handleGPSMessage(MantisIIReaderSession sess, StringBuilder msg) {
        /* Ignore tag messages when we're not valid state */
        if(!ReaderStatus.isOnline(status)) {
            return;
        }
		GPSData d = parseGPSData(msg.toString());
		/* If no error, replace GPSData record */
		if(d != null) {
			if((our_gps != null) && (our_gps.getType().equals(GPS.GPSTYPE_GPS))) {
				GPSData prevd = our_gps.getGPSData();
				our_gps.setGPSData(d);
				/* If fix status changed, call listener */
                notifyGPSFixStatusChange(d.getGPSFix(), (prevd != null)?prevd.getGPSFix():GPSData.Fix.NO_GPS);
                /* Update GPS rule, as needed */
                if(our_gps_rule != null) {
                    our_gps_rule.handlGPSFixChange(d.hasFix());
                }
			}
			//RangerServer.info(d.toString());
		}
	}
    private void notifyGPSFixStatusChange(GPSData.Fix newfix, GPSData.Fix prevfix) {
        for (ReaderStatusListener l : status_listeners) {
            if(l instanceof ReaderGPSStatusListener) {
                ReaderGPSStatusListener sl = (ReaderGPSStatusListener)l;
                sl.readerGPSFixStatusChanged(this, newfix, prevfix);
            }
        }
    }
    /**
     * Handle service message (=,<svcid>,<svcmsg>).
     * Currently only serial service supported
     *
     */
    private void handleServiceMessage(String svcid, String svcmsg) {
        /* Call our listeners */
        for (ReaderStatusListener l : status_listeners) {
            if(l instanceof ReaderServiceStatusListener) {
                ReaderServiceStatusListener sl = (ReaderServiceStatusListener)l;
                sl.reportServiceMessage(this, svcid, svcmsg);
            }
        }
    }
    /**
     * Send string to serial device
     */
    public void sendSerialMessage(String msg) {
        String sdrv = getAttribString(M250ReaderFactory.ATTRIB_SERPORT_DRVR);
        if((sdrv != null) && (sdrv.equals("console") == false)) {
            String smsg = ":toser," + msg;
            session.enqueueCommand(smsg, null);
        }
    }
    /**
     * Get reader firmware version
     * 
     * @return firmware version
     */
    public String getReaderFirmwareVersion() {
		if(true_fw_ver.length() > 0)
			return true_fw_ver;
		else
	        return splitFromVer(fw_ver, false);
    }
    /**
     * Get reader firmware family
     * 
     * @return firmware family
     */
    public String getReaderFirmwareFamily() {
		return fw_fam;
	}

    /**
     * Up connection session connected - passes in established session
     * @param s - session
     */
    public void upConnectionEstablished(Session s, String mac) {
        if(enabled && upconn_enabled) {
            last_upconn_mac = mac;
			if(session != null) {
	            session.upConnectionEstablished(s);
    	        updateInitialization();
			}
        }
    }
    /**
     * Find reader by upconnection reader ID
     */
    public static MantisIIReader findReaderByUpconnectionID(String id) {
        return upconn_rdrs.get(id);
    }
    /**
     * Get up connection enabled
     */
    public boolean getUpConnectionEnabled() {
        return upconn_enabled;
    }
    /**
     * Get last upconnection MAC address
     */
    public String getLastUpConnectionMAC() {
        return last_upconn_mac;
    }
    /*
     * Check authentication from reader for up-connection
     */
    public boolean checkUpConnectionAuth(int sesskey, String response) {
        int hash = sesskey;       /* Start with sesskey */
        for(int i = 0; i < upconn_pwd.length(); i++) {
            hash = ((hash >> 3) & 0x1FFFFFFF) | (hash << 29); /* Roll 3 to right */
            hash = hash ^ (int)upconn_pwd.charAt(i); /* XOR in password character */
        }
        long lhash = 0xFFFFFFFFL & (long)hash;
        try {
            long resp = Long.parseLong(response, 16);  /* Parse answer as hex */
            if(resp == lhash) {  /* If match */
                return true;
            }
        } catch (NumberFormatException nfx) {
        }
        return false;
    }
    /**
     * Simple action for rotating partitions
     */
    private class RotatePartAction implements Runnable {
        /**
         * Method for implementing reconnect attempt
         */
        public void run() {
            rotact = null;
            if(attribs_ro == null)   /* Cleanup if deleted */
                return;
            if((part_count > 1) && (part_period > 0)) {
                Map<String, Object> ra = new HashMap<String,Object>(getReaderAttributes());
                /* Advance to next setting */
                ra.put(MantisIIReaderFactory.ATTRIB_PARTINDEX,
                    Integer.valueOf((part_index+1) % part_count));
                try {
                    setReaderAttributes(ra);
                } catch (BadParameterException bpx) {
                    RangerServer.error(
                        "Error during partition rotation - " + bpx);
                }
            }
        }
    }
    /**
     * Get tag capacity usage (percent)
     * @return usage (0-100), -1 if N/A
     */
    public int getUsedTagCapacityPercent() {
        return tag_cap_pct;
    }
    /**
     * Get tag timeout
     */
    public int getTagTimeout() {
        return getAttribInteger(MantisIIReaderFactory.ATTRIB_TAGTIMEOUT,
            MantisIIReaderFactory.DEFAULT_TAGTIMEOUT);
    }
    /**
     * Enable/disable pseudo-tags
     * @param enab - true=enable, false=disable
     */
    static void setPseudoTags(boolean enab) {
    }

	/**
	 * Request (or cancel) firmware upgrade
	 * @return true if request successfully started
	 */
	public boolean requestFirmwareUpgrade(boolean do_cancel) {
		boolean success = false;
		if(do_cancel) {
			if(upgrade_requested) {
				upgrade_requested = false;
				updateInitialization();
			}
			success = true;
		}
		else if (firmwareUpgradeAvailable()) {	/* We have firmware for this family? */
			if(upgrade_requested == false) {
				upgrade_requested = true;
				updateInitialization();
			}
			success = true;
		}
		return success;
	}
	/**
	 * Check upgrade process (percent done - -1 if not active) 
	 */
	public int firmwareUpgradeProgress() {
		if(upgrade_requested) {
			if(upg_fw != null) {
				return (int)((100 * upg_off) / upg_fw.length);
			}
			else if(upg_off == -1) {	/* We finished successfully */
				return 100;
			}
			else {
				return 0;
			}
		}
		else {
			return -1;
		}
	}
	/**
	 * Check if upgrade available
	 */
	public boolean firmwareUpgradeAvailable() {
		boolean good = false;

		if(FirmwareLibrary.checkFirmware(fw_fam)) {	/* We have firmware for this family? */
			String meth = FirmwareLibrary.getFirmwareMethod(fw_fam);
			if((meth != null) && (meth.equals(UPGMETH_FW) || meth.equals(UPGMETH_TERMUPG)) ) {
                Map<String,String> versions = FirmwareLibrary.getFirmwareVersions();
                String v = versions.get(fw_fam);
                if((v != null) && (true_fw_ver != null)) {
                    String split_v[] = v.split(".");
                    String split_cur_v[] = true_fw_ver.split(".");
                    if(split_v.length == split_cur_v.length) {
        				good = true;
                        for(int i = 0; i < split_v.length; i++) {
                            try {
                                int v1 = Integer.valueOf(split_v[i]);
                                int v2 = Integer.valueOf(split_cur_v[i]);
                                if(v2 > v1) {
                                    good = false;
                                    break;
                                }
                                else if (v1 > v2) {
                                    break;
                                }

                            } catch (NumberFormatException nfx) {
                                good = false;
                            }
                        }
                    }                                
                }
			}
		}
		return good;
	}
	/**
	 * Freshen DNS name - called from thread allowed to block
	 */
	public void freshenDNS() {
		InetSocketAddress newsock;

		if(hostname != null) {
			newsock = new InetSocketAddress(hostname, portnum);
            if ((sockaddr == null) || (!newsock.equals(sockaddr)) || sockaddr.isUnresolved()) {
                sockaddr = newsock;
				if(session != null)
					session.setIPAddress(sockaddr);
			}
		}
	}
	/**
	 * Is payload verify supported by reader firmware version
	 */
	public boolean isEnhPayloadVerifySupported() {
		return verify_supported;
	}
	/**
	 * Test if enhanced payload verification is active for a given tag
	 * for payloads from this reader
	 */
	public boolean isEnhPayloadVerifyActive(MantisIITag tag) {
        switch(tag.getTagGroup().getEnhPayloadVerify()) {
            case NO:
                return false;
            case IFSUPPORTED:
                return verify_supported;
            default:
                return true;
        }
	}
	/**
	 * Get address sessions is connected to (remote peer)
	 *
	 * @return ip address and port of peer, or null if not connected
	 */
	public InetAddress getRemoteAddress() {
		if((session != null) && (ReaderStatus.isOnline(status)))
			return session.getRemoteAddress();
		return null;
	}

	/**
     * Callback invoked whenever defaults are updated
	 */
	public void gpsDefaultsUpdated(GPSDefaults def) {
        /* Update reader initialization */
        if (inited)
            updateInitialization();
	}
	/**
	 * Get GPS tied to reader, if any
	 */
	public GPS getGPS() {
		return our_gps;
	}
    /**
     * Get reader startup time
     * @return 0 if unknown, else utc-msec from epoch
     */
    public long getReaderStartupTime() {
        return startup_utc_msec;
    }

    private static class NetInterface implements ReaderNetworkInterface {
        private String name;
        private String hwaddr;
        private boolean isup;
        private String ipv4addr;
        private List<String> ipv6addr;
        private String ipv4gw;
        private String ipv6gw;
        private long rxbytes, txbytes;

        public String getName() { return name; }
        public String getHWAddress() { return hwaddr; }
        public boolean isUp() { return isup; }
        public String[] getIPv4Addresses() { 
            if((ipv4addr == null) || (ipv4addr.length() == 0))
                return new String[0];
            else
                return new String[] { ipv4addr };
        }
        public String[] getIPv6Addresses() {
            return ipv6addr.toArray(new String[ipv6addr.size()]);
        }
        public String getIPv4Gateway() {
            return ipv4gw;
        }
        public String getIPv6Gateway() {
            return ipv6gw;
        }
        public long    getTotalRXBytes() {
            return rxbytes;
        }
        public long    getTotalTXBytes() {
            return txbytes;
        }
    }

    private class InterfaceRequest implements MantisIIReaderCommandResponseListener {
        private ReaderNetworkInterfaceStatusListener callback;

        InterfaceRequest(ReaderNetworkInterfaceStatusListener callback) {
            this.callback = callback;
        }
        public void commandResponseCallback(MantisIIReaderSession sess, 
            List<String> rsp, boolean completed) {
            ArrayList<ReaderNetworkInterface> rslt = null;
            if(completed && (rsp.size() > 0) && (!rsp.get(0).startsWith("E,"))) {   /* Good response? */
                rslt = new ArrayList<ReaderNetworkInterface>();
                for(String line : rsp) {
                    NetInterface ni = new NetInterface();
                    ni.name = "";
                    int idx = line.indexOf(":");    /* Get colon */
                    if(idx > 0) ni.name = line.substring(0, idx);  /* Parse out name */
                    /* Get hw address */
                    ni.hwaddr = getArg(line, "hwaddress");
                    /* Get ipv4 address */
                    ni.ipv4addr = getArg(line, "ipaddress");
                    /* Get ipv4 subnet */
                    String ipv4subnet = getArg(line, "subnet");
                    int ipv4subnetnum = 0;
                    if((ipv4subnet != null) && (ipv4subnet.length() > 0)) {
                        try {
                            InetAddress addr = InetAddress.getByName(ipv4subnet);
                            byte[] addr2 = addr.getAddress();
                            for(ipv4subnetnum = 0; ipv4subnetnum < 32; ipv4subnetnum++) {
                                if((addr2[ipv4subnetnum/8] & (1 << (7-(ipv4subnetnum%8)))) == 0)
                                    break;
                            }
                            if(ni.ipv4addr.length() > 0) {
                                ni.ipv4addr = ni.ipv4addr + "/" + ipv4subnetnum;
                            }
                        } catch (UnknownHostException uhx) {
                        }
                    }
                    /* Get ipv4 gateway */
                    ni.ipv4gw = getArg(line, "gateway");
                    /* Get ipv6 addresses */
                    ni.ipv6addr = getListArg(line, "ipaddr6list");
                    /* Get ipv6 gateway */
                    ni.ipv6gw = getArg(line, "gateway6");
                    /* Get rxbytes */
                    ni.rxbytes = getLongArg(line, "rxbytes");
                    /* Get txbytes */
                    ni.txbytes = getLongArg(line, "txbytes");
                    /* Get up status */
                    ni.isup = getArg(line, "up").equals("true");

                    rslt.add(ni);
                }
            }
            callback.readerNetworkInterfaceResponse(MantisIIReader.this, 
                (rslt != null)?rslt.toArray(new ReaderNetworkInterface[rslt.size()]):null);
        }
        private String getArg(String line, String aid) {
            String id = aid + "=";
            int idx = line.indexOf(id);
            if(idx >= 0) {
                idx = idx + id.length();
                int idx2 = line.indexOf(",", idx);   /* Find end */
                if(idx2 < 0) idx2 = line.length();
                String rslt = line.substring(idx, idx2);
                rslt = rslt.replaceAll("\"", "");
                return rslt;
            }
            return "";
        }
        private long getLongArg(String line, String aid) {
            String id = aid + "=";
            int idx = line.indexOf(id);
            if(idx >= 0) {
                idx = idx + id.length();
                int idx2 = line.indexOf(",", idx);   /* Find end */
                if(idx2 < 0) idx2 = line.length();
                String rslt = line.substring(idx, idx2);
                try {
                    return (long)Double.parseDouble(rslt);
                } catch (NumberFormatException nfx) {
                    return 0;   
                }
            }
            return 0;
        }
        private List<String> getListArg(String line, String aid) {
            String id = aid + "=";
            int idx = line.indexOf(id);
            ArrayList<String> rslt = new ArrayList<String>();
            if(idx >= 0) {
                idx = idx + id.length();
                idx = line.indexOf("{", idx);   /* Find start of list */
                if(idx < 0) return rslt;
                idx++;
                int idx2 = line.indexOf("}", idx);  /* Find end */
                if(idx2 < 0) return rslt;
                String listval = line.substring(idx, idx2);   /* Split out value */
                String[] tok = listval.split(",");
                for(String v : tok) {
                    v = v.replaceAll("\"", "");
                    rslt.add(v);
                }
            }
            return rslt;
        }
    }
    /**
     * Request reader network interface status
     */
    public void requestReaderNetworkInterfaceStatus(ReaderNetworkInterfaceStatusListener callback) {
        if(factory.getInterfaceStatusSupport()) {
            session.enqueueCommand("!interfacestatus", new InterfaceRequest(callback));
        }
        else {
            callback.readerNetworkInterfaceResponse(this, null);
        }
    }
    /**
     * Return reader connection encryption status
     */
    public boolean getReaderConnectionEncrypted() {
		if((session != null) && (ReaderStatus.isOnline(status)))
			return session.getEncrypted();
        return false;
    }
    /**
     * Set license for reader
     */
    public boolean setReaderLicense(boolean actval) {
    	return ReaderEntityDirectory.activateReader(actval);
    }
    /**
     * Add reader status attributes (non-configuration)
     */
    public void addReaderStatusAttributes(Map<String, Object> map) {
    	if (serialnum != null)
    		map.put("serialnum", serialnum);
    	if (servicedate != null)
    		map.put("servicedate", servicedate);
    	if (manufdate != null)
    		map.put("manufacturedate", manufdate);
    	if (macaddr != null)
    		map.put("macaddr", macaddr);
    }
}
