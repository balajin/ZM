package com.rfcode.drivers.readers;

/**
 * Defines a custom tag value field, consisting of a field ID and a value
 * 
 * @author Mike Primm
 */
public class CustomFieldValue {
	private final String fieldID;
	private final Object value;
	
	public CustomFieldValue(String field, Object val) {
		fieldID = field;
		value = val;
	}
	public final Object getValue() {
		return value;
	}
	public final String getField() {
		return fieldID;
	}
	public String toString() {
		return fieldID + "=" + value.toString();
	}
	public boolean equals(Object o) {
		if (o instanceof CustomFieldValue) {
			CustomFieldValue cfv = (CustomFieldValue) o;
			if (cfv.fieldID.equals(fieldID) && ((cfv.value == null)?(value == null):(cfv.value.equals(value)))) {
				return true;
			}
		}
		return false;
	}
}
