package com.rfcode.drivers.readers;

import java.util.Map;
import java.util.Set;

/**
 * This interface defines the model for representing a physical tag, and its
 * associated attributes and status. The attributes of the Tag object are
 * intended to represent those of the tag itself, independent of a specific
 * observer (i.e. a specific reader or channel) - observer-specific data should
 * be represented via the TagLink interface. Tags observed by multiple readers
 * should still only have a single instance of the class implementing the Tag
 * interface. Each Tag object will have one or more TagLink objects associated
 * with it - representing the view of the Tag by each of the channels of the
 * readers currently observing it.
 * 
 * @author Mike Primm
 * 
 */
public interface Tag {
    /**
     * Return values of tag attributes in provided array. The attributes are
     * indexed in the same order as the attribute IDs returned by the
     * getTagAttributes() call of the TagType defining the tag. This is intended
     * to be a high-performance interface.
     * 
     * @param val -
     *            array of length matching the number of attributes to return
     * @param start -
     *            index (relative to getTagAttributes()) of first attribute to
     *            return (goes in val[0])
     * @param count -
     *            number of consecutive attributes to return
     * @return number of attribute values returned
     */
    public int readTagAttributes(Object[] val, int start, int count);
    /**
     * Read tag attribute at given index
     * 
     * @param index -
     *            index of attribute to be read
     * @return attribute value, or null if not defined
     */
    public Object readTagAttribute(int index);
    /**
     * Get a copy of the curret tag attributes values
     * 
     * @return map containing copy of attribute values, keyed by attribute ID
     */
    public Map<String, Object> getTagAttributes();
    /**
     * Replace set of tag attributes.
     * 
     * @param idx -
     *            array of indexes of values to be written
     * @param val -
     *            array of new values to be written (note: not copied)
     * @param count -
     *            number of value to write (arrays may be longer than needed)
     * @return true if any of the attributes were changed
     */
    public boolean updateTagAttributes(int[] idx, Object[] val, int count);
    /**
     * Get the tag's globally unique ID. This MUST be consistent for all
     * observers of the tag.
     * 
     * @return GUID string
     */
    public String getTagGUID();
    /**
     * Get the tag's ID, relative to its TagGroup. This may or may not be the
     * same as the unique ID, but it will be unique relative to the TagGroup.
     * 
     * @return Tag ID
     */
    public String getTagID();
    /**
     * Get the TagGroup for the tag.
     * 
     * @return Tag's TagGroup
     */
    public TagGroup getTagGroup();
    /**
     * Get the TagType for the tag.
     * 
     * @return Tag's TagType
     */
    public TagType getTagType();
    /**
     * Get TagLink for given ReaderChannel
     * 
     * @param channel -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(ReaderChannel channel);
    /**
     * Get TagLink for given ReaderChannel channel ID
     * 
     * @param channelid -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(String channelid);
    /**
     * Get current set of TagLinks for Tag
     * 
     * @return Map of TagLinks, keyed by ReaderChannel ID
     */
    public Map<String, TagLink> getTagLinks();
    /**
     * Get current number of TagLinks for the Tag
     * 
     * @return number of TagLinks
     */
    public int getTagLinkCount();
    /**
     * Finish initialization of tag - must be called once attributes set
     */
    public void init();
    /**
     * Cleanup tag - unhook and make ready for discard
     */
    public void cleanup();
    /**
     * Is tag valid? Used to test if tag has been cleaned-up
     * 
     * @return true if tag is still valid
     */
    public boolean isValid();
    /**
     * Lock/unlock tag - used to keep tag from being cleaned up when no
     * links see it.  Each call with lock=true increments lock count.
     * Each call with lock=false decrements it.
     * @param lock - true to lock, false to unlock
     * @return new lock count
     */
    public int lockUnlockTag(boolean lock);
    /**
     * Get set of defaulted tag attributes
     *
     * @return Set<String> of attribute IDs, or null if none
     */
    public Set<String> getDefaultedTagAttributes();
    /**
     * Test if attribute is still default
     */
    public boolean isDefaultFlagged(int idx);
	/**
	 * Get set of subtags (null if none)
	 */
	public Set<Tag>	getSubTags();
	/**
	 * Get parent tag, if subtag
	 */
	public Tag getParentTag();
    /**
     * Set location match timestamp
     */
    public void setLocationMatchTimestamp(long ts);
    /**
     * Get location match timestamp
     */
    public long getLocationMatchTimestamp();
    /**
     * Test if tag is IR detecting tag
     * Note: may return false for tags that are IR, but have not yet reported a value
     * @param true if definitely IR detecting tag, false if may not be
     */
    public boolean isIRDetectingTag();
}
