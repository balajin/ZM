package com.rfcode.drivers.readers;

/**
 * Extension of GPS lifecycle listener, adding notifications for GPS data updates.
 * 
 * @author Mike Primm
 */
public interface GPSStatusListener extends GPSLifecycleListener {
	/**
	 * GPS position data changed
     * @param gps - GPS with new position data (access using gps.getGPSData())
	 * @param prev_data - previous GPS data for the GPS (value just replaced)
	 */
	public void gpsDataChanged(GPS gps, GPSData prev_data);
}
