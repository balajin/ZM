package com.rfcode.drivers.readers.mantis2;
import java.util.Map;

/**
 * Tag type for treatment 04P tags - Mantis II Tags - Treatment 04P (air pressure tags)
 * 
 * @author Mike Primm
 */
public class MantisIITagType04P extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04P";

	/** Tag type attribute - averaging period (minutes) (int) */
	public static final String TAGTYPEATTRIB_AVGPERIOD = "avgperiod";
	/** Tag type attribute - pressure rounding increment */
	public static final String TAGTYPEATTRIB_PRESSROUND = "pressround";
    /* Default rounding increment (in Pascals) */
    public static final double PRESSROUND_DEFAULT = 0.1;
	/** Tag type attribute - no data when low battery */
	public static final String TAGTYPEATTRIB_NODATALOWBATT = "nodatalowbatt";

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.DIFFPRESS_ATTRIB,
        MantisIITagType.AVGDIFFPRESS_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.DIFFPRESS_ATTRIB_LABEL,
        MantisIITagType.AVGDIFFPRESS_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL
        };
    private static final int DIFFPRESS_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int AVGDIFFPRESS_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, null,
        Boolean.FALSE};

    /* Previous payload needed - just 1 */
    private static final int PREV_PAYLOAD_CNT = 1;
    /* Maximum time between high byte message, and following low byte, in msec */
    private static final int MAX_HIGH_LOW_PERIOD = 20000;

	private static final double MAX_UNVERIFIED_PRESS_DELTA = 20.0;	/* 20 Pa max change without verify */
	
	private static final int DEFAULT_AVG_PERIOD = 10;


	/**
	 * Tag state - use to accumulate PDU data
	 */
	private static class OurTagState implements MantisIITag.MantisIITagState {
		long last_reading_ts;	/* Timestamp of last accumulated reading */
		double avg_accum;		/* Accumulator */
		boolean lowbatt;

		public void cleanupTag(MantisIITag t) { }
	}

    /**
     * Constructor
     */
    public MantisIITagType04P() {
        super(4, 'P', PREV_PAYLOAD_CNT, new String[] { TAGTYPEATTRIB_AVGPERIOD, TAGTYPEATTRIB_PRESSROUND, TAGTYPEATTRIB_NODATALOWBATT } );
        setLabel("RF Code Mantis II Tag - Treatment 04P");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_AVGPERIOD, Integer.valueOf(DEFAULT_AVG_PERIOD));	/* Default to 10 */
        defs.put(TAGTYPEATTRIB_PRESSROUND, Double.valueOf(PRESSROUND_DEFAULT));
        defs.put(TAGTYPEATTRIB_NODATALOWBATT, Boolean.valueOf(false));	/* Default to false */
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		OurTagState ts = (OurTagState)tag.getTagState();
		if(ts == null) {
			ts = new OurTagState();
			tag.setTagState(ts);
		}

        /* If second half of two part payload (low part) */
        if (cur_payload < 0400) {
            /* Get low battery value - verify on setting flag */
			ts.lowbatt = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, ts.lowbatt, verify && ts.lowbatt) || change;
            Map<String,Object> grp_attrib = tag.getTagGroup().getTagGroupAttributes();
            // If low battery, and we're nulling potentially invalid sensor values
            if (ts.lowbatt && Boolean.TRUE.equals(grp_attrib.get(TAGTYPEATTRIB_NODATALOWBATT))) {
                change = updateNull(tag, DIFFPRESS_ATTRIB_INDEX) || change;
                change = updateNull(tag, AVGDIFFPRESS_ATTRIB_INDEX) || change;
            }
            else {
            	/* If previous was high value, and not too old */
            	if ((payloads[0] >= 0400)
            			&& ((cur_timestamp - payloadage[0]) < MAX_HIGH_LOW_PERIOD)) {
            		int v = ((payloads[0] & 0xFF) << 8) | (cur_payload & 0xFF);
            		v >>= 4;     /* Roll off other flags */
            		if(v > 0x7FF) { /* If negative, sign extend */
            			v |= (-1 - 0xFFF);
            		}
            		Object rnd = grp_attrib.get(TAGTYPEATTRIB_PRESSROUND);
            		double rndval = PRESSROUND_DEFAULT;
            		if(rnd instanceof Double)
            			rndval = ((Double)rnd).doubleValue();
            		/* Compute pressure in Pascals */
            		double dpress = (double)v / 3.75;
            		double dpressrnd = dpress;
            		if(rndval > 0.0)
            			dpressrnd = Math.rint(dpress / rndval) * rndval;
            		change = updateDoubleVerify(tag, DIFFPRESS_ATTRIB_INDEX, dpressrnd, verify,
            				MAX_UNVERIFIED_PRESS_DELTA) || change;
            		/* Now, update averaged value */
            		if(ts.last_reading_ts != 0) {
            			long difftime = cur_timestamp - ts.last_reading_ts;
            			long avgtc = 60000 * DEFAULT_AVG_PERIOD;

            			Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_AVGPERIOD);
            			if((tc != null) && (tc instanceof Integer)) {
            				avgtc = 60000 * ((Integer)tc).intValue();
            			}
            			ts.avg_accum = ((ts.avg_accum * (avgtc - difftime)) + (dpress * difftime)) / (double)avgtc;
            		}
            		else {
            			ts.avg_accum = dpress;
            		}
            		ts.last_reading_ts = cur_timestamp;
            		double davg = ts.avg_accum;
            		if(rndval > 0.0)
            			davg = Math.rint(davg / rndval) * rndval;

            		change = updateDoubleVerify(tag, AVGDIFFPRESS_ATTRIB_INDEX, 
            				davg, verify, MAX_UNVERIFIED_PRESS_DELTA) || change;
            	}
            }
        }
        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
}
