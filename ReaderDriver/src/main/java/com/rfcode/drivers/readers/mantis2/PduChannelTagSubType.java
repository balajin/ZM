package com.rfcode.drivers.readers.mantis2;

/**
 * PDU generic-channel-specific data tag subtype
 * 
 * @author Mike Primm
 */
public class PduChannelTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduChannel";

	/* Tag attribute - channel amperage (amps) (double) */
	public static final String PDUAMPS_ATTRIB = "channelAmps";
	public static final String PDUAMPS_ATTRIB_LABEL = "Amperage";

	public static final int PDUAMPS_INDEX = 2;
	public static final int PDUTOWERID_INDEX = 3;

	public static final Double VALUE_NA = Double.valueOf(-1.0);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, PDUAMPS_ATTRIB, 
		MantisIITagType.PDUTOWERID_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, PDUAMPS_ATTRIB_LABEL,
		MantisIITagType.PDUTOWERID_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null, 
        null };
    /**
     * Constructor
     */
    public PduChannelTagSubType() {
        setLabel("PDU Channel Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }

	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double amps, Long towerid) {
		if(tag == null)
			return;
		int[] idx = new int[2];
		Object[] val = new Object[2];
		int cnt = 0;
		if(amps != null) {
			idx[cnt] = PDUAMPS_INDEX;
			if(amps.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = amps;
			cnt++;
		}
		if(towerid != null) {
			idx[cnt] = PDUTOWERID_INDEX;
			val[cnt] = towerid;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}
}
