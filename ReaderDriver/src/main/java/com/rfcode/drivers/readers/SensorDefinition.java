package com.rfcode.drivers.readers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.rfcode.drivers.BadParameterException;
import com.rfcode.ranger.RangerServer;

/**
 * Generic sensor definition (mapping function for converting raw readings to 'cooked' ones
 * 
 * @author Mike Primm
 */
public class SensorDefinition implements ReaderEntity {
	public static final String DEFTYPE = "deftype";
	public static final String SRC = "source";
	public static final String DEST = "dest";
	public static final String TAGS = "tags";
	
	private String id;
	private String source_attrib;
	private String dest_attrib;
	private final String type;
	private String label;
	private Set<String> tags_using_def = new HashSet<String>();
	
	private static HashMap<String, Class<? extends SensorDefinition>> deftypes = new HashMap<String, Class<? extends SensorDefinition>>();
	
	static {
		deftypes.put(CurrentLoopSensorDefinition.CURRENT_LOOP_DEF, CurrentLoopSensorDefinition.class);
		deftypes.put(Voltage0To5SensorDefinition.VOLTAGE_0TO5_DEF, Voltage0To5SensorDefinition.class);
	}

	public static SensorDefinition createDefinition(String type) {
		Class<? extends SensorDefinition> def = deftypes.get(type);
		if (def == null) {
			RangerServer.error("Invalid sensor definition type - " + type);
			return null;
		}
		try {
			return def.newInstance();
		} catch (InstantiationException x) {
			RangerServer.error("Cannot create sensor definition - " + x.getMessage());
			return null;
		} catch (IllegalAccessException e) {
			RangerServer.error("Cannot create sensor definition - " + e.getMessage()); 
			return null;
		}
	}
	
	public static boolean isValidDefinitionType(String type) {
		return deftypes.containsKey(type);
	}
	
	protected SensorDefinition(String type) {
		this.type = type;
	}
	
	/**
	 * Read attributes for given sensor definition
	 * 
	 * @return
	 */
	public Map<String,Object> getAttributes() {
		HashMap<String, Object> map = new HashMap<String,Object>();
		map.put(TAGS, new ArrayList<String>(this.tags_using_def));
		return map;
	}
	/**
	 * Load attributes from given map
	 */
	@SuppressWarnings("unchecked")
	public void setAttributes(Map<String, Object> map) throws BadParameterException {
		Object v = map.get(TAGS);
		
		tags_using_def.clear();
		if ((v != null) && (v instanceof List)) {
			tags_using_def.addAll((List<String>) v);
		}
	}
	
	@Override
	public String getID() {
		return id;
	}

	@Override
	public void setID(String id) {
		this.id = id;
	}
	
	public String getSourceAttrib() {
		return source_attrib;
	}
	
	public void setSourceAttrib(String src_attrib) {
		this.source_attrib = src_attrib;
	}
	
	public String getDestinationAttrib() {
		return dest_attrib;
	}
	
	public void setDestinationAttrib(String dest_attr) {
		this.dest_attrib = dest_attr;
	}
	
	public void setLabel(String lbl) {
		label = lbl;
	}
	
	public String getLabel() {
		if (label == null)
			return id;
		else
			return label;
	}

	@Override
	public void init() throws DuplicateEntityIDException, BadParameterException {
        if (id == null) {
            throw new BadParameterException("Missing ID");
        }
        if (source_attrib == null) {
            throw new BadParameterException("Missing Source Attribute");
        }
        if (dest_attrib == null) {
            throw new BadParameterException("Missing Destination Attribute");
        }
		/* Finally, add ourselves to the directory */
        ReaderEntityDirectory.addEntity(this);
	}

	@Override
	public void cleanup() {
		for (String id : tags_using_def) {
			removeTag(id);
		}
		if(id != null) {
			ReaderEntityDirectory.removeEntity(this);
			id = null;
		}
	}
	
	public String getSensorDefinitionType() {
		return type;
	}
	
	public boolean addTag(String tagid) {
		boolean rslt = !tags_using_def.contains(tagid);
		if (rslt) {
			TagGroup tg = TagGroup.findTagGroupForTagID(tagid);
			/* If valid group for tag ID AND suitable sensor defintiion for the tag type */
			if ((tg != null) && (tg.getTagType().isValidSensorDefinition(this))) {
				tags_using_def.add(tagid);
				Tag t = tg.findTag(tagid);
				if ((t != null) && (t instanceof SensorDefTag)) {
					((SensorDefTag) t).setSensorDefinition(this);
				}
			}
			else {
				rslt = false;
			}
		}
		return rslt;
	}
	
	public boolean removeTag(String tagid) {
		boolean rslt = tags_using_def.remove(tagid);
		if (rslt) {
			Tag t = TagGroup.findTagInAnyGroup(tagid);
			if ((t != null) && (t instanceof SensorDefTag)) {
				((SensorDefTag) t).setSensorDefinition(null);
			}
		}
		return rslt;
	}
	
	public boolean addAllTags(Set<String> tagids) {
		boolean rslt = false;
		for (String id : tagids) {
			rslt |= addTag(id);
		}
		return rslt;
	}

	public boolean removeAllTags(Set<String> tagids) {
		boolean rslt = false;
		for (String id : tagids) {
			rslt |= removeTag(id);
		}
		return rslt;
	}

	public Set<String> getTags() {
		return tags_using_def;
	}
	
	public static SensorDefinition findSensorDefinitionByTagID(String guid) {
		for (SensorDefinition sd : ReaderEntityDirectory.getSensorDefinitions()) {
			if (sd.tags_using_def.contains(guid)) {
				return sd;
			}
		}
		return null;
	}
}
