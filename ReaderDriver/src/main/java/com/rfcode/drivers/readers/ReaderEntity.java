package com.rfcode.drivers.readers;

import com.rfcode.drivers.BadParameterException;

/**
 * Common interface for reader objects with IDs and basic attribute access
 * methods. All entities must be registered with the ReaderEntityDirectory.
 * 
 * @author Mike Primm
 */
public interface ReaderEntity {
    /**
     * Get ID of entity - unique among all reader entities in system
     * 
     * @return ID
     */
    public String getID();
    /**
     * Assign unique ID to entity - does not apply until init() invoked
     * 
     * @param id -
     *            ID to be assigned
     */
    public void setID(String id);
    /**
     * Initialization method - must be called once attributes set. init() method
     * MUST call addEntity() to add entity to directory
     */
    public void init() throws DuplicateEntityIDException, BadParameterException;
    /**
     * Cleanup method. cleanup() method MUST call removeEntity() to remove
     * entity from directory
     */
    public void cleanup();

}
