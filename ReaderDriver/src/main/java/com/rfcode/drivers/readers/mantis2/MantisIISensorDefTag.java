package com.rfcode.drivers.readers.mantis2;

import com.rfcode.drivers.readers.SensorDefTag;
import com.rfcode.drivers.readers.SensorDefinition;

public class MantisIISensorDefTag extends MantisIITag implements SensorDefTag {
	
	private SensorDefinition sensordef;
	

	MantisIISensorDefTag(MantisIITagGroup grp, String guid) {
		super(grp, guid);

		sensordef = SensorDefinition.findSensorDefinitionByTagID(guid);
	}

	@Override
	public void setSensorDefinition(SensorDefinition sd) {
		sensordef = sd;
	}

	@Override
	public SensorDefinition getSensorDefinition() {
		if (sensordef != null)
			return sensordef;
		else
			return this.getMantisIITagType().getDefaultSensorDefinition((MantisIITagGroup) this.getTagGroup());
	}
}
