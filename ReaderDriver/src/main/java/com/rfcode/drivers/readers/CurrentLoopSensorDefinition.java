package com.rfcode.drivers.readers;

import java.util.Map;

import com.rfcode.drivers.BadParameterException;

/**
 * Sensor definition specific to 4-20mA current loop sensors
 *   Source attribute is always 'currentLoop' (type double)
 *   Destination attribute is always a double (or null)
 *   
 * @author Mike Primm
 */
public class CurrentLoopSensorDefinition extends SensorDefinition {
	public static final String CURRENT_LOOP_DEF = "currentloop";
    public static final String CURRENT_LOOP_READING_ATTRIB = "currentLoop";
    public static final String VAL_4MA = "value4mA";
    public static final String VAL_20MA = "value20mA";
    

    private double val_at_4ma;
    private double val_at_20ma;
    
    public CurrentLoopSensorDefinition() {
    	super(CURRENT_LOOP_DEF);
    	
    	setSourceAttrib(CURRENT_LOOP_READING_ATTRIB);
    }
    
    @Override
    public Map<String,Object> getAttributes() {
    	Map<String,Object> map = super.getAttributes();
    	map.put(VAL_4MA, val_at_4ma);
    	map.put(VAL_20MA, val_at_20ma);
    	return map;
    }
    
    @Override
    public void setAttributes(Map<String, Object> map) throws BadParameterException {
    	super.setAttributes(map);
    	
    	try {
    		val_at_4ma = (Double) map.get(VAL_4MA);
    		val_at_20ma = (Double) map.get(VAL_20MA);
		} catch (Exception x) {
			throw new BadParameterException("Bad value - " + x.getMessage());
		}
    }
    /**
     * Calculate value, given raw value in mA
     * 
     * @param rawval - raw mA
     * 
     * @return cooked value
     */
    public double getValue(double rawval) {
    	double off = (rawval - 4.0) / 16.0;
    	return val_at_4ma + (off * (val_at_20ma - val_at_4ma));
    }
}
