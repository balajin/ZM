package com.rfcode.drivers.readers;

/**
 * Network interface state object
 *
 * @author Mike Primm
 */
public interface ReaderNetworkInterface {
    /**
     * Get interface name
     */
    String getName();
    /**
     * Get HW address
     */
    String getHWAddress();
    /**
     * Is interface up?
     */
    boolean isUp();
    /**
     * Get IPv4 addresses (address/subnetbits)
     */
    String[] getIPv4Addresses();
    /**
     * Get IPv6 addresses (address/subnetbits)
     */
    String[] getIPv6Addresses();
    /**
     * Get IPv4 default gateway
     */
    String getIPv4Gateway();
    /**
     * Get IPv6 default gateway
     */
    String getIPv6Gateway();
    /**
     * Get current RX bytes
     */
    long    getTotalRXBytes();
    /**
     * Get current TX bytes
     */
    long    getTotalTXBytes();
}

