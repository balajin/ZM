package com.rfcode.drivers.readers.mantis2;

import java.util.HashMap;

import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagType;
import com.rfcode.ranger.RangerServer;

/**
 * Class for supporting tag subgroups
 * 
 * @author Mike Primm
 */
public class TagSubGroup extends TagGroup {
    /**
     * Constructor for tag group
     * 
     * @param tt -
     *            our tag type
     */
    TagSubGroup(TagType tt, TagGroup par) {
        super((TagType) tt, par);
		setTagGroupAttributes(new HashMap<String,Object>());
		RangerServer.info("Construct TagSubGroup: tt=" + tt.getID() + ", par=" + par.getID());
    }
    /**
     * Call tag lifecycle listeners for creation of given tag.
     * 
     * @param tag -
     *            new tag
     * @param cause -
     *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
     */
    void reportTagLifecycleCreate(Tag tag, String cause) {
        notifyTagLifecycleCreate(tag, cause);
    }
    /**
     * Call tag lifecycle listeners for deletion of given tag.
     * 
     * @param tag -
     *            deleted tag
     */
    void reportTagLifecycleDelete(Tag tag) {
        notifyTagLifecycleDelete(tag);
    }
    /**
     * Call tag status listeners for attribute changed notification
     * 
     * @param tag -
     *            tag with change attributes
     * @param oldval -
     *            array of previous tag attribute values (may be longer than the
     *            number of atributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    void reportTagStatusAttributeChange(Tag tag, Object[] oldval) {
        notifyTagStatusAttributeChange(tag, oldval);
    }
    /**
     * Add tag to the set of live tags in the group
     * 
     * @param tag -
     *            tag to add
     */
    void addSubTag(SubTag tag) {
        addTag(tag);
    }
    /**
     * Remove tag from the set of live tags in the group
     * 
     * @param tag -
     *            tag to remove
     */
    void removeSubTag(SubTag tag) {
        removeTag(tag);
    }
    /**
     * Test if given tag ID matches group
     * @param tag_guid - tag GUID
     * @return true if matches, false if not
     */
    public boolean isMatchingTagID(String tag_guid) {
    	return false;
    }

}
