package com.rfcode.drivers.readers.mantis2;
import java.util.Map;

/**
 * Tag type for treatment 06A tags - Mantis II Tags - Treatment 06A (temperature/humidity)
 * 
 * @author Mike Primm
 */
public class MantisIITagType06A extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis06A";

	/** Tag type attribute - temperature rounding increment */
	public static final String TAGTYPEATTRIB_TMPROUND = "tmpround";
    /* Default rounding increment */
    public static final double TMPROUND_DEFAULT = 0.1;
	/** Tag type attribute - humidity rounding increment */
	public static final String TAGTYPEATTRIB_HUMROUND = "humround";
    /* Default rounding increment */
    public static final double HUMROUND_DEFAULT = 0.1;

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.TEMPERATURE_ATTRIB,
        MantisIITagType.HUMIDITY_ATTRIB,
        MantisIITagType.DEWPOINT_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.TEMPERATURE_ATTRIB_LABEL,
        MantisIITagType.HUMIDITY_ATTRIB_LABEL,
        MantisIITagType.DEWPOINT_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL
        };
    private static final int TEMPERATURE_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int HUMIDITY_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int DEWPOINT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 3; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, null, null,
        Boolean.FALSE };

    private static final double DEWP_A = 17.271;
    private static final double DEWP_B = 237.7;

	private static final double MAX_UNVERIFIED_TMP_DELTA = 2.0;	/* 2C max change without verify */
	private static final double MAX_UNVERIFIED_HUM_DELTA = 2.0;	/* 2% max change without verify */
	private static final double MAX_UNVERIFIED_DEW_DELTA = 2.0;	/* 2C max change without verify */

    /**
     * Constructor
     */
    public MantisIITagType06A() {
        super(6, 'A', 0, new String[] { TAGTYPEATTRIB_TMPROUND, TAGTYPEATTRIB_HUMROUND });
        setLabel("RF Code Mantis II Tag - Treatment 06A");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_TMPROUND, Double.valueOf(TMPROUND_DEFAULT));	/* Default to 0.1 */
		defs.put(TAGTYPEATTRIB_HUMROUND, Double.valueOf(HUMROUND_DEFAULT));	/* Default to 0.1 */
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		/* Compute temperature */
        int vt = (cur_payload & 0x00FFF0) >> 4;
        double rawtmp = -46.85 + (175.72 * vt) / 4096.0;
        /* Apply rounding */
        double tmp = rawtmp;
    	Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_TMPROUND);
    	double rnd = TMPROUND_DEFAULT;
    	if(tc instanceof Double) {
    		rnd = ((Double)tc).doubleValue();
    	}
    	if(rnd > 0.0)
    		tmp = Math.rint(tmp / rnd) * rnd;
    	/* Compute humidity */
    	int vh = (cur_payload & 0xFF0000) >> 16;
    	double rawhumi = -6.0 + (125.0*vh) / 256.0;
    	if(rawhumi < 0.0) rawhumi = 0.0;
    	if(rawhumi > 100.0) rawhumi = 100.0;
    	/* Apply rounding */
    	double humi = rawhumi;
    	Object hr = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_HUMROUND);
    	double hrnd = HUMROUND_DEFAULT;
    	if(hr instanceof Double)
    		hrnd = ((Double)hr).doubleValue();
    	if(hrnd > 0.0)
    		humi = Math.rint(humi / hrnd) * hrnd;
    	if(rawhumi < 0.5) rawhumi = 0.5;    /* Assume 0.5% RH minimum for dew point calculation */
    	double dew = (DEWP_A * rawtmp / (DEWP_B + rawtmp)) + Math.log(rawhumi/100.0);
    	dew = DEWP_B * dew / (DEWP_A - dew);
    	/* Apply rounding to dew point */
    	if(rnd > 0.0)
    		dew = Math.rint(dew / rnd) * rnd;
    	if ((vt == 0xFFF) && (vh == 0xFF)) {	// Bad sensor?
    		change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX) || change;
    		change = updateNull(tag, HUMIDITY_ATTRIB_INDEX) || change;
    		change = updateNull(tag, DEWPOINT_ATTRIB_INDEX) || change;
    	}
    	else {
    		change = updateDoubleVerify(tag, TEMPERATURE_ATTRIB_INDEX, tmp, 
    			verify, MAX_UNVERIFIED_TMP_DELTA) || change;
    		change = updateDoubleVerify(tag, HUMIDITY_ATTRIB_INDEX, humi, verify,
    			MAX_UNVERIFIED_HUM_DELTA)
    			|| change;
    		change = updateDoubleVerify(tag, DEWPOINT_ATTRIB_INDEX, dew, verify, 
    			MAX_UNVERIFIED_DEW_DELTA)
    			|| change;
    	}
    	/* Get low battery value */
		boolean lowbatt = ((cur_payload & 0x02) != 0);
		/* Only require verify if setting flag */
        change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, lowbatt, verify && lowbatt) || change;

        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
