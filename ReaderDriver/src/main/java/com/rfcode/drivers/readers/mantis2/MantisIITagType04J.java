package com.rfcode.drivers.readers.mantis2;

/**
 * Tag type for treatment 04J tags - Mantis II Tags - Treatment 04J
 * 
 * @author Mike Primm
 */
public class MantisIITagType04J extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis04J";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.FLUID_ATTRIB,
        MantisIITagType.TAMPER_ATTRIB, 
        MantisIITagType.SENSORDISCONNECT_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB,
        MantisIITagType.MOTION_ATTRIB,
        MantisIITagType.PANIC_ATTRIB,
        MantisIITagType.DOOR_ATTRIB,
        MantisIITagType.DRY_ATTRIB };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.FLUID_ATTRIB_LABEL,
        MantisIITagType.TAMPER_ATTRIB_LABEL,
        MantisIITagType.SENSORDISCONNECT_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL,
        MantisIITagType.MOTION_ATTRIB_LABEL,
        MantisIITagType.PANIC_ATTRIB_LABEL,
        MantisIITagType.DOOR_ATTRIB_LABEL,
        MantisIITagType.DRY_ATTRIB_LABEL };
    private static final int FLUID_ATTRIB_INDEX = 0; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int TAMPER_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int SENSORDISCONNECT_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int LOWBATT_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int MOTION_ATTRIB_INDEX = 4; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int PANIC_ATTRIB_INDEX = 5; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int DOOR_ATTRIB_INDEX = 6; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int DRY_ATTRIB_INDEX = 7; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = {
        null, null, null, null, null, null, null, null };

    /* Payload value - minimum and max fluid tag payload */
    private static final int MIN_FLUID_PAYLOAD = 0740;
    private static final int MAX_FLUID_PAYLOAD = 0757;

    /* Payload value - minimum and max beacon tag payload */
    private static final int MIN_BEACON_PAYLOAD = 0760;
    private static final int MAX_BEACON_PAYLOAD = 0777;

    /* Payload value - minimum and max door tag payload */
    private static final int MIN_DOOR_PAYLOAD = 0720;
    private static final int MAX_DOOR_PAYLOAD = 0737;

    /* Payload value - minimum and max dry tag payload */
    private static final int MIN_DRY_PAYLOAD = 0700;
    private static final int MAX_DRY_PAYLOAD = 0717;

    /**
     * 
     * Constructor
     */
    public MantisIITagType04J() {
        super(4, 'J', 0);
        setLabel("RF Code Mantis II Tag - Treatment 04J");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean v;
		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        /* If payload is a fluid tag payload */
        if((cur_payload >= MIN_FLUID_PAYLOAD) && (cur_payload <= MAX_FLUID_PAYLOAD)) {
            /* Set motion flag */
			v = ((cur_payload & 0x04) != 0);
            change = updateBoolean(tag, SENSORDISCONNECT_ATTRIB_INDEX, v) || change;
            /* Set tamper flag - verify on set */
			v = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, v, verify && v) || change;
            /* Set fluid flag - verify on set */
			v = ((cur_payload & 0x01) != 0);
            change = updateBooleanVerify(tag, FLUID_ATTRIB_INDEX, v, verify && v) || change;
            /* Set low battery flag - verify on set */
			v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Null other attributes, if needed */
			change = updateNullVerify(tag, MOTION_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, PANIC_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DOOR_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DRY_ATTRIB_INDEX, verify) || change;
        }
        /* Else if payload is a classic beacon tag */
        else if((cur_payload >= MIN_BEACON_PAYLOAD) && (cur_payload <= MAX_BEACON_PAYLOAD)) {
            /* Set motion flag */
			v = ((cur_payload & 0x04) != 0);
            change = updateBoolean(tag, MOTION_ATTRIB_INDEX, v) || change;
            /* Set tamper flag - verify on set */
			v = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, v, verify && v) || change;
            /* Set panic flag - verify on set */
			v = ((cur_payload & 0x01) != 0);
            change = updateBooleanVerify(tag, PANIC_ATTRIB_INDEX, v, verify && v) || change;
            /* Set low battery flag - verify on set */
			v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Null other attributes, if needed */
			change = updateNullVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DOOR_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, FLUID_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DRY_ATTRIB_INDEX, verify) || change;
        }
        /* Else if payload is a door beacon tag */
        else if((cur_payload >= MIN_DOOR_PAYLOAD) && (cur_payload <= MAX_DOOR_PAYLOAD)) {
            /* Set motion flag */
			v = ((cur_payload & 0x04) != 0);
            change = updateBoolean(tag, MOTION_ATTRIB_INDEX, v) || change;
            /* Set tamper flag - verify on set */
			v = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, v, verify && v) || change;
            /* Set door flag - verify on set/clear */
			v = ((cur_payload & 0x01) != 0);
            change = updateBooleanVerify(tag, DOOR_ATTRIB_INDEX, v, verify) || change;
            /* Set low battery flag - verify on set */
			v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Null other attributes, if needed */
			change = updateNullVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, PANIC_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, FLUID_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DRY_ATTRIB_INDEX, verify) || change;
        }
        /* Else if payload is a dry beacon tag */
        else if((cur_payload >= MIN_DRY_PAYLOAD) && (cur_payload <= MAX_DRY_PAYLOAD)) {
            /* Set motion flag */
			v = ((cur_payload & 0x04) != 0);
            change = updateBoolean(tag, MOTION_ATTRIB_INDEX, v) || change;
            /* Set tamper flag - verify on set */
			v = ((cur_payload & 0x08) != 0);
            change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, v, verify && v) || change;
            /* Set dry flag - verify on set/clear */
			v = ((cur_payload & 0x01) != 0);
            change = updateBooleanVerify(tag, DRY_ATTRIB_INDEX, v, verify) || change;
            /* Set low battery flag - verify on set */
			v = ((cur_payload & 0x02) != 0);
            change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, v, verify && v) || change;
			/* Null other attributes, if needed */
			change = updateNullVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, DOOR_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, FLUID_ATTRIB_INDEX, verify) || change;
			change = updateNullVerify(tag, PANIC_ATTRIB_INDEX, verify) || change;
        }
        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
}
