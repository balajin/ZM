package com.rfcode.drivers.readers.mantis2;

import com.rfcode.drivers.readers.GPSDefaults;

/**
 * Thunderbolt 433MHz reader factory
 * 
 * @author Mike Primm
 */
public class M250ReaderFactory extends X200ReaderFactory {
    /** Our factory ID */
    public static final String FACTORYID = "M250";
    /* Operating mode number for reader */
    public static final int MODENUMBER = 433;
    /* Factory label */
    public static final String FACTORYLABEL = "RF Code M250 Readers (network interface)";
    /* Factory description */
    public static final String FACTORYDESC = "RF Code M250 Readers, using the network (TCP/IP) interface for communications.";
	/* Additional attributes - gps data sets (enum) */
	public static final String GPS_DATASET = GPSDefaults.GPS_DATASET;
	public static final String GPS_DATASET_USE_GLOBAL = "use-global";
	public static final String GPS_DATASET_DEFAULT = GPS_DATASET_USE_GLOBAL;
	/* Additional attributes - gps min period (seconds) */
	public static final String GPS_MIN_PERIOD = GPSDefaults.GPS_MIN_PERIOD;
	public static final int GPS_MIN_PERIOD_USE_GLOBAL = -1;
	public static final Integer GPS_MIN_PERIOD_DEFAULT = GPS_MIN_PERIOD_USE_GLOBAL;
	/* Additional attributes - gps min horizontal move (double - meters) */
	public static final String GPS_MIN_HORIZ = GPSDefaults.GPS_MIN_HORIZ;
	public static final double GPS_MIN_HORIZ_USE_GLOBAL = -1;
	public static final double GPS_MIN_HORIZ_DEFAULT = GPS_MIN_HORIZ_USE_GLOBAL;
	/* Additional attributes - gps min vertical move (double - meters) */
	public static final String GPS_MIN_VERT = GPSDefaults.GPS_MIN_VERT;
	public static final double GPS_MIN_VERT_USE_GLOBAL = -1;
	public static final double GPS_MIN_VERT_DEFAULT = GPS_MIN_VERT_USE_GLOBAL;
    /* Additional attributes - SSL mode (string) */
    public static final String SSLMODE = "sslmode";
    public static final String SSLMODE_DEFAULT = MantisIIReader.SSLMODE_IFAVAIL;
    /* Additional attributes - ignore low confidence initial beacons */
    public static final String IGNORE_LOWCONF_INITIAL = "ignorelowconfinit";
    public static final boolean IGNORE_LOWCONF_INITIAL_DEFAULT = false;
    /**
     * Constructor
     */
    public M250ReaderFactory() {
        setModeNumber(MODENUMBER);
        setID(FACTORYID);
        setReaderFactoryLabel(FACTORYLABEL);
        setReaderFactoryDescription(FACTORYDESC);
		setGroupCodeMax(32);	/* Allow 32 group codes on M250 */

		addAttribute(GPS_DATASET, GPS_DATASET_DEFAULT);
		addAttribute(GPS_MIN_PERIOD, GPS_MIN_PERIOD_DEFAULT);
		addAttribute(GPS_MIN_HORIZ, GPS_MIN_HORIZ_DEFAULT);
		addAttribute(GPS_MIN_VERT, GPS_MIN_VERT_DEFAULT);
		addAttribute(SSLMODE, SSLMODE_DEFAULT);
		addAttribute(IGNORE_LOWCONF_INITIAL, IGNORE_LOWCONF_INITIAL_DEFAULT);
    }
	/**
	 * Get GPS support
	 */
	public boolean getGPSSupport() {
		return true;
	}
    /* 
     * Get support for !interfacestatus command
     */
    public boolean getInterfaceStatusSupport() {
        return true;
    }
}
