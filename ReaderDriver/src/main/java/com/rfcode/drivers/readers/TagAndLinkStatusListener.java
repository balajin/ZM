package com.rfcode.drivers.readers;

/**
 * Extension of tag status listener, adding notifications for tag link
 * attribute changes.
 * 
 * @author Mike Primm
 */

public interface TagAndLinkStatusListener extends TagStatusListener {
    /**
     * Tag link attributes changed notification - called when any of the tag
     * link's attributes have been changed from their previous values.
     * 
     * @param taglink -
     *            tag link with change attributes
     */
    public void tagLinkStatusAttributeChange(TagLink taglink);
}
