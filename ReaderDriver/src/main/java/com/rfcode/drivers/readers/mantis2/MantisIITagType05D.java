package com.rfcode.drivers.readers.mantis2;

import java.util.HashMap;
import java.util.Map;

import com.rfcode.drivers.readers.ReaderEntityDirectory;
import com.rfcode.drivers.readers.SensorDefinition;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.Voltage0To5SensorDefinition;

/**
 * Tag type for treatment 05D tags - Mantis II Tags - Treatment 05D (0-5v)
 * 
 * @author Mike Primm
 */
public class MantisIITagType05D extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis05D";

	/** Tag type attribute - current rounding increment */
	public static final String TAGTYPEATTRIB_VOLTAGEROUNDING = "voltageRound";
	/** Tag type attribute - default sensor definition */
	public static final String TAGTYPEATTRIB_DEFSENSORDEF = "defaultSensorDef";
	
	
    /* Default rounding increment */
    public static final double VOLTAGEROUND_DEFAULT = 0.001;

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
    	SENSOR_VOLTAGE_READING_ATTRIB,
    	CUSTOM_FIELD_ATTRIB,
        LOWBATT_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
    	SENSOR_VOLTAGE_READING_ATTRIB_LABEL,
    	CUSTOM_FIELD_ATTRIB_LABEL,
        LOWBATT_ATTRIB_LABEL
        };
    private static final int SENSOR_VOLTAGE_READING_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int CUSTOM_FIELD_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 2; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, null,
        Boolean.FALSE};

	private static final double MAX_UNVERIFIED_V_DELTA = 0.01;	/* 0.01V max change without verify */
	private static final int VOLTAGE_UNKNOWN = 0xFFFF;
    /**
     * Constructor
     */
    public MantisIITagType05D() {
        super(5, 'D', 0, new String[] { TAGTYPEATTRIB_VOLTAGEROUNDING, TAGTYPEATTRIB_DEFSENSORDEF } );
        setLabel("RF Code Mantis II Tag - Treatment 05D");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_VOLTAGEROUNDING, Double.valueOf(VOLTAGEROUND_DEFAULT));	/* Default to 0.001 */
		defs.put(TAGTYPEATTRIB_DEFSENSORDEF, "");
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        int v = cur_payload >> 2;	/* Roll off other flags */
        /* Compute voltage */
        double voltage = 0.0002 * (double)v;
        /* Apply rounding, if defined */
		Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_VOLTAGEROUNDING);
        double rnd = VOLTAGEROUND_DEFAULT;
        if(tc instanceof Double) {
            rnd = ((Double)tc).doubleValue();
        }
        if(rnd > 0.0)
            voltage = Math.rint(voltage / rnd) * rnd;
        SensorDefinition sd = ((MantisIISensorDefTag) tag).getSensorDefinition();
            
        if (v == VOLTAGE_UNKNOWN) {	// No value yet
			change = updateNullVerify(tag, SENSOR_VOLTAGE_READING_ATTRIB_INDEX, verify) || change;
			if (sd == null) {
				change = updateNull(tag, CUSTOM_FIELD_ATTRIB_INDEX) || change;
			}
			else {
				change = updateCustomFieldVerify(tag, CUSTOM_FIELD_ATTRIB_INDEX, sd.getDestinationAttrib(), null, verify) || change;
			}
        }
        else {
            change = updateDoubleVerify(tag, SENSOR_VOLTAGE_READING_ATTRIB_INDEX, voltage, verify, MAX_UNVERIFIED_V_DELTA) || change;
			if (sd == null) {
				change = updateNull(tag, CUSTOM_FIELD_ATTRIB_INDEX) || change;
			}
			else {
				change = updateCustomFieldVerify(tag, CUSTOM_FIELD_ATTRIB_INDEX, sd.getDestinationAttrib(), 
						((Voltage0To5SensorDefinition)sd ).getValue(voltage), verify) || change;
			}
        }
        /* Get low battery value - verify on setting flag */
		boolean vv = ((cur_payload & 0x02) != 0);
        change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;

        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
	/**
	 * Check if tag type needs all beacons (versus being able to support
     * 'exception mode' non-reporting of duplicate beacons)
	 * @return true if all beacons needed (false is default) 
	 */
	public boolean verboseBeaconsRequired() {
		return true;
	}
	
    /**
     * Create tag instance
     * @param tg - tag group
     * @param guid - tag guid
     * @return tag
     */
	@Override
    public MantisIITag makeTag(MantisIITagGroup grp, String guid) {
    	return new MantisIISensorDefTag(grp, guid);
    }

	@Override
	public boolean isValidSensorDefinition(SensorDefinition sd) {
		return (sd instanceof Voltage0To5SensorDefinition);
	}
	
    /**
     * Get default sensor definition for group (group matches tag type)
     * @param grp - group
     * @return null if none
     */
	@Override
    public SensorDefinition getDefaultSensorDefinition(MantisIITagGroup grp) {
    	SensorDefinition sd = grp_to_sd.get(grp.getID());
    	if ((sd != null) && (sd.getID() == null)) {	// deleted?
    		grp_to_sd.put(grp.getID(), null);
    		sd = null;
    	}
    	return sd;
    }
	private HashMap<String, SensorDefinition> grp_to_sd = new HashMap<String, SensorDefinition>();
	/**
	 * Notification that tag group attributes have been set/updated
	 * 
	 * @param grp - tag group
	 */
	public void tagGroupAttributesUpdated(TagGroup grp) {
		super.tagGroupAttributesUpdated(grp);
		
		Object tc = grp.getTagGroupAttributes().get(TAGTYPEATTRIB_DEFSENSORDEF);
		SensorDefinition sd = null;
		if (tc instanceof String) {
			sd = ReaderEntityDirectory.findSensorDefinition((String)tc);
		}
		grp_to_sd.put(grp.getID(), sd);
	}
	/**
	 * Notification that tag group has been deleted
	 * 
	 * @param grp - tag group
	 */
    @Override
	public void tagGroupDeleted(TagGroup grp) {
    	super.tagGroupDeleted(grp);
    	
    	grp_to_sd.remove(grp.getID());
    }

}
