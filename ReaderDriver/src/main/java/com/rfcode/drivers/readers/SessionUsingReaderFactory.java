package com.rfcode.drivers.readers;

import com.rfcode.drivers.SessionFactory;

/**
 * Subclass of interface for reader factories that need to have a session
 * factory provided for them.
 * 
 * @author Mike Primm
 * 
 */
public interface SessionUsingReaderFactory extends ReaderFactory {
    /**
     * Inject the SessionFactory to be used for communications
     * 
     * @param sessfact -
     *            session communications factory
     */
    public void setSessionFactory(SessionFactory sessfact);
    /**
     * Get our session factory
     * 
     * @return session factory
     */
    public SessionFactory getSessionFactory();
}
