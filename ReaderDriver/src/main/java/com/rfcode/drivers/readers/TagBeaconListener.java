package com.rfcode.drivers.readers;

public interface TagBeaconListener {
	/**
	 * Raw beacon report
	 * 
	 * @param tag
	 * @param timestamp
	 * @param payload
	 * @param reader
	 * @param ssi
	 */
	public void tagBeacon(Tag tag, long timestamp, long payload, Reader reader, int[] ssi);
}
