package com.rfcode.drivers.readers;

import com.rfcode.drivers.readers.Reader;
import com.rfcode.drivers.readers.GPSData;

/**
 * General interface for position sources (such as GPS devices)
 * 
 * @author Mike Primm
 */
public interface GPS extends ReaderEntity {
    public static final String GPSTYPE_GPS = "GPS";
    public static final String GPSTYPE_STATIC = "STATIC";
	/**
	 * Get reader associated with GPS, if any
	 */
	public Reader getReader();
	/**
	 * Get ID of reader associated with GPS, if any
	 */
	public String getReaderID();
	/**
	 * Get current GPS readings
	 */
	public GPSData getGPSData();
    /**
     * Get Type of entity
     * 
     * @return "GPS" or "STATIC"
     */
    public String getType();
    /**
     * Get last GPS timestamp for given tag
     * @param guid - tag GUID
     */
    public long getLastGPSTimestampForTag(String guid);
    /**
     * Get last GPS data for given tag
     * @param guid - tag GUID
     */
    public GPSData getLastGPSDataForTag(String guid);
}
