package com.rfcode.drivers.readers.mantis2;

/**
 * Interface for handling session lifecycle events.
 * 
 * All methods MUST be handled quickly and without blocking, as they are invoked
 * by the SessionFactory thread.
 * 
 * @author Mike Primm
 */
public interface MantisIIReaderSessionHandler {
    /**
     * Session connect starting. Called before the connect is initiated.
     * 
     * @param s -
     *            session
     */
    public void readerSessionStartingConnect(MantisIIReaderSession s);
    /**
     * Session connected - used to notify when the session has completed an
     * attempt to connect to its target. If is_connected = true, the session has
     * connected successfully. If connected successfully, requests to write data
     * to the session can be enqueued and sessionDataRead() callbacks can begin
     * after this callback completes.
     * 
     * @param s -
     *            session
     * @param is_connected -
     *            if true, session was connected successfully. if false, connect
     *            failed.
     */
    public void readerSessionConnectCompleted(MantisIIReaderSession s,
        boolean is_connected);
    /**
     * Session disconnected - used to notify when the session has become
     * disconnected from its target. Starting with this call, requests to write
     * data to the session can no longer be enqueued. sessionDataRead() and
     * sessionDataWriteComplete() callbacks will not happen once the session is
     * disconnected.
     * 
     * @param s -
     *            session
     */
    public void readerSessionDisconnectCompleted(MantisIIReaderSession s);
    /**
     * Channel diagnostic updates - returns dBm noise levels for channels
     * @param s - session
     * @param noise_dbm - array of noise levels, in dbm
     * @param mps - estimates messages/sec (null if NA)
     * @param tagcappct - percent tag capacity used (0-100, -1 if unsupported)
     */
    public void readerSesssionChannelDiag(MantisIIReaderSession s,
        int noise_dbm[], int mps[], int tagcappct);
}
