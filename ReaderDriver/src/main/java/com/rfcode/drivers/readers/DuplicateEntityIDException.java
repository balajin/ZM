package com.rfcode.drivers.readers;

/**
 * Exception for flagging duplicate reader entity IDs
 * 
 * @author Mike Primm f
 */
public class DuplicateEntityIDException extends Exception {
    public static final long serialVersionUID = 0x9868459687492L;
    private String id;
    /**
     * Constructor for exception
     */
    public DuplicateEntityIDException(String id) {
        this.id = id;
    }
    public String getDuplicateID() {
        return id;
    }
}
