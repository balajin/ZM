package com.rfcode.drivers.readers;

import java.util.Map;
import java.util.List;

/**
 * General interface for object representing the channels on a reader. Channels
 * are the entities that detect and receive data from tags. Any reader must have
 * one or more channels.
 * 
 * @author Mike Primm
 */
public interface ReaderChannel extends ReaderEntity {
    /**
     * Get channel ID (unique relative to other channels on the same reader)
     * 
     * @return id
     */
    public String getRelativeChannelID();
    /**
     * Get channel label (presentation label)
     * 
     * @return label
     */
    public String getChannelLabel();
    /**
     * Set channel label (presentation label)
     * 
     * @param lab -
     *            channel label
     */
    public void setChannelLabel(String lab);
    /**
     * Get the reader that owns the channel
     * 
     * @return Reader
     */
    public Reader getReader();
    /**
     * Get copy of map of tag links associated with this channel
     * 
     * @return Map of links, keyed by Tag GUID
     */
    public Map<String, TagLink> getTagLinks();
    /**
     * Get number of tag links associated with this channel
     * 
     * @return number of links
     */
    public int getTagLinkCount();
    /**
     * Add tag link lifecycle or status listener for any tag links on this
     * channel.
     * 
     * @param listen -
     *            listener to add
     */
    public void addTagLinkListener(TagLinkLifecycleListener listen);
    /**
     * Remove tag link lifecycle or status listener for any tag links on this
     * channel.
     * 
     * @param listen -
     *            listener to remove
     */
    public void removeTagLinkListener(TagLinkLifecycleListener listen);
    /**
     * Get list of tag link lifecycle and status listeners for this channel.
     * 
     * @return list of listeners (read-only)
     */
    public List<TagLinkLifecycleListener> getTagLinkListeners();
    /**
     * Test if channel is still valid (not cleaned up)
     * 
     * @return true if valid
     */
    public boolean isValid();
    /**
     * Get channel noise level, in dbm
     */
    public int getChannelNoiseLevel();
    /**
     * Get channel est events per sec
     */
    public int getChannelEventsPerSec();

}
