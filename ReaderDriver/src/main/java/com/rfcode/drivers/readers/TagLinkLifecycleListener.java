package com.rfcode.drivers.readers;

/**
 * Tag link lifecycle listener - used for reporting tag link add/remove events.
 * 
 * @author Mike Primm
 */
public interface TagLinkLifecycleListener {
    /**
     * TagLink added notificiation. Called after taglink has been added to tag.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    public void tagLinkAdded(Tag tag, TagLink taglink);
    /**
     * TagLink removed notificiation. Called after taglink has been removed from
     * tag.
     * 
     * @param tag -
     *            tag with removed link
     * @param taglink -
     *            link removed from tag
     */
    public void tagLinkRemoved(Tag tag, TagLink taglink);
}
