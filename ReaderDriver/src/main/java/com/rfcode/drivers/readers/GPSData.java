package com.rfcode.drivers.readers;

/**
 * Invariant set of GPS coordinates
 * 
 * @author Mike Primm
 */
public class GPSData {
	/** Enum of GPS fix status */
	public enum Fix {
		NO_GPS,
		NO_FIX,
		FIX_2D,
		FIX_3D
	};

	private Double lat, lon;
	private Double alt;
	private Double crse, spd;
	private Double epe_horiz, epe_vert;
    private long    ts;

	private  Fix fix;

	/**
	 * Constructor
	 */
	public GPSData(Fix f, Double latitude, Double longitude, Double altitude, Double course, Double speed,
		Double epe_h, Double epe_v, long ts) {
		fix = f;
		lat = latitude;
		lon = longitude;
		alt = altitude;
		crse = course;
		spd = speed;
		epe_horiz = epe_h;
		epe_vert = epe_v;
        this.ts = ts;
	}
	/**
	 * Constructor
	 */
	public GPSData(Fix f, Double latitude, Double longitude, Double altitude, Double course, Double speed,
		Double epe_h, Double epe_v) {
        this(f, latitude, longitude, altitude, course, speed, epe_h, epe_v, 0);
    }
	/**
	 * Constructor
	 */
	public GPSData(Fix f) {
		this(f, null, null, null,null, null, null, null);
	}
    /**
     * Copy constructor, with new timestamp
     */
    public GPSData(GPSData d, long ts) {
		fix = d.fix;
		lat = d.lat;
		lon = d.lon;
		alt = d.alt;
		crse = d.crse;
		spd = d.spd;
		epe_horiz = d.epe_horiz;
		epe_vert = d.epe_vert;
        this.ts = ts;
    }
	/**
	 * Get latitude (degrees N)
	 */
	public Double getLatitude() { return lat; }
	/**
	 * Get longitude (degrees E)
	 */
	public Double getLongitude() { return lon; }
	/**
	 * Get fix status
	 */
	public Fix getGPSFix() { return fix; }
	/**
	 * Get altitude (meters above WGS-84)
	 */
	public Double getAltitude() { return alt; }
	/**
	 * Get course (degrees from N)
	 */
	public Double getCourse() { return crse; }
	/**
	 * Get speed (meters/sec)
	 */
	public Double getSpeed() { return spd; }
	/**
	 * Get EPE horizontal (meters)
	 */
	public Double getEPEHorizontal() { return epe_horiz; }
	/**
	 * Get EPE vertical (meters)
	 */
	public Double getEPEVertical() { return epe_vert; }
	/**
	 * Get reading timestamp (UTC msec) - returns zero if no associated timestamp
	 */
	public long getTimestamp() { return ts; }
	/**
	 * String version of GPS data
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("GPSData: fix=").append(fix);
		if(lat != null)
			sb.append(" lat=").append(lat);
		if(lon != null)
			sb.append(" lon=").append(lon);			
		if(alt != null)
			sb.append(" alt=").append(alt);
		if(crse != null)
			sb.append(" course=").append(crse);
		if(spd != null)
			sb.append(" speed=").append(spd);
		if(epe_horiz != null)
			sb.append(" epe-h=").append(epe_horiz);
		if(epe_vert != null)
			sb.append(" epe-v=").append(epe_vert);
		return sb.toString();
	}
    /**
     * Test if GPS has fix (2D or 3D)
     */
    public boolean hasFix() {
        return (fix == Fix.FIX_2D) || (fix == Fix.FIX_3D);
    }
}

