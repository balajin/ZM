package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Collections;

import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagGroup;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagType;

/**
 * Class representing a specific sub-tag.
 * 
 * @author Mike Primm
 * 
 */
public class SubTag implements Tag {
    private TagSubGroup our_group;  /* Our group (and through it, tag type) */
    private Object[] attribs; 		/* Attribute values, indexed to match array of */
    private boolean[] updattribs;   /* Flags indicating which attributes are not default */
    /* attribute IDs returned from our TagType */
	private Tag    par_tag;			/* Parent tag */
    private String tag_subid;		/* Our sub-id (unique ID among subtags of our parent) */
	private String tag_guid;		/* Our guid : par_guid + "_" + tag_subid */
    private int lock_cnt;			/* Tag lock count */
	private static HashMap<String, Set<Tag>>	subtags = new HashMap<String, Set<Tag>>();
    /**
     * Constructor for tag
     * 
     * @param grp -
     *            our group
     * @param parent_tag -
     *            our parent tag
	 * @param subid -
     *            our sub-id (unique among subtags for our parent)
     */
    private SubTag(TagSubGroup grp, Tag parent_tag, String subid) {
        our_group = grp; 			/* Remember group */
        par_tag = parent_tag;		/* Save parent tag */
		tag_subid = subid;			/* Save our subid */
		tag_guid = parent_tag.getTagGUID() + "_" + tag_subid;
        attribs = grp.getTagType().getTagAttributeDefaultValues(); /*
                                                                     * Get
                                                                     * default
                                                                     * attributes
                                                                     */
        updattribs = new boolean[attribs.length];
		int idx = grp.getTagType().getTagAttributeIndex(TagSubType.SUBINDEX_ATTRIB);
		if(idx >= 0) {
			attribs[idx] = subid;	/* Fill in index attribute */
			updattribs[idx] = true;
		}
		idx = grp.getTagType().getTagAttributeIndex(TagSubType.PARENTTAG_ATTRIB);
		if(idx >= 0) {
			attribs[idx] = parent_tag.getTagGUID();
			updattribs[idx] = true;
		}
    }
    /**
     * Finish initialization of tag - must be called once attributes set
     */
    public void init() {
		String pid = par_tag.getTagGUID();
		if(pid != null) {
			synchronized(subtags) {
				Set<Tag> ts = subtags.get(pid);	/* Get subtag set */
				if(ts == null) {	/* Not found, we're first one */
					ts = new HashSet<Tag>();
					subtags.put(pid, ts);
				}
				ts.add(this);
			}
		}
		our_group.addSubTag(this);		/* Add our tag to our group */
    }
    /**
     * Cleanup tag - unhook and make ready for discard
     */
    public void cleanup() {
        if (our_group != null) {
            our_group.reportTagLifecycleDelete(this);
            our_group.removeSubTag(this);
            our_group = null;
        }
        attribs = null;
        updattribs = null;
		if(par_tag != null) {
			String pid = par_tag.getTagGUID();
			if(pid != null) {
				synchronized(subtags) {
					Set<Tag> ts = subtags.get(pid);	/* Get subtag set */
					if(ts != null) {
						ts.remove(this);
						if(ts.isEmpty()) {	/* No more? */
							subtags.remove(pid);
						}
					}
				}
			}
			par_tag = null;
		}
    }
    /**
     * Is tag valid? Used to test if tag has been cleaned-up
     * 
     * @return true if tag is still valid
     */
    public boolean isValid() {
        return (attribs != null);
    }
    /**
     * Build GUID for tag with given group, parent tag ID and subid
     * 
     * @param par_guid -
     *            parent tag GUID
     * @param subid -
     *            tag subid
     * @return GUID
     */
    public static String buildTagGUID(Tag par_tag, String subid) {
		return par_tag.getTagGUID() + "_" + subid;
    }
    /**
     * Find tag, if exists, or create new tag
     * 
     * @param grp -
     *            our group
     * @param par_tag -
     *            our parent tag
     * @param subid -
     *            our subid
     * @param no_create -
     *            if true, do not do create if missing
     * @return tag instance
     */
    public static SubTag findOrCreateTag(TagSubGroup grp, Tag par_tag, String subid,
        boolean no_create) {
		return findOrCreateTag(grp, par_tag, subid, no_create, null, null);
	}
    /**
     * Find tag, if exists, or create new tag, and apply given attributes to tag
     * 
     * @param grp -
     *            our group
     * @param par_tag -
     *            our parent tag
     * @param subid -
     *            our subid
     * @param no_create -
     *            if true, do not do create if missing
     * @return tag instance
     */
    public static SubTag findOrCreateTag(TagSubGroup grp, Tag par_tag, String subid,
        boolean no_create, int[] idx, Object[] val) {
        String guid = buildTagGUID(par_tag, subid);
        SubTag tag = (SubTag) grp.findTag(guid); /* Try to find it */
        if ((tag == null) && (!no_create)) { /* Not found, make new one */
            tag = new SubTag(grp, par_tag, subid); /* Create new one */
            tag.init();
			if(idx != null) {
		        for (int i = 0; i < idx.length; i++) {
		            int ix = idx[i];
		            /* If was null or is different */
            		boolean diff = (tag.attribs[ix] == null)?(val[i] != null):
		                (!tag.attribs[ix].equals(val[i]));
		            if (diff || (!tag.updattribs[ix])) {
		                tag.attribs[ix] = val[i];
		                tag.updattribs[ix] = true;
		            }
		        }
			}
            grp.reportTagLifecycleCreate(tag, null);
        }
		else {	/* Else, if needed, update the existing one */
			if(idx != null) {
				tag.updateTagAttributes(idx, val, idx.length);
			}
		}
        return tag;
    }
    /**
     * Validate our attribute set - make sure we fault in new values
     */
    private void validateAttribs() {
        int cnt = getTagType().getTagAttributeCount();
        if (attribs.length >= cnt) {
            return;
        }
        /* Get defaults and overlay our values */
        Object[] newattr = getTagType().getTagAttributeDefaultValues();
        System.arraycopy(attribs, 0, newattr, 0, attribs.length);
        attribs = newattr; /* And switch to new stuff */
        boolean[] newupd = new boolean[newattr.length];
        System.arraycopy(updattribs, 0, newupd, 0, updattribs.length);
        updattribs = newupd;
    }
    /**
     * Return values of tag attributes in provided array. The attributes are
     * indexed in the same order as the attribute IDs returned by the
     * getTagAttributes() call of the TagType defining the tag. This is intended
     * to be a high-performance interface.
     * 
     * @param val -
     *            array of length matching the number of attributes to return
     * @param start -
     *            index (relative to getTagAttributes()) of first attribute to
     *            return (goes in val[0])
     * @param count -
     *            number of consecutive attributes to return
     * @return number of attribute values returned
     */
    public int readTagAttributes(Object[] val, int start, int count) {
        int i, j;
        boolean did_validate = false;
        for (i = start, j = 0; j < count; i++, j++) {
            /*
             * If in range, return value (just let negatives throw
             * ArrayIndexOutOfBounds - its what we'd do
             */
            if (i < attribs.length) {
                val[j] = attribs[i];
            } else { /* Past end */
                if (!did_validate) {
                    validateAttribs(); /* Update if needed once */
                    did_validate = true;
                }
                if (i < attribs.length) /* If in bounds now, get it */
                    val[j] = attribs[i];
                else
                    /* Else, undefined */
                    val[j] = null;
            }
        }
        return count; /* Return number of values copied */
    }
    /**
     * Read tag attribute at given index
     * 
     * @param index -
     *            index of attribute to be read
     * @return attribute value, or null if not defined
     */
    public Object readTagAttribute(int index) {
        if (index < attribs.length)
            return attribs[index];
        validateAttribs();
        if (index < attribs.length)
            return attribs[index];
        return null;
    }

    /**
     * Get a copy of the curret tag attributes values
     * 
     * @return map containing copy of attribute values, keyed by attribute ID
     */
    public Map<String, Object> getTagAttributes() {
        validateAttribs(); /* Update if needed */

        HashMap<String, Object> map = new HashMap<String, Object>();
        String[] ids = getTagType().getTagAttributes();

        for (int i = 0; i < attribs.length; i++) { /* Add values to map */
            map.put(ids[i], attribs[i]);
        }
        return map;
    }
    /**
     * Replace set of tag attributes.
     * 
     * @param idx -
     *            array of indexes of values to be written
     * @param val -
     *            array of new values to be written (note: not copied)
     * @param count -
     *            number of value to write (arrays may be longer than needed)
     */
    public boolean updateTagAttributes(int[] idx, Object[] val, int count) {
        if((attribs == null) || (idx == null) || (val == null))
            return false;
        validateAttribs();
        Object oldval[] = null;
        for (int i = 0; i < count; i++) {
            int ix = idx[i];
            /* If was null or is different */
            boolean diff = (attribs[ix] == null)?(val[i] != null):
                (!attribs[ix].equals(val[i]));
            if (diff || (!updattribs[ix])) {
                /* Back up old values, if not already done */
                if (oldval == null)
                    oldval = attribs.clone();
                attribs[ix] = val[i];
                updattribs[ix] = true;
            }
        }
        /* If changes made, report them */
        if (oldval != null) {
            our_group.reportTagStatusAttributeChange(this, oldval);
        }
        return (oldval != null);
    }
    /**
     * Get the tag's globally unique ID. This MUST be consistent for all
     * observers of the tag.
     * 
     * @return GUID string
     */
    public String getTagGUID() {
        return tag_guid;
    }
    /**
     * Get the tag's ID, relative to its TagGroup. This may or may not be the
     * same as the unique ID, but it will be unique relative to the TagGroup.
     * 
     * @return Tag ID
     */
    public String getTagID() {
        return par_tag.getTagID() + "_" + tag_subid;
    }
    /**
     * Get the TagGroup for the tag.
     * 
     * @return Tag's TagGroup
     */
    public TagGroup getTagGroup() {
        return our_group;
    }
    /**
     * Get TagLink for given ReaderChannel
     * 
     * @param channel -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(ReaderChannel channel) {
        return null;
    }
    /**
     * Get TagLink for given ReaderChannel ID
     * 
     * @param channelid -
     *            Channel to be accessed
     * @return TagLink, or null if not defined
     */
    public TagLink findTagLink(String channelid) {
        return null;
    }
    /**
     * Set TagLink for given ReaderChannel - replaces existing if one is
     * present.
     * 
     * @param channel -
     *            Channel to be accessed
     * @param taglink -
     *            TagLink to be inserted: null to remove existing link
     */
    public void insertTagLink(ReaderChannel channel, TagLink taglink) {
		return;
    }
    /**
     * Get current set of TagLinks for Tag (read-only)
     * 
     * @return Map of TagLinks, keyed by ReaderChannel ID
     */
    public Map<String, TagLink> getTagLinks() {
        return Collections.emptyMap();
    }
    /**
     * Get current number of TagLinks for this tag
     * 
     * @return number of active TagLinks
     */
    public int getTagLinkCount() {
        return 0;
    }
    /**
     * Get the TagType for the tag.
     * 
     * @return Tag's TagType
     */
    public TagType getTagType() {
        if(our_group != null)
            return our_group.getTagType();
        else
            return null;
    }
    /**
     * Lock/unlock tag - used to keep tag from being cleaned up when no
     * links see it.  Each call with lock=true increments lock count.
     * Each call with lock=false decrements it.
     * @param lock - true to lock, false to unlock
     * @return new lock count
     */
    public int lockUnlockTag(boolean lock) {
        if(lock) {
            lock_cnt++;
        }
        else {
            lock_cnt--;
            if(lock_cnt <= 0) {
                cleanup();
            }
        }
        return lock_cnt;
    }
    /**
     * Equals handler
     */
    public boolean equals(Object o) {
        if((o != null) && (o instanceof Tag)) {
            return tag_guid.equals(((Tag)o).getTagGUID());
        }
        return false;
    }
    /**
     * Get set of defaulted tag attributes
     *
     * @return Set<String> of attribute IDs, or null if none
     */
    public Set<String> getDefaultedTagAttributes() {
        Set<String> rslt = null;
        String[] ids = null;
        for(int i = 0; i < updattribs.length; i++) {
            if(updattribs[i] == false) { /* If defaulted? */
                if(ids == null)
                    ids = getTagType().getTagAttributes();
                if(rslt == null)
                    rslt = new TreeSet<String>();
                rslt.add(ids[i]);
            }
        }
        return rslt;
    }
    /**
     * Access raw attribute array (internal use only)
     */
    Object[] getRawAttribs() {
        return attribs;
    }
    /**
     * Test if attribute is still default
     */
    public boolean isDefaultFlagged(int idx) {
        return !updattribs[idx];
    }
    /**
     * Mark attribute as updated from default
     */
    void clearDefaultFlag(int idx) {
        updattribs[idx] = true;
    }
	/**
	 * Get set of subtags (null if none)
	 */
	public Set<Tag>	getSubTags() {
		return null;
	}
	/**
     * Get set of subtags
	 */
	public static Set<Tag>	getSubTagsByTag(String tagid) {
		synchronized(subtags) {
			Set<Tag> ts = subtags.get(tagid);
			if(ts != null)
				return new HashSet<Tag>(ts);
		}
		return null;
	}
	/**
	 * Get parent tag, if subtag
	 */
	public Tag getParentTag() {
		return par_tag;
	}
    /**
     * Set location match timestamp
     */
    public void setLocationMatchTimestamp(long ts) {
    }
    /**
     * Get location match timestamp
     */
    public long getLocationMatchTimestamp() {
        return 0;
    }
    /**
     * Test if tag is IR detecting tag
     * Note: may return false for tags that are IR, but have not yet reported a value
     * @param true if definitely IR detecting tag, false if may not be
     */
    public boolean isIRDetectingTag() {
        return par_tag.isIRDetectingTag();
    }
}
