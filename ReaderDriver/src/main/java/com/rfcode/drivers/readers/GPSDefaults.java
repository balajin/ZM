package com.rfcode.drivers.readers;

import java.util.LinkedList;
import java.util.ArrayList;
/**
 * Global defaults for GPS parameters
 * 
 * @author Mike Primm
 */
public class GPSDefaults {
	private static GPSDefaults def = new GPSDefaults();

	private int min_period = 30;	/* Minimum period (seconds) */
	private double min_horiz = 10.0;	/* Min horizontal move (meters) */
	private double min_vert = 100.0;	/* Min vertical move (meters) */
	private String dataset = GPS_DATASET_LATLON;	/* Data set */
	
	/* Additional attributes - gps data sets (enum) */
	public static final String GPS_DATASET = "gpsdata";
	public static final String GPS_MIN_PERIOD = "gpsminperiod";
	public static final String GPS_MIN_HORIZ = "gpsminhoriz";
	public static final String GPS_MIN_VERT = "gpsminvert";

	/* Values for data set */
	public static final String GPS_DATASET_LATLON = "lat-lon";
	public static final String GPS_DATASET_LATLONEPE = "lat-lon-epe";
	public static final String GPS_DATASET_LATLONSPD = "lat-lon-spd";
	public static final String GPS_DATASET_LATLONSPDEPE = "lat-lon-spd-epe";
	public static final String GPS_DATASET_LATLONALT = "lat-lon-alt";
	public static final String GPS_DATASET_LATLONALTEPE = "lat-lon-alt-epe";
	public static final String GPS_DATASET_LATLONALTSPD = "lat-lon-alt-spd";
	public static final String GPS_DATASET_LATLONALTSPDEPE = "lat-lon-alt-spd-epe";

	public static final String[] GPS_DATASET_VALUES = {	GPS_DATASET_LATLON, GPS_DATASET_LATLONEPE, GPS_DATASET_LATLONSPD,
		GPS_DATASET_LATLONSPDEPE, GPS_DATASET_LATLONALT, GPS_DATASET_LATLONALTEPE, GPS_DATASET_LATLONALTSPD,
		GPS_DATASET_LATLONALTSPDEPE };

	public interface UpdateListener {
		/**
	     * Callback invoked whenever defaults are updated
		 */
		public void gpsDefaultsUpdated(GPSDefaults def);
	}

	private static LinkedList<UpdateListener> listeners = new LinkedList<UpdateListener>();

	/**
	 * Get singleton
	 */
	public static GPSDefaults getDefault() {
		return def;
	}
	/**
	 * Update with new defaults
	 */
	public static boolean replaceDefault(String dataset, int min_per, double min_horiz, double min_vert) {
		GPSDefaults newdef = new GPSDefaults();
		boolean isgood = false;

		/* Make sure valid dataset value */
		for(String v : GPS_DATASET_VALUES) {
			if(v.equals(dataset)) {
				isgood = true;
				break;
			}
		}
		/* If not good dataset, or other parm error */
		if((!isgood) ||	
			(min_per < 0) ||
			(min_horiz < 0.0) ||
			(min_vert < 0.0)) {
			return false;
		}
		newdef.dataset = dataset;
		newdef.min_period = min_per;
		newdef.min_horiz = min_horiz;
		newdef.min_vert = min_vert;

		def = newdef;	/* Replace settings */

		/* Do safe reaversal of listener list (make sure we can handle add/remove during callbacks) */
		ArrayList<UpdateListener> lst;
		synchronized(listeners) {
			lst = new ArrayList<UpdateListener>(listeners);
		}
		for(UpdateListener ul : lst) {
			ul.gpsDefaultsUpdated(newdef);
		}

		return true;
	}
	/**
	 * Get default data set
	 */
	public String getGPSDataSet() {
		return dataset;
	}
	/**
	 * Get min period
	 */
	public int getGPSMinPeriod() {
		return min_period;
	}
	/**
	 * Get min horiz move
	 */
	public double getGPSMinHoriz() {
		return min_horiz;
	}
	/**
	 * Get min vert move
	 */
	public double getGPSMinVert() {
		return min_vert;
	}

	/**
	 * Add update listener	
	 */
	public static void addUpdateListener(UpdateListener l) {
		synchronized(listeners) {
			listeners.add(l);
		}
	}
	/**
	 * Remove update listener
	 */
	public static void removeUpdateListener(UpdateListener l) {
		synchronized(listeners) {
			listeners.remove(l);
		}
	}
}
