package com.rfcode.drivers.readers;

import java.util.Map;

import com.rfcode.drivers.BadParameterException;

/**
 * Sensor definition specific to 0-5V sensors
 *   Source attribute is always 'sensorVoltage' (type double)
 *   Destination attribute is always a double (or null)
 *   
 * @author Mike Primm
 */
public class Voltage0To5SensorDefinition extends SensorDefinition {
	public static final String VOLTAGE_0TO5_DEF = "voltage0to5";
    public static final String VOLTAGE_0TO5_ATTRIB = "sensorVoltage";
    public static final String VAL_0V = "value0V";
    public static final String VAL_5V = "value5V";

    private double val_at_0v;
    private double val_at_5v;
    
    public Voltage0To5SensorDefinition() {
    	super(VOLTAGE_0TO5_DEF);
    	
    	setSourceAttrib(VOLTAGE_0TO5_ATTRIB);
    }
    
    @Override
    public Map<String,Object> getAttributes() {
    	Map<String,Object> map = super.getAttributes();
    	map.put(VAL_0V, val_at_0v);
    	map.put(VAL_5V, val_at_5v);
    	return map;
    }
    
    @Override
    public void setAttributes(Map<String, Object> map) throws BadParameterException {
    	super.setAttributes(map);
    	
    	try {
    		val_at_0v = (Double) map.get(VAL_0V);
    		val_at_5v = (Double) map.get(VAL_5V);
		} catch (Exception x) {
			throw new BadParameterException("Bad value - " + x.getMessage());
		}
    }
    /**
     * Calculate value, given raw value in V
     * 
     * @param rawval - raw V
     * 
     * @return cooked value
     */
    public double getValue(double rawval) {
    	double off = rawval / 5.0;
    	return val_at_0v + (off * (val_at_5v - val_at_0v));
    }
}
