package com.rfcode.drivers.readers.mantis2;

/**
 * Tag type for pseudo-tags for Mantis II readers - causes all mantis-2
 * family readers to appear to have tag with "READER" + last 6 digits of
 * serial/mac as being "visible" to their own channels (for sake of reference
 * tag based location with reader attached to moving asset).
 * 
 * @author Mike Primm
 */
public class MantisIIReaderPseudoTagType extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "readerPseudoTag";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {};
    private static final String[] TAGATTRIBLABS = {};
    /** Our list of default tag attributes */
    private static final Object[] TAGDEFATTRIBS = {};

    /**
     * Constructor
     */
    public MantisIIReaderPseudoTagType() {
        super(99, 'Z', 0);
        setLabel("RF Code Reader Pseudo-Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /*
                                                         * Set tag attribute
                                                         * defaults
                                                         */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag) {
        return false;
    }
}
