package com.rfcode.drivers.readers;

/**
 * Listener interface for subscribing to tag group lifecycle events for all tag
 * groups.
 * 
 * Reports events for tag group creation, deletion
 * 
 * @author Mike Primm
 */
public interface TagGroupLifecycleListener {
    /**
     * TagGroup creation notification - called when new tag group object
     * creation completed (at the end of its init() call).
     * 
     * @param group -
     *            new tag group
     */
    public void tagGroupLifecycleCreate(TagGroup group);
    /**
     * TagGroup deletion notification - called at start of tag group's cleanup()
     * method, when tag group is about to be deleted.
     * 
     * @param group -
     *            tag group object being deleted
     */
    public void tagGroupLifecycleDelete(TagGroup group);
}
