package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Collections;

import com.rfcode.drivers.readers.ReaderChannel;
import com.rfcode.drivers.readers.Tag;
import com.rfcode.drivers.readers.TagLink;
import com.rfcode.drivers.readers.TagLinkLifecycleListener;
import com.rfcode.drivers.readers.TagLinkStatusListener;
import com.rfcode.drivers.SessionFactory;
import com.rfcode.ranger.RangerProcessor;

/**
 * Class implementing the TagLink interface for Mantis-family readers.
 * Represents the viewing of a specific tag by a specific channel of a specific
 * reader.
 * 
 * @author Mike Primm
 */
public class MantisIITagLink implements TagLink {
    /* Table of supported tag link attributes */
    static final String[] TAGLINKATTRIBIDS = {ATTRIB_SSI};
    private static final int ATTRIB_SSI_INDEX = 0;
    private static final Integer ssi_value_na = Integer.valueOf(SSI_VALUE_NA);
    /* Period for timeout processing is 1 second */
    private static final int TIMEOUT_PERIOD = 1000;
    /*
     * Integer value cache - we only use (-40) to (-150) for values, and we have
     * LOTS of these, so this will save us potentially LOTS of Integer objects
     * in memory
     */
    private static Integer[] ssi_cache;
    private static final int CACHE_MIN = -150;
    private static final int CACHE_MAX = -40;
    /* Instance variables */
    private MantisIITag our_tag;
    private MantisIIReaderChannel our_channel;
    private int ssi; /* Current SSI value */
    /*
     * Exception table - cache of Integers, keyed by TagLink, for TagLinks that
     * have started to age-out (had one or more SSI_VALUE_NA reports). Used to
     * avoid burdening instance data with variables that most TagLinks don't
     * need most of the time.
     */
    private static IdentityHashMap<MantisIITagLink, ExceptionState> link_exceptions = new IdentityHashMap<MantisIITagLink, ExceptionState>();

    static {
        ssi_cache = new Integer[CACHE_MAX - CACHE_MIN + 1];
        for (int i = 0; i < CACHE_MAX - CACHE_MIN + 1; i++) {
            ssi_cache[i] = Integer.valueOf(i + CACHE_MIN);
        }
    }
    /**
     * Exception state holder - used for handling variables only needed for
     * TagLinks that are in some form of exception processing, such as a missing
     * SSI age-out
     */
    private static class ExceptionState {
        int ageout_cnt; /* Number of seconds remaining before age-out */
    }

    /**
     * Simple handler object for periodic age-out processing
     */
    private static class OurAgeOutHandler implements Runnable {
        SessionFactory factory;
        /**
         * Method to be run when action is executed by ranger processor thread
         */
        public void run() {
            handleAgeOut(factory);
        }
    }
    private static OurAgeOutHandler age_handler = new OurAgeOutHandler();

    /**
     * Constructor
     * 
     * @param tag -
     *            our tag
     * @param channel -
     *            our channel
     */
    MantisIITagLink(MantisIITag tag, MantisIIReaderChannel channel) {
        our_tag = tag;
        our_channel = channel;
        our_channel.addLink(this);
        ssi = CACHE_MAX;
    }
    /**
     * Cleanup the tag link - NOT responsible for unhooking from Tag
     */
    public void cleanup() {
        stopLinkAgeOut(); /* Drop us from exception table */
        our_channel.removeLink(this);
        our_tag = null;
        our_channel = null;
    }
    /**
     * Get the Tag object associated with this TagLink
     * 
     * @return Tag associated with the link
     */
    public Tag getTag() {
        return our_tag;
    }
    /**
     * Get the MantisIITag object associated with this TagLink
     * 
     * @return MantisIITag associated with the link
     */
    public MantisIITag getMantisIITag() {
        return our_tag;
    }
    /**
     * Get the ReaderChannel object associated with this TagLink
     * 
     * @return ReaderChannel associated with the link
     */
    public ReaderChannel getChannel() {
        return our_channel;
    }
    /**
     * Return values of tag link attributes in provided array. The attributes
     * are indexed in the same order as the attribute IDs returned by the
     * getTagLinkAttributeIDs() call of the ReaderFactory for the Reader
     * observing the tag. This is intended to be a high-performance interface.
     * 
     * @param val -
     *            array of length matching the number of attributes to return
     * @param start -
     *            index (relative to getTagLinkAttributeIDs()) of first
     *            attribute to return (goes in val[0])
     * @param count -
     *            number of consecutive attributes to return
     * @return number of attribute values returned
     */
    public int readTagLinkAttributes(Object[] val, int start, int count) {
        int i, j;
        for (i = start, j = 0; j < count; i++, j++) {
            if (i == ATTRIB_SSI_INDEX) /* If SSI attribute */
                val[j] = getCachedSSI();
            else
                val[j] = null;
        }
        return count;
    }
    /**
     * Read tag link attribute at given index
     * 
     * @param index -
     *            index of attribute to be read
     * @return attribute value, or null if not defined
     */
    public Object readTagLinkAttribute(int index) {
        if (index == ATTRIB_SSI_INDEX)
            return getCachedSSI();
        else
            return null;
    }
    /**
     * Read tag link attribute with given ID
     * 
     * @param id -
     *            attribute ID
     * @return attribute value, or null if not defined
     */
    public Object readTagLinkAttribute(String id) {
        if (id.equals(ATTRIB_SSI)) {
            return getCachedSSI();
        }
        return null;
    }

    /**
     * Get a copy of the curret tag link attributes values
     * 
     * @return map containing copy of attribute values, keyed by attribute ID
     */
    public Map<String, Object> getTagLinkAttributes() {
        return Collections.singletonMap(ATTRIB_SSI, (Object)getCachedSSI());
    }
    /**
     * Set SSI value
     * 
     * @param newssi -
     *            new SSI value in -dbm, or MantisIITagLink.SSI_VALUE_NA if n/a
     * @return true if value changed, false if not
     */
    public boolean setSSI(int newssi) {
        boolean change = false;
        /* Update if changed */
        if (ssi != newssi) {
            ssi = newssi;
            change = true;
        }
        return change;
    }
    /**
     * Report the SSI value - used to indicate that a message was received with
     * this value. This path makes use of the channel ageout to determine if the
     * actual SSI value should be changed when SSI_VALUE_NA is reported (the SSI
     * was missing for the tag message).
     * 
     * @param newssi -
     *            new SSI value in -dbm, or MantisIITagLink.SSI_VALUE_NA if n/a
     * @return true if value changed, false if not
     */
    public boolean reportSSI(int newssi) {
        if (newssi == SSI_VALUE_NA) { /* Are we missing value? */
            /* Start age-out using missing SSI time */
            startLinkAgeOut(our_channel.getMissingSSIAgeOutTime());
            /* Just return without changing value just yet */
            return false;
        } else {
            stopLinkAgeOut();/* If valid value, forget exception */
        }
        return setSSI(newssi);
    }

    /**
     * Get SSI value as cached Integer (keep dynamic allocation to minimum)
     * 
     * @return Integer value of ssi
     */
    private Integer getCachedSSI() {
        if ((ssi >= CACHE_MIN) && (ssi <= CACHE_MAX))
            return ssi_cache[ssi - CACHE_MIN];
        if (ssi == SSI_VALUE_NA)
            return ssi_value_na;
        return Integer.valueOf(ssi);
    }
    /**
     * Get current SSI value
     * 
     * @return SSI in -dbm, or SSI_VALUE_NA if undefined/missing
     */
    public int getSSI() {
        return ssi;
    }
    /**
     * Test if link is lost or disconnected
     * 
     * @return true if link to tag is lost, false if not
     */
    public boolean isLinkLost() {
        return (ssi == SSI_VALUE_NA);
    }
    /**
     * Get attribute count
     * 
     * @return number of attributes
     */
    public int getTagLinkAttributeCount() {
        return TAGLINKATTRIBIDS.length;
    }
    /**
     * Report tag link attribute change
     */
    void reportTagLinkAttributeChange() {
        for (TagLinkLifecycleListener lsnr : our_channel.getTagLinkListeners()) {
            if (lsnr instanceof TagLinkStatusListener) {
                ((TagLinkStatusListener) lsnr)
                    .tagLinkStatusAttributeChange(this);
            }
        }
    }

    /**
     * Handle periodic age-out processing
     */
    private static void handleAgeOut(SessionFactory factory) {
        ArrayList<MantisIITagLink> tosslist = null;
        /* Loop through the exceptions */
        for (Map.Entry<MantisIITagLink, ExceptionState> rec : link_exceptions
            .entrySet()) {
            ExceptionState es = rec.getValue();
            es.ageout_cnt--; /* Drop age count */
            if (es.ageout_cnt <= 0) { /* Done aging? */
                /* Add to list to toss (don't mess with iterator now) */
                if (tosslist == null)
                    tosslist = new ArrayList<MantisIITagLink>();
                tosslist.add(rec.getKey());
            }
        }
        /* If we tossed anyone, do it now */
        if (tosslist != null) {
            for (MantisIITagLink tl : tosslist) {
                tl.setSSI(SSI_VALUE_NA); /* Clear SSI */
                tl.our_tag.insertTagLink(tl.our_channel, null);
            }
        }
        /* If not empty now, re-enqueue timeout processing */
        if (link_exceptions.isEmpty() == false) {
            RangerProcessor.addDelayedAction(age_handler, TIMEOUT_PERIOD);
        }
    }
    /**
     * Start link age-out, if not already started
     * 
     * @param t_o -
     *            timeout in seconds
     */
    void startLinkAgeOut(int t_o) {
        ExceptionState es = link_exceptions.get(this);
        if (es == null) { /* If first time, initialize it */
            es = new ExceptionState();
            es.ageout_cnt = t_o;
            if (link_exceptions.isEmpty()) { /* We first exception? */
                age_handler.factory = our_channel.getSessionFactory();
                /* Add delayed action for our age-out handler */
                RangerProcessor.addDelayedAction(age_handler, TIMEOUT_PERIOD);
            }
            link_exceptions.put(this, es);
        } else if (es.ageout_cnt > t_o) { /*
                                             * If shorter, shorten time
                                             * remaining
                                             */
            es.ageout_cnt = t_o;
        }
    }
    /**
     * Stop link age-out, if started
     */
    void stopLinkAgeOut() {
        link_exceptions.remove(this);
    }
}
