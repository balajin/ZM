package com.rfcode.drivers.readers;

/**
 * Tag lifecycle listener - used for reporting tag creation and deletion (lost)
 * events.
 * 
 * @author Mike Primm
 */
public interface TagLifecycleListener {
    /**
     * Tag created notification. Called after tag has been created, but before
     * its TagLinks have been added (i.e. after end of init()). Any initial
     * attributes from creating message will have been added by the time this is
     * called.
     * 
     * @param tag -
     *            tag being created
     * @param cause -
     *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
     */
    public void tagLifecycleCreate(Tag tag, String cause);
    /**
     * Tag deleted notification. Called at start of tag cleanup(), when tag is
     * about to be deleted (generally due to having no active TagLinks).
     * 
     * @param tag -
     *            tag being deleted
     */
    public void tagLifecycleDelete(Tag tag);
}
