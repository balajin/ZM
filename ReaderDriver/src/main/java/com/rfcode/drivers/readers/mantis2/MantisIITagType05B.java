package com.rfcode.drivers.readers.mantis2;
import java.util.Map;

/**
 * Tag type for treatment 04D tags - Mantis II Tags - Treatment 04D (temperature)
 * 
 * @author Mike Primm
 */
public class MantisIITagType05B extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis05B";

	/** Tag type attribute - temperature rounding increment */
	public static final String TAGTYPEATTRIB_TMPROUND = "tmpround";
    /* Default rounding increment */
    public static final double TMPROUND_DEFAULT = 0.5;

    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.TEMPERATURE_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB,
		MantisIITagType.SENSORDISCONNECT_ATTRIB
        };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.TEMPERATURE_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL,
		MantisIITagType.SENSORDISCONNECT_ATTRIB_LABEL
        };
    private static final int TEMPERATURE_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int LOWBATT_ATTRIB_INDEX = 1; /*
                                                             * Index in list of
                                                             * attribute
                                                             */

	private static final int SENSORDISCONNECT_ATTRIB_INDEX = 2;
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null,
        Boolean.FALSE, Boolean.FALSE};

	private static final double MAX_UNVERIFIED_TMP_DELTA = 2.0;	/* 2C max change without verify */

	/**
     * Constructor
     */
    public MantisIITagType05B() {
        super(5, 'B', 0, new String[] { TAGTYPEATTRIB_TMPROUND } );
        setLabel("RF Code Mantis II Tag - Treatment 05B");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */

		Map<String, Object> defs = getTagGroupDefaultAttributes(); /* Get defaults */
		defs.put(TAGTYPEATTRIB_TMPROUND, Double.valueOf(TMPROUND_DEFAULT));	/* Default to 0.5 */
		setTagGroupDefaultAttributes(defs);
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;

		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

		boolean unplugged = false;
		// Get raw temp data
        int v = (cur_payload & 0x3FFC0) >> 6;
        if(v > 0x7FF) { /* If negative, sign extend */
        	if (v == 0x800)
        		unplugged = true;
        	v |= (-1 - 0xFFF);
        }
        if (unplugged) {
        	change = updateNull(tag, TEMPERATURE_ATTRIB_INDEX) || change;
        }
        else {
        	/* Compute temperature */
        	double tmp = 0.0625 * (double)v;
        	/* Apply rounding, if defined */
        	Object tc = tag.getTagGroup().getTagGroupAttributes().get(TAGTYPEATTRIB_TMPROUND);
        	double rnd = TMPROUND_DEFAULT;
        	if(tc instanceof Double) {
        		rnd = ((Double)tc).doubleValue();
        	}
        	if(rnd > 0.0)
        		tmp = Math.rint(tmp / rnd) * rnd;
    		change = updateDoubleVerify(tag, TEMPERATURE_ATTRIB_INDEX, tmp, verify,
    				MAX_UNVERIFIED_TMP_DELTA) || change;
        }
		change = updateBooleanVerify(tag, SENSORDISCONNECT_ATTRIB_INDEX, unplugged, verify && unplugged) || change;
		
        /* Get low battery value - verify on setting flag */
		boolean vv = ((cur_payload & 0x02) != 0);
        change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
		
        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}
}
