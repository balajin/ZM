package com.rfcode.drivers.readers.mantis2;

/**
 * PDU feed-line-specific data tag subtype
 * 
 * @author Mike Primm
 */
public class PduFeedLineTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduFeedLine";

	/* Tag attribute - feed line amperage (amps) (double) */
	public static final String PDUAMPS_ATTRIB = "feedAmps";
	public static final String PDUAMPS_ATTRIB_LABEL = "Amperage";

	/* Tag attribute - feed line configuration (string) */
	public static final String PDUCONFIG_ATTRIB = "feedConfig";
	public static final String PDUCONFIG_ATTRIB_LABEL = "Line ID";

	/* Tag attribute - feed line overload state (boolean) */
	public static final String PDUOVERLOAD_ATTRIB = "feedOverload";
	public static final String PDUOVERLOAD_ATTRIB_LABEL = "Line Overload";

	/* Tag attribute - feed line load warning state (boolean) */
	public static final String PDULOADWARNING_ATTRIB = "feedLoadWarning";
	public static final String PDULOADWARNING_ATTRIB_LABEL = "Line Load Warning";

	public static final int PDUAMPS_INDEX = 2;
	public static final int PDUCONFIG_INDEX = 3;
	public static final int PDUFEEDSETID_INDEX = 4;
	public static final int PDUTOWERID_INDEX = 5;
	public static final int PDUOVERLOAD_INDEX = 6;
	public static final int PDULOADWARNING_INDEX = 7;

	public static final Double VALUE_NA = Double.valueOf(-1.0);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, PDUAMPS_ATTRIB, 
		PDUCONFIG_ATTRIB, MantisIITagType.PDUFEEDSETID_ATTRIB, MantisIITagType.PDUTOWERID_ATTRIB, 
		PDUOVERLOAD_ATTRIB, PDULOADWARNING_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, PDUAMPS_ATTRIB_LABEL,
		PDUCONFIG_ATTRIB_LABEL, MantisIITagType.PDUFEEDSETID_ATTRIB, MantisIITagType.PDUTOWERID_ATTRIB_LABEL,
		PDUOVERLOAD_ATTRIB_LABEL, PDULOADWARNING_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null, 
        null, null, null, null, null };
    /**
     * Constructor
     */
    public PduFeedLineTagSubType() {
        setLabel("PDU Feed Line Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }

	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double amps, String config, Long towerid, Boolean overload, 
		Boolean loadwarn, Long feedsetid) {
		if(tag == null)
			return;
		int[] idx = new int[6];
		Object[] val = new Object[6];
		int cnt = 0;
		if(amps != null) {
			idx[cnt] = PDUAMPS_INDEX;
			if(amps.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = amps;
			cnt++;
		}
		if(config != null) {
			idx[cnt] = PDUCONFIG_INDEX;
			val[cnt] = config;
			cnt++;
		}
		if(feedsetid != null) {
			idx[cnt] = PDUFEEDSETID_INDEX;
			val[cnt] = feedsetid;
			cnt++;
		}
		if(towerid != null) {
			idx[cnt] = PDUTOWERID_INDEX;
			val[cnt] = towerid;
			cnt++;
		}
		if(overload != null) {
			idx[cnt] = PDUOVERLOAD_INDEX;
			val[cnt] = overload;
			cnt++;
		}
		if(loadwarn != null) {
			idx[cnt] = PDULOADWARNING_INDEX;
			val[cnt] = loadwarn;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}

}
