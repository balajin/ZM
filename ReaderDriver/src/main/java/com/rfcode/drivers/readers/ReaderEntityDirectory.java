package com.rfcode.drivers.readers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.Set;
import java.util.ArrayList;
import com.rfcode.drivers.locationrules.LocationRule;
import com.rfcode.drivers.locationrules.LocationRuleFactory;
import com.rfcode.drivers.locationrules.Location;
import com.rfcode.drivers.locationrules.LocationRuleException;
import com.rfcode.util.keys.LicenseKey;
import com.rfcode.ranger.RangerServer;
import com.rfcode.spm.SPMReader;

/**
 * Simple directory object for managing location of and registration of named
 * objects, such as Readers, ReaderChannels, TagGroups, TagTypes and Tags.
 * 
 * @author Mike Primm
 */
public class ReaderEntityDirectory {
    /** Base hashmap of all objects in directory, keyed by name */
    private static final HashMap<String, ReaderEntity> objectdir = new HashMap<String, ReaderEntity>();
    /** Hashmap for readers, keyed by name */
    private static final HashMap<String, Reader> readerdir = new HashMap<String, Reader>();
    /** Hashmap for reader channels, keyed by name */
    private static final HashMap<String, ReaderChannel> channeldir = new HashMap<String, ReaderChannel>();
    /** Hashmap for reader factory, keyed by name */
    private static final HashMap<String, ReaderFactory> factorydir = new HashMap<String, ReaderFactory>();
    /** Hashmap for tag groups, keyed by name */
    private static final HashMap<String, TagGroup> groupdir = new LinkedHashMap<String, TagGroup>();
    /** Hashmap for tag types, keyed by name */
    private static final HashMap<String, TagType> typedir = new HashMap<String, TagType>();
    /** Hashmap for location rules, keyed by name */
    private static final HashMap<String, LocationRule> ruledir = new HashMap<String, LocationRule>();
    /** Hashmap for location rule factories, keyed by name */
    private static final HashMap<String, LocationRuleFactory> rulefdir = new HashMap<String, LocationRuleFactory>();
    /** Hashmap for locations, keyed by name */
    private static final HashMap<String, Location> locdir = new HashMap<String, Location>();
    /** Hashmap for GPS, keyed by name */
    private static final HashMap<String, GPS> gpsdir = new HashMap<String, GPS>();
    /** Hashmap for rule exceptions, keyed by name */
    private static final HashMap<String, LocationRuleException> ruleexceptdir = new HashMap<String, LocationRuleException>();
    /** Hashmap for sensor definition, keyed by name */
    private static final HashMap<String, SensorDefinition> sensordefdir = new HashMap<String, SensorDefinition>();
    /** Active reader count - for licensing */
    private static int active_rdr_cnt;
    /** Active reader limit - for licensing */
    private static int active_rdr_limit = 0;
    /** Active SPM count - for licensing */
    private static int active_spm_cnt;
    /** Active SPM limit - for licensing */
    private static int active_spm_limit = 0;
    /** Hashmap for license keys, keyed by keystring */
    private static final HashMap<String, LicenseKey> keys = new HashMap<String, LicenseKey>();
    /**
     * Find entity by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found
     */
    public static ReaderEntity findEntity(String id) {
        return objectdir.get(id);
    }
    /**
     * Find reader object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not reader
     */
    public static Reader findReader(String id) {
        return readerdir.get(id);
    }
    /**
     * Find GPS object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not reader
     */
    public static GPS findGPS(String id) {
        return gpsdir.get(id);
    }
    /**
     * Find reader channel object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not reader channel
     */
    public static ReaderChannel findReaderChannel(String id) {
        return channeldir.get(id);
    }
    /**
     * Find reader factory object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not reader factory
     */
    public static ReaderFactory findReaderFactory(String id) {
        return factorydir.get(id);
    }
    /**
     * Find tag type object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag type
     */
    public static TagType findTagType(String id) {
        return typedir.get(id);
    }
    /**
     * Find location rule object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag type
     */
    public static LocationRule findLocationRule(String id) {
        return ruledir.get(id);
    }
    /**
     * Find location rule factory object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag type
     */
    public static LocationRuleFactory findLocationRuleFactory(String id) {
        return rulefdir.get(id);
    }
    /**
     * Find location object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag type
     */
    public static Location findLocation(String id) {
        return locdir.get(id);
    }
    /**
     * Find tag group object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag group
     */
    public static TagGroup findTagGroup(String id) {
        return groupdir.get(id);
    }
    /**
     * Find location rule exception object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not tag type
     */
    public static LocationRuleException findLocationRuleException(String id) {
        return ruleexceptdir.get(id);
    }
    /**
     * Find sensor definition object by ID
     * 
     * @param id -
     *            ID of object
     * @return object found, or null if not found or not sensor definition
     */
    public static SensorDefinition findSensorDefinition(String id) {
        return sensordefdir.get(id);
    }
    /**
     * Test to see if ID is unused - throw exception if in use
     * 
     * @param id -
     *            ID to test
     * @throws DuplicateEntityIDException
     *             if matches existing ID
     */
    public static void testID(String id) throws DuplicateEntityIDException {
        if (objectdir.containsKey(id))
            throw new DuplicateEntityIDException(id);
    }
    /**
     * Add entity to directory
     * 
     * @param obj -
     *            entity to add
     * @throws DuplicateEntityIDException
     *             if ID in use
     */
    public static void addEntity(ReaderEntity obj)
        throws DuplicateEntityIDException {
        String id = obj.getID();
        ReaderEntity oldval = objectdir.put(id, obj); /* Insert it */
        if (oldval != null) { /* Value was there */
            objectdir.put(id, oldval); /* Put it back */
            throw new DuplicateEntityIDException(id); /* And throw exception */
        }
        if (obj instanceof Reader) { /* If reader, add to reader catalog */
            readerdir.put(id, (Reader) obj);
        } else if (obj instanceof ReaderChannel) { /*
                                                     * If channel, add to
                                                     * channel catalog
                                                     */
            channeldir.put(id, (ReaderChannel) obj);
        } else if (obj instanceof TagGroup) { /* If group add to list */
            groupdir.put(id, (TagGroup) obj);
        } else if (obj instanceof TagType) { /* If type add to list */
            typedir.put(id, (TagType) obj);
        } else if (obj instanceof ReaderFactory) { /* If factory add to list */
            factorydir.put(id, (ReaderFactory) obj);
        } else if (obj instanceof LocationRule) { /* If location rule */
            ruledir.put(id, (LocationRule) obj);
        } else if (obj instanceof LocationRuleFactory) { /* If location rule */
            rulefdir.put(id, (LocationRuleFactory) obj);
        } else if (obj instanceof Location) { /* If location */
            locdir.put(id, (Location) obj);
        } else if (obj instanceof GPS) {
			gpsdir.put(id, (GPS)obj);
		} else if (obj instanceof LocationRuleException) {
            ruleexceptdir.put(id, (LocationRuleException)obj);
		} else if (obj instanceof SensorDefinition) {
            sensordefdir.put(id, (SensorDefinition)obj);
        }
    }
    /**
     * Remove entity from directory
     * 
     * @param obj -
     *            entity to add
     */
    public static void removeEntity(ReaderEntity obj) {
        String id = obj.getID();
        objectdir.remove(id); /* Remove from directory */
        if (obj instanceof Reader) {
            readerdir.remove(id);
        } else if (obj instanceof ReaderChannel) {
            channeldir.remove(id);
        } else if (obj instanceof TagGroup) {
            groupdir.remove(id);
        } else if (obj instanceof TagType) {
            typedir.remove(id);
        } else if (obj instanceof ReaderFactory) {
            factorydir.remove(id);
        } else if (obj instanceof LocationRule) {
            ruledir.remove(id);
        } else if (obj instanceof LocationRuleFactory) {
            rulefdir.remove(id);
        } else if (obj instanceof Location) {
            locdir.remove(id);
        } else if (obj instanceof GPS) {
			gpsdir.remove(id);
		} else if (obj instanceof LocationRuleException) {
            ruleexceptdir.remove(id);
		} else if (obj instanceof SensorDefinition) {
            sensordefdir.remove(id);
        }
    }
    /**
     * Get all readers
     * 
     * @return Collection of reader
     */
    public static Collection<Reader> getReaders() {
        return readerdir.values();
    }
    /**
     * Get all reader IDs
     * 
     * @return Set of reader IDs
     */
    public static Set<String> getReaderIDs() {
        return readerdir.keySet();
    }
    /**
     * Get all GPS
     * 
     * @return Collection of GPS
     */
    public static Collection<GPS> getGPSs() {
        return gpsdir.values();
    }
    /**
     * Get all reader IDs
     * 
     * @return Set of reader IDs
     */
    public static Set<String> getGPSIDs() {
        return gpsdir.keySet();
    }
    /**
     * Get all reader channels
     * 
     * @return Collection of reader channels
     */
    public static Collection<ReaderChannel> getReaderChannels() {
        return channeldir.values();
    }
    /**
     * Get all reader channel IDs
     * 
     * @return Set of reader channel IDs
     */
    public static Set<String> getReaderChannelIDs() {
        return channeldir.keySet();
    }
    /**
     * Get all tag groups
     * 
     * @return Collection of tag groups
     */
    public static Collection<TagGroup> getTagGroups() {
        return groupdir.values();
    }
    /**
     * Get all tag group IDs
     * 
     * @return Set of reader channel IDs
     */
    public static Set<String> getTagGroupIDs() {
        return groupdir.keySet();
    }
    /**
     * Get all tag types
     * 
     * @return Collection of tag types
     */
    public static Collection<TagType> getTagTypes() {
        return typedir.values();
    }
    /**
     * Get all tag type IDs
     * 
     * @return Set of tag type IDs
     */
    public static Set<String> getTagTypeIDs() {
        return typedir.keySet();
    }
    /**
     * Get all reader factories
     * 
     * @return Collection of reader factories
     */
    public static Collection<ReaderFactory> getReaderFactories() {
        return factorydir.values();
    }
    /**
     * Get all reader factory IDs
     * 
     * @return Set of reader factory IDs
     */
    public static Set<String> getReaderFactoryIDs() {
        return factorydir.keySet();
    }
    /**
     * Get all location rules
     * 
     * @return Collection of location rules
     */
    public static Collection<LocationRule> getLocationRules() {
        return ruledir.values();
    }
    /**
     * Get all location rule IDs
     * 
     * @return Set of location rule IDs
     */
    public static Set<String> getLocationRuleIDs() {
        return ruledir.keySet();
    }
    /**
     * Get all location rule exceptions
     * 
     * @return Collection of location rule exceptions
     */
    public static Collection<LocationRuleException> getLocationRuleExceptions() {
        return ruleexceptdir.values();
    }
    /**
     * Get all location rule exception IDs
     * 
     * @return Set of location rule exception IDs
     */
    public static Set<String> getLocationRuleExceptionIDs() {
        return ruleexceptdir.keySet();
    }
    /**
     * Get all sensor definitions
     * 
     * @return Collection of sensor definitions
     */
    public static Collection<SensorDefinition> getSensorDefinitions() {
        return sensordefdir.values();
    }
    /**
     * Get all sensor definition IDs
     * 
     * @return Set of sensor definition IDs
     */
    public static Set<String> getSensorDefinitionIDs() {
        return sensordefdir.keySet();
    }
    /**
     * Get all location rule factories
     * 
     * @return Collection of location rule factories
     */
    public static Collection<LocationRuleFactory> getLocationRuleFactories() {
        return rulefdir.values();
    }
    /**
     * Get all location rule factory IDs
     * 
     * @return Set of location rule factory IDs
     */
    public static Set<String> getLocationRuleFactoryIDs() {
        return rulefdir.keySet();
    }
    /**
     * Get all location IDs
     * 
     * @return Set of location IDs
     */
    public static Set<String> getLocationIDs() {
        return locdir.keySet();
    }
    /**
     * Get all locations
     * 
     * @return Collection of locations
     */
    public static Collection<Location> getLocations() {
        return locdir.values();
    }
    /**
     * Activate/deactivate reader
     * @param to_active - true if activate, false if not
     * @return true if allowed, false if not
     */
    public static boolean activateReader(boolean to_active) {
        if(to_active) {
            /* If limit AND we're at/above it */
            if(active_rdr_cnt >= active_rdr_limit) {
                return false;   /* Not allowed */
            }
            active_rdr_cnt++;
        }
        else {
            active_rdr_cnt--;
        }
        return true;
    }
    /**
     * Activate/deactivate SPM
     * @param to_active - true if activate, false if not
     * @return true if allowed, false if not
     */
    public static boolean activateSPM(boolean to_active) {
        if(to_active) {
            /* If limit AND we're at/above it */
            if(active_spm_cnt >= active_spm_limit) {
                return false;   /* Not allowed */
            }
            active_spm_cnt++;
        }
        else {
            active_spm_cnt--;
        }
        return true;
    }
    /**
     * Set active reader limit
     * @param cnt - max count (Integer.MAX_VALUE is no limit)
     */
    public static void setActiveReaderLimit(int cnt) {
        active_rdr_limit = cnt;
    }
    /**
     * Get active reader limit
     * @return max count (Integer.MAX_VALUE is no limit)
     */
    public static int getActiveReaderLimit() {
        return active_rdr_limit;
    }
    /**
     * Get active reader count
     * @return current count
     */
    public static int getActiveReaderCount() {
        return active_rdr_cnt;
    }
    /**
     * Get active SPM count
     * @return current count
     */
    public static int getActiveSPMCount() {
        return active_spm_cnt;
    }
    /**
     * Set active SPM limit
     * @param cnt - max count (Integer.MAX_VALUE is no limit)
     */
    public static void setActiveSPMLimit(int cnt) {
        active_spm_limit = cnt;
    }
    /**
     * Get active SPM limit
     * @return max count (Integer.MAX_VALUE is no limit)
     */
    public static int getActiveSPMLimit() {
        return active_spm_limit;
    }
    /**
     * Add license key to active set
     * @param keystr - key to add
     */
    public static void addLicenseKey(String keystr) {
        LicenseKey k = new LicenseKey(keystr);
        /* If key is valid, non-expired, and for zone manager,
         * and not a duplicate */
        if(k.isKeyValid() && (!k.isKeyExpired()) &&
            ((k.getProductID() == LicenseKey.PROD_ZONEMGR) ||
    		 (k.getProductID() == LicenseKey.PROD_ZONEMGR_SPM)) &&
            (!keys.containsKey(k.getKeyString()))) {
            keys.put(k.getKeyString(), k);
            /* Recompute count */
            updateKeyCount();
        }
    }
    /**
     * Remove license key from active set
     * @param keystr - key string, or "*" for all keys
     */
    public static void removeLicenseKey(String keystr) {
        /* If remove all, drop all keys */
        if(keystr.equals("*")) {
            if(keys.size() > 0) {
                keys.clear();
                updateKeyCount();
            }
        }
        else {
            /* Decode key so we can get normalized ID */
            LicenseKey lk = new LicenseKey(keystr);
        
            LicenseKey k = keys.remove(lk.getKeyString());
            if(k != null) {
                /* Recompute count */
                updateKeyCount();
            }
        }
    }
    /**
     * Recompute license count
     */
    public static void updateKeyCount() {
        int cnt = 0;
        int spmcnt = 0;
        ArrayList<LicenseKey> klist = new ArrayList<LicenseKey>(keys.values());
        for(int i = 0; i < klist.size(); i++) {
            LicenseKey k = klist.get(i);
            if(!k.isKeyExpired()) {
                if(k.getLicenseCount() == LicenseKey.COUNT_UNLIMITED) {
                	if (k.getProductID() == LicenseKey.PROD_ZONEMGR_SPM) {
                		spmcnt = Integer.MAX_VALUE;
                	}
                	else if (k.getProductID() == LicenseKey.PROD_ZONEMGR) {
                		cnt = Integer.MAX_VALUE;
                	}
                }
                else {
                	if (k.getProductID() == LicenseKey.PROD_ZONEMGR_SPM && spmcnt != Integer.MAX_VALUE) {
                		spmcnt += k.getLicenseCount();
                	}
                	else if (k.getProductID() == LicenseKey.PROD_ZONEMGR && cnt != Integer.MAX_VALUE) {
                		cnt += k.getLicenseCount();
                	}
                }
            }
        }
        active_rdr_limit = cnt;
        active_spm_limit = spmcnt;
        /* If too many active, need to deactivate some */
        if (cnt < active_rdr_cnt) {
            ArrayList<Reader> rlist = new ArrayList<Reader>(getReaders());
            /* Walk list until we have enough disabled */
            for(int i = 0; i < rlist.size(); i++) {
                Reader rdr = rlist.get(i);
                if (rdr instanceof SPMReader) continue;
                if(rdr.getReaderEnabled()) {    /* If enabled */
                    /* Release license and disable it */
                    if(rdr.setReaderLicense(false)) {
                        rdr.setReaderEnabled(false);
                        RangerServer.saveReader(rdr);
                    }
                }
                /* If enough disabled, quit loop */
                if(active_rdr_cnt <= active_rdr_limit) {
                    break;
                }
            }
        }
        /* If too many active, need to deactivate some */
        if (cnt < active_spm_cnt) {
            ArrayList<Reader> rlist = new ArrayList<Reader>(getReaders());
            /* Walk list until we have enough disabled */
            for(int i = 0; i < rlist.size(); i++) {
                Reader rdr = rlist.get(i);
                if (!(rdr instanceof SPMReader)) continue;
                if(rdr.getReaderEnabled()) {    /* If enabled */
                    /* Release license and disable it */
                    if(rdr.setReaderLicense(false)) {
                        rdr.setReaderEnabled(false);
                        RangerServer.saveReader(rdr);
                    }
                }
                /* If enough disabled, quit loop */
                if(active_spm_cnt <= active_spm_limit) {
                    break;
                }
            }
        }
    }
    /**
     * Get all license keys
     * 
     * @return Collection of keys
     */
    public static Collection<LicenseKey> getLicenseKeys() {
        return new ArrayList<LicenseKey>(keys.values());
    }
}
