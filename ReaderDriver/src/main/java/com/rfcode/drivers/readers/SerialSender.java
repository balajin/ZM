package com.rfcode.drivers.readers;

/**
 * Serial sender interface.  Used to handle 'serialsend' commands.
 * 
 * @author Mike Primm
 */
public interface SerialSender {
    /**
     * Send string to serial device
     */
    public void sendSerialMessage(String msg);
}
