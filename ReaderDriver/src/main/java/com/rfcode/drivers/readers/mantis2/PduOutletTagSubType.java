package com.rfcode.drivers.readers.mantis2;

/**
 * PDU outlet-specific data tag subtype
 * 
 * @author Mike Primm
 */
public class PduOutletTagSubType extends TagSubType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "pduOutlet";

	/* Tag attribute - phase-to-neutral RMS voltage (double) */
	public static final String PDUVOLTS_ATTRIB = "outVolts";
	public static final String PDUVOLTS_ATTRIB_LABEL = "Volts";

	/* Tag attribute - outlet true power (watts) (double) */
	public static final String PDUTRUEPOWER_ATTRIB = "outTruePower";
	public static final String PDUTRUEPOWER_ATTRIB_LABEL = "Active Power";

	/* Tag attribute - outlet power factor (%) (double) */
	public static final String PDUPWRFACTOR_ATTRIB = "outPwrFactor";
	public static final String PDUPWRFACTOR_ATTRIB_LABEL = "Power Factor (%)";

	/* Tag attribute - outlet apparent power (VA) (double) */
	public static final String PDUAPPPOWER_ATTRIB = "outAppPower";
	public static final String PDUAPPPOWER_ATTRIB_LABEL = "Apparent Power";

	/* Tag attribute - outlet amperage (amps) (double) */
	public static final String PDUAMPS_ATTRIB = "outAmps";
	public static final String PDUAMPS_ATTRIB_LABEL = "Amperage";

	/* Tag attribute - outlet configuration (string) */
	public static final String PDUCONFIG_ATTRIB = "outConfig";
	public static final String PDUCONFIG_ATTRIB_LABEL = "Outlet Configuration";

	/* Tag attribute - phase watt-hours (double) */
	public static final String PDUWATTHOURS_ATTRIB = "outTotalWH";
	public static final String PDUWATTHOURS_ATTRIB_LABEL = "Total True Power (W-h)";

	/* Tag attribute - phase volt-amp-hours (double) */
	public static final String PDUAPPPWRHOURS_ATTRIB = "outTotalVAH";
	public static final String PDUAPPPWRHOURS_ATTRIB_LABEL = "Total Apparent Power (VA-h)";

	/* Tag attribute - phase watt-hours start time (UTC milliseconds) (double) */
	public static final String PDUWATTHOURSTS_ATTRIB = "outTotalPwrTS";
	public static final String PDUWATTHOURSTS_ATTRIB_LABEL = "Total Power Start Time";

	/* Tag attribute - outlet label (string) */
	public static final String PDULABEL_ATTRIB = "outLabel";
	public static final String PDULABEL_ATTRIB_LABEL = "Outlet Label";

	/* Tag attribute - outlet switch (boolean) */
	public static final String PDUSWITCH_ATTRIB = "outSwitch";
	public static final String PDUSWITCH_ATTRIB_LABEL = "Outlet Switch";

	/* Tag attribute - outlet bank ID (int) */
	public static final String PDUBANKID_ATTRIB = "outBank";
	public static final String PDUBANKID_ATTRIB_LABEL = "Outlet Bank";

	public static final int	SUBINDEX_INDEX = 0;
	public static final int PARENTTAG_INDEX = 1;
	public static final int PDUVOLTS_INDEX = 2;
	public static final int PDUTRUEPOWER_INDEX = 3;
	public static final int PDUPWRFACTOR_INDEX = 4;
	public static final int PDUAPPPOWER_INDEX = 5;
	public static final int PDUAMPS_INDEX = 6;
	public static final int PDUCONFIG_INDEX = 7;
	public static final int PDUFEEDSETID_INDEX = 8;
	public static final int PDUTOWERID_INDEX = 9;
	public static final int PDUWATTHOURS_INDEX = 10;
	public static final int PDUAPPPWRHOURS_INDEX = 11;
	public static final int PDUWATTHOURSTS_INDEX = 12;
	public static final int PDULABEL_INDEX = 13;
	public static final int PDUSWITCH_INDEX = 14;
	public static final int PDUBANKID_INDEX = 15;

	public static final Double VALUE_NA = Double.valueOf(-1.0);
	public static final Long TS_VALUE_NA = Long.valueOf(-1);

	private static final String[] TAGATTRIBS = { SUBINDEX_ATTRIB, PARENTTAG_ATTRIB, PDUVOLTS_ATTRIB, 
		PDUTRUEPOWER_ATTRIB, PDUPWRFACTOR_ATTRIB,
		PDUAPPPOWER_ATTRIB, PDUAMPS_ATTRIB, PDUCONFIG_ATTRIB, 
		MantisIITagType.PDUFEEDSETID_ATTRIB, MantisIITagType.PDUTOWERID_ATTRIB, 
		PDUWATTHOURS_ATTRIB, PDUAPPPWRHOURS_ATTRIB, PDUWATTHOURSTS_ATTRIB,
		PDULABEL_ATTRIB, PDUSWITCH_ATTRIB, PDUBANKID_ATTRIB };
	private static final String[] TAGATTRIBLABS = { SUBINDEX_ATTRIB_LABEL, PARENTTAG_ATTRIB_LABEL, PDUVOLTS_ATTRIB_LABEL, 
		PDUTRUEPOWER_ATTRIB_LABEL, PDUPWRFACTOR_ATTRIB_LABEL,
		PDUAPPPOWER_ATTRIB_LABEL, PDUAMPS_ATTRIB_LABEL, PDUCONFIG_ATTRIB_LABEL, 
		MantisIITagType.PDUFEEDSETID_ATTRIB_LABEL, MantisIITagType.PDUTOWERID_ATTRIB_LABEL, 
		PDUWATTHOURS_ATTRIB_LABEL, PDUAPPPWRHOURS_ATTRIB_LABEL, PDUWATTHOURSTS_ATTRIB_LABEL,
		PDULABEL_ATTRIB_LABEL, PDUSWITCH_ATTRIB_LABEL, PDUBANKID_ATTRIB_LABEL };
	private static final Object[] TAGDEFATTRIBS = { null, null, null, 
		null, null,
		null, null, null,
		null, null,
		null, null, null,
		null, null, null };
    /**
     * Constructor
     */
    public PduOutletTagSubType() {
        setLabel("PDU Outlet Data Tag");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
	/**
	 * Update subtag with provided values
	 */
	public static void updateTag(SubTag tag, Double volts,
		Double truepower, Double powfact, Double apppower, Double amps, String config,
		Double tot_wh, Long wh_ts, Double tot_vah) {
		updateTag(tag, volts, truepower, powfact, apppower, amps, config, tot_wh, wh_ts, tot_vah, 
			null, null, null, null, null);
	}
	public static void updateTag(SubTag tag, Double volts,
		Double truepower, Double powfact, Double apppower, Double amps, String config,
		Double tot_wh, Long wh_ts, Double tot_vah, Long towerid, String outlabel, Long feedsetid) {
		updateTag(tag, volts, truepower, powfact, apppower, amps, config, tot_wh, wh_ts, tot_vah, 
			towerid, outlabel, feedsetid, null, null);
	}
	public static void updateTag(SubTag tag, Double volts,
		Double truepower, Double powfact, Double apppower, Double amps, String config,
		Double tot_wh, Long wh_ts, Double tot_vah, Long towerid, String outlabel, Long feedsetid,
		Boolean sw) {
		updateTag(tag, volts, truepower, powfact, apppower, amps, config, tot_wh, wh_ts, tot_vah, 
			towerid, outlabel, feedsetid, sw, null);
	}
	public static void updateTag(SubTag tag, Double volts,
		Double truepower, Double powfact, Double apppower, Double amps, String config,
		Double tot_wh, Long wh_ts, Double tot_vah, Long towerid, String outlabel, Long feedsetid,
		Boolean sw, Integer bankid) {
		if(tag == null)
			return;
		int[] idx = new int[15];
		Object[] val = new Object[15];
		int cnt = 0;
		if(volts != null) {
			idx[cnt] = PDUVOLTS_INDEX;
			if(volts.doubleValue() < 0.0)
				val[cnt] = null;
			else
				val[cnt] = volts;
			cnt++;
		}
		if(truepower != null) {
			idx[cnt] = PDUTRUEPOWER_INDEX;
			if(truepower.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = truepower;
			cnt++;
		}
		if(powfact != null) {
			idx[cnt] = PDUPWRFACTOR_INDEX;
			if(powfact.doubleValue() < 0.0) {	/* Invalid? */
				val[cnt] = null;	/* Set to N/A */
			}
			else {
				val[cnt] = powfact;
			}
			cnt++;
		}
		if(apppower != null) {
			idx[cnt] = PDUAPPPOWER_INDEX;
			if(apppower.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = apppower;
			cnt++;
		}
		if(amps != null) {
			idx[cnt] = PDUAMPS_INDEX;
			if(amps.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = amps;
			cnt++;
		}
		if(config != null) {
			idx[cnt] = PDUCONFIG_INDEX;
			val[cnt] = config;
			cnt++;
		}
		if(tot_wh != null) {
			idx[cnt] = PDUWATTHOURS_INDEX;
			if(tot_wh.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = tot_wh;
			cnt++;
		}
		if(wh_ts != null) {
			idx[cnt] = PDUWATTHOURSTS_INDEX;
			if(wh_ts.longValue() < 0)
				val[cnt] = null;	
			else
				val[cnt] = wh_ts;
			cnt++;
		}
		if(tot_vah != null) {
			idx[cnt] = PDUAPPPWRHOURS_INDEX;
			if(tot_vah.doubleValue() < 0.0)
				val[cnt] = null;	
			else
				val[cnt] = tot_vah;
			cnt++;
		}
		if(towerid != null) {
			idx[cnt] = PDUTOWERID_INDEX;
			val[cnt] = towerid;
			cnt++;
		}
		if(feedsetid != null) {
			idx[cnt] = PDUFEEDSETID_INDEX;
			val[cnt] = feedsetid;
			cnt++;
		}
		if(outlabel != null) {
			idx[cnt] = PDULABEL_INDEX;
			val[cnt] = outlabel;
			cnt++;
		}
		if(sw != null) {
			idx[cnt] = PDUSWITCH_INDEX;
			val[cnt] = sw;
			cnt++;
		}
		if(bankid != null) {
			idx[cnt] = PDUBANKID_INDEX;
			val[cnt] = bankid;
			cnt++;
		}
		if(cnt != 0) {
			tag.updateTagAttributes(idx, val, cnt);
		}		
	}
}
