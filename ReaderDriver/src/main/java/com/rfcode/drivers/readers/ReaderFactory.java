package com.rfcode.drivers.readers;

import java.util.Map;

/**
 * This is the generic interface for a reader factory. A reader factory is
 * intended to provide the interfaces needed to create a Reader instance, as
 * well as describe the attributes needed in order to do so.
 * 
 * @author Mike Primm
 * 
 */
public interface ReaderFactory extends ReaderEntity {
    /**
     * Get ordered list of attribute IDs required by Reader instances created by
     * this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getReaderAttributeIDs();
    /**
     * Get label for reader factory - describes type of readers supported
     * 
     * @return label
     */
    public String getReaderFactoryLabel();
    /**
     * Get description for reader factory - more verbose than label.
     * 
     * @return description
     */
    public String getReaderFactoryDescription();
    /**
     * Get copy of default attribute set for a reader. This method should be used to
     * initialize the attribute set ultimately passed to the Reader instances,
     * in order to handle cases where code updates add new attributes.
     * 
     * @return default attribute set
     */
    public Map<String, Object> getDefaultReaderAttributes();
    /**
     * Create uninitialized instance of Reader, as defined by this factory. The
     * resulting instance must be supplied with attributes, as indicated by the
     * getReaderAttributeIDs(), before the instance is usable.
     * 
     * @return Reader instance
     */
    public Reader createReader();
    /**
     * Get list of TagType IDs supported by Readers from this factory.
     * 
     * @return list of TagType IDs supported
     */
    public String[] getSupportedTagTypeIDs();
    /**
     * Get list of TagLink attribute IDs reported for TagLinks.
     * 
     * @return list of TagLink attribute IDs
     */
    public String[] getTagLinkAttributeIDs();
    /**
     * Add a reader lifecycle listener.
     * 
     * @param listener -
     *            lifecycle listener
     */
    public void addReaderLifecycleListener(ReaderLifecycleListener listener);
    /**
     * Remove a reader lifecycle listener.
     * 
     * @param listener -
     *            lifecycle listener
     */
    public void removeReaderLifecycleListener(ReaderLifecycleListener listener);
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion();
}
