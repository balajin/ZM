package com.rfcode.drivers.readers;

/**
 * Generic status for Reader class.
 * 
 * @author Mike Primm
 */
public enum ReaderStatus {
    /** Reader is disabled */
    DISABLED(0),
    /** Reader driver is attempting to start communication with hardware */
    CONNECTING(1),
    /** Reader driver is connected, and is configuring reader for operation */
    INITIALIZING(2),
    /** Reader is connected and initialized, but is not active */
    INITIALIZED(3),
    /** Reader is connected, initialized, and active */
    ACTIVE(4),
    /** Reader driver is closing connection to reader */
    DISCONNECTING(5),
    /** Reader driver is not communicating with reader */
    DISCONNECTED(6),
    /** Reader hardware has experienced a failure */
    READERFAILURE(7),
    /** Reader driver is misconfigured */
    CONFIGFAILURE(8),
    /** Reader driver is unable to connect to the reader */
    CONNECTFAILURE(9),
    /** Excessive noise/interference detected by reader */
    NOISEDETECTED(10),
    /** Reader status is indeterminate */
    UNKNOWN(11),
    /** Access denied (bad userid/password) */
    ACCESSDENIED(12),
    /** High traffic detected */
    HIGHTRAFFIC(13),
	/** Firmware upgrading */
	FIRMWAREUPGRADING(14);

    private int ord;

    ReaderStatus(int idx) {
        ord = idx;
    }
    /**
     * Test if state is an online one
     */
    public static boolean isOnline(ReaderStatus rs) {
        switch(rs) {
            case ACTIVE:
            case NOISEDETECTED:
            case HIGHTRAFFIC:
                return true;
            default:
                return false;
        }
    }
    public int getOrd() {
        return ord;
    }
}
