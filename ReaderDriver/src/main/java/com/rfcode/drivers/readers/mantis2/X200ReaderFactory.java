package com.rfcode.drivers.readers.mantis2;

import java.util.Map;
import java.util.HashMap;

/**
 * Factory for Mantis II (*200) fixed readers, via network interface.
 * 
 * @author Mike Primm
 */
public abstract class X200ReaderFactory extends MantisIIReaderFactory {
    /** Default port number for Mantis II fixed readers */
    public static final int DEFAULT_PORTNUM = 6500;
    /** Default hostname for Mantis II fixed readers */
    public static final String DEFAULT_HOSTNAME = "192.168.1.129";
    /* List of attributes for fixed readers */
    private static final String[] ATTRIBS = {
        ATTRIB_READERNAME, ATTRIB_HOSTNAME, ATTRIB_PORTNUM,
        ATTRIB_USERID, ATTRIB_PASSWORD, ATTRIB_TAGTIMEOUT, ATTRIB_AGEINCOUNT,
        ATTRIB_SSIDELTATHRESH, ATTRIB_OFFLINELINKAGEOUT, ATTRIB_SSICUTOFF[0],
        ATTRIB_SSICUTOFF[1], ATTRIB_AGEOUTTIME[0], ATTRIB_AGEOUTTIME[1],
        ATTRIB_CHANNEL_BIAS[0], ATTRIB_CHANNEL_BIAS[1], ATTRIB_NOISETHRESH,
        ATTRIB_EVTPERSECTHRESH,
        ATTRIB_SERPORT_DRVR, ATTRIB_SERPORT_BAUD, ATTRIB_REPORTTRIGGERS,
        ATTRIB_MERGECHANNELS, ATTRIB_JOINCHANNELS,
        ATTRIB_UPCONNENABLED, ATTRIB_UPCONNRDRID,
        ATTRIB_UPCONNPWD, ATTRIB_PARTINDEX, ATTRIB_PARTCOUNT,
        ATTRIB_PARTPERIOD, ATTRIB_PAYLOADFAULTINMASK, ATTRIB_PAYLOADFAULTINVALUE,
        ATTRIB_PAYLOADCHGIGNOREMASK, ATTRIB_POSITIONSRC, ATTRIB_STATICLATLON,
		ATTRIB_GPS_MIN_SSI
    };
    private Map<String, Object> attrib_defaults;
	private String[] attribs;

    private static final Object[] ATTRIBS_DEF = {
        DEFAULT_READERNAME,
        DEFAULT_HOSTNAME,
        Integer.valueOf(DEFAULT_PORTNUM), "", "",
        Integer.valueOf(DEFAULT_TAGTIMEOUT),
        Integer.valueOf(DEFAULT_AGEINCOUNT),
        Integer.valueOf(DEFAULT_SSIDELTATHRESH),
        Integer.valueOf(DEFAULT_OFFLINELINKAGEOUT),
        Integer.valueOf(DEFAULT_CHANNEL_SSICUTOFF),
        Integer.valueOf(DEFAULT_CHANNEL_SSICUTOFF),
        Integer.valueOf(DEFAULT_CHANNEL_AGEOUTTIME),
        Integer.valueOf(DEFAULT_CHANNEL_AGEOUTTIME),
        Integer.valueOf(DEFAULT_CHANNEL_BIAS),
        Integer.valueOf(DEFAULT_CHANNEL_BIAS),
        Integer.valueOf(DEFAULT_NOISETHRESH),
        Integer.valueOf(DEFAULT_EVTPERSECTHRESH),
        DEFAULT_SERPORT_DRVR,
        Integer.valueOf(DEFAULT_SERPORT_BAUD),
        Boolean.valueOf(DEFAULT_REPORTTRIGGERS),
        Boolean.valueOf(DEFAULT_MERGECHANNELS),
        Boolean.valueOf(DEFAULT_JOINCHANNELS),
        Boolean.valueOf(DEFAULT_UPCONNENABLED),
        DEFAULT_UPCONNRDRID,
        DEFAULT_UPCONNPWD,
        Integer.valueOf(DEFAULT_PARTINDEX),
        Integer.valueOf(DEFAULT_PARTCOUNT),
        Integer.valueOf(DEFAULT_PARTPERIOD),
        Integer.valueOf(DEFAULT_PAYLOADFAULTINMASK),
        Integer.valueOf(DEFAULT_PAYLOADFAULTINVALUE),
        Integer.valueOf(DEFAULT_PAYLOADCHGIGNOREMASK),
        DEFAULT_POSITIONSRC,
        DEFAULT_STATICLATLON,
		Integer.valueOf(DEFAULT_GPS_MIN_SSI)
        };
    /**
     * Factory constructor
     */
    protected X200ReaderFactory() {
        attrib_defaults = new HashMap<String, Object>();
        for (int i = 0; i < ATTRIBS.length; i++) {
            attrib_defaults.put(ATTRIBS[i], ATTRIBS_DEF[i]);
        }
		attribs = attrib_defaults.keySet().toArray(new String[0]);

        setChannelCount(2); /* All these default to 2 channels */
    }
	/**
	 * Add additional attribute - for subclasses
	 */
	protected void addAttribute(String id, Object defval) {
		attrib_defaults.put(id, defval);
		attribs = attrib_defaults.keySet().toArray(new String[0]);
	}
    /**
     * Get ordered list of attribute IDs required by Reader instances created by
     * this factory
     * 
     * @return array of attribute IDs
     */
    public String[] getReaderAttributeIDs() {
        return attribs;
    }
    /**
     * Get copy of default attribute set for a reader. This method should be used to
     * initialize the attribute set ultimately passed to the Reader instances,
     * in order to handle cases where code updates add new attributes.
     * 
     * @return default attribute set
     */
    public Map<String, Object> getDefaultReaderAttributes() {
        return new HashMap<String,Object>(attrib_defaults);
    }
    /**
     * Get factory version - must be incremented each time there are changes to
     * attributes or other external interfaces.  First version should be 1.
     */
    public int  getVersion() {
        return 1;
    }
}
