package com.rfcode.drivers.readers.mantis2;

/**
 * Tag type for treatment 04W tags - Mantis II Tags - Treatment 05A (series 2 IR + tamper option)
 * 
 * @author Mike Primm
 */
public class MantisIITagType05A extends MantisIITagType {
    /** TagType ID for this tag type */
    public static final String TAGTYPEID = "mantis05A";
    /** Our list of tag attributes */
    private static final String[] TAGATTRIBS = {
        MantisIITagType.IRLOCATOR_ATTRIB, MantisIITagType.MOTION_ATTRIB,
        MantisIITagType.TAMPER_ATTRIB,
        MantisIITagType.LOWBATT_ATTRIB, MantisIITagType.TAMPER_ARMED_ATTRIB };
    private static final String[] TAGATTRIBLABS = {
        MantisIITagType.IRLOCATOR_ATTRIB_LABEL, MantisIITagType.MOTION_ATTRIB_LABEL,
        MantisIITagType.TAMPER_ATTRIB_LABEL,
        MantisIITagType.LOWBATT_ATTRIB_LABEL, MantisIITagType.TAMPER_ARMED_ATTRIB_LABEL };
    private static final int IRLOCATOR_ATTRIB_INDEX = 0; /*
                                                             * Index in list of
                                                             * attribute
                                                             */
    private static final int MOTION_ATTRIB_INDEX = 1; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int TAMPER_ATTRIB_INDEX = 2; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int LOWBATT_ATTRIB_INDEX = 3; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    private static final int TAMPER_ARMED_ATTRIB_INDEX = 4; /*
                                                         * Index in list of
                                                         * attribute
                                                         */
    /** Our list of tag attribute defaults */
    private static final Object[] TAGDEFATTRIBS = { null, Boolean.FALSE,
        Boolean.FALSE, Boolean.FALSE, Boolean.FALSE };

    /**
     * Constructor
     */
    public MantisIITagType05A() {
        super(5, 'A', 0);
        setLabel("RF Code Mantis II Tag - Treatment 05A");
        setTagAttributes(TAGATTRIBS); /* Set tag attribute list */
        setTagAttributeLabels(TAGATTRIBLABS); /* Set tag attribute list */
        setTagAttributeDefaultValues(TAGDEFATTRIBS); /* Set defaults */
        setID(TAGTYPEID); /* Set our tag type */
    }
    /**
     * Parse payloads - input is current payload, and an ordered list of
     * payloads, from most recent to least, of length specified by
     * getRequiredPayloadCount(). If payloads are not available, such as from a
     * new tag, the corresponding values are -1.
     * 
     * @param cur_payload -
     *            current payload
     * @param cur_timestamp -
     *            current payload timestamp, in UTC msec
     * @param payloads -
     *            list of previous payloads, most recent is always index 0
     * @param payloadage -
     *            list of timestamps for the payloads, in UTC milliseconds
     * @param tag - tag being updated
     * @return true if any values changed, false if all unchanged
     */
    public boolean parsePayload(int cur_payload, long cur_timestamp,
        int[] payloads, long[] payloadage, MantisIITag tag, MantisIIReader rdr) {
        boolean change = false;
		boolean verify = false;
		if(rdr != null)
			verify = rdr.isEnhPayloadVerifyActive(tag);

        /* Compute IR payload */
		int ir = (cur_payload & 0x3FFF0) >> 4;
        /* See if series 1 IR code */
        String v = "";
        if((ir >= 0x3E00) && (ir <= 0x3FEF)) {
            ir = ir - 0x3E00;
            v = Integer.toOctalString(ir);
            if(ir < 010)
                v = "00" + v;
            else if(ir < 0100)
                v = "0" + v;
        }
        else if(ir == 0) {  /* Not found, return compatable code */
            v = "000";
        }
        else if((ir >= 0x0001) && (ir <= 0x270F)) { /* Series 2 code */
            v = Integer.toString(ir);
            if(ir < 10)
                v = "000" + v;
            else if(ir < 100)
                v = "00" + v;
            else if(ir < 1000)
                v = "0" + v;
        }
        change = updateString(tag, IRLOCATOR_ATTRIB_INDEX, v) || change;

        boolean vv;
        /* Set motion flag */
		vv = ((cur_payload & 0x04) != 0);
		change = updateBoolean(tag, MOTION_ATTRIB_INDEX, vv) || change;
        /* Set tamper flag - verify on set */
		vv = ((cur_payload & 0x08) != 0);
        change = updateBooleanVerify(tag, TAMPER_ATTRIB_INDEX, vv, verify && vv) || change;
        /* Set tamper armed flag 0 - verify on change */
		vv = ((cur_payload & 0x01) != 0);
        change = updateBooleanVerify(tag, TAMPER_ARMED_ATTRIB_INDEX, vv, verify) || change;
        /* Set low battery flag - verify on set */
		vv = ((cur_payload & 0x02) != 0);
        change = updateBooleanVerify(tag, LOWBATT_ATTRIB_INDEX, vv, verify && vv) || change;
        
        return change;
    }
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return true;
	}

}
