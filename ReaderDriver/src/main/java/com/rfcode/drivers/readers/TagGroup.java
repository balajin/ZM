package com.rfcode.drivers.readers;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import com.rfcode.drivers.BadParameterException;

/**
 * Class for representing a specific set of tags, defined by a tag type and a
 * set of values for the attributes indicated by that tag type as needed to
 * define a group. In general, these attributes indicate distinctive population
 * selection characteristics, such as the group code on Mantis tags, which are
 * required in order for a reader supporting the given tag type to observe the
 * set of tags described by the tag group.
 * 
 * @author Mike Primm
 */
public abstract class TagGroup implements ReaderEntity {
    /** Well known attribute - location update delay (in seconds) */
    public static final String ATTRIB_LOC_UPD_DELAY = "locupddelay";
	/** Well known attribute - tag attribute update block list */
	public static final String ATTRIB_BLK_ATTR_UPD_LIST = "blockattrupd";
	/** Well known attribute - enhanced payload verify active (string - YES|NO|IFSUPPORTED) */
	public static final String ATTRIB_ENH_PAYLOAD_VERIFY = "enhpayloadverify";
	public static final String ENH_PAYLOAD_VERIFY_YES = "YES";
	public static final String ENH_PAYLOAD_VERIFY_NO = "NO";
	public static final String ENH_PAYLOAD_VERIFY_IFSUPPORTED = "IFSUPPORTED";
	public static final String ENH_PAYLOAD_VERIFY_MULTI3 = "MULTI3";
	public static final String ENH_PAYLOAD_VERIFY_MULTI4 = "MULTI4";
	public static final String ENH_PAYLOAD_VERIFY_MULTI5 = "MULTI5";
    /** Well known attribute - parent group */
    public static final String ATTRIB_PAR_GROUP = "pargrp";
	/* Seperator for subgroups */
	public static final String SUBGROUP_SEP = "__";
    /** Well known attribute - tag timeout (in seconds), 0=never, -1=use reader timeout */
    public static final String ATTRIB_GROUP_TIMEOUT = "grouptimeout";
    public static final int GROUP_TIMEOUT_USEREADER = -1;
    public static final int DEFAULT_GROUP_TIMEOUT = GROUP_TIMEOUT_USEREADER;
    /** Well known attribute - matching location confidence boost */
    public static final String ATTRIB_LOC_MATCH_BOOST = "locmatchboost";
    public static final double DEFAULT_LOC_MATCH_BOOST = 0.0;
    /** Well known attribute - matching location confidence boost time (in seconds) */
    public static final String ATTRIB_LOC_MATCH_TIME = "locmatchboosttime";
    public static final int DEFAULT_LOC_MATCH_TIME = 300;
    /* Our group ID */
    private String groupid;
    /* Our label */
    private String label;
    /* Our TagType */
    private TagType tagtype;
    /* Location update delay (cached) */
    private int loc_upd_delay;
    /* Location match boost */
    private double loc_match_boost;
    /* Location match time constant */
    private int loc_match_boost_time;
    /* Tag timeout (cached) */
    private Integer group_timeout;
    /* Multireader verify count */
    private int multiverifycnt;
	/* Tag attribute update bloc; set */
	private Set<String> blk_upd_attr;
	private boolean[] blk_upd_attr_by_idx;
    /* Our attributes, as a map keyed by attribute ID */
    private HashMap<String, Object> attribs;
    private Map<String, Object> attribs_ro;
    /* Map of all live tag instances in this group */
    private HashMap<String, Tag> tags;
    /* List of all lifecycle listeners */
    private static ArrayList<TagGroupLifecycleListener> lifecycle_listeners = new ArrayList<TagGroupLifecycleListener>();
    /* List of group-specific lifecycle listeners */
    private ArrayList<TagLifecycleListener> tag_listeners;
	/* Subgroups */
	private HashMap<String, TagGroup> subgroups;
	private TagGroup parent;

	public static enum PayloadVerify {
		YES, NO, IFSUPPORTED, MULTI3, MULTI4, MULTI5
	}

	private PayloadVerify verify = PayloadVerify.NO;
    /**
     * Constructor for TagGroup
     * 
     * @param tt -
     *            our tag type
     */
    protected TagGroup(TagType tt) {
		this(tt, null);
    }
    /**
     * Constructor for TagGroup
     * 
     * @param tt -
     *            our tag type
     */
    protected TagGroup(TagType tt, TagGroup par) {
        tagtype = tt;
        tags = new HashMap<String, Tag>();
        loc_upd_delay = -1;
        loc_match_boost = -1;
        loc_match_boost_time = -1;
		blk_upd_attr = null;
        multiverifycnt = 0;
		if(tt.getSubTypes() != null)
			subgroups = new HashMap<String, TagGroup>();
		parent = par;
		if(parent != null) {
			if(parent.subgroups != null) {
				parent.subgroups.put(tt.getID(), this);
			}
		}
        tag_listeners = new ArrayList<TagLifecycleListener>();
	}		
    /**
     * Initialization method - must be called once all attributes have been
     * initialized
     * 
     * @throws DuplicateEntityIDException
     *             if TagGroup is duplicate of existing entity
     * @throws if
     *             missing required attribute
     */
    public void init() throws DuplicateEntityIDException, BadParameterException {
        int i;
        if (tagtype == null)
            throw new BadParameterException("Missing tagType");
        if (groupid == null)
            throw new BadParameterException("Missing tagGroupID");
		ReaderEntityDirectory.testID(groupid); /* Already exists? */
		if(!isSubGroup()) {	/* If parent group, check attributes */
	        if (attribs == null)
    	        throw new BadParameterException("Missing tagGroupAttributes");
	        String[] ga = tagtype.getTagGroupAttributes(); /* Get group attributes */
	        /* Make sure we have values for all the required ones */
	        for (i = 0; i < ga.length; i++) {
	            if (!attribs.containsKey(ga[i])) {
	                throw new BadParameterException(
	                    "Missing required tagGroupAttribute " + ga[i]);
	            }
	        }
	        /* Get list of distinctive group attributes */
	        String[] dga = tagtype.getTagGroupDistinctAttributes();
	        if ((dga == null) || (dga.length == 0))
	            dga = ga;
	        /* Walk through existing groups, looking for match */
	        Collection<TagGroup> grps = ReaderEntityDirectory.getTagGroups();
	        for (TagGroup t : grps) {
	            boolean mismatch = false;
	            for (i = 0; (!mismatch) && (i < dga.length); i++) {
	                Object v = attribs.get(dga[i]); /* Get our value */
	                Object v2 = t.attribs.get(dga[i]); /*
	                                                     * Get value from other
	                                                     * group
	                                                     */
	                if ((v2 == null) || (!v.equals(v2))) { /*
	                                                         * If no value, or not
	                                                         * equals
	                                                         */
	                    mismatch = true; /* Mismatch */
	                }
	            }
	            if (!mismatch) { /* If no mismatch, duplicate group */
	                throw new DuplicateEntityIDException(groupid);
	            }
	        }
		}
        /* Group validated - add to set */
        ReaderEntityDirectory.addEntity(this); /* Add ourselves to the map */
        if (label == null) /* Use as default label, if not provided */
            label = groupid;
        /* Call our lifecycle listeners */
        ArrayList<TagGroupLifecycleListener> lst = lifecycle_listeners;
        int sz = lst.size();
        for (i = 0; i < sz; i++) {
            lst.get(i).tagGroupLifecycleCreate(this);
        }
		/* Now, if there are subtypes for tag type, make subgroups */
		String[] st = tagtype.getSubTypes();
		if(st != null) {
			for(i = 0; i < st.length; i++) {
		        TagType stt = ReaderEntityDirectory.findTagType(st[i]);
				if((stt != null) && (stt.isSubType())) {
			        TagGroup stg = stt.createTagSubGroup(this);
					stg.setID(groupid + SUBGROUP_SEP + st[i]);
					stg.init();
				}
			}
		}
    }
    /**
     * Discard the TagGroup
     */
    public void cleanup() {
        if (groupid != null) { /* Already defined? Remove from map */
            /* First, clean up all our tags */
            if(tags != null) {
                HashSet<Tag> tset = new HashSet<Tag>(tags.values());
                for(Tag t : tset) {
                    t.cleanup();
                }
            }
			/* Now, if there are subtypes for tag type, cleanup subgroups */
			if(subgroups != null) {
				for(TagGroup sg : subgroups.values()) {
					sg.cleanup();
				}
				subgroups = null;
			}
            /* Call our lifecycle listeners */
            ArrayList<TagGroupLifecycleListener> lst = lifecycle_listeners;
            int sz = lst.size();
            for (int i = 0; i < sz; i++) {
                lst.get(i).tagGroupLifecycleDelete(this);
            }
            // Notify tag type
            this.tagtype.tagGroupDeleted(this);
            
            ReaderEntityDirectory.removeEntity(this);
			parent = null;
            groupid = null;
        }
        if (attribs != null) {
            attribs.clear();
            attribs = null;
            attribs_ro = null;
        }
    }
    /**
     * Set tag group ID - this is required and must be unique, and cannot be
     * changed once set
     * 
     * @param id
     */
    public void setID(String id) {
        if (groupid != null)
            return;
        groupid = id; /* Save ID */
    }
    /**
     * Get ID for the tag group.
     * 
     * @return TagGroup ID
     */
    public String getID() {
        return groupid;
    }
    /**
     * Get the tag type for this group
     * 
     * @return tag type
     */
    public TagType getTagType() {
        return tagtype;
    }
    /**
     * Set attributes required for TagGroup.
     * 
     * @param attrs -
     *            Map of attribute values, keyed by attribute ids
     */
    public void setTagGroupAttributes(Map<String, Object> attrs) {
        attribs = new HashMap<String, Object>(attrs);
        if(attribs.get(ATTRIB_LOC_MATCH_BOOST) == null)
            attribs.put(ATTRIB_LOC_MATCH_BOOST, Double.valueOf(DEFAULT_LOC_MATCH_BOOST));
        if(attribs.get(ATTRIB_LOC_MATCH_TIME) == null)
            attribs.put(ATTRIB_LOC_MATCH_TIME, Integer.valueOf(DEFAULT_LOC_MATCH_TIME));
        attribs_ro = Collections.unmodifiableMap(attribs);
        loc_upd_delay = -1;
        loc_match_boost = -1;
        loc_match_boost_time = -1;
        group_timeout = null;
        multiverifycnt = 0;
		blk_upd_attr = null;
		if(subgroups != null) {
			for(TagGroup sg : subgroups.values())
				sg.blk_upd_attr = null;
		}
		if(isEnhPayloadVerifySupported()) {
			Object v = attribs.get(ATTRIB_ENH_PAYLOAD_VERIFY);
			if(v != null) {
				String vv = v.toString();
				if(vv.equals(ENH_PAYLOAD_VERIFY_NO))
					verify = PayloadVerify.NO;
				else if(vv.equals(ENH_PAYLOAD_VERIFY_YES))
					verify = PayloadVerify.YES;
                else if(vv.equals(ENH_PAYLOAD_VERIFY_MULTI3)) {
                    verify = PayloadVerify.MULTI3;
                    multiverifycnt = 3;
                }
                else if(vv.equals(ENH_PAYLOAD_VERIFY_MULTI4)) {
                    verify = PayloadVerify.MULTI4;
                    multiverifycnt = 4;
                }
                else if(vv.equals(ENH_PAYLOAD_VERIFY_MULTI5)) {
                    verify = PayloadVerify.MULTI5;
                    multiverifycnt = 5;
                }
				else
					verify = PayloadVerify.IFSUPPORTED;
			}
			else {
				verify = PayloadVerify.IFSUPPORTED;
			}
		}
		else {
			verify = PayloadVerify.NO;
		}
		this.tagtype.tagGroupAttributesUpdated(this);
    }
    /**
     * Get attributes set for TagGroup
     * 
     * @returns map of attibute IDs (read-only)
     */
    public Map<String, Object> getTagGroupAttributes() {
        return attribs_ro;
    }
    /**
     * Set label for tag type
     * 
     * @param lab -
     *            label
     */
    public void setLabel(String lab) {
        label = lab;
    }
    /**
     * Get label for tag type
     * 
     * @return Label
     */
    public String getLabel() {
        return label;
    }
    /**
     * Get TagGroups of given TagType
     * 
     * @param tt -
     *            tag type
     * @return Map of tag groups, keyed by tag group ID
     */
    public static Map<String, TagGroup> getTagGroupsByTagType(TagType tt) {
        Map<String, TagGroup> tgbt = new HashMap<String, TagGroup>();
        for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
            if (tg.getTagType() == tt)
                tgbt.put(tg.getID(), tg);
        }
        return tgbt;
    }
    /**
     * Update boolean attribute value at given index, if needed
     * 
     * @param obj -
     *            list of objects
     * @param index -
     *            index in list
     * @param val -
     *            new boolean value
     */
    protected static final void updateBoolean(Object[] obj, int index,
        boolean val) {
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Boolean))
            || (((Boolean) o).booleanValue() != val))
            obj[index] = Boolean.valueOf(val);
    }
    /**
     * Update integer attribute value at given index, if needed
     * 
     * @param obj -
     *            list of objects
     * @param index -
     *            index in list
     * @param val -
     *            new integer value
     */
    protected static final void updateInt(Object[] obj, int index, int val) {
        Object o = obj[index];
        /* If not defined, wrong type, or new value, set new value */
        if ((o == null) || (!(o instanceof Integer))
            || (((Integer) o).intValue() != val))
            obj[index] = Integer.valueOf(val);
    }
    /**
     * Add tag to the set of live tags in the group
     * 
     * @param tag -
     *            tag to add
     */
    protected void addTag(Tag tag) {
        tags.put(tag.getTagGUID(), tag);
    }
    /**
     * Remove tag from the set of live tags in the group
     * 
     * @param tag -
     *            tag to remove
     */
    protected void removeTag(Tag tag) {
        tags.remove(tag.getTagGUID());
    }
    /**
     * Find tag, by tag GUID
     * 
     * @param guid -
     *            tag's GUID
     * @return tag, or null if not found
     */
    public Tag findTag(String guid) {
        return tags.get(guid);
    }
    /**
     * Get shallow copy of map of live tags, keyed by GUID
     * 
     * @return Map of tags, keyed by GUID
     */
    public Map<String, Tag> getTags() {
        return new HashMap<String, Tag>(tags);
    }
    /**
     * Get tag iterator (must not be used if tag population can change during iteration)
     * @return Map.Entry set for traversing tags
     */
    public Set<Map.Entry<String, Tag>> getTagIterator() {
        return tags.entrySet();
    }
    /**
     * Get number of live tags
     * 
     * @return tag count
     */
    public int getTagCount() {
        return tags.size();
    }
    /**
     * Find tag in any group, by GUID
     * 
     * @param guid -
     *            tag's GUID
     * @return tag, or null if not found
     */
    public static Tag findTagInAnyGroup(String guid) {
        Tag t = null;
        for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
            t = tg.findTag(guid);
            if (t != null)
                break;
        }
        return t;
    }
    /**
     * Find group for given tag GUID (even if tag does not yet exist)
     * 
     * @param guid -
     *            tag's GUID
     * @return group, or null if none matched
     */
    public static TagGroup findTagGroupForTagID(String guid) {
        for (TagGroup tg : ReaderEntityDirectory.getTagGroups()) {
            if (tg.isMatchingTagID(guid)) {
            	return tg;
            }
        }
        return null;
    }
    /**
     * Add TagLifecycleListener or TagStatusListener for all tags in this group.
     * 
     * @param listen -
     *            listener to be added
     */
    public void addTagLifecycleListener(TagLifecycleListener listen) {
        ArrayList<TagLifecycleListener> newlist = new ArrayList<TagLifecycleListener>(tag_listeners);
        newlist.add(listen);
        tag_listeners = newlist;
    }
    /**
     * Remove TagLifecycleListener or TagStatusListener for all tags in this
     * group.
     * 
     * @param listen -
     *            listener to be removed
     */
    public void removeTagLifecycleListener(TagLifecycleListener listen) {
        ArrayList<TagLifecycleListener> newlist = new ArrayList<TagLifecycleListener>(tag_listeners);
        newlist.remove(listen);
        tag_listeners = newlist;
    }
    /**
     * Call tag lifecycle listeners for creation of given tag.
     * 
     * @param tag -
     *            new tag
     * @param cause -
     *            reason for create (null=tag beacon, otherwise event type (i.e. optima msg)
     */
    protected void notifyTagLifecycleCreate(Tag tag, String cause) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            lst.get(i).tagLifecycleCreate(tag, cause);
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            lst.get(i).tagLifecycleCreate(tag, cause);
        }
    }
    /**
     * Call tag lifecycle listeners for deletion of given tag.
     * 
     * @param tag -
     *            deleted tag
     */
    protected void notifyTagLifecycleDelete(Tag tag) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            lst.get(i).tagLifecycleDelete(tag);
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            lst.get(i).tagLifecycleDelete(tag);
        }
    }
    /**
     * Call tag status listeners for attribute changed notification
     * 
     * @param tag -
     *            tag with change attributes
     * @param oldval -
     *            array of previous tag attribute values (may be longer than the
     *            number of atributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    protected void notifyTagStatusAttributeChange(Tag tag, Object[] oldval) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusAttributeChange(tag,
                    oldval);
            }
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr)
                    .tagStatusAttributeChange(tag, oldval);
            }
        }
    }
    /**
     * Call tag status listeners for attribute changed notification
     * 
     * @param tag -
     *            tag with change attributes
     * @param oldval -
     *            array of previous tag attribute values (may be longer than the
     *            number of atributes for the given tag). Ordered to match
     *            attribute IDs from TagType.getTagAttributes()
     */
    protected void notifyTagTriggeredMsg(Tag tag, String cmd, String[] attrids, Object[] attrvals) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusTriggeredMsg(tag,
                    cmd, attrids, attrvals);
            }
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusTriggeredMsg(tag,
                        cmd, attrids, attrvals);
            }
        }
    }
    /**
     * Call tag status listeners for tag link added event.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    protected void notifyTagStatusLinkAdded(Tag tag, TagLink taglink) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusLinkAdded(tag, taglink);
            }
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusLinkAdded(tag, taglink);
            }
        }
        /* And call the listeners for the channel */
        List<TagLinkLifecycleListener> linkListeners = taglink.getChannel().getTagLinkListeners();
        sz = linkListeners.size();
        for (i = 0; i < sz; i++) {
            TagLinkLifecycleListener lsnr = linkListeners.get(i);
            lsnr.tagLinkAdded(tag, taglink);
        }
    }
    /**
     * Call tag status listeners for tag link removed event.
     * 
     * @param tag -
     *            tag with new link
     * @param taglink -
     *            link added to tag
     */
    protected void notifyTagStatusLinkRemoved(Tag tag, TagLink taglink) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusLinkRemoved(tag,
                    taglink);
            }
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagStatusListener) {
                ((TagStatusListener) lsnr).tagStatusLinkRemoved(tag, taglink);
            }
        }
        /* And call the listeners for the channel */
        List<TagLinkLifecycleListener> linkListeners = taglink.getChannel().getTagLinkListeners();
        sz = linkListeners.size();
        for (i = 0; i < sz; i++) {
            TagLinkLifecycleListener lsnr = linkListeners.get(i);
            lsnr.tagLinkRemoved(tag, taglink);
        }
    }
    /**
     * Call tag link status listeners for tag link updated event.
     * 
     * @param tag -
     *            tag with updated link
     * @param taglink -
     *            link updated to tag
     */
    protected void notifyTagLinkUpdated(Tag tag, TagLink taglink) {
        int i, sz;
        ArrayList<TagLifecycleListener> lst;
        /* Call our listeners */
        lst = tag_listeners;
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagAndLinkStatusListener) {
                ((TagAndLinkStatusListener) lsnr).tagLinkStatusAttributeChange(taglink);
            }
        }
        /* And fall through to tagtype listeners */
        lst = tagtype.getTagLifecycleListeners();
        sz = lst.size();
        for (i = 0; i < sz; i++) {
            TagLifecycleListener lsnr = lst.get(i);
            if (lsnr instanceof TagAndLinkStatusListener) {
                ((TagAndLinkStatusListener) lsnr).tagLinkStatusAttributeChange(taglink);
            }
        }
    }
    /**
     * Get location update delay
     */
    public int getLocationUpdateDelay() {
        if(loc_upd_delay < 0) {
            Object v = null;
            if(attribs != null)
 
               v = attribs.get(ATTRIB_LOC_UPD_DELAY);
            if((v != null) && (v instanceof Integer))
                loc_upd_delay = ((Integer)v).intValue();
            else
                loc_upd_delay = 0;
        }
        return loc_upd_delay;
    }
    /**
     * Get location match boost
     */
    public double getLocationMatchBoost() {
        if(loc_match_boost < 0) {
            Object v = null;
            if(attribs != null)
 
               v = attribs.get(ATTRIB_LOC_MATCH_BOOST);
            if((v != null) && (v instanceof Double))
                loc_match_boost = ((Double)v).doubleValue();
            else
                loc_match_boost = 0;
        }
        return loc_match_boost;
    }
    /**
     * Get location match boost time
     */
    public int getLocationMatchTime() {
        if(loc_match_boost_time < 0) {
            Object v = null;
            if(attribs != null)
 
               v = attribs.get(ATTRIB_LOC_MATCH_TIME);
            if((v != null) && (v instanceof Integer))
                loc_match_boost_time = ((Integer)v).intValue();
            else
                loc_match_boost_time = 0;
        }
        return loc_match_boost_time;
    }
    /**
     * Get group-level tag timeout
     */
    public long getGroupTimeout() {
        if(group_timeout == null) {
            Object v = null;
            if(attribs != null)
                v = attribs.get(ATTRIB_GROUP_TIMEOUT);
            if((v != null) && (v instanceof Integer) && (((Integer)v).intValue() >= 0))
                group_timeout = (Integer)v;
            else
                group_timeout = Integer.valueOf(GROUP_TIMEOUT_USEREADER);
        }
        return group_timeout.intValue();
    }

    /**
     * Get update blocked tag attribute set
     */
    public Set<String> getBlockedUpdateTagAttribs() {
        if(blk_upd_attr == null) {
            Object v = null;
			if(parent != null) {
				if(parent.attribs != null)
					v = parent.attribs.get(ATTRIB_BLK_ATTR_UPD_LIST);
			}
			else {
	            if(attribs != null)
    	            v = attribs.get(ATTRIB_BLK_ATTR_UPD_LIST);
			}
			blk_upd_attr = new HashSet<String>();
			blk_upd_attr_by_idx = new boolean[tagtype.getTagAttributeCount()];
            if((v != null) && (v instanceof List)) {
				for(Object o : (List<?>)v) {
					String id = (String)o;
					int idx = tagtype.getTagAttributeIndex(id);
					if((idx >= 0) && (idx < blk_upd_attr_by_idx.length)) {
						blk_upd_attr.add(id);
						blk_upd_attr_by_idx[idx] = true;
						//System.out.println("grp " + getID() + " block " + id + ", index " + idx);
					}
				}
			}
        }
        return blk_upd_attr;
    }
	/**
	 * Test if given attribute ID is update blocked
	 */
	public boolean isBlockedUpdateTagAttrib(String aid) {
		return getBlockedUpdateTagAttribs().contains(aid);
	}
	/**
	 * Test if given attribute index is update blocked
	 */
	public boolean isBlockedUpdateTagAttrib(int idx) {
		if(blk_upd_attr == null)
			getBlockedUpdateTagAttribs();
		if((idx >= 0) && (idx < blk_upd_attr_by_idx.length))
			return blk_upd_attr_by_idx[idx];
		return false;
	}
	/**
	 * Test if we're a subgroup (group created to handle subtype)
	 */
	public boolean isSubGroup() {
		return tagtype.isSubType();
	}
	/**
	 * Get parent group (if this is a subgroup)
	 */
	public TagGroup getParentGroup() {
		return parent;
	}
	/**
	 * Get subgroup for given subtype
	 * @param subtype - subtype ID
 	 */
	public TagGroup	getSubGroup(String subtype) {
		if(subgroups != null)
			return subgroups.get(subtype);
		return null;
	}
	/**
	 * Test if there are subgroups/subtags
	 */
	public boolean hasSubGroups() {
		return (subgroups != null);
	}
	/**
	 * Enhanced payload verification supported
	 */
	public boolean isEnhPayloadVerifySupported() {
		return tagtype.isEnhPayloadVerifySupported();
	}
	/**
	 * Enhanced payload verification setting
	 */
	public PayloadVerify getEnhPayloadVerify() {
		return verify;
	}
    /**
     * Get multi-reader verify count setting
     */
    public int getMultiReaderVerifyCount() {
        return multiverifycnt;
    }
    /**
     * Test if given tag ID matches group
     * @param tag_guid - tag GUID
     * @return true if matches, false if not
     */
    public abstract boolean isMatchingTagID(String tag_guid);
}
