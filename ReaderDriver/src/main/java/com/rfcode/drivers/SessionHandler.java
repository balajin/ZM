package com.rfcode.drivers;

/**
 * Interface for handling session lifecycle events, including delivery of data
 * received from an active session. All methods MUST be handled quickly and
 * without blocking, as they are invoked by the SessionFactory thread.
 * 
 * @author Mike Primm
 */
public interface SessionHandler {
    /**
     * Session about to attempt connect - used to notify when the session is
     * about to attempt a connection.
     * 
     * @param s -
     *            session
     */
    public void sessionStartingConnect(Session s);

    /**
     * Session connected - used to notify when the session has completed an
     * attempt to connect to its target. If is_connected = true, the session has
     * connected successfully. If connected successfully, requests to write data
     * to the session can be enqueued and sessionDataRead() callbacks can begin
     * after this callback completes.
     * 
     * @param s -
     *            session
     * @param is_connected -
     *            if true, session was connected successfully. if false, connect
     *            failed.
     */
    public void sessionConnectCompleted(Session s, boolean is_connected);
    /**
     * Session disconnected - used to notify when the session has become
     * disconnected from its target. Starting with this call, requests to write
     * data to the session can no longer be enqueued. sessionDataRead() and
     * sessionDataWriteComplete() callbacks will not happen once the session is
     * disconnected.
     * 
     * @param s -
     *            session
     */
    public void sessionDisconnectCompleted(Session s);
    /**
     * Data read from session - used to deliver raw data reads from session.
     * 
     * @param sdr -
     *            read request result - only valid for duration of
     *            sessionDataRead callback
     */
    public void sessionDataRead(SessionRead sdr);
}
