package com.rfcode.drivers;

/**
 * Interface for Serial Port-connection specific sessions.
 * 
 * @author Mike Primm
 */
public interface SerialPortSession {
    /** Parity setting for no parity */
    public static final int PARITY_NONE = 0;
    /** Parity setting for even parity */
    public static final int PARITY_EVEN = 1;
    /** Parity setting for odd parity */
    public static final int PARITY_ODD = 2;
    /**
     * Set baud rate for session. Default is 115,200.
     * 
     * @param baud -
     *            baud rate
     */
    public void setBaudRate(int baud);
    /**
     * Get baud rate for session. Default is 115,200.
     * 
     * @return baud rate
     */
    public int getBaudRate();
    /**
     * Set data bits (7 or 8). Default is 8.
     * 
     * @param bits -
     *            data bits (7 or 8)
     */
    public void setDataBits(int bits);
    /**
     * Get data bits (7 or 8). Default is 8.
     * 
     * @return data bits
     */
    public int getDataBits();
    /**
     * Set parity. SerialPortSession.PARITY_NONE is default.
     * 
     * @param parity -
     *            parity setting (PARITY_NONE, PARITY_EVEN, PARITY_ODD)
     */
    public void setParity(int parity);
    /**
     * Get parity.
     * 
     * @return parity setting (PARITY_NONE, PARITY_EVEN, PARITY_ODD).
     */
    public int getParity();
}
