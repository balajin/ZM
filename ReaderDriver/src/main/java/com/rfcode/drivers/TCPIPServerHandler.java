package com.rfcode.drivers;

/**
 * Interface for handling server socket connect requests. All methods MUST be
 * handled quickly and without blocking, as they are invoked by the
 * SessionFactory thread.
 * 
 * @author Mike Primm
 */

public interface TCPIPServerHandler {
    /**
     * Session accepted - used to notify when a client connection has been
     * established with the server
     * 
     * @param server -
     *            our server
     * @param session -
     *            session with client
     */
    public void sessionAcceptCompleted(TCPIPServerSocket server,
        TCPIPSession session);
    /**
     * Server socket bind completed
     * 
     * @param server -
     *            our server
     * @param is_bound -
     *            true if successful, false if failed
     */
    public void serverBindCompleted(TCPIPServerSocket server, boolean is_bound);
    /**
     * Server socket unbind completed
     * 
     * @param server -
     *            our server
     */
    public void serverUnbindCompleted(TCPIPServerSocket server);
}
