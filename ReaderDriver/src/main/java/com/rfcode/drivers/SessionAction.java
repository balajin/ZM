package com.rfcode.drivers;

/**
 * Interface for actions run by SessionFactory thread.
 * 
 * @author Mike Primm
 */
public interface SessionAction {
    /**
     * Method to be run when action is executed by factory thread
     */
    public void runSessionAction();
}
