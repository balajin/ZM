package com.rfcode.drivers;

/**
 * Callback interface for reporting the completion of a data write request to a
 * session.
 * 
 * @author Mike Primm
 */
public interface SessionDataWriteHandler {
    /**
     * Data write to session completed - callback to notify that a write request
     * finished.
     * 
     * @param sdw -
     *            write request result - only valid until end of
     *            sessionDataWriteComplete callback
     */
    public void sessionDataWriteComplete(SessionDataWriteRequest sdw);
}
