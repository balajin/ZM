package com.rfcode.drivers;

import java.net.InetSocketAddress;

/**
 * Interface for TCP-based server socket
 * 
 * @author Mike Primm
 */
public interface TCPIPServerSocket {
    /**
     * Get our session factory
     * 
     * @returns session factory
     */
    public SessionFactory getSessionFactory();
    /**
     * Set the server socket handler for this server - used to report
     * connections
     * 
     * @param handler -
     *            server hander (null=none)
     */
    public void setServerHandler(TCPIPServerHandler handler);
    /**
     * Get the server socket handler for this session.
     * 
     * @return server socket handler (null=none)
     */
    public TCPIPServerHandler getServerHandler();
    /**
     * Set the TCP/IP address and port for the server to bind to. Use
     * InetSocketAddress(int port) for "any" interface.
     * 
     * @param ip -
     *            address
     */
    public void setIPAddress(InetSocketAddress ip);
    /**
     * Get the TCP/IP address and port the server is to bind to.
     * 
     * @return ip address and port
     */
    public InetSocketAddress getIPAddress();
    /**
     * Request that the server socket bind (which makes it active)
     */
    public void requestBind() throws SessionException;
    /**
     * Request that the server socket unbind (which makes it inactive)
     */
    public void requestUnbind() throws SessionException;
}
