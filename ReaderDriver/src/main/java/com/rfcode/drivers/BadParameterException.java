package com.rfcode.drivers;

/**
 * General exception for reporting bad parameters, generally tied to init()
 * methods.
 * 
 * @author Mike Primm
 * 
 */
public class BadParameterException extends Exception {
    static final long serialVersionUID = 128409120894029518L;
    /**
     * Constructor
     * 
     * @param msg -
     *            error message
     */
    public BadParameterException(String msg) {
        super(msg);
    }
}
