%global _binaries_in_noarch_packages_terminate_build 0

Summary: @COMPANYNAME@ @PRODUCTNAME@ aggregates data from @COMPANYNAME@ RFID readers
Name: @RPMNAME@
Version: @VERSION@
Release: @RELEASE@
License: commercial
URL: http://www.rfcode.com
Packager: Josh Martinek <josh@rfcode.com>
Group: Application/System
Requires: chkconfig shadow-utils
BuildRoot: /nodir
BuildArch: noarch
Vendor: @COMPANYNAME@

%description
@COMPANYNAME@ @PRODUCTNAME@ is a server application that aggregates data from @COMPANYNAME@
RFID readers. The server also provides an API to access this data.

%pre
# Add rfcode user if not already present
if ! grep -q rfcode /etc/passwd; then
    useradd -c "@COMPANYNAME@ Service User" -d /usr/share/rfcode -s /sbin/nologin -M rfcode 2>/dev/null
elif grep -q rfcode /etc/passwd; then
    # Modify the rfcode user's home directory
    usermod -d /usr/share/rfcode rfcode
fi

# Remove the contents of the work directory if present
if [ -e /usr/share/rfcode/zonemgr/jetty/work ]; then
    rm -rf /usr/share/rfcode/zonemgr/jetty/work/*
fi

# If this is an upgrade from Ranger to zonemgr, then stop Ranger and remove its startup scripts
if [ -e /etc/init.d/rangerinit ]; then
    /etc/init.d/rangerinit stop >/dev/null 2>&1
    rm -f /etc/rc.d/rc5.d/S99rangerinit /etc/rc.d/rc4.d/S99rangerinit /etc/rc.d/rc3.d/S99rangerinit /etc/init.d/rangerinit
fi

# Stop service before we install, if it's running
rfczonemgr_status=`/sbin/service rfczonemgr status 2>/dev/null`
if echo $rfczonemgr_status | grep -q running; then
  /etc/rc.d/init.d/rfczonemgr stop >/dev/null
fi

# rpm cannot handle laying down a file whose name has been used as a directory name in the previous version
#  i.e. jre-i586/man/ja
if [ -d /usr/share/rfcode/zonemgr/jre-i586 ]; then
    echo "Remove old jre-i586..."
    rm -rf /usr/share/rfcode/zonemgr/jre-i586
fi
if [ -d /usr/share/rfcode/zonemgr/jre-x86_64 ]; then
    echo "Remove old jre-x86_64..."
    rm -rf /usr/share/rfcode/zonemgr/jre-x86_64
fi

%files
%defattr(-,root,root)
/etc/rc.d/init.d/rfczonemgr
%attr(-,rfcode,rfcode) /usr/share/rfcode/.jettyrc
%attr(-,rfcode,rfcode) /usr/share/rfcode/zonemgr
%attr(755,rfcode,rfcode) /var/run/zonemgr
%config(noreplace) /usr/share/rfcode/zonemgr/zonemgr.datadir/license.key

%doc
/usr/share/doc/@RPMNAME@-%{version}/

%post
/sbin/chkconfig --level 345 rfczonemgr on
chmod 755 /usr/share/rfcode/zonemgr/jre-i586/bin/*
chmod 755 /usr/share/rfcode/zonemgr/jre-x86_64/bin/*
chmod 755 /etc/rc.d/init.d/rfczonemgr
chmod 755 /usr/share/rfcode/zonemgr/jetty/bin/jetty.sh

# Migrate data from Ranger if applicable
if [ -d /usr/share/rfcode/Ranger/ranger.datadir ]; then
    mv -f /usr/share/rfcode/Ranger/ranger.datadir /usr/share/rfcode/zonemgr/zonemgr.datadir
fi

# Remove old Ranger dir
if [ -d /usr/share/rfcode/Ranger ]; then
    rm -rf /usr/share/rfcode/Ranger
fi

# Create SSL self-signed cert if necessary
if [ ! -e /usr/share/rfcode/zonemgr/conf/keystore ]; then
    if uname -p | grep x86_64 > /dev/null ; then
      /usr/share/rfcode/zonemgr/jre-x86_64/bin/keytool -genkey \
        -keystore /usr/share/rfcode/zonemgr/conf/keystore  \
        -keyalg rsa -dname "CN=@CERT_CN@, OU=@CERT_OU@, O=@CERT_O@, L=@CERT_L@, ST=@CERT_ST@, C=@CERT_C@" \
        -alias selfsigned -validity 3650 -keypass rfcode -storepass rfcode
    else
      /usr/share/rfcode/zonemgr/jre-i586/bin/keytool -genkey \
        -keystore /usr/share/rfcode/zonemgr/conf/keystore  \
        -keyalg rsa -dname "CN=@CERT_CN@, OU=@CERT_OU@, O=@CERT_O@, L=@CERT_L@, ST=@CERT_ST@, C=@CERT_C@" \
        -alias selfsigned -validity 3650 -keypass rfcode -storepass rfcode
    fi
fi

# Change ownership from root to rfcode
chown -R rfcode.rfcode /usr/share/rfcode/zonemgr

# Start service
/sbin/service rfczonemgr start >/dev/null

%preun
# Stop service before we uninstall, if it's running
rfczonemgr_status=`/sbin/service rfczonemgr status 2>/dev/null`
if echo $rfczonemgr_status | grep -q running; then
  /etc/rc.d/init.d/rfczonemgr stop >/dev/null
fi

# Remove service from run control
/sbin/chkconfig rfczonemgr off

%postun
# If this was an upgrade set service to start again
if [ -e /etc/rc.d/init.d/rfczonemgr ]; then
    /sbin/chkconfig --level 345 rfczonemgr on
    /sbin/service rfczonemgr start >/dev/null
else
    # Remove rfcode user if present
    if grep -q rfcode /etc/passwd; then
        userdel rfcode
    fi
fi

# Remove dynamic doc directory since rpm will not
if [ -d /usr/share/doc/@RPMNAME@-%{version} ]; then
    rm -rf /usr/share/doc/@RPMNAME@-%{version} 2>/dev/null
fi

%changelog
* Tue Nov 27 2007 Josh Martinek <josh@rfcode.com>
- Initial release

