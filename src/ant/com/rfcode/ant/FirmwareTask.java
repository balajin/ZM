//------------------------------ tabstop = 4 ----------------------------------
//Copyright (C) 2007. RFCode, Inc.
//
//All rights reserved.
//
//This software is protected by copyright laws of the United States
//and of foreign countries. This material may also be protected by
//patent laws of the United States and of foreign countries.
//
//This software is furnished under a license agreement and/or a
//nondisclosure agreement and may only be used or copied in accordance
//with the terms of those agreements.
//
//The mere transfer of this software does not imply any licenses of trade
//secrets, proprietary technology, copyrights, patents, trademarks, or
//any other form of intellectual property whatsoever.
//
//RFCode, Inc. retains all ownership rights.
//
//-----------------------------------------------------------------------------
//
//Class Name:          FirmwareTask.java
//
//Written By:          Richie Jefts
//------------------------------ tabstop = 4 ----------------------------------
package com.rfcode.ant;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class FirmwareTask extends Task {

    private static final String DOWNLOAD_REPO = "http://source.rfcode.com/artifacts/firmwarepackager/";

    private String _top;
    private String _oem;

    public void setTop(String top) {
        _top = top;
    }

    public void setOem(String oem) {
        _oem = oem;
    }

    public void execute() throws BuildException {
        File firmwareProps = new File(_top + File.separator + "oem" + File.separator + _oem + File.separator + "firmware.properties");
        if (!firmwareProps.exists()) {
            firmwareProps = new File(_top + File.separator + "oem" + File.separator + "firmware.properties");
        }
        Properties props = loadProperties(firmwareProps);

        String tag   = props.getProperty("firmwarepackage.tag");
        String fwzip = props.getProperty("firmwarepackage.file");
        String input = tag + "/" + fwzip;
        File   image = new File(_top, "images");
        image.mkdirs();


        image = new File(image, "firmware.zip");

        downloadImage(DOWNLOAD_REPO + input, image);
        downloadImage(DOWNLOAD_REPO + tag + "/firmware.properties", new File(_top, _oem + "-firmware.properties"));
    }

    private void downloadImage(String inputURL, File file) throws BuildException {
        // ensure directories exist
        file.getParentFile().mkdirs();

        System.out.println("Downloading firmware: " + inputURL + " -> " + file);

        HttpURLConnection conn;
        InputStream  input = null;
        OutputStream output = null;

        try {
            URL url = new URL(inputURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            input  = conn.getInputStream();
            output = new BufferedOutputStream(new FileOutputStream(file), 8096);

            int inByte;
            while((inByte = input.read()) != -1) {
                output.write(inByte);
            }
            output.flush();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (input != null) {
                    input.close();
                }
            } 
            catch (Exception e) {
            }
            try {
                if (output != null) {
                    output.close();
                }
            } 
            catch (Exception e) {
            }
        }
    }

    /**
     * Loads the firmware.properties file containing information about the
     * firmware to include.
     */
    private static Properties loadProperties(File file) throws BuildException {
        Properties  props = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(file);
            props.load(input);
            return props;
        }
        catch (IOException e) {
            throw new BuildException("Unable to load properties file: " + file, e);
        }
        finally {
            try {
                if (input != null) input.close();
            }
            catch (IOException e) {
            }
        }
    }
}
